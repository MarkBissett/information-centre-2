﻿namespace WoodPlan5
{
    partial class frm_AS_Sim_Card_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Sim_Card_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.simCardGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11117SimCardItemBindingSource = new System.Windows.Forms.BindingSource();
            this.simCardGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSimID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSimNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProviderCustomerReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobileNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSimStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSimStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConnectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisconnectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHasBeenSwapped = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NotesMemoExEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.webRepositoryItemHyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.lueSupplierType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp_AS_11117_Sim_Card_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11117_Sim_Card_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simCardGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11117SimCardItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simCardGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoExEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.webRepositoryItemHyperLinkEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.Manager = null;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1380, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 751);
            this.barDockControlBottom.Size = new System.Drawing.Size(1380, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 751);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1380, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 751);
            // 
            // bbiSave
            // 
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.LargeGlyph")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.Manager = null;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiCancel});
            this.barManager1.MaxItemId = 31;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("save_16x16.png", "images/save/save_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/save/save_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "save_16x16.png");
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1675, 521, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1380, 751);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1380, 751);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // simCardGridControl
            // 
            this.simCardGridControl.DataSource = this.spAS11117SimCardItemBindingSource;
            this.simCardGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simCardGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.simCardGridControl.EmbeddedNavigator.Buttons.Append.ImageIndex = 0;
            this.simCardGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.simCardGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.simCardGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.simCardGridControl.EmbeddedNavigator.Buttons.Edit.ImageIndex = 1;
            this.simCardGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.simCardGridControl.EmbeddedNavigator.Buttons.EndEdit.ImageIndex = 4;
            this.simCardGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.simCardGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.simCardGridControl.EmbeddedNavigator.Buttons.Remove.ImageIndex = 2;
            this.simCardGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "";
            this.simCardGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.simCardGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "", "delete")});
            this.simCardGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.simCardGridControl_EmbeddedNavigator_ButtonClick);
            this.simCardGridControl.Location = new System.Drawing.Point(0, 0);
            this.simCardGridControl.MainView = this.simCardGridView;
            this.simCardGridControl.Name = "simCardGridControl";
            this.simCardGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.NotesMemoExEdit,
            this.webRepositoryItemHyperLinkEdit,
            this.lueSupplierType,
            this.TextEdit});
            this.simCardGridControl.Size = new System.Drawing.Size(1380, 751);
            this.simCardGridControl.TabIndex = 5;
            this.simCardGridControl.UseEmbeddedNavigator = true;
            this.simCardGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.simCardGridView});
            // 
            // spAS11117SimCardItemBindingSource
            // 
            this.spAS11117SimCardItemBindingSource.DataMember = "sp_AS_11117_Sim_Card_Item";
            this.spAS11117SimCardItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // simCardGridView
            // 
            this.simCardGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSimID,
            this.colSimNumber,
            this.colProviderCustomerReference,
            this.colMobileNumber,
            this.colSimStatusID,
            this.colSimStatus,
            this.colConnectionDate,
            this.colDisconnectionDate,
            this.colHasBeenSwapped,
            this.colNotes,
            this.colMode,
            this.colRecordID});
            this.simCardGridView.GridControl = this.simCardGridControl;
            this.simCardGridView.Name = "simCardGridView";
            this.simCardGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.simCardGridView.OptionsFind.AlwaysVisible = true;
            this.simCardGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.simCardGridView.OptionsLayout.StoreAppearance = true;
            this.simCardGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.simCardGridView.OptionsSelection.MultiSelect = true;
            this.simCardGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.simCardGridView.OptionsView.ColumnAutoWidth = false;
            this.simCardGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.simCardGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.simCardGridView_PopupMenuShowing);
            this.simCardGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.simCardGridView_SelectionChanged);
            this.simCardGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.simCardGridView_CustomDrawEmptyForeground);
            this.simCardGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.simCardGridView_CustomFilterDialog);
            this.simCardGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.simCardGridView_MouseUp);
            this.simCardGridView.DoubleClick += new System.EventHandler(this.simCardGridView_DoubleClick);
            // 
            // colSimID
            // 
            this.colSimID.FieldName = "SimID";
            this.colSimID.Name = "colSimID";
            this.colSimID.OptionsColumn.AllowEdit = false;
            this.colSimID.OptionsColumn.AllowFocus = false;
            this.colSimID.OptionsColumn.ReadOnly = true;
            this.colSimID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSimNumber
            // 
            this.colSimNumber.FieldName = "SimNumber";
            this.colSimNumber.Name = "colSimNumber";
            this.colSimNumber.OptionsColumn.AllowEdit = false;
            this.colSimNumber.OptionsColumn.AllowFocus = false;
            this.colSimNumber.OptionsColumn.ReadOnly = true;
            this.colSimNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSimNumber.Visible = true;
            this.colSimNumber.VisibleIndex = 0;
            this.colSimNumber.Width = 269;
            // 
            // colProviderCustomerReference
            // 
            this.colProviderCustomerReference.FieldName = "ProviderCustomerReference";
            this.colProviderCustomerReference.Name = "colProviderCustomerReference";
            this.colProviderCustomerReference.OptionsColumn.AllowEdit = false;
            this.colProviderCustomerReference.OptionsColumn.AllowFocus = false;
            this.colProviderCustomerReference.OptionsColumn.ReadOnly = true;
            this.colProviderCustomerReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProviderCustomerReference.Visible = true;
            this.colProviderCustomerReference.VisibleIndex = 1;
            this.colProviderCustomerReference.Width = 241;
            // 
            // colMobileNumber
            // 
            this.colMobileNumber.FieldName = "MobileNumber";
            this.colMobileNumber.Name = "colMobileNumber";
            this.colMobileNumber.OptionsColumn.AllowEdit = false;
            this.colMobileNumber.OptionsColumn.AllowFocus = false;
            this.colMobileNumber.OptionsColumn.ReadOnly = true;
            this.colMobileNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMobileNumber.Visible = true;
            this.colMobileNumber.VisibleIndex = 2;
            this.colMobileNumber.Width = 183;
            // 
            // colSimStatusID
            // 
            this.colSimStatusID.FieldName = "SimStatusID";
            this.colSimStatusID.Name = "colSimStatusID";
            this.colSimStatusID.OptionsColumn.AllowEdit = false;
            this.colSimStatusID.OptionsColumn.AllowFocus = false;
            this.colSimStatusID.OptionsColumn.ReadOnly = true;
            this.colSimStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSimStatusID.Width = 85;
            // 
            // colSimStatus
            // 
            this.colSimStatus.FieldName = "SimStatus";
            this.colSimStatus.Name = "colSimStatus";
            this.colSimStatus.OptionsColumn.AllowEdit = false;
            this.colSimStatus.OptionsColumn.AllowFocus = false;
            this.colSimStatus.OptionsColumn.ReadOnly = true;
            this.colSimStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSimStatus.Visible = true;
            this.colSimStatus.VisibleIndex = 3;
            this.colSimStatus.Width = 134;
            // 
            // colConnectionDate
            // 
            this.colConnectionDate.FieldName = "ConnectionDate";
            this.colConnectionDate.Name = "colConnectionDate";
            this.colConnectionDate.OptionsColumn.AllowEdit = false;
            this.colConnectionDate.OptionsColumn.AllowFocus = false;
            this.colConnectionDate.OptionsColumn.ReadOnly = true;
            this.colConnectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colConnectionDate.Visible = true;
            this.colConnectionDate.VisibleIndex = 4;
            this.colConnectionDate.Width = 101;
            // 
            // colDisconnectionDate
            // 
            this.colDisconnectionDate.FieldName = "DisconnectionDate";
            this.colDisconnectionDate.Name = "colDisconnectionDate";
            this.colDisconnectionDate.OptionsColumn.AllowEdit = false;
            this.colDisconnectionDate.OptionsColumn.AllowFocus = false;
            this.colDisconnectionDate.OptionsColumn.ReadOnly = true;
            this.colDisconnectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDisconnectionDate.Visible = true;
            this.colDisconnectionDate.VisibleIndex = 5;
            this.colDisconnectionDate.Width = 113;
            // 
            // colHasBeenSwapped
            // 
            this.colHasBeenSwapped.FieldName = "HasBeenSwapped";
            this.colHasBeenSwapped.Name = "colHasBeenSwapped";
            this.colHasBeenSwapped.OptionsColumn.AllowEdit = false;
            this.colHasBeenSwapped.OptionsColumn.AllowFocus = false;
            this.colHasBeenSwapped.OptionsColumn.ReadOnly = true;
            this.colHasBeenSwapped.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHasBeenSwapped.Visible = true;
            this.colHasBeenSwapped.VisibleIndex = 6;
            this.colHasBeenSwapped.Width = 113;
            // 
            // colNotes
            // 
            this.colNotes.FieldName = "Notes";
            this.colNotes.Name = "colNotes";
            this.colNotes.OptionsColumn.AllowEdit = false;
            this.colNotes.OptionsColumn.AllowFocus = false;
            this.colNotes.OptionsColumn.ReadOnly = true;
            this.colNotes.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes.Visible = true;
            this.colNotes.VisibleIndex = 7;
            this.colNotes.Width = 737;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // NotesMemoExEdit
            // 
            this.NotesMemoExEdit.AutoHeight = false;
            this.NotesMemoExEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NotesMemoExEdit.Name = "NotesMemoExEdit";
            this.NotesMemoExEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.NotesMemoExEdit.ShowIcon = false;
            // 
            // webRepositoryItemHyperLinkEdit
            // 
            this.webRepositoryItemHyperLinkEdit.AutoHeight = false;
            this.webRepositoryItemHyperLinkEdit.Name = "webRepositoryItemHyperLinkEdit";
            this.webRepositoryItemHyperLinkEdit.SingleClick = true;
            // 
            // lueSupplierType
            // 
            this.lueSupplierType.AutoHeight = false;
            this.lueSupplierType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSupplierType.DisplayMember = "Value";
            this.lueSupplierType.Name = "lueSupplierType";
            this.lueSupplierType.ValueMember = "Pick_List_ID";
            // 
            // TextEdit
            // 
            this.TextEdit.AutoHeight = false;
            this.TextEdit.Name = "TextEdit";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // bbiCancel
            // 
            this.bbiCancel.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiCancel.Caption = "Cancel and Close";
            this.bbiCancel.DropDownEnabled = false;
            this.bbiCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCancel.Glyph")));
            this.bbiCancel.Id = 30;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11117_Sim_Card_ItemTableAdapter
            // 
            this.sp_AS_11117_Sim_Card_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Sim_Card_Manager
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1380, 751);
            this.Controls.Add(this.simCardGridControl);
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Sim_Card_Manager";
            this.Text = "Sim Card Manager";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.frm_AS_Sim_Card_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Sim_Card_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            this.Controls.SetChildIndex(this.simCardGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simCardGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11117SimCardItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simCardGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoExEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.webRepositoryItemHyperLinkEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl simCardGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView simCardGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit NotesMemoExEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit webRepositoryItemHyperLinkEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueSupplierType;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit TextEdit;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource spAS11117SimCardItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSimID;
        private DevExpress.XtraGrid.Columns.GridColumn colSimNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colProviderCustomerReference;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSimStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colSimStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colConnectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDisconnectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHasBeenSwapped;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11117_Sim_Card_ItemTableAdapter sp_AS_11117_Sim_Card_ItemTableAdapter;
    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraSplashScreen;
using System.IO;
using System.Net.Mail;
using System.Net;

namespace WoodPlan5
{
    public partial class frm_AS_P11D_Manager : BaseObjects.frmBase
    {
        public frm_AS_P11D_Manager()
        {
            InitializeComponent();
        }

        #region Instance Variables

        DataTable dt = new DataTable();

        private enum SendEmailType { none, selected, all }
        private bool isRunning = false;
        private string strSMTPMailServerAddress = "";
        private string strSMTPMailServerUsername = "";
        private string strSMTPMailServerPassword = "";
        private string strSMTPMailServerPort = "";

        private string strP11DSubjectLine = "";
        private string strFromAddress = "";
        private string strFromAddressDisplay = "";
        private string strP11DEmailHTMLLayoutFile = "";
        private string strP11DEmailList = "";
        bool blnSendOk = true;

        // Used by Grid View State Facilities //
        public RefreshGridState RefreshUnsentGridViewStateP11D;
        public RefreshGridState RefreshSentGridViewStateP11D; 
        private Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public int numOfSelectedRows = 0;
        private int i_int_FocusedGrid = 0;

        GridHitInfo downHitInfo = null;
        private uint _PropertyName;
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        private bool bool_FormLoading = true;

        public bool p11dChanged = false;
        public enum FormMode { add, edit, view, delete, block_add, block_edit };
        public enum SentenceCase { Upper, Lower, Title, AsIs }

        private string strMessage1, strMessage2;

        private bool iBool_AllowDelete = false;
        private bool iBool_AllowAdd = false;
        private bool iBool_AllowEdit = false;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        private string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //               
        private string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //


        #endregion

        #region Events

        private void frm_AS_P11D_Manager_Load(object sender, EventArgs e)
        {
            this.FormID = 1121;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            LoadConnectionStrings(this.GlobalSettings.ConnectionString);
            LoadAdapters();
            // Get Form Permissions //            
            ProcessPermissionsForForm();
            RefreshUnsentGridViewStateP11D = new RefreshGridState(unsentManagerGridView, "P11DID");
            RefreshSentGridViewStateP11D = new RefreshGridState(sentManagerGridView, "P11DID");
            PostOpen(null);
            createDatatable();
            loadSettings();
        }

        private void managerGridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void managerGridView_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void managerGridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "unsentManagerGridView":
                     message = "No P11D records flagged as unsent";
                    break;
                case "sentManagerGridView":
                     message = "No P11D records flagged as sent";
                    break;             
                default:
                     message = "No Details Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void managerGridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            if (isRunning) return;
            GridView view = sender as GridView;
            numOfSelectedRows = view.SelectedRowsCount;
            isRunning = false;
            
            int tempNumOfSelectedRows;
            if (numOfSelectedRows > 0)
            {
                tempNumOfSelectedRows = numOfSelectedRows;
            }
            else
            {
                tempNumOfSelectedRows = 1;
            }
            int[] intP11DIDs = new int[tempNumOfSelectedRows];

            int[] intRowHandles = view.GetSelectedRows();
            if (view.SelectedRowsCount > 0)
            {
                int countRows = 0;

                foreach (int intRowHandle in intRowHandles)
                {
                    DataRow dr = view.GetDataRow(intRowHandle);
                    switch (i_int_FocusedGrid)
                    {
                        case 0://P11D
                            intP11DIDs[countRows] = (((DataSet_AS_DataEntry.sp_AS_11166_P11D_ItemRow)(dr)).P11DID);
                            countRows += 1;
                            break;   
                    }
                }               
                    strRecordIDs = stringRecords(intP11DIDs);               
            }
            else
            {
                strRecordIDs = "";
            }
            SetMenuStatus();  
        }
        
        private void p11dGridControl_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = unsentManagerGridView;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                  
                    break;
                default:
                    break;
            }
        }
             
        private void frm_AS_Supplier_Manager_Activated(object sender, EventArgs e)
        {
            frmActivated();
        }
        
        private void bbiCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void commonGridView_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            string GridName = ((GridView)sender).Name;
            switch (GridName)
            {
                case "unsentManagerGridView":
                    i_int_FocusedGrid = 0;
                    break;

                case "sentManagerGridView":
                    i_int_FocusedGrid = 1;
                    break;
            }
        }

        private void p11dGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void p11dGridView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void bbiRefreshAll_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadAdapters();
            loadSettings();
        }
        
        private void bbiSelectedUnsent_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            GridView view = (GridView)unsentManagerGridControl.MainView;
            view.PostEditor();
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedRecords = "";
            if (intCount >= 1)
            {
                switch (XtraMessageBox.Show("You are about to email " + intCount + " selected record(s), do you want to proceed ?", "Mailing Selected Records", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.No:
                        // cancel operation //
                        XtraMessageBox.Show("The operation has been cancelled successfully.", "Emailing Cancelled", MessageBoxButtons.OK);
                        break;
                    case DialogResult.Yes:
                        // email selected //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strSelectedRecords += ((DataSet_AS_DataEntry.sp_AS_11166_P11D_ItemRow)(dr)).P11DID.ToString() + ',';
                        }
                        sendEmailList(SendEmailType.selected, strSelectedRecords);
                        break;
                }
            }
            else
            {
                XtraMessageBox.Show("Please select records in the 'Unsent Email List' to proceed", "No Records Selected", MessageBoxButtons.OK);
            }
        }

        private void bbiEditEmailList_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void ceSelected_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;

            bool blnCheckStatus = ce.Checked;
            e.Cancel = false;
            string strRecordSelected = "";
            DataRowView currentRow = null;
            switch (i_int_FocusedGrid)
            {
                case 0:
                    currentRow = (DataRowView)spAS11166P11DUnsentBindingSource.Current;
                    break;
                case 1:
                    currentRow = (DataRowView)spAS11166P11DSentBindingSource.Current;
                    break;

            }

            if (currentRow != null)
            {
                strRecordSelected = currentRow["P11DID"].ToString();
            }

            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter updateRecord = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            updateRecord.ChangeConnectionString(strConnectionString);
            updateRecord.sp_AS_11167_P11D_Update_Manager("edit", strRecordSelected, Convert.ToInt32(strRecordSelected), !blnCheckStatus);

            LoadAdapters();
        }

        private void bbiAllUnsentEmails_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            sendEmailList(SendEmailType.all, "");
        }
        
        #endregion

        #region Method

        public void frmActivated()
        {
            if (UpdateRefreshStatus > 0 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
            {
                Load_Data();
            }
            SetMenuStatus();
        }

        private string stringRecords(int[] IDs)
        {
            string strIDs = "";
            foreach (int rec in IDs)
            {
                strIDs += Convert.ToString(rec) + ',';
            }
            return strIDs;
        }
              
        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }
  
        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0)
                UpdateRefreshStatus = 0;


            sp_AS_11166_P11D_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11166_P11D_Item, "", strFormMode);
            unsentManagerGridControl.BeginUpdate();
            this.RefreshUnsentGridViewStateP11D.LoadViewInfo();  // Reload any expanded groups and selected rows //
            unsentManagerGridControl.EndUpdate();

            sentManagerGridControl.BeginUpdate();
            this.RefreshSentGridViewStateP11D.LoadViewInfo();  // Reload any expanded groups and selected rows //
            sentManagerGridControl.EndUpdate();

            //// Highlight any recently added new rows //
            //if (i_str_AddedRecordIDs1 != "")
            //{
            //    char[] delimiters = new char[] { ';' };
            //    string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            //    int intID = 0;
            //    int intRowHandle = 0;
            //    GridView view = (GridView)unsentManagerGridControl.MainView;
            //    view.ClearSelection(); // Clear any current selection so just the new record is selected //
            //    foreach (string strElement in strArray)
            //    {
            //        intID = Convert.ToInt32(strElement);
            //        intRowHandle = view.LocateByValue(0, view.Columns["P11DID"], intID);
            //        if (intRowHandle != GridControl.InvalidRowHandle)
            //        {
            //            view.SelectRow(intRowHandle);
            //            view.MakeRowVisible(intRowHandle, false);
            //        }
            //    }
            //    i_str_AddedRecordIDs1 = "";
            //}
        }

        private void getCurrentGridControl(out GridControl gridControl, out string strMessage1, out string strMessage2)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://P11D
                    gridControl = unsentManagerGridControl;
                    strMessage1 = "1 P11D record";
                    strMessage2 = " P11D records";
                    break;               
                default:
                    gridControl = unsentManagerGridControl;
                    strMessage1 = "1 P11D record";
                    strMessage2 = " P11D records";
                    break;
            }
        }
        
        private void storeGridViewState()
        {
            // Store Grid View State //
            this.RefreshUnsentGridViewStateP11D.SaveViewInfo();
            this.RefreshSentGridViewStateP11D.SaveViewInfo();
        }

        private void emptyForeGroundMessage(GridView view, string message, CustomDrawEventArgs e)
        {
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString(message, e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }
              
        private void Delete_Record()
        {
            if (!iBool_AllowDelete)
                return;
            int[] intRowHandles;
            int intCount = 0;
            GridControl gridControl = null;
            GridView view = null;
            string strMessage = "";


            switch (i_int_FocusedGrid)
            {
                case 0:     // P11D
                    if (!iBool_AllowDelete)
                        return;
                    break;
                default:
                    if (!iBool_AllowDelete)
                        return;
                    break;
            }
            getCurrentGridControl(out gridControl, out strMessage1, out strMessage2);

            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records to delete.", "No Records To Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strRecordsToLoad = "";
            foreach (int intRowHandle in intRowHandles)
            {
                DataRow dr = view.GetDataRow(intRowHandle);
                switch (i_int_FocusedGrid)
                {
                    case 0:     // P11D
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11166_P11D_ItemRow)(dr)).P11DID).ToString() + ',';
                        break;
                }
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? strMessage1 : Convert.ToString(intRowHandles.Length) + strMessage2) +
            " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this record" : "these records") +
            " will no longer be available for selection and any related records will also be deleted!";
            if (XtraMessageBox.Show(strMessage, "Permanently Delete Record(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshUnsentGridViewStateP11D.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshUnsentGridViewStateP11D.SaveViewInfo();  // Store Grid View State //
                try
                {
                    switch (i_int_FocusedGrid)
                    {
                        case 0:     // P11D record
                            sp_AS_11166_P11D_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                            break;                        
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error deleting", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                Load_Data();

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Edit_Record()
        {
            XtraMessageBox.Show("Manipulation of P11D records is restricted.", "Restricted Access Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            return;       
            //if (!iBool_AllowEdit)
            //    return;
            //OpenEditForm(FormMode.edit, "frm_AS_P11D_Manager");
        }

        private void Add_Record()
        {
            if (!iBool_AllowAdd)
                return;
            OpenEditForm(FormMode.add, "frm_AS_P11D_Manager");
        }

        private void OpenEditForm(FormMode mode, string frmCaller)
        {
            try
            {
                GridView view = (GridView)unsentManagerGridControl.MainView; ;
                frmProgress fProgress = null;
                System.Reflection.MethodInfo method = null;
                string strRecordsToLoad = "";
               
                int intCount = 0;

                int[] intRowHandles = view.GetSelectedRows();

                intCount = intRowHandles.Length;
                if (mode == FormMode.edit || mode == FormMode.block_edit)
                {
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more P11D record(s) to edit before proceeding.", "Edit P11D Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        DataRow dr = view.GetDataRow(intRowHandle);
                        strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11166_P11D_ItemRow)(dr)).P11DID).ToString() + ',';
                    }
                }

               
                storeGridViewState();
                //Check if open already
                bool alreadyOpen ;
                if (Application.OpenForms["frm_AS_P11D_Edit"] != null)
                {
                    alreadyOpen = true;
                }
                else
                {
                    alreadyOpen = false;
                }

                if (alreadyOpen)
                {
                    XtraMessageBox.Show("Another instance of the P11D Edit form is already open, please finish operation before you start another.", "Edit Form Open", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {

                    frm_AS_P11D_Edit sChildForm = new frm_AS_P11D_Edit();
                    sChildForm.MdiParent = this.MdiParent;
                    sChildForm.GlobalSettings = this.GlobalSettings;
                    sChildForm.strRecordIDs = strRecordsToLoad;
                    sChildForm.strFormMode = mode.ToString();
                    sChildForm.formMode = (frm_AS_P11D_Edit.FormMode)mode;
                    sChildForm.strCaller = frmCaller;
                    sChildForm.intRecordCount = 0;
                    sChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager1 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    sChildForm.splashScreenManager = splashScreenManager1;
                    sChildForm.splashScreenManager.ShowWaitForm();
                    sChildForm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to load the form.", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                    

        }
            
        private void CheckChangedTable(DataTable dt)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                p11dChanged = true;
            }
        }
     
        private string SaveChangesExtracted(out bool shouldReturn)
        {
            shouldReturn = false;

            if (dxErrorProvider.HasErrors)
            {
                shouldReturn = true;
                return "Error";
            }

            return String.Empty;
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            if (strFormMode.ToLower() == "add")
            {
                // add
            }
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //         
            EndEdit();
            this.Validate();
            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
                return result;

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            EndEdit();

            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11166_P11D_Item);//P11D Check
            try
            {
                // Insert and Update queries defined in Table Adapter //
                if (p11dChanged)
                {
                    this.sp_AS_11166_P11D_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                    p11dChanged = false;
                }
               
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }
            
            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private void EndEdit()
        {
            spAS11166P11DSentBindingSource.EndEdit();
        }

        private string CheckForPendingSave()
        {
            EndEdit();

            string strMessage = "";
            string strMessageSupplier = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11166_P11D_Item, " on the P11D Form ");//P11D Check

            if (strMessageSupplier != "")
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (strMessageSupplier != "") strMessage += strMessageSupplier;
            }
            return strMessage;
        }

        private string CheckTablePendingSave(DataTable dt, string formArea)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)" + formArea + "\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)" + formArea + "\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)" + formArea + "\n";
            }
            return strMessage;
        }
                
        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;

            bbiSave.Enabled = false;
            bbiCancel.Enabled = true;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            getCurrentView(out view);

            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            toggleMenuButtons(alItems, intRowHandles);

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) 
                frmParent.PermissionsHandler(alItems);

            // Set enabled status of equipmentView navigator custom buttons //
            setCustomNavigatorAccess(unsentManagerGridControl, view);
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        private void setCustomNavigatorAccess(GridControl xGridControl, GridView view)
        {
            view = (GridView)xGridControl.MainView;
            int[] intRowHandles = view.GetSelectedRows();
         
            if (iBool_AllowEdit)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
           
        }

        private void toggleMenuButtons(ArrayList alItems, int[] intRowHandles)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://P11D

                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        bbiBlockAdd.Enabled = false;
                    }

                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
            }
        }
        
        private void getCurrentView(out GridView view)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://P11D
                    view = (GridView)unsentManagerGridControl.MainView;
                    break;
                default:
                    view = (GridView)unsentManagerGridControl.MainView;
                    break;
            }

        }

        private void LoadConnectionStrings(string ConnString)
        {
            strConnectionString = ConnString;
            sp_AS_11166_P11D_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
        }

        internal void LoadAdapters()
        {
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            sp_AS_11166_P11D_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11166_P11D_Item, "", strFormMode);
            spAS11166P11DSentBindingSource.Filter = "SelectedForEmail = 0";
            spAS11166P11DUnsentBindingSource.Filter = "SelectedForEmail = 1";
        }

        public override void PostOpen(object objParameter)
        {
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            bool_FormLoading = false;
          
        }

        private void loadSettings()
        {

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);

            strFromAddress = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetFromAddress"));
            strFromAddressDisplay = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetFromAddressDisplay"));
            strP11DSubjectLine = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetP11DSubjectLine"));
            strP11DEmailList = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetP11DEmailList"));
            string strAppBase = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetP11DHTMLPath"));
            strP11DEmailHTMLLayoutFile = string.Format(@"{0}\{1}", strAppBase, Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetP11DHTMLLayoutFile")));
            strSMTPMailServerAddress = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerAddress"));
            strSMTPMailServerUsername = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerUsername"));
            strSMTPMailServerPassword = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerPassword"));
            strSMTPMailServerPort = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerPort"));
        }

        private void sendEmailList(SendEmailType sendEmailType, string strSelectedIDs)
        {
            bool errorOccured = false;
            string strSendEmailTo = strP11DEmailList;
            string strSendFullName = "P11D Administration Team";
            string strBody = "";
            bool boolSelectedForMail = false;
            try
            {
                //string test = Directory.GetCurrentDirectory() +@"\AssetCheck_HTML_Layout.htm";
                try
                {
                    strBody = File.ReadAllText(strP11DEmailHTMLLayoutFile);
                }
                catch (FileNotFoundException ex)
                {
                    XtraMessageBox.Show("An error occurred reading an HTML email file. Please contact administrator.", "Error Reading File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    errorOccured = true;
                    return;
                }

                string strP11DList = "No P11D records found.";

                //Fill dataset
                string filter = "";
                filter = "SelectedForEmail = 1";
                string strSummary = "<br/>P11D Summary: ";
                DataRow[] drTickedEmailList = this.dataSet_AS_DataEntry.sp_AS_11166_P11D_Item.Select(filter);
                //Loop through all records to send
                int progressTotal = drTickedEmailList.Length;
                int intCounter = 0;
                float increament = ((float)(1) / progressTotal) * 100;
                float progressPercentage = increament;
                frmProgress fProgress = new frmProgress(0);
                fProgress.UpdateCaption("Sending Emails...");
                fProgress.Show();

                if (progressTotal < 1)
                {
                    fProgress.UpdateProgress(100); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                
                int intSendCounter = 0;
                int intUpdatedCounter = 0;
                int intAddedCounter = 0;
                foreach (DataSet_AS_DataEntry.sp_AS_11166_P11D_ItemRow dataRowEmailList in drTickedEmailList)
                {
                    boolSelectedForMail = dataRowEmailList.SelectedForEmail;
                    //boolIsCurrentKeeper = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11166_P11D_ItemRow)(dataRowEmailList)).IsCurrentKeeper;
                    ArrayList arrSelectionArray = new ArrayList();
                    if (boolSelectedForMail)
                    {
                        switch (sendEmailType)
                        {
                            case SendEmailType.selected://email selected
                                string[] strSelectionArray = splitStrRecords(strSelectedIDs);
                               
                                foreach (string h in strSelectionArray)
                                {
                                    arrSelectionArray.Add(Regex.Replace(h, @"\s+", ""));
                                }
                                goto case SendEmailType.all;
                             
                             
                            case SendEmailType.all:
                                    bool blnToSend;
                                    if (SendEmailType.all == sendEmailType)
                                    {
                                        blnToSend = dataRowEmailList.SelectedForEmail;
                                    }
                                    else
                                    {
                                        if (arrSelectionArray.Contains(dataRowEmailList.P11DID.ToString()))
                                        {
                                            blnToSend = dataRowEmailList.SelectedForEmail;
                                        }
                                        else
                                        {
                                            blnToSend = false;
                                        }
                                    }
                                   
                                    if (blnToSend)
                                    {
                                        if (strP11DList == "No P11D records found.")
                                        {
                                            strP11DList = "";
                                        }
                                        //strP11DList += "<br/>" + getAppendedP11DRow(dataRowEmailList.P11DID);
                                        addRow(dataRowEmailList.P11DID);
                                       intSendCounter++;
                                       switch (dataRowEmailList.Notes.ToString())
                                       {
                                           case "New Keeper Record":
                                               intAddedCounter++;
                                               break;
                                           case "Modified Keeper Record":
                                               intUpdatedCounter++;
                                               break;
                                       }
                                    }
                                
                                break;
                        }
                    }
                    intCounter++;
                    float f = (((float)(intCounter) / (float)(progressTotal)) * 100);
                    if (f > progressPercentage)
                    {
                        if (fProgress != null)
                            fProgress.UpdateProgress(increament); // Update Progress Bar //
                        progressPercentage++;
                    }

                }
                
                string strUpdated = "";
                string strAdded = "";
                if (intSendCounter > 0)
                {
                    if (intAddedCounter > 0)
                    {
                        strAdded = "<br/>" + intAddedCounter + " New keeper record(s)";
                        strSummary += strAdded;
                    }
                    if (intUpdatedCounter > 0)
                    {
                        strUpdated = "<br/>" + intUpdatedCounter + " Modified keeper record(s)";
                        strSummary += strUpdated;
                    }
                }
                strP11DList += "<br/>" + ConvertDataTableToHTML(dt);  
                //send email
                strBody = "";
                strBody = File.ReadAllText(strP11DEmailHTMLLayoutFile);
                strBody = strBody.Replace("Summary", strSummary);
                strBody = strBody.Replace("FullName", strSendFullName);
                strBody = strBody.Replace("AssetList", strP11DList);

                sendEmail(strSendEmailTo, strSendFullName, strBody);
                if (fProgress != null)
                {
                    fProgress.UpdateProgress(10); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }

                if (strSelectedIDs == "")
                {
                    foreach (DataSet_AS_DataEntry.sp_AS_11166_P11D_ItemRow dataRowEmailList1 in drTickedEmailList)
                    {
                        strSelectedIDs += dataRowEmailList1.P11DID + ",";
                    }
                }
                if (blnSendOk)
                {
                    DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter updateRecord = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
                    updateRecord.ChangeConnectionString(strConnectionString);
                    updateRecord.sp_AS_11167_P11D_Update_Manager("blockedit", strSelectedIDs, 0, false);
                    LoadAdapters();
                }
                
            }
            catch (Exception ex)
            {
                //XtraMessageBox.Show("An error occurred trying to send email file. Please contact administrator.", "Error Sending Email", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // Get reference to the dialog type.
                var dialogTypeName = "System.Windows.Forms.PropertyGridInternal.GridErrorDlg";
                var dialogType = typeof(Form).Assembly.GetType(dialogTypeName);

                // Create dialog instance.
                var dialog = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());

                // Populate relevant properties on the dialog instance.
                dialog.Text = "Error Sending Email";
                dialogType.GetProperty("Details").SetValue(dialog, ex.Message, null);
                dialogType.GetProperty("Message").SetValue(dialog, "An error occurred trying to send email file. Please contact administrator.", null);

                // Display dialog.
                var result = dialog.ShowDialog();


                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                errorOccured = true;
                return;
            }
            finally
            {
                if (!errorOccured)
                {
                    XtraMessageBox.Show("Emails have been successfully sent.", "Emails Successfully Sent", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
          
        }

        private void addRow(int intP11DID)
        {
            object objAllocationEndDate = null;
            object objAllocationDate = null;
            object objRegistrationDate = null;
            object objTransactionDate = null;
            DataSet_AS_DataEntry.sp_AS_11166_P11D_ItemRow p11dDR = dataSet_AS_DataEntry.sp_AS_11166_P11D_Item.FindByP11DID(intP11DID);
            try
            {
                objAllocationEndDate = p11dDR.AllocationEndDate;
            }
            catch (StrongTypingException ex)
            { }
            try
            {
                objAllocationDate = p11dDR.AllocationDate;
            }
            catch (StrongTypingException ex)
            { }
            try
            {
                objRegistrationDate = p11dDR.RegistrationDate;
            }
            catch (StrongTypingException ex)
            { }
            try
            {
                objTransactionDate = p11dDR.TransactionDate;
            }
            catch (StrongTypingException ex)
            { }
                DataRow dtNewRow = dt.NewRow();
                dtNewRow[0] = p11dDR.EquipmentReference;
                dtNewRow[1] = p11dDR.ManufacturerID;
                if (objTransactionDate != null)
                {
                    dtNewRow[2] = p11dDR.TransactionDate.ToString("yyyy-MMM-dd");
                }
                else
                {
                    dtNewRow[2] = "";
                }
                
                dtNewRow[3] = p11dDR.KeeperType;
                dtNewRow[4] = p11dDR.KeeperFullName;
                if (objAllocationDate != null)
                {
                    dtNewRow[5] = p11dDR.AllocationDate.ToString("yyyy-MMM-dd");
                }
                else
                {
                    dtNewRow[5] = "";
                }

                if (objAllocationEndDate != null)
                {
                    dtNewRow[6] = p11dDR.AllocationEndDate.ToString("yyyy-MMM-dd");
                }
                else
                {
                    dtNewRow[6] = "";
                }
                dtNewRow[7] = p11dDR.AllocationStatus;
                dtNewRow[8] = String.Format("{0:C}", p11dDR.ListPrice);// "£" + p11dDR.ListPrice.ToString();
                dtNewRow[9] = String.Format("{0:C}", p11dDR.AdditionalCost);// "£" + p11dDR.AdditionalCost.ToString();
                dtNewRow[10] = p11dDR.Emissions;
                dtNewRow[11] = p11dDR.EngineSize;
                dtNewRow[12] = p11dDR.FuelType;
                dtNewRow[13] = p11dDR.RegistrationPlate;
                if (objRegistrationDate != null)
                {
                    dtNewRow[14] = p11dDR.RegistrationDate.ToString("yyyy-MMM-dd");
                }
                else
                {
                    dtNewRow[14] = "";
                }
                dtNewRow[15] = p11dDR.Make;
                dtNewRow[16] = p11dDR.Model;
                dtNewRow[17] = p11dDR.Notes;
                dt.Rows.Add(dtNewRow);
                     
        }

        public string ConvertDataTableToHTML(DataTable dt)
        {         
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html >");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");
            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<table border=1 style=background-color:lightgreen;border:1px dotted black;width:80%;border-collapse:collapse;>");

            strHTMLBuilder.Append("<tr style=background-color:orange;color:white;>");
            foreach (DataColumn myColumn in dt.Columns)
            {
                strHTMLBuilder.Append("<th style=padding:3px;>");
                strHTMLBuilder.Append(myColumn.ColumnName);
                strHTMLBuilder.Append("</th>");

            }
            strHTMLBuilder.Append("</tr>");


            foreach (DataRow myRow in dt.Rows)
            {

                strHTMLBuilder.Append("<tr >");
                foreach (DataColumn myColumn in dt.Columns)
                {
                    strHTMLBuilder.Append("<td nowrap=nowrap>");
                    strHTMLBuilder.Append(myRow[myColumn.ColumnName].ToString());
                    strHTMLBuilder.Append("</td>");

                }
                strHTMLBuilder.Append("</tr>");
            }

            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</body>");
            strHTMLBuilder.Append("</html>");

            string Htmltext = strHTMLBuilder.ToString();

            return Htmltext; 
        }

        private void createDatatable()
        {
            dt.Columns.Add("Equipment Ref");
            dt.Columns.Add("ManufacturerID");
            dt.Columns.Add("Transaction Date");
            dt.Columns.Add("Keeper Type");
            dt.Columns.Add("Keeper");
            dt.Columns.Add("Allocation Date");
            dt.Columns.Add("Allocation End Date");
            dt.Columns.Add("Allocation Status");
            dt.Columns.Add("List Price");
            dt.Columns.Add("Additional Cost");
            dt.Columns.Add("Emissions");
            dt.Columns.Add("Engine Size");
            dt.Columns.Add("Fuel Type");
            dt.Columns.Add("Registration Plate");
            dt.Columns.Add("Registration Date");
            dt.Columns.Add("Make");
            dt.Columns.Add("Model");
            dt.Columns.Add("Notes");

        }
        
        private void sendEmail(string strSendEmailTo, string strSendFullName, string strBody)
        {
            try
            {
                // Create a MailMessage object
                MailMessage mm = new MailMessage();

                // Define the sender and recipient
                mm.From = new MailAddress(strFromAddress, strFromAddressDisplay);
                mm.To.Add(new MailAddress(strSendEmailTo, strSendFullName));

                // Define the subject and body
                mm.Subject = strP11DSubjectLine;
                mm.Body = strBody;
                mm.IsBodyHtml = true;
                blnSendOk = true;
                // Configure the mail server
                SmtpClient sc = new SmtpClient(strSMTPMailServerAddress, Convert.ToInt32(strSMTPMailServerPort));
                sc.EnableSsl = false;
                if (!String.IsNullOrEmpty(strSMTPMailServerUsername)) 
                    sc.Credentials = new NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);

                // Send the message and notify the user of success
                sc.Send(mm);
                
              //  MessageBox.Show("Message sent successfully.","Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException)
            {
                MessageBox.Show("You must type from and to e-mail addresses", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                blnSendOk = false;
            }
            catch (FormatException)
            {
                MessageBox.Show("You must provide valid from and to e-mail addresses", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                blnSendOk = false;
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Please provide a server name.","No SMTP server provided", MessageBoxButtons.OK, MessageBoxIcon.Error);
                blnSendOk = false;
            }
            catch (SmtpFailedRecipientException)
            {
                MessageBox.Show("The mail server says that there is no mailbox for " + strSendEmailTo + ".", "Invalid recipient", MessageBoxButtons.OK, MessageBoxIcon.Error);
                blnSendOk = false;
            }
            catch (SmtpException ex)
            {
                // Invalid hostnames result in a WebException InnerException that provides
                // a more descriptive error, so get the base exception
                Exception inner = ex.GetBaseException();
                MessageBox.Show("Could not send message: " + inner.Message, "Problem sending message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                blnSendOk = false;
            }
        }

        private string[] splitStrRecords(string newIDs)
        {
            string[] parts;
            char[] delimiters = new char[] { ';', ',' };
            if (newIDs == "")
            {
                parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                parts = newIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }

            return parts;
        }
        
        #endregion



    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Classes.Assets;     //Equipment Email
using WoodPlan5.Properties;
using System.Collections.Specialized;
using DevExpress.XtraSplashScreen;
using WoodPlan5.verilocation;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Net;
using DevExpress.Spreadsheet;
using System.IO;


namespace WoodPlan5
{
    public partial class frm_AS_Fleet_Manager : BaseObjects.frmBase
    {

        private void frm_AS_Fleet_Manager_Load(object sender, EventArgs e)
        {         
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 1105;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            LoadConnectionStrings(this.GlobalSettings.ConnectionString);
            LoadAdapters();
            // Get Form Permissions //            
            ProcessPermissionsForForm();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            checkExchequerCategoryRestrictions();

            RefreshGridViewStateEquipment = new RefreshGridState(equipmentGridView, "EquipmentID");

            RefreshGridViewStateVehicle = new RefreshGridState(vehicleGridView, "EquipmentID");

            RefreshGridViewStatePlant = new RefreshGridState(plantGridView, "EquipmentID");

            RefreshGridViewStateRoadTax = new RefreshGridState(roadTaxGridView, "RoadTaxID");

            RefreshGridViewStateOffice = new RefreshGridState(officeGridView, "EquipmentID");

            //RefreshGridViewStateTracker = new RefreshGridState(trackerGridView, "TrackerInformationID");

            RefreshGridViewStateSoftware = new RefreshGridState(softwareGridView, "EquipmentID");

            RefreshGridViewStatePlant = new RefreshGridState(plantGridView, "EquipmentID");

            RefreshGridViewStateTransactions = new RefreshGridState(transactionsGridView, "TransactionID");

            RefreshGridViewStateKeeper = new RefreshGridState(keeperGridView, "KeeperAllocationID");

            RefreshGridViewStateBilling = new RefreshGridState(billingGridView, "EquipmentBillingID");

            RefreshGridViewStateDepreciation = new RefreshGridState(depreciationGridView, "DepreciationID");

            RefreshGridViewStateCover = new RefreshGridState(coverGridView, "CoverID");

            RefreshGridViewStateServiceData = new RefreshGridState(serviceDataGridView, "ServiceDataID");

            RefreshGridViewStateWork = new RefreshGridState(workDetailGridView, "WorkDetailID");

            RefreshGridViewStateIncident = new RefreshGridState(incidentGridView, "IncidentID");

            RefreshGridViewStatePurpose = new RefreshGridState(purposeGridView, "EquipmentPurposeID");

            RefreshGridViewStateFuelCard = new RefreshGridState(fuelCardGridView, "fuelCardID");

            //RefreshGridViewStateSpeeding = new RefreshGridState(speedingGridView, "SpeedingID");

            RefreshGridViewStateNotification = new RefreshGridState(notificationGridView, "NotificationID");

            RefreshGridViewStateP11D = new RefreshGridState(p11dGridView, "P11DID");

            RefreshGridViewStateLinkedDocs = new RefreshGridState(gridViewLinkedDocs, "LinkedDocumentID");

            // Get default layout file directory from System_Settings table //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strVerilocationUserName = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetVerilocationUserName"));
                strVerilocationPassword = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetVerilocationPassword"));            
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Location of the Mail Merge File Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Mail Merge Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }


            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            if (strPassedInRecordDs != "")  // Opened in drill-down mode //
            {
                //popupContainerEdit1.Text = "Custom Filter";
                Load_Data();  // Load records //
            }

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            emptyEditor = new RepositoryItem();
            connectToVerilocation();
        }

        #region Instance Variables...

        int intExchequerCategoryMVG = 0;
        private string strImportFile;
        private string strVerilocationUserName;
        private string strVerilocationPassword;
        private Boolean iboolDistinctMulitpleChildSelected = false;
        private int iDistinctSelectedType = 0;
        private Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public string strPassedInRecordDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //
        public string strRecordIDs = "";
        public string strRecordsToLoad = "";
        public int numOfSelectedRows = 0;
        //public int numOfSelectedTyreInfoRows = 0;
        public int numOfSelectedIncidentInfoRows = 0;
        private string gcRef = "";
        private string tempMake = "";
        private string tempModel = "";
        private string tempManufacturer = "";
        private string tempRegistration = "";
        private string strMessage1, strMessage2, strMessage3;
        private string strToken = "";
        internal Version_001 tsClient;
        int verilocationCounter1 = 0;

        private bool iBool_AllowDelete = false;
        private bool iBool_AllowAdd = false;
        private bool iBool_AllowEdit = false;

        private bool iBool_AllowAddVehicle = false;
        private bool iBool_AllowEditVehicle = false;
        private bool iBool_AllowDeleteVehicle = false;

        private bool iBool_AllowAddPlant = false;
        private bool iBool_AllowEditPlant = false;
        private bool iBool_AllowDeletePlant = false;

        private bool iBool_AllowAddRoadTax = false;
        private bool iBool_AllowEditRoadTax = false;
        private bool iBool_AllowDeleteRoadTax = false;
             
        //private bool iBool_AllowAddSpeeding = false;
        //private bool iBool_AllowEditSpeeding = false;
        //private bool iBool_AllowDeleteSpeeding = false;

        //private bool iBool_AllowAddTracker = false;
        //private bool iBool_AllowEditTracker = false;
        //private bool iBool_AllowDeleteTracker = false;

        private bool iBool_AllowAddSoftware = false;
        private bool iBool_AllowEditSoftware = false;
        private bool iBool_AllowDeleteSoftware = false;

        private bool iBool_AllowAddOffice = false;
        private bool iBool_AllowEditOffice = false;
        private bool iBool_AllowDeleteOffice = false;

        private bool iBool_AllowAddBilling = false;
        private bool iBool_AllowEditBilling = false;
        private bool iBool_AllowDeleteBilling = false;

        private bool iBool_AllowAddKeeper = false;
        private bool iBool_AllowEditKeeper = false;
        private bool iBool_AllowDeleteKeeper = false;

        private bool iBool_AllowAddTransactions = false;
        private bool iBool_AllowEditTransactions = false;
        private bool iBool_AllowDeleteTransactions = false;

        private bool iBool_AllowAddNotifications = false;
        private bool iBool_AllowEditNotifications = false;
        private bool iBool_AllowDeleteNotifications = false;
        
        private bool iBool_AllowAddDepreciation = false;
        private bool iBool_AllowEditDepreciation = false;
        private bool iBool_AllowDeleteDepreciation  = false;

        private bool iBool_AllowAddCover = false;
        private bool iBool_AllowEditCover = false;
        private bool iBool_AllowDeleteCover  = false;

        private bool iBool_AllowAddServiceData = false;
        private bool iBool_AllowEditServiceData = false;
        private bool iBool_AllowDeleteServiceData  = false;
        
        private bool iBool_AllowAddWorkDetail = false;
        private bool iBool_AllowEditWorkDetail = false;
        private bool iBool_AllowDeleteWorkDetail  = false;

        private bool iBool_AllowAddIncident = false;
        private bool iBool_AllowEditIncident = false;
        private bool iBool_AllowDeleteIncident  = false;

        private bool iBool_AllowAddPurpose = false;
        private bool iBool_AllowEditPurpose = false;
        private bool iBool_AllowDeletePurpose  = false;
        
        private bool iBool_AllowAddFuelCard = false;
        private bool iBool_AllowEditFuelCard = false;
        private bool iBool_AllowDeleteFuelCard = false;

        private bool iBool_AllowAddP11D = false;
        private bool iBool_AllowEditP11D = false;
        private bool iBool_AllowDeleteP11D = false;
        
        private bool iBool_AllowAddNetBookValue = false;
        private bool iBool_AllowEditNetBookValue = false;
        private bool iBool_AllowDeleteNetBookValue = false;

        private bool iBool_AllowReadAllExchequerCategories = false;
        private bool iBool_AllowAddAllExchequerCategories = false;
        private bool iBool_AllowEditAllExchequerCategories = false;
        private bool iBool_AllowDeleteAllExchequerCategories = false;
        int i_int_FocusedGrid = 0;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        
        
        // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateEquipment; 
        public RefreshGridState RefreshGridViewStateVehicle;  
        public RefreshGridState RefreshGridViewStatePlant;
        public RefreshGridState RefreshGridViewStateRoadTax;
        //public RefreshGridState RefreshGridViewStateTracker;
        public RefreshGridState RefreshGridViewStateSoftware;
        public RefreshGridState RefreshGridViewStateOffice;
        public RefreshGridState RefreshGridViewStateKeeper;
        public RefreshGridState RefreshGridViewStateTransactions;
        public RefreshGridState RefreshGridViewStateBilling;
        public RefreshGridState RefreshGridViewStateDepreciation;
        public RefreshGridState RefreshGridViewStateCover;
        public RefreshGridState RefreshGridViewStateServiceData;
        public RefreshGridState RefreshGridViewStateWork;
        public RefreshGridState RefreshGridViewStateIncident;
        public RefreshGridState RefreshGridViewStatePurpose;
        public RefreshGridState RefreshGridViewStateFuelCard;
        //public RefreshGridState RefreshGridViewStateSpeeding;
        public RefreshGridState RefreshGridViewStateNotification;
        public RefreshGridState RefreshGridViewStateP11D;
        public RefreshGridState RefreshGridViewStateLinkedDocs;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        private string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        private enum FormType { child, parent };
        private enum GridType {child, parent };
        public enum FormMode { add, edit, view, delete, blockadd, blockedit };
        private enum EquipmentType { None, Vehicle = 1, Plant = 2, Gadget = 3, Hardware = 4, Software = 5, Office = 6}
        private enum FileExtension { Excel2010Xlsx,Excel2003Xls }

         #endregion

        #region Constructor and compulsory implementation

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            getCurrentView(out view);

            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            toggleMenuButtons(alItems,intRowHandles);

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of equipmentView navigator custom buttons //
            getCRUDButtonPermissions(equipmentGridControl, iBool_AllowAdd, iBool_AllowEdit, iBool_AllowDelete);

            // Set enabled status of plant GridControl navigator custom buttons //
            getCRUDButtonPermissions(plantGridControl, iBool_AllowAddPlant, iBool_AllowEditPlant, iBool_AllowDeletePlant);

            // Set enabled status of vehicle GridControl navigator custom buttons //
            getCRUDButtonPermissions(vehicleGridControl, iBool_AllowAddVehicle, iBool_AllowEditVehicle, iBool_AllowDeleteVehicle);

            // Set enabled status of RoadTax GridControl navigator custom buttons //
            getCRUDButtonPermissions(roadTaxGridControl, iBool_AllowAddRoadTax, iBool_AllowEditRoadTax, iBool_AllowDeleteRoadTax);

            //// Set enabled status of Tracker GridControl navigator custom buttons //
            //getCRUDButtonPermissions(trackerGridControl, iBool_AllowAddTracker, iBool_AllowEditTracker, iBool_AllowDeleteTracker);

            // Set enabled status of software GridControl navigator custom buttons //
            getCRUDButtonPermissions(softwareGridControl, iBool_AllowAddSoftware, iBool_AllowEditSoftware, iBool_AllowDeleteSoftware);

            // Set enabled status of office GridControl navigator custom buttons //
            getCRUDButtonPermissions(officeGridControl, iBool_AllowAddOffice, iBool_AllowEditOffice, iBool_AllowDeleteOffice);

            // Set enabled status of keeper GridControl navigator custom buttons //
            getCRUDButtonPermissions(keeperGridControl, iBool_AllowAddKeeper, iBool_AllowEditKeeper, iBool_AllowDeleteKeeper);

            // Set enabled status of transaction GridControl navigator custom buttons //
            getCRUDButtonPermissions(transactionGridControl, iBool_AllowAddTransactions, iBool_AllowEditTransactions, iBool_AllowDeleteTransactions);

            // Set enabled status of billing GridControl navigator custom buttons //
            getCRUDButtonPermissions(billingGridControl, iBool_AllowAddBilling, iBool_AllowEditBilling, iBool_AllowDeleteBilling);

            // Set enabled status of depreciation GridControl navigator custom buttons //
            getCRUDButtonPermissions(depreciationGridControl, iBool_AllowAddDepreciation, iBool_AllowEditDepreciation, iBool_AllowDeleteDepreciation);

            // Set enabled status of cover GridControl navigator custom buttons //
            getCRUDButtonPermissions(coverGridControl, iBool_AllowAddCover, iBool_AllowEditCover, iBool_AllowDeleteCover);

            // Set enabled status of ServiceData GridControl navigator custom buttons //
            getCRUDButtonPermissions(serviceDataGridControl, iBool_AllowAddServiceData, iBool_AllowEditServiceData, iBool_AllowDeleteServiceData);

            // Set enabled status of work GridControl navigator custom buttons //
            getCRUDButtonPermissions(workDetailGridControl, iBool_AllowAddWorkDetail, iBool_AllowEditWorkDetail, iBool_AllowDeleteWorkDetail);

            // Set enabled status of incident GridControl navigator custom buttons //
            getCRUDButtonPermissions(incidentGridControl, iBool_AllowAddIncident, iBool_AllowEditIncident, iBool_AllowDeleteIncident);

            // Set enabled status of purpose GridControl navigator custom buttons //
            getCRUDButtonPermissions(purposeGridControl, iBool_AllowAddPurpose, iBool_AllowEditPurpose, iBool_AllowDeletePurpose);

            // Set enabled status of Fuel Cards GridControl navigator custom buttons //
            getCRUDButtonPermissions(fuelCardGridControl, iBool_AllowAddFuelCard, iBool_AllowEditFuelCard, iBool_AllowDeleteFuelCard);

            //// Set enabled status of Speeding GridControl navigator custom buttons //
            //getCRUDButtonPermissions(speedingGridControl, iBool_AllowAddSpeeding, iBool_AllowEditSpeeding, iBool_AllowDeleteSpeeding);

            // Set enabled status of Installation GridControl navigator custom buttons //
            getCRUDButtonPermissions(gridControlLinkedDocs, true, true, true);
        }

        private void toggleMenuButtons(ArrayList alItems, int[] intRowHandles)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://equipment
                                   
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                    bbiBlockAdd.Enabled = false;
                }
                
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    //if (intRowHandles.Length >= 2)
                    //{
                    if (intRowHandles.Length >= 2 && !iboolDistinctMulitpleChildSelected)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                    else
                    {
                        bbiBlockEdit.Enabled = false;
                    }
                    //}
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                break;
                case 1://vehicle
                getChildMouseMenuPermissions(alItems,intRowHandles, iBool_AllowAddVehicle, iBool_AllowEditVehicle, iBool_AllowDeleteVehicle);
                break;
                case 2://plant
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddPlant, iBool_AllowEditPlant, iBool_AllowDeletePlant);
                break;
                case 3://RoadTax
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddRoadTax, iBool_AllowEditRoadTax, iBool_AllowDeleteRoadTax);
                break;
                //case 4://Tracker
                //getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddTracker, iBool_AllowEditTracker, iBool_AllowDeleteTracker);
                //break;
                case 5://software                    
                getChildMouseMenuPermissions(alItems,intRowHandles, iBool_AllowAddSoftware, iBool_AllowEditSoftware, iBool_AllowDeleteSoftware);
                break;
                case 6://office                    
                getChildMouseMenuPermissions(alItems,intRowHandles, iBool_AllowAddOffice, iBool_AllowEditOffice, iBool_AllowDeleteOffice);
                break;
                case 7://billing 
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddBilling, iBool_AllowEditBilling, iBool_AllowDeleteBilling);
                break;
                case 8://keeper   
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddKeeper, iBool_AllowEditKeeper, iBool_AllowDeleteKeeper);
                break;
                case 9://transaction                
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddTransactions, iBool_AllowEditTransactions, iBool_AllowDeleteTransactions);
                break;
                case 10://notification                
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddNotifications, iBool_AllowEditNotifications, iBool_AllowDeleteNotifications);
                break;
                case 11://depreciation      
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddDepreciation, iBool_AllowEditDepreciation, iBool_AllowDeleteDepreciation);
                break;
                case 12://Incident      
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddIncident, iBool_AllowEditIncident, iBool_AllowDeleteIncident);
                break;
                case 13://ServiceData      
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddServiceData, iBool_AllowEditServiceData, iBool_AllowDeleteServiceData);
                break;
                case 14://Cover      
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddCover, iBool_AllowEditCover, iBool_AllowDeleteCover);
                break;
                case 15://Work      
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddWorkDetail, iBool_AllowEditWorkDetail, iBool_AllowDeleteWorkDetail);
                break;
                case 16://Purpose      
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddPurpose, iBool_AllowEditPurpose, iBool_AllowDeletePurpose);
                break;
                case 17://Fuel Card     
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddFuelCard, iBool_AllowEditFuelCard, iBool_AllowDeleteFuelCard);
                break;
                case 19://P11D
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddP11D, iBool_AllowEditP11D, iBool_AllowDeleteP11D);
                break;                    
                case 20://Net Book Value
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddNetBookValue, iBool_AllowEditNetBookValue, iBool_AllowDeleteNetBookValue);
                break;
                case 99://Linked Documents
                getChildMouseMenuPermissions(alItems, intRowHandles, true, true, true);
                break;
            }
        }

        private void getCRUDButtonPermissions(GridControl GridControlX,bool iBool_AllowAddX, bool iBool_AllowEditX, bool iBool_AllowDeleteX)
        {
            GridControl xGridControl = null;
            xGridControl = GridControlX;
            GridView view = null;
            view = (GridView)xGridControl.MainView;
            int[] intRowHandles = view.GetSelectedRows();

            if (iBool_AllowAddX)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEditX)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDeleteX)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

        }
        
        private void getChildMouseMenuPermissions(ArrayList alItems, int[] intRowHandles, bool iBool_AllowAddChild, bool iBool_AllowEditChild, bool iBool_AllowDeleteChild)
        {
            if (iBool_AllowAddChild)
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                bbiBlockAdd.Enabled = false;
                if (i_int_FocusedGrid == 9 || i_int_FocusedGrid == 99) //Transactions or Linked Documents //
                {
                    GridView viewParent = (GridView)equipmentGridControl.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2 && !iboolDistinctMulitpleChildSelected)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                if (i_int_FocusedGrid == 7 )//|| i_int_FocusedGrid == 3)//Billing or Tyre Information
                {
                    bbiSingleAdd.Enabled = false;
                    bbiBlockAdd.Enabled = false;
                }
            }
            if (iBool_AllowEditChild && intRowHandles.Length >= 1)
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
                if (intRowHandles.Length >= 2 && !iboolDistinctMulitpleChildSelected && (i_int_FocusedGrid != 9 && i_int_FocusedGrid != 8 && i_int_FocusedGrid != 11 && i_int_FocusedGrid != 15 && i_int_FocusedGrid != 17))//suppress transaction, Keeper , depreciation,work,Fuel Card 
                {
                    alItems.Add("iBlockEdit");
                    bbiBlockEdit.Enabled = true;
                }
                if (intRowHandles.Length >= 2 && (i_int_FocusedGrid == 16 || i_int_FocusedGrid == 12))//allow asset purpose and incident 
                {
                    alItems.Add("iBlockEdit");
                    bbiBlockEdit.Enabled = true;
                }

                if (i_int_FocusedGrid == 8)// Keeper 
                {
                    alItems.Add("iBlockEdit");
                    bbiBlockEdit.Enabled = false;
                }
                //if (i_int_FocusedGrid == 7)//billing 
                //{
                //    alItems.Add("iBlockEdit");
                //    bbiBlockEdit.Enabled = true;
                //}
            }
            if (iBool_AllowDeleteChild && intRowHandles.Length >= 1)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
            }
            if (iBool_AllowDeleteChild && i_int_FocusedGrid == 7 )
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = false;
            }
        }
 
        private void getCurrentView(out GridView view)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://equipment
                    view = (GridView)equipmentGridControl.MainView;
                    break;
                case 1://vehicle
                    view = (GridView)vehicleGridControl.MainView;
                    break;
                case 2://plant
                    view = (GridView)plantGridControl.MainView;
                    break;
                case 3://RoadTax
                    view = (GridView)roadTaxGridControl.MainView;
                    break;
                //case 4://Tracker
                //    view = (GridView)trackerGridControl.MainView;
                //    break;
                case 5://software
                    view = (GridView)softwareGridControl.MainView;
                    break;
                case 6://office
                    view = (GridView)officeGridControl.MainView;
                    break;
                case 7://billing 
                    view = (GridView)billingGridControl.MainView;
                    break;
                case 8://keeper
                    view = (GridView)keeperGridControl.MainView;
                    break;
                case 9://transaction
                    view = (GridView)transactionGridControl.MainView;
                    break;
                case 10:  // Notifications //
                    view = (GridView)notificationGridControl.MainView;
                    break;
                case 11:  // Depreciation // 
                    view = (GridView)depreciationGridControl.MainView;
                    break;
                case 12:  // Incident //
                    view = (GridView)incidentGridControl.MainView;
                    break;
                case 13:  // ServiceData //
                    view = (GridView)serviceDataGridControl.MainView;
                    break;
                case 14:  // Cover //
                    view = (GridView)coverGridControl.MainView;
                    break;
                case 15:  // Work //
                    view = (GridView)workDetailGridControl.MainView;
                    break;
                case 16:  // Purpose // 
                    view = (GridView)purposeGridControl.MainView;                            
                    break;
                case 17:  // FuelCard //
                    view = (GridView)fuelCardGridControl.MainView;
                    break;
                //case 18:  // Speeding //
                //    view = (GridView)speedingGridControl.MainView;
                //    break;
                case 19:  // P11D //
                    view = (GridView)p11dGridControl.MainView;
                    break;
                case 99:  // Linked Documents //
                    view = (GridView)gridControlLinkedDocs.MainView;
                    break;
                default:
                    view = (GridView)billingGridControl.MainView;
                    break;
            }
     
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Vehicle //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddVehicle = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditVehicle = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteVehicle = true;
                        }
                        break;
                    case 2:  // Plant //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddPlant = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditPlant = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeletePlant = true;
                        }
                        break;
                    case 3:  // RoadTax //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddRoadTax = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditRoadTax = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteRoadTax = true;
                        }
                        break;
                    //case 4:  // Tracker //
                    //    if (sfpPermissions.blCreate)
                    //    {
                    //        iBool_AllowAddTracker = true;
                    //    }
                    //    if (sfpPermissions.blUpdate)
                    //    {
                    //        iBool_AllowEditTracker = true;
                    //    }
                    //    if (sfpPermissions.blDelete)
                    //    {
                    //        iBool_AllowDeleteTracker = true;
                    //    }
                    //    break;
                    case 5:  // Software //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddSoftware = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditSoftware = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteSoftware = true;
                        }
                        break;
                    case 6:  // Office //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddOffice = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditOffice = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteOffice = true;
                        }
                        break;
                    case 7:  // Billings //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddBilling = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditBilling = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteBilling = true;
                        }
                        break;
                    case 8:  // Keeper //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddKeeper = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditKeeper = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteKeeper = true;
                        }
                        break;
                    case 9:  // Transactions //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddTransactions = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditTransactions = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteTransactions = true;
                        }
                        break;
                    case 10:  // Notifications //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddNotifications = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditNotifications = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteNotifications = true;
                        }
                        break;
                    case 11:  // Depreciation //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddDepreciation = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditDepreciation = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteDepreciation = true;
                        }
                        break;
                    case 12:  // Incident //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddIncident = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditIncident = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteIncident = true;
                        }
                        break;
                    case 13:  // ServiceData //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddServiceData = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditServiceData = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteServiceData = true;
                        }
                        break;
                    case 14:  // Cover //13:  // ServiceData //12:  // Incident // 11:  // Depreciation //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddCover = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditCover = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteCover = true;
                        }
                        break;
                    case 15:  // Work //14:  // Cover //13:  // ServiceData //12:  // Incident // 11:  // Depreciation //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddWorkDetail = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditWorkDetail = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteWorkDetail = true;
                        }
                        break;
                    case 16:  // Purpose //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddPurpose = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditPurpose = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeletePurpose = true;
                        }
                        break;
                    case 17:  // FuelCard //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddFuelCard = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditFuelCard = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteFuelCard = true;
                        }
                        break;
                    //case 18:  // Speeding //
                    //    if (sfpPermissions.blCreate)
                    //    {
                    //        iBool_AllowAddSpeeding = true;
                    //    }
                    //    if (sfpPermissions.blUpdate)
                    //    {
                    //        iBool_AllowEditSpeeding = true;
                    //    }
                    //    if (sfpPermissions.blDelete)
                    //    {
                    //        iBool_AllowDeleteSpeeding = true;
                    //    }
                    //    break;
                    case 19:  // P11D //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddP11D = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditP11D = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteP11D = true;
                        }
                        break;

                    case 20:  // Net Book Value //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddNetBookValue = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditNetBookValue = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteNetBookValue = true;
                        }
                        break;
                    case 21:  // Access To All Exchequer Categories  //
                        if (sfpPermissions.blRead)
                        {
                            iBool_AllowReadAllExchequerCategories = true;
                        }
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddAllExchequerCategories = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditAllExchequerCategories = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteAllExchequerCategories = true;
                        }
                        break;
                }
            }
        }

        public frm_AS_Fleet_Manager()
        {
            InitializeComponent();
        }

        #endregion

        #region Form Events

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

        private void bbiMOTDue_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenNotificationManagerViaMain();
        }


        private void OpenNotificationManagerViaMain()
        {
            // Open Notification Manager via main //
            if (Application.OpenForms["frmMain2"] != null)
            {
                (Application.OpenForms["frmMain2"] as frmMain2).Open_Screen(1111);
            }

            string strNotificationFilter = "[NotificationType] = 'MOT' and [Status]  = 'Open'";//MOT
            DataRow[] rows = this.dataSet_AS_DataEntry.sp_AS_11126_Notification_Alert.Select(strNotificationFilter);
            int intNotificationCount = rows.Length;
            string strToLoad = "";
            foreach (DataRow dr in rows)
            {
                strToLoad += ((DataSet_AS_DataEntry.sp_AS_11126_Notification_AlertRow)(dr)).NotificationID.ToString() + ";";
            }

            // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
            if (Application.OpenForms["frm_AS_Notification_Manager"] != null)
            {
                (Application.OpenForms["frm_AS_Notification_Manager"] as frm_AS_Notification_Manager).UpdateFormRefreshStatus(1, strToLoad, "", "");
                (Application.OpenForms["frm_AS_Notification_Manager"] as frm_AS_Notification_Manager).frmActivated();
            }
        }
        private void acNotifications_AlertClick(object sender, DevExpress.XtraBars.Alerter.AlertClickEventArgs e)
        {
            OpenNotificationManagerViaMain();
        }


        private void bbiUpdateMOT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter updateMOT = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            updateMOT.ChangeConnectionString(strConnectionString);
            updateMOT.sp_AS_11147_Update_MOT_Notifications("edit");
            XtraMessageBox.Show("MOT notification details updated", "Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            loadAlertControl();
        }

        private void bbiNewMOT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter createMOT = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            createMOT.ChangeConnectionString(strConnectionString);
            createMOT.sp_AS_11147_Update_MOT_Notifications("add");
            XtraMessageBox.Show("MOT notification details updated", "Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            loadAlertControl();
        }

       

        private void bbiSpeeding_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            addSpeedData();
        }


        private void commonGridView_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    if (i_int_FocusedGrid != 18)
                    {
                        Edit_Record();
                    }
            }
        }

        private void commonGridView_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            string GridName = ((GridView)sender).Name;
            switch (GridName)
            {
                case "equipmentGridView":
                    i_int_FocusedGrid = 0;
                    break;

                case "vehicleGridView":
                    i_int_FocusedGrid = 1;
                    break;

                case "plantGridView":
                    i_int_FocusedGrid = 2;
                    break;

                case "roadTaxGridView":
                    i_int_FocusedGrid = 3;
                    break;

                case "trackerGridView":
                    i_int_FocusedGrid = 4;
                    break;

                case "softwareGridView":
                    i_int_FocusedGrid = 5;
                    break;

                case "officeGridView":
                    i_int_FocusedGrid = 6;
                    break;

                case "billingGridView":
                    i_int_FocusedGrid = 7;
                    break;

                case "keeperGridView":
                    i_int_FocusedGrid = 8;
                    break;

                case "transactionsGridView":
                    i_int_FocusedGrid = 9;
                    break;

                case "notificationGridView":
                    i_int_FocusedGrid = 10;
                    break;

                case "depreciationGridView":
                    i_int_FocusedGrid = 11;
                    break;
                case "incidentGridView":
                    i_int_FocusedGrid = 12;
                    break;
                case "serviceDataGridView": 
                    i_int_FocusedGrid = 13;
                    break;
                case "coverGridView":
                    i_int_FocusedGrid = 14;
                    break;
                case "workDetailGridView": 
                    i_int_FocusedGrid = 15;
                    break;
                case "purposeGridView":
                    i_int_FocusedGrid = 16;
                    break;
                case "fuelCardGridView":
                    i_int_FocusedGrid = 17;
                    break;
                case "speedingGridView":
                    i_int_FocusedGrid = 18;
                    break;
                case "p11dGridView":
                    i_int_FocusedGrid = 19;
                    break;
            }

            SetMenuStatus();
        }

        private void customFilterDraw(GridView view, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(view, e);
        }

        private void commonGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("insert".Equals(e.Button.Tag))
                    {
                        Insert_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                   
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        Linked_Documents_To_Record();
                    }
                    
                    break;
                default:
                    break;
            }
        }

        private void commonGridView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void childGridViewSelectionChanged(GridView view, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(view, e, ref isRunning);
            SetMenuStatus();

            if (i_int_FocusedGrid == 12)//Incident
            {
                GridView childView = incidentGridView;
                numOfSelectedIncidentInfoRows = childView.SelectedRowsCount;
                int tempNumOfSelectedRows;
                if (numOfSelectedIncidentInfoRows > 0)
                {
                    tempNumOfSelectedRows = numOfSelectedIncidentInfoRows;
                }
                else
                {
                    tempNumOfSelectedRows = 1;
                }

                int[] IDs = new int[tempNumOfSelectedRows];
                int[] intRowHandles = childView.GetSelectedRows();

                if (numOfSelectedIncidentInfoRows > 0)
                {
                    int countRows = 0;

                    foreach (int intRowHandle in intRowHandles)
                    {
                        DataRow dr = childView.GetDataRow(intRowHandle);
                        IDs[countRows] = (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)(dr)).IncidentID);
                        countRows += 1;
                    }
                }
                else
                {
                    IDs[0] = 0;
                }
                //load linked incident losses records
                // sp_AS_11081_Incident_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11081_Incident_Item, stringRecords(IDs), "view");
            }
        }

        private void LoadAdapters()
        {
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            sp_AS_11053_Fleet_ManagerTableAdapter.Fill(dataSet_AS_Core.sp_AS_11053_Fleet_Manager, strPassedInRecordDs, "view");
                     
        }
        private void checkExchequerCategoryRestrictions()
        {
            intExchequerCategoryMVG = 0;
            if (iBool_AllowReadAllExchequerCategories)
            {
                spAS11053FleetManagerBindingSource.Filter = "";
            }
            else
            {
                intExchequerCategoryMVG = getExchequerCategory("MVG");
                spAS11053FleetManagerBindingSource.Filter = String.Format("ExchequerCategoryID in ({0})", intExchequerCategoryMVG);
            }
        }
        private int getExchequerCategory(string strCategory)
        {
            int intExchequerCategory = 0;
            sp_AS_11000_PL_Exchequer_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_Category, 54); //Exchequer Category
            DataRow[] drExCats = this.dataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_Category.Select("[value] like '" + strCategory + "'");
            if (drExCats.Length > 0)
            {
                foreach (DataRow drExCat in drExCats)
                {
                    intExchequerCategory = ((DataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_CategoryRow)(drExCat)).PickListID;
                }
            }
            return intExchequerCategory;
        }

        private void LoadConnectionStrings(string ConnString)
        {
            strConnectionString = ConnString;
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;            
            sp_AS_11053_Fleet_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11002_Equipment_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11005_Vehicle_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11008_Plant_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11029_Office_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            //sp_AS_11023_Tracker_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11035_Software_ItemTableAdapter.Connection.ConnectionString = strConnectionString;                                                                                 
            sp_AS_11041_Transaction_ItemTableAdapter.Connection.ConnectionString =strConnectionString;
            sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11047_Equipment_Billing_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11054_Service_Data_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11050_Depreciation_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11075_Cover_ItemTableAdapter.Connection.ConnectionString = strConnectionString;                                                                                                                                      sp_AS_11078_Work_Detail_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11081_Incident_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11084_Purpose_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11089_Fuel_Card_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11096_Road_Tax_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            //sp_AS_11105_Speeding_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11023_Verilocation_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11126_Notification_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11169_NetBookValue_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11166_P11D_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11053_Fleet_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
        }

        private void frm_AS_Fleet_Manager_Activated(object sender, EventArgs e)
        {
            frmActivated();
        }


        #endregion

        #region Form Functions

        internal void loadAlertControl()
        {

            sp_AS_11126_Notification_AlertTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11126_Notification_Alert, "", "view");
            string strNotificationFilter = "[NotificationType] = 'MOT' and [Status]  = 'Open'";//MOT
            DataRow[] rows = this.dataSet_AS_DataEntry.sp_AS_11126_Notification_Alert.Select(strNotificationFilter);
            int intNotificationCount = rows.Length;
            if (intNotificationCount > 0)
            {
                bbiMOTDue.EditValue = intNotificationCount + " MOT Record(s) Due.";
                bbiMOTDue.Enabled = true;
            }
            else
            {
                bbiMOTDue.EditValue = "No MOT Records Due.";
                bbiMOTDue.Enabled = false;
            }
            if (intNotificationCount > 0)
            {
                tiNotifications.BalloonTipIcon = ToolTipIcon.Info;
                tiNotifications.Text = intNotificationCount.ToString() + " MOT Notifications";
                //open_MOT_Form();
                if (intNotificationCount < 5)
                {
                    foreach (DataRow dr in rows)
                    {
                        string vehicle = ((DataSet_AS_DataEntry.sp_AS_11126_Notification_AlertRow)(dr)).EquipmentReference;
                        string priorityText = ((DataSet_AS_DataEntry.sp_AS_11126_Notification_AlertRow)(dr)).Priority;
                        string strMessage = ((DataSet_AS_DataEntry.sp_AS_11126_Notification_AlertRow)(dr)).Message;
                        acNotifications.Show(this, "MOT Alert", vehicle + " has a " + priorityText + " priority! " + strMessage);
                    }
                }
                else
                {
                    acNotifications.Show(this, "Multiple vehicle MOT Notifications", "There are " + intNotificationCount + " vehicles due for MOT.");
                }
            }
            else
            {
                tiNotifications.BalloonTipIcon = ToolTipIcon.Info;
                tiNotifications.Text = "No MOT Notifications";
            }

        }


        private void bbiUpdateSelectedEquipment_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bbiUpdateSelectedEquipment.Enabled = false;
            updateSelectedExistingVehicles();
            bbiUpdateSelectedEquipment.Enabled = true;
        }

        private void bbiSearchTracker_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            XtraMessageBox.Show("The Verilocation Service has been deprecated. Please contact the administrator for further assistance.", "Verilocation Service Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Information);
            // bbiSearchTracker.Enabled = false;
           // bbiAutoMatchVerilocation.Enabled = false;
           // bbiUpdateTracker.Enabled = false;
           // addNewVehicles();
           // matchRecordsVerilocation(false);
           // bbiUpdateTracker.Enabled = true;
           // bbiAutoMatchVerilocation.Enabled = true;
           // bbiSearchTracker.Enabled = true;
           // Load_Data(); 
        }

        private void bbiUpdateTracker_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bbiSearchTracker.Enabled = false;
            bbiAutoMatchVerilocation.Enabled = false;
            bbiUpdateTracker.Enabled = false;
            updateExistingVehicles();
            bbiUpdateTracker.Enabled = true;
            bbiAutoMatchVerilocation.Enabled = true;
            bbiSearchTracker.Enabled = true;
        }

        private void bbiAutoMatchVerilocation_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bbiAutoMatchVerilocation.Enabled = false; 
            XtraMessageBox.Show("The Verilocation Service has been deprecated. Please contact the administrator for further assistance.", "Verilocation Service Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Information);           
            //matchRecordsVerilocation(true);
            bbiAutoMatchVerilocation.Enabled = true;
        }

        private void connectToVerilocation()
        {
            try
            {
                this.sp_AS_11023_Verilocation_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11023_Verilocation_Item, "", "");
                tsClient = new Version_001();
                strToken = tsClient.Logon(strVerilocationUserName, strVerilocationPassword);
            }
            catch (WebException ex)
            {
                XtraMessageBox.Show("Cannot find internet connection or user credentials are incorrect.","Verilocation Connection failure",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void addNewVehicles()
        {
            XtraMessageBox.Show("The Verilocation Service has been deprecated. Please contact the administrator for further assistance.", "Verilocation Service Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //connectToVerilocation();
            //verilocationCounter1 = 0;
            //wscGpsUnitExt[] unitList = tsClient.VehiclesExt(strToken);
            //NameValueCollection driver = getDriverDetails();
            //NameValueCollection location = getLocationDetails();

            //int progressTotal = unitList.Length;

            //float increament = ((float)(1) / progressTotal) * 100;
            //float progressPercentage = increament;
            //frmProgress fProgress = new frmProgress(0);
            //fProgress.UpdateCaption("Adding Trackers...");
            //fProgress.Show();

            //if (progressTotal <1)
            //{
            //    fProgress.UpdateProgress(100); // Update Progress Bar //
            //    fProgress.Close();
            //    fProgress = null;
            //}
            ////Fill table
            //sp_AS_11023_Verilocation_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11023_Verilocation_Item, "", "");

            //foreach (wscGpsUnitExt t in unitList)
            //{
            //    //check if exists
            //    string filter = "";
            //    filter = "[TrackerReference] = '" + t.Reg + "' AND " + "[TrackerVID] = '" + t.Vid + "'";
            //    DataRow[] drs = this.dataSet_AS_DataEntry.sp_AS_11023_Verilocation_Item.Select(filter);
                
            //    if (drs.Length < 1)
            //    {
                    
            //        string[] driverArray = splitRecords(driver[t.Vid.ToString() + ";"]);
            //        string[] locationArray = splitRecords(location[t.Vid.ToString() + ";"]);

            //        if (locationArray != null && locationArray.Length == 10)
            //        {
            //            decimal journeyAVGSpeed = 0;
            //            string strLatestStartJourney = "Unknown";
            //            string strLatestEndJourney = "Unknown";
            //            string name = "Unknown";
            //            string mobile = "Unknown";
            //            string email = "Unknown";

            //            //get jouney details
            //            NameValueCollection journey = getLatestJourneyDetails(t.Vid.ToString());
            //            string[] journeyArray = splitRecords(journey["1;"]);
            //            if (journeyArray != null && journeyArray.Length > 4)
            //            {
            //                journeyAVGSpeed = Convert.ToDecimal((journeyArray[1].ToString()).TrimStart(','));
            //                strLatestStartJourney = (journeyArray[2].ToString()).TrimStart(',');
            //                strLatestEndJourney = (journeyArray[3].ToString()).TrimStart(',');
            //            }

            //            //get driver details
            //            if (driverArray != null && driverArray.Length == 4)
            //            {
            //                name = (driverArray[0].ToString() == "" ? "None" : (driverArray[0].ToString())).TrimStart(',');
            //                mobile = (driverArray[1].ToString() == "" ? "None" : (driverArray[1].ToString())).TrimStart(',');
            //                email = (driverArray[2].ToString() == "" ? "None" : (driverArray[2].ToString())).TrimStart(',');
            //            }

            //            //get location details
            //            int odometer = (Convert.ToInt32((locationArray[0].ToString()).TrimStart(',')) == null ? 0 : Convert.ToInt32((locationArray[0].ToString()).TrimStart(',')));//0
            //            string reason = ((locationArray[1].ToString()).TrimStart(',') == "" ? "None" : (locationArray[1].ToString()).TrimStart(','));//1
            //            decimal latitude = (Convert.ToDecimal((locationArray[2].ToString()).TrimStart(',')) == null ? 0 : Convert.ToDecimal((locationArray[2].ToString()).TrimStart(',')));//2
            //            decimal longitude = (Convert.ToDecimal((locationArray[3].ToString()).TrimStart(',')) == null ? 0 : Convert.ToDecimal((locationArray[3].ToString()).TrimStart(',')));//3
            //            int speedKPH = (Convert.ToInt32((locationArray[4].ToString()).TrimStart(',')) == null ? 0 : Convert.ToInt32((locationArray[4].ToString()).TrimStart(',')));//4
            //            string placename = ((locationArray[5].ToString()).TrimStart(',') == "" ? "None" : (locationArray[5].ToString()).TrimStart(','));//5
            //            string roadname = ((locationArray[6].ToString()).TrimStart(',') == "" ? "None" : (locationArray[6].ToString()).TrimStart(','));//6
            //            string postsector = ((locationArray[7].ToString()).TrimStart(',') == "" ? "None" : (locationArray[7].ToString()).TrimStart(','));//7
            //            string geofenceName = ((locationArray[8].ToString()).TrimStart(',') == "" ? "None" : (locationArray[8].ToString()).TrimStart(','));//8
            //            string district = ((locationArray[9].ToString()).TrimStart(',') == "" ? "None" : (locationArray[9].ToString()).TrimStart(','));//9

            //            decimal mileage = Convert.ToDecimal(Convert.ToDouble(odometer) * 0.000621371192);
            //            int mph = Convert.ToInt32(Convert.ToDouble(speedKPH) * 0.621371);

            //            ////no registration -->insert record
            //            this.sp_AS_11023_Verilocation_ItemTableAdapter.Insert("add", "", -verilocationCounter1, splitReg(t.Reg), t.Vid.ToString(), t.Reg, 0, t.Make, t.Model, odometer, mileage, reason, 0,
            //            latitude, longitude, speedKPH, mph, journeyAVGSpeed, strLatestStartJourney, strLatestEndJourney, placename, roadname, postsector, geofenceName, district,
            //            getCurrentDate(), name, mobile, email);
            //            verilocationCounter1++;
            //        }

            //    }

            //    float f = (((float)(verilocationCounter1) / (float)(progressTotal))*100);
            //    if (f > progressPercentage)
            //    {
            //        if (fProgress != null)
            //            fProgress.UpdateProgress(increament); // Update Progress Bar //
            //        progressPercentage++;
            //    }               
            //}
            //if (fProgress != null)
            //{
            //    fProgress.UpdateProgress(10); // Update Progress Bar //
            //    fProgress.Close();
            //    fProgress = null;
            //}
            //XtraMessageBox.Show(verilocationCounter1 + " new tracker(s) added","New Tracker(s) Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void matchRecordsVerilocation(bool matchUpdate)
        {
            XtraMessageBox.Show("The Verilocation Service has been deprecated. Please contact the administrator for further assistance.", "Verilocation Service Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Information);
           
            // SqlConnection conn = new SqlConnection(strConnectionString);

            // SqlCommand cmd = new SqlCommand("dbo.sp_AS_11000_Match_Verilocation_Data", conn);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add(new SqlParameter("@RecordCount", SqlDbType.Int));
            //cmd.Parameters["@RecordCount"].Direction = ParameterDirection.Output;
            //cmd.Parameters.Add(new SqlParameter("@RecordTotalCount", SqlDbType.Int));
            //cmd.Parameters["@RecordTotalCount"].Direction = ParameterDirection.Output;
            //conn.Open();
            //try
            //{
            //    cmd.ExecuteNonQuery();
            //    int intRecCount = Convert.ToInt32(cmd.Parameters["@RecordCount"].Value);
            //    int intRecTotalCount = Convert.ToInt32(cmd.Parameters["@RecordTotalCount"].Value);
            //    if (matchUpdate)
            //    {
            //        XtraMessageBox.Show(intRecCount.ToString() + " records matched out of a possible " + intRecTotalCount.ToString() +" records.", "Match complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    XtraMessageBox.Show(ex.Message, "Error Matching", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //finally
            //{
            //    conn.Close();
            //}
        }

        private void updateSelectedExistingVehicles()
        {
            XtraMessageBox.Show("The Verilocation Service has been deprecated. Please contact the administrator for further assistance.", "Verilocation Service Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //string selectedVID = getSelectedVID(equipmentGridControl);
            //selectedVID = (selectedVID == "" ? "-1" : selectedVID);
            //string[] arrayVID = null;
            //float progressTotal;
            //verilocationCounter1 = 0;
            //if (selectedVID != "-1")
            //{
            //    arrayVID = splitStrRecords(selectedVID);
            //    progressTotal = (float)(arrayVID.Length);
            //}
            //else
            //{
            //    progressTotal = 0;
            //}
            
            //frmProgress fProgress = new frmProgress(0);
            //fProgress.UpdateCaption("Updating Trackers...");
            //fProgress.Show();

            //if (progressTotal < 1)
            //{
            //    fProgress.UpdateProgress(100); // Update Progress Bar //
            //    fProgress.Close();
            //    fProgress = null;
            //}
            //else
            //{
            //    connectToVerilocation();
            //    float increament = (float)(1) / progressTotal*100;
            //    float progressPercentage = increament;

            //    wscGpsUnitExt[] unitList = tsClient.VehiclesExt(strToken);
            //    NameValueCollection driver = getDriverDetails();
            //    NameValueCollection location = getLocationDetails();

            //    foreach (wscGpsUnitExt t in unitList)
            //    {
            //        foreach (string x in arrayVID)
            //        {
            //            if (x.Contains(t.Vid.ToString()))
            //            {
            //                string[] driverArray = splitRecords(driver[t.Vid.ToString() + ";"]);
            //                string[] locationArray = splitRecords(location[t.Vid.ToString() + ";"]);

            //                verilocationCounter1++;
            //                if (locationArray != null && locationArray.Length == 10)
            //                {
            //                    decimal journeyAVGSpeed = 0;
            //                    string strLatestStartJourney = "Unknown";
            //                    string strLatestEndJourney = "Unknown";
            //                    string name = "Unknown";
            //                    string mobile = "Unknown";
            //                    string email = "Unknown";

            //                    //get jouney details
            //                    NameValueCollection journey = getLatestJourneyDetails(t.Vid.ToString());
            //                    string[] journeyArray = splitRecords(journey["1;"]);
            //                    if (journeyArray != null && journeyArray.Length > 4)
            //                    {
            //                        journeyAVGSpeed = Convert.ToDecimal((journeyArray[1].ToString()).TrimStart(','));
            //                        strLatestStartJourney = (journeyArray[2].ToString()).TrimStart(',');
            //                        strLatestEndJourney = (journeyArray[3].ToString()).TrimStart(',');
            //                    }

            //                    //get driver details
            //                    if (driverArray != null && driverArray.Length == 4)
            //                    {
            //                        name = (driverArray[0].ToString() == "" ? "None" : (driverArray[0].ToString())).TrimStart(',');
            //                        mobile = (driverArray[1].ToString() == "" ? "None" : (driverArray[1].ToString())).TrimStart(',');
            //                        email = (driverArray[2].ToString() == "" ? "None" : (driverArray[2].ToString())).TrimStart(',');
            //                    }

            //                    //get location details
            //                    int odometer = (Convert.ToInt32((locationArray[0].ToString()).TrimStart(',')) == null ? 0 : Convert.ToInt32((locationArray[0].ToString()).TrimStart(',')));//0
            //                    string reason = ((locationArray[1].ToString()).TrimStart(',') == "" ? "None" : (locationArray[1].ToString()).TrimStart(','));//1
            //                    decimal latitude = (Convert.ToDecimal((locationArray[2].ToString()).TrimStart(',')) == null ? 0 : Convert.ToDecimal((locationArray[2].ToString()).TrimStart(',')));//2
            //                    decimal longitude = (Convert.ToDecimal((locationArray[3].ToString()).TrimStart(',')) == null ? 0 : Convert.ToDecimal((locationArray[3].ToString()).TrimStart(',')));//3
            //                    int speedKPH = (Convert.ToInt32((locationArray[4].ToString()).TrimStart(',')) == null ? 0 : Convert.ToInt32((locationArray[4].ToString()).TrimStart(',')));//4
            //                    string placename = ((locationArray[5].ToString()).TrimStart(',') == "" ? "None" : (locationArray[5].ToString()).TrimStart(','));//5
            //                    string roadname = ((locationArray[6].ToString()).TrimStart(',') == "" ? "None" : (locationArray[6].ToString()).TrimStart(','));//6
            //                    string postsector = ((locationArray[7].ToString()).TrimStart(',') == "" ? "None" : (locationArray[7].ToString()).TrimStart(','));//7
            //                    string geofenceName = ((locationArray[8].ToString()).TrimStart(',') == "" ? "None" : (locationArray[8].ToString()).TrimStart(','));//8
            //                    string district = ((locationArray[9].ToString()).TrimStart(',') == "" ? "None" : (locationArray[9].ToString()).TrimStart(','));//9

            //                    decimal mileage = Convert.ToDecimal(Convert.ToDouble(odometer) * 0.000621371192);
            //                    int mph = Convert.ToInt32(Convert.ToDouble(speedKPH) * 0.621371);

            //                    ////no registration -->insert record
            //                    this.sp_AS_11023_Verilocation_ItemTableAdapter.Update("update", "", -verilocationCounter1, splitReg(t.Reg), t.Vid.ToString(), t.Reg, 0, t.Make, t.Model, odometer, mileage, reason, 0,
            //                    latitude, longitude, speedKPH, mph, journeyAVGSpeed, strLatestStartJourney, strLatestEndJourney, placename, roadname, postsector, geofenceName, district,
            //                    getCurrentDate(), name, mobile, email);
            //                }


            //                float f = (((float)(verilocationCounter1) / (float)(progressTotal)) * 100);

            //                if (f > progressPercentage)
            //                {
            //                    if (fProgress != null)
            //                        fProgress.UpdateProgress(increament); // Update Progress Bar //
            //                    progressPercentage = progressPercentage + increament;
            //                }
            //            }
            //        }
            //    }
            //}
            //if (fProgress != null)
            //{
            //    fProgress.UpdateProgress(1); // Update Progress Bar //
            //    fProgress.Close();
            //    fProgress = null;
            //}
            //XtraMessageBox.Show(verilocationCounter1 + " tracker(s) updated", "Tracker(s) Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private string getSelectedVID(GridControl gridControl)
        {
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            string strVIDRecordsToLoad = "";
            string strCurrentID = "";
            foreach (int intRowHandle in intRowHandles)
            {
                DataRow dr = view.GetDataRow(intRowHandle);
              
                switch (i_int_FocusedGrid)
                {
                    case 0:
                        strCurrentID = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString() ;
                        DataRow[] veriRows = this.dataSet_AS_DataEntry.sp_AS_11023_Verilocation_Item.Select("EquipmentID = " + strCurrentID);
                        if (veriRows.Length > 0)
                        {
                            strVIDRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11023_Verilocation_ItemRow)(veriRows[0])).TrackerVID).ToString() + ',';
                        }
                        break;
                            
                    default:
                        strVIDRecordsToLoad = "";
                        break;
                }                        
            }

            return strVIDRecordsToLoad;
        }

        private void updateExistingVehicles()
        {
            XtraMessageBox.Show("The Verilocation Service has been deprecated. Please contact the administrator for further assistance.", "Verilocation Service Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //connectToVerilocation();
            //verilocationCounter1 = 0;
            //wscGpsUnitExt[] unitList = tsClient.VehiclesExt(strToken);
            //NameValueCollection driver = getDriverDetails();
            //NameValueCollection location = getLocationDetails();

            //float progressTotal = (float)(unitList.Length);
            //float increament = ((float)(1) / progressTotal)*100;
            //float progressPercentage = increament;
            //frmProgress fProgress = new frmProgress(0);
            //fProgress.UpdateCaption("Updating Trackers...");
            //fProgress.Show();

            //if (progressTotal < 1)
            //{
            //    fProgress.UpdateProgress(100); // Update Progress Bar //
            //    fProgress.Close();
            //    fProgress = null;
            //}
            //foreach (wscGpsUnitExt t in unitList)
            //{
            //    string[] driverArray = splitRecords(driver[t.Vid.ToString() + ";"]);
            //    string[] locationArray = splitRecords(location[t.Vid.ToString() + ";"]);

            //    verilocationCounter1++;
            //    if (locationArray != null && locationArray.Length == 10)
            //    {
            //        decimal journeyAVGSpeed = 0;
            //        string strLatestStartJourney = "Unknown";
            //        string strLatestEndJourney = "Unknown";
            //        string name = "Unknown";
            //        string mobile = "Unknown";
            //        string email = "Unknown";

            //        //get jouney details
            //        NameValueCollection journey = getLatestJourneyDetails(t.Vid.ToString());
            //        string[] journeyArray = splitRecords(journey["1;"]);
            //        if (journeyArray != null && journeyArray.Length > 4)
            //        {
            //            journeyAVGSpeed = Convert.ToDecimal((journeyArray[1].ToString()).TrimStart(','));
            //            strLatestStartJourney = (journeyArray[2].ToString()).TrimStart(',');
            //            strLatestEndJourney = (journeyArray[3].ToString()).TrimStart(',');
            //        }

            //        //get driver details
            //        if (driverArray != null && driverArray.Length == 4)
            //        {
            //            name = (driverArray[0].ToString() == "" ? "None" : (driverArray[0].ToString())).TrimStart(',');
            //            mobile = (driverArray[1].ToString() == "" ? "None" : (driverArray[1].ToString())).TrimStart(',');
            //            email = (driverArray[2].ToString() == "" ? "None" : (driverArray[2].ToString())).TrimStart(',');
            //        }

            //        //get location details
            //        int odometer = (Convert.ToInt32((locationArray[0].ToString()).TrimStart(',')) == null ? 0 : Convert.ToInt32((locationArray[0].ToString()).TrimStart(',')));//0
            //        string reason = ((locationArray[1].ToString()).TrimStart(',') == "" ? "None" : (locationArray[1].ToString()).TrimStart(','));//1
            //        decimal latitude = (Convert.ToDecimal((locationArray[2].ToString()).TrimStart(',')) == null ? 0 : Convert.ToDecimal((locationArray[2].ToString()).TrimStart(',')));//2
            //        decimal longitude = (Convert.ToDecimal((locationArray[3].ToString()).TrimStart(',')) == null ? 0 : Convert.ToDecimal((locationArray[3].ToString()).TrimStart(',')));//3
            //        int speedKPH = (Convert.ToInt32((locationArray[4].ToString()).TrimStart(',')) == null ? 0 : Convert.ToInt32((locationArray[4].ToString()).TrimStart(',')));//4
            //        string placename = ((locationArray[5].ToString()).TrimStart(',') == "" ? "None" : (locationArray[5].ToString()).TrimStart(','));//5
            //        string roadname = ((locationArray[6].ToString()).TrimStart(',') == "" ? "None" : (locationArray[6].ToString()).TrimStart(','));//6
            //        string postsector = ((locationArray[7].ToString()).TrimStart(',') == "" ? "None" : (locationArray[7].ToString()).TrimStart(','));//7
            //        string geofenceName = ((locationArray[8].ToString()).TrimStart(',') == "" ? "None" : (locationArray[8].ToString()).TrimStart(','));//8
            //        string district = ((locationArray[9].ToString()).TrimStart(',') == "" ? "None" : (locationArray[9].ToString()).TrimStart(','));//9

            //        decimal mileage = Convert.ToDecimal(Convert.ToDouble(odometer) * 0.000621371192);
            //        int mph = Convert.ToInt32(Convert.ToDouble(speedKPH) * 0.621371);

            //        ////no registration -->insert record
            //        this.sp_AS_11023_Verilocation_ItemTableAdapter.Update("update", "", -verilocationCounter1, splitReg(t.Reg), t.Vid.ToString(), t.Reg, 0, t.Make, t.Model, odometer, mileage, reason, 0,
            //        latitude, longitude, speedKPH, mph, journeyAVGSpeed, strLatestStartJourney, strLatestEndJourney, placename, roadname, postsector, geofenceName, district,
            //        getCurrentDate(), name, mobile, email);
            //    }


            //    float f = (((float)(verilocationCounter1) / (float)(progressTotal)) *100);

            //    if (f > progressPercentage)
            //    {
            //        if (fProgress != null)
            //            fProgress.UpdateProgress(increament); // Update Progress Bar //
            //        progressPercentage = progressPercentage + increament;
            //    }
            //}
            //if (fProgress != null)
            //{
            //    fProgress.UpdateProgress(1); // Update Progress Bar //
            //    fProgress.Close();
            //    fProgress = null;
            //}
            //XtraMessageBox.Show(verilocationCounter1 + " tracker(s) updated", "Tracker(s) Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void addSpeedData()
        {
            XtraMessageBox.Show("The Verilocation Service has been deprecated. Please contact the administrator for further assistance.", "Verilocation Service Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Information);
           
            //connectToVerilocation();
            //verilocationCounter1 = 0;
            //wscGpsUnitExt[] unitList = tsClient.VehiclesExt(strToken);
            //NameValueCollection driver = getDriverDetails();
            //NameValueCollection location = getLocationDetails();

            //float progressTotal = (float)(unitList.Length);
            //float increament = ((float)(1) / progressTotal) * 100;
            //float progressPercentage = increament;
            //frmProgress fProgress = new frmProgress(0);
            //fProgress.UpdateCaption("Updating Trackers...");
            //fProgress.Show();

            //if (progressTotal < 1)
            //{
            //    fProgress.UpdateProgress(100); // Update Progress Bar //
            //    fProgress.Close();
            //    fProgress = null;
            //}
            //foreach (wscGpsUnitExt t in unitList)
            //{                    
            //    getSpeedDetails(t.Vid.ToString());
                
            //    verilocationCounter1++;

            //    float f = (((float)(verilocationCounter1) / (float)(progressTotal)) * 100);

            //    if (f > progressPercentage)
            //    {
            //        if (fProgress != null)
            //            fProgress.UpdateProgress(increament); // Update Progress Bar //
            //        progressPercentage = progressPercentage + increament;
            //    }
            //}
            //if (fProgress != null)
            //{
            //    fProgress.UpdateProgress(1); // Update Progress Bar //
            //    fProgress.Close();
            //    fProgress = null;
            //}
            //matchRecordsVerilocation(false);
            //XtraMessageBox.Show(verilocationCounter1 + " tracker(s) updated", "Tracker(s) Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        private NameValueCollection getLatestJourneyDetails(string vid)
        {
            wscJourney[] journeyDetailList = tsClient.JourneysByVehicle(strToken,(getCurrentDate()).AddDays(-1),getCurrentDate(),Convert.ToInt32(vid));
            NameValueCollection journey = new NameValueCollection();
            NameValueCollection journeyLatest = new NameValueCollection();
            string averageSpeed = "";
            string journeyStartLocation = "";
            string journeyEndLocation = "";
            string journeyStartUTC = "";
            string prevJourneyStartUTC = "";
            string journeyEndUTC = "";
            int counter = 1; 
            int lastCounter = 0;
            foreach (wscJourney journeyRecord in journeyDetailList)
            {
                averageSpeed = (journeyRecord.AveSpeed.ToString() == "" ? "None" : journeyRecord.AveSpeed.ToString());
                journeyStartLocation = (journeyRecord.StartName.ToString() == "" ? "None" : journeyRecord.StartName.ToString());
                journeyEndLocation = (journeyRecord.EndName.ToString() == "" ? "None" : journeyRecord.EndName.ToString());
                journeyStartUTC = (journeyRecord.StartUtc.ToString() == "" ? "None" : journeyRecord.StartUtc.ToString());
                journeyEndUTC = (journeyRecord.EndUtc.ToString() == "" ? "None" : journeyRecord.EndUtc.ToString());

                if (counter > 1)
                {
                    if (Convert.ToDateTime(journeyRecord.StartUtc) > Convert.ToDateTime(prevJourneyStartUTC))
                    {
                         if (journeyLatest.Count > 0)
                         {
                             journeyLatest.Remove(lastCounter.ToString()+";");
                         }
            
                        journeyLatest.Add(counter.ToString() + ";", journeyRecord.Vid.ToString() + ";");
                        journeyLatest.Add(counter.ToString() + ";", averageSpeed + ";");
                        journeyLatest.Add(counter.ToString() + ";", journeyStartLocation + ";");
                        journeyLatest.Add(counter.ToString() + ";", journeyEndLocation + ";");
                        journeyLatest.Add(counter.ToString() + ";", journeyStartUTC + ";");
                        journeyLatest.Add(counter.ToString() + ";", journeyEndUTC + ";");
                        lastCounter = counter;
                        prevJourneyStartUTC = journeyStartUTC;
                    }
                }
                else
                {
                    journey.Add(counter.ToString() + ";", journeyRecord.Vid.ToString() + ";");
                    journey.Add(counter.ToString() + ";", averageSpeed + ";");
                    journey.Add(counter.ToString() + ";", journeyStartLocation + ";");
                    journey.Add(counter.ToString() + ";", journeyEndLocation + ";");
                    journey.Add(counter.ToString() + ";", journeyStartUTC + ";");
                    journey.Add(counter.ToString() + ";", journeyEndUTC + ";");
                    prevJourneyStartUTC = journeyStartUTC;
                }
                counter++;
            }

            if (journey.Count == 1 && journeyLatest.Count == 0)
            {
                journeyLatest =journey;
            }
         return journeyLatest;                 
        }

        private NameValueCollection getJourneyDetails(string vid)
        {
            wscJourney[] journeyDetailList = tsClient.JourneysByVehicle(strToken, (getCurrentDate()).AddDays(-1), getCurrentDate(), Convert.ToInt32(vid));
            NameValueCollection journey = new NameValueCollection();
            string averageSpeed = "";
            string journeyStartLocation = "";
            string journeyEndLocation = "";
            string journeyStartUTC = "";
            string journeyEndUTC = "";
            int counter = 1;
            foreach (wscJourney journeyRecord in journeyDetailList)
            {
                averageSpeed = (journeyRecord.AveSpeed.ToString() == "" ? "None" : journeyRecord.AveSpeed.ToString());
                journeyStartLocation = (journeyRecord.StartName.ToString() == "" ? "None" : journeyRecord.StartName.ToString());
                journeyEndLocation = (journeyRecord.EndName.ToString() == "" ? "None" : journeyRecord.EndName.ToString());
                journeyStartUTC = (journeyRecord.StartUtc.ToString() == "" ? "None" : journeyRecord.StartUtc.ToString());
                journeyEndUTC = (journeyRecord.EndUtc.ToString() == "" ? "None" : journeyRecord.EndUtc.ToString());


                journey.Add(counter.ToString() + ";", journeyRecord.Vid.ToString() + ";");
                journey.Add(counter.ToString() + ";", averageSpeed + ";");
                journey.Add(counter.ToString() + ";", journeyStartLocation + ";");
                journey.Add(counter.ToString() + ";", journeyEndLocation + ";");
                journey.Add(counter.ToString() + ";", journeyStartUTC + ";");
                journey.Add(counter.ToString() + ";", journeyEndUTC + ";");
                counter++;
            }
            return journey;
        }

        private NameValueCollection getDriverDetails()
        { 
            wscDriver[] driverDetailList = tsClient.Drivers(strToken);
            NameValueCollection driver = new NameValueCollection();
            string name = "";
            string mobile = "";
            string email = "";
             foreach (wscDriver driverRecord in driverDetailList)
            {

                name = (driverRecord.Name.ToString() == "" ? "None" : driverRecord.Name.ToString());
                mobile = (driverRecord.Mobile.ToString() == "" ? "None" : driverRecord.Mobile.ToString());
                email = (driverRecord.Email.ToString() == "" ? "None" : driverRecord.Email.ToString());

                driver.Add(driverRecord.Vid.ToString() + ";", name + ";");
                driver.Add(driverRecord.Vid.ToString() + ";", mobile + ";");
                driver.Add(driverRecord.Vid.ToString() + ";", email + ";");
            }
             return driver;
        }

        private NameValueCollection getLocationDetails()
        {
            wscGpsPlot[] locationList = tsClient.LocationsLatest(strToken);
            NameValueCollection location = new NameValueCollection();

            string odometer, reason, latitude, longitude, velocity, placename, roadname, postsector, geofenceName, district;
            foreach (wscGpsPlot locationRecord in locationList)
            {
                odometer = (locationRecord.Odometer.ToString() == "" ? "0" : locationRecord.Odometer.ToString());//0
                reason = (locationRecord.Reason.ToString() == "" ? "None" : locationRecord.Reason.ToString());//1
                latitude = (locationRecord.Latitude.ToString() == "" ? "0" : locationRecord.Latitude.ToString());//2
                longitude = (locationRecord.Longitude.ToString() == "" ? "0" : locationRecord.Longitude.ToString());//3
                velocity = (locationRecord.Velocity.ToString() == "" ? "0" : locationRecord.Velocity.ToString());//4
                placename = (locationRecord.Placename.ToString() == "" ? "None" : locationRecord.Placename.ToString());//5
                roadname = (locationRecord.RoadName.ToString() == "" ? "None" : locationRecord.RoadName.ToString());//6
                postsector = (locationRecord.PostSector.ToString() == "" ? "None" : locationRecord.PostSector.ToString());//7
                geofenceName = (locationRecord.GeoFenceName.ToString() == "" ? "None" : locationRecord.GeoFenceName.ToString());//8
                district = (locationRecord.District.ToString() == "" ? "None" : locationRecord.District.ToString());//9

                location.Add(locationRecord.Vid.ToString() + ";", odometer + ";");//0
                location.Add(locationRecord.Vid.ToString() + ";", reason + ";");//1
                location.Add(locationRecord.Vid.ToString() + ";", latitude + ";");//2
                location.Add(locationRecord.Vid.ToString() + ";", longitude + ";");//3
                location.Add(locationRecord.Vid.ToString() + ";", velocity + ";");//4
                location.Add(locationRecord.Vid.ToString() + ";", placename + ";");//5
                location.Add(locationRecord.Vid.ToString() + ";", roadname + ";");//6
                location.Add(locationRecord.Vid.ToString() + ";", postsector + ";");//7
                location.Add(locationRecord.Vid.ToString() + ";", geofenceName + ";");//8
                location.Add(locationRecord.Vid.ToString() + ";", district + ";");//9
            }
            return location;
        }

        private string[] splitStrRecords(string newIDs)
        {
            string[] parts;
            char[] delimiters = new char[] { ';', ',' };
            if (newIDs == "")
            {
                parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                parts = newIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }

            return parts;
        }

        private DateTime getCurrentDate()
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetDate = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetDate.ChangeConnectionString(strConnectionString);
            GetDate.sp_AS_11138_Get_Server_Date();
            DateTime d = DateTime.Parse(GetDate.sp_AS_11138_Get_Server_Date().ToString());
            return d;
        }

        public void frmActivated()
        {
            if (UpdateRefreshStatus > 0 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
            {
                Load_Data();
            }
            SetMenuStatus();
        }
        
        private void OpenEditForm(FormMode mode, string frmCaller, GridControl gridControl,EquipmentType equipmentType)
        {
            switch (equipmentType)
            {
                case EquipmentType.None:
                case EquipmentType.Vehicle:
                case EquipmentType.Plant:
                break;                
                case EquipmentType.Gadget:
                case EquipmentType.Hardware:
                case EquipmentType.Software:
                case EquipmentType.Office:
                    XtraMessageBox.Show("Manipulation of this equipment type is restricted on the " + this.Text, "Restricted Access Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
            }
            bool cancelChildLoad = false;
            GridView view = null;
            frmProgress fProgress = null;
            MethodInfo method = null;
            strRecordsToLoad = "";
            int[] intRowHandles;
            int intCount = 0;
            saveGridViewState();
            switch (i_int_FocusedGrid)
            {
                #region Equipment Main

                case 0:     // Equipment
                case 1:     // Vehicle
                case 2:     // Plant 
               // case 3:     // Gadget 
                //case 4:     // Hardware 
                case 5:     // Software 
                case 6:     // Office
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    if (mode == FormMode.blockadd)
                    {
                        XtraMessageBox.Show("Blockadd is restricted.", "Blockadd Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (mode == FormMode.edit )
                    {   
                        if (mode == FormMode.blockedit)
                        {
                            if (intCount <= 1)
                            {
                                XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                        }

                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        DataRow dr = view.GetDataRow(intRowHandle);
                        switch (i_int_FocusedGrid)
                        {
                            case 0:
                                strRecordsToLoad += (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString() + ',';
                                break;
                            case 1:
                                strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(dr)).EquipmentID).ToString() + ',';
                                break;
                            case 2:
                                strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11008_Plant_ItemRow)(dr)).EquipmentID).ToString() + ',';
                                break;
                            //case 3:
                            //    strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11026_Gadget_ItemRow)(dr)).EquipmentID).ToString() + ',';
                            //    break;
                            //case 4:
                            //    strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11032_Hardware_ItemRow)(dr)).EquipmentID).ToString() + ',';
                            //    break;
                            //case 5:
                            //    strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11035_Software_ItemRow)(dr)).EquipmentID).ToString() + ',';
                            //    break;
                            //case 6:
                            //    strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11029_Office_ItemRow)(dr)).EquipmentID).ToString() + ',';
                            //    break;
                            default:
                                XtraMessageBox.Show("Manipulation of this equipment type is restricted on the " + this.Text, "Restricted Access Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                        }                        
                    }
                    
                    frm_AS_Equipment_Edit fChildForm = new frm_AS_Equipment_Edit();
                    fChildForm.intExchequerCategoryMVG = intExchequerCategoryMVG;
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordsToLoad;
                    fChildForm.strEquipmentIDs = strRecordIDs;
                    fChildForm.formMode = (frm_AS_Equipment_Edit.FormMode)mode;
                    fChildForm.strFormMode = (mode.ToString()).ToLower();
                    fChildForm.strCaller = frmCaller;
                    fChildForm.iBool_AllowAddDepreciation = iBool_AllowAddDepreciation;
                    fChildForm.iBool_AllowEditDepreciation = iBool_AllowEditDepreciation;                        
                    fChildForm.iBool_AllowDeleteDepreciation = iBool_AllowDeleteDepreciation;
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;
                    if (equipmentType != EquipmentType.None)
                    {
                        fChildForm.passedEquipType = (frm_AS_Equipment_Edit.EquipmentType)equipmentType;
                    }
                  
                    SplashScreenManager splashScreenManager1 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                #endregion

                #region Billing

                case 7:     // Billing

                    if (mode == FormMode.blockadd || mode == FormMode.add)
                    {
                        XtraMessageBox.Show("Adding is restricted in this section, add billing records in the depreciation data.", "Add/Block Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (numOfSelectedRows != 1)
                    {
                        XtraMessageBox.Show("Select one equipment record to edit/block edit the equipment's billing records.", "Edit/Block Edit Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Billing Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }                    

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);
                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11050_Depreciation_ItemRow)(dr)).DepreciationID).ToString() + ',';                         
                        }
                    }
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows < 1)
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate billing(s) before proceeding.", "Add Billing Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";
                   
                    if (cancelChildLoad)
                    {
                        return;
                    }

                    frm_AS_Billings_Edit bChildForm = new frm_AS_Billings_Edit();
                    bChildForm.MdiParent = this.MdiParent;
                    bChildForm.GlobalSettings = this.GlobalSettings;
                    bChildForm.strRecordIDs = strRecordsToLoad;
                    bChildForm.strEquipmentIDs = strRecordIDs;
                    bChildForm.formMode = (frm_AS_Billings_Edit.FormMode)mode;
                    bChildForm.strFormMode = (mode.ToString()).ToLower();
                    bChildForm.strCaller = frmCaller;
                    bChildForm.strGCReference = gcRef;                        
                    bChildForm.intRecordCount = intCount;
                    bChildForm.FormPermissions = this.FormPermissions;
                   
                    SplashScreenManager splashScreenManager2 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    bChildForm.splashScreenManager = splashScreenManager2;
                    bChildForm.splashScreenManager.ShowWaitForm();
                    bChildForm.Show();
                    break;
                #endregion

                #region Keeper

                case 8:     // Keeper
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
              
                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Keeper Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dr)).KeeperAllocationID).ToString() + ','; 
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Keeper Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate keepers before proceeding.", "Add Keeper Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";    
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 )
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add keeper allocations.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1 )//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the keepers";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }
                            
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }
                        
                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();  
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add keepers, otherwise right click for block adding keepers.", "Add Keeper Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();        
                        }            
                    }

                    frm_AS_Keeper_Edit kChildForm = new frm_AS_Keeper_Edit();
                    kChildForm.MdiParent = this.MdiParent;
                    kChildForm.GlobalSettings = this.GlobalSettings;
                    kChildForm.strRecordIDs = strRecordsToLoad;
                    kChildForm.strEquipmentIDs = strRecordIDs;
                    kChildForm.formMode = (frm_AS_Keeper_Edit.FormMode)mode;
                    kChildForm.strFormMode = (mode.ToString()).ToLower();
                    kChildForm.strCaller = frmCaller;
                    kChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        kChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        kChildForm.intRecordCount = intCount;
                    }
                    kChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager3 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    kChildForm.splashScreenManager = splashScreenManager3;
                    kChildForm.splashScreenManager.ShowWaitForm();
                    kChildForm.Show();
                    break;

                #endregion

                #region Transactions 

                case 9:     // Transactions
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Transaction Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);                           
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11041_Transaction_ItemRow)(dr)).TransactionID).ToString() + ',';     
                        }
                    }

                    gcRef = "";
                   
                    bool hasPurchaseRecord = false;
                    cancelChildLoad = false;
                    //check if purchase already exists
                    DataRow[] drs = dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item.Select("TransactionType like 'purchase'");
                    foreach (DataRow dr in drs)
                    {
                        hasPurchaseRecord = true;
                    }

                    bool hasPriceListRecord = false;
                    //check if price list transaction type already exists
                    DataRow[] drs1 = dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item.Select("TransactionType like 'list price'");
                    foreach (DataRow dr in drs1)
                    {
                        hasPriceListRecord = true;
                    }
                    tempMake = "";
                    tempModel = "";
                    tempManufacturer = "";
                    tempRegistration = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 || iDistinctSelectedType == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add transactions.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows != 1 && iDistinctSelectedType != 0)//block add option
                        {
                            string strTypes = "";
                            switch (iDistinctSelectedType)
                            {
                                case 1:
                                    strTypes = "You have selected multiple vehicles, do you want to block add vehicle purchases";
                                    break;
                                case 2:
                                    strTypes = "You have selected multiple plants, do you want to block add plant purchases";
                                    break;
                                case 3:
                                    strTypes = "You have selected multiple gadgets, do you want to block add gadget purchases";
                                    break;
                                case 4:
                                    strTypes = "You have selected multiple hardware records, do you want to block add hardware purchases";
                                    break;
                                case 5:
                                    strTypes = "You have selected multiple software records, do you want to block add software purchases";
                                    break;
                                case 6:
                                    strTypes = "You have selected multiple office records, do you want to block add office purchases";
                                    break;
                                default:
                                    strTypes = "You have selected multiple records, do you want to block add the purchases";
                                    break;
                            }
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }
                            
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }
                        if (mode == FormMode.blockadd && (hasPurchaseRecord || hasPriceListRecord))
                        {
                            XtraMessageBox.Show("One of the records already has a purchase / price-list record, remove this record to proceed","Block Add Restricted");
                            return;
                        }
                        
                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();
                            tempMake ="GC Reference : "+gcRef+ "\r\n" + "Make : " + (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).Make).ToString();
                            tempModel = "\r\n" + "Model : " + (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).Model).ToString();
                            tempManufacturer = "\r\n" + "Manufacturer ID : " + (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).ManufacturerID).ToString();
                            if (gcRef.Substring(0,3).ToLower() =="veh")
                            {
                                tempManufacturer = "\r\n" + "VIN : " + (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).ManufacturerID).ToString();
                                tempRegistration = "\r\n" + "Registration : " + getVehicleReg(((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID);                                
                            }
                            
                        }

                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    frm_AS_Transactions_Edit tChildForm = new frm_AS_Transactions_Edit();
                    tChildForm.MdiParent = this.MdiParent;
                    tChildForm.GlobalSettings = this.GlobalSettings;
                    tChildForm.hasPurchaseRecord = hasPurchaseRecord;
                    tChildForm.hasPriceListRecord = hasPriceListRecord;
                    tChildForm.strEquipmentIDs = strRecordIDs;
                    tChildForm.strRecordIDs = strRecordsToLoad;
                    tChildForm.strFormMode = (mode.ToString()).ToLower();
                    tChildForm.formMode = (frm_AS_Transactions_Edit.FormMode)mode;
                    tChildForm.strCaller = frmCaller;            
                    tChildForm.strGCReference = gcRef;
                    if (mode == FormMode.add)
                    {
                        tChildForm.strTransactionDescription = tempMake + tempModel + tempManufacturer + tempRegistration;
                    }
                    if (mode == FormMode.blockadd)
                    {
                        tChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        tChildForm.intRecordCount = intCount;
                    }
                    tChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager4 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    tChildForm.splashScreenManager = splashScreenManager4;
                    tChildForm.splashScreenManager.ShowWaitForm();
                    tChildForm.Show();
                    break;
                #endregion

                #region Notifications

                case 10:     // Notification
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Equipment Notification Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11126_Notification_ItemRow)(dr)).NotificationID).ToString() + ',';
                        }
                    }

                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Notification.", "Add Equipment Notification Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the s";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11001_Equipment_ManagerRow)(dr)).EquipmentReference).ToString();
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Equipment Notification records, otherwise right click for block adding Equipment Purpose records.", "Add Notification Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11001_Equipment_ManagerRow)(dr)).EquipmentReference).ToString();
                        }
                    }

                    frm_AS_Notification_Edit nChildForm = new frm_AS_Notification_Edit();
                    nChildForm.MdiParent = this.MdiParent;
                    nChildForm.GlobalSettings = this.GlobalSettings;
                    nChildForm.strRecordIDs = strRecordsToLoad;
                    nChildForm.strEquipmentIDs = strRecordIDs;
                    nChildForm.formMode = (frm_AS_Notification_Edit.FormMode)mode;
                    nChildForm.strFormMode = (mode.ToString()).ToLower();
                    nChildForm.strCaller = frmCaller;
                    nChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        nChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        nChildForm.intRecordCount = intCount;
                    }
                    nChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager10n = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    nChildForm.splashScreenManager = splashScreenManager10n;
                    nChildForm.splashScreenManager.ShowWaitForm();
                    nChildForm.Show();
                    break;
                #endregion

                #region Depreciation

                case 11:     // Depreciation
                    break;
                #endregion

                #region Incidents
                case 18: // Speeding data                
                case 12:     // Incidents 
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
              
                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Incident Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)(dr)).IncidentID).ToString() + ','; 
                        }
                    }
                    

                    cancelChildLoad = false;
                    gcRef = "";    
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 )
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add incidents.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1 )//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the incidents";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }
                            
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }
                        
                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();  
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add incidents, otherwise right click for block adding incidents.", "Add Incident Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();        
                        }            
                    }

                    frm_AS_Incident_Edit iChildForm = new frm_AS_Incident_Edit();
                    iChildForm.MdiParent = this.MdiParent;
                    iChildForm.GlobalSettings = this.GlobalSettings;
                    iChildForm.strRecordIDs = strRecordsToLoad;
                    iChildForm.strEquipmentIDs = strRecordIDs;
                    iChildForm.formMode = (frm_AS_Incident_Edit.FormMode)mode;
                    iChildForm.strFormMode = (mode.ToString()).ToLower();
                    iChildForm.strCaller = frmCaller;
                    iChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        iChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        iChildForm.intRecordCount = intCount;
                    }
                    iChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager5 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    iChildForm.splashScreenManager = splashScreenManager5;
                    iChildForm.splashScreenManager.ShowWaitForm();
                    iChildForm.Show();
                    break;
                #endregion

                #region Service Data

                case 13:     // Service Data
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (mode == FormMode.blockadd || mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("blockadd/blockedit is restricted in this section.", "Block Add/Edit Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Service Interval Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);
                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceDataID).ToString() + ','; 
                        }
                    }

                                   
                    cancelChildLoad = false;
                    gcRef = "";    
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 || iDistinctSelectedType == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add service interval(s).", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1 )//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the Service Intervals";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }
                            
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }
                        
                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();  
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add service intervals, otherwise right click for block adding service intervals.", "Add Service Interval Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();        
                        }            
                    }

                    frm_AS_Service_Data_Edit sIChildForm = new frm_AS_Service_Data_Edit();
                    sIChildForm.MdiParent = this.MdiParent;
                    sIChildForm.GlobalSettings = this.GlobalSettings;
                    sIChildForm.strRecordIDs = strRecordsToLoad;
                    sIChildForm.strEquipmentIDs = strRecordIDs;
                    sIChildForm.formMode = (frm_AS_Service_Data_Edit.FormMode)mode;
                    sIChildForm.strFormMode = (mode.ToString()).ToLower();
                    sIChildForm.strCaller = frmCaller;
                    sIChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        sIChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        sIChildForm.intRecordCount = intCount;
                    }
                    sIChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager6 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    sIChildForm.splashScreenManager = splashScreenManager6;
                    sIChildForm.splashScreenManager.ShowWaitForm();
                    sIChildForm.Show();
                    break;
                #endregion

                #region Cover

                case 14:     // Cover
                     view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
              
                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Cover Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11075_Cover_ItemRow)(dr)).CoverID).ToString() + ','; 
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Cover Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    cancelChildLoad = false;
                    gcRef = "";    
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 || iDistinctSelectedType == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add cover details.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1 )//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the cover details";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }                            
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }
                        
                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();  
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add cover details, otherwise right click for block adding cover details.", "Add Cover Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();        
                        }            
                    }

                    frm_AS_Cover_Edit cChildForm = new frm_AS_Cover_Edit();
                    cChildForm.MdiParent = this.MdiParent;
                    cChildForm.GlobalSettings = this.GlobalSettings;
                    cChildForm.strRecordIDs = strRecordsToLoad;
                    cChildForm.strEquipmentIDs = strRecordIDs;
                    cChildForm.formMode = (frm_AS_Cover_Edit.FormMode)mode;
                    cChildForm.strFormMode = (mode.ToString()).ToLower();
                    cChildForm.strCaller = frmCaller;
                    cChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        cChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        cChildForm.intRecordCount = intCount;
                    }
                    cChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager7 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    cChildForm.splashScreenManager = splashScreenManager7;
                    cChildForm.splashScreenManager.ShowWaitForm();
                    cChildForm.Show();
                    break;
                #endregion

                #region Work Detail

                case 15:     // Work Detail
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
              
                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Work Order Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11078_Work_Detail_ItemRow)(dr)).WorkDetailID).ToString() + ','; 
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Work Order Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                                        
                    cancelChildLoad = false;
                    gcRef = "";    
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 )
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Work Orders.", "Add Work Order Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1 )//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the Work Orders";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }
                            
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }
                        
                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();  
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Work Orders, otherwise right click for block adding Work Orders.", "Add Work Order Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();        
                        }            
                    }

                    frm_AS_WorkDetail_Edit wChildForm = new frm_AS_WorkDetail_Edit();
                    wChildForm.MdiParent = this.MdiParent;
                    wChildForm.GlobalSettings = this.GlobalSettings;
                    wChildForm.strRecordIDs = strRecordsToLoad;
                    wChildForm.strEquipmentIDs = strRecordIDs;
                    wChildForm.formMode = (frm_AS_WorkDetail_Edit.FormMode)mode;
                    wChildForm.strFormMode = (mode.ToString()).ToLower();
                    wChildForm.strCaller = frmCaller;
                    wChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        wChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        wChildForm.intRecordCount = intCount;
                    }
                    wChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager8 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    wChildForm.splashScreenManager = splashScreenManager8;
                    wChildForm.splashScreenManager.ShowWaitForm();
                    wChildForm.Show();
                    break;

                #endregion

                #region Purpose

                case 16:     // Purpose
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Equipment Purpose Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11084_Purpose_ItemRow)(dr)).EquipmentPurposeID).ToString() + ','; 
                        }
                    }

                    //if (mode == FormMode.blockedit)
                    //{
                    //    XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Equipment Purpose Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //    return;
                    //}
                                      
                    cancelChildLoad = false;
                    gcRef = "";    
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 )
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Equipment Purpose.", "Add Equipment Purpose Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1 )//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the equipment purpose?";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }
                            
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }
                        
                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();  
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Equipment Purpose records, otherwise right click for block adding Equipment Purpose records.", "Add Equipment Purpose Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();        
                        }            
                    }

                    frm_AS_Purpose_Edit pChildForm = new frm_AS_Purpose_Edit();
                    pChildForm.MdiParent = this.MdiParent;
                    pChildForm.GlobalSettings = this.GlobalSettings;
                    pChildForm.strRecordIDs = strRecordsToLoad;
                    pChildForm.strEquipmentIDs = strRecordIDs;
                    pChildForm.formMode = (frm_AS_Purpose_Edit.FormMode)mode;
                    pChildForm.strFormMode = (mode.ToString()).ToLower();
                    pChildForm.strCaller = frmCaller;
                    pChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        pChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        pChildForm.intRecordCount = intCount;
                    }
                    pChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager9 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    pChildForm.splashScreenManager = splashScreenManager9;
                    pChildForm.splashScreenManager.ShowWaitForm();
                    pChildForm.Show();
                    break;
                #endregion

                #region Fuel Card

                case 17:     // Fuel Card
                     view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
              
                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_ItemRow)(dr)).FuelCardID).ToString() + ','; 
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    if (mode == FormMode.blockadd)
                    {
                        XtraMessageBox.Show("Blockadd is restricted.", "Blockadd Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (mode == FormMode.add )
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate Fuel Cards before proceeding.", "Add Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";    
                    if (mode == FormMode.add )
                    {
                        if (numOfSelectedRows == 0 )
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Fuel Card allocations.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1 )//block add option
                        {
                            string strTypes = "You have selected multiple records, please select one equipment to add the Fuel Cards";
                            XtraMessageBox.Show(strTypes, "Block Add Restricted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            cancelChildLoad = true;
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        int equipType = 0;
                        string equipID = "";

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();  

                            //restrict  addition to plant and vehicle

                            equipType = Convert.ToInt32(((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentCategoryID);
                            if (equipType != 1 && equipType != 2)
                            {
                                XtraMessageBox.Show("Fuel Card allocations are restricted to Plant and Vehicles.", "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }


                            equipID = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString();
                            sp_AS_11044_Keeper_Allocation_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List, equipID, "view");
                            if (this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.Count < 1)
                            {
                                XtraMessageBox.Show(("Please add Keepers to Equipment Reference "+ gcRef+" before you proceed to add Fuel Cards"), "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Fuel Cards, otherwise right click for block adding Fuel Cards.", "Add Fuel Card Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();        
                        }            
                    }

                    frm_AS_FuelCard_Edit fcChildForm = new frm_AS_FuelCard_Edit();
                    fcChildForm.MdiParent = this.MdiParent;
                    fcChildForm.GlobalSettings = this.GlobalSettings;
                    fcChildForm.strRecordIDs = strRecordsToLoad;
                    fcChildForm.strEquipmentIDs = strRecordIDs;
                    fcChildForm.formMode = (frm_AS_FuelCard_Edit.FormMode)mode;
                    fcChildForm.strFormMode = (mode.ToString()).ToLower();
                    fcChildForm.strCaller = frmCaller;
                    fcChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        fcChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        fcChildForm.intRecordCount = intCount;
                    }
                    fcChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager10 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fcChildForm.splashScreenManager = splashScreenManager10;
                    fcChildForm.splashScreenManager.ShowWaitForm();
                    fcChildForm.Show();
                    break;
                #endregion

                #region Road Tax

                case 3:     //  Road Tax
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit ||  mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Road Tax Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11096_Road_Tax_ItemRow)(dr)).RoadTaxID).ToString() + ',';
                        }
                    }
                                     
                    
                //if (mode == FormMode.blockadd)
                    //{
                    //    XtraMessageBox.Show("Blockadd is restricted.", "Blockadd Road Tax Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //    return;
                    //}
                    if (mode == FormMode.add)
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate Road Tax before proceeding.", "Add Road Tax Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Road Tax.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the road tax ?";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        int equipType = 0;
                        string equipID = "";

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();

                            //restrict  addition to plant and vehicle

                            equipType = Convert.ToInt32(((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentCategoryID);
                            if (equipType != 1 && equipType != 2)
                            {
                                XtraMessageBox.Show("Road Tax allocations are restricted to Plant and Vehicles.", "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }


                            equipID = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString();
                            //sp_AS_11044_Keeper_Allocation_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List, equipID, "view");
                            //if (this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.Count < 1)
                            //{
                            //    XtraMessageBox.Show(("Please add Keepers to Equipment Reference " + gcRef + " before you proceed to add Road Taxs"), "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            //    return;
                            //}
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Road Tax, otherwise right click for block adding Road Tax.", "Add Road Tax Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();
                        }
                    }

                    frm_AS_Road_Tax_Edit rtChildForm = new frm_AS_Road_Tax_Edit();
                    rtChildForm.MdiParent = this.MdiParent;
                    rtChildForm.GlobalSettings = this.GlobalSettings;
                    rtChildForm.strRecordIDs = strRecordsToLoad;
                    rtChildForm.strEquipmentIDs = strRecordIDs;
                    rtChildForm.formMode = (frm_AS_Road_Tax_Edit.FormMode)mode;
                    rtChildForm.strFormMode = (mode.ToString()).ToLower();
                    rtChildForm.strCaller = frmCaller;
                    rtChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        rtChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        rtChildForm.intRecordCount = intCount;
                    }
                    rtChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager11 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    rtChildForm.splashScreenManager = splashScreenManager11;
                    rtChildForm.splashScreenManager.ShowWaitForm();
                    rtChildForm.Show();
                    break;
                #endregion

                #region Tracker Details

                case 4:     //  Tracker
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Verilocation Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11023_Tracker_ListRow)(dr)).TrackerInformationID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Verilocation Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    if (mode == FormMode.blockadd)
                    {
                        XtraMessageBox.Show("Blockadd is restricted.", "Blockadd Verilocation Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (mode == FormMode.add)
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate Tracker before proceeding.", "Add Tracker(Verilocation) Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Tracker.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, block add is restricted";
                            XtraMessageBox.Show(strTypes, "Block Add Restricted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            cancelChildLoad = true;
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        int equipType = 0;
                        string equipID = "";

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();

                            //restrict  addition to plant and vehicle

                            equipType = Convert.ToInt32(((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentCategoryID);
                            if (equipType != 1 && equipType != 2)
                            {
                                XtraMessageBox.Show("Verilocation Tracker allocations are restricted to Plant and Vehicles.", "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }

                            equipID = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString();                            
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Verilocation tracker.", "Add Verilocation Tracker Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();
                        }
                    }

                    //frm_AS_Tracker_Edit trChildForm = new frm_AS_Tracker_Edit();
                    //trChildForm.MdiParent = this.MdiParent;
                    //trChildForm.GlobalSettings = this.GlobalSettings;
                    //trChildForm.strRecordIDs = strRecordsToLoad;
                    //trChildForm.strEquipmentIDs = strRecordIDs;
                    //trChildForm.formMode = (frm_AS_Tracker_Edit.FormMode)mode;
                    //trChildForm.strFormMode = (mode.ToString()).ToLower();
                    //trChildForm.strCaller = frmCaller;
                    //trChildForm.strGCReference = gcRef;
                    //if (mode == FormMode.blockadd)
                    //{
                    //    trChildForm.intRecordCount = numOfSelectedRows;
                    //}
                    //else
                    //{
                    //    trChildForm.intRecordCount = intCount;
                    //}
                    //trChildForm.FormPermissions = this.FormPermissions;
                    //SplashScreenManager splashScreenManager12 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    //trChildForm.splashScreenManager = splashScreenManager12;
                    //trChildForm.splashScreenManager.ShowWaitForm();
                    //trChildForm.Show();
                    break;
                #endregion

                #region P11D

                case 20:     // P11D
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit P11D Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11166_P11D_ItemRow)(dr)).P11DID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit P11D Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 || iDistinctSelectedType == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add P11D details.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the P11D details";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();
                        }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add P11D details, otherwise right click for block adding P11D details.", "Add P11D Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                        foreach (int intRowHandle in intEquipRowhandles)
                        {
                            DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                            gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();
                        }
                    }

                    frm_AS_P11D_Edit p11ChildForm = new frm_AS_P11D_Edit();
                    p11ChildForm.MdiParent = this.MdiParent;
                    p11ChildForm.GlobalSettings = this.GlobalSettings;
                    p11ChildForm.strRecordIDs = strRecordsToLoad;
                    p11ChildForm.strEquipmentIDs = strRecordIDs;
                    p11ChildForm.formMode = (frm_AS_P11D_Edit.FormMode)mode;
                    p11ChildForm.strFormMode = (mode.ToString()).ToLower();
                    p11ChildForm.strCaller = frmCaller;
                    p11ChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        p11ChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        p11ChildForm.intRecordCount = intCount;
                    }
                    p11ChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManagerp11 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    p11ChildForm.splashScreenManager = splashScreenManagerp11;
                    p11ChildForm.splashScreenManager.ShowWaitForm();
                    p11ChildForm.Show();
                    break;
                #endregion


                default:
                    MessageBox.Show("Failed to load edit form", "Error Loading Edit form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private string getVehicleReg(int ID)
        {
            string reg = "";
            DataRow[] vehDR = this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Select();

            int i = 0;
            foreach (DataRow row in vehDR)
            {
                if (Convert.ToInt32((vehDR[i]).ItemArray[0]) == ID)
                {
                    reg = (vehDR[i]).ItemArray[2].ToString();
                    return reg;
                }
                i++;
            }
            return reg;
        }

        private void saveGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStateEquipment.SaveViewInfo();  
            this.RefreshGridViewStateVehicle.SaveViewInfo(); 
            this.RefreshGridViewStatePlant.SaveViewInfo();
            this.RefreshGridViewStateRoadTax.SaveViewInfo();
            this.RefreshGridViewStateSoftware.SaveViewInfo();
            //this.RefreshGridViewStateTracker.SaveViewInfo();
            this.RefreshGridViewStateOffice.SaveViewInfo();
            this.RefreshGridViewStateBilling.SaveViewInfo();
            this.RefreshGridViewStateKeeper.SaveViewInfo();
            this.RefreshGridViewStateTransactions.SaveViewInfo();
            this.RefreshGridViewStateDepreciation.SaveViewInfo();
            this.RefreshGridViewStateCover.SaveViewInfo();
            this.RefreshGridViewStateServiceData.SaveViewInfo();
            this.RefreshGridViewStateWork.SaveViewInfo();
            this.RefreshGridViewStateIncident.SaveViewInfo();
            this.RefreshGridViewStatePurpose.SaveViewInfo();
            this.RefreshGridViewStateFuelCard.SaveViewInfo();
            //this.RefreshGridViewStateSpeeding.SaveViewInfo();
            this.RefreshGridViewStateNotification.SaveViewInfo();
            this.RefreshGridViewStateP11D.SaveViewInfo();
            this.RefreshGridViewStateLinkedDocs.SaveViewInfo();
        }

        private void loadGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStateEquipment.LoadViewInfo();
            this.RefreshGridViewStateVehicle.LoadViewInfo();
            this.RefreshGridViewStatePlant.LoadViewInfo();
            this.RefreshGridViewStateRoadTax.LoadViewInfo();
            this.RefreshGridViewStateSoftware.LoadViewInfo();
            //this.RefreshGridViewStateTracker.LoadViewInfo();
            this.RefreshGridViewStateOffice.LoadViewInfo();
            this.RefreshGridViewStateBilling.LoadViewInfo();
            this.RefreshGridViewStateKeeper.LoadViewInfo();
            this.RefreshGridViewStateTransactions.LoadViewInfo();
            this.RefreshGridViewStateDepreciation.LoadViewInfo();
            this.RefreshGridViewStateCover.LoadViewInfo();
            this.RefreshGridViewStateServiceData.LoadViewInfo();
            this.RefreshGridViewStateWork.LoadViewInfo();
            this.RefreshGridViewStateIncident.LoadViewInfo();
            this.RefreshGridViewStatePurpose.LoadViewInfo();
            this.RefreshGridViewStateFuelCard.LoadViewInfo();
            //this.RefreshGridViewStateSpeeding.LoadViewInfo();
            this.RefreshGridViewStateNotification.LoadViewInfo();
            this.RefreshGridViewStateP11D.LoadViewInfo();
            this.RefreshGridViewStateLinkedDocs.LoadViewInfo();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInRecordDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                //LoadLastSavedUserScreenSettings();
            }
            loadAlertControl();
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }
     
        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0)
                UpdateRefreshStatus = 0;
            equipmentGridControl.BeginUpdate();
            LoadAdapters();
            //this.RefreshGridViewStateEquipment.LoadViewInfo();  // Reload any expanded groups and selected rows //
            loadGridViewState();
            equipmentGridControl.EndUpdate();

            // Highlight any recently added Equipment new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                SetGridViewSelection((GridView)equipmentGridControl.MainView, "EquipmentID", i_str_AddedRecordIDs1);

                i_str_AddedRecordIDs1 = "";
            }
            if (i_str_AddedRecordIDs2 != "")
            {
                highlightChildrow();
            }           
        }

        private void SetGridViewSelection(GridView view, string columnName,string recordIDs)
        {
            string[] strArray = splitStrRecords(recordIDs);
            int intID = 0;
            int intRowHandle = 0;
   
            view.ClearSelection(); // Clear any current selection so just the required rows are selected //
            
            //Begin/End Selectoin Block improves performance.
            view.BeginSelection();
            foreach (string strElement in strArray)
            {
                intID = Convert.ToInt32(strElement);
                intRowHandle = view.LocateByValue(0, view.Columns[columnName], intID);
                if (intRowHandle != GridControl.InvalidRowHandle)
                {
                    view.MakeRowVisible(intRowHandle, false);
                    view.SelectRow(intRowHandle);
                }
            }
            view.EndSelection();
        }

        private void highlightChildrow()
        {
            GridView view = null;
            string viewID = "";
            switch (i_int_FocusedGrid)
            {
                case 1://vehicles
                    // Highlight any recently added vehicle new rows //
                    view = (GridView)vehicleGridControl.MainView;
                    viewID = "EquipmentID";
                    break;                    
                case 2://plant
                    // Highlight any recently added plant new rows //
                    view = (GridView)plantGridControl.MainView;
                    viewID = "EquipmentID";
                    break;
                case 3://Road Tax
                    // Highlight any recently added Road Tax new rows //
                    view = (GridView)roadTaxGridControl.MainView;
                    viewID = "RoadTaxID";
                    break;
                //case 4://Tracker
                //    // Highlight any recently added Tracker new rows //
                //    view = (GridView)trackerGridControl.MainView;
                //    viewID = "TrackerInformationID";
                //    break;
                case 5://software
                    // Highlight any recently added software new rows //
                    view = (GridView)softwareGridControl.MainView;
                    viewID = "EquipmentID";
                    break;
                case 6://office
                    // Highlight any recently added office new rows //
                    view = (GridView)officeGridControl.MainView;
                    viewID = "EquipmentID";
                    break;
                case 7://billing
                    // Highlight any recently added billing new rows //
                    view = (GridView)billingGridControl.MainView;
                    viewID = "DepreciationID";
                    break;
                case 8://keeper
                    // Highlight any recently added keepers new rows //
                    view = (GridView)keeperGridControl.MainView;
                    viewID = "KeeperAllocationID";
                    break;
                case 9://transactions
                    // Highlight any recently added transaction new rows //
                    view = (GridView)transactionGridControl.MainView;
                    viewID = "TransactionID";
                    break;
                case 10://notification
                    // Highlight any recently added notifications new rows //
                    view = (GridView)notificationGridControl.MainView;
                    viewID = "NotificationID";
                    break;
                case 11://depreciation
                    // Highlight any recently added depreciation new rows //
                    view = (GridView)depreciationGridControl.MainView;
                    viewID = "DepreciationID";
                    break;
                case 12://Incident
                    // Highlight any recently added Incident new rows //
                    view = (GridView)incidentGridControl.MainView;
                    viewID = "IncidentID";
                    break;
                case 13://Service Interval
                    // Highlight any recently added ServiceData new rows //
                    view = (GridView)serviceDataGridControl.MainView;
                    viewID = "ServiceDataID";
                    break;
                case 14://Cover
                    // Highlight any recently added Cover new rows //
                    view = (GridView)coverGridControl.MainView;
                    viewID = "CoverID";
                    break;
                case 15://work
                    // Highlight any recently added workService new rows //
                    view = (GridView)workDetailGridControl.MainView;
                    viewID = "WorkDetailID";
                    break;
                case 16://Purpose
                    // Highlight any recently added Purpose new rows //
                    view = (GridView)purposeGridControl.MainView;
                    viewID = "EquipmentPurposeID";
                    break;
                case 17://Fuel Cards
                    // Highlight any recently added Fuel Cards new rows //
                    view = (GridView)fuelCardGridControl.MainView;
                    viewID = "FuelCardID";
                    break;
                //case 18://Speeding
                //    // Highlight any recently added Speeding new rows //
                //    view = (GridView)speedingGridControl.MainView;
                //    viewID = "SpeedingID";
                //    break;
                case 19://P11D
                    // Highlight any recently added Speeding new rows //
                    view = (GridView)p11dGridControl.MainView;
                    viewID = "P11DID";
                    break;
                case 99://Linked Docs
                    view = (GridView)gridControlLinkedDocs.MainView;
                    viewID = "gridViewLinkedDocs";
                    break;
            }
            if (i_str_AddedRecordIDs3 != "")//handles equipment grid edits
            {
                switch (i_str_AddedRecordIDs3)
                {
                    case "1"://vehicles
                        // Highlight any recently added vehicle new rows //
                        view = (GridView)vehicleGridControl.MainView;
                        viewID = "EquipmentID";
                        break;
                    case "2"://plant
                        // Highlight any recently added plant new rows //
                        view = (GridView)plantGridControl.MainView;
                        viewID = "EquipmentID";
                        break;                   
               
                    case "5"://software
                        // Highlight any recently added software new rows //
                        view = (GridView)softwareGridControl.MainView;
                        viewID = "EquipmentID";
                        break;
                    case "6"://office
                        // Highlight any recently added office new rows //
                        view = (GridView)officeGridControl.MainView;
                        viewID = "EquipmentID";
                        break;                  
                }
            }
            if (i_str_AddedRecordIDs2 != "")
            {
                SetGridViewSelection(view, viewID, i_str_AddedRecordIDs2);

                i_str_AddedRecordIDs2 = "";
            }
            
        }
        
        #endregion
        
        
        #region CRUD

        #region CRUD Methods
           
        private void Add_Record()
        {
           
            switch (i_int_FocusedGrid)
            {
                case 0:     // Equipment //
                    if (!iBool_AllowAdd)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", equipmentGridControl,EquipmentType.None);
                    break;
                case 1:     // vehicle //
                    if (!iBool_AllowAddVehicle) 
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", vehicleGridControl,EquipmentType.Vehicle);                  
                    break;
                case 2:     // plant //
                    if (!iBool_AllowAddPlant) 
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", plantGridControl, EquipmentType.Plant );
                    break;
                case 3:     // Road Tax //
                    if (!iBool_AllowAddRoadTax)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", roadTaxGridControl, EquipmentType.None);
                    break;
                //case 4:     // Tracker //
                //    if (!iBool_AllowAddTracker)
                //        return;
                //    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", trackerGridControl,EquipmentType.None);
                //    break;
                case 5:     // software //
                    if (!iBool_AllowAddSoftware)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", softwareGridControl,EquipmentType.Software);
                    break;
                case 6:     // office //
                    if (!iBool_AllowAddOffice)
                        return;
                    OpenEditForm(FormMode.add,"frm_AS_Fleet_Manager", officeGridControl,EquipmentType.Office);
                    break;
                case 7:     // Billings //
                    if (!iBool_AllowAddBilling)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowAddKeeper)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowAddTransactions)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", transactionGridControl, EquipmentType.None);
                    break;
                case 10:     // notifications //
                    if (!iBool_AllowAddNotifications)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", notificationGridControl, EquipmentType.None);
                    break;
                case 11:     // depreciation //
                    if (!iBool_AllowAddDepreciation)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // incident //
                    if (!iBool_AllowAddIncident)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceData //
                    if (!iBool_AllowAddServiceData)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", serviceDataGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowAddCover)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // work //
                    if (!iBool_AllowAddWorkDetail)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowAddPurpose)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowAddFuelCard)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", fuelCardGridControl, EquipmentType.None);
                    break;
                //case 18:     // Speeding //
                //    if (!iBool_AllowAddSpeeding)
                //        return;
                //    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", speedingGridControl, EquipmentType.None);
                //    break;
                case 19:     // P11D //
                    if (!iBool_AllowAddP11D)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", p11dGridControl, EquipmentType.None);
                    break;
                case 99:     // Licked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        GridView view = (GridView)gridControlLinkedDocs.MainView;
                        view.PostEditor();
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 200;  // Linked Docs //

                        GridView ParentView = (GridView)equipmentGridControl.MainView;
                        int[] intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EquipmentID"));
                            fChildForm2.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "EquipmentReference"));
                        }
                        else
                        {
                            fProgress.Close();
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to add a Linked Document to by clicking on it then try again.\n\nIf you wish to add a Linked Document to more than one record, select the records to add to then select Block Add from the right click menu.", "Add Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fChildForm2.Show();

                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Insert_Record()
        {
            //switch (i_int_FocusedGrid)
            //{
            //    case 18:
            //           if (!iBool_AllowAddIncident)
            //                        return;

            //               string autoInsertIncident = "Do you want to auto add selected speeding record(s) into the incident register?";
            //               switch (XtraMessageBox.Show(autoInsertIncident, "Auto Insert Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
            //               {
            //                   case DialogResult.No:
            //                       // cancel operation //
            //                       XtraMessageBox.Show("You can manually enter the incident in the Incident Form", "Opening Incident Form", MessageBoxButtons.OK);
            //                       OpenEditForm(FormMode.add, "frm_AS_Fleet_Manager", incidentGridControl, EquipmentType.None);
            //                       break;
            //                   case DialogResult.Yes:
            //                       DateTime dateHappened;
            //                       string district,notes;
            //                       int equipID;

            //                       this.sp_AS_11000_PL_Incident_TypeTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Type,31);
            //                       this.sp_AS_11000_PL_Incident_StatusTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Status,30);
            //                       this.sp_AS_11000_PL_Legal_ActionTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Legal_Action,38);
            //                       this.sp_AS_11000_PL_SeverityTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Severity,36);
            //                       this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Repair_Due,32);
            //                       this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Keeper_At_Fault,37);
            //                       // insert record into incident table //
            //                     // DataRow[] speedingDR = this.dataSet_AS_DataEntry.sp_AS_11105_Speeding_Item.Select("SpeedingID  = '");

            //                       //int equipID = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11105_Over_Speeding_ItemRow)(speedingDR[0])).EquipmentID;

            //                       int incStatusID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Status.Select("Value = 'Open'"))[0]["PickListID"]);
            //                       int incTypeID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Type.Select("Value = 'Speeding'"))[0]["PickListID"]);
            //                       int incLegalActionID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Legal_Action.Select("Value = 'None'"))[0]["PickListID"]);
            //                       int incSeverityID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Severity.Select("Value = 'High'"))[0]["PickListID"]);
            //                       int incKeeper_At_FaultID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Keeper_At_Fault.Select("Value = 'No'"))[0]["PickListID"]);
            //                       int incRepairDueID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Repair_Due.Select("Value = 'None'"))[0]["PickListID"]);
            //                       //-->insert record
            //                        GridView view = null;
            
            //                        int[] intRowHandles;
            //                        int intCount = 0;
            //                        int rowInsertCount = 0;
            //                        view = (GridView)speedingGridControl.MainView;
            //                        view.PostEditor();
            //                        intRowHandles = view.GetSelectedRows();
            //                        intCount = intRowHandles.Length;
            //                        string strUpdatedEquipID = "";
            //                        foreach (int intRowHandle in intRowHandles)
            //                        {
            //                            DataRow dr = view.GetDataRow(intRowHandle);
            //                            district = ((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).District;
            //                            dateHappened = ((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).TravelTime;
            //                            equipID = ((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).EquipmentID;
            //                            strUpdatedEquipID += equipID + ";";
            //                            notes = "Speed of " + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).SpeedMPH).ToString() + " MPH recorded on Road Type "
            //                                + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).RoadType).ToString() + " at location "
            //                                + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).PlaceName).ToString() + " in the "
            //                                + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).District).ToString() + " area.";
            //                            this.sp_AS_11081_Incident_ItemTableAdapter.Insert("add", "", equipID, 0, incStatusID, incTypeID, "VERILOCATION SPEEDING", dateHappened, district, incRepairDueID, incSeverityID, "Verilocation", incKeeper_At_FaultID, incLegalActionID, dateHappened.AddDays(7), 0, notes);

            //                            rowInsertCount++;
            //                        }

            //                        i_str_AddedRecordIDs1 = strUpdatedEquipID;
                                            
            //                        frmActivated();          
            //                        XtraMessageBox.Show(rowInsertCount + " Speeding incidents added.","Auto Update Complete", MessageBoxButtons.OK);
                                   

            //                       break;
            //               }       
            //           break;
            //}
        }
          
        private void Block_Add()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Equipment //     
                case 1:     // vehicle //
                case 2:     // plant //
               // case 3:     // gadget //
                //case 4:     // hardware //
                case 5:     // software //
                case 6:     // office //                       
                    XtraMessageBox.Show("Block adding is restricted.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    break;
                case 7:     // Billings //
                    if (!iBool_AllowAddBilling)
                        return;
                    OpenEditForm(FormMode.blockadd , "frm_AS_Fleet_Manager", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowAddKeeper)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowAddTransactions)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", transactionGridControl, EquipmentType.None);
                    break;
                case 10:     // notifications //
                    if (!iBool_AllowAddNotifications)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", notificationGridControl, EquipmentType.None);
                    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowAddDepreciation)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowAddIncident)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceData //
                    if (!iBool_AllowAddServiceData)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", serviceDataGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowAddCover)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowAddWorkDetail)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowAddPurpose)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowAddFuelCard)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", fuelCardGridControl, EquipmentType.None);
                    break;
                case 3:     // Road Tax //
                    if (!iBool_AllowAddRoadTax)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", roadTaxGridControl, EquipmentType.None);
                    break;
                //case 4:     // Tracker Information //
                //    if (!iBool_AllowAddTracker)
                //        return;
                //    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", trackerGridControl, EquipmentType.None);
                //    break;
                //case 18:     // speeding //
                //    if (!iBool_AllowAddSpeeding)
                //        return;
                //    OpenEditForm(FormMode.blockadd, "frm_AS_Fleet_Manager", speedingGridControl, EquipmentType.None);
                //    break;
                case 19:     // P11D //
                    if (!iBool_AllowAddP11D)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Equipment_Manager", p11dGridControl, EquipmentType.None);
                    break;
                case 99:     // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;
                        GridView view = (GridView)equipmentGridControl.MainView;
                        view.PostEditor();
                        int[] intRowHandles = view.GetSelectedRows();
                        int intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientID")) + ',';
                        }
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 200;  // Linked Docs //
                        fChildForm2.Show();

                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Equipment //
                    if (!iBool_AllowEdit)
                        return;
                    EquipmentType distinctType;
                    
                    distinctType = (EquipmentType)iDistinctSelectedType;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", equipmentGridControl, distinctType);
                    break;
                case 1:     // vehicle //
                    if (!iBool_AllowEditVehicle)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", vehicleGridControl, EquipmentType.Vehicle);
                    break;
                case 2:     // plant //
                    if (!iBool_AllowEditPlant)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", plantGridControl, EquipmentType.Plant);
                    break;
                case 3:     // Road Tax //
                    if (!iBool_AllowEditRoadTax)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", roadTaxGridControl, EquipmentType.None);
                    break;
                //case 4:     // Tracker //
                //    if (!iBool_AllowEditTracker)
                //        return;
                //    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", trackerGridControl, EquipmentType.None);
                //    break;
                case 5:     // software //
                    if (!iBool_AllowEditSoftware)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", softwareGridControl, EquipmentType.Software);
                    break;
                case 6:     // office //
                    if (!iBool_AllowEditOffice)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", officeGridControl, EquipmentType.Office);
                    break;
                case 7:     // Billings //
                    if (!iBool_AllowEditBilling)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowEditKeeper)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowEditTransactions)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", transactionGridControl, EquipmentType.None);
                    break;
                case 10:     // Notifications //
                    if (!iBool_AllowEditNotifications)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", notificationGridControl, EquipmentType.None);
                    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowEditDepreciation)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowEditIncident)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceData //
                    if (!iBool_AllowEditServiceData)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", serviceDataGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowEditCover)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowEditWorkDetail)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowEditPurpose)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowEditFuelCard)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", fuelCardGridControl, EquipmentType.None);
                    break;
                //case 18:     // Speeding //
                //    if (!iBool_AllowEditSpeeding)
                //        return;
                //    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", speedingGridControl, EquipmentType.None);
                //    break;
                case 19:     // P11D //
                    if (!iBool_AllowEditP11D)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Fleet_Manager", p11dGridControl, EquipmentType.None);
                    break;
                case 99:     // Licked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        GridView view = (GridView)gridControlLinkedDocs.MainView;
                        view.PostEditor();
                        int[] intRowHandles = view.GetSelectedRows();
                        int intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 200;  // Linked Docs //
                        fChildForm2.Show();

                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Equipment //
                    if (!iBool_AllowEdit)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", equipmentGridControl, EquipmentType.None);
                    break;
                case 1:     // vehicle //
                    if (!iBool_AllowEditVehicle)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", vehicleGridControl, EquipmentType.Vehicle);
                    break;
                case 2:     // plant //
                    if (!iBool_AllowEditPlant)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", plantGridControl, EquipmentType.Plant);
                    break;
                case 3:     // Road Tax //
                    if (!iBool_AllowEditRoadTax)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", roadTaxGridControl, EquipmentType.None);
                    break;
                //case 4:     // Tracker //
                //    if (!iBool_AllowEditTracker)
                //        return;
                //    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", trackerGridControl, EquipmentType.None);
                //    break;
                case 5:     // software //
                    if (!iBool_AllowEditSoftware)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", softwareGridControl, EquipmentType.Software);
                    break;
                case 6:     // office //
                    if (!iBool_AllowEditOffice)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", officeGridControl, EquipmentType.Office);
                    break;
                case 7:     // Billings //
                    if (!iBool_AllowEditBilling)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowEditKeeper)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowEditTransactions)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", transactionGridControl, EquipmentType.None);
                    break;
                case 10:     // Notifications //
                    if (!iBool_AllowEditNotifications)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", notificationGridControl, EquipmentType.None);
                    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowEditDepreciation)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowEditIncident)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceData //
                    if (!iBool_AllowEditServiceData)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", serviceDataGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowEditCover)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowEditWorkDetail)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", workDetailGridControl , EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowEditPurpose)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowEditFuelCard)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", fuelCardGridControl, EquipmentType.None);
                    break;
                //case 18:     // speeding //
                //    if (!iBool_AllowEditSpeeding)
                //        return;
                //    OpenEditForm(FormMode.edit, "frm_AS_Fleet_Manager", speedingGridControl, EquipmentType.None);
                //    break;
                case 19:     // P11D //
                    if (!iBool_AllowEditP11D)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Equipment_Manager", p11dGridControl, EquipmentType.None);
                    break;
                case 99:     // Licked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        GridView view = (GridView)gridControlLinkedDocs.MainView;
                        view.PostEditor();
                        int[] intRowHandles = view.GetSelectedRows();
                        int intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 200;  // Linked Docs //
                        fChildForm2.Show();

                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
            
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridControl gridControl = null;
            GridView view = null;
            string strMessage = "";


            switch (i_int_FocusedGrid)
            {
                case 0:     // Equipment
                    if (!iBool_AllowDelete)
                        return;
                    break;
                case 1:     // Vehicle
                    if (!iBool_AllowDeleteVehicle)
                        return;
                    break;
                case 2:     // Plant 
                    if (!iBool_AllowDeletePlant)
                        return;
                    break;
                case 3:     // road tax
                    if (!iBool_AllowDeleteRoadTax)
                        return;
                    break;
                //case 4:     // Tracker 
                //    if (!iBool_AllowDeleteTracker)
                //        return;
                    break;
                case 5:     // Software 
                    if (!iBool_AllowDeleteSoftware)
                        return;
                    break;
                case 6:     // Office
                    if (!iBool_AllowDeleteOffice)
                        return;
                    break;
                case 7:     // Billing
                    if (!iBool_AllowDeleteBilling)
                        return;
                    break;
                case 8:     // Keeper
                    if (!iBool_AllowDeleteKeeper)
                        return;
                    break;
                case 9:     // Transaction
                    if (!iBool_AllowDeleteTransactions)
                        return;
                    break;
                case 10:     // notifications //
                    if (!iBool_AllowDeleteNotifications)
                        return;
                    break;
                case 11:     // Depreciation
                    if (!iBool_AllowDeleteDepreciation)
                        return;
                    break;
                case 12:     // Incident
                    if (!iBool_AllowDeleteIncident)
                        return;
                    break;
                case 13:     // ServiceData
                    if (!iBool_AllowDeleteServiceData)
                        return;
                    break;
                case 14:     // Cover
                    if (!iBool_AllowDeleteCover)
                        return;
                    break;
                case 15:     // Work
                    if (!iBool_AllowDeleteWorkDetail)
                        return;
                    break;
                case 16:     // Purpose
                    if (!iBool_AllowDeletePurpose)
                        return;
                    break;
                case 17:     // Fuel Cards
                    if (!iBool_AllowDeleteFuelCard)
                        return;
                    break;
                //case 18:     // Speeding
                //    if (!iBool_AllowDeleteSpeeding)
                //        return;
                //    break;
                case 19:     // P11D
                    if (!iBool_AllowDeleteP11D)
                        return;
                    break;
                case 99:     // Linked Docs
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlLinkedDocs.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    return;
                default:
                    if (!iBool_AllowDelete)
                        return;
                    break;
            }

            getCurrentGridControl(out gridControl, out strMessage1, out strMessage2, out strMessage3);

            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
                                        
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Equipment records to delete.", "No Equipment Records To Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? strMessage1 : Convert.ToString(intRowHandles.Length) + strMessage2) + 
            " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this record" : "these records") + 
            " will no longer be available for selection and any related records will also be deleted!";
            if (XtraMessageBox.Show(strMessage, "Permanently Delete Record(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                string strRecordsToLoad = "";
                string strChildLinkedRecordsToLoad = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    DataRow dr = view.GetDataRow(intRowHandle);
                    switch (i_int_FocusedGrid)
                    {
                        case 0:     // Equipment
                        case 1:     // Vehicle
                        case 2:     // Plant 
                        //case 3:     // Gadget 
                        //case 4:     // Hardware 
                        case 5:     // Software 
                        case 6:     // Office
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString() + ',';
                        break;
                        case 3:     // Road Tax
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11096_Road_Tax_ItemRow)(dr)).RoadTaxID).ToString() + ',';
                        break;
                        //case 4:     // tracker
                        //strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11023_Tracker_ListRow)(dr)).TrackerInformationID).ToString() + ',';
                        //break;
                        case 7:     // Billing
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11047_Equipment_Billing_ItemRow)(dr)).DepreciationID).ToString() + ',';
                        break;
                        case 8:     // Keeper
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dr)).KeeperAllocationID).ToString() + ',';
                        break;
                        case 9:     // Transaction
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11041_Transaction_ItemRow)(dr)).TransactionID).ToString() + ',';
                        break;
                        case 10:     // notifications //
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11126_Notification_ItemRow)(dr)).NotificationID).ToString() + ',';
                        break;
                        case 11:     // Depreciation
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11050_Depreciation_ItemRow)(dr)).DepreciationID).ToString() + ',';
                        break;
                        case 12:     // Incident
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)(dr)).IncidentID).ToString() + ',';
                        break;
                        case 13:     // ServiceData
                            //check if there is linked data 
                        DataRow[] drs = dataSet_AS_DataEntry.sp_AS_11054_Service_Data_Item.Select("ServiceDataID = " + (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceDataID).ToString());

                        if (drs.Length >= 2)
                        {
                            // delete service interval schedule
                            strChildLinkedRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceIntervalScheduleID).ToString() + ',';                           
                        }
          
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceDataID).ToString() + ',';
                        break;
                        case 14:     // Cover
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11075_Cover_ItemRow)(dr)).CoverID).ToString() + ',';
                        break;
                        case 15:     // WorkDetail
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11078_Work_Detail_ItemRow)(dr)).WorkDetailID).ToString() + ',';
                        break;
                        case 16:     // Purpose
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11084_Purpose_ItemRow)(dr)).EquipmentPurposeID).ToString() + ',';
                        break;
                        case 17:     // FuelCard
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_ItemRow)(dr)).FuelCardID).ToString() + ',';
                        break;
                        //case 18:     // speeding
                        //strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).SpeedingID).ToString() + ',';
                        //break;
                        case 19:     // P11D
                        strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11166_P11D_ItemRow)(dr)).P11DID).ToString() + ',';
                        break;
                        default:
                            strRecordsToLoad = "";
                            break;
                    }
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshGridViewStateEquipment.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshGridViewStateEquipment.SaveViewInfo();  // Store Grid View State //
                switch (i_int_FocusedGrid)
                {
                    case 0:     // Equipment
                    case 1:     // Vehicle
                    case 2:     // Plant 
                    //case 3:     // Gadget 
                   // case 4:     // Hardware 
                    case 5:     // Software 
                    case 6:     // Office
                    sp_AS_11002_Equipment_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    break;
                    case 3:     // Road Tax
                    sp_AS_11096_Road_Tax_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    break;
                    //case 4:     // Tracker
                    //sp_AS_11023_Tracker_ListTableAdapter.Delete("delete", strRecordsToLoad);
                    //break;
                    case 7:     // Billing
                    //sp_AS_11047_Equipment_Billing_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    break;
                    case 8:     // Keeper
                    sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    break;
                    case 9:     // Transaction
                    sp_AS_11041_Transaction_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    break;
                    case 10:     // Notification
                    sp_AS_11126_Notification_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    break;
                    case 11:     // Depreciation
                    sp_AS_11050_Depreciation_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    break;
                    case 12:     // Incident
                    sp_AS_11081_Incident_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    break;
                    case 13:     // Service Data
                    if (strChildLinkedRecordsToLoad != "")
                    {
                        sp_AS_11054_Service_Data_ItemTableAdapter.Delete("del_child", strChildLinkedRecordsToLoad);
                        UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    }
                    else if (strRecordsToLoad != "")
                    {
                        sp_AS_11054_Service_Data_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    }
                    break;
                    case 14:     // Cover
                    sp_AS_11075_Cover_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    break;
                    case 15:     // WorkDetail
                    sp_AS_11078_Work_Detail_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    break;
                    case 16:     // Purpose
                    sp_AS_11084_Purpose_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    break;
                    case 17:     // Fuel Card
                    sp_AS_11089_Fuel_Card_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    break;
                    //case 18:     // Speeding
                    //sp_AS_11105_Speeding_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    //break;
                    case 19:     // P11D
                    sp_AS_11166_P11D_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                    UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    break;
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                Load_Data();

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void getCurrentGridControl(out GridControl gridControl, out string strMessage1, out string strMessage2, out string strMessage3)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://equipment
                    gridControl = equipmentGridControl;                    
                    strMessage1 = "1 Equipment record";
                    strMessage2 = " Equipment records";
                    strMessage3 = "";
                    break;
                case 1://vehicle
                    gridControl = vehicleGridControl;                  
                    strMessage1 = "1 Vehicle record";
                    strMessage2 = " Vehicle records";
                    strMessage3 = "";
                    break;
                case 2://plant
                    gridControl = plantGridControl;
                    
                    strMessage1 = "1 Plant record";
                    strMessage2 = " Plant records";
                    strMessage3 = "";
                    break;
                case 3://road tax
                    gridControl = roadTaxGridControl;
                    strMessage1 = "1 Road Tax record";
                    strMessage2 = " Road Tax records";
                    strMessage3 = "";
                    break;
                //case 4://Tracker
                //    gridControl = trackerGridControl;                    
                //    strMessage1 = "1 Verilocation Tracker record";
                //    strMessage2 = " Verilocation Tracker records";
                //    strMessage3 = "";
                //    break;
                case 5://software
                    gridControl = softwareGridControl;                    
                    strMessage1 = "1 Software record";
                    strMessage2 = " Software records";
                    strMessage3 = "";
                    break;
                case 6://office
                    gridControl = officeGridControl;                    
                    strMessage1 = "1 Office record";
                    strMessage2 = " Office records";
                    strMessage3 = "";
                    break;
                case 7://billing 
                    gridControl = billingGridControl;
                    strMessage1 = "1 Billing record";
                    strMessage2 = " Billing records";
                    strMessage3 = "";
                    break;
                case 8://keeper
                    gridControl = keeperGridControl;                    
                    strMessage1 = "1 Keeper record";
                    strMessage2 = " Keeper records";
                    strMessage3 = "";
                    break;
                case 9://transaction
                    gridControl = transactionGridControl;                    
                    strMessage1 = "1 Transaction record";
                    strMessage2 = " Transaction records";
                    strMessage3 = "";
                    break;
                case 10://notification
                    gridControl = notificationGridControl;
                    strMessage1 = "1 Notification record";
                    strMessage2 = " Notification records";
                    strMessage3 = "";
                    break;
                case 11://depreciation
                    gridControl = depreciationGridControl;
                    strMessage1 = "1 Depreciation record";
                    strMessage2 = " Depreciation records";
                    strMessage3 = "";
                    break;
                case 12://Incident
                    gridControl = incidentGridControl;
                    strMessage1 = "1 Incident record";
                    strMessage2 = " Incident records";
                    strMessage3 = "";
                    break;
                case 13://Service Interval
                    gridControl = serviceDataGridControl;

                    strMessage1 = "1 Service Interval record";
                    strMessage2 = " Service Interval records";
                    strMessage3 = "";
                    break;
                case 14://Cover
                    gridControl = coverGridControl;

                    strMessage1 = "1 Cover record";
                    strMessage2 = " Cover records";
                    strMessage3 = "";
                    break;
                case 15://WorkDetail
                    gridControl = workDetailGridControl;

                    strMessage1 = "1 Work Order Detail record";
                    strMessage2 = " Work Order Detail records";
                    strMessage3 = "";
                    break;
                case 16://Purpose
                    gridControl = purposeGridControl;

                    strMessage1 = "1 Purpose record";
                    strMessage2 = " Purpose records";
                    strMessage3 = "";
                    break;
                case 17://Fuel Cards
                    gridControl = fuelCardGridControl;

                    strMessage1 = "1 Fuel Card record";
                    strMessage2 = " Fuel Card records";
                    strMessage3 = "";
                    break;
                //case 18://Speeding
                //    gridControl = speedingGridControl;

                //    strMessage1 = "1 Speeding record";
                //    strMessage2 = " Speeding records";
                //    strMessage3 = "";
                //    break;
                case 19://P11D
                    gridControl = p11dGridControl;

                    strMessage1 = "1 P11D record";
                    strMessage2 = " P11D records";
                    strMessage3 = "";
                    break;
                case 99://Linked Docs
                    gridControl = gridControlLinkedDocs;

                    strMessage1 = "1 Linked Document record";
                    strMessage2 = " Linked Document records";
                    strMessage3 = "";
                    break;
                default:
                    gridControl = equipmentGridControl;
                    strMessage1 = "1 Equipment record";
                    strMessage2 = " Equipment records";
                    strMessage3 = "";
                    break;
            }
        }

        private void getCurrentGridControl(out GridControl gridControl)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://equipment
                    gridControl = equipmentGridControl;
                    break;
                case 1://vehicle
                    gridControl = vehicleGridControl;
                    break;
                case 2://plant
                    gridControl = plantGridControl;
                    break;
                case 3://road Tax
                    gridControl = roadTaxGridControl;
                    break;
                //case 4://Tracker
                //    gridControl = trackerGridControl;
                //    break;
                case 5://software
                    gridControl = softwareGridControl;
                    break;
                case 6://office
                    gridControl = officeGridControl;
                    break;
                case 7://keeper
                    gridControl = keeperGridControl;
                    break;
                case 8://transaction
                    gridControl = transactionGridControl;
                    break;
                case 9://billing 
                    gridControl = billingGridControl;
                    break;
                case 10://notification 
                    gridControl = notificationGridControl;
                    break;
                case 11://depreciation
                    gridControl = depreciationGridControl;
                    break;
                case 12://incident
                    gridControl = incidentGridControl;
                    break;
                case 13://service Interval
                    gridControl = serviceDataGridControl;
                    break;
                case 14://Cover
                    gridControl = coverGridControl;
                    break;
                case 15://WorkDetail
                    gridControl = workDetailGridControl;
                    break;
                case 16://Purpose
                    gridControl = purposeGridControl;
                    break;
                case 17://Fuel Cards
                    gridControl = fuelCardGridControl;
                    break;
                //case 18://Speeding
                //    gridControl = speedingGridControl;
                //    break;
                case 19://P11D
                    gridControl = p11dGridControl;
                    break;
                case 99://Linked Docs
                    gridControl = gridControlLinkedDocs;
                    break;
                default:
                    gridControl = equipmentGridControl;
                    break;
            }
        }

       
 
        #endregion

        #region CRUD Events

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void View_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Equipment //
                    if (!iBool_AllowEdit)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", equipmentGridControl, EquipmentType.None);
                    break;
                case 1:     // vehicle //
                    if (!iBool_AllowEditVehicle)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", vehicleGridControl, EquipmentType.Vehicle);
                    break;
                case 2:     // plant //
                    if (!iBool_AllowEditPlant)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", plantGridControl, EquipmentType.Plant);
                    break;
                case 3:     // Road Tax //
                    if (!iBool_AllowEditRoadTax)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", roadTaxGridControl, EquipmentType.None);
                    break;
                //case 4:     // Tracker //
                //    if (!iBool_AllowEditTracker)
                //        return;
                //    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", trackerGridControl, EquipmentType.None);
                //    break;
                case 5:     // software //
                    if (!iBool_AllowEditSoftware)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", softwareGridControl, EquipmentType.Software);
                    break;
                case 6:     // office //
                    if (!iBool_AllowEditOffice)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", officeGridControl, EquipmentType.Office);
                    break;
                case 7:     // Billings //
                    if (!iBool_AllowEditBilling)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowEditKeeper)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowEditTransactions)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", transactionGridControl, EquipmentType.None);
                    break;
                case 10:     // notifications //
                    if (!iBool_AllowEditNotifications)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", notificationGridControl, EquipmentType.None);
                    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowEditDepreciation)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowEditIncident)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceData //
                    if (!iBool_AllowEditServiceData)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", serviceDataGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowEditCover)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowEditWorkDetail)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowEditPurpose)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowEditFuelCard)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", fuelCardGridControl, EquipmentType.None);
                    break;
                //case 18:     // speeding //
                //    if (!iBool_AllowEditSpeeding)
                //        return;
                //    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", speedingGridControl, EquipmentType.None);
                //    break;
                case 19:     // P11D //
                    if (!iBool_AllowEditP11D)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Fleet_Manager", p11dGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }
        }

        private void Linked_Documents_To_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 8:     // Keeper //
                    if (!iBool_AllowEditKeeper)
                        return;

                    // Drilldown to Linked Document Manager //
                    GridView view = keeperGridView;
                    int intRecordType = 201;  // Asset -  //
                    int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle,colKeeperAllocationID));

                    string startDate = "Unknown Date";
                    if (view.GetRowCellValue(view.FocusedRowHandle, colAllocationDate) != DBNull.Value)
                    {
                        startDate = (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, colAllocationDate)).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date");
                    }

                    string endDate = "Unknown Date";
                    if (view.GetRowCellValue(view.FocusedRowHandle, colAllocationEndDate) != DBNull.Value)
                    {
                        endDate = (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, colAllocationEndDate)).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date");
                    }

                    string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, colEquipmentReference).ToString()
                        + " :"
                        + view.GetRowCellValue(view.FocusedRowHandle, colKeeper).ToString()
                        + ", Duration: "
                        + startDate
                        + " - "
                        + endDate;
                    Linked_Document_Drill_Down(intRecordType, intRecordID, strRecordDescription);

                    break;
                default:
                    break;
            }
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";

            System.Reflection.MethodInfo method = null;

            var frmChild = new frm_AT_Linked_Document_Manager(this.FormID);
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            frmChild.splashScreenManager = splashScreenManager1;
            frmChild.splashScreenManager.ShowWaitForm();

            frmChild.MdiParent = this.MdiParent;

            frmChild.GlobalSettings = this.GlobalSettings;
            frmChild.strPassedInLinkedDocumentIDs = strRecordIDs;
            frmChild.intPassedInRecordTypeID = intRecordType;
            frmChild.intPassedInLinkedToRecordID = intRecordID;
            frmChild.strPassedInLinkedToRecord = strRecordDescription;
            frmChild.Show();

            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmChild, new object[] { null });

            frmChild.LinkedDocumentsUpdated += new System.EventHandler(this.LinkedDocumentsUpdated);
        }

        private void LinkedDocumentsUpdated(object sender, EventArgs e)
        {
            //Just want to refresh the Keeper Allocation Grid
            sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item, strRecordIDs, "view");
        }

        #endregion

        #endregion


        #region Equipment_Grid_Functions

        private void equipmentGridView_RowStyle(object sender, RowStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {
                string status = View.GetRowCellDisplayText(e.RowHandle, View.Columns["OwnershipStatus"]);

                if (status.ToLower() == "sold")
                {
                    e.Appearance.BackColor = Color.Salmon;
                    e.Appearance.BackColor2 = Color.SeaShell;
                    View.OptionsView.EnableAppearanceEvenRow = false;
                }
            }

        }
        
        private void equipmentView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
            GridView view = sender as GridView;
            numOfSelectedRows = view.SelectedRowsCount;


            int tempNumOfSelectedRows;
            if (numOfSelectedRows > 0)
            {
                tempNumOfSelectedRows = numOfSelectedRows;
            }
            else
            {
                tempNumOfSelectedRows = 1;
            }

            if (numOfSelectedRows > 0 && numOfSelectedRows < 26)
            {
                bbiUpdateSelectedEquipment.Enabled = true;
            }
            else
            {
                bbiUpdateSelectedEquipment.Enabled = false;
            }
            int[] equipTypes = new int[tempNumOfSelectedRows];
            int[] IDs = new int[tempNumOfSelectedRows];
            ArrayList distinctTypes = new ArrayList();
            NameValueCollection equipList = new NameValueCollection();

            int[] intRowHandles = view.GetSelectedRows();
            if (view.SelectedRowsCount > 0)
            {
                int countRows = 0;

                foreach (int intRowHandle in intRowHandles)
                {
                    DataRow dr = view.GetDataRow(intRowHandle);
                    equipTypes[countRows] = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentCategoryID);
                    IDs[countRows] = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID);
                    equipList.Add((((DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentCategoryID).ToString(), (((DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString());
                    countRows += 1;
                }
                foreach (int typeOfEquipment  in equipTypes)
                {
                    if (!distinctTypes.Contains(typeOfEquipment))
                    {
                        distinctTypes.Add(typeOfEquipment);
                        iDistinctSelectedType = typeOfEquipment;
                    }
                }
            }
            else
            {                    
                IDs[0] = 0;
                distinctTypes.Add(0);
            }

            strRecordIDs = stringRecords(IDs);
            ChildVisibility(IDs, equipList);
            LoadLinkedRecords();
            expandChildGridView();
            SetMenuStatus();
            if (distinctTypes.Count > 1)
            {
                iboolDistinctMulitpleChildSelected = true;
                iDistinctSelectedType = 0;
            }
            else
            {
                iboolDistinctMulitpleChildSelected = false;
            }

        }

        private void LoadLinkedRecords()
        {
            sp_AS_11096_Road_Tax_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11096_Road_Tax_Item, strRecordIDs, "view");
            sp_AS_11041_Transaction_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item, strRecordIDs, "view");
            sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item, strRecordIDs, "view");
            sp_AS_11047_Equipment_Billing_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11047_Equipment_Billing_Item, strRecordIDs, "view");
            sp_AS_11050_Depreciation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11050_Depreciation_Item, strRecordIDs, "view");
            sp_AS_11078_Work_Detail_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11078_Work_Detail_Item, strRecordIDs, "view");
            sp_AS_11075_Cover_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11075_Cover_Item, strRecordIDs, "view");
            try
            {
                sp_AS_11054_Service_Data_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11054_Service_Data_Item, strRecordIDs, "view");
            }
            catch (ConstraintException ex)
            {
                /***
                //ICE-62 : Remove this message as Users do not require to be aware of missing Service Interval data
                
                var dialogTypeName = "System.Windows.Forms.PropertyGridInternal.GridErrorDlg";
                var dialogType = typeof(Form).Assembly.GetType(dialogTypeName);
                var dialog = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());
                dialog.Text = "Error Load Service Data Information";
                dialogType.GetProperty("Details").SetValue(dialog, ex.Message, null);
                dialogType.GetProperty("Message").SetValue(dialog, "The Service Data record linked to this equipment has incomplete information. Please fix this by completing the record .", null);
                var result = dialog.ShowDialog();
                ***/
            }
            sp_AS_11081_Incident_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11081_Incident_Item, strRecordIDs, "view");
            sp_AS_11084_Purpose_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11084_Purpose_Item, strRecordIDs, "view");
            sp_AS_11089_Fuel_Card_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_Item, strRecordIDs, "view");
            //sp_AS_11023_Tracker_ListTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11023_Tracker_List, strRecordIDs, "view");
            //sp_AS_11105_Speeding_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11105_Speeding_Item, strRecordIDs, "view");
            sp_AS_11126_Notification_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11126_Notification_Item, strRecordIDs, "view");
            sp_AS_11166_P11D_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11166_P11D_Item, strRecordIDs, "view");
            sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, strRecordIDs, 200, strDefaultPath);
        }

        private string stringRecords(int[] IDs)
        {
            strRecordIDs = "";
            foreach (int rec in IDs)
            {
                strRecordIDs += Convert.ToString(rec) + ',';
            }
            return strRecordIDs;
        }

        private void expandChildGridView()
        {
            GridView view = (GridView)vehicleGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)plantGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)roadTaxGridControl.MainView;
            view.ExpandAllGroups();

            //view = (GridView)trackerGridControl.MainView;
            //view.ExpandAllGroups();

            //view = (GridView)speedingGridControl.MainView;
            //view.ExpandAllGroups();
            //view = (GridView)softwareGridControl.MainView;
            //view.ExpandAllGroups();

            //view = (GridView)officeGridControl.MainView;
            //view.ExpandAllGroups();

            view = (GridView)keeperGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)transactionGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)billingGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)incidentGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)serviceDataGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)workDetailGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)coverGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)depreciationGridControl.MainView;
            view.ExpandAllGroups();
            
            view = (GridView)purposeGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)fuelCardGridControl.MainView;
            view.ExpandAllGroups();
            
            view = (GridView)notificationGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)p11dGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)gridControlLinkedDocs.MainView;
            view.ExpandAllGroups();
        }

        private void ChildVisibility(int[] IDs, NameValueCollection equipList)
        {      

            HideAllChildPages();

            if (equipList["1"] != null)//vehicles
            {
                vehicleTabPage.PageVisible = true;
                sp_AS_11005_Vehicle_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item, strRecordIDs, "view");
            }

            if (equipList["2"] != null)//plant
            {
                plantTabPage.PageVisible = true;
                sp_AS_11008_Plant_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11008_Plant_Item, strRecordIDs, "view");
            }
        
            if (equipList["5"] != null)//software
            {
                softwareTabPage.PageVisible = true;
                sp_AS_11035_Software_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11035_Software_Item, strRecordIDs, "view");
            }

            if (equipList["6"] != null)//office
            {
                officeTabPage.PageVisible = true;
                sp_AS_11029_Office_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11029_Office_Item, strRecordIDs, "view");
            }

            if (vehicleTabPage.PageVisible == true || plantTabPage.PageVisible == true || softwareTabPage.PageVisible == true 
                || officeTabPage.PageVisible == true)
            {
                NoDetailsTabPage.PageVisible = false; 
            }
        }

        private void HideAllChildPages()
        {
            NoDetailsTabPage.PageVisible = true;
            vehicleTabPage.PageVisible = false;
            plantTabPage.PageVisible = false;
            softwareTabPage.PageVisible = false;
            officeTabPage.PageVisible = false;
        }

        private void equipmentView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void commonView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //
            GridView view = (GridView)sender;
            customFilterDraw(view, e);
        }

        private void equipmentView_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            // The code in this event only fires if the Ctrl Key is held down by the end user when the Filter Editor is activated //
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //        
            if (Control.ModifierKeys != Keys.Control) return;  // CTRL key not held down so abort //
            GridView view = (GridView)sender;
            List<RepositoryItemComboBox> myRICBlist = new List<RepositoryItemComboBox>();
            foreach (GridColumn col in view.Columns)
            {
                if (col.Visible == false) continue;
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[col.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values == null || col.ColumnType.ToString() == "System.DateTime" || col.ColumnType.ToString() == "DateTime") continue;
                comboBox.Items.AddRange(values);
                myRICBlist.Add(comboBox);
                e.FilterControl.FilterColumns[col.FieldName].SetColumnEditor(myRICBlist[myRICBlist.Count - 1]);
            }
        }
        
        private void equipmentView_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedLocalityCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedLocalityCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }



        #endregion


        #region childGridView

        private void childGridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "vehicleGridView":
                     message = "No Vehicle Information Available";
                    break;
                case "plantGridView":
                     message = "No Plant Information Available";
                    break;
                case "roadTaxGridView":
                     message = "No Road Tax Available";
                    break;
                //case "trackerGridView":
                //     message = "No Verilocation Tracker Information Available",;
                //    break;
                case "softwareGridView":
                     message = "No Software Information Available";
                    break;
                case "officeGridView":
                     message = "No Office Information Available";
                    break;
                case "keeperGridView":
                     message = "No Keeper Details Available";
                    break;
                case "transactionsGridView":
                     message = "No Transactions Details Available";
                    break;
                case "billingGridView":
                     message = "No Billings Information Available";
                    break;
                case "notificationGridView":
                     message = "No Notifications Details Available";
                    break;
                case "depreciationGridView":
                     message = "No Depreciation Data Available";
                    break;
                case "incidentGridView":
                     message = "No Incident Data Available";
                    break;
                case "serviceDataGridView":
                     message = "No Service Interval Data Available";
                    break;
                case "coverGridView":
                     message = "No Cover Data Available";
                    break;
                case "workDetailGridView":
                     message = "No Work Order Detail Data Available";
                    break;
                case "purposeGridView":
                     message = "No Equipment Purpose Data Available";
                    break;
                case "fuelCardGridView":
                     message = "No Fuel Card Data Available";
                    break;
                //case "speedingGridView":
                //     message = "No Speeding Data Available";
                //    break;
                case "p11dGridView":
                     message = "No P11D Details Available";
                    break;
                case "gridViewLinkedDocs":
                    message = "No Linked Documents Available - Select one or more records to view Linked Documents";
                    break;
                default:
                     message = "No Details Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);

        }

        private void childGridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning)
                return;
            isRunning = true;
            GridView view = sender as GridView;
            childGridViewSelectionChanged(view, e);  
        }


        #endregion


        #region plantGridView



        private void plantGridView_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            // The code in this event only fires if the Ctrl Key is held down by the end user when the Filter Editor is activated //
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //        
            if (Control.ModifierKeys != Keys.Control) return;  // CTRL key not held down so abort //
            GridView view = (GridView)sender;
            List<RepositoryItemComboBox> myRICBlist = new List<RepositoryItemComboBox>();
            foreach (GridColumn col in view.Columns)
            {
                if (col.Visible == false) continue;
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[col.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values == null || col.ColumnType.ToString() == "System.DateTime" || col.ColumnType.ToString() == "DateTime") continue;
                comboBox.Items.AddRange(values);
                myRICBlist.Add(comboBox);
                e.FilterControl.FilterColumns[col.FieldName].SetColumnEditor(myRICBlist[myRICBlist.Count - 1]);
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No plant deatail linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion

        #region Excel Events and Functions

        private void bbiExportExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExportGrid(FileExtension.Excel2010Xlsx);
        }

        private void bbiExcelOld_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExportGrid(FileExtension.Excel2003Xls);
        }

        private void bbiImportExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!iBool_AllowAddNetBookValue)
            {
                XtraMessageBox.Show("You do not have sufficient permission to Import The Net Book Value.", "Restricted Access", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            openExcelFileDialog.Filter = "Excel 2010/2007 Files|*.xlsx";
            if (openExcelFileDialog.ShowDialog() == DialogResult.OK)
            {
                strImportFile = openExcelFileDialog.FileName;
                if (strImportFile != "")
                {
                    try
                    {
                        ImportNetBookValue(FileExtension.Excel2010Xlsx, strImportFile, true);
                    }
                    catch (Exception ex)
                    {
                        var dialogTypeName = "System.Windows.Forms.PropertyGridInternal.GridErrorDlg";
                        var dialogType = typeof(Form).Assembly.GetType(dialogTypeName);
                        var dialog = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());
                        dialog.Text = "Error Importing";
                        dialogType.GetProperty("Details").SetValue(dialog, ex.Message, null);
                        dialogType.GetProperty("Message").SetValue(dialog, "Error Importing.", null);
                        var result = dialog.ShowDialog();
                    }
                }
            }
        }

        private void bbiImportExcelOld_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            openExcelFileDialog.Filter = "Excel 2003 Files|*.xls";
            if (openExcelFileDialog.ShowDialog() == DialogResult.OK)
            {
                strImportFile = openExcelFileDialog.FileName;
                if (strImportFile != "")
                {
                    ImportNetBookValue(FileExtension.Excel2003Xls, strImportFile,true);
                }
            }
        }


        private void ImportNetBookValue(FileExtension fileFormat, string strFileName, bool bln1stRowAsColumnHeaders)
        {
            refreshNetBookValues();
            try
            {

                DataTable _dt = new DataTable();
                DataTable _dtErrors = new DataTable();
                DevExpress.Spreadsheet.Workbook originalWorkBook = new DevExpress.Spreadsheet.Workbook();
                originalWorkBook.CreateNewDocument();
                //IWorkbook originalWorkBook = new spreadsheetControl1.Document;
                bool blnSheetOk = false;
                // Load a workbook from a stream. 
                using (FileStream stream = new FileStream(strFileName, FileMode.Open))
                {
                    originalWorkBook.LoadDocument(stream, DocumentFormat.OpenXml);
                    //check for Sheet1
                    foreach (DevExpress.Spreadsheet.Worksheet sheet in originalWorkBook.Worksheets)
                    {
                        if (sheet.Name == "Sheet1")
                            blnSheetOk = true;
                    }
                    if (!blnSheetOk)
                    {
                        XtraMessageBox.Show("Worksheet named 'Sheet1' not found, Import cancelled", "Error Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    try
                    {
                        if (originalWorkBook.Worksheets["Unsuccessful Import"] != null)
                            originalWorkBook.Worksheets.Remove(originalWorkBook.Worksheets["Successful Import"]);

                        if (originalWorkBook.Worksheets["Unsuccessful Import"] != null)
                            originalWorkBook.Worksheets.Remove(originalWorkBook.Worksheets["Unsuccessful Import"]);
                    }
                    catch (ArgumentOutOfRangeException ex)
                    { }



                    foreach (DevExpress.Spreadsheet.Worksheet sheet in originalWorkBook.Worksheets)
                    {
                        switch (sheet.Name.ToString().ToLower())
                        {
                            case "sheet1":
                                if (sheet.Cells[0, 0].DisplayText.ToLower() != "asset code" || sheet.Cells[0, 1].DisplayText.ToLower() != "company" || sheet.Cells[0, 3].DisplayText.ToLower() != "net book value")
                                {
                                    string strErrColumn = "";
                                    if (sheet.Cells[0, 0].DisplayText.ToLower() != "asset code")
                                        strErrColumn = "\n" + "Column header for the first column should be 'asset code'";
                                    if (sheet.Cells[0, 1].DisplayText.ToLower() != "company")
                                        strErrColumn += "\n" + "Column header for the second column should be 'company'";
                                    if (sheet.Cells[0, 3].DisplayText.ToLower() != "net book value")
                                        strErrColumn += "\n" + "Column header for the fourth column should be 'net book value'";

                                    XtraMessageBox.Show("Unexpected Column header, Import cancelled." + strErrColumn, "Error Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }

                                Range _usedRange = sheet.GetUsedRange();
                                int _startRow = 0;
                                if (bln1stRowAsColumnHeaders)
                                {
                                    _startRow = 1;
                                }
                                // set the columns defined
                                for (int _ix = 0; _ix < _usedRange.ColumnCount; _ix++)
                                {
                                    if (bln1stRowAsColumnHeaders)
                                    {
                                        _dt.Columns.Add(sheet.Cells[0, _ix].DisplayText);
                                        _dtErrors.Columns.Add(sheet.Cells[0, _ix].DisplayText);
                                    }
                                    else
                                    {
                                        _dt.Columns.Add(string.Format("Column{0}", _ix));
                                        _dtErrors.Columns.Add(string.Format("Column{0}", _ix));
                                    }
                                }
                                int intSuccessCount = 0;
                                int intAddedCount = 0;
                                int intUpdatedCount = 0;
                                int intFailCount = 0;
                                //add Result column
                                _dt.Columns.Add("ConversionResult");
                                _dtErrors.Columns.Add("ConversionResult");
                                _dt.Columns.Add("AssetMatchResult");
                                _dtErrors.Columns.Add("AssetMatchResult");
                                // add the data
                                for (int _irow = _startRow; _irow < _usedRange.RowCount; _irow++)
                                {
                                    DataRow _row = _dt.NewRow();
                                    DataRow _rowError = _dtErrors.NewRow();
                                    for (int _icol = 0; _icol < _usedRange.ColumnCount; _icol++)
                                    {
                                        _row[_icol] = sheet.Cells[_irow, _icol].DisplayText;
                                        _rowError[_icol] = sheet.Cells[_irow, _icol].DisplayText;
                                    }

                                    //validate 
                                    Decimal decimalNumber;
                                    int intNetBookRecord = 0;
                                    string strAssetID = _row[0].ToString();
                                    string strNetBookValueUpdate = _row[3].ToString();
                                    string strAssetDescription = _row[2].ToString();
                                    string strCompanyCode = _row[1].ToString();
                                    bool blnDecimalResult = Decimal.TryParse(_row[3].ToString(), out decimalNumber);
                                    DataSet_AS_DataEntry.sp_AS_11169_NetBookValue_ItemRow[] netBookValueRows = (DataSet_AS_DataEntry.sp_AS_11169_NetBookValue_ItemRow[])dataSet_AS_DataEntry.sp_AS_11169_NetBookValue_Item.Select(String.Format("ExchequerID = '{0}' and CompanyCode  = '{1}'", _row[0], _row[1]));
                                    bool blnAssetMatch = (netBookValueRows.Length == 1 && strCompanyCode != "");
                                    if (blnAssetMatch)
                                    {
                                        DataSet_AS_DataEntry.sp_AS_11169_NetBookValue_ItemRow _dr = netBookValueRows[0];
                                        intNetBookRecord = _dr.NetBookValueID;
                                        strAssetID = _dr.ExchequerID;
                                    }

                                    _row["AssetMatchResult"] = (blnAssetMatch ? "Match Successful" : "Match Failed");

                                    _row["ConversionResult"] = (blnDecimalResult ? String.Format("Converted '{0}'.", _row[3]) : String.Format("Attempted conversion of {0} failed.", _row[3].ToString() == null ? "<null>" : _row[3].ToString()));

                                    _rowError["AssetMatchResult"] = (blnAssetMatch ? "Match Successful" : "Match Failed");

                                    _rowError["ConversionResult"] = (blnDecimalResult ? String.Format("Converted '{0}' Successfully.", _row[3]) : String.Format("Attempted conversion of {0} failed.", _row[3].ToString() == null ? "<null>" : _row[3].ToString()));

                                    if (blnDecimalResult)
                                    {
                                        if (blnAssetMatch)
                                        {
                                            //update
                                            this.sp_AS_11169_NetBookValue_ItemTableAdapter.Update("edit", intNetBookRecord.ToString(), intNetBookRecord, 0, strAssetID, strCompanyCode, strAssetDescription, decimalNumber, getCurrentDate());
                                            intUpdatedCount++;
                                        }
                                        else if (strAssetID != "" && strCompanyCode != "")
                                        {
                                            //add
                                            this.sp_AS_11169_NetBookValue_ItemTableAdapter.Update("add", "", -1, 0, strAssetID, strCompanyCode, strAssetDescription, decimalNumber, getCurrentDate());
                                            intAddedCount++;
                                        }
                                        else
                                        {
                                            intFailCount++;
                                        }
                                        intSuccessCount++;
                                    }
                                    else
                                    {
                                        intFailCount++;
                                    }
                                    _dt.Rows.Add(_row);
                                } 
                                DataTable dt3 = new DataTable();
                                    dt3 =_dt;
                                int intMatched = exchequerMatchCount();

                                DataRow[] advRow = _dt.Select();

                                for (int i = advRow.Length - 1; i >= 0; i--)
                                {
                                    string strInitialCompany =  advRow[i][1].ToString();
                                    string strSavedExchequerID =  advRow[i][0].ToString();
                                    DataRow[] netBookValueRows = dataSet_AS_DataEntry.sp_AS_11169_NetBookValue_Item.Select(String.Format("ExchequerID = '{0}' and CompanyCode  = '{1}' and EquipmentID <> 0", strSavedExchequerID, strInitialCompany));
                                    bool blnFinalAssetMatch = (netBookValueRows.Length == 1 && strInitialCompany != "");
                                    if (!blnFinalAssetMatch)
                                    {
                                        DataRow _rowError = _dtErrors.NewRow();
                                        _rowError["AssetMatchResult"] = "No Equipment Match";
                                        _rowError["ConversionResult"] = advRow[i][4].ToString(); ;
                                        _rowError["asset code"] = strSavedExchequerID;
                                        _rowError["company"] = advRow[i][1].ToString();
                                        _rowError["net book value"] = advRow[i][3].ToString(); ;
                                        _rowError["Asset Description"] = advRow[i][2].ToString(); ;
                                        _dt.Rows.Remove(advRow[i]);
                                        _dtErrors.Rows.Add(_rowError);
                                    }
                                }

                               
                              _dt.AcceptChanges();
                              _dtErrors.AcceptChanges();

                                XtraMessageBox.Show(String.Format("{0} Exchequer records were matched.", intMatched), "Import Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                break;

                        }
                    }
                }
                //save and close  
                
                originalWorkBook.Worksheets.Add().Name = "Successful Import";

                originalWorkBook.Worksheets["Successful Import"].Import(_dt, true, 0, 0);

                originalWorkBook.Worksheets.Add().Name = "Unsuccessful Import";

                originalWorkBook.Worksheets["Unsuccessful Import"].Import(_dtErrors, true, 0, 0);
                originalWorkBook.Worksheets.ActiveWorksheet = originalWorkBook.Worksheets["Successful Import"];
                originalWorkBook.SaveDocument(strFileName);
                originalWorkBook.Dispose();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "Error Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                var dialogTypeName = "System.Windows.Forms.PropertyGridInternal.GridErrorDlg";
                var dialogType = typeof(Form).Assembly.GetType(dialogTypeName);
                var dialog = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());
                dialog.Text = "Error Importing";
                dialogType.GetProperty("Details").SetValue(dialog, ex.Message, null);
                dialogType.GetProperty("Message").SetValue(dialog, "Error Importing.", null);
                var result = dialog.ShowDialog();
            }
            
        }

        private int exchequerMatchCount()
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetMatchCount = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetMatchCount.ChangeConnectionString(strConnectionString);
            return Convert.ToInt32(GetMatchCount.sp_AS_11000_Match_Exchequer_Data());
        }

        private void refreshNetBookValues()
        {
            sp_AS_11169_NetBookValue_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11169_NetBookValue_Item, "", "");
        }
        private void ExportGrid(FileExtension excelFormat)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();

            folderDlg.ShowNewFolderButton = true;

            DialogResult result = folderDlg.ShowDialog();

            Environment.SpecialFolder root = folderDlg.RootFolder;
            string FileName;
            GridControl Grid_To_Export;

            switch (i_int_FocusedGrid)
            {
                case 0://equipment
                    Grid_To_Export = equipmentGridControl;
                    break;
                case 1://vehicle
                    Grid_To_Export = vehicleGridControl;
                    break;
                case 2://plant
                    Grid_To_Export = plantGridControl;
                    break;

                case 3://RoadTax
                    Grid_To_Export = roadTaxGridControl;
                    break;
                //case 4://tracker
                //    Grid_To_Export = trackerGridControl;
                //    break;
                case 5://software
                    Grid_To_Export = softwareGridControl;
                    break;
                case 6://office
                    Grid_To_Export = officeGridControl;
                    break;
                case 7://billing 
                    Grid_To_Export = billingGridControl;
                    break;
                case 8://keeper
                    Grid_To_Export = keeperGridControl;
                    break;
                case 9://transaction
                    Grid_To_Export = transactionGridControl;
                    break;
                case 10:  // Notifications //
                    Grid_To_Export = notificationGridControl;
                    break;
                case 11:  // Depreciation // 
                    Grid_To_Export = depreciationGridControl;
                    break;
                case 12:  // Incident //
                    Grid_To_Export = incidentGridControl;
                    break;
                case 13:  // ServiceData //
                    Grid_To_Export = serviceDataGridControl;
                    break;
                case 14:  // Cover //
                    Grid_To_Export = coverGridControl;
                    break;
                case 15:  // Work //
                    Grid_To_Export = workDetailGridControl;
                    break;
                case 16:  // Purpose // 
                    Grid_To_Export = purposeGridControl;
                    break;
                case 17:  // fuelCard //
                    Grid_To_Export = fuelCardGridControl;
                    break;
                //case 18:  // speeding //
                //    Grid_To_Export = speedingGridControl;
                    //break;
                case 19:  // P11D //
                    Grid_To_Export = p11dGridControl;
                    break;
                case 99:  // Linked Document //
                    Grid_To_Export = gridControlLinkedDocs;
                    break;
                default:
                    Grid_To_Export = equipmentGridControl;
                    break;
            }
            if (result == DialogResult.OK)
            {
                try
                {
                    if (excelFormat == FileExtension.Excel2010Xlsx)
                    {
                        FileName = folderDlg.SelectedPath + "\\" + Grid_To_Export.Tag + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss") + ".xlsx";
                        Grid_To_Export.ExportToXlsx((FileName));
                    }
                    else if (excelFormat  == FileExtension.Excel2003Xls)
                    {
                        FileName = folderDlg.SelectedPath + "\\" + Grid_To_Export.Tag + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss") + ".xls";
                        Grid_To_Export.ExportToXls((FileName));
                    }

                }
                catch (Exception)
                {
                    XtraMessageBox.Show("Please contact ICT to upgrade or install a compatible  and enable the feature", "Compatibility Issue", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        

        #endregion

        private void equipmentGridControl_Click(object sender, EventArgs e)
        {

        }

        private void bbiAutoMatchExchequer_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bbiAutoMatchExchequer.Enabled = false;

            XtraMessageBox.Show(String.Format("{0} Exchequer records were matched to the Equipment Records.", exchequerMatchCount()), "Macth Results", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            bbiAutoMatchExchequer.Enabled = true;
        }


        #region GridView Linked Documents

        private void gridControlLinkedDocs_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewLinkedDocs_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //
            GridView view = (GridView)sender;
            if (e.Column.ColumnType != typeof(DateTime))
            {
                GridColumnCollection cols = new GridColumnCollection(view);
                GridColumn column = cols.AddField(e.Column.FieldName);
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[column.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values != null)
                {
                    comboBox.Items.AddRange(values);
                    column.ColumnEdit = comboBox;
                }
                DevExpress.XtraGrid.Filter.FilterCustomDialog dlg = new DevExpress.XtraGrid.Filter.FilterCustomDialog(column, false);
                dlg.ShowDialog();
                e.FilterInfo = null;
                e.Handled = true;
                view.GridControl.Refresh();
            }
        }

        private void gridViewLinkedDocs_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewLinkedDocs_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            // The code in this event only fires if the Ctrl Key is held down by the end user when the Filter Editor is activated //
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //        
            if (Control.ModifierKeys != Keys.Control) return;  // CTRL key not held down so abort //
            GridView view = (GridView)sender;
            List<RepositoryItemComboBox> myRICBlist = new List<RepositoryItemComboBox>();
            foreach (GridColumn col in view.Columns)
            {
                if (col.Visible == false) continue;
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[col.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values == null || col.ColumnType.ToString() == "System.DateTime" || col.ColumnType.ToString() == "DateTime") continue;
                comboBox.Items.AddRange(values);
                myRICBlist.Add(comboBox);
                e.FilterControl.FilterColumns[col.FieldName].SetColumnEditor(myRICBlist[myRICBlist.Count - 1]);
            }
        }

        private void gridViewLinkedDocs_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 99;
            SetMenuStatus();
        }

        private void gridViewLinkedDocs_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 99;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewLinkedDocs_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void keeperGridView_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void keeperGridView_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit4_OpenLink(object sender, OpenLinkEventArgs e)
        {
            /**
            // Drilldown to Linked Document Manager //
            int intRecordType = 41;  // Client Contract //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientContractID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "ClientName").ToString() + ", Duration: " + (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "StartDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date") + " - " + (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "EndDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date");
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
            **/

            Linked_Documents_To_Record();
        }

        private void bbiEmail_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int[] intRowHandles = equipmentGridView.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one item to email to the keeper.\n\n", "Email current Keepers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            StringBuilder strEquipmentIDs = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                strEquipmentIDs.Append(Convert.ToString(equipmentGridView.GetRowCellDisplayText(intRowHandle, colEquipmentID4)) + ",");
            }

            //Pass the EquipmentIds to the Enail process which will deternmine Emails for current holders where they exist.
            EquipmentEmail.Build(strConnectionString, strEquipmentIDs.ToString(),"Fleet: ");
        }

        private void gridViewLinkedDocs_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
            SetMenuStatus();
        }

        private void repositoryItemHyperLinkEditLinkedDocs_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion



    }
}


﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;
using DevExpress.XtraEditors.Repository;
using DevExpress.Utils;
using System.Reflection;
using DevExpress.XtraSplashScreen;
using System.Data.SqlClient;

namespace WoodPlan5
{
    public partial class frm_AS_Rental_Edit: BaseObjects.frmBase
    {
        public frm_AS_Rental_Edit()
        {
            InitializeComponent();
        }

        private void frm_AS_Rental_Edit_Load(object sender, EventArgs e)
        {
            txtEquipmentReference.Properties.ReadOnly = true;

            this.FormID = 110901;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            sp_AS_11000_FleetManagerDummyScreenTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11000_FleetManagerDummyScreenTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_FleetManagerDummyScreen,strCaller);
            gridControl1.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            // Populate Main Dataset //
            sp_AS_11102_Rental_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            // Populate Child dataSet
            sp_AS_11000_Duplicate_SearchTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11014_Make_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11017_Model_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11020_Supplier_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11066_Depreciation_Settings_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11096_Road_Tax_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11108_Rental_Details_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            CreateNewInstancesGridStates();
            rentalLayoutControlGroup.BeginUpdate();
            lueEquipmentCategoryID.Properties.ReadOnly = true;
            switch (strFormMode.ToLower())
            {
                case "add":
                    addNewRow();
                    LockRentalEquip(lockControls);
                    LockRentalInterval(lockControls);
                    break;
                case "blockadd":
                    this.Close();
                    break;
                case "blockedit":
                    MessageBox.Show("this section is not yet ready","Under Construction", MessageBoxButtons.OK,MessageBoxIcon.Information);
                    this.Close();
                    //Get_ChildData();
                        loadRentalEquipment();
                        equipmentDataNavigator.Enabled = false;
                        DataRow drNewRow = this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item.NewRow();
                        drNewRow["Mode"] = "blockedit";
                        drNewRow["RecordID"] = strRecordIDs;
                        drNewRow["EquipmentID"] = "";
                        drNewRow["EquipmentReference"] = "";
                        drNewRow["EquipmentCategoryID"] = "7";
                        this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item.Rows.Add(drNewRow);
                        this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                                           
                    break;
                case "edit":
                case "view":
                    try
                    {
                      loadRentalEquipment();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
            }
            rentalLayoutControlGroup.EndUpdate();
            if (strFormMode == "view")  // Disable all controls //
            {
                equipmentDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }

            //Populate picklist Data
            PopulateRentalPickList();
            //Get_ChildData();
            
            ProcessPermissionsForForm();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }

        private void CreateNewInstancesGridStates()
        {

            RefreshGridViewStateRoadTax = new RefreshGridState(roadTaxGridView, "RoadTaxID");


            RefreshGridViewStateTracker = new RefreshGridState(trackerGridView, "TrackerInformationID");


            RefreshGridViewStateTransactions = new RefreshGridState(transactionsGridView, "TransactionID");

            RefreshGridViewStateKeeper = new RefreshGridState(keeperGridView, "KeeperAllocationID");

            RefreshGridViewStateBilling = new RefreshGridState(billingGridView, "EquipmentBillingID");

            RefreshGridViewStateDepreciation = new RefreshGridState(depreciationGridView, "DepreciationID");

            RefreshGridViewStateCover = new RefreshGridState(coverGridView, "CoverID");

            RefreshGridViewStateServiceInterval = new RefreshGridState(serviceIntervalGridView, "ServiceIntervalID");

            RefreshGridViewStateWork = new RefreshGridState(workDetailGridView, "WorkDetailID");

            RefreshGridViewStateIncident = new RefreshGridState(incidentGridView, "IncidentID");

            RefreshGridViewStatePurpose = new RefreshGridState(purposeGridView, "EquipmentPurposeID");

            RefreshGridViewStateFuelCard = new RefreshGridState(fuelCardGridView, "fuelCardID");

            RefreshGridViewStateSpeeding = new RefreshGridState(speedingGridView, "SpeedingID");
        }

        #region Instance Variables
        bool forceCloseForm = false;
        bool lockControls = false;
        private string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //        
        private string strMessage1, strMessage2;
        private int iDistinctSelectedType = 1;
        public int numOfSelectedRows = 1;
        public string strRecordsToLoad = "";
        private string gcRef = "";
        int i_int_FocusedGrid = 0;
        GridHitInfo downHitInfo = null;
        private Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private bool isRunning = false;
        BaseObjects.GridCheckMarksSelection selection1;
        string shownTabs = "";
        private uint _PropertyName;
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        bool bool_FormLoading = true;
        
        private string tempMake = "";
        private string tempModel = "";
        private string tempManufacturer = "";
        private string tempRegistration = "";

        public bool rentalChanged = false;
        public bool rentalDetailsChanged = false;

        private bool P11dManagerPromptRequired = false;

        public string strEquipmentIDs = ""; 
        public string strRentalDetailsID = "";
        public enum FormMode { add, edit, view, delete, blockadd, blockedit };
        public FormMode formMode;
        public enum EquipmentType { None, Rental = 1}
        public enum SentenceCase { Upper , Lower, Title, AsIs }
        public EquipmentType passedEquipType;
        
        private bool iBool_AllowAddRoadTax = false;
        private bool iBool_AllowEditRoadTax = false;
        private bool iBool_AllowDeleteRoadTax = false;

        private bool iBool_AllowAddSpeeding = false;
        private bool iBool_AllowEditSpeeding = false;
        private bool iBool_AllowDeleteSpeeding = false;

        private bool iBool_AllowAddTracker = false;
        private bool iBool_AllowEditTracker = false;
        private bool iBool_AllowDeleteTracker = false;

        private bool iBool_AllowAddBilling = false;
        private bool iBool_AllowEditBilling = false;
        private bool iBool_AllowDeleteBilling = false;

        private bool iBool_AllowAddKeeper = false;
        private bool iBool_AllowEditKeeper = false;
        private bool iBool_AllowDeleteKeeper = false;

        private bool iBool_AllowAddTransactions = false;
        private bool iBool_AllowEditTransactions = false;
        private bool iBool_AllowDeleteTransactions = false;

        private bool iBool_AllowAddReminders = false;
        private bool iBool_AllowEditReminders = false;
        private bool iBool_AllowDeleteReminders = false;

        internal bool iBool_AllowAddDepreciation = false;
        internal bool iBool_AllowEditDepreciation = false;
        internal bool iBool_AllowDeleteDepreciation = false;

        private bool iBool_AllowAddCover = false;
        private bool iBool_AllowEditCover = false;
        private bool iBool_AllowDeleteCover = false;

        private bool iBool_AllowAddServiceInterval = false;
        private bool iBool_AllowEditServiceInterval = false;
        private bool iBool_AllowDeleteServiceInterval = false;

        private bool iBool_AllowAddWorkDetail = false;
        private bool iBool_AllowEditWorkDetail = false;
        private bool iBool_AllowDeleteWorkDetail = false;

        private bool iBool_AllowAddIncident = false;
        private bool iBool_AllowEditIncident = false;
        private bool iBool_AllowDeleteIncident = false;

        private bool iBool_AllowAddPurpose = false;
        private bool iBool_AllowEditPurpose = false;
        private bool iBool_AllowDeletePurpose = false;

        private bool iBool_AllowAddFuelCard = false;
        private bool iBool_AllowEditFuelCard = false;
        private bool iBool_AllowDeleteFuelCard = false;

        // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateRoadTax;
        public RefreshGridState RefreshGridViewStateTracker;
        public RefreshGridState RefreshGridViewStateKeeper;
        public RefreshGridState RefreshGridViewStateTransactions;
        public RefreshGridState RefreshGridViewStateBilling;
        public RefreshGridState RefreshGridViewStateDepreciation;
        public RefreshGridState RefreshGridViewStateCover;
        public RefreshGridState RefreshGridViewStateServiceInterval;
        public RefreshGridState RefreshGridViewStateWork;
        public RefreshGridState RefreshGridViewStateIncident;
        public RefreshGridState RefreshGridViewStatePurpose;
        public RefreshGridState RefreshGridViewStateFuelCard;
        public RefreshGridState RefreshGridViewStateSpeeding;        
        #endregion

        #region Compulsory Implementation 
        
        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode.ToLower())
            {
                case "add":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                      //  barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        if (passedEquipType != EquipmentType.None)
                        {
                            lueEquipmentCategoryID.Enabled = false;
                            if (strCaller == "frm_AS_Fleet_Manager")
                            {
                                this.spAS11011EquipmentCategoryListBindingSource.Filter = "EquipmentCategory = 1 or EquipmentCategory = 2";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        ////barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        txtEquipmentReference.Focus();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        //barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;           
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }

                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Viewing";
                        bsiFormMode.ImageIndex = 4;  // View //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        //barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                equipmentDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
        
            bbiSave.Enabled = false;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        #endregion

        #region Form_Events

        private void lueStatusID_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (bool_FormLoading)
                return;

            if (strFormMode.ToLower() == "add" && !bool_MakeEnabled)
            {
                lueMakeID.Enabled = true;
            }
        }


        private void lueMakeID_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (bool_FormLoading)
                return;

            if (strFormMode.ToLower() == "add" && !bool_ModelEnabled)
            {
                lueModelID.Enabled = true;
            }
        }

        private void lueModelID_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (bool_FormLoading)
                return;

            if (strFormMode.ToLower() == "add" && !bool_SupplierEnabled)
            {
                lueSupplierID.Enabled = true;        
            }
        }
        bool bool_MakeEnabled = false;
        bool bool_ModelEnabled = false;
        bool bool_RentalNotesEnabled = false;
        bool bool_RentalTypeEnabled = false;
        bool bool_SupplierEnabled = false;

        private void lueSupplierID_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (bool_FormLoading)
                return;

            if (strFormMode.ToLower() == "add" && !bool_RentalNotesEnabled)
            {
                memRentalNotes.Enabled = true;
                LockRentalEquip(true);
                LockRentalInterval(true);
            }
        }
      
        private void txtUniqueReference_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (bool_FormLoading)
                return;

            if (strFormMode.ToLower() == "add" && !bool_RentalTypeEnabled)
            {
                lueStatusID.Enabled = true;
            }
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) 
                this.Close();
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private void frm_AS_Rental_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view" || forceCloseForm) return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        private void frm_AS_Rental_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

        }
        
        #endregion

        #region Form_Functions


        private void ChildVisibility(int IDs, int typesOfEquipment)
        {
            string strChildRecordIDs = IDs.ToString();

            //navigate to record
            if (strFormMode.ToLower() != "add")
            {
                spAS11108RentalDetailsItemBindingSource.Position = spAS11108RentalDetailsItemBindingSource.Find("RentalDetailsID", strChildRecordIDs);
            }
            if (strFormMode.ToLower() == "add")
            {
                RejectChildChanges();
                DataRow drNewRow;
                drNewRow = this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item.NewRow();
                drNewRow["Mode"] = "add";
                drNewRow["EquipmentID"] = "-1";
                drNewRow["RentalDetailsID"] = "-1";
                drNewRow["RecordID"] = strEquipmentIDs;
                drNewRow["SetAsMain"] = true;
                drNewRow["StartDate"] = (getCurrentDate()).ToString();
                drNewRow["EndDate"] = (getCurrentDate().AddDays(1)).ToString();
                drNewRow["ActualEndDate"] = (getCurrentDate().AddDays(1)).ToString();
                if (spAS11108RentalDetailsItemBindingSource.Find("RentalDetailsID", "-1") < 0)
                {
                    this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item.Rows.Add(drNewRow);
                }
            }

        }

        private void LockRentalEquip(bool lockControls)
        {
            txtUniqueReference.Focus();
            lueEquipmentCategoryID.Enabled = lockControls;
            lueStatusID.Enabled = lockControls;
            lueMakeID.Enabled = lockControls;
            lueModelID.Enabled = lockControls;
            txtModelSpecifications.Enabled = lockControls; 
            lueSupplierID.Enabled = lockControls;
            memRentalNotes.Enabled = lockControls;
        }

        private void LockRentalInterval(bool lockControls)
        {
            deStartDate.Enabled = lockControls;
            deEndDate.Enabled = lockControls;
            deActualEndDate.Enabled = lockControls;
            ceSetAsMain.Enabled = lockControls;
            lueRentalCategoryID.Enabled = lockControls;
            lueRentalStatusID.Enabled = lockControls;
            lueRentalSpecificationID.Enabled = lockControls;
            txtCustomerReference.Enabled = lockControls;
            spnRate.Enabled = lockControls;
            memIntervalNotes.Enabled = lockControls; 
        }

        private DateTime getCurrentDate()
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetDate = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetDate.ChangeConnectionString(strConnectionString);
            GetDate.sp_AS_11138_Get_Server_Date();
            DateTime d = DateTime.Parse(GetDate.sp_AS_11138_Get_Server_Date().ToString());
            return d;
        }

        private void RejectChildChanges()
        {
            this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item.RejectChanges();
        }

        private void saveGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStateRoadTax.SaveViewInfo();
            this.RefreshGridViewStateTracker.SaveViewInfo();
            this.RefreshGridViewStateBilling.SaveViewInfo();
            this.RefreshGridViewStateKeeper.SaveViewInfo();
            this.RefreshGridViewStateTransactions.SaveViewInfo();
            this.RefreshGridViewStateDepreciation.SaveViewInfo();
            this.RefreshGridViewStateCover.SaveViewInfo();
            this.RefreshGridViewStateServiceInterval.SaveViewInfo();
            this.RefreshGridViewStateWork.SaveViewInfo();
            this.RefreshGridViewStateIncident.SaveViewInfo();
            this.RefreshGridViewStatePurpose.SaveViewInfo();
            this.RefreshGridViewStateFuelCard.SaveViewInfo();
            this.RefreshGridViewStateSpeeding.SaveViewInfo();
            //this.RefreshGridViewStateReminder.SaveViewInfo();

        }

        private void loadGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStateRoadTax.LoadViewInfo();
            this.RefreshGridViewStateTracker.LoadViewInfo();
            this.RefreshGridViewStateBilling.LoadViewInfo();
            this.RefreshGridViewStateKeeper.LoadViewInfo();
            this.RefreshGridViewStateTransactions.LoadViewInfo();
            this.RefreshGridViewStateDepreciation.LoadViewInfo();
            this.RefreshGridViewStateCover.LoadViewInfo();
            this.RefreshGridViewStateServiceInterval.LoadViewInfo();
            this.RefreshGridViewStateWork.LoadViewInfo();
            this.RefreshGridViewStateIncident.LoadViewInfo();
            this.RefreshGridViewStatePurpose.LoadViewInfo();
            this.RefreshGridViewStateFuelCard.LoadViewInfo();
            this.RefreshGridViewStateSpeeding.LoadViewInfo();
            //this.RefreshGridViewStateReminder.LoadViewInfo();        
        }

        private void OpenEditForm(FormMode mode, string frmCaller, GridControl gridControl, EquipmentType equipmentType)
        {
            if (strFormMode != "edit")
            {
                if (strFormMode != "blockedit")
                {
                    XtraMessageBox.Show("Please complete and save main form records before making detailed form changes.", "Add/Edit Restricted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    XtraMessageBox.Show("Making changes to this section is restricted during block editting.", "Add/Edit Restricted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                return;
            }
            
            bool cancelChildLoad = false;
            GridView view = null;
            frmProgress fProgress = null;
            MethodInfo method = null;
            strRecordsToLoad = "";
            int[] intRowHandles;
            int intCount = 0;
            saveGridViewState();
            gcRef = "";
            
            switch (i_int_FocusedGrid)
            {
                
                #region Keeper

                case 8:     // Keeper
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Keeper Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dr)).KeeperAllocationID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Keeper Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate keepers before proceeding.", "Add Keeper Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add keeper allocations.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the keepers";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        gcRef =  ((DataSet_AS_DataEntry.sp_AS_11102_Rental_ItemRow)((DataRowView)(spAS11102RentalItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add keepers, otherwise right click for block adding keepers.", "Add Keeper Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        gcRef = ((DataSet_AS_DataEntry.sp_AS_11102_Rental_ItemRow)((DataRowView)(spAS11102RentalItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
                    }
                       
                  
                    frm_AS_Keeper_Edit kChildForm = new frm_AS_Keeper_Edit();
                    kChildForm.MdiParent = this.MdiParent;
                    kChildForm.GlobalSettings = this.GlobalSettings;
                    kChildForm.strRecordIDs = strRecordsToLoad;
                    kChildForm.strEquipmentIDs = strRecordIDs;
                    kChildForm.formMode = (frm_AS_Keeper_Edit.FormMode)mode;
                    kChildForm.strFormMode = (mode.ToString()).ToLower();
                    kChildForm.strCaller = frmCaller;
                    kChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        kChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        kChildForm.intRecordCount = intCount;
                    }
                    kChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager3 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    kChildForm.splashScreenManager = splashScreenManager3;
                    kChildForm.splashScreenManager.ShowWaitForm();
                    kChildForm.Show();
                    break;

                #endregion

                #region Transactions
                        
                case 9:     // Transactions
                   XtraMessageBox.Show("Please complete editing the main form and save before accessing making changes to the transactions", "Add/Edit Restricted");
                            return;
                #endregion

                #region Reminders

                case 10:     // Reminders

                    break;
                #endregion
                    
                #region Incidents
                case 18: // Speeding data                
                case 12:     // Incidents 
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Incident Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)(dr)).IncidentID).ToString() + ',';
                        }
                    }


                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add incidents.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the incidents";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        gcRef = ((DataSet_AS_DataEntry.sp_AS_11102_Rental_ItemRow)((DataRowView)(spAS11102RentalItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add incidents, otherwise right click for block adding incidents.", "Add Incident Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        gcRef = ((DataSet_AS_DataEntry.sp_AS_11102_Rental_ItemRow)((DataRowView)(spAS11102RentalItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
                    }

                    frm_AS_Incident_Edit iChildForm = new frm_AS_Incident_Edit();
                    iChildForm.MdiParent = this.MdiParent;
                    iChildForm.GlobalSettings = this.GlobalSettings;
                    iChildForm.strRecordIDs = strRecordsToLoad;
                    iChildForm.strEquipmentIDs = strRecordIDs;
                    iChildForm.formMode = (frm_AS_Incident_Edit.FormMode)mode;
                    iChildForm.strFormMode = (mode.ToString()).ToLower();
                    iChildForm.strCaller = frmCaller;
                    iChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        iChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        iChildForm.intRecordCount = intCount;
                    }
                    iChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager5 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    iChildForm.splashScreenManager = splashScreenManager5;
                    iChildForm.splashScreenManager.ShowWaitForm();
                    iChildForm.Show();
                    break;
                #endregion

                #region Service Interval

                case 13:     // Service Interval
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Service Interval Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceDataID).ToString() + ',';
                        }
                    }


                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 || iDistinctSelectedType == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add service interval(s).", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the Service Intervals";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        gcRef = getEquipmentReference();
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add service intervals, otherwise right click for block adding service intervals.", "Add Service Interval Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        gcRef = getEquipmentReference();
                    }

                    frm_AS_ServiceIntervals_Edit sIChildForm = new frm_AS_ServiceIntervals_Edit();
                    sIChildForm.MdiParent = this.MdiParent;
                    sIChildForm.GlobalSettings = this.GlobalSettings;
                    sIChildForm.strRecordIDs = strRecordsToLoad;
                    sIChildForm.strEquipmentIDs = strRecordIDs;
                    sIChildForm.formMode = (frm_AS_ServiceIntervals_Edit.FormMode)mode;
                    sIChildForm.strFormMode = (mode.ToString()).ToLower();
                    sIChildForm.strCaller = frmCaller;
                    sIChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        sIChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        sIChildForm.intRecordCount = intCount;
                    }
                    sIChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager6 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    sIChildForm.splashScreenManager = splashScreenManager6;
                    sIChildForm.splashScreenManager.ShowWaitForm();
                    sIChildForm.Show();
                    break;
                #endregion

                #region Cover

                case 14:     // Cover
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Cover Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11075_Cover_ItemRow)(dr)).CoverID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Cover Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0 || iDistinctSelectedType == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add cover details.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the cover details";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }


                        gcRef = getEquipmentReference();
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add cover details, otherwise right click for block adding cover details.", "Add Cover Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }


                        gcRef = getEquipmentReference();

                    }

                    frm_AS_Cover_Edit cChildForm = new frm_AS_Cover_Edit();
                    cChildForm.MdiParent = this.MdiParent;
                    cChildForm.GlobalSettings = this.GlobalSettings;
                    cChildForm.strRecordIDs = strRecordsToLoad;
                    cChildForm.strEquipmentIDs = strRecordIDs;
                    cChildForm.formMode = (frm_AS_Cover_Edit.FormMode)mode;
                    cChildForm.strFormMode = (mode.ToString()).ToLower();
                    cChildForm.strCaller = frmCaller;
                    cChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        cChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        cChildForm.intRecordCount = intCount;
                    }
                    cChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager7 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    cChildForm.splashScreenManager = splashScreenManager7;
                    cChildForm.splashScreenManager.ShowWaitForm();
                    cChildForm.Show();
                    break;
                #endregion

                #region Work Detail

                case 15:     // Work Detail
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Work Order Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11078_Work_Detail_ItemRow)(dr)).WorkDetailID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Work Order Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Work Orders.", "Add Work Order Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the Work Orders";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }


                        gcRef = getEquipmentReference();
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Work Orders, otherwise right click for block adding Work Orders.", "Add Work Order Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }


                        gcRef = getEquipmentReference();

                    }

                    frm_AS_WorkDetail_Edit wChildForm = new frm_AS_WorkDetail_Edit();
                    wChildForm.MdiParent = this.MdiParent;
                    wChildForm.GlobalSettings = this.GlobalSettings;
                    wChildForm.strRecordIDs = strRecordsToLoad;
                    wChildForm.strEquipmentIDs = strRecordIDs;
                    wChildForm.formMode = (frm_AS_WorkDetail_Edit.FormMode)mode;
                    wChildForm.strFormMode = (mode.ToString()).ToLower();
                    wChildForm.strCaller = frmCaller;
                    wChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        wChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        wChildForm.intRecordCount = intCount;
                    }
                    wChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager8 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    wChildForm.splashScreenManager = splashScreenManager8;
                    wChildForm.splashScreenManager.ShowWaitForm();
                    wChildForm.Show();
                    break;

                #endregion

                #region Purpose

                case 16:     // Purpose
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Equipment Purpose Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11084_Purpose_ItemRow)(dr)).EquipmentPurposeID).ToString() + ',';
                        }
                    }

                    //if (mode == FormMode.blockedit)
                    //{
                    //    XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Equipment Purpose Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //    return;
                    //}

                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add || mode == FormMode.blockadd)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Equipment Purpose.", "Add Equipment Purpose Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the equipment purpose?";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }


                        gcRef = getEquipmentReference();
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Equipment Purpose records, otherwise right click for block adding Equipment Purpose records.", "Add Equipment Purpose Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }


                        gcRef = getEquipmentReference();
                    }

                    frm_AS_Purpose_Edit pChildForm = new frm_AS_Purpose_Edit();
                    pChildForm.MdiParent = this.MdiParent;
                    pChildForm.GlobalSettings = this.GlobalSettings;
                    pChildForm.strRecordIDs = strRecordsToLoad;
                    pChildForm.strEquipmentIDs = strRecordIDs;
                    pChildForm.formMode = (frm_AS_Purpose_Edit.FormMode)mode;
                    pChildForm.strFormMode = (mode.ToString()).ToLower();
                    pChildForm.strCaller = frmCaller;
                    pChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        pChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        pChildForm.intRecordCount = intCount;
                    }
                    pChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager9 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    pChildForm.splashScreenManager = splashScreenManager9;
                    pChildForm.splashScreenManager.ShowWaitForm();
                    pChildForm.Show();
                    break;
                #endregion

                #region Fuel Card

                case 17:     // Fuel Card
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_ItemRow)(dr)).FuelCardID).ToString() + ',';
                        }
                    }

                    if (mode == FormMode.blockedit)
                    {
                        XtraMessageBox.Show("Blockedit is restricted.", "Blockedit Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    if (mode == FormMode.blockadd)
                    {
                        XtraMessageBox.Show("Blockadd is restricted.", "Blockadd Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    if (mode == FormMode.add)
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate Fuel Cards before proceeding.", "Add Fuel Card Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Fuel Card allocations.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, please select one equipment to add the Fuel Cards";
                            XtraMessageBox.Show(strTypes, "Block Add Restricted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            cancelChildLoad = true;
                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        int equipType = 7;
                        string equipID = "";
                        gcRef = getEquipmentReference();

                        
                        equipID = ((DataSet_AS_DataEntry.sp_AS_11102_Rental_ItemRow)((DataRowView)(spAS11102RentalItemBindingSource.CurrencyManager.Current)).Row).EquipmentID.ToString();
                        if (equipType != 7)
                        {
                            XtraMessageBox.Show("Fuel Card allocations are restricted to Rental or Lease on this Form.", "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        sp_AS_11044_Keeper_Allocation_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List, equipID, "view");
                        if (this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.Count < 1)
                        {
                            XtraMessageBox.Show(("Please add Keepers to Equipment Reference " + gcRef + " before you proceed to add Fuel Cards"), "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Fuel Cards, otherwise right click for block adding Fuel Cards.", "Add Fuel Card Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        gcRef = getEquipmentReference();
                    }

                    frm_AS_FuelCard_Edit fcChildForm = new frm_AS_FuelCard_Edit();
                    fcChildForm.MdiParent = this.MdiParent;
                    fcChildForm.GlobalSettings = this.GlobalSettings;
                    fcChildForm.strRecordIDs = strRecordsToLoad;
                    fcChildForm.strEquipmentIDs = strRecordIDs;
                    fcChildForm.formMode = (frm_AS_FuelCard_Edit.FormMode)mode;
                    fcChildForm.strFormMode = (mode.ToString()).ToLower();
                    fcChildForm.strCaller = frmCaller;
                    fcChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        fcChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        fcChildForm.intRecordCount = intCount;
                    }
                    fcChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager10 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fcChildForm.splashScreenManager = splashScreenManager10;
                    fcChildForm.splashScreenManager.ShowWaitForm();
                    fcChildForm.Show();
                    break;
                #endregion

                #region Road Tax

                case 19:     //  Road Tax
                    view = (GridView)gridControl.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    if (mode == FormMode.edit || mode == FormMode.blockedit)
                    {
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Road Tax Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);

                            strRecordsToLoad += (((DataSet_AS_DataEntry.sp_AS_11096_Road_Tax_ItemRow)(dr)).RoadTaxID).ToString() + ',';
                        }
                    }


                    //if (mode == FormMode.blockadd)
                    //{
                    //    XtraMessageBox.Show("Blockadd is restricted.", "Blockadd Road Tax Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //    return;
                    //}
                    if (mode == FormMode.add)
                    {
                        if (strRecordIDs == "")
                        {
                            XtraMessageBox.Show("Please select an equipment to allocate Road Tax before proceeding.", "Add Road Tax Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    cancelChildLoad = false;
                    gcRef = "";
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows == 0)
                        {
                            XtraMessageBox.Show("Select one equipment record or one type of equipment to add Road Tax.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (numOfSelectedRows > 1)//block add option
                        {
                            string strTypes = "You have selected multiple records, do you want to block add the road tax ?";
                            switch (XtraMessageBox.Show(strTypes, "Block Add Option", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    XtraMessageBox.Show("Please select the one record to proceed", "Block Add Option Cancelled", MessageBoxButtons.OK);
                                    cancelChildLoad = true;
                                    break;
                                case DialogResult.Yes:
                                    // change to block add //
                                    mode = FormMode.blockadd;
                                    break;
                            }

                        }
                        if (cancelChildLoad)
                        {
                            return;
                        }

                        //int equipType = 0;
                        //string equipID = "";
                        gcRef = getEquipmentReference();

                    //    int[] intEquipRowhandles = equipmentGridView.GetSelectedRows();
                    //    foreach (int intRowHandle in intEquipRowhandles)
                    //    {
                    //        DataRow dr = equipmentGridView.GetDataRow(intRowHandle);
                    //        gcRef = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentReference).ToString();

                    //        //restrict  addition to plant and vehicle

                    //        equipType = Convert.ToInt32(((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentCategoryID);
                    //        if (equipType != 1 && equipType != 2)
                    //        {
                    //            XtraMessageBox.Show("Road Tax allocations are restricted to Plant and Vehicles.", "Add Record Restiction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    //            return;
                    //        }


                    //        equipID = (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString();
                           
                    //    }
                    }

                    if (mode == FormMode.blockadd)
                    {
                        gcRef = "";
                    }
                    if (mode == FormMode.add)
                    {
                        if (numOfSelectedRows != 1)
                        {
                            XtraMessageBox.Show("Select one equipment record to add Road Tax, otherwise right click for block adding Road Tax.", "Add Road Tax Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        gcRef = getEquipmentReference();
                    }

                    frm_AS_Road_Tax_Edit rtChildForm = new frm_AS_Road_Tax_Edit();
                    rtChildForm.MdiParent = this.MdiParent;
                    rtChildForm.GlobalSettings = this.GlobalSettings;
                    rtChildForm.strRecordIDs = strRecordsToLoad;
                    rtChildForm.strEquipmentIDs = strRecordIDs;
                    rtChildForm.formMode = (frm_AS_Road_Tax_Edit.FormMode)mode;
                    rtChildForm.strFormMode = (mode.ToString()).ToLower();
                    rtChildForm.strCaller = frmCaller;
                    rtChildForm.strGCReference = gcRef;
                    if (mode == FormMode.blockadd)
                    {
                        rtChildForm.intRecordCount = numOfSelectedRows;
                    }
                    else
                    {
                        rtChildForm.intRecordCount = intCount;
                    }
                    rtChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager11 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    rtChildForm.splashScreenManager = splashScreenManager11;
                    rtChildForm.splashScreenManager.ShowWaitForm();
                    rtChildForm.Show();
                    break;
                #endregion

                #region Tracker Details

                
                #endregion

                default:
                    MessageBox.Show("Failed to load edit form", "Error Loading Edit form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private string getEquipmentReference()
        {
            return ((DataSet_AS_DataEntry.sp_AS_11102_Rental_ItemRow)((DataRowView)(spAS11102RentalItemBindingSource.CurrencyManager.Current)).Row).EquipmentReference;
        }
        
        private void ProcessPermissionsForForm()
        {
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 19:  // RoadTax //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddRoadTax = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditRoadTax = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteRoadTax = true;
                        }
                        break;
                    case 4:  // Tracker //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddTracker = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditTracker = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteTracker = true;
                        }
                        break;
                   
                    case 7:  // Billings //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddBilling = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditBilling = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteBilling = true;
                        }
                        break;
                    case 8:  // Keeper //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddKeeper = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditKeeper = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteKeeper = true;
                        }
                        break;
                    case 9:  // Transactions //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddTransactions = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditTransactions = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteTransactions = true;
                        }
                        break;
                    case 10:  // Reminders //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddReminders = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditReminders = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteReminders = true;
                        }
                        break;
                    case 11:  // Depreciation //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddDepreciation = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditDepreciation = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteDepreciation = true;
                        }
                        break;
                    case 12:  // Incident //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddIncident = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditIncident = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteIncident = true;
                        }
                        break;
                    case 13:  // ServiceInterval //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddServiceInterval = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditServiceInterval = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteServiceInterval = true;
                        }
                        break;
                    case 14:  // Cover //13:  // ServiceInterval //12:  // Incident // 11:  // Depreciation //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddCover = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditCover = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteCover = true;
                        }
                        break;
                    case 15:  // Work //14:  // Cover //13:  // ServiceInterval //12:  // Incident // 11:  // Depreciation //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddWorkDetail = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditWorkDetail = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteWorkDetail = true;
                        }
                        break;
                    case 16:  // Purpose //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddPurpose = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditPurpose = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeletePurpose = true;
                        }
                        break;
                    case 17:  // FuelCard //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddFuelCard = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditFuelCard = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteFuelCard = true;
                        }
                        break;
                    case 18:  // Speeding //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddSpeeding = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditSpeeding = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteSpeeding = true;
                        }
                        break;
                }
            }
        }

        private void ClearErrors(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is LookUpEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is TextEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is DateEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is ColorPickEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is SpinEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is ContainerControl)
                    this.ClearErrors(item.Controls);
                if (item2 is DevExpress.XtraDataLayout.DataLayoutControl)
                    this.ClearErrors(item.Controls);

            }
        }

        internal void RefreshModel()
        {
            if (bool_FormLoading)
                return;
            sp_AS_11017_Model_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11017_Model_List, "", "view"); //Model
            if (lueMakeID.EditValue == null ? false : true)
            {
                this.spAS11017ModelListBindingSource.Filter = "Make = " + lueMakeID.EditValue;

                if (this.spAS11017ModelListBindingSource.Count == 0)
                {
                    lueModelID.Properties.NullText = "--No Models Available For Selected Make, Please Add--";
                    lueModelID.Properties.AllowFocused = false;
                }
                else
                {
                    lueModelID.Properties.NullText = "--Please Select a Model--";
                    lueModelID.Properties.AllowFocused = true;
                }
            }
            else
            {
                this.spAS11017ModelListBindingSource.Filter = "Make = 0";
                lueModelID.Properties.NullText = "--No Models Available, Please Select Make Before Proceeding--";
            }

            
        }

        internal void RefreshMake()
        {
            if (bool_FormLoading)
                return;
            sp_AS_11014_Make_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11014_Make_List, "", "view"); //Make
            if (lueEquipmentCategoryID.EditValue == null ? false : true)
            {
                this.spAS11014MakeListBindingSource.Filter = "EquipmentCategory = " + lueEquipmentCategoryID.EditValue;

                if (this.spAS11014MakeListBindingSource.Count == 0)
                {
                    lueMakeID.Properties.NullText = "--No Makes Available For Selected Category, Please Add--";
                    lueMakeID.Properties.AllowFocused = false;
                }
                else
                {
                    lueMakeID.Properties.NullText = "--Please Select a Make--";
                    lueMakeID.Properties.AllowFocused = true;
                }
            }
            else
            {
                this.spAS11014MakeListBindingSource.Filter = "EquipmentCategory = 0";
                lueMakeID.Properties.NullText = "--No Makes Available, Please Select Category Before Proceeding--";
            }
            
        }

        private void RefreshSupplier()
        {
            sp_AS_11020_Supplier_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11020_Supplier_List, "", "view"); //Supplier
            if (this.spAS11020SupplierListBindingSource.Count == 0)
            {
                lueSupplierID.Properties.NullText = "--No Suppliers Available, Please Add Suppliers--";
                lueSupplierID.Properties.AllowFocused = false;
            }
            else
            {
                lueSupplierID.Properties.NullText = "--Please Select a Supplier--";
                lueSupplierID.Properties.AllowFocused = true;
                //Filter suppliers based on caller
                string strSupplierFilter = "";
                switch (strCaller)
                {
                    case "frm_AS_Rental_Manager":
                        strSupplierFilter = "[RentalSupplier] = True";
                        break;
                    default:
                        strSupplierFilter = "";
                        break;
                }
                spAS11020SupplierListBindingSource.Filter = strSupplierFilter;
            }
        }

        private void Open_Child_Forms(string formName, FormMode formMode, string frmCaller)
        {
            System.Reflection.MethodInfo method = null; 
            switch (formName)
            {
                //case "frm_Makes_Model_Wizard":
                case "frm_AS_Makes_Models_Wizard":
                    frm_Makes_Model_Wizard wChildForm = new frm_Makes_Model_Wizard();
                    wChildForm.GlobalSettings = this.GlobalSettings;
                    wChildForm.strRecordIDs = "";
                    wChildForm.strFormMode = "add";
                    wChildForm.strCaller = "frmMain2";
                    //mChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    wChildForm.splashScreenManager = splashScreenManager1;
                    wChildForm.splashScreenManager.ShowWaitForm();
                    wChildForm.ShowDialog();
                    break;
                //case "frm_AS_Makes_Models_Wizard":
                //    frm_AS_Makes_Models_Wizard mChildForm = new frm_AS_Makes_Models_Wizard();
           
                //    mChildForm.GlobalSettings = this.GlobalSettings;
                //    mChildForm.strRecordIDs = "";
                //    mChildForm.strFormMode = formMode.ToString();
                //    mChildForm.strCaller = frmCaller;
                //    mChildForm.intRecordCount = 0;
                //    mChildForm.FormPermissions = this.FormPermissions;
                //    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                //    mChildForm.splashScreenManager = splashScreenManager1;
                //    mChildForm.splashScreenManager.ShowWaitForm();
                    
                //    mChildForm.ShowDialog();
                //  //  method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                //    //if (method != null) 
                //    //    method.Invoke(mChildForm, new object[] { null });
                //    break;
                case "frm_AS_Supplier_Edit":
                    frm_AS_Supplier_Edit sChildForm = new frm_AS_Supplier_Edit();
                    sChildForm.GlobalSettings = this.GlobalSettings;
                    sChildForm.MaximumSize = new System.Drawing.Size(1065, 586);
                    sChildForm.MinimumSize = new System.Drawing.Size(1065, 586);
                    sChildForm.strRecordIDs = "";
                    sChildForm.strFormMode = formMode.ToString();
                    sChildForm.strCaller = frmCaller;
                    sChildForm.intRecordCount = 0;
                    sChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    sChildForm.splashScreenManager = splashScreenManager2;
                    sChildForm.splashScreenManager.ShowWaitForm();
                    sChildForm.ShowDialog();
                    //method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    //if (method != null) method.Invoke(sChildForm, new object[] { null });
                    break;
                default:
                    MessageBox.Show("Failed to load the form.", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;

            }
        }

        private void addNewRow()
        {
            try
            {
                DataRow drNewRow;
                drNewRow = this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item.NewRow();
                drNewRow["Mode"] = "add";
                drNewRow["RecordID"] = "-1";
                if ((int)passedEquipType == 0)
                {
                    drNewRow["EquipmentCategoryID"] = "7";
                }
                else
                {
                    drNewRow["EquipmentCategoryID"] = (int)passedEquipType;
                }
                
                this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item.Rows.Add(drNewRow);


                DataRow drNewRowChild;
                drNewRowChild = this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item.NewRow();
                drNewRowChild["Mode"] = "add";
                drNewRowChild["RecordID"] = "-1";
                drNewRowChild["RentalDetailsID"] = "-1";
                drNewRowChild["SetAsMain"] = false;
                
                if (spAS11108RentalDetailsItemBindingSource.Find("RentalDetailsID", "-1") < 0)
                {
                    this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item.Rows.Add(drNewRowChild);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RefreshChildBindingSource()
        {
            spAS11102RentalItemBindingSource.CancelEdit();
        }

        private void loadRentalEquipment()
        {
            //load Rental Equipment and Child
            sp_AS_11102_Rental_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item, strRecordIDs, strFormMode);
            if (strFormMode.ToLower() == "edit")
            {
                sp_AS_11108_Rental_Details_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item, strEquipmentIDs, strFormMode + "main");
            }
            else
            {
                sp_AS_11108_Rental_Details_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item, strEquipmentIDs, strFormMode);
            }
        }
        
        private void spAS11002RentalEquipmentItemBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            if (strFormMode.ToLower() != "add" && strFormMode.ToLower() != "blockadd")
            {
                LoadLinkedRecords((((DataRowView)(spAS11102RentalItemBindingSource.CurrencyManager.Current)).Row).ItemArray[0].ToString());
            }
            //Get_ChildData();
        }
        private void LoadLinkedRecords(string strLinkedRecordIDs)
        {
            spAS11108RentalDetailsItemBindingSource.Filter ="EquipmentID = '" + strLinkedRecordIDs + "'"; 
            if (strFormMode == "edit")
            {
                string[] t = splitStrRecords(PopupContainerEdit1_Get_Selected());
                ArrayList tabs = new ArrayList();
                foreach (string h in t)
                {
                    tabs.Add(Regex.Replace(h, @"\s+", ""));
                }
                if (tabs.Contains("NoLinkedDataFilter"))
                {
                    sp_AS_11041_Transaction_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item, strLinkedRecordIDs, "view");
                    sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item, strLinkedRecordIDs, "view");
                    sp_AS_11050_Depreciation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11050_Depreciation_Item, strLinkedRecordIDs, "view");
                    sp_AS_11078_Work_Detail_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11078_Work_Detail_Item, strLinkedRecordIDs, "view");
                    sp_AS_11075_Cover_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11075_Cover_Item, strLinkedRecordIDs, "view");
                    sp_AS_11054_Service_Data_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11054_Service_Data_Item, strLinkedRecordIDs, "view");
                    sp_AS_11081_Incident_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11081_Incident_Item, strLinkedRecordIDs, "view");
                    sp_AS_11084_Purpose_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11084_Purpose_Item, strLinkedRecordIDs, "view");
                    sp_AS_11089_Fuel_Card_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_Item, strLinkedRecordIDs, "view");
                    sp_AS_11023_Tracker_ListTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11023_Tracker_List, strLinkedRecordIDs, "view");
                    sp_AS_11105_Speeding_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11105_Speeding_Item, strLinkedRecordIDs, "view");
                   // sp_AS_11096_Road_Tax_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11096_Road_Tax_Item, strLinkedRecordIDs, "view");
                    loadGridViewState();

                    //this.RefreshGridViewStateRoadTax.LoadViewInfo();
                    //this.RefreshGridViewStateSoftware.LoadViewInfo();
                    //this.RefreshGridViewStateTracker.LoadViewInfo();
                    //this.RefreshGridViewStateOffice.LoadViewInfo();
                    //this.RefreshGridViewStateBilling.LoadViewInfo();
                    //this.RefreshGridViewStateKeeper.LoadViewInfo();
                    //this.RefreshGridViewStateTransactions.LoadViewInfo();
                    //this.RefreshGridViewStateDepreciation.LoadViewInfo();
                    //this.RefreshGridViewStateCover.LoadViewInfo();
                    //this.RefreshGridViewStateServiceInterval.LoadViewInfo();
                    //this.RefreshGridViewStateWork.LoadViewInfo();
                    //this.RefreshGridViewStateIncident.LoadViewInfo();
                    //this.RefreshGridViewStatePurpose.LoadViewInfo();
                    //this.RefreshGridViewStateFuelCard.LoadViewInfo();
                    //this.RefreshGridViewStateSpeeding.LoadViewInfo();
                }
                else
                {
                    if (tabs.Contains("RoadTax"))
                    {
                        //sp_AS_11096_Road_Tax_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11096_Road_Tax_Item, strLinkedRecordIDs, "view");
                        //this.RefreshGridViewStateRoadTax.LoadViewInfo();
                    }                  

                    if (tabs.Contains("Verilocation"))
                    {
                        sp_AS_11023_Tracker_ListTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11023_Tracker_List, strLinkedRecordIDs, "view");
                    }                   

                    if (tabs.Contains("Keepers"))
                    {
                        sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item, strLinkedRecordIDs, "view");
                        this.RefreshGridViewStateKeeper.LoadViewInfo();
                    }

                    if (tabs.Contains("Transactions"))
                    {
                        sp_AS_11041_Transaction_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("DepreciationData"))
                    {
                        sp_AS_11050_Depreciation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11050_Depreciation_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("BillingDetails"))
                    {
                        sp_AS_11050_Depreciation_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11050_Depreciation_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("PolicyandInsuranceCoverDetails"))
                    {
                        sp_AS_11075_Cover_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11075_Cover_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("ServiceIntervalInformation"))
                    {
                        sp_AS_11054_Service_Data_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11054_Service_Data_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("WorkOrderDetails"))
                    {
                        sp_AS_11078_Work_Detail_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11078_Work_Detail_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("IncidentDetails"))
                    {
                        sp_AS_11081_Incident_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11081_Incident_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("AssetPurpose"))
                    {
                        sp_AS_11084_Purpose_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11084_Purpose_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("FuelCard"))
                    {
                        sp_AS_11089_Fuel_Card_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_Item, strLinkedRecordIDs, "view");
                    }

                    if (tabs.Contains("SpeedingData"))
                    {
                        sp_AS_11105_Speeding_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11105_Speeding_Item, strLinkedRecordIDs, "view");
                    }
                }

              
            }
        }
        private void PopulateRentalPickList()
        {
         sp_AS_11011_Equipment_Category_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11011_Equipment_Category_List, "", "rental"); //Equipment Category
       
        sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Equipment_Ownership_Status, 44); //Rental Status

         sp_AS_11000_PL_Equipment_AvailabilityTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Equipment_Availability, 2); //Availability

            sp_AS_11000_PL_FuelTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Fuel, 5); //Fuel Type
             
        sp_AS_11014_Make_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11014_Make_List,"","view"); //Make
         
         sp_AS_11017_Model_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11017_Model_List, "", "view"); //Model
         
         sp_AS_11020_Supplier_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11020_Supplier_List, "", "view"); //Supplier

         sp_AS_11066_Depreciation_Settings_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item,"",""); //Depreciation seting

         sp_AS_11000_PL_Rental_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Rental_Category, 41);
         sp_AS_11000_PL_Rental_StatusTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Rental_Status, 42);
         sp_AS_11000_PL_Rental_SpecificationTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Rental_Specification, 43);
         //Filter suppliers based on caller
         string strSupplierFilter = "";
         switch (strCaller)
         {
             case "frm_AS_Rental_Manager":
                 strSupplierFilter = "[RentalSupplier] = True";
                 break;
             default:
                 strSupplierFilter = "";
                 break;
         }
         spAS11020SupplierListBindingSource.Filter = strSupplierFilter;
        }

        private void OpenP11DManagerViaMain()
        {
            // Open Notification Manager via main //
            if (Application.OpenForms["frmMain2"] != null)
            {
                (Application.OpenForms["frmMain2"] as frmMain2).Open_Screen(1121);
            }

            if (Application.OpenForms["frm_AS_P11D_Manager"] != null)
            {
                (Application.OpenForms["frm_AS_P11D_Manager"] as frm_AS_P11D_Manager).LoadAdapters();
            }
        }

        private string SaveChangesExtracted(out bool shouldReturn)
        {
            shouldReturn = false;

            if (dxErrorProvider.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider, equipmentDataLayoutControl);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors +
                "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                shouldReturn = true;
                return "Error";
            }

            return String.Empty;
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //         
            EndEdit();

            this.Validate();
            ValidateChildren();
            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
                return result;

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            EndEdit();

            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item);//Rental Equipment Check
            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item);//Rental Interval

            try
            {
                // Insert and Update queries defined in Table Adapter //
                if (rentalChanged)
                    {
                      this.sp_AS_11102_Rental_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                      rentalChanged = false;
                    }
                if (rentalDetailsChanged)
                {

                    if (strFormMode.ToLower() == "add")
                    {
                        this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item.Rows[0]["EquipmentID"] = this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item[0].EquipmentID;
                        this.sp_AS_11108_Rental_Details_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                    }
                    else
                    {
                        this.sp_AS_11108_Rental_Details_ItemTableAdapter.Update(dataSet_AS_DataEntry);

                    }
                    rentalDetailsChanged = false;
                }
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }
            string strNewID = "";
            string strChildNewID = "";
            string Category = "7";
            DataRowView currentRow = (DataRowView)spAS11102RentalItemBindingSource.Current;
            if (currentRow != null)
            {
                strNewID = this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item[0].EquipmentID + ";";
            }
            DataRowView currentChildRow = (DataRowView)spAS11108RentalDetailsItemBindingSource.Current;
            if (currentChildRow != null)
            {
                strChildNewID = this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item[0].RentalDetailsID + ";";
            }
            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //     
            switch (strCaller)
            {
                case "frm_AS_Rental_Manager":
                    if (formMode == FormMode.add)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).UpdateFormRefreshStatus(1, strNewID, strChildNewID, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, Category);
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", Category);
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).frmActivated();
                        }
                    }
                    break;
                
            }
           
            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();

            if (P11dManagerPromptRequired)
            {
                switch (XtraMessageBox.Show("Would you like to view the P11D Manager to email changes?", "Open P11D", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.No:
                        // cancel operation //
                        break;
                    case DialogResult.Yes:
                        OpenP11DManagerViaMain();
                        break;
                }

                P11dManagerPromptRequired = false;
            }

            return "";  // No problems //
        }

        private string[] splitStrRecords(string t)
        {
            char[] delimiters = new char[] { ',' };
            string[] parts = t.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            return parts;
        }

        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
                if (item2 is DevExpress.XtraDataLayout.DataLayoutControl) this.Attach_EditValueChanged_To_Children(item.Controls);

            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }

        private Boolean SetChangesPendingLabel()
        {
            EndEdit();

            DataSet dsChanges = this.dataSet_AS_DataEntry.GetChanges();
            if (dsChanges != null) 
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "block_add" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;

                if (strFormMode.ToLower() == "add")
                {
                    bbiFormSave.Enabled = true;
                }
                return false;
            }

           
        }

        private void EndEdit()
        {
            spAS11102RentalItemBindingSource.EndEdit();
            spAS11108RentalDetailsItemBindingSource.EndEdit();
        }

        private string CheckForPendingSave()
        {
            EndEdit();

            string strMessage = "";
            string strMessageRentalEquip = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item, " on the Rental Equipment Tab Page ");//Rental Check
            string strMessageRentalInterval = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item, " on the Rental Interval Tab Page ");//Rental ?Interval Check

            if (strMessageRentalEquip != "" || strMessageRentalInterval != "")
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (strMessageRentalEquip != "") strMessage += strMessageRentalEquip;
                if (strMessageRentalInterval != "") strMessage += strMessageRentalInterval;
            }
            return strMessage;
        }

        private string CheckTablePendingSave(DataTable dt, string formArea)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

  
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)" + formArea + "\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)" + formArea + "\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)" + formArea + "\n";
            }
            return strMessage;
        }

        private void CheckChangedTable(DataTable dt)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                IdentifyChangedBindingSource(dt);
            }
        }
        
        private void IdentifyChangedBindingSource(DataTable dt)
        {

            switch (dt.TableName)
            {
                case "sp_AS_11102_Rental_Item"://Rental
                    rentalChanged = true;
                    break;
                case "sp_AS_11108_Rental_Details_Item"://Rental Interval
                    rentalDetailsChanged = true;
                    break;
            }
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            bool_FormLoading = false;
            RefreshMake();

        }

        #region ValidateMethod

        private void txtModelSpecifications_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Title, EquipmentType.None))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void txtUniqueReference_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.None))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private bool validateLookupEdit(LookUpEdit lookupEdit, EquipmentType equipmentType)
        {

            bool valid = true;
            if (bool_FormLoading)
                return valid;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(lueEquipmentCategoryID.EditValue))
                {
                    dxErrorProvider.SetError(lookupEdit, "");
                    return valid;
                }
            }

            string ErrorText = "Please Select Value.";

            if ( lookupEdit.Tag == null ? false : true)
            {
                ErrorText = "Please Select " + lookupEdit.Tag + ".";
            }
            if ((lookupEdit.EditValue == null) || this.strFormMode.ToLower() != "blockedit" && lookupEdit.Properties.GetDisplayText(lookupEdit.EditValue) == "")
            {
                dxErrorProvider.SetError(lookupEdit, ErrorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }
            else
            {
                dxErrorProvider.SetError(lookupEdit, "");
                return valid;
            }
        }

        private bool validateTextBox(TextEdit txtBox, SentenceCase SentenceCase, EquipmentType equipmentType)
        {
            if (bool_FormLoading)
                return true;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(lueEquipmentCategoryID.EditValue))
                {
                    dxErrorProvider.SetError(txtBox, "");
                    return true;
                }
            }
            bool valid = false;
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.Text = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.Text = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            string originalText = txtBox.Text;
            string parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9]", "");
            if (txtBox.Name == "txtModelSpecifications")
            {
                parsedText = Regex.Replace(txtBox.Text, "[^/a-z_A-Z0-9]", ""); 
            }
            //check non alphanumeric characters
            if (txtBox.Name == "txtUniqueReference")
            {
                if (originalText != parsedText || txtBox.Text == "")
                {
                    dxErrorProvider.SetError(txtBox, "Please enter a valid text, avoid entering non alphanumeric characters, invalid characters have been removed");
                    return valid;
                }
            }
            else
            if (originalText != parsedText)
            {
                dxErrorProvider.SetError(txtBox, "Please enter a valid text, avoid entering non alphanumeric characters, invalid characters have been removed");
                return valid;
            }
            

            
            //check duplication last
            if (txtBox.Name == "txtUniqueReference")
            {
                string searchMode, searchValue;
                
                if (forceCloseForm)
                {
                    this.Close();
                }
                searchMode = "rental";
                searchValue = txtBox.Text;

                valid = checkDuplicate(searchValue, searchMode, EquipmentType.Rental);
                if (!valid)
                {
                    switch (XtraMessageBox.Show("Duplicate Unique Reference already exists, do you want to add a new rental interval to this previously rented equipment?", "Edit Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                    {
                        case DialogResult.No:
                            // cancel operation //
                            dxErrorProvider.SetError(txtBox, "Duplicate Unique Reference already exists");
                            break;
                        case DialogResult.Yes:
                            // change to edit //
                            forceCloseForm = true;
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).RentalIntervalEdit(frm_AS_Rental_Manager.FormMode.add, strCaller, intRecordCount);
                           
                            break;
                    }
                    //dxErrorProvider.SetError(txtBox, "Duplicate Unique Reference already exists");
                }
                
            }
            else
            {
                valid = true;
            }
            if (valid)
            {
                dxErrorProvider.SetError(txtBox, "");
            }
            return valid;
        }

        #endregion

        private bool checkDuplicate(string searchValue, string searchMode,EquipmentType equipmentType )
        {
            bool dup = true;

            switch (strFormMode.ToLower())
            {
                case "add":
                    sp_AS_11000_Duplicate_SearchTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search, searchValue, Convert.ToInt32(lueEquipmentCategoryID.EditValue), searchMode);

                    switch (spAS11000DuplicateSearchBindingSource.Count)
                    {
                        case 0:
                            dup = true;
                            break;
                        case 1:
                            dup = false;
                            break;
                        default:
                            dup = true;
                            break;
                    }
                        break;
                case "edit":
                        string whereClause = "[UniqueReference] ='" + searchValue + "'";
                        dup = returnDuplicateStatus(whereClause, this.spAS11102RentalItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11102_Rental_Item);       
                        break;
            }
            return dup;
        }
        
        private bool returnDuplicateStatus(string whereClause, BindingSource bs, DataTable dt)
        {

            if (dt.Select(whereClause).GetLength(0) > 1)
            {
                bs.ResetCurrentItem();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void checkPreliminaryFields()
        {
            MessageBox.Show("Please select equipment category before changing this field", "Preliminary fields to be completed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            dxErrorProvider.SetError(lueEquipmentCategoryID, "Please Select Category.");
            this.lueEquipmentCategoryID.Focus();
        }

        #endregion

        #region Data Navigator

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion
              
        #region EquipmentValidator
        private void lueEquipmentCategoryID_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading && strFormMode.ToLower() != "add")
                return;

            if (lueEquipmentCategoryID.EditValue != null)
            {
                if (lueEquipmentCategoryID.Properties.GetDisplayText(lueEquipmentCategoryID.EditValue) != "")
                {
                    RejectChildChanges();
                    ClearErrors(this.Controls);
                    RefreshMake();
                    RefreshModel();

                    try
                    {
                        switch (strFormMode.ToLower())
                        {
                            case "add":
                                if (bool_FormLoading)
                                {
                                    ChildVisibility(0, (passedEquipType == EquipmentType.None ? 7 : (int)passedEquipType));
                                }
                                else
                                {
                                    RefreshChildBindingSource();
                                    ChildVisibility(0, Convert.ToInt32(lueEquipmentCategoryID.EditValue));
                                }

                                this.lueMakeID.EditValue = null;
                                break;

                            case "block_add":

                                break;

                            case "edit":
                            case "view":
                                DataRowView currRow = (DataRowView)spAS11102RentalItemBindingSource.Current;
                                ChildVisibility(Convert.ToInt32(currRow["EquipmentID"]), Convert.ToInt32(lueEquipmentCategoryID.EditValue));
                                if (currRow.Row.RowState == DataRowState.Modified)
                                {
                                    this.lueMakeID.EditValue = null;
                                }
                                break;
                            case "blockedit":
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                }
            }
        }

        private void lueOwnership_Status_Validating(object sender, CancelEventArgs e)
        {        
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueSupplier_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueModel_Validating(object sender, CancelEventArgs e)
        {         
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueMake_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueEquipment_Category_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }
        
        private void lueMake_EditValueChanged(object sender, EventArgs e)
        {

            if (lueMakeID.EditValue != null)
            {
                if (lueMakeID.EditValue.ToString() != "")
                {
                    RefreshModel();
                    try
                    {
                        switch (strFormMode.ToLower())
                        {
                            case "add":

                                // ChildVisibility(0, (int)passedEquipType);

                                //CategoryEditValueChangedCount+=1;
                                this.lueModelID.EditValue = null;
                                break;

                            case "block_add":

                                break;

                            case "edit":
                            //DataRowView currRow = (DataRowView)spAS11102RentalItemBindingSource.Current;
                            //ChildVisibility(Convert.ToInt32(currRow["EquipmentID"]), Convert.ToInt32(currRow["EquipmentCategory"]));
                            //break;
                            case "view":
                                DataRowView currRow = (DataRowView)spAS11102RentalItemBindingSource.Current;
                                //ChildVisibility(Convert.ToInt32(currRow["EquipmentID"]), Convert.ToInt32(currRow["EquipmentCategory"]));
                                if (currRow.Row.RowState == DataRowState.Modified)
                                {
                                    this.lueModelID.EditValue = null;
                                }
                                break;
                            case "blockedit":
     
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                }
            }
            else
            {
                this.lueModelID.EditValue = null;
            }
        }
        
        private void lueSupplier_EditValueChanged(object sender, EventArgs e)
        {
            RefreshSupplier();
        }

        #endregion

        #region Child Objects

        #region Child Validate Method

        private bool validateChildTextBox(TextEdit txtBox, SentenceCase SentenceCase)
        {
            if (bool_FormLoading)
                return true;

            bool valid = false;
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.Text = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.Text = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            string originalText = txtBox.Text;
            string parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9]", "");
            if (strFormMode.ToLower() == "blockedit")
            {
                valid = true;
            }
            else
            {
                //check non alphanumeric characters
                if (originalText != parsedText || txtBox.Text == "")
                {
                    dxErrorProvider.SetError(txtBox, "Please enter a valid text, avoid entering non alphanumeric characters, invalid characters have been removed");
                    return valid;
                }
                else
                {
                    valid = true;
                }
            }
            if (valid)
            {
                dxErrorProvider.SetError(txtBox, "");
            }
            return valid;
        }

        private bool validateChildLookupEdit(LookUpEdit lookupEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;

            string ErrorText = "Please Select Value.";

            if (lookupEdit.Tag == null ? false : true)
            {
                ErrorText = "Please Select " + lookupEdit.Tag.ToString() + ".";
            }
            if (this.strFormMode.ToLower() != "blockedit" && (lookupEdit.EditValue == null || lookupEdit.EditValue.ToString() == ""))
            { 
                dxErrorProvider.SetError(lookupEdit, ErrorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }
            else
            {
                dxErrorProvider.SetError(lookupEdit, "");
                return valid;
            }
        }

        private bool validateChildDateEdit(DateEdit dateEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;

            string ErrorText = "Please Enter a Valid Date.";

            if (dateEdit.Tag == null ? false : true)
            {
                ErrorText = "Please Enter a valid " + dateEdit.Tag.ToString() + ".";
            }
            if (this.strFormMode.ToLower() != "blockedit" && (dateEdit.EditValue == null || string.IsNullOrEmpty(dateEdit.EditValue.ToString())))
            {
                dxErrorProvider.SetError(dateEdit, ErrorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }
            else
            {                
                dxErrorProvider.SetError(dateEdit, "");
                return valid;
            }
        }

        private bool validateChildSpinEdit(SpinEdit spnEdit)
        {
            bool valid = false;

            if (bool_FormLoading)
                return valid;
            string originalText = spnEdit.Text;
            string parsedText = Regex.Replace(spnEdit.Text, "[^0-9].£", "");


            //check non alphanumeric characters
            if (originalText != parsedText)
            {
                dxErrorProvider.SetError(spnEdit, "Please enter valid amount, avoid entering non alphanumeric characters, invalid characters have been removed.");
                return valid;
            }
            else
            {
                valid = true;
            }

            if (spnEdit.EditValue == null || spnEdit.EditValue.ToString() == "")
            {
                dxErrorProvider.SetError(spnEdit, "Please enter a valid " + spnEdit.Tag + " value");
                //spnEdit.Focus();
                return valid = false;  // Show stop icon as field is invalid //
            }

            if (valid)
            {
                dxErrorProvider.SetError(spnEdit, "");
            }
            return valid;

        }

        #endregion         
        
        #region Child EditorEvents

        private void lueRentalCategoryID_Validating(object sender, CancelEventArgs e)
        {
            if (validateChildLookupEdit((LookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        //private void lueRentalCategoryID_Validating(object sender, CancelEventArgs e)
        //{
        //    if (validateLookupEdit((LookUpEdit)sender))
        //    {
        //        e.Cancel = false;// Remove stop icon as field is valid //
        //    }
        //    else
        //    {
        //        e.Cancel = true;// Show stop icon as field is invalid //
        //    }
        //}

        private void deEndDate_EditValueChanged(object sender, EventArgs e)
        {
            if (deEndDate.EditValue != null && !string.IsNullOrEmpty(deEndDate.EditValue.ToString()))
            {
                deActualEndDate.EditValue = deEndDate.EditValue;
            }
        }

        private void deActualEndDate_Validating(object sender, CancelEventArgs e)
        {
            if (validateChildDateEdit((DateEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void spnRate_Validating(object sender, CancelEventArgs e)
        {
            if (validateChildSpinEdit((SpinEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void txtCustomerReference_Validating(object sender, CancelEventArgs e)
        {
            if (validateChildTextBox((TextEdit)sender, SentenceCase.Upper))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void deStartDate_Validating(object sender, CancelEventArgs e)
        {
            if (validateChildDateEdit((DateEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }


        private void deEndDate_Validating(object sender, CancelEventArgs e)
        {
            if (validateChildDateEdit((DateEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

 

        private void lueRentalStatusID_Validating(object sender, CancelEventArgs e)
        {
            if (validateChildLookupEdit((LookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueRentalSpecificationID_Validating(object sender, CancelEventArgs e)
        {
            if (validateChildLookupEdit((LookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }
   


        private void makeLookUpEdit_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("Add".Equals(e.Button.Tag)) // Add Make Button 
                {                    
                    Open_Child_Forms("frm_AS_Makes_Models_Wizard", FormMode.view,"frm_AS_Rental_Edit");  
                }
                else if ("Refresh".Equals(e.Button.Tag)) //Refresh Make List
                {                    
                    RefreshMake();
                }
            }
        }

        private void modelLookUpEdit_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("Add".Equals(e.Button.Tag)) // Add Make Button 
                {
                    Open_Child_Forms("frm_AS_Makes_Models_Wizard", FormMode.edit, "frm_AS_Rental_Edit");
                }
                else if ("Refresh".Equals(e.Button.Tag)) //Refresh Make List
                {
                    RefreshModel();
                }
            }
        }

        private void supplierLookUpEdit_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("Add".Equals(e.Button.Tag)) // Add Make Button 
                {
                    Open_Child_Forms("frm_AS_Supplier_Edit", FormMode.add, "frm_AS_Rental_Edit");
                }
                else if ("Refresh".Equals(e.Button.Tag)) //Refresh Make List
                {
                    RefreshSupplier();
                }
            }
        }

        #endregion
        
        #region Common Child Events & Methods

        #region CommonChildEvents

        //private void childLayoutControlGroup_Shown(object sender, EventArgs e)
        //{
        //    NoDetailsLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        //}

        #endregion

       
        #region linkedChildMethodsAnd Events

        private void btnPopUp1Ok_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }
        
        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();

            linkedchildVisibility();
            
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            shownTabs = "";
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0)
            {
               
                return "No Linked Data Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                return "No Linked Data Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        
                        if (intCount == 0)
                        {
                            shownTabs = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            shownTabs += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }

            return (string.IsNullOrEmpty(shownTabs) ? "No Linked Data Filter" : shownTabs);
        }
        
        private void linkedchildVisibility()
        {
            string[] t = splitStrRecords(PopupContainerEdit1_Get_Selected());
            ArrayList tabs  = new ArrayList();
            foreach (string h in t)
            {
                tabs.Add(Regex.Replace(h, @"\s+", ""));
            }
            if (tabs.Contains("NoLinkedDataFilter"))
            {
                equipmentChildTabControl.TabPages.Add(roadTaxTabPage);
                equipmentChildTabControl.TabPages.Add(trackerTabPage);
                equipmentChildTabControl.TabPages.Add(keeperTabPage);
                equipmentChildTabControl.TabPages.Add(transactionTabPage);
                equipmentChildTabControl.TabPages.Add(depreciationTabPage);
                equipmentChildTabControl.TabPages.Add(billingTabPage);
                equipmentChildTabControl.TabPages.Add(coverTabPage);
                equipmentChildTabControl.TabPages.Add(serviceIntervalTabPage);
                equipmentChildTabControl.TabPages.Add(workDetailTabPage);
                equipmentChildTabControl.TabPages.Add(incidentTabPage);
                equipmentChildTabControl.TabPages.Add(purposeTabPage);
                equipmentChildTabControl.TabPages.Add(fuelCardTabPage);
                equipmentChildTabControl.TabPages.Add(speedingTabPage);
            }
            else
            {
                if (tabs.Contains("RoadTax"))
                {
                    equipmentChildTabControl.TabPages.Add(roadTaxTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(roadTaxTabPage);
                }

                if (tabs.Contains("Verilocation"))
                {
                    equipmentChildTabControl.TabPages.Add(trackerTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(trackerTabPage);
                }

                if (tabs.Contains("Keepers"))
                {
                    equipmentChildTabControl.TabPages.Add(keeperTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(keeperTabPage);
                }

                if (tabs.Contains("Transactions"))
                {
                    equipmentChildTabControl.TabPages.Add(transactionTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(transactionTabPage);
                }

                if (tabs.Contains("DepreciationData"))
                {
                    equipmentChildTabControl.TabPages.Add(depreciationTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(depreciationTabPage);
                }

                if (tabs.Contains("BillingDetails"))
                {
                    equipmentChildTabControl.TabPages.Add(billingTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(billingTabPage);
                }

                if (tabs.Contains("PolicyandInsuranceCoverDetails"))
                {
                    equipmentChildTabControl.TabPages.Add(coverTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(coverTabPage);
                }

                if (tabs.Contains("ServiceIntervalInformation"))
                {
                    equipmentChildTabControl.TabPages.Add(serviceIntervalTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(serviceIntervalTabPage);
                }

                if (tabs.Contains("WorkOrderDetails"))
                {
                    equipmentChildTabControl.TabPages.Add(workDetailTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(workDetailTabPage);
                }

                if (tabs.Contains("IncidentDetails"))
                {
                    equipmentChildTabControl.TabPages.Add(incidentTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(incidentTabPage);
                }

                if (tabs.Contains("AssetPurpose"))
                {
                    equipmentChildTabControl.TabPages.Add(purposeTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(purposeTabPage);
                }

                if (tabs.Contains("FuelCard"))
                {
                    equipmentChildTabControl.TabPages.Add(fuelCardTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(fuelCardTabPage);
                }

                if (tabs.Contains("SpeedingData"))
                {
                    equipmentChildTabControl.TabPages.Add(speedingTabPage);
                }
                else
                {
                    equipmentChildTabControl.TabPages.Remove(speedingTabPage);
                }
            }
          
        }

        #region childGridView


        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }

        private void Load_linkedData()
        {
            if (UpdateRefreshStatus > 0)
                UpdateRefreshStatus = 0;
            LoadLinkedRecords((((DataRowView)(spAS11102RentalItemBindingSource.CurrencyManager.Current)).Row).ItemArray[0].ToString());
            //this.RefreshGridViewStateEquipment.LoadViewInfo();  // Reload any expanded groups and selected rows //
            loadGridViewState();

            //// Highlight any recently added Equipment new rows //
            //if (i_str_AddedRecordIDs1 != "")
            //{
            //    string[] strArray = splitStrRecords(i_str_AddedRecordIDs1);//i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            //    int intID = 0;
            //    int intRowHandle = 0;
            //    GridView view = (GridView)equipmentGridControl.MainView;
            //    view.ClearSelection(); // Clear any current selection so just the new record is selected //
            //    foreach (string strElement in strArray)
            //    {
            //        intID = Convert.ToInt32(strElement);
            //        intRowHandle = view.LocateByValue(0, view.Columns["EquipmentID"], intID);
            //        if (intRowHandle != GridControl.InvalidRowHandle)
            //        {
            //            view.MakeRowVisible(intRowHandle, false);
            //            view.SelectRow(intRowHandle);
            //        }
            //    }
            //    i_str_AddedRecordIDs1 = "";
            //}
            if (i_str_AddedRecordIDs2 != "")
            {
                highlightChildrow();
            }
        }

        private void highlightChildrow()
        {
            GridView view = null;
            string viewID = "";
            switch (i_int_FocusedGrid)
            {
              
                case 19://Road Tax
                    // Highlight any recently added Road Tax new rows //
                    view = (GridView)roadTaxGridControl.MainView;
                    viewID = "RoadTaxID";
                    break;
                case 4://Tracker
                    // Highlight any recently added Tracker new rows //
                    view = (GridView)trackerGridControl.MainView;
                    viewID = "TrackerInformationID";
                    break;
               
                case 7://billing
                    // Highlight any recently added billing new rows //
                    view = (GridView)billingGridControl.MainView;
                    viewID = "DepreciationID";
                    break;
                case 8://keeper
                    // Highlight any recently added keepers new rows //
                    view = (GridView)keeperGridControl.MainView;
                    viewID = "KeeperAllocationID";
                    break;
                case 9://transactions
                    // Highlight any recently added transaction new rows //
                    view = (GridView)transactionGridControl.MainView;
                    viewID = "TransactionID";
                    break;
                case 11://depreciation
                    // Highlight any recently added depreciation new rows //
                    view = (GridView)depreciationGridControl.MainView;
                    viewID = "DepreciationID";
                    break;
                case 12://Incident
                    // Highlight any recently added Incident new rows //
                    view = (GridView)incidentGridControl.MainView;
                    viewID = "IncidentID";
                    break;
                case 13://Service Interval
                    // Highlight any recently added ServiceInterval new rows //
                    view = (GridView)serviceIntervalGridControl.MainView;
                    viewID = "ServiceIntervalID";
                    break;
                case 14://Cover
                    // Highlight any recently added Cover new rows //
                    view = (GridView)coverGridControl.MainView;
                    viewID = "CoverID";
                    break;
                case 15://work
                    // Highlight any recently added workService new rows //
                    view = (GridView)workDetailGridControl.MainView;
                    viewID = "WorkDetailID";
                    break;
                case 16://Purpose
                    // Highlight any recently added Purpose new rows //
                    view = (GridView)purposeGridControl.MainView;
                    viewID = "EquipmentPurposeID";
                    break;
                case 17://Fuel Cards
                    // Highlight any recently added Fuel Cards new rows //
                    view = (GridView)fuelCardGridControl.MainView;
                    viewID = "FuelCardID";
                    break;
                case 18://Speeding
                    // Highlight any recently added Speeding new rows //
                    view = (GridView)speedingGridControl.MainView;
                    viewID = "SpeedingID";
                    break;
            }
            
            if (i_str_AddedRecordIDs2 != "")
            {
                string[] strArray = splitStrRecords(i_str_AddedRecordIDs2);
                int intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns[viewID], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.MakeRowVisible(intRowHandle, false);
                        view.SelectRow(intRowHandle);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }

        }
        
        private void childGridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {                
                case "roadTaxGridView":
                     message = "No Road Tax Available";
                    break;
                case "trackerGridView":
                     message = "No Verilocation Tracker Information Available";
                    break;               
                case "keeperGridView":
                     message = "No Keeper Details Available";
                    break;
                case "transactionsGridView":
                     message = "No Transactions Details Available";
                    break;
                case "billingGridView":
                     message = "No Billings Information Available";
                    break;
                case "depreciationGridView":
                     message = "No Depreciation Data Available";
                    break;
                case "incidentGridView":
                     message = "No Incident Data Available";
                    break;
                case "serviceIntervalGridView":
                     message = "No Service Interval Data Available";
                    break;
                case "coverGridView":
                     message = "No Cover Data Available";
                    break;
                case "workDetailGridView":
                     message = "No Work Order Detail Data Available";
                    break;
                case "purposeGridView":
                     message = "No Equipment Purpose Data Available";
                    break;
                case "fuelCardGridView":
                     message = "No Fuel Card Data Available";
                    break;
                case "speedingGridView":
                     message = "No Speeding Data Available";
                    break;
                default:
                     message = "No Details Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void childGridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning)
                return;
            isRunning = true;
            GridView view = sender as GridView;
            childGridViewSelectionChanged(view, e);
        }

        private void commonGridView_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                if (i_int_FocusedGrid != 18)
                {
                    Edit_Record();
                }
            }
        }

        private void commonGridView_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            string GridName = ((GridView)sender).Name;
            switch (GridName)
            {
                case "roadTaxGridView":
                    i_int_FocusedGrid = 19;
                    break;

                case "trackerGridView":
                    i_int_FocusedGrid = 4;
                    break;

                case "softwareGridView":
                    i_int_FocusedGrid = 5;
                    break;

                case "officeGridView":
                    i_int_FocusedGrid = 6;
                    break;

                case "billingGridView":
                    i_int_FocusedGrid = 7;
                    break;

                case "keeperGridView":
                    i_int_FocusedGrid = 8;
                    break;

                case "transactionsGridView":
                    i_int_FocusedGrid = 9;
                    break;

                //case "reminderGridView":
                //    i_int_FocusedGrid = 10;
                //    break;
                case "depreciationGridView":
                    i_int_FocusedGrid = 11;
                    break;
                case "incidentGridView":
                    i_int_FocusedGrid = 12;
                    break;
                case "serviceIntervalGridView":
                    i_int_FocusedGrid = 13;
                    break;
                case "coverGridView":
                    i_int_FocusedGrid = 14;
                    break;
                case "workDetailGridView":
                    i_int_FocusedGrid = 15;
                    break;
                case "purposeGridView":
                    i_int_FocusedGrid = 16;
                    break;
                case "fuelCardGridView":
                    i_int_FocusedGrid = 17;
                    break;
                case "speedingGridView":
                    i_int_FocusedGrid = 18;
                    break;
            }

            SetMenuStatus();
        }

        private void commonGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("insert".Equals(e.Button.Tag))
                    {
                        Insert_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void commonGridView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void customFilterDraw(GridView view, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(view, e);
        }

        private void childGridViewSelectionChanged(GridView view, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(view, e, ref isRunning);
            SetMenuStatus();

            if (i_int_FocusedGrid == 12)//Incident
            {
                GridView childView = incidentGridView;
                int numOfSelectedIncidentInfoRows = childView.SelectedRowsCount;
                int tempNumOfSelectedRows;
                if (numOfSelectedIncidentInfoRows > 0)
                {
                    tempNumOfSelectedRows = numOfSelectedIncidentInfoRows;
                }
                else
                {
                    tempNumOfSelectedRows = 1;
                }

                int[] IDs = new int[tempNumOfSelectedRows];
                int[] intRowHandles = childView.GetSelectedRows();

                if (numOfSelectedIncidentInfoRows > 0)
                {
                    int countRows = 0;

                    foreach (int intRowHandle in intRowHandles)
                    {
                        DataRow dr = childView.GetDataRow(intRowHandle);
                        IDs[countRows] = (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)(dr)).IncidentID);
                        countRows += 1;
                    }
                }
                else
                {
                    IDs[0] = 0;
                }
                //load linked incident losses records
                // sp_AS_11081_Incident_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11081_Incident_Item, stringRecords(IDs), "view");
            }
        }


        #region CRUD

        #region CRUD Methods

        private void Add_Record()
        {

            switch (i_int_FocusedGrid)
            {                
                case 19:     // Road Tax //
                    if (!iBool_AllowAddRoadTax)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", roadTaxGridControl, EquipmentType.None);
                    break;
                case 4:     // Tracker //
                    if (!iBool_AllowAddTracker)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", trackerGridControl, EquipmentType.None);
                    break;                
                case 7:     // Billings //
                    if (!iBool_AllowAddBilling)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowAddKeeper)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowAddTransactions)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", transactionGridControl, EquipmentType.None);
                    break;
                //case 10:     // reminders //
                //    if (!iBool_AllowAddReminders)
                //        return;
                //    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", officeGridControl, EquipmentType.None);
                //    break;
                case 11:     // depreciation //
                    if (!iBool_AllowAddDepreciation)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // incident //
                    if (!iBool_AllowAddIncident)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceInterval //
                    if (!iBool_AllowAddServiceInterval)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", serviceIntervalGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowAddCover)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // work //
                    if (!iBool_AllowAddWorkDetail)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowAddPurpose)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowAddFuelCard)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", fuelCardGridControl, EquipmentType.None);
                    break;
                case 18:     // Speeding //
                    if (!iBool_AllowAddSpeeding)
                        return;
                    OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", speedingGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }
        }

        private void Insert_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 18:
                    if (!iBool_AllowAddIncident)
                        return;

                    string autoInsertIncident = "Do you want to auto add selected speeding record(s) into the incident register?";
                    switch (XtraMessageBox.Show(autoInsertIncident, "Auto Insert Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                    {
                        case DialogResult.No:
                            // cancel operation //
                            XtraMessageBox.Show("You can manually enter the incident in the Incident Form", "Opening Incident Form", MessageBoxButtons.OK);
                            OpenEditForm(FormMode.add, "frm_AS_Rental_Edit", incidentGridControl, EquipmentType.None);
                            break;
                        case DialogResult.Yes:
                            DateTime dateHappened;
                            string district, notes;
                            int equipID;

                            this.sp_AS_11000_PL_Incident_TypeTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Type, 31);
                            this.sp_AS_11000_PL_Incident_StatusTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Status, 30);
                            this.sp_AS_11000_PL_Legal_ActionTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Legal_Action, 38);
                            this.sp_AS_11000_PL_SeverityTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Severity, 36);
                            this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Repair_Due, 32);
                            this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Keeper_At_Fault, 37);
                            // insert record into incident table //
                            // DataRow[] speedingDR = this.dataSet_AS_DataEntry.sp_AS_11105_Speeding_Item.Select("SpeedingID  = '");

                            //int equipID = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11105_Over_Speeding_ItemRow)(speedingDR[0])).EquipmentID;

                            int incStatusID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Status.Select("Value = 'Open'"))[0]["PickListID"]);
                            int incTypeID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Type.Select("Value = 'Speeding'"))[0]["PickListID"]);
                            int incLegalActionID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Legal_Action.Select("Value = 'None'"))[0]["PickListID"]);
                            int incSeverityID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Severity.Select("Value = 'High'"))[0]["PickListID"]);
                            int incKeeper_At_FaultID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Keeper_At_Fault.Select("Value = 'No'"))[0]["PickListID"]);
                            int incRepairDueID = Convert.ToInt32((this.dataSet_AS_DataEntry.sp_AS_11000_PL_Incident_Repair_Due.Select("Value = 'None'"))[0]["PickListID"]);
                            //-->insert record
                            GridView view = null;

                            int[] intRowHandles;
                            int intCount = 0;
                            int rowInsertCount = 0;
                            view = (GridView)speedingGridControl.MainView;
                            view.PostEditor();
                            intRowHandles = view.GetSelectedRows();
                            intCount = intRowHandles.Length;
                            string strUpdatedEquipID = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                DataRow dr = view.GetDataRow(intRowHandle);
                                district = ((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).District;
                                dateHappened = ((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).TravelTime;
                                equipID = ((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).EquipmentID;
                                strUpdatedEquipID += equipID + ";";
                                notes = "Speed of " + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).SpeedMPH).ToString() + " MPH recorded on Road Type "
                                    + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).RoadType).ToString() + " at location "
                                    + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).PlaceName).ToString() + " in the "
                                    + (((DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).District).ToString() + " area.";
                                this.sp_AS_11081_Incident_ItemTableAdapter.Insert("add", "", equipID, 0, incStatusID, incTypeID, "VERILOCATION SPEEDING", dateHappened, district, incRepairDueID, incSeverityID, "Verilocation", incKeeper_At_FaultID, incLegalActionID, dateHappened.AddDays(7), 0, notes);

                                rowInsertCount++;
                            }
                                                        
                            XtraMessageBox.Show(rowInsertCount + " Speeding incidents added.", "Auto Update Complete", MessageBoxButtons.OK);


                            break;
                    }
                    break;
            }
        }

        private void Block_Add()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Equipment //     
                case 1:     // vehicle //
                case 2:     // plant //
                // case 3:     // gadget //
                //case 4:     // hardware //
                case 5:     // software //
                case 6:     // office //                       
                    XtraMessageBox.Show("Block adding is restricted.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    break;
                case 7:     // Billings //
                    if (!iBool_AllowAddBilling)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowAddKeeper)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowAddTransactions)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", transactionGridControl, EquipmentType.None);
                    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowAddDepreciation)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowAddIncident)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceInterval //
                    if (!iBool_AllowAddServiceInterval)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", serviceIntervalGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowAddCover)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowAddWorkDetail)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowAddPurpose)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowAddFuelCard)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", fuelCardGridControl, EquipmentType.None);
                    break;
                case 19:     // Road Tax //
                    if (!iBool_AllowAddRoadTax)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", roadTaxGridControl, EquipmentType.None);
                    break;
                case 4:     // Tracker Information //
                    if (!iBool_AllowAddTracker)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", trackerGridControl, EquipmentType.None);
                    break;
                case 18:     // speeding //
                    if (!iBool_AllowAddSpeeding)
                        return;
                    OpenEditForm(FormMode.blockadd, "frm_AS_Rental_Edit", speedingGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            switch (i_int_FocusedGrid)
            {
                case 19:     // Road Tax //
                    if (!iBool_AllowEditRoadTax)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", roadTaxGridControl, EquipmentType.None);
                    break;
                case 4:     // Tracker //
                    if (!iBool_AllowEditTracker)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", trackerGridControl, EquipmentType.None);
                    break;
               
                case 7:     // Billings //
                    if (!iBool_AllowEditBilling)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowEditKeeper)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowEditTransactions)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", transactionGridControl, EquipmentType.None);
                    break;
                //case 10:     // Reminders //
                //    if (!iBool_AllowEditReminders)
                //        return;
                //    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", officeGridControl, EquipmentType.Office);
                //    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowEditDepreciation)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowEditIncident)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceInterval //
                    if (!iBool_AllowEditServiceInterval)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", serviceIntervalGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowEditCover)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowEditWorkDetail)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowEditPurpose)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowEditFuelCard)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", fuelCardGridControl, EquipmentType.None);
                    break;
                case 18:     // Speeding //
                    if (!iBool_AllowEditSpeeding)
                        return;
                    OpenEditForm(FormMode.blockedit, "frm_AS_Rental_Edit", speedingGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 19:     // Road Tax //
                    if (!iBool_AllowEditRoadTax)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", roadTaxGridControl, EquipmentType.None);
                    break;
                case 4:     // Tracker //
                    if (!iBool_AllowEditTracker)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", trackerGridControl, EquipmentType.None);
                    break;
               
                case 7:     // Billings //
                    if (!iBool_AllowEditBilling)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowEditKeeper)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowEditTransactions)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", transactionGridControl, EquipmentType.None);
                    break;
                //case 10:     // Reminders //
                //    if (!iBool_AllowEditReminders)
                //        return;
                //    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", officeGridControl, EquipmentType.Office);
                //    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowEditDepreciation)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowEditIncident)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceInterval //
                    if (!iBool_AllowEditServiceInterval)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", serviceIntervalGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowEditCover)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowEditWorkDetail)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowEditPurpose)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowEditFuelCard)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", fuelCardGridControl, EquipmentType.None);
                    break;
                case 18:     // speeding //
                    if (!iBool_AllowEditSpeeding)
                        return;
                    OpenEditForm(FormMode.edit, "frm_AS_Rental_Edit", speedingGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }

        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridControl gridControl = null;
            GridView view = null;
            string strMessage = "";


            switch (i_int_FocusedGrid)
            {
                case 19:     // road tax
                    if (!iBool_AllowDeleteRoadTax)
                        return;
                    break;
                case 4:     // Tracker 
                    if (!iBool_AllowDeleteTracker)
                        return;
                    break;
               
                case 7:     // Billing
                    if (!iBool_AllowDeleteBilling)
                        return;
                    break;
                case 8:     // Keeper
                    if (!iBool_AllowDeleteKeeper)
                        return;
                    break;
                case 9:     // Transaction
                    if (!iBool_AllowDeleteTransactions)
                        return;
                    break;
                case 11:     // Depreciation
                    if (!iBool_AllowDeleteDepreciation)
                        return;
                    break;
                case 12:     // Incident
                    if (!iBool_AllowDeleteIncident)
                        return;
                    break;
                case 13:     // ServiceInterval
                    if (!iBool_AllowDeleteServiceInterval)
                        return;
                    break;
                case 14:     // Cover
                    if (!iBool_AllowDeleteCover)
                        return;
                    break;
                case 15:     // Work
                    if (!iBool_AllowDeleteWorkDetail)
                        return;
                    break;
                case 16:     // Purpose
                    if (!iBool_AllowDeletePurpose)
                        return;
                    break;
                case 17:     // Fuel Cards
                    if (!iBool_AllowDeleteFuelCard)
                        return;
                    break;
                case 18:     // Speeding
                    if (!iBool_AllowDeleteSpeeding)
                        return;
                    break;
                
            }
            getCurrentGridControl( out strMessage1, out strMessage2);

            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Equipment records to delete.", "No Equipment Records To Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? strMessage1 : Convert.ToString(intRowHandles.Length) + strMessage2) +
            " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this record" : "these records") +
            " will no longer be available for selection and any related records will also be deleted!";
            if (XtraMessageBox.Show(strMessage, "Permanently Delete Record(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                string strRecordsToLoad = "";
                string strChildLinkedRecordsToLoad = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    DataRow dr = view.GetDataRow(intRowHandle);
                    switch (i_int_FocusedGrid)
                    {
                        case 0:     // Equipment
                        case 1:     // Vehicle
                        case 2:     // Plant 
                        //case 3:     // Gadget 
                        //case 4:     // Hardware 
                        case 5:     // Software 
                        case 6:     // Office
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_Core.sp_AS_11053_Fleet_ManagerRow)(dr)).EquipmentID).ToString() + ',';
                            break;
                        case 19:     // Road Tax
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11096_Road_Tax_ItemRow)(dr)).RoadTaxID).ToString() + ',';
                            break;
                        case 4:     // tracker
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11023_Tracker_ListRow)(dr)).TrackerInformationID).ToString() + ',';
                            break;
                        case 7:     // Billing
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11047_Equipment_Billing_ItemRow)(dr)).DepreciationID).ToString() + ',';
                            break;
                        case 8:     // Keeper
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dr)).KeeperAllocationID).ToString() + ',';
                            break;
                        case 9:     // Transaction
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11041_Transaction_ItemRow)(dr)).TransactionID).ToString() + ',';
                            break;
                        case 11:     // Depreciation
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11050_Depreciation_ItemRow)(dr)).DepreciationID).ToString() + ',';
                            break;
                        case 12:     // Incident
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)(dr)).IncidentID).ToString() + ',';
                            break;
                        case 13:     // ServiceInterval
                            //check if there is linked data 
                            DataRow[] drs = dataSet_AS_DataEntry.sp_AS_11054_Service_Data_Item.Select("ServiceDataID = " + (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceDataID).ToString());

                            if (drs.Length >= 2)
                            {
                                // delete service interval schedule
                                strChildLinkedRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceIntervalScheduleID).ToString() + ',';
                            }

                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11054_Service_Data_ItemRow)(dr)).ServiceDataID).ToString() + ',';
                            break;
                        case 14:     // Cover
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11075_Cover_ItemRow)(dr)).CoverID).ToString() + ',';
                            break;
                        case 15:     // WorkDetail
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11078_Work_Detail_ItemRow)(dr)).WorkDetailID).ToString() + ',';
                            break;
                        case 16:     // Purpose
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11084_Purpose_ItemRow)(dr)).EquipmentPurposeID).ToString() + ',';
                            break;
                        case 17:     // FuelCard
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_ItemRow)(dr)).FuelCardID).ToString() + ',';
                            break;
                        case 18:     // speeding
                            strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11105_Speeding_ItemRow)(dr)).SpeedingID).ToString() + ',';
                            break;
                        default:
                            strRecordsToLoad = "";
                            break;
                    }
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

               
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                switch (i_int_FocusedGrid)
                {
                    case 0:     // Equipment
                    case 1:     // Vehicle
                    case 2:     // Plant 
                    //case 3:     // Gadget 
                    // case 4:     // Hardware 
                    case 5:     // Software 
                    case 6:     // Office
                        sp_AS_11102_Rental_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 19:     // Road Tax
                        sp_AS_11096_Road_Tax_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 4:     // Tracker
                        sp_AS_11023_Tracker_ListTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 7:     // Billing
                        //sp_AS_11047_Equipment_Billing_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        break;
                    case 8:     // Keeper
                        sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        break;
                    case 9:     // Transaction
                        sp_AS_11041_Transaction_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        break;
                    case 11:     // Depreciation
                        sp_AS_11050_Depreciation_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        break;
                    case 12:     // Incident
                        sp_AS_11081_Incident_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        break;
                    case 13:     // Service Interval
                        if (strChildLinkedRecordsToLoad != "")
                    {
                        sp_AS_11054_Service_Data_ItemTableAdapter.Delete("del_child", strChildLinkedRecordsToLoad);
                       // UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    }
                    else if (strRecordsToLoad != "")
                    {
                        sp_AS_11054_Service_Data_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                    }
                    break;
                        break;
                    case 14:     // Cover
                        sp_AS_11075_Cover_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        break;
                    case 15:     // WorkDetail
                        sp_AS_11078_Work_Detail_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        break;
                    case 16:     // Purpose
                        sp_AS_11084_Purpose_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        break;
                    case 17:     // Fuel Card
                        sp_AS_11089_Fuel_Card_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        break;
                    case 18:     // Speeding
                        sp_AS_11105_Speeding_ItemTableAdapter.Delete("delete", strRecordsToLoad);
                        //UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                        break;
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                LoadLinkedRecords((((DataRowView)(spAS11102RentalItemBindingSource.CurrencyManager.Current)).Row).ItemArray[0].ToString());

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void getCurrentGridControl(out string strMessage1, out string strMessage2)
        {
            switch (i_int_FocusedGrid)
            {
                case 19://road tax
                    strMessage1 = "1 Road Tax record";
                    strMessage2 = " Road Tax records";
                 
                    break;
                case 4://Tracker
                    strMessage1 = "1 Verilocation Tracker record";
                    strMessage2 = " Verilocation Tracker records";
                   
                    break;
                case 7://billing 
                    strMessage1 = "1 Billing record";
                    strMessage2 = " Billing records";
                  
                    break;
                case 8://keeper
                    strMessage1 = "1 Keeper record";
                    strMessage2 = " Keeper records";
                   
                    break;
                case 9://transaction
                    strMessage1 = "1 Transaction record";
                    strMessage2 = " Transaction records";
                 
                    break;
                case 11://depreciation
                    strMessage1 = "1 Depreciation record";
                    strMessage2 = " Depreciation records";
                
                    break;
                case 12://Incident
                    strMessage1 = "1 Incident record";
                    strMessage2 = " Incident records";
                 
                    break;
                case 13://Service Interval

                    strMessage1 = "1 Service Interval record";
                    strMessage2 = " Service Interval records";
               
                    break;
                case 14://Cover
                    strMessage1 = "1 Cover record";
                    strMessage2 = " Cover records";
                    
                    break;
                case 15://WorkDetail

                    strMessage1 = "1 Work Order Detail record";
                    strMessage2 = " Work Order Detail records";
                   
                    break;
                case 16://Purpose

                    strMessage1 = "1 Purpose record";
                    strMessage2 = " Purpose records";
                 
                    break;
                case 17://Fuel Cards

                    strMessage1 = "1 Fuel Card record";
                    strMessage2 = " Fuel Card records";
                
                    break;
                case 18://Speeding

                    strMessage1 = "1 Speeding record";
                    strMessage2 = " Speeding records";
               
                    break;
                default:
                     strMessage1 = "1 record";
                    strMessage2 = " records";
                    break;
               
            }
        }


        private void commonView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //
            GridView view = (GridView)sender;
            customFilterDraw(view, e);
        }

        private void equipmentView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion

        #region CRUD Events

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void lueRentalStatusID_EditValueChanged(object sender, EventArgs e)
        {
            P11dManagerPromptRequired = false;

            if (bool_FormLoading)
                return;

            if (lueRentalStatusID.Text != "Returned")
                return;

            if (this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item == null)
                return;

            if (this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.Count == 0)
                return;

            DataRow[] ActiveKeepers = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.Select("AllocationStatus = 'Active'");

            if (ActiveKeepers.Length == 0)
                return;

            XtraMessageBox.Show("Rental status of Returned will end the current Active keeper allocation.",
                                "Rental Edit", MessageBoxButtons.OK, MessageBoxIcon.Information);
            P11dManagerPromptRequired = true;
        }

        private void View_Record()
        {
            switch (i_int_FocusedGrid)
            {
                
                case 19:     // Road Tax //
                    if (!iBool_AllowEditRoadTax)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", roadTaxGridControl, EquipmentType.None);
                    break;
                case 4:     // Tracker //
                    if (!iBool_AllowEditTracker)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", trackerGridControl, EquipmentType.None);
                    break;
                
                case 7:     // Billings //
                    if (!iBool_AllowEditBilling)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", billingGridControl, EquipmentType.None);
                    break;
                case 8:     // Keeper //
                    if (!iBool_AllowEditKeeper)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", keeperGridControl, EquipmentType.None);
                    break;
                case 9:     // Transaction //
                    if (!iBool_AllowEditTransactions)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", transactionGridControl, EquipmentType.None);
                    break;
                //case 10:     // Reminders //
                //    if (!iBool_AllowEditReminders)
                //        return;
                //    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", officeGridControl, EquipmentType.Office);
                //    break;
                case 11:     // Depreciation //
                    if (!iBool_AllowEditDepreciation)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", depreciationGridControl, EquipmentType.None);
                    break;
                case 12:     // Incident //
                    if (!iBool_AllowEditIncident)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", incidentGridControl, EquipmentType.None);
                    break;
                case 13:     // ServiceInterval //
                    if (!iBool_AllowEditServiceInterval)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", serviceIntervalGridControl, EquipmentType.None);
                    break;
                case 14:     // Cover //
                    if (!iBool_AllowEditCover)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", coverGridControl, EquipmentType.None);
                    break;
                case 15:     // Work //
                    if (!iBool_AllowEditWorkDetail)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", workDetailGridControl, EquipmentType.None);
                    break;
                case 16:     // Purpose //
                    if (!iBool_AllowEditPurpose)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", purposeGridControl, EquipmentType.None);
                    break;
                case 17:     // Fuel Cards //
                    if (!iBool_AllowEditFuelCard)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", fuelCardGridControl, EquipmentType.None);
                    break;
                case 18:     // speeding //
                    if (!iBool_AllowEditSpeeding)
                        return;
                    OpenEditForm(FormMode.view, "frm_AS_Rental_Edit", speedingGridControl, EquipmentType.None);
                    break;
                default:
                    break;
            }
        }

        #endregion


        #endregion

        

        #endregion

        

        

        
        #endregion


       
        #endregion

        #endregion

        

  









    }
}

﻿namespace WoodPlan5
{
    partial class frm_AS_Keeper_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Keeper_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11044KeeperAllocationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.lueKeeperTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLKeeperTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueKeeperID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11044KeepersItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueAllocationStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLKeeperAllocationStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deAllocationDate = new DevExpress.XtraEditors.DateEdit();
            this.deAllocationEndDate = new DevExpress.XtraEditors.DateEdit();
            this.memNotes = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForKeeperTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllocationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllocationStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKeeperID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllocationEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter();
            this.sp_AS_11000_PL_Keeper_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_TypeTableAdapter();
            this.sp_AS_11044_Keepers_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keepers_ItemTableAdapter();
            this.spAS11044KeeperAllocationListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11044_Keeper_Allocation_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ListTableAdapter();
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter();
            this.sp_AS_11166_P11D_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11166_P11D_ItemTableAdapter();
            this.sp_AS_11001_Equipment_ManagerTableAdapter = new WoodPlan5.DataSet_AS_CoreTableAdapters.sp_AS_11001_Equipment_ManagerTableAdapter();
            this.dataSet_AS_Core = new WoodPlan5.DataSet_AS_Core();
            this.sp_AS_11041_Transaction_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter();
            this.spAS11041TransactionItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_PL_Transaction_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Transaction_TypeTableAdapter();
            this.spAS11000PLTransactionTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11005_Vehicle_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.sp_AS_11102_Rental_ManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11102_Rental_ManagerTableAdapter = new WoodPlan5.DataSet_AS_CoreTableAdapters.sp_AS_11102_Rental_ManagerTableAdapter();
            this.tableAdapterManager1 = new WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager();
            this.sp_AS_11108_Rental_Details_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11108_Rental_Details_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11108_Rental_Details_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeepersItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAllocationStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperAllocationStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deAllocationDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deAllocationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deAllocationEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deAllocationEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLTransactionTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11102_Rental_ManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11108_Rental_Details_ItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(871, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 528);
            this.barDockControlBottom.Size = new System.Drawing.Size(871, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 502);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(871, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 502);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(871, 502);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(130, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(296, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11044KeeperAllocationItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(142, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(292, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.editFormDataNavigator.TextStringFormat = "Keeper Allocation {0} of {1}";
            this.editFormDataNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.editFormDataNavigator_ButtonClick);
            // 
            // spAS11044KeeperAllocationItemBindingSource
            // 
            this.spAS11044KeeperAllocationItemBindingSource.DataMember = "sp_AS_11044_Keeper_Allocation_Item";
            this.spAS11044KeeperAllocationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEquipmentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueKeeperTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueKeeperID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueAllocationStatusID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deAllocationDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deAllocationEndDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memNotes);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11044KeeperAllocationItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2458, 247, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(871, 502);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11044KeeperAllocationItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(125, 35);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(308, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling1);
            this.txtEquipmentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEquipmentReference.TabIndex = 124;
            // 
            // lueKeeperTypeID
            // 
            this.lueKeeperTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11044KeeperAllocationItemBindingSource, "KeeperTypeID", true));
            this.lueKeeperTypeID.Location = new System.Drawing.Point(125, 59);
            this.lueKeeperTypeID.MenuManager = this.barManager1;
            this.lueKeeperTypeID.Name = "lueKeeperTypeID";
            this.lueKeeperTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKeeperTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Keeper Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueKeeperTypeID.Properties.DataSource = this.spAS11000PLKeeperTypeBindingSource;
            this.lueKeeperTypeID.Properties.DisplayMember = "Value";
            this.lueKeeperTypeID.Properties.NullText = "";
            this.lueKeeperTypeID.Properties.NullValuePrompt = "-- Please Select Keeper Type --";
            this.lueKeeperTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueKeeperTypeID.Properties.ValueMember = "PickListID";
            this.lueKeeperTypeID.Size = new System.Drawing.Size(308, 20);
            this.lueKeeperTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueKeeperTypeID.TabIndex = 125;
            this.lueKeeperTypeID.Tag = "Keeper Type";
            this.lueKeeperTypeID.EditValueChanged += new System.EventHandler(this.lueKeeperTypeID_EditValueChanged);
            this.lueKeeperTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLKeeperTypeBindingSource
            // 
            this.spAS11000PLKeeperTypeBindingSource.DataMember = "sp_AS_11000_PL_Keeper_Type";
            this.spAS11000PLKeeperTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueKeeperID
            // 
            this.lueKeeperID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11044KeeperAllocationItemBindingSource, "KeeperID", true));
            this.lueKeeperID.Location = new System.Drawing.Point(550, 59);
            this.lueKeeperID.MenuManager = this.barManager1;
            this.lueKeeperID.Name = "lueKeeperID";
            this.lueKeeperID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKeeperID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FullName", "Full Name", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueKeeperID.Properties.DataSource = this.spAS11044KeepersItemBindingSource;
            this.lueKeeperID.Properties.DisplayMember = "FullName";
            this.lueKeeperID.Properties.NullText = "";
            this.lueKeeperID.Properties.NullValuePrompt = "-- Please Select Keeper --";
            this.lueKeeperID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueKeeperID.Properties.PopupWidth = 150;
            this.lueKeeperID.Properties.ValueMember = "EmployeeNumber";
            this.lueKeeperID.Size = new System.Drawing.Size(309, 20);
            this.lueKeeperID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueKeeperID.TabIndex = 126;
            this.lueKeeperID.Tag = "Keeper";
            this.lueKeeperID.EditValueChanged += new System.EventHandler(this.lueKeeperID_EditValueChanged);
            this.lueKeeperID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11044KeepersItemBindingSource
            // 
            this.spAS11044KeepersItemBindingSource.DataMember = "sp_AS_11044_Keepers_Item";
            this.spAS11044KeepersItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueAllocationStatusID
            // 
            this.lueAllocationStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11044KeeperAllocationItemBindingSource, "AllocationStatusID", true));
            this.lueAllocationStatusID.Location = new System.Drawing.Point(550, 35);
            this.lueAllocationStatusID.MenuManager = this.barManager1;
            this.lueAllocationStatusID.Name = "lueAllocationStatusID";
            this.lueAllocationStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAllocationStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Allocation Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAllocationStatusID.Properties.DataSource = this.spAS11000PLKeeperAllocationStatusBindingSource;
            this.lueAllocationStatusID.Properties.DisplayMember = "Value";
            this.lueAllocationStatusID.Properties.NullText = "";
            this.lueAllocationStatusID.Properties.NullValuePrompt = "-- Please Select Status --";
            this.lueAllocationStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueAllocationStatusID.Properties.ValueMember = "PickListID";
            this.lueAllocationStatusID.Size = new System.Drawing.Size(309, 20);
            this.lueAllocationStatusID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueAllocationStatusID.TabIndex = 127;
            this.lueAllocationStatusID.Tag = "Allocation Status";
            this.lueAllocationStatusID.EditValueChanged += new System.EventHandler(this.lueAllocationStatusID_EditValueChanged);
            this.lueAllocationStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLKeeperAllocationStatusBindingSource
            // 
            this.spAS11000PLKeeperAllocationStatusBindingSource.DataMember = "sp_AS_11000_PL_Keeper_Allocation_Status";
            this.spAS11000PLKeeperAllocationStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deAllocationDate
            // 
            this.deAllocationDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11044KeeperAllocationItemBindingSource, "AllocationDate", true));
            this.deAllocationDate.EditValue = null;
            this.deAllocationDate.Location = new System.Drawing.Point(125, 83);
            this.deAllocationDate.MenuManager = this.barManager1;
            this.deAllocationDate.Name = "deAllocationDate";
            this.deAllocationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deAllocationDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deAllocationDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deAllocationDate.Properties.NullValuePrompt = "-- Please Enter Allocation Date --";
            this.deAllocationDate.Properties.NullValuePromptShowForEmptyValue = true;
            this.deAllocationDate.Size = new System.Drawing.Size(308, 20);
            this.deAllocationDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deAllocationDate.TabIndex = 128;
            this.deAllocationDate.Tag = "Allocation Date";
            this.deAllocationDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // deAllocationEndDate
            // 
            this.deAllocationEndDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11044KeeperAllocationItemBindingSource, "AllocationEndDate", true));
            this.deAllocationEndDate.EditValue = null;
            this.deAllocationEndDate.Location = new System.Drawing.Point(550, 83);
            this.deAllocationEndDate.MenuManager = this.barManager1;
            this.deAllocationEndDate.Name = "deAllocationEndDate";
            this.deAllocationEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deAllocationEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deAllocationEndDate.Properties.NullDate = new System.DateTime(3000, 1, 1, 0, 0, 0, 0);
            this.deAllocationEndDate.Size = new System.Drawing.Size(309, 20);
            this.deAllocationEndDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deAllocationEndDate.TabIndex = 129;
            this.deAllocationEndDate.Tag = "Allocation End Date";
            this.deAllocationEndDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // memNotes
            // 
            this.memNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11044KeeperAllocationItemBindingSource, "Notes", true));
            this.memNotes.Location = new System.Drawing.Point(24, 143);
            this.memNotes.MenuManager = this.barManager1;
            this.memNotes.Name = "memNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memNotes, true);
            this.memNotes.Size = new System.Drawing.Size(823, 335);
            this.scSpellChecker.SetSpellCheckerOptions(this.memNotes, optionsSpelling2);
            this.memNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.memNotes.TabIndex = 130;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForKeeperTypeID,
            this.ItemForAllocationDate,
            this.ItemForEquipmentReference,
            this.ItemForAllocationStatusID,
            this.ItemForKeeperID,
            this.ItemForAllocationEndDate,
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(851, 459);
            // 
            // ItemForKeeperTypeID
            // 
            this.ItemForKeeperTypeID.Control = this.lueKeeperTypeID;
            this.ItemForKeeperTypeID.CustomizationFormText = "Keeper Type :";
            this.ItemForKeeperTypeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForKeeperTypeID.Name = "ItemForKeeperTypeID";
            this.ItemForKeeperTypeID.Size = new System.Drawing.Size(425, 24);
            this.ItemForKeeperTypeID.Text = "Keeper Type :";
            this.ItemForKeeperTypeID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForAllocationDate
            // 
            this.ItemForAllocationDate.Control = this.deAllocationDate;
            this.ItemForAllocationDate.CustomizationFormText = "Allocation Date :";
            this.ItemForAllocationDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForAllocationDate.Name = "ItemForAllocationDate";
            this.ItemForAllocationDate.Size = new System.Drawing.Size(425, 24);
            this.ItemForAllocationDate.Text = "Allocation Date :";
            this.ItemForAllocationDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.txtEquipmentReference;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference :";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(425, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference :";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForAllocationStatusID
            // 
            this.ItemForAllocationStatusID.Control = this.lueAllocationStatusID;
            this.ItemForAllocationStatusID.CustomizationFormText = "Allocation Status :";
            this.ItemForAllocationStatusID.Location = new System.Drawing.Point(425, 0);
            this.ItemForAllocationStatusID.Name = "ItemForAllocationStatusID";
            this.ItemForAllocationStatusID.Size = new System.Drawing.Size(426, 24);
            this.ItemForAllocationStatusID.Text = "Allocation Status :";
            this.ItemForAllocationStatusID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForKeeperID
            // 
            this.ItemForKeeperID.Control = this.lueKeeperID;
            this.ItemForKeeperID.CustomizationFormText = "Keeper :";
            this.ItemForKeeperID.Location = new System.Drawing.Point(425, 24);
            this.ItemForKeeperID.Name = "ItemForKeeperID";
            this.ItemForKeeperID.Size = new System.Drawing.Size(426, 24);
            this.ItemForKeeperID.Text = "Keeper :";
            this.ItemForKeeperID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForAllocationEndDate
            // 
            this.ItemForAllocationEndDate.Control = this.deAllocationEndDate;
            this.ItemForAllocationEndDate.CustomizationFormText = "Allocation End Date :";
            this.ItemForAllocationEndDate.Location = new System.Drawing.Point(425, 48);
            this.ItemForAllocationEndDate.Name = "ItemForAllocationEndDate";
            this.ItemForAllocationEndDate.Size = new System.Drawing.Size(426, 24);
            this.ItemForAllocationEndDate.Text = "Allocation End Date :";
            this.ItemForAllocationEndDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 72);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(851, 387);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup2.CaptionImage")));
            this.layoutControlGroup2.CustomizationFormText = "Notes";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(827, 339);
            this.layoutControlGroup2.Text = "Notes";
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.memNotes;
            this.ItemForNotes.CustomizationFormText = "Notes";
            this.ItemForNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(827, 339);
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNotes.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(130, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(426, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(425, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Keeper_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Keeper_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keepers_ItemTableAdapter
            // 
            this.sp_AS_11044_Keepers_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11044KeeperAllocationListBindingSource
            // 
            this.spAS11044KeeperAllocationListBindingSource.DataMember = "sp_AS_11044_Keeper_Allocation_List";
            this.spAS11044KeeperAllocationListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11044_Keeper_Allocation_ListTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keeper_Allocation_ItemTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11166_P11D_ItemTableAdapter
            // 
            this.sp_AS_11166_P11D_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11001_Equipment_ManagerTableAdapter
            // 
            this.sp_AS_11001_Equipment_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AS_Core
            // 
            this.dataSet_AS_Core.DataSetName = "DataSet_AS_Core";
            this.dataSet_AS_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_AS_11041_Transaction_ItemTableAdapter
            // 
            this.sp_AS_11041_Transaction_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11041TransactionItemBindingSource
            // 
            this.spAS11041TransactionItemBindingSource.DataMember = "sp_AS_11041_Transaction_Item";
            this.spAS11041TransactionItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_PL_Transaction_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Transaction_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11000PLTransactionTypeBindingSource
            // 
            this.spAS11000PLTransactionTypeBindingSource.DataMember = "sp_AS_11000_PL_Transaction_Type";
            this.spAS11000PLTransactionTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11005_Vehicle_ItemTableAdapter
            // 
            this.sp_AS_11005_Vehicle_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Status Bar";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Status bar";
            // 
            // sp_AS_11102_Rental_ManagerBindingSource
            // 
            this.sp_AS_11102_Rental_ManagerBindingSource.DataMember = "sp_AS_11102_Rental_Manager";
            this.sp_AS_11102_Rental_ManagerBindingSource.DataSource = this.dataSet_AS_Core;
            // 
            // sp_AS_11102_Rental_ManagerTableAdapter
            // 
            this.sp_AS_11102_Rental_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.Connection = null;
            this.tableAdapterManager1.UpdateOrder = WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_AS_11108_Rental_Details_ItemBindingSource
            // 
            this.sp_AS_11108_Rental_Details_ItemBindingSource.DataMember = "sp_AS_11108_Rental_Details_Item";
            this.sp_AS_11108_Rental_Details_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11108_Rental_Details_ItemTableAdapter
            // 
            this.sp_AS_11108_Rental_Details_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Keeper_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(871, 558);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Keeper_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Keeper Allocations";
            this.Activated += new System.EventHandler(this.frm_AS_Keeper_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Keeper_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Keeper_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeepersItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAllocationStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperAllocationStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deAllocationDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deAllocationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deAllocationEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deAllocationEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLTransactionTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11102_Rental_ManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11108_Rental_Details_ItemBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private DevExpress.XtraEditors.LookUpEdit lueKeeperTypeID;
        private DevExpress.XtraEditors.LookUpEdit lueKeeperID;
        private DevExpress.XtraEditors.LookUpEdit lueAllocationStatusID;
        private DevExpress.XtraEditors.DateEdit deAllocationDate;
        private DevExpress.XtraEditors.DateEdit deAllocationEndDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKeeperTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKeeperID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllocationStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllocationDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllocationEndDate;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource spAS11000PLKeeperAllocationStatusBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLKeeperTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_TypeTableAdapter sp_AS_11000_PL_Keeper_TypeTableAdapter;
        private System.Windows.Forms.BindingSource spAS11044KeepersItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keepers_ItemTableAdapter sp_AS_11044_Keepers_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11044KeeperAllocationListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ListTableAdapter sp_AS_11044_Keeper_Allocation_ListTableAdapter;
        private System.Windows.Forms.BindingSource spAS11044KeeperAllocationItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter sp_AS_11044_Keeper_Allocation_ItemTableAdapter;
        private DevExpress.XtraEditors.MemoEdit memNotes;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11166_P11D_ItemTableAdapter sp_AS_11166_P11D_ItemTableAdapter;
        private DataSet_AS_CoreTableAdapters.sp_AS_11001_Equipment_ManagerTableAdapter sp_AS_11001_Equipment_ManagerTableAdapter;
        private DataSet_AS_Core dataSet_AS_Core;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter sp_AS_11041_Transaction_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11041TransactionItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Transaction_TypeTableAdapter sp_AS_11000_PL_Transaction_TypeTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLTransactionTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter sp_AS_11005_Vehicle_ItemTableAdapter;
        private DevExpress.XtraBars.Bar bar2;
        private System.Windows.Forms.BindingSource sp_AS_11102_Rental_ManagerBindingSource;
        private DataSet_AS_CoreTableAdapters.sp_AS_11102_Rental_ManagerTableAdapter sp_AS_11102_Rental_ManagerTableAdapter;
        private DataSet_AS_CoreTableAdapters.TableAdapterManager tableAdapterManager1;
        private System.Windows.Forms.BindingSource sp_AS_11108_Rental_Details_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11108_Rental_Details_ItemTableAdapter sp_AS_11108_Rental_Details_ItemTableAdapter;


    }
}

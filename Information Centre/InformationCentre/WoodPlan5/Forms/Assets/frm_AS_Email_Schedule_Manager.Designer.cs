﻿namespace WoodPlan5
{
    partial class frm_AS_Email_Schedule_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Email_Schedule_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.emailScheduleGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11157EmailScheduleItemBindingSource = new System.Windows.Forms.BindingSource();
            this.emailScheduleGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmailScheduleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.deEmailDateFormatted = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colTaskType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaskDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaskStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaskStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsCurrentTask = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceSetAsDefault = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp_AS_11157_Email_Schedule_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11157_Email_Schedule_ItemTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.bbiExportOldExcel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExcelExport = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailScheduleGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11157EmailScheduleItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailScheduleGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEmailDateFormatted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEmailDateFormatted.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSetAsDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.Manager = null;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1372, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 476);
            this.barDockControlBottom.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 450);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1372, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 450);
            // 
            // bbiSave
            // 
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.LargeGlyph")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.Manager = null;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiCancel,
            this.bbiRefresh,
            this.barSubItem1,
            this.bbiExportOldExcel,
            this.bbiExcelExport});
            this.barManager1.MaxItemId = 35;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("save_16x16.png", "images/save/save_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/save/save_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "save_16x16.png");
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1675, 521, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1372, 450);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1372, 450);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // emailScheduleGridControl
            // 
            this.emailScheduleGridControl.DataSource = this.spAS11157EmailScheduleItemBindingSource;
            this.emailScheduleGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.Append.ImageIndex = 0;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.Edit.ImageIndex = 1;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.EndEdit.ImageIndex = 4;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.Remove.ImageIndex = 2;
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "";
            this.emailScheduleGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.emailScheduleGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, false, "", "delete")});
            this.emailScheduleGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.emailScheduleGridControl_EmbeddedNavigator_ButtonClick);
            this.emailScheduleGridControl.Location = new System.Drawing.Point(0, 26);
            this.emailScheduleGridControl.MainView = this.emailScheduleGridView;
            this.emailScheduleGridControl.Name = "emailScheduleGridControl";
            this.emailScheduleGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceSetAsDefault,
            this.deEmailDateFormatted});
            this.emailScheduleGridControl.Size = new System.Drawing.Size(1372, 450);
            this.emailScheduleGridControl.TabIndex = 5;
            this.emailScheduleGridControl.Tag = "EmailScheduledTask";
            this.emailScheduleGridControl.UseEmbeddedNavigator = true;
            this.emailScheduleGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.emailScheduleGridView});
            // 
            // spAS11157EmailScheduleItemBindingSource
            // 
            this.spAS11157EmailScheduleItemBindingSource.DataMember = "sp_AS_11157_Email_Schedule_Item";
            this.spAS11157EmailScheduleItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // emailScheduleGridView
            // 
            this.emailScheduleGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmailScheduleID,
            this.colEmailDate,
            this.colTaskType,
            this.colTaskDescription,
            this.colTaskStatusID,
            this.colTaskStatus,
            this.colIsCurrentTask,
            this.colMode,
            this.colRecordID});
            this.emailScheduleGridView.GridControl = this.emailScheduleGridControl;
            this.emailScheduleGridView.Name = "emailScheduleGridView";
            this.emailScheduleGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.emailScheduleGridView.OptionsFind.AlwaysVisible = true;
            this.emailScheduleGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.emailScheduleGridView.OptionsLayout.StoreAppearance = true;
            this.emailScheduleGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.emailScheduleGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.emailScheduleGridView.OptionsView.ColumnAutoWidth = false;
            this.emailScheduleGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.emailScheduleGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.emailScheduleGridView_PopupMenuShowing);
            this.emailScheduleGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.emailScheduleGridView_SelectionChanged);
            this.emailScheduleGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.emailScheduleGridView_CustomDrawEmptyForeground);
            this.emailScheduleGridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.emailScheduleGridView_ShowingEditor);
            this.emailScheduleGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.emailScheduleGridView_FocusedRowChanged);
            this.emailScheduleGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.emailScheduleGridView_CustomFilterDialog);
            this.emailScheduleGridView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.emailScheduleGridView_MouseDown);
            this.emailScheduleGridView.DoubleClick += new System.EventHandler(this.emailScheduleGridView_DoubleClick);
            // 
            // colEmailScheduleID
            // 
            this.colEmailScheduleID.FieldName = "EmailScheduleID";
            this.colEmailScheduleID.Name = "colEmailScheduleID";
            this.colEmailScheduleID.OptionsColumn.AllowEdit = false;
            this.colEmailScheduleID.OptionsColumn.AllowFocus = false;
            this.colEmailScheduleID.OptionsColumn.ReadOnly = true;
            this.colEmailScheduleID.OptionsColumn.ShowInCustomizationForm = false;
            this.colEmailScheduleID.OptionsColumn.ShowInExpressionEditor = false;
            this.colEmailScheduleID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmailScheduleID.Width = 105;
            // 
            // colEmailDate
            // 
            this.colEmailDate.ColumnEdit = this.deEmailDateFormatted;
            this.colEmailDate.FieldName = "EmailDate";
            this.colEmailDate.Name = "colEmailDate";
            this.colEmailDate.OptionsColumn.AllowEdit = false;
            this.colEmailDate.OptionsColumn.AllowFocus = false;
            this.colEmailDate.OptionsColumn.ReadOnly = true;
            this.colEmailDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmailDate.Visible = true;
            this.colEmailDate.VisibleIndex = 0;
            this.colEmailDate.Width = 180;
            // 
            // deEmailDateFormatted
            // 
            this.deEmailDateFormatted.AutoHeight = false;
            this.deEmailDateFormatted.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEmailDateFormatted.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEmailDateFormatted.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.deEmailDateFormatted.Mask.EditMask = "dd-MMM-yyyy";
            this.deEmailDateFormatted.Mask.UseMaskAsDisplayFormat = true;
            this.deEmailDateFormatted.Name = "deEmailDateFormatted";
            // 
            // colTaskType
            // 
            this.colTaskType.FieldName = "TaskType";
            this.colTaskType.Name = "colTaskType";
            this.colTaskType.OptionsColumn.AllowEdit = false;
            this.colTaskType.OptionsColumn.AllowFocus = false;
            this.colTaskType.OptionsColumn.ReadOnly = true;
            this.colTaskType.OptionsColumn.ShowInCustomizationForm = false;
            this.colTaskType.OptionsColumn.ShowInExpressionEditor = false;
            this.colTaskType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colTaskDescription
            // 
            this.colTaskDescription.FieldName = "TaskDescription";
            this.colTaskDescription.Name = "colTaskDescription";
            this.colTaskDescription.OptionsColumn.AllowEdit = false;
            this.colTaskDescription.OptionsColumn.AllowFocus = false;
            this.colTaskDescription.OptionsColumn.ReadOnly = true;
            this.colTaskDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaskDescription.Visible = true;
            this.colTaskDescription.VisibleIndex = 1;
            this.colTaskDescription.Width = 306;
            // 
            // colTaskStatusID
            // 
            this.colTaskStatusID.FieldName = "TaskStatusID";
            this.colTaskStatusID.Name = "colTaskStatusID";
            this.colTaskStatusID.OptionsColumn.AllowEdit = false;
            this.colTaskStatusID.OptionsColumn.AllowFocus = false;
            this.colTaskStatusID.OptionsColumn.ReadOnly = true;
            this.colTaskStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTaskStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTaskStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaskStatusID.Width = 91;
            // 
            // colTaskStatus
            // 
            this.colTaskStatus.FieldName = "TaskStatus";
            this.colTaskStatus.Name = "colTaskStatus";
            this.colTaskStatus.OptionsColumn.AllowEdit = false;
            this.colTaskStatus.OptionsColumn.AllowFocus = false;
            this.colTaskStatus.OptionsColumn.ReadOnly = true;
            this.colTaskStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaskStatus.Visible = true;
            this.colTaskStatus.VisibleIndex = 2;
            this.colTaskStatus.Width = 220;
            // 
            // colIsCurrentTask
            // 
            this.colIsCurrentTask.FieldName = "IsCurrentTask";
            this.colIsCurrentTask.Name = "colIsCurrentTask";
            this.colIsCurrentTask.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIsCurrentTask.Visible = true;
            this.colIsCurrentTask.VisibleIndex = 3;
            this.colIsCurrentTask.Width = 245;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ceSetAsDefault
            // 
            this.ceSetAsDefault.AutoHeight = false;
            this.ceSetAsDefault.Caption = "Set As Default";
            this.ceSetAsDefault.Name = "ceSetAsDefault";
            this.ceSetAsDefault.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceIsCurrentTask_EditValueChanging);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // bbiCancel
            // 
            this.bbiCancel.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiCancel.Caption = "Cancel and Close";
            this.bbiCancel.DropDownEnabled = false;
            this.bbiCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCancel.Glyph")));
            this.bbiCancel.Id = 30;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11157_Email_Schedule_ItemTableAdapter
            // 
            this.sp_AS_11157_Email_Schedule_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 2";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1)});
            this.bar2.Text = "Custom 2";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh Tasks";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 31;
            this.bbiRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.LargeGlyph")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Export";
            this.barSubItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem1.Glyph")));
            this.barSubItem1.Id = 32;
            this.barSubItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barSubItem1.LargeGlyph")));
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportOldExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExcelExport)});
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiExportOldExcel
            // 
            this.bbiExportOldExcel.Caption = "Export To Excel 2003";
            this.bbiExportOldExcel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExportOldExcel.Glyph")));
            this.bbiExportOldExcel.Id = 33;
            this.bbiExportOldExcel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiExportOldExcel.LargeGlyph")));
            this.bbiExportOldExcel.Name = "bbiExportOldExcel";
            this.bbiExportOldExcel.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiExportOldExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportOldExcel_ItemClick);
            // 
            // bbiExcelExport
            // 
            this.bbiExcelExport.Caption = "Export To Excel";
            this.bbiExcelExport.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExcelExport.Glyph")));
            this.bbiExcelExport.Id = 34;
            this.bbiExcelExport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiExcelExport.LargeGlyph")));
            this.bbiExcelExport.Name = "bbiExcelExport";
            this.bbiExcelExport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiExcelExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExcelExport_ItemClick);
            // 
            // frm_AS_Email_Schedule_Manager
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1372, 476);
            this.Controls.Add(this.emailScheduleGridControl);
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Email_Schedule_Manager";
            this.Text = "Email Schedule Manager";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.frm_AS_Email_Schedule_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Email_Schedule_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            this.Controls.SetChildIndex(this.emailScheduleGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailScheduleGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11157EmailScheduleItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailScheduleGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEmailDateFormatted.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEmailDateFormatted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSetAsDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl emailScheduleGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView emailScheduleGridView;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceSetAsDefault;
        private System.Windows.Forms.BindingSource spAS11157EmailScheduleItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailScheduleID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTaskType;
        private DevExpress.XtraGrid.Columns.GridColumn colTaskDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTaskStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colTaskStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colIsCurrentTask;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11157_Email_Schedule_ItemTableAdapter sp_AS_11157_Email_Schedule_ItemTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit deEmailDateFormatted;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem bbiExportOldExcel;
        private DevExpress.XtraBars.BarButtonItem bbiExcelExport;
    }
}

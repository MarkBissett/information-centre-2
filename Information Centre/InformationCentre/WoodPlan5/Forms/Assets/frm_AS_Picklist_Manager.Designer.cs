﻿namespace WoodPlan5
{
    partial class frm_AS_Picklist_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Picklist_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.managerGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11132PicklistHeadersBindingSource = new System.Windows.Forms.BindingSource();
            this.managerGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChildEditScreen = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProgramPartID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NotesMemoExEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.webRepositoryItemHyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.lueSupplierType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp_AS_11132_Picklist_HeadersTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11132_Picklist_HeadersTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11132PicklistHeadersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoExEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.webRepositoryItemHyperLinkEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.Manager = null;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 748);
            this.barDockControlBottom.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 748);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1372, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 748);
            // 
            // bbiSave
            // 
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.LargeGlyph")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.Manager = null;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiCancel});
            this.barManager1.MaxItemId = 31;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("save_16x16.png", "images/save/save_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/save/save_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "save_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16.png");
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1675, 521, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1372, 748);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1372, 748);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // managerGridControl
            // 
            this.managerGridControl.DataSource = this.spAS11132PicklistHeadersBindingSource;
            this.managerGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.managerGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.Append.ImageIndex = 0;
            this.managerGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.Edit.ImageIndex = 1;
            this.managerGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.EndEdit.ImageIndex = 4;
            this.managerGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.managerGridControl.EmbeddedNavigator.Buttons.Remove.ImageIndex = 2;
            this.managerGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "";
            this.managerGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.managerGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, false, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, false, "", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "", "refresh")});
            this.managerGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.managerGridControl_EmbeddedNavigator_ButtonClick);
            this.managerGridControl.Location = new System.Drawing.Point(0, 0);
            this.managerGridControl.MainView = this.managerGridView;
            this.managerGridControl.Name = "managerGridControl";
            this.managerGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.NotesMemoExEdit,
            this.webRepositoryItemHyperLinkEdit,
            this.lueSupplierType,
            this.TextEdit});
            this.managerGridControl.Size = new System.Drawing.Size(1372, 748);
            this.managerGridControl.TabIndex = 5;
            this.managerGridControl.UseEmbeddedNavigator = true;
            this.managerGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.managerGridView});
            // 
            // spAS11132PicklistHeadersBindingSource
            // 
            this.spAS11132PicklistHeadersBindingSource.DataMember = "sp_AS_11132_Picklist_Headers";
            this.spAS11132PicklistHeadersBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // managerGridView
            // 
            this.managerGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHeaderID,
            this.colHeaderDescription,
            this.colChildEditScreen,
            this.colProgramPartID,
            this.colLinkedRecordCount});
            this.managerGridView.GridControl = this.managerGridControl;
            this.managerGridView.Name = "managerGridView";
            this.managerGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.managerGridView.OptionsFind.AlwaysVisible = true;
            this.managerGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.managerGridView.OptionsLayout.StoreAppearance = true;
            this.managerGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.managerGridView.OptionsSelection.MultiSelect = true;
            this.managerGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.managerGridView.OptionsView.ColumnAutoWidth = false;
            this.managerGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.managerGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.managerGridView_PopupMenuShowing);
            this.managerGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.managerGridView_SelectionChanged);
            this.managerGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.managerGridView_CustomDrawEmptyForeground);
            this.managerGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.managerGridView_CustomFilterDialog);
            this.managerGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.managerGridView_MouseUp);
            this.managerGridView.DoubleClick += new System.EventHandler(this.managerGridView_DoubleClick);
            // 
            // colHeaderID
            // 
            this.colHeaderID.FieldName = "HeaderID";
            this.colHeaderID.Name = "colHeaderID";
            this.colHeaderID.OptionsColumn.AllowEdit = false;
            this.colHeaderID.OptionsColumn.AllowFocus = false;
            this.colHeaderID.OptionsColumn.ReadOnly = true;
            this.colHeaderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeaderID.Width = 184;
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeaderDescription.Visible = true;
            this.colHeaderDescription.VisibleIndex = 0;
            this.colHeaderDescription.Width = 201;
            // 
            // colChildEditScreen
            // 
            this.colChildEditScreen.FieldName = "ChildEditScreen";
            this.colChildEditScreen.Name = "colChildEditScreen";
            this.colChildEditScreen.OptionsColumn.AllowEdit = false;
            this.colChildEditScreen.OptionsColumn.AllowFocus = false;
            this.colChildEditScreen.OptionsColumn.ReadOnly = true;
            this.colChildEditScreen.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colChildEditScreen.Visible = true;
            this.colChildEditScreen.VisibleIndex = 1;
            this.colChildEditScreen.Width = 176;
            // 
            // colProgramPartID
            // 
            this.colProgramPartID.FieldName = "ProgramPartID";
            this.colProgramPartID.Name = "colProgramPartID";
            this.colProgramPartID.OptionsColumn.AllowEdit = false;
            this.colProgramPartID.OptionsColumn.AllowFocus = false;
            this.colProgramPartID.OptionsColumn.ReadOnly = true;
            this.colProgramPartID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProgramPartID.Visible = true;
            this.colProgramPartID.VisibleIndex = 2;
            this.colProgramPartID.Width = 198;
            // 
            // colLinkedRecordCount
            // 
            this.colLinkedRecordCount.FieldName = "LinkedRecordCount";
            this.colLinkedRecordCount.Name = "colLinkedRecordCount";
            this.colLinkedRecordCount.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordCount.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordCount.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedRecordCount.Visible = true;
            this.colLinkedRecordCount.VisibleIndex = 3;
            this.colLinkedRecordCount.Width = 246;
            // 
            // NotesMemoExEdit
            // 
            this.NotesMemoExEdit.AutoHeight = false;
            this.NotesMemoExEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NotesMemoExEdit.Name = "NotesMemoExEdit";
            this.NotesMemoExEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.NotesMemoExEdit.ShowIcon = false;
            // 
            // webRepositoryItemHyperLinkEdit
            // 
            this.webRepositoryItemHyperLinkEdit.AutoHeight = false;
            this.webRepositoryItemHyperLinkEdit.Name = "webRepositoryItemHyperLinkEdit";
            this.webRepositoryItemHyperLinkEdit.SingleClick = true;
            // 
            // lueSupplierType
            // 
            this.lueSupplierType.AutoHeight = false;
            this.lueSupplierType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSupplierType.DisplayMember = "Value";
            this.lueSupplierType.Name = "lueSupplierType";
            this.lueSupplierType.ValueMember = "Pick_List_ID";
            // 
            // TextEdit
            // 
            this.TextEdit.AutoHeight = false;
            this.TextEdit.Name = "TextEdit";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // bbiCancel
            // 
            this.bbiCancel.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiCancel.Caption = "Cancel and Close";
            this.bbiCancel.DropDownEnabled = false;
            this.bbiCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCancel.Glyph")));
            this.bbiCancel.Id = 30;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11132_Picklist_HeadersTableAdapter
            // 
            this.sp_AS_11132_Picklist_HeadersTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Picklist_Manager
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1372, 748);
            this.Controls.Add(this.managerGridControl);
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Picklist_Manager";
            this.Text = "Pick List Manager";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.frm_AS_Picklist_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Picklist_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            this.Controls.SetChildIndex(this.managerGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11132PicklistHeadersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoExEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.webRepositoryItemHyperLinkEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl managerGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView managerGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit NotesMemoExEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit webRepositoryItemHyperLinkEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueSupplierType;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit TextEdit;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource spAS11132PicklistHeadersBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colChildEditScreen;
        private DevExpress.XtraGrid.Columns.GridColumn colProgramPartID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordCount;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11132_Picklist_HeadersTableAdapter sp_AS_11132_Picklist_HeadersTableAdapter;
    }
}

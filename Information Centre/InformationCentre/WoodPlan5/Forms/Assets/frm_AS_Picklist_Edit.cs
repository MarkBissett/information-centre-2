using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_AS_Picklist_Edit : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strCaller = "";
        public int intPicklistHeaderID = 0;
        public string strPickListHeaderName = "";
        public int intRecordCount = 0;
        public bool iBool_AllowDelete = false;
        public bool iBool_AllowAdd = false;
        public bool iBool_AllowEdit = false;
        #endregion

        public frm_AS_Picklist_Edit()
        {
            InitializeComponent();
        }

        private void frm_AS_Picklist_Edit_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 111001;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp_AS_11133_Picklist_ItemsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11133_Picklist_ItemsTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11133_Picklist_Items, intPicklistHeaderID);

            this.Text += " - " + strPickListHeaderName;
            
            //Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            ConfigureFormAccordingToMode();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //             
        }

     

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
        }

        private void frm_AS_Picklist_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private Boolean SetChangesPendingLabel()
        {

            //GridView view = (GridView)gridControl1.MainView;
            //view.CloseEditor();
            //view.UpdateCurrentRow();
            this.spAS11133PicklistItemsBindingSource.EndEdit();
            DataSet dsChanges = this.dataSet_AS_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = true;
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            bbiDelete.Enabled = false;
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            
            //alItems.Add("iSave");
            //bbiSave.Enabled = true;
            if (SetChangesPendingLabel()) alItems.Add("iSave");
       
            if (!iBool_AllowEdit)
            {
                gridControl1.Enabled = false;
                return;
            }
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (iBool_AllowDelete && intRowHandles.Length >= 1)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;

            }
            if (iBool_AllowAdd)
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
            }
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            if (iBool_AllowAdd)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

        }

        public override void PostLoadView(object objParameter)
        {
            this.spAS11133PicklistItemsBindingSource.EndEdit();
            DataSet dsChanges = this.dataSet_AS_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
            }
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (intPicklistHeaderID == 135 || intPicklistHeaderID == 136)  // 135 = Heights, 138 = DBHs //
            {
                view.Columns["BandStart"].Visible = true;
                view.Columns["BandEnd"].Visible = true;
                bar2.Visible = true;
            }
            else if (intPicklistHeaderID == 174 || intPicklistHeaderID == 182)  // 174 = Tender Sector, 182 = Depth Of Customer Relationship //
            {
                view.Columns["decRiskFactor"].Caption = "Formula Value";
                view.Columns["decRiskFactor"].CustomizationCaption = "Formula Value";
                view.Columns["decRiskFactor"].ColumnEdit = repositoryItemTextEdit1;
                view.Columns["BandStart"].Visible = false;
                view.Columns["BandEnd"].Visible = false;
                bar2.Visible = false;
            }
            else if (intPicklistHeaderID == 190 || intPicklistHeaderID == 191)  // 190 = Work Permit Category,191 = Work Permit Status //
            {
                view.Columns["decRiskFactor"].Caption = "Ignore";
                view.Columns["decRiskFactor"].CustomizationCaption = "Ignore";
                view.Columns["decRiskFactor"].ColumnEdit = repositoryItemCheckEdit1;
                view.Columns["BandStart"].Visible = false;
                view.Columns["BandEnd"].Visible = false;
                bar2.Visible = false;
            }
            else  // Everything else //
            {
                view.Columns["decRiskFactor"].Caption = "Risk Factor";
                view.Columns["decRiskFactor"].CustomizationCaption = "Risk Factor";
                view.Columns["decRiskFactor"].ColumnEdit = repositoryItemTextEdit1;
                view.Columns["BandStart"].Visible = false;
                view.Columns["BandEnd"].Visible = false;
                bar2.Visible = false;
            }

        }



       #region GridControl 1

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView View = sender as GridView;
            string cellValue = View.GetRowCellValue(View.FocusedRowHandle, colstrCode).ToString();
            if (cellValue == "True")
                e.Cancel = true;
        }

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No items for current picklist - click Add button on toolbar or grid navigation panel to add");
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        AddRecord();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        DeleteRecord();
                    }
                    else if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 0) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "intOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "intOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "intOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "intOrder")) + 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow >= view.DataRowCount - 1) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "intOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "intOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "intOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "intOrder")) - 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiCreateDescriptionsFromBands.Enabled = (intPicklistHeaderID == 135 || intPicklistHeaderID == 136 ? true : false);
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
            SetMenuStatus();
        }

        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            // Show changes Pending //
            //barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
        }

       #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            AddRecord();
        }

        private void AddRecord()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("Column Error on current row in Picklist Item Grid - Correct before procceeding.", "Add", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intMaxOrder = 0;
            decimal decLastEndBand = (decimal)0.00;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "intOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "intOrder"));
                if (Convert.ToDecimal(view.GetRowCellValue(i, "BandEnd")) > decLastEndBand) decLastEndBand = Convert.ToDecimal(view.GetRowCellValue(i, "BandEnd"));
            }

            DataRow drNewRow = dataSet_AS_DataEntry.sp_AS_11133_Picklist_Items.NewRow();
            drNewRow["intHeaderID"] = intPicklistHeaderID;
            drNewRow["intOrder"] = intMaxOrder + 1;
            drNewRow["decRiskFactor"] = 0.00;
            drNewRow["strCode"] = "False";

            if (intPicklistHeaderID == 135 || intPicklistHeaderID == 136)
            {
                drNewRow["BandStart"] = decLastEndBand + (decimal)0.01;
                drNewRow["BandEnd"] = decLastEndBand + (decimal)beiBandIncrementSpinEdit.EditValue;
            }
            else
            {
                drNewRow["BandStart"] = (decimal)0.00;
                drNewRow["BandEnd"] = (decimal)0.00;
            }
            dataSet_AS_DataEntry.sp_AS_11133_Picklist_Items.Rows.Add(drNewRow);
            gridView1.FocusedRowHandle = gridView1.DataRowCount - 1;
            
            barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            DeleteRecord();
        }

        private void DeleteRecord()
        {
            GridView view = (GridView)gridControl1.MainView;
            /*if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                XtraMessageBox.Show("Select the row to be deleted by clicking on it first then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have 1 Picklist Item selected for delete!\n\nProceed?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                gridControl1.BeginUpdate();
                view.BeginSort();
                view.DeleteRow(view.FocusedRowHandle);  // Delete record from grid //
                view.EndSort();
                gridControl1.EndUpdate();
            }*/
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more picklist items to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            foreach (int intRowHandle in intRowHandles)
            {
                DataRow dr = view.GetDataRow(intRowHandle);

                if (((DataSet_AS_DataEntry.sp_AS_11133_Picklist_ItemsRow)(dr)).strCode == "True")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("System Field has been selected for deletion, this is a restricted operation and this delete process will be terminated.", "Delete Restricted", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Picklist " + (intRowHandles.Length == 1 ? "Item" : "Items") + " selected for delete!\n\nImportant Note: Deletions are only finalised on clicking the Save button.\n\nProceed?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(0);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();
                int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                int intUpdateProgressTempCount = 0;
              
                gridControl1.BeginUpdate();
                view.BeginSort();
                int intDeletedOrder = 0;
                int intItemOrder = 0;
                for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                {
                    intDeletedOrder = Convert.ToInt32(view.GetRowCellValue(intRowHandles[intRowHandle], "intOrder"));
                    view.DeleteRow(intRowHandles[intRowHandle]);
                    for (int i = 0; i < view.DataRowCount - 1; i++)
                    {
                        intItemOrder = Convert.ToInt32(view.GetRowCellValue(i, "intOrder"));
                        if (intItemOrder > intDeletedOrder) view.SetRowCellValue(i, "intOrder", intItemOrder - 1);
                    }
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                view.EndSort();
                gridControl1.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }

                DataSet dsChanges = this.dataSet_AS_DataEntry.GetChanges();
                if (dsChanges != null)
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                }
                else
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                }

            }
        }
        
        private void bbiSetOrder_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            gridControl1.BeginUpdate();
            view.BeginSort();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                view.SetRowCellValue(i, "intOrder", i + 1);  // Add 1 to value as DataSet is 0 based //
            }
            view.EndSort();
            gridControl1.EndUpdate();
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            GridView view = (GridView)gridControl1.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("Column Error on current row in Picklist Item Grid - Correct before procceeding.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            } 

            this.spAS11133PicklistItemsBindingSource.EndEdit();
            try
            {
                this.sp_AS_11133_Picklist_ItemsTableAdapter.Update(dataSet_AS_DataEntry);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the picklist item changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            //// Notify any open instances of Picklist Manager they will need to refresh their data on activating //
            //frm_AS_Picklist_Manager fParentForm;
            //foreach (Form frmChild in this.ParentForm.MdiChildren)
            //{
            //    if (frmChild.Name == "frm_AS_Picklist_Manager")
            //    {
            //        fParentForm = (frm_AS_Picklist_Manager)frmChild;
            //        fParentForm.UpdateFormRefreshStatus(0);
            //    }
            //}

                // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                if (Application.OpenForms[strCaller] != null)
                {
                    (Application.OpenForms[strCaller] as frm_AS_Picklist_Manager).UpdateFormRefreshStatus(1, intPicklistHeaderID.ToString(), "", "");
                    (Application.OpenForms[strCaller] as frm_AS_Picklist_Manager).frmActivated();
                }
           

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();          
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.spAS11133PicklistItemsBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //

            for (int i = 0; i < this.dataSet_AS_DataEntry.sp_AS_11133_Picklist_Items.Rows.Count; i++)
            {
                switch (this.dataSet_AS_DataEntry.sp_AS_11133_Picklist_Items.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void frm_AS_Picklist_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        this.dataSet_AS_DataEntry.sp_AS_11133_Picklist_Items.Rows.Clear();
                        //Detach_EditValueChanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();

        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }



        private void repositoryItemTextEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemTextEdit2_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemTextEdit3_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemSpinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void bbiCreateDescriptionsFromBands_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("Column Error on current row in Picklist Item Grid - Correct before procceeding.", "Set Item Descriptions From Bands", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            view.BeginUpdate();
            view.BeginSort();
            view.BeginDataUpdate();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                view.SetRowCellValue(i, "strDescription", view.GetRowCellValue(i, "BandStart").ToString() + " - " + view.GetRowCellValue(i, "BandEnd").ToString());
            }
            view.EndDataUpdate();
            view.EndSort();
            view.EndUpdate();
        }



    

     


    }
}


﻿namespace WoodPlan5
{
    partial class frm_AS_Billing_Codes_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Billing_Codes_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProviderBillingCentreCode = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.billingCentreCodeDataLayoutControl = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtBillingCentreCode = new DevExpress.XtraEditors.TextEdit();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ceTick3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ceTick2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spAS11038BillingCentreCodeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ceTick1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.lueDepartment = new DevExpress.XtraEditors.LookUpEdit();
            this.lueCostCentre = new DevExpress.XtraEditors.LookUpEdit();
            this.lueCompany = new DevExpress.XtraEditors.LookUpEdit();
            this.billingCentreCodeLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDepartment_ID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCost_Centre_ID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCompany_ID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter();
            this.sp_AS_11000_Duplicate_SearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter();
            this.spAS11057CompanyItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11057_Company_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11057_Company_ItemTableAdapter();
            this.spAS11060DepartmentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11060_Department_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11060_Department_ItemTableAdapter();
            this.spAS11063CostCentreItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11063_Cost_Centre_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11063_Cost_Centre_ItemTableAdapter();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompany1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompany2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProviderBillingCentreCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingCentreCodeDataLayoutControl)).BeginInit();
            this.billingCentreCodeDataLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillingCentreCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceTick3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceTick2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11038BillingCentreCodeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceTick1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCostCentre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCompany.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingCentreCodeLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartment_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCost_Centre_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompany_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11057CompanyItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11060DepartmentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11063CostCentreItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1668, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 664);
            this.barDockControlBottom.Size = new System.Drawing.Size(1668, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 638);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1668, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 638);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 36;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            //this.tableAdapterManager.sp_AS_11069_Tyre_Information_ItemTableAdapter = null;
            //this.tableAdapterManager.sp_AS_11072_Tyre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProviderBillingCentreCode
            // 
            this.dxErrorProviderBillingCentreCode.ContainerControl = this;
            // 
            // billingCentreCodeDataLayoutControl
            // 
            this.billingCentreCodeDataLayoutControl.Controls.Add(this.txtBillingCentreCode);
            this.billingCentreCodeDataLayoutControl.Controls.Add(this.gridSplitContainer3);
            this.billingCentreCodeDataLayoutControl.Controls.Add(this.gridSplitContainer2);
            this.billingCentreCodeDataLayoutControl.Controls.Add(this.gridSplitContainer1);
            this.billingCentreCodeDataLayoutControl.Controls.Add(this.lueDepartment);
            this.billingCentreCodeDataLayoutControl.Controls.Add(this.lueCostCentre);
            this.billingCentreCodeDataLayoutControl.Controls.Add(this.lueCompany);
            this.billingCentreCodeDataLayoutControl.DataSource = this.spAS11038BillingCentreCodeItemBindingSource;
            this.billingCentreCodeDataLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billingCentreCodeDataLayoutControl.Location = new System.Drawing.Point(0, 26);
            this.billingCentreCodeDataLayoutControl.Name = "billingCentreCodeDataLayoutControl";
            this.billingCentreCodeDataLayoutControl.Root = this.billingCentreCodeLayoutControlGroup;
            this.billingCentreCodeDataLayoutControl.Size = new System.Drawing.Size(1668, 638);
            this.billingCentreCodeDataLayoutControl.TabIndex = 4;
            this.billingCentreCodeDataLayoutControl.Text = "dataLayoutControl1";
            // 
            // txtBillingCentreCode
            // 
            this.txtBillingCentreCode.Location = new System.Drawing.Point(105, 12);
            this.txtBillingCentreCode.MenuManager = this.barManager1;
            this.txtBillingCentreCode.Name = "txtBillingCentreCode";
            this.txtBillingCentreCode.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtBillingCentreCode, true);
            this.txtBillingCentreCode.Size = new System.Drawing.Size(1551, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtBillingCentreCode, optionsSpelling1);
            this.txtBillingCentreCode.StyleController = this.billingCentreCodeDataLayoutControl;
            this.txtBillingCentreCode.TabIndex = 11;
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer3.Grid = this.gridControl3;
            this.gridSplitContainer3.Location = new System.Drawing.Point(1171, 60);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer3.Size = new System.Drawing.Size(485, 532);
            this.gridSplitContainer3.TabIndex = 10;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.spAS11038BillingCentreCodeItemBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceTick3});
            this.gridControl3.Size = new System.Drawing.Size(485, 532);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingCentreCodeID2,
            this.colBillingCentreCode2,
            this.colCompanyID2,
            this.colCompany2,
            this.colCompanyCode2,
            this.colDepartmentID2,
            this.colDepartment2,
            this.colDepartmentCode2,
            this.colCostCentreID2,
            this.colCostCentre2,
            this.colCostCentreCode2,
            this.colMode1,
            this.colRecordID1,
            this.gridColumn3});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 1;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            // 
            // ceTick3
            // 
            this.ceTick3.AutoHeight = false;
            this.ceTick3.Caption = "Check";
            this.ceTick3.Name = "ceTick3";
            this.ceTick3.ValueChecked = 0;
            this.ceTick3.ValueUnchecked = 1;
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(576, 60);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(591, 532);
            this.gridSplitContainer2.TabIndex = 9;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.spAS11038BillingCentreCodeItemBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceTick2});
            this.gridControl2.Size = new System.Drawing.Size(591, 532);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingCentreCodeID1,
            this.colBillingCentreCode1,
            this.colCompanyID1,
            this.colCompany1,
            this.colCompanyCode1,
            this.colDepartmentID1,
            this.colDepartment,
            this.colDepartmentCode1,
            this.colCostCentreID1,
            this.colCostCentre1,
            this.colCostCentreCode1,
            this.colMode2,
            this.colRecordID2,
            this.gridColumn1});
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 1;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // ceTick2
            // 
            this.ceTick2.AutoHeight = false;
            this.ceTick2.Caption = "Check";
            this.ceTick2.Name = "ceTick2";
            this.ceTick2.ValueChecked = 0;
            this.ceTick2.ValueUnchecked = 1;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(12, 60);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(560, 532);
            this.gridSplitContainer1.TabIndex = 8;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spAS11038BillingCentreCodeItemBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceTick1});
            this.gridControl1.Size = new System.Drawing.Size(560, 532);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spAS11038BillingCentreCodeItemBindingSource
            // 
            this.spAS11038BillingCentreCodeItemBindingSource.DataMember = "sp_AS_11038_Billing_Centre_Code_Item";
            this.spAS11038BillingCentreCodeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingCentreCodeID,
            this.colBillingCentreCode,
            this.colCompanyID,
            this.colCompany,
            this.colCompanyCode,
            this.colDepartmentID,
            this.colDepartment1,
            this.colDepartmentCode,
            this.colCostCentreID,
            this.colCostCentre,
            this.colCostCentreCode,
            this.colMode,
            this.colRecordID,
            this.gridColumn2});
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 1;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // ceTick1
            // 
            this.ceTick1.AutoHeight = false;
            this.ceTick1.Caption = "Check";
            this.ceTick1.Name = "ceTick1";
            this.ceTick1.ValueChecked = 0;
            this.ceTick1.ValueUnchecked = 1;
            // 
            // lueDepartment
            // 
            this.lueDepartment.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11038BillingCentreCodeItemBindingSource, "Department_ID", true));
            this.lueDepartment.Location = new System.Drawing.Point(669, 36);
            this.lueDepartment.MenuManager = this.barManager1;
            this.lueDepartment.Name = "lueDepartment";
            this.lueDepartment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDepartment.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Department_Code", "Department Code", 98, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Department", "Department", 67, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueDepartment.Properties.DataSource = this.spAS11060DepartmentItemBindingSource;
            this.lueDepartment.Properties.DisplayMember = "Department_Code";
            this.lueDepartment.Properties.NullText = "--Please Select A Department--";
            this.lueDepartment.Properties.ValueMember = "Department_ID";
            this.lueDepartment.Size = new System.Drawing.Size(496, 20);
            this.lueDepartment.StyleController = this.billingCentreCodeDataLayoutControl;
            this.lueDepartment.TabIndex = 21;
            this.lueDepartment.Tag = "Department";
            this.lueDepartment.EditValueChanged += new System.EventHandler(this.lueCommon_EditValueChanged);
            this.lueDepartment.Validating += new System.ComponentModel.CancelEventHandler(this.lueDepartment_Validating);
            // 
            // lueCostCentre
            // 
            this.lueCostCentre.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11038BillingCentreCodeItemBindingSource, "Cost_Centre_ID", true));
            this.lueCostCentre.Location = new System.Drawing.Point(1262, 36);
            this.lueCostCentre.MenuManager = this.barManager1;
            this.lueCostCentre.Name = "lueCostCentre";
            this.lueCostCentre.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueCostCentre.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Cost_Centre_Code", "Cost Centre Code", 102, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Cost_Centre", "Cost Centre", 71, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueCostCentre.Properties.DataSource = this.spAS11063CostCentreItemBindingSource;
            this.lueCostCentre.Properties.DisplayMember = "Cost_Centre_Code";
            this.lueCostCentre.Properties.NullText = "--Please Select A Cost Centre--";
            this.lueCostCentre.Properties.ValueMember = "Cost_Centre_ID";
            this.lueCostCentre.Size = new System.Drawing.Size(394, 20);
            this.lueCostCentre.StyleController = this.billingCentreCodeDataLayoutControl;
            this.lueCostCentre.TabIndex = 22;
            this.lueCostCentre.Tag = "Cost Centre";
            this.lueCostCentre.EditValueChanged += new System.EventHandler(this.lueCommon_EditValueChanged);
            this.lueCostCentre.Validating += new System.ComponentModel.CancelEventHandler(this.lueCostCentre_Validating);
            // 
            // lueCompany
            // 
            this.lueCompany.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11038BillingCentreCodeItemBindingSource, "Company_ID", true));
            this.lueCompany.Location = new System.Drawing.Point(105, 36);
            this.lueCompany.MenuManager = this.barManager1;
            this.lueCompany.Name = "lueCompany";
            this.lueCompany.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueCompany.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Company_Code", "Company Code", 86, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Company", "Company", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueCompany.Properties.DataSource = this.spAS11057CompanyItemBindingSource;
            this.lueCompany.Properties.DisplayMember = "Company_Code";
            this.lueCompany.Properties.NullText = "--Please Select A Company--";
            this.lueCompany.Properties.ValueMember = "Company_ID";
            this.lueCompany.Size = new System.Drawing.Size(467, 20);
            this.lueCompany.StyleController = this.billingCentreCodeDataLayoutControl;
            this.lueCompany.TabIndex = 23;
            this.lueCompany.Tag = "Company";
            this.lueCompany.EditValueChanged += new System.EventHandler(this.lueCommon_EditValueChanged);
            this.lueCompany.Validating += new System.ComponentModel.CancelEventHandler(this.lueCompany_Validating);
            // 
            // billingCentreCodeLayoutControlGroup
            // 
            this.billingCentreCodeLayoutControlGroup.CustomizationFormText = "keeperLayoutControlGroup";
            this.billingCentreCodeLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.billingCentreCodeLayoutControlGroup.GroupBordersVisible = false;
            this.billingCentreCodeLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.ItemForDepartment_ID,
            this.ItemForCost_Centre_ID,
            this.layoutControlGroup1,
            this.ItemForCompany_ID});
            this.billingCentreCodeLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.billingCentreCodeLayoutControlGroup.Name = "billingCentreCodeLayoutControlGroup";
            this.billingCentreCodeLayoutControlGroup.Size = new System.Drawing.Size(1668, 638);
            this.billingCentreCodeLayoutControlGroup.Text = "billingCentreCodeLayoutControlGroup";
            this.billingCentreCodeLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridSplitContainer1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(564, 536);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridSplitContainer2;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(564, 48);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(595, 536);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridSplitContainer3;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(1159, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(489, 536);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtBillingCentreCode;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1648, 24);
            this.layoutControlItem5.Text = "Billing Centre Code";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 13);
            // 
            // ItemForDepartment_ID
            // 
            this.ItemForDepartment_ID.Control = this.lueDepartment;
            this.ItemForDepartment_ID.CustomizationFormText = "Department Code";
            this.ItemForDepartment_ID.Location = new System.Drawing.Point(564, 24);
            this.ItemForDepartment_ID.Name = "ItemForDepartment_ID";
            this.ItemForDepartment_ID.Size = new System.Drawing.Size(593, 24);
            this.ItemForDepartment_ID.Text = "Department Code";
            this.ItemForDepartment_ID.TextSize = new System.Drawing.Size(90, 13);
            // 
            // ItemForCost_Centre_ID
            // 
            this.ItemForCost_Centre_ID.Control = this.lueCostCentre;
            this.ItemForCost_Centre_ID.CustomizationFormText = "Cost Centre Code";
            this.ItemForCost_Centre_ID.Location = new System.Drawing.Point(1157, 24);
            this.ItemForCost_Centre_ID.Name = "ItemForCost_Centre_ID";
            this.ItemForCost_Centre_ID.Size = new System.Drawing.Size(491, 24);
            this.ItemForCost_Centre_ID.Text = "Cost Centre Code";
            this.ItemForCost_Centre_ID.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 584);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1648, 34);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // ItemForCompany_ID
            // 
            this.ItemForCompany_ID.Control = this.lueCompany;
            this.ItemForCompany_ID.CustomizationFormText = "Company";
            this.ItemForCompany_ID.Location = new System.Drawing.Point(0, 24);
            this.ItemForCompany_ID.Name = "ItemForCompany_ID";
            this.ItemForCompany_ID.Size = new System.Drawing.Size(564, 24);
            this.ItemForCompany_ID.Text = "Company";
            this.ItemForCompany_ID.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 407);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1504, 1);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // sp_AS_11038_Billing_Centre_Code_ItemTableAdapter
            // 
            this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_Duplicate_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11057CompanyItemBindingSource
            // 
            this.spAS11057CompanyItemBindingSource.DataMember = "sp_AS_11057_Company_Item";
            this.spAS11057CompanyItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11057_Company_ItemTableAdapter
            // 
            this.sp_AS_11057_Company_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11060DepartmentItemBindingSource
            // 
            this.spAS11060DepartmentItemBindingSource.DataMember = "sp_AS_11060_Department_Item";
            this.spAS11060DepartmentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11060_Department_ItemTableAdapter
            // 
            this.sp_AS_11060_Department_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11063CostCentreItemBindingSource
            // 
            this.spAS11063CostCentreItemBindingSource.DataMember = "sp_AS_11063_Cost_Centre_Item";
            this.spAS11063CostCentreItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11063_Cost_Centre_ItemTableAdapter
            // 
            this.sp_AS_11063_Cost_Centre_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // colBillingCentreCode
            // 
            this.colBillingCentreCode.FieldName = "BillingCentreCode";
            this.colBillingCentreCode.Name = "colBillingCentreCode";
            this.colBillingCentreCode.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCode.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCode.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBillingCentreCode.Width = 111;
            // 
            // colCompanyID
            // 
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyID.Width = 80;
            // 
            // colCompany
            // 
            this.colCompany.FieldName = "Company";
            this.colCompany.Name = "colCompany";
            this.colCompany.OptionsColumn.AllowEdit = false;
            this.colCompany.OptionsColumn.AllowFocus = false;
            this.colCompany.OptionsColumn.ReadOnly = true;
            this.colCompany.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompany.Visible = true;
            this.colCompany.VisibleIndex = 0;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyCode.Visible = true;
            this.colCompanyCode.VisibleIndex = 1;
            this.colCompanyCode.Width = 94;
            // 
            // colDepartmentID
            // 
            this.colDepartmentID.FieldName = "DepartmentID";
            this.colDepartmentID.Name = "colDepartmentID";
            this.colDepartmentID.OptionsColumn.AllowEdit = false;
            this.colDepartmentID.OptionsColumn.AllowFocus = false;
            this.colDepartmentID.OptionsColumn.ReadOnly = true;
            this.colDepartmentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentID.Width = 92;
            // 
            // colDepartment1
            // 
            this.colDepartment1.FieldName = "Department";
            this.colDepartment1.Name = "colDepartment1";
            this.colDepartment1.OptionsColumn.AllowEdit = false;
            this.colDepartment1.OptionsColumn.AllowFocus = false;
            this.colDepartment1.OptionsColumn.ReadOnly = true;
            this.colDepartment1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartment1.Width = 78;
            // 
            // colDepartmentCode
            // 
            this.colDepartmentCode.FieldName = "DepartmentCode";
            this.colDepartmentCode.Name = "colDepartmentCode";
            this.colDepartmentCode.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentCode.Width = 106;
            // 
            // colCostCentreID
            // 
            this.colCostCentreID.FieldName = "CostCentreID";
            this.colCostCentreID.Name = "colCostCentreID";
            this.colCostCentreID.OptionsColumn.AllowEdit = false;
            this.colCostCentreID.OptionsColumn.AllowFocus = false;
            this.colCostCentreID.OptionsColumn.ReadOnly = true;
            this.colCostCentreID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreID.Width = 93;
            // 
            // colCostCentre
            // 
            this.colCostCentre.FieldName = "CostCentre";
            this.colCostCentre.Name = "colCostCentre";
            this.colCostCentre.OptionsColumn.AllowEdit = false;
            this.colCostCentre.OptionsColumn.AllowFocus = false;
            this.colCostCentre.OptionsColumn.ReadOnly = true;
            this.colCostCentre.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentre.Width = 79;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreCode.Width = 107;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tick To Include Company";
            this.gridColumn2.ColumnEdit = this.ceTick1;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            this.gridColumn2.Width = 140;
            // 
            // colBillingCentreCodeID1
            // 
            this.colBillingCentreCodeID1.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID1.Name = "colBillingCentreCodeID1";
            this.colBillingCentreCodeID1.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID1.Width = 125;
            // 
            // colBillingCentreCode1
            // 
            this.colBillingCentreCode1.FieldName = "BillingCentreCode";
            this.colBillingCentreCode1.Name = "colBillingCentreCode1";
            this.colBillingCentreCode1.Width = 111;
            // 
            // colCompanyID1
            // 
            this.colCompanyID1.FieldName = "CompanyID";
            this.colCompanyID1.Name = "colCompanyID1";
            this.colCompanyID1.Width = 80;
            // 
            // colCompany1
            // 
            this.colCompany1.FieldName = "Company";
            this.colCompany1.Name = "colCompany1";
            // 
            // colCompanyCode1
            // 
            this.colCompanyCode1.FieldName = "CompanyCode";
            this.colCompanyCode1.Name = "colCompanyCode1";
            this.colCompanyCode1.Width = 94;
            // 
            // colDepartmentID1
            // 
            this.colDepartmentID1.FieldName = "DepartmentID";
            this.colDepartmentID1.Name = "colDepartmentID1";
            this.colDepartmentID1.Width = 92;
            // 
            // colDepartment
            // 
            this.colDepartment.FieldName = "Department";
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 0;
            this.colDepartment.Width = 78;
            // 
            // colDepartmentCode1
            // 
            this.colDepartmentCode1.FieldName = "DepartmentCode";
            this.colDepartmentCode1.Name = "colDepartmentCode1";
            this.colDepartmentCode1.Visible = true;
            this.colDepartmentCode1.VisibleIndex = 1;
            this.colDepartmentCode1.Width = 106;
            // 
            // colCostCentreID1
            // 
            this.colCostCentreID1.FieldName = "CostCentreID";
            this.colCostCentreID1.Name = "colCostCentreID1";
            this.colCostCentreID1.Width = 93;
            // 
            // colCostCentre1
            // 
            this.colCostCentre1.FieldName = "CostCentre";
            this.colCostCentre1.Name = "colCostCentre1";
            this.colCostCentre1.Width = 79;
            // 
            // colCostCentreCode1
            // 
            this.colCostCentreCode1.FieldName = "CostCentreCode";
            this.colCostCentreCode1.Name = "colCostCentreCode1";
            this.colCostCentreCode1.Width = 107;
            // 
            // colMode2
            // 
            this.colMode2.FieldName = "Mode";
            this.colMode2.Name = "colMode2";
            this.colMode2.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode2.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colRecordID2
            // 
            this.colRecordID2.FieldName = "RecordID";
            this.colRecordID2.Name = "colRecordID2";
            this.colRecordID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID2.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Tick To Include Department";
            this.gridColumn1.ColumnEdit = this.ceTick2;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 2;
            this.gridColumn1.Width = 152;
            // 
            // colBillingCentreCodeID2
            // 
            this.colBillingCentreCodeID2.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID2.Name = "colBillingCentreCodeID2";
            this.colBillingCentreCodeID2.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID2.Width = 125;
            // 
            // colBillingCentreCode2
            // 
            this.colBillingCentreCode2.FieldName = "BillingCentreCode";
            this.colBillingCentreCode2.Name = "colBillingCentreCode2";
            this.colBillingCentreCode2.Width = 111;
            // 
            // colCompanyID2
            // 
            this.colCompanyID2.FieldName = "CompanyID";
            this.colCompanyID2.Name = "colCompanyID2";
            this.colCompanyID2.Width = 80;
            // 
            // colCompany2
            // 
            this.colCompany2.FieldName = "Company";
            this.colCompany2.Name = "colCompany2";
            // 
            // colCompanyCode2
            // 
            this.colCompanyCode2.FieldName = "CompanyCode";
            this.colCompanyCode2.Name = "colCompanyCode2";
            this.colCompanyCode2.Width = 94;
            // 
            // colDepartmentID2
            // 
            this.colDepartmentID2.FieldName = "DepartmentID";
            this.colDepartmentID2.Name = "colDepartmentID2";
            this.colDepartmentID2.Width = 92;
            // 
            // colDepartment2
            // 
            this.colDepartment2.FieldName = "Department";
            this.colDepartment2.Name = "colDepartment2";
            this.colDepartment2.Width = 78;
            // 
            // colDepartmentCode2
            // 
            this.colDepartmentCode2.FieldName = "DepartmentCode";
            this.colDepartmentCode2.Name = "colDepartmentCode2";
            this.colDepartmentCode2.Width = 106;
            // 
            // colCostCentreID2
            // 
            this.colCostCentreID2.FieldName = "CostCentreID";
            this.colCostCentreID2.Name = "colCostCentreID2";
            // 
            // colCostCentre2
            // 
            this.colCostCentre2.FieldName = "CostCentre";
            this.colCostCentre2.Name = "colCostCentre2";
            this.colCostCentre2.Visible = true;
            this.colCostCentre2.VisibleIndex = 0;
            this.colCostCentre2.Width = 79;
            // 
            // colCostCentreCode2
            // 
            this.colCostCentreCode2.FieldName = "CostCentreCode";
            this.colCostCentreCode2.Name = "colCostCentreCode2";
            this.colCostCentreCode2.Visible = true;
            this.colCostCentreCode2.VisibleIndex = 1;
            this.colCostCentreCode2.Width = 107;
            // 
            // colMode1
            // 
            this.colMode1.FieldName = "Mode";
            this.colMode1.Name = "colMode1";
            this.colMode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode1.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colRecordID1
            // 
            this.colRecordID1.FieldName = "RecordID";
            this.colRecordID1.Name = "colRecordID1";
            this.colRecordID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID1.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Tick To Include Department";
            this.gridColumn3.ColumnEdit = this.ceTick3;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            this.gridColumn3.Width = 152;
            // 
            // frm_AS_Billing_Codes_Edit
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1668, 694);
            this.Controls.Add(this.billingCentreCodeDataLayoutControl);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_AS_Billing_Codes_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Billing Centre Code Details Edit";
            this.Activated += new System.EventHandler(this.frm_AS_Billing_Codes_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Billing_Codes_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Billing_Codes_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.billingCentreCodeDataLayoutControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProviderBillingCentreCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingCentreCodeDataLayoutControl)).EndInit();
            this.billingCentreCodeDataLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBillingCentreCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceTick3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceTick2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11038BillingCentreCodeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceTick1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCostCentre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCompany.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingCentreCodeLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartment_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCost_Centre_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompany_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11057CompanyItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11060DepartmentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11063CostCentreItemBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProviderBillingCentreCode;
        private DevExpress.XtraDataLayout.DataLayoutControl billingCentreCodeDataLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup billingCentreCodeLayoutControlGroup;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceTick1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceTick2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceTick3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txtBillingCentreCode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private System.Windows.Forms.BindingSource spAS11038BillingCentreCodeItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter sp_AS_11038_Billing_Centre_Code_ItemTableAdapter;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDepartment_ID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCost_Centre_ID;
        private DevExpress.XtraEditors.LookUpEdit lueDepartment;
        private DevExpress.XtraEditors.LookUpEdit lueCostCentre;
        private DevExpress.XtraEditors.LookUpEdit lueCompany;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCompany_ID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter sp_AS_11000_Duplicate_SearchTableAdapter;
        private System.Windows.Forms.BindingSource spAS11060DepartmentItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11063CostCentreItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11057CompanyItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11057_Company_ItemTableAdapter sp_AS_11057_Company_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11060_Department_ItemTableAdapter sp_AS_11060_Department_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11063_Cost_Centre_ItemTableAdapter sp_AS_11063_Cost_Centre_ItemTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID2;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCode2;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID2;
        private DevExpress.XtraGrid.Columns.GridColumn colCompany2;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode2;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment2;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreID2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode2;
        private DevExpress.XtraGrid.Columns.GridColumn colMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompany1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;


    }
}

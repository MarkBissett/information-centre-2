﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;
using System.Data.SqlClient;

namespace WoodPlan5
{
    public partial class frm_AS_Keeper_Edit : BaseObjects.frmBase
    {
        public frm_AS_Keeper_Edit()
        {
            InitializeComponent();
        }

        private void frm_AS_Keeper_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 110102;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            //
            sp_AS_11002_Equipment_ItemTableAdaper = new DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter();
            sp_AS_11002_Equipment_ItemTableAdaper.Connection.ConnectionString = strConnectionString;

            sp_AS_11002_Equipment_ItemTableAdaper.Fill(this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, strEquipmentIDs, "view");


            P11DCategoryIncluded = false;
            foreach (DataRow dR in this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item.Rows)
            {
                int category = 0;
                int.TryParse(dR["EquipmentCategory"].ToString(),out category);
                
                if (Classes.Assets.EquipmentCategory.Is_P11D_Required(category))
                { P11DCategoryIncluded = true; }

            }

            // connect adapters //
            sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11044_Keeper_Allocation_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11044_Keepers_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11000_PL_Keeper_TypeTableAdapter.Connection.ConnectionString = strConnectionString;

            //Populate picklist Data
            PopulateKeeperPickList();

            editFormDataLayoutControlGroup.BeginUpdate();
            txtEquipmentReference.Properties.ReadOnly = true;
            switch (strFormMode.ToLower())
            {
                case "add":
                    lueKeeperID.Enabled = false;
                    lueKeeperTypeID.Enabled = false;
                    deAllocationDate.Enabled = false;
                    deAllocationEndDate.Enabled = false;
                    addNewRow(FormMode.add);

                    if (strCaller == "ucStep3Page")
                    {
                        ItemForEquipmentReference.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    }
                    break;
                case "blockadd":
                    lueKeeperID.Enabled = false;
                    lueKeeperTypeID.Enabled = false;
                    deAllocationDate.Enabled = false;
                    deAllocationEndDate.Enabled = false;
                    addNewRow(FormMode.blockadd);
                    break;
                case "blockedit":
                    addNewRow(FormMode.blockedit);
                    this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.Rows[0].AcceptChanges();
                    break;
                case "edit":
                case "view":
                    try
                    {
                        loadKeeper();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
            }
            editFormDataLayoutControlGroup.EndUpdate();


            if (strFormMode == "view")  // Disable all controls //
            {
                editFormDataLayoutControlGroup.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            PostOpen(null);
        }

        #region Instance Variables


        bool blnKeeperTypeChanging = false;
        bool blnSavingChanges = false;
        bool forceFormClose = false;
        private bool IsActiveStatusSelected = false;
        private int ActiveStatusID = 0;
        public string strKeeperDescription = "";
        public bool KeeperChanged = false;
        public FormMode formMode;
        public string strEquipmentIDs = "";
        public string strGCReference;
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        bool bool_FormLoading = true;
        public bool editorDataChanged = false;
        public enum FormMode { add, edit, view, delete, blockadd, blockedit };
        public enum SentenceCase { Upper, Lower, Title, AsIs }
        public enum TransactionType { ListPrice, AdditionalPurchase }

        private DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter sp_AS_11002_Equipment_ItemTableAdaper;
        private bool P11DCategoryIncluded = false;
        private bool showAddPopup = true;
        #endregion

        #region Validate Method

        private bool validateLookupEdit(LookUpEdit lookupEdit)
        {
            bool valid = true;
            bool infoFlag = false;

            if (bool_FormLoading)
                return valid;
            if (forceFormClose)
                return valid;
            string filter = "";
            string errorText = "Please Select Value.";
            string infoText = "";

            if (lookupEdit.Tag == null ? false : true)
            {
                errorText = String.Format("Please Select {0}.", lookupEdit.Tag);
            }
            if (lookupEdit.EditValue == null || lookupEdit.Properties.GetDisplayText(lookupEdit.EditValue) == "")
            {
                dxErrorProvider.SetError(lookupEdit, errorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }

            //validate status
            if (lookupEdit.Name == "lueAllocationStatusID")
            {
                checkAllocationStatus();

                //Check if there are existing active
                //foreach (string p in splitStrRecords(strEquipmentIDs))
                //{
                    //sp_AS_11044_Keeper_Allocation_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List, p, "view");

                    DataRowView kRowView = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                    DataRow kRow = kRowView.Row;
                    string k = ((DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(kRow)).KeeperAllocationID.ToString();

                    string p = ((DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(kRow)).EquipmentID.ToString();
                    sp_AS_11044_Keeper_Allocation_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List, p, "view");

                    //check if there are existing active
                    if (this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List.Count > 0 && IsActiveStatusSelected)
                    {

                        if (formMode == FormMode.add || formMode == FormMode.blockadd)
                        {
                            filter = String.Format("[AllocationStatusID]  = '{0}'", ActiveStatusID);
                        }
                        else
                        {
                            filter = String.Format("[KeeperAllocationID] <> {0}and [AllocationStatusID]  = '{1}'", k, ActiveStatusID);
                        }

                        DataRow[] dr1 = dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List.Select(filter);
                        if (dr1.Length > 0)
                        {
                            if (formMode == FormMode.add || formMode == FormMode.blockadd)
                            {
                                infoText = "Current Active Keeper Allocation will be ended";
                                infoFlag = true;
                            }
                            else
                            { 
                            errorText = "Only 1 active status can be assigned per Asset/Equipment";
                            valid = false;
                            }
                        }
                    }

                //}

            }

            if (infoFlag)
            {
                if (showAddPopup)
                { 
                    XtraMessageBox.Show("Adding a new Active Keeper will end the current Active keeper allocation.",
                    "Keeper Edit", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            if (valid)
            {
                if (infoFlag)
                {
                    dxErrorProvider.SetError(lookupEdit, infoText, ErrorType.Information);

                }
                else
                {
                    dxErrorProvider.SetError(lookupEdit, "");
                }
            }
            else
            {
                dxErrorProvider.SetError(lookupEdit, errorText);
            }
            return valid;
        }

        private DateTime getCurrentAllocationDate()
        {
            DateTime currentAllocationDate;
            try
            {
                if (deAllocationDate.Properties.GetDisplayText(deAllocationDate.EditValue) == "")
                {
                    currentAllocationDate = DateTime.Parse("01/01/1900");
                }
                else
                {
                    currentAllocationDate = DateTime.Parse(deAllocationDate.Properties.GetDisplayText(deAllocationDate.EditValue));
                }
            }
            catch (Exception ex)
            {
                currentAllocationDate = DateTime.Parse("01/01/1900");
            }

            return currentAllocationDate;
        }

        private DateTime getCurrentEndDate()
        {
            DateTime currentEndDate;
            if (!IsActiveStatusSelected)
            {
                try
                {
                    if (deAllocationEndDate.Properties.GetDisplayText(deAllocationEndDate.EditValue) == "")
                    {
                        currentEndDate = DateTime.Parse("01/01/3000");
                    }
                    else
                    {
                        currentEndDate = DateTime.Parse(deAllocationEndDate.Properties.GetDisplayText(deAllocationEndDate.EditValue));
                    }
                }
                catch (Exception ex)
                {
                    currentEndDate = DateTime.Parse("01/01/3000");
                }
            }
            else
            {
                currentEndDate = DateTime.Parse("01/01/3001");
            }
            return currentEndDate;
        }

        private bool validateDateEdit(DateEdit dateEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
            if (forceFormClose)
                return valid;

            string ErrorText = "Please Enter a Valid Date.";
            checkAllocationStatus();

            if (dateEdit.Tag == null ? false : true)
            {
                ErrorText = String.Format("Please Enter a valid {0}.", dateEdit.Tag);
            }
            if (dateEdit.Tag == "Allocation End Date")
            {
                if (deAllocationDate.Properties.GetDisplayText(deAllocationDate.EditValue) == "")
                {
                    dxErrorProvider.SetError(deAllocationDate, "Please Enter a Valid Allocation Date.");
                }
            }
            if (IsActiveStatusSelected && dateEdit.Tag == "Allocation End Date")
            {
                dxErrorProvider.SetError(dateEdit, "");
                dateEdit.EditValue = null;
                valid = true;
                return valid;
            }
            else
            {
                if (dateEdit.Properties.GetDisplayText(dateEdit.EditValue) == "")
                {
                    dxErrorProvider.SetError(dateEdit, "Invalid date - correct before proceeding.");
                    valid = false;
                    return valid;
                }
            }

            //check if period is valid
            if (dateEdit.Tag == "Allocation End Date")
            {
                valid = checkPeriod(Convert.ToDateTime(dateEdit.EditValue));
            }
            //Check if period is not overlapping
            if (valid)
            {
                if (dateEdit.Tag == "Allocation Date")
                {
                    valid = checkOverlap(Convert.ToDateTime(dateEdit.EditValue), getCurrentEndDate());
                }
                else
                {
                    valid = checkOverlap(getCurrentAllocationDate(), Convert.ToDateTime(dateEdit.EditValue));
                }
            }

            if (dxErrorProvider.GetError(dateEdit) != "")
                ErrorText = dxErrorProvider.GetError(dateEdit);

            if (this.strFormMode.ToLower() == "blockedit" && dateEdit.Properties.GetDisplayText(dateEdit.EditValue) == "")
            {
                valid = false;
            }

            if (valid)
            {
                dxErrorProvider.SetError(dateEdit, "");
            }
            else
            {
                dxErrorProvider.SetError(dateEdit, ErrorText);
            }
            return valid;
        }

        #endregion

        #region Editor Events


        private void dateEdit_Validating(object sender, CancelEventArgs e)
        {
            if (bool_FormLoading)
                return;
            if (validateDateEdit((DateEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueKeeperID_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;
            if (formMode == FormMode.add || formMode == FormMode.blockadd)
            {
                //deAllocationDate.EditValue = null;
                //deAllocationEndDate.EditValue = null;
                if (IsActiveStatusSelected)
                {
                    deAllocationDate.Enabled = true;
                }
                else
                {
                    deAllocationDate.Enabled = true;
                    deAllocationEndDate.Enabled = true;
                }
            }
        }

        private void lueKeeperTypeID_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading || lueKeeperTypeID.EditValue == null || lueKeeperTypeID.Properties.GetDisplayText(lueKeeperTypeID.EditValue) == "")
                return;
            //blnKeeperTypeChanging = true;
            lueKeeperID.Enabled = true;
            filterKeeperItem(lueKeeperTypeID.EditValue.ToString());
            lueKeeperID.EditValue = null;
            //  blnKeeperTypeChanging = false;
        }

        private void filterKeeperItem(string strKeeperType)
        {
            this.spAS11044KeepersItemBindingSource.Filter = "KeeperType = " + strKeeperType;
        }

        private void lueAllocationStatusID_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;

            checkAllocationStatus();
            if (formMode == FormMode.add || formMode == FormMode.blockadd)
            {
                deAllocationDate.EditValue = null;
                deAllocationEndDate.EditValue = null;
                lueKeeperTypeID.EditValue = null;
                lueKeeperTypeID.Enabled = true;
                if (lueKeeperID.Enabled)//ignore disable if keeper not yet assigned
                {
                    if (IsActiveStatusSelected)
                    {
                        deAllocationDate.Enabled = true;
                    }
                    else
                    {
                        deAllocationDate.Enabled = true;
                        deAllocationEndDate.Enabled = true;
                    }
                }
            }
        }

        private void lookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        #endregion

        #region Unique Form Functions

        private void IdentifyChangedBindingSource(DataTable dt)
        {

            switch (dt.TableName)
            {
                case "sp_AS_11044_Keeper_Allocation_Item"://Keeper
                    editorDataChanged = true;
                    break;
            }
        }

        private void EndEdit()
        {
            spAS11044KeeperAllocationItemBindingSource.EndEdit();
        }

        private void OpenP11DManagerViaMain()
        {
            // Open Notification Manager via main //
            if (Application.OpenForms["frmMain2"] != null)
            {
                (Application.OpenForms["frmMain2"] as frmMain2).Open_Screen(1121);
            }

            if (Application.OpenForms["frm_AS_P11D_Manager"] != null)
            {
                (Application.OpenForms["frm_AS_P11D_Manager"] as frm_AS_P11D_Manager).LoadAdapters();
            }
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            showAddPopup = false;
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //         
            EndEdit();
            // this.BindingContext[equipmentDataLayoutControl.DataSource, equipmentDataLayoutControl.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            this.Validate();
            this.ValidateChildren();

            showAddPopup = true;

            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
                return result;

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            EndEdit();
            blnSavingChanges = true;

            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item);//Keeper Check
            blnSavingChanges = false;
            try
            {
                // Insert and Update queries defined in Table Adapter //
                if (editorDataChanged)
                {
                    if (strCaller == "ucStep3Page")
                    {
                        WoodPlan5.Forms.Assets.ImportWizard.ucExecutePage.drKeeperNewRowView = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                        WoodPlan5.Forms.Assets.ImportWizard.ucStep3Page.KeeperValid(true);
                        WoodPlan5.Forms.Assets.ImportWizard.ucExecutePage.strKeeper = lueKeeperID.Properties.GetDisplayText(lueKeeperID.EditValue);
                        WoodPlan5.Forms.Assets.ImportWizard.ucExecutePage.strKeeperType = lueKeeperTypeID.Properties.GetDisplayText(lueKeeperTypeID.EditValue);
                    }
                    else
                    {
                        this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Update(dataSet_AS_DataEntry);

                        /***
                        // P11d changes been included within sp_AS_11045_update as calls to exec sp_AS_11178
                        if (strFormMode.ToLower() == "add")
                        {
                            this.dataSet_AS_DataEntry.sp_AS_11166_P11D_Item.Rows[0]["KeeperAllocationID"] = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID;
                            //insert P11D changes
                           this.sp_AS_11166_P11D_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        else
                        {
                            //insert P11D changes
                            this.sp_AS_11166_P11D_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        ***/
                    }

                    editorDataChanged = false;
                    //option to email
                    if (strCaller != "ucStep3Page")
                    {
                        if (strFormMode.ToLower() == "add" || strFormMode.ToLower() == "blockadd")
                        {
                            XtraMessageBox.Show("Remember to check if the Billing Centre requires amending due to the changed keeper", "Keeper Edit Billing Centre", MessageBoxButtons.OK,MessageBoxIcon.Information);
                        }

                        if (P11DCategoryIncluded)
                        {
                            switch (XtraMessageBox.Show("Would you like to view the P11D Manager to email changes?", "Open P11D", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                            {
                                case DialogResult.No:
                                    // cancel operation //
                                    break;
                                case DialogResult.Yes:
                                    OpenP11DManagerViaMain();
                                    break;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            finally
            {
                if (!editorDataChanged)
                    forceFormClose = true;
            }
            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //          

            UpdateManagerScreen();
            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private void UpdateManagerScreen()
        {
            switch (strCaller)
            {
                case "frm_AS_Equipment_Manager":
                    if (formMode == FormMode.add)
                    {
                        string strEquipID = "";
                        string strNewID = "";
                        DataRowView currentRow = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                        if (currentRow != null)
                            strEquipID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].EquipmentID + ";";

                        strNewID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID + ";";
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).UpdateFormRefreshStatus(1, strEquipID, strNewID, "");
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, "");
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).frmActivated();
                        }
                    }
                    break;

                case "frm_AS_Rental_Manager":
                    if (formMode == FormMode.add)
                    {
                        string strEquipID = "";
                        string strNewID = "";
                        DataRowView currentRow = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                        if (currentRow != null)
                            strEquipID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].EquipmentID + ";";

                        strNewID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID + ";";
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).UpdateFormRefreshStatus(1, strEquipID, strNewID, "");
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, "");
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                            (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).frmActivated();
                        }
                    }
                    break;
                case "frm_AS_Fleet_Manager":
                    if (formMode == FormMode.add)
                    {
                        string strEquipID = "";
                        string strNewID = "";
                        DataRowView currentRow = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                        if (currentRow != null)
                            strEquipID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].EquipmentID + ";";

                        strNewID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID + ";";
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).UpdateFormRefreshStatus(1, strEquipID, strNewID, "");
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, "");
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                            (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).frmActivated();
                        }
                    }
                    break;
                case "frm_AS_Office_Manager":
                    if (formMode == FormMode.add)
                    {
                        string strEquipID = "";
                        string strNewID = "";
                        DataRowView currentRow = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                        if (currentRow != null)
                            strEquipID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].EquipmentID + ";";

                        strNewID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID + ";";
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).UpdateFormRefreshStatus(1, strEquipID, strNewID, "");
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, "");
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                            (Application.OpenForms[strCaller] as frm_AS_Office_Manager).frmActivated();
                        }
                    }
                    break;

                case "frm_AS_Gadget_Manager":
                    if (formMode == FormMode.add)
                    {
                        string strEquipID = "";
                        string strNewID = "";
                        DataRowView currentRow = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                        if (currentRow != null)
                            strEquipID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].EquipmentID + ";";

                        strNewID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID + ";";
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).UpdateFormRefreshStatus(1, strEquipID, strNewID, "");
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, "");
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                            (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).frmActivated();
                        }
                    }
                    break;
                case "frm_AS_Software_Manager":
                    if (formMode == FormMode.add)
                    {
                        string strEquipID = "";
                        string strNewID = "";
                        DataRowView currentRow = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                        if (currentRow != null)
                            strEquipID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].EquipmentID + ";";

                        strNewID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID + ";";
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).UpdateFormRefreshStatus(1, strEquipID, strNewID, "");
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, "");
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                            (Application.OpenForms[strCaller] as frm_AS_Software_Manager).frmActivated();
                        }
                    }
                    break;
                case "frm_AS_Hardware_Manager":
                    if (formMode == FormMode.add)
                    {
                        string strEquipID = "";
                        string strNewID = "";
                        DataRowView currentRow = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                        if (currentRow != null)
                            strEquipID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].EquipmentID + ";";

                        strNewID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID + ";";
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).UpdateFormRefreshStatus(1, strEquipID, strNewID, "");
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, "");
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                            (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).frmActivated();
                        }
                    }
                    break;
                case "frm_AS_Equipment_Edit":
                    if (formMode == FormMode.add)
                    {
                        string strEquipID = "";
                        string strNewID = "";
                        DataRowView currentRow = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                        if (currentRow != null)
                            strEquipID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].EquipmentID + ";";

                        strNewID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID + ";";
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Edit).UpdateFormRefreshStatus(1, strEquipID, strNewID, "");
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Edit).frmActivated();
                        }
                    }
                    else if (formMode == FormMode.edit || formMode == FormMode.blockedit)
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Edit).UpdateFormRefreshStatus(1, strEquipmentIDs, strRecordIDs, "");
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Edit).frmActivated();
                        }
                    }
                    else
                    {
                        // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                        if (Application.OpenForms[strCaller] != null)
                        {
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Edit).UpdateFormRefreshStatus(1, strRecordIDs, "", "");
                            (Application.OpenForms[strCaller] as frm_AS_Equipment_Edit).frmActivated();
                        }
                    }
                    break;
            }
        }

        private void PopulateKeeperPickList()
        {
            sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Keeper_Allocation_Status, 23);
            sp_AS_11000_PL_Keeper_TypeTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Keeper_Type, 4);
            sp_AS_11044_Keepers_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keepers_Item, null);
            sp_AS_11044_Keeper_Allocation_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List, "", "");
        }

        private void addNewRow(FormMode mode)
        {
            try
            {
                DataRow drNewRow = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.NewRow();
                drNewRow["Mode"] = (mode.ToString()).ToLower();
                string[] parts = splitStrRecords(strEquipmentIDs);

                drNewRow["EquipmentID"] = parts[0];
                drNewRow["EquipmentCategory"] = 0;

                if (mode == FormMode.blockedit)
                {
                    drNewRow["RecordID"] = strRecordIDs;
                    drNewRow["EquipmentReference"] = "";
                }
                if (mode == FormMode.add || mode == FormMode.blockadd)
                {
                    drNewRow["KeeperAllocationID"] = -1;
                    drNewRow["Keeper"] = strEquipmentIDs;
                    drNewRow["RecordID"] = strEquipmentIDs;
                    drNewRow["EquipmentReference"] = strGCReference;
                    drNewRow["AllocationDate"] = getCurrentDate();
                    if (this.GlobalSettings.Username == "tafadzwa.mandengu" || this.GlobalSettings.Username == "william.self")
                    {
                        drNewRow["KeeperTypeID"] = 10;
                        drNewRow["KeeperID"] = 316;
                        drNewRow["AllocationStatusID"] = 51;
                        drNewRow["AllocationDate"] = getCurrentDate();
                    }
                }
                this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.Rows.Add(drNewRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void loadKeeper()
        {
            //load Keepers
            editFormLayoutControlGroup.BeginUpdate();
            sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item, strRecordIDs, strFormMode);
            DataRowView kRowView = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
            DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow kRow = (DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)kRowView.Row;
            filterKeeperItem(kRow.KeeperTypeID.ToString());
            editFormLayoutControlGroup.EndUpdate();
        }

        private void checkAllocationStatus()
        {
            if (bool_FormLoading)
                return;

            if (lueAllocationStatusID.EditValue == null || lueAllocationStatusID.Properties.GetDisplayText(lueAllocationStatusID.EditValue) == "")
            {
                IsActiveStatusSelected = false;

                return;
            }
            else
            {
                DataSet_AS_DataEntry.sp_AS_11000_PL_Keeper_Allocation_StatusRow drs =
                    this.dataSet_AS_DataEntry.sp_AS_11000_PL_Keeper_Allocation_Status.FindByPickListID(Convert.ToInt32(lueAllocationStatusID.EditValue));

                if ((drs.Value).ToLower() == "active")
                {
                    IsActiveStatusSelected = true;
                    ActiveStatusID = Convert.ToInt32(drs.PickListID);
                }
                else
                {
                    IsActiveStatusSelected = false;
                }
            }

            if (IsActiveStatusSelected)
            {
                deAllocationEndDate.Properties.NullValuePrompt = "";
            }
            else
            {
                deAllocationEndDate.Properties.NullValuePrompt = "--Please Select End Date--";
            }
        }

        private bool checkOverlap(DateTime dateEditValue, DateTime dateEditValue1)
        {
            bool checkOK = true;
            // bool failedCheck = false;
            string filter = "";

            //Check for the current selected record.
            //Code was checking every supplied ID against only teh current selected items range
            //so was producing errored fields for different occurences, not that currently displayed
            //Check if period is not overlapping
            //foreach (string p in splitStrRecords(strEquipmentIDs))
            //{
                //sp_AS_11044_Keeper_Allocation_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List, p, "view");

                DataRowView kRowView = (DataRowView)spAS11044KeeperAllocationItemBindingSource.Current;
                DataRow kRow = kRowView.Row;
                string kID = ((DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(kRow)).KeeperAllocationID.ToString();
                string kRef = ((DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(kRow)).EquipmentReference.ToString();

                string p = ((DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(kRow)).EquipmentID.ToString();
                sp_AS_11044_Keeper_Allocation_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List, p, "view");


                //check if allocation date is between existing periods
                if (getCurrentAllocationDate() != DateTime.Parse("01/01/1900"))
                {

                    if (formMode == FormMode.add || formMode == FormMode.blockadd)
                    {
                        filter = "([AllocationDate] <=  '" + dateEditValue + "'  and  '" + dateEditValue + "' <= [AllocationEndDate] )";
                    }
                    else
                    {
                        filter = "[KeeperAllocationID] <> " + kID + " and ([AllocationDate] <=  '" + dateEditValue + "'  and  '" + dateEditValue + "' <= [AllocationEndDate] )";
                    }

                    DataRow[] dr1 = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List.Select(filter);
                    if (dr1.Length > 0)
                    {
                        dxErrorProvider.SetError(deAllocationDate, "Date Periods overlap on equipment reference " + kRef);
                        checkOK = false;
                        return checkOK;
                    }
                }
                else
                {
                    dxErrorProvider.SetError(deAllocationDate, "Invalid date, please correct to proceed.");
                    checkOK = false;
                    return checkOK;
                }

                if (getCurrentEndDate() != DateTime.Parse("01/01/3000"))
                {
                    if (formMode == FormMode.add || formMode == FormMode.blockadd)
                    {
                        filter = "([AllocationDate] <=  '" + dateEditValue1 + "'  and  '" + dateEditValue1 + "' <= [AllocationEndDate] )";
                    }
                    else
                    {
                        filter = "[KeeperAllocationID] <> " + kID + " and ([AllocationDate] <=  '" + dateEditValue1 + "'  and  '" + dateEditValue1 + "' <= [AllocationEndDate] )";
                    }

                    DataRow[] dr2 = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List.Select(filter);
                    if (dr2.Length > 0)
                    {
                        dxErrorProvider.SetError(deAllocationEndDate, "Date Periods overlap on equipment reference " + kRef);
                        checkOK = false;
                        return checkOK;
                    }
                }

                //check active overlap
                if (dateEditValue != DateTime.Parse("01/01/1900"))
                {
                    if (formMode == FormMode.add || formMode == FormMode.blockadd)
                    {
                        if (IsActiveStatusSelected)
                        {
                            filter = "('" + dateEditValue + "'<=  " + "[AllocationDate] and [AllocationEndDate] is null)";
                        }
                        else
                        {
                            filter = "('" + dateEditValue + "'>=  " + "[AllocationDate] and [AllocationEndDate] is null)";
                        }
                    }
                    else
                    {
                        filter = "[KeeperAllocationID] <> " + kID + " and ('" + dateEditValue + "'>=  " + "[AllocationDate] and [AllocationEndDate] is null)";
                    }

                    DataRow[] dr3 = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List.Select(filter);
                    if (dr3.Length > 0)
                    {
                        if ( (IsActiveStatusSelected) &&
                           (formMode == FormMode.add || formMode == FormMode.blockadd) )
                        {
                            dxErrorProvider.SetError(deAllocationDate, "Allocation Date must be greater than the current active allocation on equipment reference " + kRef);
                            dxErrorProvider.SetError(deAllocationEndDate, "Allocation Date must be greater than the current active allocation on equipment reference " + kRef);

                        }
                        else
                        {
                            dxErrorProvider.SetError(deAllocationDate, "Date Periods overlap with active allocation on equipment reference " + kRef);
                            dxErrorProvider.SetError(deAllocationEndDate, "Date Periods overlap with active allocation on equipment reference " + kRef);
                        }

                        checkOK = false;
                        return checkOK;
                    }
                }

                if (dateEditValue1 != DateTime.Parse("01/01/3000"))
                {
                    if (formMode == FormMode.add || formMode == FormMode.blockadd)
                    {
                        if (IsActiveStatusSelected)
                        {
                            filter = "('" + dateEditValue1 + "'<=  " + "[AllocationDate] and [AllocationEndDate] is null)";
                        }
                        else
                        {
                            filter = "('" + dateEditValue1 + "'>=  " + "[AllocationDate] and [AllocationEndDate] is null)";
                        }
                    }
                    else
                    {
                        filter = "[KeeperAllocationID] <> " + kID + " and ('" + dateEditValue1 + "'>=  " + "[AllocationDate] and [AllocationEndDate] is null)";
                    }

                    DataRow[] dr4 = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List.Select(filter);
                    if (dr4.Length > 0)
                    {
                        if (formMode == FormMode.add || formMode == FormMode.blockadd)
                        {
                          dxErrorProvider.SetError(deAllocationDate, "Date Periods overlap with active allocation on equipment reference " + kRef);
                            dxErrorProvider.SetError(deAllocationEndDate, "Date Periods overlap with active allocation on equipment reference " + kRef);
                        }

                    checkOK = false;
                    return checkOK;
                    }
                }

                //check if its the lastest allocation for active
                if (IsActiveStatusSelected)
                {
                    if (formMode == FormMode.add || formMode == FormMode.blockadd)
                    {
                        filter = "('" + dateEditValue + "'<=  " + "[AllocationDate])";
                    }
                    else
                    {
                        filter = "[KeeperAllocationID] <> " + kID + " and ('" + dateEditValue + "'<=  " + "[AllocationDate])";
                    }

                    DataRow[] dr5 = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_List.Select(filter);
                    if (dr5.Length > 0)
                    {
                        dxErrorProvider.SetError(deAllocationDate, "Active allocation has to be the latest allocation, error on equipment reference " + kRef);
                        checkOK = false;
                        return checkOK;
                    }
                }
            //}

            return checkOK;
        }

        private bool checkPeriod(DateTime endDate)
        {
            bool checkOK = true;

            string errorPeriod = "Invalid period - correct before proceeding.";
            if (!IsActiveStatusSelected)
            {
                //if (currentAllocationDate == DateTime.Parse("01/01/1990"))
                if (getCurrentDate() == DateTime.Parse("01/01/1900"))
                {
                    dxErrorProvider.SetError(deAllocationDate, errorPeriod);
                    checkOK = false;
                }

                if (getCurrentEndDate() == DateTime.Parse("01/01/3000"))
                {
                    dxErrorProvider.SetError(deAllocationEndDate, errorPeriod);
                    checkOK = false;
                }

                //TimeSpan ts1 = endDate - currentAllocationDate;
                TimeSpan ts1 = endDate - DateTime.Parse(deAllocationDate.Properties.GetDisplayText(deAllocationDate.EditValue));
                int differenceInDays1 = ts1.Days;
                if (differenceInDays1 < 0) //invalid period
                {
                    dxErrorProvider.SetError(deAllocationDate, errorPeriod);
                    dxErrorProvider.SetError(deAllocationEndDate, errorPeriod);
                    checkOK = false;
                }
            }
            return checkOK;
        }

        private void checkStatus()
        {
            string errorText = "Mismatch in Status and End date.";

            if (IsActiveStatusSelected)
            {
                //if (currentEndDate != DateTime.Parse("01/01/0001"))
                if (DateTime.Parse(deAllocationEndDate.EditValue.ToString()) != DateTime.Parse("01/01/3000"))
                {
                    errorText = "Please delete entered End Date. Active status requires blank end date";
                    dxErrorProvider.SetError(deAllocationEndDate, errorText);
                    return;
                }
            }
            else
            {
                //if (currentEndDate == DateTime.Parse("01/01/0001"))
                if (DateTime.Parse(deAllocationEndDate.EditValue.ToString()) == DateTime.Parse("01/01/3000"))
                {
                    errorText = "Please enter End Date. Non-Active status requires an end date";
                    dxErrorProvider.SetError(deAllocationEndDate, errorText);
                }
            }
        }


        private int getTransactionTypeID(string strTransactionType)
        {
            int defaultType = 0;
            DataRow[] dr = this.dataSet_AS_DataEntry.sp_AS_11000_PL_Transaction_Type.Select();
            int i = 0;
            foreach (DataRow row in dr)
            {
                if ((row[2].ToString()).ToLower() == strTransactionType.ToLower())
                {
                    defaultType = Convert.ToInt32(row[0]);
                    return defaultType;
                }
                i++;
            }
            return defaultType;
        }

        private void adjustTransactionType(TransactionType transactType)
        {
            string strTransactionTypeFilter = "";
            if (transactType == TransactionType.ListPrice)
            {
                //price list type                        
                strTransactionTypeFilter = "[TransactionTypeID] = " + getTransactionTypeID("list price");
            }
            else if (transactType == TransactionType.AdditionalPurchase)
            {
                strTransactionTypeFilter = "[TransactionTypeID] = " + getTransactionTypeID("additional purchase");
            }
            else
            {
                strTransactionTypeFilter = "";
            }

            //spAS11000PLTransactionTypeBindingSource.Filter = strTransactionTypeFilter;
            spAS11041TransactionItemBindingSource.Filter = strTransactionTypeFilter;

        }
        #endregion

        #region Standard Form Functions

        private DateTime getCurrentDate()
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetDate = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetDate.ChangeConnectionString(strConnectionString);
            GetDate.sp_AS_11138_Get_Server_Date();
            DateTime d = DateTime.Parse(GetDate.sp_AS_11138_Get_Server_Date().ToString());
            return d;
        }

        private string[] splitStrRecords(string records)
        {
            char[] delimiters = new char[] { ',', ';' };
            string[] parts;
            if (records == "")
            {
                parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                parts = records.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            return parts;
        }

        private void ClearErrors(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is LookUpEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is TextEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is DateEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is ColorPickEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is SpinEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is ContainerControl)
                    this.ClearErrors(item.Controls);
                if (item2 is DataLayoutControl)
                    this.ClearErrors(item.Controls);

            }
        }

        private string[] splitStrRecords()
        {
            char[] delimiters = new char[] { ',' };
            string[] parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            return parts;
        }

        private string SaveChangesExtracted(out bool shouldReturn)
        {
            shouldReturn = false;
           
            //if (dxErrorProvider.HasErrors)
            //Specific check as have include Information Type warning
            if (dxErrorProvider.HasErrorsOfType(ErrorType.Default))
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider, editFormDataLayoutControlGroup);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors +
                "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                shouldReturn = true;
                return "Error";
            }

            return String.Empty;
        }

        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
                if (item2 is DataLayoutControl) this.Attach_EditValueChanged_To_Children(item.Controls);

            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }

        private Boolean SetChangesPendingLabel()
        {
            EndEdit();

            DataSet dsChanges = this.dataSet_AS_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;

                if (strFormMode.ToLower() == "add")
                {
                    bbiFormSave.Enabled = true;
                }
                return false;
            }


        }

        private string CheckForPendingSave()
        {
            EndEdit();

            string strMessage = "";
            string strMessage1 = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item, " on the " + this.Text + " Form ");


            if (strMessage1 != "")
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (strMessage1 != "")
                    strMessage += strMessage1;
            }
            return strMessage;
        }

        private string CheckTablePendingSave(DataTable dt, string formArea)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (intRecordNew > 0) strMessage += string.Format("{0} New record(s){1}\n", Convert.ToString(intRecordNew), formArea);
                if (intRecordModified > 0) strMessage += string.Format("{0} Updated record(s){1}\n", Convert.ToString(intRecordModified), formArea);
                if (intRecordDeleted > 0) strMessage += string.Format("{0} Deleted record(s){1}\n", Convert.ToString(intRecordDeleted), formArea);
            }
            return strMessage;
        }

        private void CheckChangedTable(DataTable dt)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            int intRowP11DID = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int intRowEquipID = 0;

                //int intRowP11DID = -1;

                intRowP11DID--;

                string strRowManufacturerID = "";
                int intRowKeeperAllocationID = 0;
                System.DateTime deRowTransactionDate = getCurrentDate();
                int intRowKeeperType = 0;
                int intRowKeeperID = 0;
                string strRowKeeperFullName = "";
                Nullable<DateTime> deRowAllocationDate = null;
                Nullable<DateTime> deRowAllocationEndDate = null;
                int intRowAllocationStatus = 0;
                Decimal dcRowListPrice = 0;
                Decimal dcRowAdditionalCost = 0;
                int intRowEmissions = 0;
                int intRowEngineSize = 0;
                string strRowFuelType = "None";
                string strRowRegistrationPlate = "None";
                Nullable<DateTime> deRowRegistrationDate = null;
                string strRowMake = "None";
                string strRowModel = "None";
                string strRowNotes = "";

                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        strRowNotes = "New Keeper Record";
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        strRowNotes = "Modified Keeper Record";
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        strRowNotes = "Deleted Keeper Record";
                        break;
                }
                if (blnSavingChanges)
                {
                    intRowEquipID = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dt.Rows[i])).EquipmentID;

                    sp_AS_11001_Equipment_ManagerTableAdapter.Fill(dataSet_AS_Core.sp_AS_11001_Equipment_Manager, intRowEquipID.ToString());
                    sp_AS_11102_Rental_ManagerTableAdapter.Fill(dataSet_AS_Core.sp_AS_11102_Rental_Manager, intRowEquipID.ToString(), "view");
                    sp_AS_11108_Rental_Details_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11108_Rental_Details_Item, intRowEquipID.ToString(), "view");
                    sp_AS_11041_Transaction_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item, intRowEquipID.ToString(), "view");
                    sp_AS_11005_Vehicle_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item, intRowEquipID.ToString(), "view");
                    sp_AS_11000_PL_Transaction_TypeTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Transaction_Type, 6);

                    //int intEquipCount = this.dataSet_AS_Core.sp_AS_11001_Equipment_Manager.Rows.Count;
                    int intRentalCount = this.dataSet_AS_Core.sp_AS_11102_Rental_Manager.Rows.Count;
                    if (this.dataSet_AS_Core.sp_AS_11001_Equipment_Manager.Rows.Count > 0 || intRentalCount > 0)
                    {
                        if (intRentalCount > 0)
                        {
                            strRowManufacturerID = "N/A";
                            strRowMake = ((WoodPlan5.DataSet_AS_Core.sp_AS_11102_Rental_ManagerRow)(this.dataSet_AS_Core.sp_AS_11102_Rental_Manager.Rows[0])).Make;
                            strRowModel = ((WoodPlan5.DataSet_AS_Core.sp_AS_11102_Rental_ManagerRow)(this.dataSet_AS_Core.sp_AS_11102_Rental_Manager.Rows[0])).Model;
                            strRowRegistrationPlate = ((WoodPlan5.DataSet_AS_Core.sp_AS_11102_Rental_ManagerRow)(this.dataSet_AS_Core.sp_AS_11102_Rental_Manager.Rows[0])).UniqueReference;

                            //Fields Newly added to AS_Rental ICE-116
                            dcRowListPrice = ((WoodPlan5.DataSet_AS_Core.sp_AS_11102_Rental_ManagerRow)(this.dataSet_AS_Core.sp_AS_11102_Rental_Manager.Rows[0])).ListPrice;
                            intRowEmissions = ((WoodPlan5.DataSet_AS_Core.sp_AS_11102_Rental_ManagerRow)(this.dataSet_AS_Core.sp_AS_11102_Rental_Manager.Rows[0])).Emissions;
                            intRowEngineSize = ((WoodPlan5.DataSet_AS_Core.sp_AS_11102_Rental_ManagerRow)(this.dataSet_AS_Core.sp_AS_11102_Rental_Manager.Rows[0])).EngineSize;
                            strRowFuelType = ((WoodPlan5.DataSet_AS_Core.sp_AS_11102_Rental_ManagerRow)(this.dataSet_AS_Core.sp_AS_11102_Rental_Manager.Rows[0])).FuelType;

                            try
                            { 
                                deRowRegistrationDate = ((WoodPlan5.DataSet_AS_Core.sp_AS_11102_Rental_ManagerRow)(this.dataSet_AS_Core.sp_AS_11102_Rental_Manager.Rows[0])).RegistrationDate;
                            }
                            catch (StrongTypingException ex)
                            { }

                        }
                        else
                        {
                            strRowManufacturerID = ((WoodPlan5.DataSet_AS_Core.sp_AS_11001_Equipment_ManagerRow)(this.dataSet_AS_Core.sp_AS_11001_Equipment_Manager.Rows[0])).ManufacturerID;
                            strRowMake = ((WoodPlan5.DataSet_AS_Core.sp_AS_11001_Equipment_ManagerRow)(this.dataSet_AS_Core.sp_AS_11001_Equipment_Manager.Rows[0])).Make;
                            strRowModel = ((WoodPlan5.DataSet_AS_Core.sp_AS_11001_Equipment_ManagerRow)(this.dataSet_AS_Core.sp_AS_11001_Equipment_Manager.Rows[0])).Model;
                        }
                        intRowKeeperAllocationID = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dt.Rows[i])).KeeperAllocationID;
                        intRowKeeperType = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dt.Rows[i])).KeeperTypeID;
                        intRowKeeperID = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dt.Rows[i])).KeeperID;

                        //strRowKeeperFullName = lueKeeperID.Properties.GetDisplayText(lueKeeperID.EditValue);  
                        string filter = string.Format("[KeeperType] ={0} and [EmployeeNumber] = {1}", intRowKeeperType, intRowKeeperID);
                        DataRow[] dr1 = dataSet_AS_DataEntry.sp_AS_11044_Keepers_Item.Select(filter);
                        if (dr1.Length > 0)
                        {
                            strRowKeeperFullName = dr1[0]["FullName"].ToString();   
                        }

                        object value = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dt.Rows[i])).AllocationDate;
                        if (value != DBNull.Value)
                        {
                            deRowAllocationDate = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dt.Rows[i])).AllocationDate;
                        }
                        object value1 = dt.Rows[i].ItemArray[10];
                        if (value1 != DBNull.Value)
                        {
                            deRowAllocationEndDate = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dt.Rows[i])).AllocationEndDate;
                        }
                        intRowAllocationStatus = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)(dt.Rows[i])).AllocationStatusID;
                        adjustTransactionType(TransactionType.ListPrice);
                        if (spAS11041TransactionItemBindingSource.Count > 0)
                        {
                            dcRowListPrice = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11041_Transaction_ItemRow)(((System.Data.DataRowView)(spAS11041TransactionItemBindingSource.Current)).Row)).NetTransactionPrice +
                                ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11041_Transaction_ItemRow)(((System.Data.DataRowView)(spAS11041TransactionItemBindingSource.Current)).Row)).VAT;
                        }

                        adjustTransactionType(TransactionType.AdditionalPurchase);
                        if (spAS11041TransactionItemBindingSource.Count > 0)
                        {
                            dcRowAdditionalCost = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11041_Transaction_ItemRow)(((System.Data.DataRowView)(spAS11041TransactionItemBindingSource.Current)).Row)).NetTransactionPrice +
                                ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11041_Transaction_ItemRow)(((System.Data.DataRowView)(spAS11041TransactionItemBindingSource.Current)).Row)).VAT;
                        }
                        if (this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows.Count > 0)
                        {
                            intRowEmissions = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0])).Emissions;
                            intRowEngineSize = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0])).EngineSize;
                            strRowFuelType = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0])).FuelType;
                            strRowRegistrationPlate = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0])).RegistrationPlate;
                            try
                            {
                                deRowRegistrationDate = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0])).RegistrationDate;
                            }
                            catch (StrongTypingException ex)
                            { }
                        }

                        if (strCaller != "ucStep3Page")
                        {
                            DataRow drNewP11Row = this.dataSet_AS_DataEntry.sp_AS_11166_P11D_Item.NewRow();
                            drNewP11Row["Mode"] = "add";
                            drNewP11Row["RecordID"] = "-1";
                            drNewP11Row["EquipmentID"] = intRowEquipID;
                            drNewP11Row["P11DID"] = intRowP11DID;
                            drNewP11Row["ManufacturerID"] = strRowManufacturerID;
                            drNewP11Row["KeeperAllocationID"] = intRowKeeperAllocationID;
                            drNewP11Row["TransactionDate"] = deRowTransactionDate;
                            drNewP11Row["KeeperTypeID"] = intRowKeeperType;
                            drNewP11Row["KeeperID"] = intRowKeeperID;
                            drNewP11Row["KeeperFullName"] = strRowKeeperFullName;
                            if (deRowAllocationDate.HasValue)
                            {
                                drNewP11Row["AllocationDate"] = deRowAllocationDate;
                            }
                            if (deRowAllocationEndDate.HasValue)
                            {
                                drNewP11Row["AllocationEndDate"] = deRowAllocationEndDate;
                            }
                            drNewP11Row["AllocationStatus"] = intRowAllocationStatus;
                            drNewP11Row["ListPrice"] = dcRowListPrice;
                            drNewP11Row["AdditionalCost"] = dcRowAdditionalCost;
                            drNewP11Row["Emissions"] = intRowEmissions;
                            drNewP11Row["EngineSize"] = intRowEngineSize;
                            drNewP11Row["FuelType"] = strRowFuelType;
                            drNewP11Row["RegistrationPlate"] = strRowRegistrationPlate;
                            if (deRowRegistrationDate.HasValue)
                            {
                                drNewP11Row["RegistrationDate"] = deRowRegistrationDate;
                            }
                            drNewP11Row["Make"] = strRowMake;
                            drNewP11Row["Model"] = strRowModel;
                            drNewP11Row["Notes"] = strRowNotes;
                            drNewP11Row["SelectedForEmail"] = true;
                            this.dataSet_AS_DataEntry.sp_AS_11166_P11D_Item.Rows.Add(drNewP11Row);
                        }

                    }
                }
            }

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                IdentifyChangedBindingSource(dt);
            }
        }


        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            if (splashScreenManager != null && splashScreenManager.IsSplashFormVisible == true)
            {
                splashScreenManager.CloseWaitForm();
            }
            bool_FormLoading = false;
        }

        #endregion

        #region Compulsory Implementation

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode.ToLower())
            {
                case "add":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.Appearance.ForeColor = color;
                        editFormDataNavigator.Visible = false;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        editFormDataNavigator.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        ////barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        // txtEquipment_GC_Reference.Focus();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        //barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        editFormDataNavigator.Visible = false;
                        //lueSupplier.Focus();
                        //txtEquipment_GC_Reference.Properties.ReadOnly = true;
                        //lueEquipment_Category.Properties.ReadOnly = true;
                        //txtManufacturer_ID.Properties.ReadOnly = true;
                        //lueMake.Properties.ReadOnly = true;
                        //lueModel.Properties.ReadOnly = true;
                        //deDepreciation_Start_Date.Properties.ReadOnly = true;
                        //txtEx_Asset_ID.Properties.ReadOnly = true;
                        //txtSage_Asset_Ref.Properties.ReadOnly = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Viewing";
                        bsiFormMode.ImageIndex = 4;  // View //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        //barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                // equipmentDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;

            bbiSave.Enabled = false;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        #endregion

        #region Form Events

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations))))
                this.Close();
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private void frm_AS_Keeper_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            if (forceFormClose) return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "" && strCaller != "ucStep3Page")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        private void frm_AS_Keeper_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

            //  IdentifyChangedBindingSource();
        }

        #endregion

        #region Data Navigator

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }



        #endregion

        private void editFormDataNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider, editFormDataLayoutControlGroup);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }

        }
    }
}

﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;
using System.Data.SqlClient;

namespace WoodPlan5
{
    public partial class frm_AS_Email_Schedule_Edit : BaseObjects.frmBase
    {
        public frm_AS_Email_Schedule_Edit()
        {
            InitializeComponent();            
        }

        private void frm_AS_Email_Schedule_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 111701;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            
            // connect adapters //
            sp_AS_11157_Email_Schedule_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            //Populate picklist Data
            PopulatePickList();
            deEmailDate.Properties.MinValue = (getCurrentDate()).AddDays(1);
            switch (strFormMode.ToLower())
            {
                case "add":
                    addNewRow(FormMode.add);
                    //lueTaskStatusID.Properties.ReadOnly = true;
                    ceIsCurrentTask.Properties.ReadOnly = true;
                    lueTaskType.Properties.ReadOnly = true;
                    break;
                case "blockadd":
                    this.Close();
                    break;
                case "blockedit":
                    this.Close(); 
                    break;
                case "edit":
                case "view":
                    //txtEquipment_GC_Reference.Properties.ReadOnly = true;
                    try
                    {
                        loadEmailSchedule();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
            }
            if (strFormMode == "view" || dtPassedEmailDate.Date < getCurrentDate().Date)  // Disable all controls //
            {
                formDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
                if (strFormMode == "edit")
                {
                    XtraMessageBox.Show("Editing tasks older than the current date is restricted", "Edit Restriction", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            PostOpen();
        }
        
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public DateTime dtPassedEmailDate;
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        bool bool_FormLoading = true;
        private bool blnIsCurrentTask = false;
        public FormMode formMode;
        public bool emailScheduleChanged = false;

        public enum FormMode { add, edit, view, delete, blockadd, blockedit };

        public enum SentenceCase { Upper , Lower, Title, AsIs }

        #endregion

        #region Compulsory Implementation 
        
        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            
            switch (strFormMode.ToLower())
            {
                case "add":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.Appearance.ForeColor = color;
                        dataNavigator.Visible = false;                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                  //  this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        dataNavigator.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        ////barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                 //   this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        //barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        dataNavigator.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Viewing";
                        bsiFormMode.ImageIndex = 4;  // View //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        //barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                formDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
        
            bbiSave.Enabled = false;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        #endregion

        #region Form_Events

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) 
                this.Close();
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private void frm_AS_Email_Schedule_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        private void frm_AS_Email_Schedule_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

          //  IdentifyChangedBindingSource();
        }

        #region EditorEvents


       

        private void ceIsCurrentTask_Validating(object sender, CancelEventArgs e)
        {
            if (validateCheckEdit((CheckEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }
        
        #endregion
        

        #endregion

        #region Form_Functions

        private void ClearErrors(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is LookUpEdit) 
                    dxErrorProvider.SetError(item2, "");
                if (item2 is TextEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is DateEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is ColorPickEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is SpinEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is ContainerControl)
                    this.ClearErrors(item.Controls);
                if (item2 is DataLayoutControl)
                    this.ClearErrors(item.Controls);

            }
        }

        private DateTime getCurrentDate()
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetDate = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetDate.ChangeConnectionString(strConnectionString);
            GetDate.sp_AS_11138_Get_Server_Date();
            DateTime d = DateTime.Parse(GetDate.sp_AS_11138_Get_Server_Date().ToString());
            return d;
        }
        private string[] splitStrRecords()
        {
            char[] delimiters = new char[] { ',' };
            string[] parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            return parts;
        }

        private void addNewRow(FormMode mode)
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetTaskStatusID = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetTaskStatusID.ChangeConnectionString(strConnectionString);
            int intTaskStatusID = Convert.ToInt32(GetTaskStatusID.sp_AS_11160_Find_Pick_List_ID("Task Status", "Scheduled"));
           
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetTaskTypeID = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetTaskTypeID.ChangeConnectionString(strConnectionString);
            int intTaskTypeID = Convert.ToInt32(GetTaskTypeID.sp_AS_11160_Find_Pick_List_ID("Task Type", "Monthly Asset Allocation Email"));
            try
            {
                DataRow drNewRow = this.dataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_Item.NewRow();
                drNewRow["Mode"] = (mode.ToString()).ToLower();
               
               
                if (mode == FormMode.add)
                {
                    //drNewRow["EmailDate"] = getCurrentDate();
                    drNewRow["TaskStatusID"] = intTaskStatusID;
                    drNewRow["TaskType"] = intTaskTypeID;
                    drNewRow["IsCurrentTask"] = true;
                }
                this.dataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_Item.Rows.Add(drNewRow);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void loadEmailSchedule()
        {
            //load Dep Setting
            formLayoutControlGroup.BeginUpdate();
            sp_AS_11157_Email_Schedule_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_Item, strRecordIDs, strFormMode);
            formLayoutControlGroup.EndUpdate();            
        }

        private void PopulatePickList()
        {
            sp_AS_11000_PL_Extra1TableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Extra1, 49);
            sp_AS_11000_PL_Extra2TableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Extra2, 50);            
        }

        private string SaveChangesExtracted(out bool shouldReturn)
        {
            shouldReturn = false;

            if (dxErrorProvider.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider, formDataLayoutControl);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors +
                "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                shouldReturn = true;
                return "Error";
            }

            return String.Empty;
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
 
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //         
            EndEdit();
            // this.BindingContext[equipmentDataLayoutControl.DataSource, equipmentDataLayoutControl.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            this.Validate();
            this.ValidateChildren();
            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
                return result;

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            EndEdit();

            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_Item);//dep setting Check

            try
            {
                // Insert and Update queries defined in Table Adapter //
                if (emailScheduleChanged)
                    {
                        ////if (onlyOneDefault(formMode))
                        ////{
                        //    if (blnIsCurrentTask)
                        //    {
                        //        DataRowView currentRow = (DataRowView)spAS11157EmailScheduleItemBindingSource.Current;
                        //        if (currentRow != null)
                        //        {
                        //            currentRow["IsCurrentTask"] = true;
                        //        }
                        //    }
                        ////}
                       
                        this.sp_AS_11157_Email_Schedule_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        emailScheduleChanged = false;                   

                    }
            }
            catch (SqlException ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                if (ex.Number == 2627)
                {
                    //Violation of unique key
                    XtraMessageBox.Show("Email schedule task entered are not unique. \n\n Please enter unique records before trying to save - if the problem persists, contact Technical Support.", "Changes Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Changes Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                return "Error";
            }
            catch (Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Changes Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            //// If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)spAS11157EmailScheduleItemBindingSource.Current;
                if (currentRow != null)
                    strNewIDs = this.dataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_Item[0].EmailScheduleID + ";";

                //Switch mode to Edit so than any subsequent changes update this record //
                //this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["Mode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            if (strNewIDs == "")
            {
                string[] t = splitStrRecords();
                strNewIDs = t[0];
            }
            //// Notify any open instances of Sequence Manager they will need to refresh their data on activating //
            if (Application.OpenForms[strCaller] != null && strCaller == "frm_AS_Email_Schedule_Manager")
            {
                (Application.OpenForms[strCaller] as frm_AS_Email_Schedule_Manager).UpdateFormRefreshStatus(1, strNewIDs, "", "");
                (Application.OpenForms[strCaller] as frm_AS_Email_Schedule_Manager).frmActivated();
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
                if (item2 is DevExpress.XtraDataLayout.DataLayoutControl) this.Attach_EditValueChanged_To_Children(item.Controls);
           
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }

        private Boolean SetChangesPendingLabel()
        {
            EndEdit();

            DataSet dsChanges = this.dataSet_AS_DataEntry.GetChanges();
            if (dsChanges != null) 
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;

                if (strFormMode.ToLower() == "add")
                {
                    bbiFormSave.Enabled = true;
                }
                return false;
            }

           
        }

        private void EndEdit()
        {
            spAS11157EmailScheduleItemBindingSource.EndEdit();
          
        }

        private string CheckForPendingSave()
        {
            EndEdit();

            string strMessage = "";
            string strMessagedepSetting = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11157_Email_Schedule_Item, " on the Depreciation Setting Edit Form ");//Dep Setting Check
           

            if (strMessagedepSetting != "")
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (strMessagedepSetting != "") strMessage += strMessagedepSetting;
            }
            return strMessage;
        }

        private string CheckTablePendingSave(DataTable dt, string formArea)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

  
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)" + formArea + "\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)" + formArea + "\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)" + formArea + "\n";
            }
            return strMessage;
        }

        private void CheckChangedTable(DataTable dt)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                IdentifyChangedBindingSource(dt);
            }
        }
        
        private void IdentifyChangedBindingSource(DataTable dt)
        {

            switch (dt.TableName)
            {
                case "sp_AS_11157_Email_Schedule_Item"://dep setting
                    emailScheduleChanged = true;
                    break;   
            }
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            bool_FormLoading = false;
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            
            ConfigureFormAccordingToMode();
            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            bool_FormLoading = false;
        }

        #region ValidateMethod

        private bool validateTextBox(TextEdit txtBox)
        {
            if (bool_FormLoading)
                return true;

            string errorMessage = "Please enter a valid text.";
            
            bool valid = true;
            if (txtBox.Text != "")
            {
                txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
            }
            string originalText = txtBox.Text;
            string parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9 ]", "");


            //check non alphanumeric characters
            if (originalText != parsedText)
            {
                dxErrorProvider.SetError(txtBox, "Please enter a valid text, avoid entering non alphanumeric characters, invalid characters have been removed");
                valid = false;
                return valid;
            }


            //check empty text box
            if (txtBox.Text == "")
            {
                dxErrorProvider.SetError(txtBox, errorMessage);
                valid = false;
                return valid;
            }

            if (valid)
            {
                dxErrorProvider.SetError(txtBox, "");
            }
            return valid;
        }


        private bool validateLookupEdit(LookUpEdit lookupEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;

            string ErrorText = "Please Select Value.";

            if (lookupEdit.Properties.GetDisplayText(lookupEdit.EditValue) == "" ? false : true)
            {
                ErrorText = "Please Select " + lookupEdit.Tag + ".";
            }
            if (lookupEdit.Properties.GetDisplayText(lookupEdit.EditValue) == "")
            {
                dxErrorProvider.SetError(lookupEdit, ErrorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }
            else
            {
                dxErrorProvider.SetError(lookupEdit, "");
                return valid;
            }
        }
        /*
       private bool onlyOneDefault(FormMode mode)
       {
           bool valid = false;

           DataTable dt = null;
           blnIsCurrentTask = false;


            
           //dt = this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search;
            
           //if (dt.Rows.Count = 1)

           if (mode == FormMode.edit)
           {
               string[] IDs = splitStrRecords();
               int i = Convert.ToInt32(IDs[0]);
               sp_AS_11000_Duplicate_SearchTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search, "", i, "depDefault");
               dt = this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search;
           }

           if (mode == FormMode.add)
           {
               sp_AS_11000_Duplicate_SearchTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search, "", 0, "depDefault");
               dt = this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search;
           }

           if (mode == FormMode.add || mode == FormMode.edit)
           {
               if (dt.Rows.Count == 1 && ceIsCurrentTask.Checked != true)//no action required
               {
                   valid = true;
                   return valid;
               }
               else if (dt.Rows.Count == 0 && ceIsCurrentTask.Checked == true)//no action required
               {
                   valid = true;
                   return valid;
               }
               else //set current as default
               {
                   blnIsCurrentTask = true;
                   valid = true;
                   return valid;
               }
           }
          return valid;
       }
       */
        private bool validateCheckEdit(CheckEdit ckEdit)
        {
            bool valid = true;

            if (bool_FormLoading)
                return valid;

            if (ckEdit.Checked != true)
            {
                ckEdit.Checked = false;
            }
           
            if (valid)
            {
                dxErrorProvider.SetError(ckEdit, "");
            }
            return valid;
        }

        private bool validateSpinEdit(SpinEdit spnEdit)
        {
            bool valid = false;

            if (bool_FormLoading)
                return valid;
            spnEdit.Text = Regex.Replace(spnEdit.Text, "[^0-9]", "");
            string originalText = Regex.Replace(spnEdit.Text, "[^0-9]", "");
            string parsedText = Regex.Replace(spnEdit.Text, "[^0-9]", "");


            //check non alphanumeric characters
            if (originalText != parsedText)
            {
                dxErrorProvider.SetError(spnEdit, "Please enter valid digits, avoid entering non alphanumeric characters, invalid characters have been removed.");
                return valid;
            }
            else
            {
                valid = true;
            }

            if (spnEdit.EditValue == null || spnEdit.Properties.GetDisplayText(spnEdit.EditValue) == "")
            {
                dxErrorProvider.SetError(spnEdit, "Please enter a valid " + spnEdit.Tag + " value");
                
                return valid = false;  // Show stop icon as field is invalid //
            }

            if (valid)
            {
                dxErrorProvider.SetError(spnEdit, "");
            }
            return valid;
        }

        #endregion

        #endregion

        #region Data Navigator
                      
        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion
 
    }
}

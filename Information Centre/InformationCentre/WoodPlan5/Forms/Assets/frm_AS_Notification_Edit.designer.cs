﻿namespace WoodPlan5
{
    partial class frm_AS_Notification_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Notification_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11126NotificationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtRequestedType = new DevExpress.XtraEditors.TextEdit();
            this.txtRequestedBy = new DevExpress.XtraEditors.TextEdit();
            this.lueNotificationTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLNotificationTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.luePriorityID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLNotificationPriorityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deDateToRemind = new DevExpress.XtraEditors.DateEdit();
            this.memMessage = new DevExpress.XtraEditors.MemoEdit();
            this.lueStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLNotificationStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForMessage = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciRequestedByID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPriorityID = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciRequestedByType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateToRemind = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.spAS11000PLKeeperTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11044KeepersItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11126_Notification_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11126_Notification_ItemTableAdapter();
            this.sp_AS_11000_PL_Notification_PriorityTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Notification_PriorityTableAdapter();
            this.sp_AS_11000_PL_Notification_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Notification_StatusTableAdapter();
            this.sp_AS_11000_PL_Notification_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Notification_TypeTableAdapter();
            this.sp_AS_11000_PL_Keeper_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_TypeTableAdapter();
            this.sp_AS_11044_Keepers_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keepers_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11126NotificationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRequestedType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRequestedBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNotificationTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLNotificationTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePriorityID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLNotificationPriorityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateToRemind.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateToRemind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memMessage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLNotificationStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRequestedByID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPriorityID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRequestedByType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateToRemind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeepersItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(945, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 602);
            this.barDockControlBottom.Size = new System.Drawing.Size(945, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 576);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(945, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 576);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(945, 576);
            this.editFormLayoutControlGroup.Text = "Root";
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(141, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(263, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11126NotificationItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(153, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // spAS11126NotificationItemBindingSource
            // 
            this.spAS11126NotificationItemBindingSource.DataMember = "sp_AS_11126_Notification_Item";
            this.spAS11126NotificationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtRequestedType);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtRequestedBy);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueNotificationTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEquipmentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.luePriorityID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deDateToRemind);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memMessage);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueStatusID);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11126NotificationItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(569, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(945, 576);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtRequestedType
            // 
            this.txtRequestedType.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11126NotificationItemBindingSource, "KeeperType", true));
            this.txtRequestedType.Location = new System.Drawing.Point(125, 107);
            this.txtRequestedType.MenuManager = this.barManager1;
            this.txtRequestedType.Name = "txtRequestedType";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtRequestedType, true);
            this.txtRequestedType.Size = new System.Drawing.Size(345, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtRequestedType, optionsSpelling1);
            this.txtRequestedType.StyleController = this.editFormDataLayoutControlGroup;
            this.txtRequestedType.TabIndex = 134;
            // 
            // txtRequestedBy
            // 
            this.txtRequestedBy.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11126NotificationItemBindingSource, "FullName", true));
            this.txtRequestedBy.Location = new System.Drawing.Point(587, 107);
            this.txtRequestedBy.MenuManager = this.barManager1;
            this.txtRequestedBy.Name = "txtRequestedBy";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtRequestedBy, true);
            this.txtRequestedBy.Size = new System.Drawing.Size(346, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtRequestedBy, optionsSpelling2);
            this.txtRequestedBy.StyleController = this.editFormDataLayoutControlGroup;
            this.txtRequestedBy.TabIndex = 133;
            // 
            // lueNotificationTypeID
            // 
            this.lueNotificationTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11126NotificationItemBindingSource, "NotificationTypeID", true));
            this.lueNotificationTypeID.Location = new System.Drawing.Point(125, 59);
            this.lueNotificationTypeID.MenuManager = this.barManager1;
            this.lueNotificationTypeID.Name = "lueNotificationTypeID";
            this.lueNotificationTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueNotificationTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Notification Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueNotificationTypeID.Properties.DataSource = this.spAS11000PLNotificationTypeBindingSource;
            this.lueNotificationTypeID.Properties.DisplayMember = "Value";
            this.lueNotificationTypeID.Properties.NullText = "";
            this.lueNotificationTypeID.Properties.NullValuePrompt = "-- Please Select Notification Type --";
            this.lueNotificationTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueNotificationTypeID.Properties.ValueMember = "PickListID";
            this.lueNotificationTypeID.Size = new System.Drawing.Size(345, 20);
            this.lueNotificationTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueNotificationTypeID.TabIndex = 132;
            this.lueNotificationTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lueNotificationTypeID_Validating_1);
            // 
            // spAS11000PLNotificationTypeBindingSource
            // 
            this.spAS11000PLNotificationTypeBindingSource.DataMember = "sp_AS_11000_PL_Notification_Type";
            this.spAS11000PLNotificationTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11126NotificationItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(125, 35);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(808, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling3);
            this.txtEquipmentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEquipmentReference.TabIndex = 124;
            // 
            // luePriorityID
            // 
            this.luePriorityID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11126NotificationItemBindingSource, "PriorityID", true));
            this.luePriorityID.Location = new System.Drawing.Point(587, 59);
            this.luePriorityID.MenuManager = this.barManager1;
            this.luePriorityID.Name = "luePriorityID";
            this.luePriorityID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePriorityID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Priority", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.luePriorityID.Properties.DataSource = this.spAS11000PLNotificationPriorityBindingSource;
            this.luePriorityID.Properties.DisplayMember = "Value";
            this.luePriorityID.Properties.NullText = "";
            this.luePriorityID.Properties.NullValuePrompt = "-- Please Select Priority--";
            this.luePriorityID.Properties.NullValuePromptShowForEmptyValue = true;
            this.luePriorityID.Properties.ValueMember = "PickListID";
            this.luePriorityID.Size = new System.Drawing.Size(346, 20);
            this.luePriorityID.StyleController = this.editFormDataLayoutControlGroup;
            this.luePriorityID.TabIndex = 125;
            this.luePriorityID.Tag = "Priority";
            this.luePriorityID.Validating += new System.ComponentModel.CancelEventHandler(this.luePriorityID_Validating);
            // 
            // spAS11000PLNotificationPriorityBindingSource
            // 
            this.spAS11000PLNotificationPriorityBindingSource.DataMember = "sp_AS_11000_PL_Notification_Priority";
            this.spAS11000PLNotificationPriorityBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deDateToRemind
            // 
            this.deDateToRemind.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11126NotificationItemBindingSource, "DateToRemind", true));
            this.deDateToRemind.EditValue = null;
            this.deDateToRemind.Location = new System.Drawing.Point(587, 83);
            this.deDateToRemind.MenuManager = this.barManager1;
            this.deDateToRemind.Name = "deDateToRemind";
            this.deDateToRemind.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateToRemind.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateToRemind.Size = new System.Drawing.Size(346, 20);
            this.deDateToRemind.StyleController = this.editFormDataLayoutControlGroup;
            this.deDateToRemind.TabIndex = 126;
            this.deDateToRemind.Tag = "Date To Remind";
            this.deDateToRemind.Validating += new System.ComponentModel.CancelEventHandler(this.deDateToRemind_Validating);
            // 
            // memMessage
            // 
            this.memMessage.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11126NotificationItemBindingSource, "Message", true));
            this.memMessage.Location = new System.Drawing.Point(24, 164);
            this.memMessage.MenuManager = this.barManager1;
            this.memMessage.Name = "memMessage";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memMessage, true);
            this.memMessage.Size = new System.Drawing.Size(897, 388);
            this.scSpellChecker.SetSpellCheckerOptions(this.memMessage, optionsSpelling4);
            this.memMessage.StyleController = this.editFormDataLayoutControlGroup;
            this.memMessage.TabIndex = 130;
            this.memMessage.Tag = "Message";
            this.memMessage.UseOptimizedRendering = true;
            this.memMessage.Validating += new System.ComponentModel.CancelEventHandler(this.memMessage_Validating);
            // 
            // lueStatusID
            // 
            this.lueStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11126NotificationItemBindingSource, "StatusID", true));
            this.lueStatusID.Location = new System.Drawing.Point(125, 83);
            this.lueStatusID.MenuManager = this.barManager1;
            this.lueStatusID.Name = "lueStatusID";
            this.lueStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueStatusID.Properties.DataSource = this.spAS11000PLNotificationStatusBindingSource;
            this.lueStatusID.Properties.DisplayMember = "Value";
            this.lueStatusID.Properties.NullText = "";
            this.lueStatusID.Properties.NullValuePrompt = "-- Please Select Status --";
            this.lueStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueStatusID.Properties.ValueMember = "PickListID";
            this.lueStatusID.Size = new System.Drawing.Size(345, 20);
            this.lueStatusID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueStatusID.TabIndex = 131;
            this.lueStatusID.Tag = "Status";
            this.lueStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.lueStatusID_Validating);
            // 
            // spAS11000PLNotificationStatusBindingSource
            // 
            this.spAS11000PLNotificationStatusBindingSource.DataMember = "sp_AS_11000_PL_Notification_Status";
            this.spAS11000PLNotificationStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.ItemForEquipmentReference,
            this.ItemForStatusID,
            this.layoutControlItem3,
            this.lciRequestedByID,
            this.ItemForPriorityID,
            this.lciRequestedByType,
            this.ItemForDateToRemind});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(925, 533);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 96);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(925, 437);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Message";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForMessage});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(901, 392);
            this.layoutControlGroup2.Text = "Message";
            // 
            // ItemForMessage
            // 
            this.ItemForMessage.Control = this.memMessage;
            this.ItemForMessage.CustomizationFormText = "Message";
            this.ItemForMessage.Location = new System.Drawing.Point(0, 0);
            this.ItemForMessage.Name = "ItemForMessage";
            this.ItemForMessage.Size = new System.Drawing.Size(901, 392);
            this.ItemForMessage.Text = "Message";
            this.ItemForMessage.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForMessage.TextToControlDistance = 0;
            this.ItemForMessage.TextVisible = false;
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.txtEquipmentReference;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference :";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(925, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference :";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.lueStatusID;
            this.ItemForStatusID.CustomizationFormText = "Status :";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 48);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(462, 24);
            this.ItemForStatusID.Text = "Status :";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lueNotificationTypeID;
            this.layoutControlItem3.CustomizationFormText = "Notification Type :";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(462, 24);
            this.layoutControlItem3.Text = "Notification Type :";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(110, 13);
            // 
            // lciRequestedByID
            // 
            this.lciRequestedByID.Control = this.txtRequestedBy;
            this.lciRequestedByID.CustomizationFormText = "Requested By :";
            this.lciRequestedByID.Location = new System.Drawing.Point(462, 72);
            this.lciRequestedByID.Name = "lciRequestedByID";
            this.lciRequestedByID.Size = new System.Drawing.Size(463, 24);
            this.lciRequestedByID.Text = "Requested By :";
            this.lciRequestedByID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForPriorityID
            // 
            this.ItemForPriorityID.Control = this.luePriorityID;
            this.ItemForPriorityID.CustomizationFormText = "Priority : ";
            this.ItemForPriorityID.Location = new System.Drawing.Point(462, 24);
            this.ItemForPriorityID.Name = "ItemForPriorityID";
            this.ItemForPriorityID.Size = new System.Drawing.Size(463, 24);
            this.ItemForPriorityID.Text = "Priority : ";
            this.ItemForPriorityID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // lciRequestedByType
            // 
            this.lciRequestedByType.Control = this.txtRequestedType;
            this.lciRequestedByType.CustomizationFormText = "Requested By Type :";
            this.lciRequestedByType.Location = new System.Drawing.Point(0, 72);
            this.lciRequestedByType.Name = "lciRequestedByType";
            this.lciRequestedByType.Size = new System.Drawing.Size(462, 24);
            this.lciRequestedByType.Text = "Requested By Type :";
            this.lciRequestedByType.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForDateToRemind
            // 
            this.ItemForDateToRemind.Control = this.deDateToRemind;
            this.ItemForDateToRemind.CustomizationFormText = "Date To Remind :";
            this.ItemForDateToRemind.Location = new System.Drawing.Point(462, 48);
            this.ItemForDateToRemind.Name = "ItemForDateToRemind";
            this.ItemForDateToRemind.Size = new System.Drawing.Size(463, 24);
            this.ItemForDateToRemind.Text = "Date To Remind :";
            this.ItemForDateToRemind.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(404, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(521, 23);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(141, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // spAS11000PLKeeperTypeBindingSource
            // 
            this.spAS11000PLKeeperTypeBindingSource.DataMember = "sp_AS_11000_PL_Keeper_Type";
            this.spAS11000PLKeeperTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11044KeepersItemBindingSource
            // 
            this.spAS11044KeepersItemBindingSource.DataMember = "sp_AS_11044_Keepers_Item";
            this.spAS11044KeepersItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11126_Notification_ItemTableAdapter
            // 
            this.sp_AS_11126_Notification_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Notification_PriorityTableAdapter
            // 
            this.sp_AS_11000_PL_Notification_PriorityTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Notification_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Notification_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Notification_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Notification_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Keeper_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Keeper_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keepers_ItemTableAdapter
            // 
            this.sp_AS_11044_Keepers_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Notification_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(945, 632);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Notification_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Notification";
            this.Activated += new System.EventHandler(this.frm_AS_Notification_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Notification_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Notification_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11126NotificationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRequestedType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRequestedBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueNotificationTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLNotificationTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePriorityID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLNotificationPriorityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateToRemind.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateToRemind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memMessage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLNotificationStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRequestedByID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPriorityID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRequestedByType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateToRemind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeepersItemBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource spAS11126NotificationItemBindingSource;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private DevExpress.XtraEditors.LookUpEdit luePriorityID;
        private DevExpress.XtraEditors.DateEdit deDateToRemind;
        private DevExpress.XtraEditors.MemoEdit memMessage;
        private DevExpress.XtraEditors.LookUpEdit lueStatusID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPriorityID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateToRemind;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMessage;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11126_Notification_ItemTableAdapter sp_AS_11126_Notification_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLNotificationPriorityBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Notification_PriorityTableAdapter sp_AS_11000_PL_Notification_PriorityTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLNotificationStatusBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Notification_StatusTableAdapter sp_AS_11000_PL_Notification_StatusTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueNotificationTypeID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource spAS11000PLNotificationTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Notification_TypeTableAdapter sp_AS_11000_PL_Notification_TypeTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLKeeperTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_TypeTableAdapter sp_AS_11000_PL_Keeper_TypeTableAdapter;
        private System.Windows.Forms.BindingSource spAS11044KeepersItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keepers_ItemTableAdapter sp_AS_11044_Keepers_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit txtRequestedBy;
        private DevExpress.XtraLayout.LayoutControlItem lciRequestedByID;
        private DevExpress.XtraEditors.TextEdit txtRequestedType;
        private DevExpress.XtraLayout.LayoutControlItem lciRequestedByType;


    }
}

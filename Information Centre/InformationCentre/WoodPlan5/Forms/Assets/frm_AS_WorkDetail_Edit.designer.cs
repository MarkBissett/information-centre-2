﻿namespace WoodPlan5
{
    partial class frm_AS_WorkDetail_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_WorkDetail_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11078WorkDetailItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.lueWorkTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLWorkTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deDateRaised = new DevExpress.XtraEditors.DateEdit();
            this.lueSupplierID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11020SupplierListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLWorkStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.meDescription = new DevExpress.XtraEditors.MemoEdit();
            this.lueTransactionID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11041TransactionItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnCurrentMileage = new DevExpress.XtraEditors.SpinEdit();
            this.deWorkCompletionDate = new DevExpress.XtraEditors.DateEdit();
            this.meNotes = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransactionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkCompletionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateRaised = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSupplierID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCurrentMileage = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11078_Work_Detail_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter();
            this.sp_AS_11000_PL_Work_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Work_TypeTableAdapter();
            this.sp_AS_11000_PL_Work_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Work_StatusTableAdapter();
            this.sp_AS_11020_Supplier_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter();
            this.sp_AS_11041_Transaction_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter();
            this.spAS11163ServiceScheduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11163_Service_ScheduleTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11163_Service_ScheduleTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWorkTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLWorkTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateRaised.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateRaised.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLWorkStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTransactionID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnCurrentMileage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deWorkCompletionDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deWorkCompletionDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransactionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkCompletionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRaised)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrentMileage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11163ServiceScheduleBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(951, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 320);
            this.barDockControlBottom.Size = new System.Drawing.Size(951, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 294);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(951, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 294);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(951, 294);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(121, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(352, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11078WorkDetailItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(133, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(348, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.editFormDataNavigator.TextStringFormat = "Work Order Record {0} of {1}";
            // 
            // spAS11078WorkDetailItemBindingSource
            // 
            this.spAS11078WorkDetailItemBindingSource.DataMember = "sp_AS_11078_Work_Detail_Item";
            this.spAS11078WorkDetailItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEquipmentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueWorkTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deDateRaised);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueSupplierID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueStatusID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.meDescription);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueTransactionID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnCurrentMileage);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deWorkCompletionDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.meNotes);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11078WorkDetailItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2234, 251, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(951, 294);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11078WorkDetailItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(129, 35);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(810, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling1);
            this.txtEquipmentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEquipmentReference.TabIndex = 123;
            // 
            // lueWorkTypeID
            // 
            this.lueWorkTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11078WorkDetailItemBindingSource, "WorkTypeID", true));
            this.lueWorkTypeID.Location = new System.Drawing.Point(129, 59);
            this.lueWorkTypeID.MenuManager = this.barManager1;
            this.lueWorkTypeID.Name = "lueWorkTypeID";
            this.lueWorkTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueWorkTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Work Order Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueWorkTypeID.Properties.DataSource = this.spAS11000PLWorkTypeBindingSource;
            this.lueWorkTypeID.Properties.DisplayMember = "Value";
            this.lueWorkTypeID.Properties.NullText = "";
            this.lueWorkTypeID.Properties.NullValuePrompt = "-- Please Select Work Order Type --";
            this.lueWorkTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueWorkTypeID.Properties.ValueMember = "PickListID";
            this.lueWorkTypeID.Size = new System.Drawing.Size(344, 20);
            this.lueWorkTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueWorkTypeID.TabIndex = 124;
            this.lueWorkTypeID.Tag = "Work Order Type";
            this.lueWorkTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLWorkTypeBindingSource
            // 
            this.spAS11000PLWorkTypeBindingSource.DataMember = "sp_AS_11000_PL_Work_Type";
            this.spAS11000PLWorkTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deDateRaised
            // 
            this.deDateRaised.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11078WorkDetailItemBindingSource, "DateRaised", true));
            this.deDateRaised.EditValue = null;
            this.deDateRaised.Location = new System.Drawing.Point(129, 133);
            this.deDateRaised.MenuManager = this.barManager1;
            this.deDateRaised.Name = "deDateRaised";
            this.deDateRaised.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateRaised.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateRaised.Size = new System.Drawing.Size(344, 20);
            this.deDateRaised.StyleController = this.editFormDataLayoutControlGroup;
            this.deDateRaised.TabIndex = 125;
            this.deDateRaised.Tag = "Work Start Date";
            this.deDateRaised.EditValueChanged += new System.EventHandler(this.deDateRaised_EditValueChanged);
            this.deDateRaised.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // lueSupplierID
            // 
            this.lueSupplierID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11078WorkDetailItemBindingSource, "SupplierID", true));
            this.lueSupplierID.Location = new System.Drawing.Point(129, 83);
            this.lueSupplierID.MenuManager = this.barManager1;
            this.lueSupplierID.Name = "lueSupplierID";
            this.lueSupplierID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSupplierID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierType", "Supplier Type", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierReference", "Supplier Reference", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSupplierID.Properties.DataSource = this.spAS11020SupplierListBindingSource;
            this.lueSupplierID.Properties.DisplayMember = "SupplierReference";
            this.lueSupplierID.Properties.NullText = "";
            this.lueSupplierID.Properties.NullValuePrompt = "-- Please Select  Supplier --";
            this.lueSupplierID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSupplierID.Properties.PopupWidth = 275;
            this.lueSupplierID.Properties.ValueMember = "SupplierID";
            this.lueSupplierID.Size = new System.Drawing.Size(810, 20);
            this.lueSupplierID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueSupplierID.TabIndex = 126;
            this.lueSupplierID.Tag = "Supplier";
            this.lueSupplierID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11020SupplierListBindingSource
            // 
            this.spAS11020SupplierListBindingSource.DataMember = "sp_AS_11020_Supplier_List";
            this.spAS11020SupplierListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueStatusID
            // 
            this.lueStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11078WorkDetailItemBindingSource, "StatusID", true));
            this.lueStatusID.Location = new System.Drawing.Point(594, 59);
            this.lueStatusID.MenuManager = this.barManager1;
            this.lueStatusID.Name = "lueStatusID";
            this.lueStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Work Order Status", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueStatusID.Properties.DataSource = this.spAS11000PLWorkStatusBindingSource;
            this.lueStatusID.Properties.DisplayMember = "Value";
            this.lueStatusID.Properties.NullText = "";
            this.lueStatusID.Properties.NullValuePrompt = "-- Please Select Work Order Status --";
            this.lueStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueStatusID.Properties.ValueMember = "PickListID";
            this.lueStatusID.Size = new System.Drawing.Size(345, 20);
            this.lueStatusID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueStatusID.TabIndex = 127;
            this.lueStatusID.Tag = "Work Order Status";
            this.lueStatusID.EditValueChanged += new System.EventHandler(this.lueStatusID_EditValueChanged);
            this.lueStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLWorkStatusBindingSource
            // 
            this.spAS11000PLWorkStatusBindingSource.DataMember = "sp_AS_11000_PL_Work_Status";
            this.spAS11000PLWorkStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // meDescription
            // 
            this.meDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11078WorkDetailItemBindingSource, "Description", true));
            this.meDescription.Location = new System.Drawing.Point(24, 193);
            this.meDescription.MenuManager = this.barManager1;
            this.meDescription.Name = "meDescription";
            this.scSpellChecker.SetShowSpellCheckMenu(this.meDescription, true);
            this.meDescription.Size = new System.Drawing.Size(903, 77);
            this.scSpellChecker.SetSpellCheckerOptions(this.meDescription, optionsSpelling2);
            this.meDescription.StyleController = this.editFormDataLayoutControlGroup;
            this.meDescription.TabIndex = 128;
            this.meDescription.Tag = "Description";
            // 
            // lueTransactionID
            // 
            this.lueTransactionID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11078WorkDetailItemBindingSource, "TransactionID", true));
            this.lueTransactionID.Location = new System.Drawing.Point(594, 107);
            this.lueTransactionID.MenuManager = this.barManager1;
            this.lueTransactionID.Name = "lueTransactionID";
            this.lueTransactionID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueTransactionID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Add Transaction", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueTransactionID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Refresh Transactions", "refresh", null, true)});
            this.lueTransactionID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EquipmentReference", "Equipment Reference", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TransactionType", "Transaction Type", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TransactionDate", "Transaction Date", 50, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TransactionOrderNumber", "Transaction Order Number", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PurchaseInvoice", "Purchase Invoice", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NetTransactionPrice", "Net Transaction Price", 50, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("VAT", "VAT", 50, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far)});
            this.lueTransactionID.Properties.DataSource = this.spAS11041TransactionItemBindingSource;
            this.lueTransactionID.Properties.DisplayMember = "TransactionOrderNumber";
            this.lueTransactionID.Properties.NullText = "";
            this.lueTransactionID.Properties.NullValuePrompt = "-- Please Select Related Transaction --";
            this.lueTransactionID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueTransactionID.Properties.PopupWidth = 500;
            this.lueTransactionID.Properties.ValueMember = "TransactionID";
            this.lueTransactionID.Size = new System.Drawing.Size(345, 22);
            this.lueTransactionID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueTransactionID.TabIndex = 129;
            this.lueTransactionID.Tag = "Purchase Order";
            this.lueTransactionID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lueTransactionID_ButtonPressed);
            this.lueTransactionID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11041TransactionItemBindingSource
            // 
            this.spAS11041TransactionItemBindingSource.DataMember = "sp_AS_11041_Transaction_Item";
            this.spAS11041TransactionItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnCurrentMileage
            // 
            this.spnCurrentMileage.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11078WorkDetailItemBindingSource, "CurrentMileage", true));
            this.spnCurrentMileage.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnCurrentMileage.Location = new System.Drawing.Point(129, 107);
            this.spnCurrentMileage.MenuManager = this.barManager1;
            this.spnCurrentMileage.Name = "spnCurrentMileage";
            this.spnCurrentMileage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnCurrentMileage.Properties.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spnCurrentMileage.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnCurrentMileage.Size = new System.Drawing.Size(344, 20);
            this.spnCurrentMileage.StyleController = this.editFormDataLayoutControlGroup;
            this.spnCurrentMileage.TabIndex = 130;
            this.spnCurrentMileage.Tag = "Current Mileage";
            this.spnCurrentMileage.Validating += new System.ComponentModel.CancelEventHandler(this.spnCurrentMileage_Validating);
            // 
            // deWorkCompletionDate
            // 
            this.deWorkCompletionDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11078WorkDetailItemBindingSource, "WorkCompletionDate", true));
            this.deWorkCompletionDate.EditValue = null;
            this.deWorkCompletionDate.Location = new System.Drawing.Point(594, 133);
            this.deWorkCompletionDate.MenuManager = this.barManager1;
            this.deWorkCompletionDate.Name = "deWorkCompletionDate";
            this.deWorkCompletionDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deWorkCompletionDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deWorkCompletionDate.Size = new System.Drawing.Size(345, 20);
            this.deWorkCompletionDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deWorkCompletionDate.TabIndex = 131;
            this.deWorkCompletionDate.Tag = "Work Completion Date";
            this.deWorkCompletionDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // meNotes
            // 
            this.meNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11078WorkDetailItemBindingSource, "Notes", true));
            this.meNotes.EditValue = "Notes";
            this.meNotes.Location = new System.Drawing.Point(24, 193);
            this.meNotes.MenuManager = this.barManager1;
            this.meNotes.Name = "meNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.meNotes, true);
            this.meNotes.Size = new System.Drawing.Size(903, 77);
            this.scSpellChecker.SetSpellCheckerOptions(this.meNotes, optionsSpelling3);
            this.meNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.meNotes.TabIndex = 133;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEquipmentReference,
            this.ItemForWorkTypeID,
            this.ItemForTransactionID,
            this.ItemForWorkCompletionDate,
            this.tabbedControlGroup1,
            this.ItemForDateRaised,
            this.ItemForStatusID,
            this.ItemForSupplierID,
            this.ItemForCurrentMileage});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(931, 251);
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.txtEquipmentReference;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference :";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(931, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference :";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(114, 13);
            // 
            // ItemForWorkTypeID
            // 
            this.ItemForWorkTypeID.Control = this.lueWorkTypeID;
            this.ItemForWorkTypeID.CustomizationFormText = "Work Order Type :";
            this.ItemForWorkTypeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForWorkTypeID.Name = "ItemForWorkTypeID";
            this.ItemForWorkTypeID.Size = new System.Drawing.Size(465, 24);
            this.ItemForWorkTypeID.Text = "Work Order Type :";
            this.ItemForWorkTypeID.TextSize = new System.Drawing.Size(114, 13);
            // 
            // ItemForTransactionID
            // 
            this.ItemForTransactionID.Control = this.lueTransactionID;
            this.ItemForTransactionID.CustomizationFormText = "Purchase Order :";
            this.ItemForTransactionID.Location = new System.Drawing.Point(465, 72);
            this.ItemForTransactionID.Name = "ItemForTransactionID";
            this.ItemForTransactionID.Size = new System.Drawing.Size(466, 26);
            this.ItemForTransactionID.Text = "Purchase Order :";
            this.ItemForTransactionID.TextSize = new System.Drawing.Size(114, 13);
            // 
            // ItemForWorkCompletionDate
            // 
            this.ItemForWorkCompletionDate.Control = this.deWorkCompletionDate;
            this.ItemForWorkCompletionDate.CustomizationFormText = "Work Completion Date :";
            this.ItemForWorkCompletionDate.Location = new System.Drawing.Point(465, 98);
            this.ItemForWorkCompletionDate.Name = "ItemForWorkCompletionDate";
            this.ItemForWorkCompletionDate.Size = new System.Drawing.Size(466, 24);
            this.ItemForWorkCompletionDate.Text = "Work Completion Date :";
            this.ItemForWorkCompletionDate.TextSize = new System.Drawing.Size(114, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 122);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(931, 129);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup2});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup2.CaptionImage")));
            this.layoutControlGroup2.CustomizationFormText = "Notes";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(907, 81);
            this.layoutControlGroup2.Text = "Notes";
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.meNotes;
            this.ItemForNotes.CustomizationFormText = "Notes";
            this.ItemForNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(907, 81);
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNotes.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Description";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDescription});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(907, 81);
            this.layoutControlGroup3.Text = "Description";
            // 
            // ItemForDescription
            // 
            this.ItemForDescription.Control = this.meDescription;
            this.ItemForDescription.CustomizationFormText = "Description";
            this.ItemForDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForDescription.Name = "ItemForDescription";
            this.ItemForDescription.Size = new System.Drawing.Size(907, 81);
            this.ItemForDescription.Text = "Description";
            this.ItemForDescription.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForDescription.TextVisible = false;
            // 
            // ItemForDateRaised
            // 
            this.ItemForDateRaised.Control = this.deDateRaised;
            this.ItemForDateRaised.CustomizationFormText = "Work Start Date :";
            this.ItemForDateRaised.Location = new System.Drawing.Point(0, 98);
            this.ItemForDateRaised.Name = "ItemForDateRaised";
            this.ItemForDateRaised.Size = new System.Drawing.Size(465, 24);
            this.ItemForDateRaised.Text = "Work Start Date :";
            this.ItemForDateRaised.TextSize = new System.Drawing.Size(114, 13);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.lueStatusID;
            this.ItemForStatusID.CustomizationFormText = "Work Order Status :";
            this.ItemForStatusID.Location = new System.Drawing.Point(465, 24);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(466, 24);
            this.ItemForStatusID.Text = "Work Order Status :";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(114, 13);
            // 
            // ItemForSupplierID
            // 
            this.ItemForSupplierID.Control = this.lueSupplierID;
            this.ItemForSupplierID.CustomizationFormText = "Supplier Reference :";
            this.ItemForSupplierID.Location = new System.Drawing.Point(0, 48);
            this.ItemForSupplierID.Name = "ItemForSupplierID";
            this.ItemForSupplierID.Size = new System.Drawing.Size(931, 24);
            this.ItemForSupplierID.Text = "Supplier Reference :";
            this.ItemForSupplierID.TextSize = new System.Drawing.Size(114, 13);
            // 
            // ItemForCurrentMileage
            // 
            this.ItemForCurrentMileage.Control = this.spnCurrentMileage;
            this.ItemForCurrentMileage.CustomizationFormText = "Current Mileage :";
            this.ItemForCurrentMileage.Location = new System.Drawing.Point(0, 72);
            this.ItemForCurrentMileage.Name = "ItemForCurrentMileage";
            this.ItemForCurrentMileage.Size = new System.Drawing.Size(465, 26);
            this.ItemForCurrentMileage.Text = "Current Mileage :";
            this.ItemForCurrentMileage.TextSize = new System.Drawing.Size(114, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(121, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(473, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(458, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11078_Work_Detail_ItemTableAdapter
            // 
            this.sp_AS_11078_Work_Detail_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Work_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Work_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Work_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Work_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11020_Supplier_ListTableAdapter
            // 
            this.sp_AS_11020_Supplier_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11041_Transaction_ItemTableAdapter
            // 
            this.sp_AS_11041_Transaction_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11163ServiceScheduleBindingSource
            // 
            this.spAS11163ServiceScheduleBindingSource.DataMember = "sp_AS_11163_Service_Schedule";
            this.spAS11163ServiceScheduleBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11163_Service_ScheduleTableAdapter
            // 
            this.sp_AS_11163_Service_ScheduleTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_WorkDetail_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(951, 350);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_WorkDetail_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Work Order Details";
            this.Activated += new System.EventHandler(this.frm_AS_WorkDetail_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_WorkDetail_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_WorkDetail_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWorkTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLWorkTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateRaised.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateRaised.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLWorkStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTransactionID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnCurrentMileage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deWorkCompletionDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deWorkCompletionDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransactionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkCompletionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRaised)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrentMileage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11163ServiceScheduleBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource spAS11078WorkDetailItemBindingSource;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private DevExpress.XtraEditors.LookUpEdit lueWorkTypeID;
        private DevExpress.XtraEditors.DateEdit deDateRaised;
        private DevExpress.XtraEditors.LookUpEdit lueSupplierID;
        private DevExpress.XtraEditors.LookUpEdit lueStatusID;
        private DevExpress.XtraEditors.MemoEdit meDescription;
        private DevExpress.XtraEditors.LookUpEdit lueTransactionID;
        private DevExpress.XtraEditors.SpinEdit spnCurrentMileage;
        private DevExpress.XtraEditors.DateEdit deWorkCompletionDate;
        private DevExpress.XtraEditors.MemoEdit meNotes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransactionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkCompletionDate;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateRaised;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSupplierID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCurrentMileage;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter sp_AS_11078_Work_Detail_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLWorkTypeBindingSource;
        private System.Windows.Forms.BindingSource spAS11020SupplierListBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLWorkStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11041TransactionItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Work_TypeTableAdapter sp_AS_11000_PL_Work_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Work_StatusTableAdapter sp_AS_11000_PL_Work_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter sp_AS_11020_Supplier_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter sp_AS_11041_Transaction_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11163ServiceScheduleBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11163_Service_ScheduleTableAdapter sp_AS_11163_Service_ScheduleTableAdapter;


    }
}

﻿namespace WoodPlan5
{
    partial class frm_AS_Supplier_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Supplier_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.managerGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11020SupplierListBindingSource = new System.Windows.Forms.BindingSource();
            this.managerGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSupplierID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccountNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCounty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMainContact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceVehicleSupplierPref = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colPlantSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cePlantSupplierPref = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colGadgetSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceGadgetSupplierPref = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colHardwareSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceHardwareSupplierPref = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colSoftwareSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceSoftwareSupplierPref = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colOfficeSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceOfficeSupplierPref = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colRentalSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceRentalSupplierPref = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NotesMemoExEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.webRepositoryItemHyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.lueSupplierType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp_AS_11020_Supplier_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceVehicleSupplierPref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cePlantSupplierPref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGadgetSupplierPref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceHardwareSupplierPref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSoftwareSupplierPref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOfficeSupplierPref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceRentalSupplierPref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoExEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.webRepositoryItemHyperLinkEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.Manager = null;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 748);
            this.barDockControlBottom.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 748);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1372, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 748);
            // 
            // bbiSave
            // 
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.LargeGlyph")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.Manager = null;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiCancel});
            this.barManager1.MaxItemId = 31;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("save_16x16.png", "images/save/save_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/save/save_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "save_16x16.png");
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1675, 521, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1372, 748);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1372, 748);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // managerGridControl
            // 
            this.managerGridControl.DataSource = this.spAS11020SupplierListBindingSource;
            this.managerGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.managerGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.Append.ImageIndex = 0;
            this.managerGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.Edit.ImageIndex = 1;
            this.managerGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.EndEdit.ImageIndex = 4;
            this.managerGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.managerGridControl.EmbeddedNavigator.Buttons.Remove.ImageIndex = 2;
            this.managerGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "";
            this.managerGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.managerGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "", "delete")});
            this.managerGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.supplierGridControl_EmbeddedNavigator_ButtonClick);
            this.managerGridControl.Location = new System.Drawing.Point(0, 0);
            this.managerGridControl.MainView = this.managerGridView;
            this.managerGridControl.Name = "managerGridControl";
            this.managerGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.NotesMemoExEdit,
            this.webRepositoryItemHyperLinkEdit,
            this.lueSupplierType,
            this.TextEdit,
            this.ceVehicleSupplierPref,
            this.cePlantSupplierPref,
            this.ceGadgetSupplierPref,
            this.ceHardwareSupplierPref,
            this.ceSoftwareSupplierPref,
            this.ceOfficeSupplierPref,
            this.ceRentalSupplierPref});
            this.managerGridControl.Size = new System.Drawing.Size(1372, 748);
            this.managerGridControl.TabIndex = 5;
            this.managerGridControl.UseEmbeddedNavigator = true;
            this.managerGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.managerGridView});
            this.managerGridControl.Click += new System.EventHandler(this.managerGridControl_Click);
            // 
            // spAS11020SupplierListBindingSource
            // 
            this.spAS11020SupplierListBindingSource.DataMember = "sp_AS_11020_Supplier_List";
            this.spAS11020SupplierListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // managerGridView
            // 
            this.managerGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSupplierID,
            this.colSupplierTypeID,
            this.colSupplierType,
            this.colAccountNumber,
            this.colSupplierReference,
            this.colName,
            this.colAddress1,
            this.colAddress2,
            this.colAddress3,
            this.colCity,
            this.colPostcode,
            this.colCounty,
            this.colCountry,
            this.colMainContact,
            this.colPhone,
            this.colMobile,
            this.colFax,
            this.colEmailAddress,
            this.colNotes,
            this.colWebsite,
            this.colVehicleSupplier,
            this.colPlantSupplier,
            this.colGadgetSupplier,
            this.colHardwareSupplier,
            this.colSoftwareSupplier,
            this.colOfficeSupplier,
            this.colRentalSupplier,
            this.colRecordID,
            this.colMode});
            this.managerGridView.GridControl = this.managerGridControl;
            this.managerGridView.Name = "managerGridView";
            this.managerGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.managerGridView.OptionsFind.AlwaysVisible = true;
            this.managerGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.managerGridView.OptionsLayout.StoreAppearance = true;
            this.managerGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.managerGridView.OptionsSelection.MultiSelect = true;
            this.managerGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.managerGridView.OptionsView.ColumnAutoWidth = false;
            this.managerGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.managerGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.supplierGridView_PopupMenuShowing);
            this.managerGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.supplierGridView_SelectionChanged);
            this.managerGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.supplierGridView_CustomDrawEmptyForeground);
            this.managerGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.managerGridView_CustomFilterDialog);
            this.managerGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.supplierGridView_MouseUp);
            this.managerGridView.DoubleClick += new System.EventHandler(this.supplierGridView_DoubleClick);
            // 
            // colSupplierID
            // 
            this.colSupplierID.FieldName = "SupplierID";
            this.colSupplierID.Name = "colSupplierID";
            this.colSupplierID.OptionsColumn.AllowEdit = false;
            this.colSupplierID.OptionsColumn.AllowFocus = false;
            this.colSupplierID.OptionsColumn.ReadOnly = true;
            this.colSupplierID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSupplierID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSupplierID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSupplierTypeID
            // 
            this.colSupplierTypeID.FieldName = "SupplierTypeID";
            this.colSupplierTypeID.Name = "colSupplierTypeID";
            this.colSupplierTypeID.OptionsColumn.AllowEdit = false;
            this.colSupplierTypeID.OptionsColumn.AllowFocus = false;
            this.colSupplierTypeID.OptionsColumn.ReadOnly = true;
            this.colSupplierTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSupplierTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSupplierTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierTypeID.Width = 100;
            // 
            // colSupplierType
            // 
            this.colSupplierType.FieldName = "SupplierType";
            this.colSupplierType.Name = "colSupplierType";
            this.colSupplierType.OptionsColumn.AllowEdit = false;
            this.colSupplierType.OptionsColumn.AllowFocus = false;
            this.colSupplierType.OptionsColumn.ReadOnly = true;
            this.colSupplierType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierType.Visible = true;
            this.colSupplierType.VisibleIndex = 1;
            this.colSupplierType.Width = 86;
            // 
            // colAccountNumber
            // 
            this.colAccountNumber.FieldName = "AccountNumber";
            this.colAccountNumber.Name = "colAccountNumber";
            this.colAccountNumber.OptionsColumn.AllowEdit = false;
            this.colAccountNumber.OptionsColumn.AllowFocus = false;
            this.colAccountNumber.OptionsColumn.ReadOnly = true;
            this.colAccountNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAccountNumber.Visible = true;
            this.colAccountNumber.VisibleIndex = 2;
            this.colAccountNumber.Width = 100;
            // 
            // colSupplierReference
            // 
            this.colSupplierReference.FieldName = "SupplierReference";
            this.colSupplierReference.Name = "colSupplierReference";
            this.colSupplierReference.OptionsColumn.AllowEdit = false;
            this.colSupplierReference.OptionsColumn.AllowFocus = false;
            this.colSupplierReference.OptionsColumn.ReadOnly = true;
            this.colSupplierReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference.Visible = true;
            this.colSupplierReference.VisibleIndex = 3;
            this.colSupplierReference.Width = 112;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowFocus = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colAddress1
            // 
            this.colAddress1.FieldName = "Address1";
            this.colAddress1.Name = "colAddress1";
            this.colAddress1.OptionsColumn.AllowEdit = false;
            this.colAddress1.OptionsColumn.AllowFocus = false;
            this.colAddress1.OptionsColumn.ReadOnly = true;
            this.colAddress1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAddress1.Visible = true;
            this.colAddress1.VisibleIndex = 4;
            this.colAddress1.Width = 165;
            // 
            // colAddress2
            // 
            this.colAddress2.FieldName = "Address2";
            this.colAddress2.Name = "colAddress2";
            this.colAddress2.OptionsColumn.AllowEdit = false;
            this.colAddress2.OptionsColumn.AllowFocus = false;
            this.colAddress2.OptionsColumn.ReadOnly = true;
            this.colAddress2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAddress2.Visible = true;
            this.colAddress2.VisibleIndex = 5;
            // 
            // colAddress3
            // 
            this.colAddress3.FieldName = "Address3";
            this.colAddress3.Name = "colAddress3";
            this.colAddress3.OptionsColumn.AllowEdit = false;
            this.colAddress3.OptionsColumn.AllowFocus = false;
            this.colAddress3.OptionsColumn.ReadOnly = true;
            this.colAddress3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAddress3.Visible = true;
            this.colAddress3.VisibleIndex = 6;
            // 
            // colCity
            // 
            this.colCity.FieldName = "City";
            this.colCity.Name = "colCity";
            this.colCity.OptionsColumn.AllowEdit = false;
            this.colCity.OptionsColumn.AllowFocus = false;
            this.colCity.OptionsColumn.ReadOnly = true;
            this.colCity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCity.Visible = true;
            this.colCity.VisibleIndex = 7;
            // 
            // colPostcode
            // 
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 8;
            // 
            // colCounty
            // 
            this.colCounty.FieldName = "County";
            this.colCounty.Name = "colCounty";
            this.colCounty.OptionsColumn.AllowEdit = false;
            this.colCounty.OptionsColumn.AllowFocus = false;
            this.colCounty.OptionsColumn.ReadOnly = true;
            this.colCounty.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCounty.Visible = true;
            this.colCounty.VisibleIndex = 9;
            // 
            // colCountry
            // 
            this.colCountry.FieldName = "Country";
            this.colCountry.Name = "colCountry";
            this.colCountry.OptionsColumn.AllowEdit = false;
            this.colCountry.OptionsColumn.AllowFocus = false;
            this.colCountry.OptionsColumn.ReadOnly = true;
            this.colCountry.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCountry.Visible = true;
            this.colCountry.VisibleIndex = 10;
            // 
            // colMainContact
            // 
            this.colMainContact.FieldName = "MainContact";
            this.colMainContact.Name = "colMainContact";
            this.colMainContact.OptionsColumn.AllowEdit = false;
            this.colMainContact.OptionsColumn.AllowFocus = false;
            this.colMainContact.OptionsColumn.ReadOnly = true;
            this.colMainContact.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMainContact.Visible = true;
            this.colMainContact.VisibleIndex = 11;
            this.colMainContact.Width = 136;
            // 
            // colPhone
            // 
            this.colPhone.FieldName = "Phone";
            this.colPhone.Name = "colPhone";
            this.colPhone.OptionsColumn.AllowEdit = false;
            this.colPhone.OptionsColumn.AllowFocus = false;
            this.colPhone.OptionsColumn.ReadOnly = true;
            this.colPhone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPhone.Visible = true;
            this.colPhone.VisibleIndex = 12;
            this.colPhone.Width = 104;
            // 
            // colMobile
            // 
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMobile.Visible = true;
            this.colMobile.VisibleIndex = 13;
            this.colMobile.Width = 117;
            // 
            // colFax
            // 
            this.colFax.FieldName = "Fax";
            this.colFax.Name = "colFax";
            this.colFax.OptionsColumn.AllowEdit = false;
            this.colFax.OptionsColumn.AllowFocus = false;
            this.colFax.OptionsColumn.ReadOnly = true;
            this.colFax.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFax.Visible = true;
            this.colFax.VisibleIndex = 14;
            // 
            // colEmailAddress
            // 
            this.colEmailAddress.FieldName = "EmailAddress";
            this.colEmailAddress.Name = "colEmailAddress";
            this.colEmailAddress.OptionsColumn.AllowEdit = false;
            this.colEmailAddress.OptionsColumn.AllowFocus = false;
            this.colEmailAddress.OptionsColumn.ReadOnly = true;
            this.colEmailAddress.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmailAddress.Visible = true;
            this.colEmailAddress.VisibleIndex = 15;
            this.colEmailAddress.Width = 161;
            // 
            // colNotes
            // 
            this.colNotes.FieldName = "Notes";
            this.colNotes.Name = "colNotes";
            this.colNotes.OptionsColumn.AllowEdit = false;
            this.colNotes.OptionsColumn.AllowFocus = false;
            this.colNotes.OptionsColumn.ReadOnly = true;
            this.colNotes.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes.Visible = true;
            this.colNotes.VisibleIndex = 24;
            this.colNotes.Width = 538;
            // 
            // colWebsite
            // 
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWebsite.Visible = true;
            this.colWebsite.VisibleIndex = 16;
            this.colWebsite.Width = 299;
            // 
            // colVehicleSupplier
            // 
            this.colVehicleSupplier.ColumnEdit = this.ceVehicleSupplierPref;
            this.colVehicleSupplier.FieldName = "VehicleSupplier";
            this.colVehicleSupplier.Name = "colVehicleSupplier";
            this.colVehicleSupplier.Visible = true;
            this.colVehicleSupplier.VisibleIndex = 22;
            this.colVehicleSupplier.Width = 95;
            // 
            // ceVehicleSupplierPref
            // 
            this.ceVehicleSupplierPref.AutoHeight = false;
            this.ceVehicleSupplierPref.Caption = "Check";
            this.ceVehicleSupplierPref.Name = "ceVehicleSupplierPref";
            this.ceVehicleSupplierPref.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceSupplierPreference_EditValueChanging);
            // 
            // colPlantSupplier
            // 
            this.colPlantSupplier.ColumnEdit = this.cePlantSupplierPref;
            this.colPlantSupplier.FieldName = "PlantSupplier";
            this.colPlantSupplier.Name = "colPlantSupplier";
            this.colPlantSupplier.Visible = true;
            this.colPlantSupplier.VisibleIndex = 23;
            this.colPlantSupplier.Width = 86;
            // 
            // cePlantSupplierPref
            // 
            this.cePlantSupplierPref.AutoHeight = false;
            this.cePlantSupplierPref.Caption = "Check";
            this.cePlantSupplierPref.Name = "cePlantSupplierPref";
            this.cePlantSupplierPref.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.cePlantSupplierPref_EditValueChanging);
            // 
            // colGadgetSupplier
            // 
            this.colGadgetSupplier.ColumnEdit = this.ceGadgetSupplierPref;
            this.colGadgetSupplier.FieldName = "GadgetSupplier";
            this.colGadgetSupplier.Name = "colGadgetSupplier";
            this.colGadgetSupplier.Visible = true;
            this.colGadgetSupplier.VisibleIndex = 20;
            this.colGadgetSupplier.Width = 97;
            // 
            // ceGadgetSupplierPref
            // 
            this.ceGadgetSupplierPref.AutoHeight = false;
            this.ceGadgetSupplierPref.Caption = "Check";
            this.ceGadgetSupplierPref.Name = "ceGadgetSupplierPref";
            this.ceGadgetSupplierPref.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceGadgetSupplierPref_EditValueChanging);
            // 
            // colHardwareSupplier
            // 
            this.colHardwareSupplier.ColumnEdit = this.ceHardwareSupplierPref;
            this.colHardwareSupplier.FieldName = "HardwareSupplier";
            this.colHardwareSupplier.Name = "colHardwareSupplier";
            this.colHardwareSupplier.Visible = true;
            this.colHardwareSupplier.VisibleIndex = 19;
            this.colHardwareSupplier.Width = 109;
            // 
            // ceHardwareSupplierPref
            // 
            this.ceHardwareSupplierPref.AutoHeight = false;
            this.ceHardwareSupplierPref.Caption = "Check";
            this.ceHardwareSupplierPref.Name = "ceHardwareSupplierPref";
            this.ceHardwareSupplierPref.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceHardwareSupplierPref_EditValueChanging);
            // 
            // colSoftwareSupplier
            // 
            this.colSoftwareSupplier.ColumnEdit = this.ceSoftwareSupplierPref;
            this.colSoftwareSupplier.FieldName = "SoftwareSupplier";
            this.colSoftwareSupplier.Name = "colSoftwareSupplier";
            this.colSoftwareSupplier.Visible = true;
            this.colSoftwareSupplier.VisibleIndex = 18;
            this.colSoftwareSupplier.Width = 106;
            // 
            // ceSoftwareSupplierPref
            // 
            this.ceSoftwareSupplierPref.AutoHeight = false;
            this.ceSoftwareSupplierPref.Caption = "Check";
            this.ceSoftwareSupplierPref.Name = "ceSoftwareSupplierPref";
            this.ceSoftwareSupplierPref.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceSoftwareSupplierPref_EditValueChanging);
            // 
            // colOfficeSupplier
            // 
            this.colOfficeSupplier.ColumnEdit = this.ceOfficeSupplierPref;
            this.colOfficeSupplier.FieldName = "OfficeSupplier";
            this.colOfficeSupplier.Name = "colOfficeSupplier";
            this.colOfficeSupplier.Visible = true;
            this.colOfficeSupplier.VisibleIndex = 21;
            this.colOfficeSupplier.Width = 91;
            // 
            // ceOfficeSupplierPref
            // 
            this.ceOfficeSupplierPref.AutoHeight = false;
            this.ceOfficeSupplierPref.Caption = "Check";
            this.ceOfficeSupplierPref.Name = "ceOfficeSupplierPref";
            this.ceOfficeSupplierPref.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceOfficeSupplierPref_EditValueChanging);
            // 
            // colRentalSupplier
            // 
            this.colRentalSupplier.ColumnEdit = this.ceRentalSupplierPref;
            this.colRentalSupplier.FieldName = "RentalSupplier";
            this.colRentalSupplier.Name = "colRentalSupplier";
            this.colRentalSupplier.Visible = true;
            this.colRentalSupplier.VisibleIndex = 17;
            this.colRentalSupplier.Width = 93;
            // 
            // ceRentalSupplierPref
            // 
            this.ceRentalSupplierPref.AutoHeight = false;
            this.ceRentalSupplierPref.Caption = "Check";
            this.ceRentalSupplierPref.Name = "ceRentalSupplierPref";
            this.ceRentalSupplierPref.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceRentalSupplierPref_EditValueChanging);
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // NotesMemoExEdit
            // 
            this.NotesMemoExEdit.AutoHeight = false;
            this.NotesMemoExEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NotesMemoExEdit.Name = "NotesMemoExEdit";
            this.NotesMemoExEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.NotesMemoExEdit.ShowIcon = false;
            // 
            // webRepositoryItemHyperLinkEdit
            // 
            this.webRepositoryItemHyperLinkEdit.AutoHeight = false;
            this.webRepositoryItemHyperLinkEdit.Name = "webRepositoryItemHyperLinkEdit";
            this.webRepositoryItemHyperLinkEdit.SingleClick = true;
            // 
            // lueSupplierType
            // 
            this.lueSupplierType.AutoHeight = false;
            this.lueSupplierType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSupplierType.DisplayMember = "Value";
            this.lueSupplierType.Name = "lueSupplierType";
            this.lueSupplierType.ValueMember = "Pick_List_ID";
            // 
            // TextEdit
            // 
            this.TextEdit.AutoHeight = false;
            this.TextEdit.Name = "TextEdit";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // bbiCancel
            // 
            this.bbiCancel.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiCancel.Caption = "Cancel and Close";
            this.bbiCancel.DropDownEnabled = false;
            this.bbiCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCancel.Glyph")));
            this.bbiCancel.Id = 30;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11020_Supplier_ListTableAdapter
            // 
            this.sp_AS_11020_Supplier_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Supplier_Manager
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1372, 748);
            this.Controls.Add(this.managerGridControl);
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Supplier_Manager";
            this.Text = "Supplier Manager";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.frm_AS_Supplier_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Supplier_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            this.Controls.SetChildIndex(this.managerGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceVehicleSupplierPref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cePlantSupplierPref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGadgetSupplierPref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceHardwareSupplierPref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSoftwareSupplierPref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOfficeSupplierPref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceRentalSupplierPref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoExEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.webRepositoryItemHyperLinkEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl managerGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView managerGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit NotesMemoExEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit webRepositoryItemHyperLinkEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueSupplierType;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit TextEdit;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource spAS11020SupplierListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierType;
        private DevExpress.XtraGrid.Columns.GridColumn colAccountNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress3;
        private DevExpress.XtraGrid.Columns.GridColumn colCity;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colCounty;
        private DevExpress.XtraGrid.Columns.GridColumn colCountry;
        private DevExpress.XtraGrid.Columns.GridColumn colMainContact;
        private DevExpress.XtraGrid.Columns.GridColumn colPhone;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colFax;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter sp_AS_11020_Supplier_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleSupplier;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceVehicleSupplierPref;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantSupplier;
        private DevExpress.XtraGrid.Columns.GridColumn colGadgetSupplier;
        private DevExpress.XtraGrid.Columns.GridColumn colHardwareSupplier;
        private DevExpress.XtraGrid.Columns.GridColumn colSoftwareSupplier;
        private DevExpress.XtraGrid.Columns.GridColumn colOfficeSupplier;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalSupplier;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit cePlantSupplierPref;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceGadgetSupplierPref;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceHardwareSupplierPref;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceSoftwareSupplierPref;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceOfficeSupplierPref;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceRentalSupplierPref;
    }
}

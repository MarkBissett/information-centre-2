﻿namespace WoodPlan5
{
    partial class frm_AS_P11D_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_P11D_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11166P11DItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_DataEntry1 = new WoodPlan5.DataSet_AS_DataEntry();
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.EquipmentIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11002EquipmentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.EquipmentReferenceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ManufacturerIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.KeeperAllocationIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11044KeeperAllocationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TransactionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.KeeperTypeIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLKeeperTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.KeeperTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.KeeperIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11044KeepersItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.KeeperTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.KeeperFullNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AllocationDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.AllocationEndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.AllocationStatusIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLKeeperAllocationStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.AllocationStatusTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ListPriceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AdditionalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EmissionsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EngineSizeSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.FuelTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RegistrationPlateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RegistrationDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.MakeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ModelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NotesMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEquipmentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForManufacturerID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKeeperAllocationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransactionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKeeperTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKeeperType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKeeperID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKeeper = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKeeperFullName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllocationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllocationEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllocationStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllocationStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForListPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAdditionalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEmissions = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEngineSize = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFuelType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegistrationPlate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegistrationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMake = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp_AS_11166_P11D_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11166_P11D_ItemTableAdapter();
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter();
            this.sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter();
            this.sp_AS_11044_Keepers_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keepers_ItemTableAdapter();
            this.sp_AS_11000_PL_Keeper_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_TypeTableAdapter();
            this.sp_AS_11002_Equipment_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11166P11DItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11002EquipmentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentReferenceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManufacturerIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperAllocationIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperTypeIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeepersItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperFullNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationEndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationEndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationStatusIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperAllocationStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationStatusTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListPriceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdditionalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmissionsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EngineSizeSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FuelTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegistrationPlateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegistrationDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegistrationDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MakeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ModelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManufacturerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperAllocationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransactionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperFullName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForListPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdditionalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmissions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEngineSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFuelType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegistrationPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegistrationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1252, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 699);
            this.barDockControlBottom.Size = new System.Drawing.Size(1252, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 673);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1252, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 673);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.layoutControlGroup2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "editFormLayoutControlGroup";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1252, 673);
            this.editFormLayoutControlGroup.Text = "editFormLayoutControlGroup";
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1232, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11166P11DItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(12, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "p11dDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // spAS11166P11DItemBindingSource
            // 
            this.spAS11166P11DItemBindingSource.DataMember = "sp_AS_11166_P11D_Item";
            this.spAS11166P11DItemBindingSource.DataSource = this.dataSet_AS_DataEntry1;
            // 
            // dataSet_AS_DataEntry1
            // 
            this.dataSet_AS_DataEntry1.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.EquipmentIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.EquipmentReferenceTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ManufacturerIDTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.KeeperAllocationIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.TransactionDateDateEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.KeeperTypeIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.KeeperTypeTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.KeeperIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.KeeperTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.KeeperFullNameTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.AllocationDateDateEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.AllocationEndDateDateEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.AllocationStatusIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.AllocationStatusTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ListPriceSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.AdditionalCostSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.EmissionsSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.EngineSizeSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.FuelTypeTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.RegistrationPlateTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.RegistrationDateDateEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.MakeTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ModelTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.NotesMemoEdit);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11166P11DItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1935, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1252, 673);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // EquipmentIDLookUpEdit
            // 
            this.EquipmentIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "EquipmentID", true));
            this.EquipmentIDLookUpEdit.Location = new System.Drawing.Point(118, 35);
            this.EquipmentIDLookUpEdit.MenuManager = this.barManager1;
            this.EquipmentIDLookUpEdit.Name = "EquipmentIDLookUpEdit";
            this.EquipmentIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EquipmentIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EquipmentReference", "Equipment Reference", 113, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EquipmentCategory", "Equipment Category", 108, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ManufacturerID", "Manufacturer ID", 89, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Make", "Make", 35, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Model", "Model", 38, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("PrivatelyUsed", "Privately Used", 79, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.EquipmentIDLookUpEdit.Properties.DataSource = this.spAS11002EquipmentItemBindingSource;
            this.EquipmentIDLookUpEdit.Properties.DisplayMember = "EquipmentReference";
            this.EquipmentIDLookUpEdit.Properties.ValueMember = "EquipmentID";
            this.EquipmentIDLookUpEdit.Size = new System.Drawing.Size(1122, 20);
            this.EquipmentIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.EquipmentIDLookUpEdit.TabIndex = 123;
            // 
            // spAS11002EquipmentItemBindingSource
            // 
            this.spAS11002EquipmentItemBindingSource.DataMember = "sp_AS_11002_Equipment_Item";
            this.spAS11002EquipmentItemBindingSource.DataSource = this.dataSet_AS_DataEntry1;
            // 
            // EquipmentReferenceTextEdit
            // 
            this.EquipmentReferenceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "EquipmentReference", true));
            this.EquipmentReferenceTextEdit.Location = new System.Drawing.Point(118, 59);
            this.EquipmentReferenceTextEdit.MenuManager = this.barManager1;
            this.EquipmentReferenceTextEdit.Name = "EquipmentReferenceTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.EquipmentReferenceTextEdit, true);
            this.EquipmentReferenceTextEdit.Size = new System.Drawing.Size(1122, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EquipmentReferenceTextEdit, optionsSpelling1);
            this.EquipmentReferenceTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.EquipmentReferenceTextEdit.TabIndex = 124;
            // 
            // ManufacturerIDTextEdit
            // 
            this.ManufacturerIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "ManufacturerID", true));
            this.ManufacturerIDTextEdit.Location = new System.Drawing.Point(118, 83);
            this.ManufacturerIDTextEdit.MenuManager = this.barManager1;
            this.ManufacturerIDTextEdit.Name = "ManufacturerIDTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ManufacturerIDTextEdit, true);
            this.ManufacturerIDTextEdit.Size = new System.Drawing.Size(1122, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ManufacturerIDTextEdit, optionsSpelling2);
            this.ManufacturerIDTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.ManufacturerIDTextEdit.TabIndex = 125;
            // 
            // KeeperAllocationIDLookUpEdit
            // 
            this.KeeperAllocationIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "KeeperAllocationID", true));
            this.KeeperAllocationIDLookUpEdit.Location = new System.Drawing.Point(118, 107);
            this.KeeperAllocationIDLookUpEdit.MenuManager = this.barManager1;
            this.KeeperAllocationIDLookUpEdit.Name = "KeeperAllocationIDLookUpEdit";
            this.KeeperAllocationIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.KeeperAllocationIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EquipmentReference", "Equipment Reference", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("KeeperType", "Keeper Type", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Keeper", "Keeper", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AllocationStatus", "Allocation Status", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AllocationDate", "Allocation Date", 100, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AllocationEndDate", "Allocation End Date", 100, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near)});
            this.KeeperAllocationIDLookUpEdit.Properties.DataSource = this.spAS11044KeeperAllocationItemBindingSource;
            this.KeeperAllocationIDLookUpEdit.Properties.DisplayMember = "AllocationStatus";
            this.KeeperAllocationIDLookUpEdit.Properties.ValueMember = "KeeperAllocationID";
            this.KeeperAllocationIDLookUpEdit.Size = new System.Drawing.Size(1122, 20);
            this.KeeperAllocationIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.KeeperAllocationIDLookUpEdit.TabIndex = 126;
            // 
            // spAS11044KeeperAllocationItemBindingSource
            // 
            this.spAS11044KeeperAllocationItemBindingSource.DataMember = "sp_AS_11044_Keeper_Allocation_Item";
            this.spAS11044KeeperAllocationItemBindingSource.DataSource = this.dataSet_AS_DataEntry1;
            // 
            // TransactionDateDateEdit
            // 
            this.TransactionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "TransactionDate", true));
            this.TransactionDateDateEdit.EditValue = null;
            this.TransactionDateDateEdit.Location = new System.Drawing.Point(118, 131);
            this.TransactionDateDateEdit.MenuManager = this.barManager1;
            this.TransactionDateDateEdit.Name = "TransactionDateDateEdit";
            this.TransactionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TransactionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TransactionDateDateEdit.Size = new System.Drawing.Size(1122, 20);
            this.TransactionDateDateEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.TransactionDateDateEdit.TabIndex = 127;
            // 
            // KeeperTypeIDLookUpEdit
            // 
            this.KeeperTypeIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "KeeperTypeID", true));
            this.KeeperTypeIDLookUpEdit.Location = new System.Drawing.Point(118, 155);
            this.KeeperTypeIDLookUpEdit.MenuManager = this.barManager1;
            this.KeeperTypeIDLookUpEdit.Name = "KeeperTypeIDLookUpEdit";
            this.KeeperTypeIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.KeeperTypeIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Keeper Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.KeeperTypeIDLookUpEdit.Properties.DataSource = this.spAS11000PLKeeperTypeBindingSource;
            this.KeeperTypeIDLookUpEdit.Properties.DisplayMember = "Value";
            this.KeeperTypeIDLookUpEdit.Properties.ValueMember = "PickListID";
            this.KeeperTypeIDLookUpEdit.Size = new System.Drawing.Size(1122, 20);
            this.KeeperTypeIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.KeeperTypeIDLookUpEdit.TabIndex = 128;
            // 
            // spAS11000PLKeeperTypeBindingSource
            // 
            this.spAS11000PLKeeperTypeBindingSource.DataMember = "sp_AS_11000_PL_Keeper_Type";
            this.spAS11000PLKeeperTypeBindingSource.DataSource = this.dataSet_AS_DataEntry1;
            // 
            // KeeperTypeTextEdit
            // 
            this.KeeperTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "KeeperType", true));
            this.KeeperTypeTextEdit.Location = new System.Drawing.Point(118, 179);
            this.KeeperTypeTextEdit.MenuManager = this.barManager1;
            this.KeeperTypeTextEdit.Name = "KeeperTypeTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.KeeperTypeTextEdit, true);
            this.KeeperTypeTextEdit.Size = new System.Drawing.Size(1122, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.KeeperTypeTextEdit, optionsSpelling3);
            this.KeeperTypeTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.KeeperTypeTextEdit.TabIndex = 129;
            // 
            // KeeperIDLookUpEdit
            // 
            this.KeeperIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "KeeperID", true));
            this.KeeperIDLookUpEdit.Location = new System.Drawing.Point(118, 203);
            this.KeeperIDLookUpEdit.MenuManager = this.barManager1;
            this.KeeperIDLookUpEdit.Name = "KeeperIDLookUpEdit";
            this.KeeperIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.KeeperIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("KeeperType", "Keeper Type", 84, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EmployeeNumber", "Employee Number", 96, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FullName", "Full Name", 56, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.KeeperIDLookUpEdit.Properties.DataSource = this.spAS11044KeepersItemBindingSource;
            this.KeeperIDLookUpEdit.Properties.DisplayMember = "FullName";
            this.KeeperIDLookUpEdit.Properties.ValueMember = "EmployeeNumber";
            this.KeeperIDLookUpEdit.Size = new System.Drawing.Size(1122, 20);
            this.KeeperIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.KeeperIDLookUpEdit.TabIndex = 130;
            // 
            // spAS11044KeepersItemBindingSource
            // 
            this.spAS11044KeepersItemBindingSource.DataMember = "sp_AS_11044_Keepers_Item";
            this.spAS11044KeepersItemBindingSource.DataSource = this.dataSet_AS_DataEntry1;
            // 
            // KeeperTextEdit
            // 
            this.KeeperTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "Keeper", true));
            this.KeeperTextEdit.Location = new System.Drawing.Point(118, 227);
            this.KeeperTextEdit.MenuManager = this.barManager1;
            this.KeeperTextEdit.Name = "KeeperTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.KeeperTextEdit, true);
            this.KeeperTextEdit.Size = new System.Drawing.Size(1122, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.KeeperTextEdit, optionsSpelling4);
            this.KeeperTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.KeeperTextEdit.TabIndex = 131;
            // 
            // KeeperFullNameTextEdit
            // 
            this.KeeperFullNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "KeeperFullName", true));
            this.KeeperFullNameTextEdit.Location = new System.Drawing.Point(118, 251);
            this.KeeperFullNameTextEdit.MenuManager = this.barManager1;
            this.KeeperFullNameTextEdit.Name = "KeeperFullNameTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.KeeperFullNameTextEdit, true);
            this.KeeperFullNameTextEdit.Size = new System.Drawing.Size(1122, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.KeeperFullNameTextEdit, optionsSpelling5);
            this.KeeperFullNameTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.KeeperFullNameTextEdit.TabIndex = 132;
            // 
            // AllocationDateDateEdit
            // 
            this.AllocationDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "AllocationDate", true));
            this.AllocationDateDateEdit.EditValue = null;
            this.AllocationDateDateEdit.Location = new System.Drawing.Point(118, 275);
            this.AllocationDateDateEdit.MenuManager = this.barManager1;
            this.AllocationDateDateEdit.Name = "AllocationDateDateEdit";
            this.AllocationDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AllocationDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AllocationDateDateEdit.Size = new System.Drawing.Size(1122, 20);
            this.AllocationDateDateEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.AllocationDateDateEdit.TabIndex = 133;
            // 
            // AllocationEndDateDateEdit
            // 
            this.AllocationEndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "AllocationEndDate", true));
            this.AllocationEndDateDateEdit.EditValue = null;
            this.AllocationEndDateDateEdit.Location = new System.Drawing.Point(118, 299);
            this.AllocationEndDateDateEdit.MenuManager = this.barManager1;
            this.AllocationEndDateDateEdit.Name = "AllocationEndDateDateEdit";
            this.AllocationEndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AllocationEndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AllocationEndDateDateEdit.Size = new System.Drawing.Size(1122, 20);
            this.AllocationEndDateDateEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.AllocationEndDateDateEdit.TabIndex = 134;
            // 
            // AllocationStatusIDLookUpEdit
            // 
            this.AllocationStatusIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "AllocationStatusID", true));
            this.AllocationStatusIDLookUpEdit.Location = new System.Drawing.Point(118, 323);
            this.AllocationStatusIDLookUpEdit.MenuManager = this.barManager1;
            this.AllocationStatusIDLookUpEdit.Name = "AllocationStatusIDLookUpEdit";
            this.AllocationStatusIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AllocationStatusIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Allocation Status", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.AllocationStatusIDLookUpEdit.Properties.DataSource = this.spAS11000PLKeeperAllocationStatusBindingSource;
            this.AllocationStatusIDLookUpEdit.Properties.DisplayMember = "Value";
            this.AllocationStatusIDLookUpEdit.Properties.ValueMember = "PickListID";
            this.AllocationStatusIDLookUpEdit.Size = new System.Drawing.Size(1122, 20);
            this.AllocationStatusIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.AllocationStatusIDLookUpEdit.TabIndex = 135;
            // 
            // spAS11000PLKeeperAllocationStatusBindingSource
            // 
            this.spAS11000PLKeeperAllocationStatusBindingSource.DataMember = "sp_AS_11000_PL_Keeper_Allocation_Status";
            this.spAS11000PLKeeperAllocationStatusBindingSource.DataSource = this.dataSet_AS_DataEntry1;
            // 
            // AllocationStatusTextEdit
            // 
            this.AllocationStatusTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "AllocationStatus", true));
            this.AllocationStatusTextEdit.Location = new System.Drawing.Point(118, 347);
            this.AllocationStatusTextEdit.MenuManager = this.barManager1;
            this.AllocationStatusTextEdit.Name = "AllocationStatusTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.AllocationStatusTextEdit, true);
            this.AllocationStatusTextEdit.Size = new System.Drawing.Size(1122, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AllocationStatusTextEdit, optionsSpelling6);
            this.AllocationStatusTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.AllocationStatusTextEdit.TabIndex = 136;
            // 
            // ListPriceSpinEdit
            // 
            this.ListPriceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "ListPrice", true));
            this.ListPriceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ListPriceSpinEdit.Location = new System.Drawing.Point(118, 371);
            this.ListPriceSpinEdit.MenuManager = this.barManager1;
            this.ListPriceSpinEdit.Name = "ListPriceSpinEdit";
            this.ListPriceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ListPriceSpinEdit.Size = new System.Drawing.Size(1122, 20);
            this.ListPriceSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.ListPriceSpinEdit.TabIndex = 137;
            // 
            // AdditionalCostSpinEdit
            // 
            this.AdditionalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "AdditionalCost", true));
            this.AdditionalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AdditionalCostSpinEdit.Location = new System.Drawing.Point(118, 395);
            this.AdditionalCostSpinEdit.MenuManager = this.barManager1;
            this.AdditionalCostSpinEdit.Name = "AdditionalCostSpinEdit";
            this.AdditionalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AdditionalCostSpinEdit.Size = new System.Drawing.Size(1122, 20);
            this.AdditionalCostSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.AdditionalCostSpinEdit.TabIndex = 138;
            // 
            // EmissionsSpinEdit
            // 
            this.EmissionsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "Emissions", true));
            this.EmissionsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EmissionsSpinEdit.Location = new System.Drawing.Point(118, 419);
            this.EmissionsSpinEdit.MenuManager = this.barManager1;
            this.EmissionsSpinEdit.Name = "EmissionsSpinEdit";
            this.EmissionsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EmissionsSpinEdit.Size = new System.Drawing.Size(1122, 20);
            this.EmissionsSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.EmissionsSpinEdit.TabIndex = 139;
            // 
            // EngineSizeSpinEdit
            // 
            this.EngineSizeSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "EngineSize", true));
            this.EngineSizeSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EngineSizeSpinEdit.Location = new System.Drawing.Point(118, 443);
            this.EngineSizeSpinEdit.MenuManager = this.barManager1;
            this.EngineSizeSpinEdit.Name = "EngineSizeSpinEdit";
            this.EngineSizeSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EngineSizeSpinEdit.Size = new System.Drawing.Size(1122, 20);
            this.EngineSizeSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.EngineSizeSpinEdit.TabIndex = 140;
            // 
            // FuelTypeTextEdit
            // 
            this.FuelTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "FuelType", true));
            this.FuelTypeTextEdit.Location = new System.Drawing.Point(118, 467);
            this.FuelTypeTextEdit.MenuManager = this.barManager1;
            this.FuelTypeTextEdit.Name = "FuelTypeTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.FuelTypeTextEdit, true);
            this.FuelTypeTextEdit.Size = new System.Drawing.Size(1122, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FuelTypeTextEdit, optionsSpelling7);
            this.FuelTypeTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.FuelTypeTextEdit.TabIndex = 141;
            // 
            // RegistrationPlateTextEdit
            // 
            this.RegistrationPlateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "RegistrationPlate", true));
            this.RegistrationPlateTextEdit.Location = new System.Drawing.Point(118, 491);
            this.RegistrationPlateTextEdit.MenuManager = this.barManager1;
            this.RegistrationPlateTextEdit.Name = "RegistrationPlateTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegistrationPlateTextEdit, true);
            this.RegistrationPlateTextEdit.Size = new System.Drawing.Size(1122, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegistrationPlateTextEdit, optionsSpelling8);
            this.RegistrationPlateTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.RegistrationPlateTextEdit.TabIndex = 142;
            // 
            // RegistrationDateDateEdit
            // 
            this.RegistrationDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "RegistrationDate", true));
            this.RegistrationDateDateEdit.EditValue = null;
            this.RegistrationDateDateEdit.Location = new System.Drawing.Point(118, 515);
            this.RegistrationDateDateEdit.MenuManager = this.barManager1;
            this.RegistrationDateDateEdit.Name = "RegistrationDateDateEdit";
            this.RegistrationDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RegistrationDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RegistrationDateDateEdit.Size = new System.Drawing.Size(1122, 20);
            this.RegistrationDateDateEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.RegistrationDateDateEdit.TabIndex = 143;
            // 
            // MakeTextEdit
            // 
            this.MakeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "Make", true));
            this.MakeTextEdit.Location = new System.Drawing.Point(118, 539);
            this.MakeTextEdit.MenuManager = this.barManager1;
            this.MakeTextEdit.Name = "MakeTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.MakeTextEdit, true);
            this.MakeTextEdit.Size = new System.Drawing.Size(1122, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.MakeTextEdit, optionsSpelling9);
            this.MakeTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.MakeTextEdit.TabIndex = 144;
            // 
            // ModelTextEdit
            // 
            this.ModelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "Model", true));
            this.ModelTextEdit.Location = new System.Drawing.Point(118, 563);
            this.ModelTextEdit.MenuManager = this.barManager1;
            this.ModelTextEdit.Name = "ModelTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ModelTextEdit, true);
            this.ModelTextEdit.Size = new System.Drawing.Size(1122, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ModelTextEdit, optionsSpelling10);
            this.ModelTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.ModelTextEdit.TabIndex = 145;
            // 
            // NotesMemoEdit
            // 
            this.NotesMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11166P11DItemBindingSource, "Notes", true));
            this.NotesMemoEdit.Location = new System.Drawing.Point(118, 587);
            this.NotesMemoEdit.MenuManager = this.barManager1;
            this.NotesMemoEdit.Name = "NotesMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.NotesMemoEdit, true);
            this.NotesMemoEdit.Size = new System.Drawing.Size(1122, 74);
            this.scSpellChecker.SetSpellCheckerOptions(this.NotesMemoEdit, optionsSpelling11);
            this.NotesMemoEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.NotesMemoEdit.TabIndex = 146;
            this.NotesMemoEdit.UseOptimizedRendering = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEquipmentID,
            this.ItemForEquipmentReference,
            this.ItemForManufacturerID,
            this.ItemForKeeperAllocationID,
            this.ItemForTransactionDate,
            this.ItemForKeeperTypeID,
            this.ItemForKeeperType,
            this.ItemForKeeperID,
            this.ItemForKeeper,
            this.ItemForKeeperFullName,
            this.ItemForAllocationDate,
            this.ItemForAllocationEndDate,
            this.ItemForAllocationStatusID,
            this.ItemForAllocationStatus,
            this.ItemForListPrice,
            this.ItemForAdditionalCost});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1232, 384);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // ItemForEquipmentID
            // 
            this.ItemForEquipmentID.Control = this.EquipmentIDLookUpEdit;
            this.ItemForEquipmentID.CustomizationFormText = "Equipment ID";
            this.ItemForEquipmentID.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentID.Name = "ItemForEquipmentID";
            this.ItemForEquipmentID.Size = new System.Drawing.Size(1232, 24);
            this.ItemForEquipmentID.Text = "Equipment ID";
            this.ItemForEquipmentID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.EquipmentReferenceTextEdit;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 24);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(1232, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForManufacturerID
            // 
            this.ItemForManufacturerID.Control = this.ManufacturerIDTextEdit;
            this.ItemForManufacturerID.CustomizationFormText = "Manufacturer ID";
            this.ItemForManufacturerID.Location = new System.Drawing.Point(0, 48);
            this.ItemForManufacturerID.Name = "ItemForManufacturerID";
            this.ItemForManufacturerID.Size = new System.Drawing.Size(1232, 24);
            this.ItemForManufacturerID.Text = "Manufacturer ID";
            this.ItemForManufacturerID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForKeeperAllocationID
            // 
            this.ItemForKeeperAllocationID.Control = this.KeeperAllocationIDLookUpEdit;
            this.ItemForKeeperAllocationID.CustomizationFormText = "Keeper Allocation ID";
            this.ItemForKeeperAllocationID.Location = new System.Drawing.Point(0, 72);
            this.ItemForKeeperAllocationID.Name = "ItemForKeeperAllocationID";
            this.ItemForKeeperAllocationID.Size = new System.Drawing.Size(1232, 24);
            this.ItemForKeeperAllocationID.Text = "Keeper Allocation ID";
            this.ItemForKeeperAllocationID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForTransactionDate
            // 
            this.ItemForTransactionDate.Control = this.TransactionDateDateEdit;
            this.ItemForTransactionDate.CustomizationFormText = "Transaction Date";
            this.ItemForTransactionDate.Location = new System.Drawing.Point(0, 96);
            this.ItemForTransactionDate.Name = "ItemForTransactionDate";
            this.ItemForTransactionDate.Size = new System.Drawing.Size(1232, 24);
            this.ItemForTransactionDate.Text = "Transaction Date";
            this.ItemForTransactionDate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForKeeperTypeID
            // 
            this.ItemForKeeperTypeID.Control = this.KeeperTypeIDLookUpEdit;
            this.ItemForKeeperTypeID.CustomizationFormText = "Keeper Type ID";
            this.ItemForKeeperTypeID.Location = new System.Drawing.Point(0, 120);
            this.ItemForKeeperTypeID.Name = "ItemForKeeperTypeID";
            this.ItemForKeeperTypeID.Size = new System.Drawing.Size(1232, 24);
            this.ItemForKeeperTypeID.Text = "Keeper Type ID";
            this.ItemForKeeperTypeID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForKeeperType
            // 
            this.ItemForKeeperType.Control = this.KeeperTypeTextEdit;
            this.ItemForKeeperType.CustomizationFormText = "Keeper Type";
            this.ItemForKeeperType.Location = new System.Drawing.Point(0, 144);
            this.ItemForKeeperType.Name = "ItemForKeeperType";
            this.ItemForKeeperType.Size = new System.Drawing.Size(1232, 24);
            this.ItemForKeeperType.Text = "Keeper Type";
            this.ItemForKeeperType.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForKeeperID
            // 
            this.ItemForKeeperID.Control = this.KeeperIDLookUpEdit;
            this.ItemForKeeperID.CustomizationFormText = "Keeper ID";
            this.ItemForKeeperID.Location = new System.Drawing.Point(0, 168);
            this.ItemForKeeperID.Name = "ItemForKeeperID";
            this.ItemForKeeperID.Size = new System.Drawing.Size(1232, 24);
            this.ItemForKeeperID.Text = "Keeper ID";
            this.ItemForKeeperID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForKeeper
            // 
            this.ItemForKeeper.Control = this.KeeperTextEdit;
            this.ItemForKeeper.CustomizationFormText = "Keeper";
            this.ItemForKeeper.Location = new System.Drawing.Point(0, 192);
            this.ItemForKeeper.Name = "ItemForKeeper";
            this.ItemForKeeper.Size = new System.Drawing.Size(1232, 24);
            this.ItemForKeeper.Text = "Keeper";
            this.ItemForKeeper.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForKeeperFullName
            // 
            this.ItemForKeeperFullName.Control = this.KeeperFullNameTextEdit;
            this.ItemForKeeperFullName.CustomizationFormText = "Keeper Full Name";
            this.ItemForKeeperFullName.Location = new System.Drawing.Point(0, 216);
            this.ItemForKeeperFullName.Name = "ItemForKeeperFullName";
            this.ItemForKeeperFullName.Size = new System.Drawing.Size(1232, 24);
            this.ItemForKeeperFullName.Text = "Keeper Full Name";
            this.ItemForKeeperFullName.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForAllocationDate
            // 
            this.ItemForAllocationDate.Control = this.AllocationDateDateEdit;
            this.ItemForAllocationDate.CustomizationFormText = "Allocation Date";
            this.ItemForAllocationDate.Location = new System.Drawing.Point(0, 240);
            this.ItemForAllocationDate.Name = "ItemForAllocationDate";
            this.ItemForAllocationDate.Size = new System.Drawing.Size(1232, 24);
            this.ItemForAllocationDate.Text = "Allocation Date";
            this.ItemForAllocationDate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForAllocationEndDate
            // 
            this.ItemForAllocationEndDate.Control = this.AllocationEndDateDateEdit;
            this.ItemForAllocationEndDate.CustomizationFormText = "Allocation End Date";
            this.ItemForAllocationEndDate.Location = new System.Drawing.Point(0, 264);
            this.ItemForAllocationEndDate.Name = "ItemForAllocationEndDate";
            this.ItemForAllocationEndDate.Size = new System.Drawing.Size(1232, 24);
            this.ItemForAllocationEndDate.Text = "Allocation End Date";
            this.ItemForAllocationEndDate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForAllocationStatusID
            // 
            this.ItemForAllocationStatusID.Control = this.AllocationStatusIDLookUpEdit;
            this.ItemForAllocationStatusID.CustomizationFormText = "Allocation Status ID";
            this.ItemForAllocationStatusID.Location = new System.Drawing.Point(0, 288);
            this.ItemForAllocationStatusID.Name = "ItemForAllocationStatusID";
            this.ItemForAllocationStatusID.Size = new System.Drawing.Size(1232, 24);
            this.ItemForAllocationStatusID.Text = "Allocation Status ID";
            this.ItemForAllocationStatusID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForAllocationStatus
            // 
            this.ItemForAllocationStatus.Control = this.AllocationStatusTextEdit;
            this.ItemForAllocationStatus.CustomizationFormText = "Allocation Status";
            this.ItemForAllocationStatus.Location = new System.Drawing.Point(0, 312);
            this.ItemForAllocationStatus.Name = "ItemForAllocationStatus";
            this.ItemForAllocationStatus.Size = new System.Drawing.Size(1232, 24);
            this.ItemForAllocationStatus.Text = "Allocation Status";
            this.ItemForAllocationStatus.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForListPrice
            // 
            this.ItemForListPrice.Control = this.ListPriceSpinEdit;
            this.ItemForListPrice.CustomizationFormText = "List Price";
            this.ItemForListPrice.Location = new System.Drawing.Point(0, 336);
            this.ItemForListPrice.Name = "ItemForListPrice";
            this.ItemForListPrice.Size = new System.Drawing.Size(1232, 24);
            this.ItemForListPrice.Text = "List Price";
            this.ItemForListPrice.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForAdditionalCost
            // 
            this.ItemForAdditionalCost.Control = this.AdditionalCostSpinEdit;
            this.ItemForAdditionalCost.CustomizationFormText = "Additional Cost";
            this.ItemForAdditionalCost.Location = new System.Drawing.Point(0, 360);
            this.ItemForAdditionalCost.Name = "ItemForAdditionalCost";
            this.ItemForAdditionalCost.Size = new System.Drawing.Size(1232, 24);
            this.ItemForAdditionalCost.Text = "Additional Cost";
            this.ItemForAdditionalCost.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmissions,
            this.ItemForEngineSize,
            this.ItemForFuelType,
            this.ItemForRegistrationPlate,
            this.ItemForRegistrationDate,
            this.ItemForMake,
            this.ItemForModel,
            this.ItemForNotes});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 407);
            this.layoutControlGroup2.Name = "autoGeneratedGroup1";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1232, 246);
            this.layoutControlGroup2.Text = "autoGeneratedGroup1";
            // 
            // ItemForEmissions
            // 
            this.ItemForEmissions.Control = this.EmissionsSpinEdit;
            this.ItemForEmissions.CustomizationFormText = "Emissions";
            this.ItemForEmissions.Location = new System.Drawing.Point(0, 0);
            this.ItemForEmissions.Name = "ItemForEmissions";
            this.ItemForEmissions.Size = new System.Drawing.Size(1232, 24);
            this.ItemForEmissions.Text = "Emissions";
            this.ItemForEmissions.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForEngineSize
            // 
            this.ItemForEngineSize.Control = this.EngineSizeSpinEdit;
            this.ItemForEngineSize.CustomizationFormText = "Engine Size";
            this.ItemForEngineSize.Location = new System.Drawing.Point(0, 24);
            this.ItemForEngineSize.Name = "ItemForEngineSize";
            this.ItemForEngineSize.Size = new System.Drawing.Size(1232, 24);
            this.ItemForEngineSize.Text = "Engine Size";
            this.ItemForEngineSize.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForFuelType
            // 
            this.ItemForFuelType.Control = this.FuelTypeTextEdit;
            this.ItemForFuelType.CustomizationFormText = "Fuel Type";
            this.ItemForFuelType.Location = new System.Drawing.Point(0, 48);
            this.ItemForFuelType.Name = "ItemForFuelType";
            this.ItemForFuelType.Size = new System.Drawing.Size(1232, 24);
            this.ItemForFuelType.Text = "Fuel Type";
            this.ItemForFuelType.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForRegistrationPlate
            // 
            this.ItemForRegistrationPlate.Control = this.RegistrationPlateTextEdit;
            this.ItemForRegistrationPlate.CustomizationFormText = "Registration Plate";
            this.ItemForRegistrationPlate.Location = new System.Drawing.Point(0, 72);
            this.ItemForRegistrationPlate.Name = "ItemForRegistrationPlate";
            this.ItemForRegistrationPlate.Size = new System.Drawing.Size(1232, 24);
            this.ItemForRegistrationPlate.Text = "Registration Plate";
            this.ItemForRegistrationPlate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForRegistrationDate
            // 
            this.ItemForRegistrationDate.Control = this.RegistrationDateDateEdit;
            this.ItemForRegistrationDate.CustomizationFormText = "Registration Date";
            this.ItemForRegistrationDate.Location = new System.Drawing.Point(0, 96);
            this.ItemForRegistrationDate.Name = "ItemForRegistrationDate";
            this.ItemForRegistrationDate.Size = new System.Drawing.Size(1232, 24);
            this.ItemForRegistrationDate.Text = "Registration Date";
            this.ItemForRegistrationDate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForMake
            // 
            this.ItemForMake.Control = this.MakeTextEdit;
            this.ItemForMake.CustomizationFormText = "Make";
            this.ItemForMake.Location = new System.Drawing.Point(0, 120);
            this.ItemForMake.Name = "ItemForMake";
            this.ItemForMake.Size = new System.Drawing.Size(1232, 24);
            this.ItemForMake.Text = "Make";
            this.ItemForMake.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForModel
            // 
            this.ItemForModel.Control = this.ModelTextEdit;
            this.ItemForModel.CustomizationFormText = "Model";
            this.ItemForModel.Location = new System.Drawing.Point(0, 144);
            this.ItemForModel.Name = "ItemForModel";
            this.ItemForModel.Size = new System.Drawing.Size(1232, 24);
            this.ItemForModel.Text = "Model";
            this.ItemForModel.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.NotesMemoEdit;
            this.ItemForNotes.CustomizationFormText = "Notes";
            this.ItemForNotes.Location = new System.Drawing.Point(0, 168);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(1232, 78);
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(103, 13);
            // 
            // sp_AS_11166_P11D_ItemTableAdapter
            // 
            this.sp_AS_11166_P11D_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keeper_Allocation_ItemTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keepers_ItemTableAdapter
            // 
            this.sp_AS_11044_Keepers_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Keeper_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Keeper_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11002_Equipment_ItemTableAdapter
            // 
            this.sp_AS_11002_Equipment_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_P11D_Edit
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1252, 729);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_P11D_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage P11D Details";
            this.Activated += new System.EventHandler(this.frm_AS_Base_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Base_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Base_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11166P11DItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11002EquipmentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentReferenceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManufacturerIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperAllocationIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperTypeIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeepersItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KeeperFullNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationEndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationEndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationStatusIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperAllocationStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationStatusTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListPriceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdditionalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmissionsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EngineSizeSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FuelTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegistrationPlateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegistrationDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegistrationDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MakeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ModelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManufacturerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperAllocationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransactionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperFullName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForListPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdditionalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmissions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEngineSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFuelType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegistrationPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegistrationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LookUpEdit EquipmentIDLookUpEdit;
        private System.Windows.Forms.BindingSource spAS11166P11DItemBindingSource;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry1;
        private DevExpress.XtraEditors.TextEdit EquipmentReferenceTextEdit;
        private DevExpress.XtraEditors.TextEdit ManufacturerIDTextEdit;
        private DevExpress.XtraEditors.LookUpEdit KeeperAllocationIDLookUpEdit;
        private DevExpress.XtraEditors.DateEdit TransactionDateDateEdit;
        private DevExpress.XtraEditors.LookUpEdit KeeperTypeIDLookUpEdit;
        private DevExpress.XtraEditors.TextEdit KeeperTypeTextEdit;
        private DevExpress.XtraEditors.LookUpEdit KeeperIDLookUpEdit;
        private DevExpress.XtraEditors.TextEdit KeeperTextEdit;
        private DevExpress.XtraEditors.TextEdit KeeperFullNameTextEdit;
        private DevExpress.XtraEditors.DateEdit AllocationDateDateEdit;
        private DevExpress.XtraEditors.DateEdit AllocationEndDateDateEdit;
        private DevExpress.XtraEditors.LookUpEdit AllocationStatusIDLookUpEdit;
        private DevExpress.XtraEditors.TextEdit AllocationStatusTextEdit;
        private DevExpress.XtraEditors.SpinEdit ListPriceSpinEdit;
        private DevExpress.XtraEditors.SpinEdit AdditionalCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit EmissionsSpinEdit;
        private DevExpress.XtraEditors.SpinEdit EngineSizeSpinEdit;
        private DevExpress.XtraEditors.TextEdit FuelTypeTextEdit;
        private DevExpress.XtraEditors.TextEdit RegistrationPlateTextEdit;
        private DevExpress.XtraEditors.DateEdit RegistrationDateDateEdit;
        private DevExpress.XtraEditors.TextEdit MakeTextEdit;
        private DevExpress.XtraEditors.TextEdit ModelTextEdit;
        private DevExpress.XtraEditors.MemoEdit NotesMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForManufacturerID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKeeperAllocationID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransactionDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKeeperTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKeeperType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKeeperID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKeeper;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKeeperFullName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllocationDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllocationEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllocationStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllocationStatus;
        private DevExpress.XtraLayout.LayoutControlItem ItemForListPrice;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAdditionalCost;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmissions;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEngineSize;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFuelType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegistrationPlate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegistrationDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMake;
        private DevExpress.XtraLayout.LayoutControlItem ItemForModel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11166_P11D_ItemTableAdapter sp_AS_11166_P11D_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11044KeepersItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLKeeperAllocationStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11044KeeperAllocationItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter sp_AS_11044_Keeper_Allocation_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter sp_AS_11000_PL_Keeper_Allocation_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keepers_ItemTableAdapter sp_AS_11044_Keepers_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11002EquipmentItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLKeeperTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_TypeTableAdapter sp_AS_11000_PL_Keeper_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter sp_AS_11002_Equipment_ItemTableAdapter;


    }
}

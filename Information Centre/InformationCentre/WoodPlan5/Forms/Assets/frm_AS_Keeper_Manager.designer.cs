namespace WoodPlan5
{
    partial class frm_AS_Keeper_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Keeper_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.tableAdapterManager1 = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.bar = new DevExpress.XtraBars.Bar();
            this.bbiRefreshAll = new DevExpress.XtraBars.BarButtonItem();
            this.barSubSendMail = new DevExpress.XtraBars.BarSubItem();
            this.bbiSendMail = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSendSelected = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClearFilter = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFindNewKeeper = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReset = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.bbiExportExcel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportExcelOld = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAutoSend = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiNextEmailDate = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.spAS11111KeeperListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bsAS11112KeeperActiveValid = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11112_Keeper_Email_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11112_Keeper_Email_ItemTableAdapter();
            this.sp_AS_11111_Keeper_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11111_Keeper_ListTableAdapter();
            this.activeUntickedEmailGridControl = new DevExpress.XtraGrid.GridControl();
            this.bsAS11112KeeperActiveUnticked = new System.Windows.Forms.BindingSource(this.components);
            this.activeUntickedEmailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceSelected1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.hplActiveSuppressed = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.activeInvalidEmailTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.activeInvalidGridControl = new DevExpress.XtraGrid.GridControl();
            this.bsAS11112KeeperActiveInvalid = new System.Windows.Forms.BindingSource(this.components);
            this.activeInvalidGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.hplInvalidAssetCount = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.activeKeeperTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btnMoveUP = new DevExpress.XtraEditors.SimpleButton();
            this.btnDown = new DevExpress.XtraEditors.SimpleButton();
            this.activeEmailGridControl = new DevExpress.XtraGrid.GridControl();
            this.activeEmailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmailTrackerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelectedForMail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceSelected = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colEmailTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastBatchEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailHasSendError = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSendReturnMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsCurrentKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAssetCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.hplAssetList = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.keeperListPage = new DevExpress.XtraTab.XtraTabPage();
            this.keeperGridControl = new DevExpress.XtraGrid.GridControl();
            this.keeperGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKeeperAllocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetMiniDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssetDetailedDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ArchiveCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.moneyTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.unresolvedCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.mailTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.spAS11111RetrieveKeeperListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11111_Retrieve_Keeper_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11111_Retrieve_Keeper_ListTableAdapter();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.sp_AS_11000_PL_Email_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Email_TypeTableAdapter();
            this.queriesTableAdapter1 = new WoodPlan5.DataSet_ATTableAdapters.QueriesTableAdapter();
            this.sp_AS_11102_Rental_ManagerTableAdapter1 = new WoodPlan5.DataSet_AS_CoreTableAdapters.sp_AS_11102_Rental_ManagerTableAdapter();
            this.spAS11157EmailScheduleItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11157_Email_Schedule_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11157_Email_Schedule_ItemTableAdapter();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11111KeeperListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAS11112KeeperActiveValid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.activeUntickedEmailGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAS11112KeeperActiveUnticked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.activeUntickedEmailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSelected1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hplActiveSuppressed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            this.activeInvalidEmailTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.activeInvalidGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAS11112KeeperActiveInvalid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.activeInvalidGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hplInvalidAssetCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.activeKeeperTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.activeEmailGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.activeEmailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hplAssetList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.keeperListPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArchiveCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unresolvedCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mailTabControl)).BeginInit();
            this.mailTabControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11111RetrieveKeeperListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11157EmailScheduleItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 632);
            this.barDockControlBottom.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 632);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1362, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 632);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("country_16x16.png", "images/miscellaneous/country_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/country_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "country_16x16.png");
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.Connection = null;
            this.tableAdapterManager1.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // bar
            // 
            this.bar.BarName = "Custom 2";
            this.bar.DockCol = 0;
            this.bar.DockRow = 0;
            this.bar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshAll),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubSendMail, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearFilter, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFindNewKeeper, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReset, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAutoSend, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiNextEmailDate, true)});
            this.bar.Text = "Custom 2";
            // 
            // bbiRefreshAll
            // 
            this.bbiRefreshAll.Caption = "Refresh All";
            this.bbiRefreshAll.Id = 41;
            this.bbiRefreshAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefreshAll.ImageOptions.Image")));
            this.bbiRefreshAll.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRefreshAll.ImageOptions.LargeImage")));
            this.bbiRefreshAll.Name = "bbiRefreshAll";
            this.bbiRefreshAll.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshAll_ItemClick);
            // 
            // barSubSendMail
            // 
            this.barSubSendMail.Caption = "Send Mail";
            this.barSubSendMail.Id = 38;
            this.barSubSendMail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubSendMail.ImageOptions.Image")));
            this.barSubSendMail.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubSendMail.ImageOptions.LargeImage")));
            this.barSubSendMail.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendMail),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSendSelected, true)});
            this.barSubSendMail.Name = "barSubSendMail";
            this.barSubSendMail.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSendMail
            // 
            this.bbiSendMail.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiSendMail.Caption = "Send Mail All Active";
            this.bbiSendMail.Hint = "Send email to all selected records";
            this.bbiSendMail.Id = 27;
            this.bbiSendMail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSendMail.ImageOptions.Image")));
            this.bbiSendMail.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiSendMail.ImageOptions.LargeImage")));
            this.bbiSendMail.Name = "bbiSendMail";
            this.bbiSendMail.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiSendMail.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendMail_ItemClick);
            // 
            // bbiSendSelected
            // 
            this.bbiSendSelected.Caption = "Send To Selected Active";
            this.bbiSendSelected.Id = 39;
            this.bbiSendSelected.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSendSelected.ImageOptions.Image")));
            this.bbiSendSelected.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiSendSelected.ImageOptions.LargeImage")));
            this.bbiSendSelected.Name = "bbiSendSelected";
            this.bbiSendSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiSendSelected.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSendSelected_ItemClick);
            // 
            // bbiClearFilter
            // 
            this.bbiClearFilter.Caption = "Clear Filter";
            this.bbiClearFilter.Id = 37;
            this.bbiClearFilter.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClearFilter.ImageOptions.Image")));
            this.bbiClearFilter.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiClearFilter.ImageOptions.LargeImage")));
            this.bbiClearFilter.Name = "bbiClearFilter";
            this.bbiClearFilter.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiClearFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearFilter_ItemClick);
            // 
            // bbiFindNewKeeper
            // 
            this.bbiFindNewKeeper.Caption = "Find New Keepers";
            this.bbiFindNewKeeper.Hint = "Adds new keepers to the relevant lists";
            this.bbiFindNewKeeper.Id = 29;
            this.bbiFindNewKeeper.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFindNewKeeper.ImageOptions.Image")));
            this.bbiFindNewKeeper.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiFindNewKeeper.ImageOptions.LargeImage")));
            this.bbiFindNewKeeper.Name = "bbiFindNewKeeper";
            this.bbiFindNewKeeper.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiFindNewKeeper.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFindNewKeeper_ItemClick);
            // 
            // bbiReset
            // 
            this.bbiReset.Caption = "Refresh Email Lists";
            this.bbiReset.Hint = "Refreshes all tab data";
            this.bbiReset.Id = 30;
            this.bbiReset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiReset.ImageOptions.Image")));
            this.bbiReset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiReset.ImageOptions.LargeImage")));
            this.bbiReset.Name = "bbiReset";
            this.bbiReset.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiReset.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Export";
            this.barSubItem1.Id = 40;
            this.barSubItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem1.ImageOptions.Image")));
            this.barSubItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem1.ImageOptions.LargeImage")));
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportExcelOld, true)});
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiExportExcel
            // 
            this.bbiExportExcel.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiExportExcel.Caption = "Export to Excel";
            this.bbiExportExcel.Id = 32;
            this.bbiExportExcel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExportExcel.ImageOptions.Image")));
            this.bbiExportExcel.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiExportExcel.ImageOptions.LargeImage")));
            this.bbiExportExcel.Name = "bbiExportExcel";
            this.bbiExportExcel.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiExportExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportOld_ItemClick);
            // 
            // bbiExportExcelOld
            // 
            this.bbiExportExcelOld.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiExportExcelOld.Caption = "Export to Excel Old";
            this.bbiExportExcelOld.Hint = "Exports active grid to excel format";
            this.bbiExportExcelOld.Id = 31;
            this.bbiExportExcelOld.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExportExcelOld.ImageOptions.Image")));
            this.bbiExportExcelOld.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiExportExcelOld.ImageOptions.LargeImage")));
            this.bbiExportExcelOld.Name = "bbiExportExcelOld";
            this.bbiExportExcelOld.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiExportExcelOld.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportExcel_ItemClick);
            // 
            // bbiAutoSend
            // 
            this.bbiAutoSend.Caption = "Auto Send Email";
            this.bbiAutoSend.Edit = this.repositoryItemCheckEdit2;
            this.bbiAutoSend.EditValue = true;
            this.bbiAutoSend.EditWidth = 20;
            this.bbiAutoSend.Id = 36;
            this.bbiAutoSend.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAutoSend.ImageOptions.Image")));
            this.bbiAutoSend.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiAutoSend.ImageOptions.LargeImage")));
            this.bbiAutoSend.ItemAppearance.Disabled.BackColor = System.Drawing.Color.Transparent;
            this.bbiAutoSend.ItemAppearance.Disabled.Options.UseBackColor = true;
            this.bbiAutoSend.ItemAppearance.Hovered.BackColor = System.Drawing.Color.Transparent;
            this.bbiAutoSend.ItemAppearance.Hovered.Options.UseBackColor = true;
            this.bbiAutoSend.ItemAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.bbiAutoSend.ItemAppearance.Normal.Options.UseBackColor = true;
            this.bbiAutoSend.ItemAppearance.Pressed.BackColor = System.Drawing.Color.Transparent;
            this.bbiAutoSend.ItemAppearance.Pressed.Options.UseBackColor = true;
            this.bbiAutoSend.ItemInMenuAppearance.Disabled.BackColor = System.Drawing.Color.Transparent;
            this.bbiAutoSend.ItemInMenuAppearance.Disabled.Options.UseBackColor = true;
            this.bbiAutoSend.ItemInMenuAppearance.Hovered.BackColor = System.Drawing.Color.Transparent;
            this.bbiAutoSend.ItemInMenuAppearance.Hovered.Options.UseBackColor = true;
            this.bbiAutoSend.ItemInMenuAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.bbiAutoSend.ItemInMenuAppearance.Normal.Options.UseBackColor = true;
            this.bbiAutoSend.ItemInMenuAppearance.Pressed.BackColor = System.Drawing.Color.Transparent;
            this.bbiAutoSend.ItemInMenuAppearance.Pressed.Options.UseBackColor = true;
            this.bbiAutoSend.Name = "bbiAutoSend";
            this.bbiAutoSend.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiAutoSend.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAutoSend_ItemClick);
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit2.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEdit2.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit2.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEdit2.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit2.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEdit2.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit2.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // bbiNextEmailDate
            // 
            this.bbiNextEmailDate.Caption = "Next Email Date";
            this.bbiNextEmailDate.Edit = this.repositoryItemDateEdit1;
            this.bbiNextEmailDate.EditValue = new System.DateTime(2014, 5, 16, 13, 30, 12, 450);
            this.bbiNextEmailDate.EditWidth = 128;
            this.bbiNextEmailDate.Id = 35;
            this.bbiNextEmailDate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiNextEmailDate.ImageOptions.Image")));
            this.bbiNextEmailDate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiNextEmailDate.ImageOptions.LargeImage")));
            this.bbiNextEmailDate.Name = "bbiNextEmailDate";
            this.bbiNextEmailDate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiNextEmailDate.EditValueChanged += new System.EventHandler(this.bbiNextEmailDate_EditValueChanged);
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.repositoryItemDateEdit1.MaxValue = new System.DateTime(2030, 1, 1, 0, 0, 0, 0);
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            this.repositoryItemDateEdit1.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // spAS11111KeeperListBindingSource
            // 
            this.spAS11111KeeperListBindingSource.DataMember = "sp_AS_11111_Keeper_List";
            this.spAS11111KeeperListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // bsAS11112KeeperActiveValid
            // 
            this.bsAS11112KeeperActiveValid.DataMember = "sp_AS_11112_Keeper_Email_Item";
            this.bsAS11112KeeperActiveValid.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11112_Keeper_Email_ItemTableAdapter
            // 
            this.sp_AS_11112_Keeper_Email_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11111_Keeper_ListTableAdapter
            // 
            this.sp_AS_11111_Keeper_ListTableAdapter.ClearBeforeFill = true;
            // 
            // activeUntickedEmailGridControl
            // 
            this.activeUntickedEmailGridControl.DataSource = this.bsAS11112KeeperActiveUnticked;
            this.activeUntickedEmailGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.activeUntickedEmailGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.activeUntickedEmailGridControl.Location = new System.Drawing.Point(0, 0);
            this.activeUntickedEmailGridControl.MainView = this.activeUntickedEmailGridView;
            this.activeUntickedEmailGridControl.MenuManager = this.barManager1;
            this.activeUntickedEmailGridControl.Name = "activeUntickedEmailGridControl";
            this.activeUntickedEmailGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.hplActiveSuppressed,
            this.ceSelected1});
            this.activeUntickedEmailGridControl.Size = new System.Drawing.Size(1353, 329);
            this.activeUntickedEmailGridControl.TabIndex = 4;
            this.activeUntickedEmailGridControl.Tag = "Active_UnTickedKeeper";
            this.activeUntickedEmailGridControl.UseEmbeddedNavigator = true;
            this.activeUntickedEmailGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.activeUntickedEmailGridView,
            this.gridView5});
            // 
            // bsAS11112KeeperActiveUnticked
            // 
            this.bsAS11112KeeperActiveUnticked.DataMember = "sp_AS_11112_Keeper_Email_Item";
            this.bsAS11112KeeperActiveUnticked.DataSource = this.dataSet_AS_DataEntry;
            // 
            // activeUntickedEmailGridView
            // 
            this.activeUntickedEmailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38});
            this.activeUntickedEmailGridView.GridControl = this.activeUntickedEmailGridControl;
            this.activeUntickedEmailGridView.Name = "activeUntickedEmailGridView";
            this.activeUntickedEmailGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.activeUntickedEmailGridView.OptionsFind.AlwaysVisible = true;
            this.activeUntickedEmailGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.activeUntickedEmailGridView.OptionsLayout.StoreAppearance = true;
            this.activeUntickedEmailGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.activeUntickedEmailGridView.OptionsSelection.MultiSelect = true;
            this.activeUntickedEmailGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.activeUntickedEmailGridView.OptionsView.ColumnAutoWidth = false;
            this.activeUntickedEmailGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.activeUntickedEmailGridView.OptionsView.ShowGroupPanel = false;
            this.activeUntickedEmailGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView_PopupMenuShowing);
            this.activeUntickedEmailGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.activeUntickedEmailGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.activeUntickedEmailGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // gridColumn19
            // 
            this.gridColumn19.FieldName = "EmailTrackerID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn19.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn19.Width = 98;
            // 
            // gridColumn20
            // 
            this.gridColumn20.ColumnEdit = this.ceSelected1;
            this.gridColumn20.FieldName = "SelectedForMail";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            this.gridColumn20.Width = 102;
            // 
            // ceSelected1
            // 
            this.ceSelected1.AutoHeight = false;
            this.ceSelected1.Caption = "Check";
            this.ceSelected1.Name = "ceSelected1";
            this.ceSelected1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceSelected_EditValueChanging);
            // 
            // gridColumn21
            // 
            this.gridColumn21.FieldName = "EmailTypeID";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn21.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn21.Width = 86;
            // 
            // gridColumn22
            // 
            this.gridColumn22.FieldName = "EmailType";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 1;
            // 
            // gridColumn23
            // 
            this.gridColumn23.FieldName = "EmailAddress";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 2;
            this.gridColumn23.Width = 163;
            // 
            // gridColumn24
            // 
            this.gridColumn24.FieldName = "FullName";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 3;
            this.gridColumn24.Width = 136;
            // 
            // gridColumn25
            // 
            this.gridColumn25.FieldName = "Emailable";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 6;
            // 
            // gridColumn26
            // 
            this.gridColumn26.FieldName = "KeeperTypeID";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn26.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 7;
            // 
            // gridColumn27
            // 
            this.gridColumn27.FieldName = "KeeperType";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 4;
            this.gridColumn27.Width = 168;
            // 
            // gridColumn28
            // 
            this.gridColumn28.FieldName = "EmployeeNumber";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Width = 107;
            // 
            // gridColumn29
            // 
            this.gridColumn29.FieldName = "UserType";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 9;
            this.gridColumn29.Width = 145;
            // 
            // gridColumn30
            // 
            this.gridColumn30.FieldName = "AssetList";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 8;
            this.gridColumn30.Width = 597;
            // 
            // gridColumn31
            // 
            this.gridColumn31.FieldName = "LastUpdated";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 11;
            this.gridColumn31.Width = 85;
            // 
            // gridColumn32
            // 
            this.gridColumn32.FieldName = "LastBatchEmail";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 98;
            // 
            // gridColumn33
            // 
            this.gridColumn33.FieldName = "EmailHasSendError";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 163;
            // 
            // gridColumn34
            // 
            this.gridColumn34.FieldName = "SendReturnMessage";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Width = 126;
            // 
            // gridColumn35
            // 
            this.gridColumn35.FieldName = "IsCurrentKeeper";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 10;
            this.gridColumn35.Width = 107;
            // 
            // gridColumn36
            // 
            this.gridColumn36.FieldName = "Mode";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn36.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // gridColumn37
            // 
            this.gridColumn37.FieldName = "RecordID";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn37.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // gridColumn38
            // 
            this.gridColumn38.ColumnEdit = this.hplActiveSuppressed;
            this.gridColumn38.FieldName = "TotalAssetCount";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 5;
            this.gridColumn38.Width = 107;
            // 
            // hplActiveSuppressed
            // 
            this.hplActiveSuppressed.AutoHeight = false;
            this.hplActiveSuppressed.Name = "hplActiveSuppressed";
            this.hplActiveSuppressed.SingleClick = true;
            this.hplActiveSuppressed.Click += new System.EventHandler(this.hplAssetList_Click);
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // gridView5
            // 
            this.gridView5.GridControl = this.activeUntickedEmailGridControl;
            this.gridView5.Name = "gridView5";
            // 
            // activeInvalidEmailTabPage
            // 
            this.activeInvalidEmailTabPage.Controls.Add(this.activeInvalidGridControl);
            this.activeInvalidEmailTabPage.Name = "activeInvalidEmailTabPage";
            this.activeInvalidEmailTabPage.Size = new System.Drawing.Size(1357, 606);
            this.activeInvalidEmailTabPage.Text = "Active Keeper (Invalid Emails)";
            // 
            // activeInvalidGridControl
            // 
            this.activeInvalidGridControl.DataSource = this.bsAS11112KeeperActiveInvalid;
            this.activeInvalidGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.activeInvalidGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.activeInvalidGridControl.Location = new System.Drawing.Point(0, 0);
            this.activeInvalidGridControl.MainView = this.activeInvalidGridView;
            this.activeInvalidGridControl.MenuManager = this.barManager1;
            this.activeInvalidGridControl.Name = "activeInvalidGridControl";
            this.activeInvalidGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.hplInvalidAssetCount,
            this.repositoryItemCheckEdit1});
            this.activeInvalidGridControl.Size = new System.Drawing.Size(1357, 606);
            this.activeInvalidGridControl.TabIndex = 4;
            this.activeInvalidGridControl.Tag = "Active_Keeper_Invalid_Email";
            this.activeInvalidGridControl.UseEmbeddedNavigator = true;
            this.activeInvalidGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.activeInvalidGridView,
            this.gridView4});
            // 
            // bsAS11112KeeperActiveInvalid
            // 
            this.bsAS11112KeeperActiveInvalid.DataMember = "sp_AS_11112_Keeper_Email_Item";
            this.bsAS11112KeeperActiveInvalid.DataSource = this.dataSet_AS_DataEntry;
            // 
            // activeInvalidGridView
            // 
            this.activeInvalidGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.activeInvalidGridView.GridControl = this.activeInvalidGridControl;
            this.activeInvalidGridView.Name = "activeInvalidGridView";
            this.activeInvalidGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.activeInvalidGridView.OptionsFind.AlwaysVisible = true;
            this.activeInvalidGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.activeInvalidGridView.OptionsLayout.StoreAppearance = true;
            this.activeInvalidGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.activeInvalidGridView.OptionsSelection.MultiSelect = true;
            this.activeInvalidGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.activeInvalidGridView.OptionsView.ColumnAutoWidth = false;
            this.activeInvalidGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.activeInvalidGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView_PopupMenuShowing);
            this.activeInvalidGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.activeInvalidGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.activeInvalidGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.FieldName = "EmailTrackerID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn1.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn1.Width = 98;
            // 
            // gridColumn2
            // 
            this.gridColumn2.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn2.FieldName = "SelectedForMail";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 102;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridColumn3
            // 
            this.gridColumn3.FieldName = "EmailTypeID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn3.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn3.Width = 86;
            // 
            // gridColumn4
            // 
            this.gridColumn4.FieldName = "EmailType";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            // 
            // gridColumn5
            // 
            this.gridColumn5.FieldName = "EmailAddress";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 163;
            // 
            // gridColumn6
            // 
            this.gridColumn6.FieldName = "FullName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            this.gridColumn6.Width = 136;
            // 
            // gridColumn7
            // 
            this.gridColumn7.FieldName = "KeeperType";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            this.gridColumn7.Width = 168;
            // 
            // gridColumn8
            // 
            this.gridColumn8.FieldName = "EmployeeNumber";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 107;
            // 
            // gridColumn9
            // 
            this.gridColumn9.FieldName = "UserType";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 7;
            this.gridColumn9.Width = 145;
            // 
            // gridColumn10
            // 
            this.gridColumn10.FieldName = "AssetList";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 6;
            this.gridColumn10.Width = 597;
            // 
            // gridColumn11
            // 
            this.gridColumn11.FieldName = "LastUpdated";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 9;
            this.gridColumn11.Width = 85;
            // 
            // gridColumn12
            // 
            this.gridColumn12.FieldName = "LastBatchEmail";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 98;
            // 
            // gridColumn13
            // 
            this.gridColumn13.FieldName = "EmailHasSendError";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 163;
            // 
            // gridColumn14
            // 
            this.gridColumn14.FieldName = "SendReturnMessage";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 126;
            // 
            // gridColumn15
            // 
            this.gridColumn15.FieldName = "IsCurrentKeeper";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 8;
            this.gridColumn15.Width = 107;
            // 
            // gridColumn16
            // 
            this.gridColumn16.FieldName = "Mode";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn16.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // gridColumn17
            // 
            this.gridColumn17.FieldName = "RecordID";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn17.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // gridColumn18
            // 
            this.gridColumn18.ColumnEdit = this.hplInvalidAssetCount;
            this.gridColumn18.FieldName = "TotalAssetCount";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 5;
            this.gridColumn18.Width = 107;
            // 
            // hplInvalidAssetCount
            // 
            this.hplInvalidAssetCount.AutoHeight = false;
            this.hplInvalidAssetCount.Name = "hplInvalidAssetCount";
            this.hplInvalidAssetCount.SingleClick = true;
            this.hplInvalidAssetCount.Click += new System.EventHandler(this.hplAssetList_Click);
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.activeInvalidGridControl;
            this.gridView4.Name = "gridView4";
            // 
            // activeKeeperTabPage
            // 
            this.activeKeeperTabPage.Controls.Add(this.splitContainerControl1);
            this.activeKeeperTabPage.Name = "activeKeeperTabPage";
            this.activeKeeperTabPage.Size = new System.Drawing.Size(1357, 606);
            this.activeKeeperTabPage.Text = "Active Keeper (Valid Emails)";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.btnMoveUP);
            this.splitContainerControl1.Panel1.Controls.Add(this.btnDown);
            this.splitContainerControl1.Panel1.Controls.Add(this.activeEmailGridControl);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Active Keeper";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.activeUntickedEmailGridControl);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Active Unticked Keeper";
            this.splitContainerControl1.Size = new System.Drawing.Size(1357, 606);
            this.splitContainerControl1.SplitterPosition = 247;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // btnMoveUP
            // 
            this.btnMoveUP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMoveUP.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveUP.ImageOptions.Image")));
            this.btnMoveUP.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnMoveUP.Location = new System.Drawing.Point(700, 199);
            this.btnMoveUP.Name = "btnMoveUP";
            this.btnMoveUP.Size = new System.Drawing.Size(32, 23);
            this.btnMoveUP.TabIndex = 8;
            this.btnMoveUP.Click += new System.EventHandler(this.btnMoveUP_Click);
            // 
            // btnDown
            // 
            this.btnDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDown.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDown.ImageOptions.Image")));
            this.btnDown.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnDown.Location = new System.Drawing.Point(662, 199);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(32, 23);
            this.btnDown.TabIndex = 4;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // activeEmailGridControl
            // 
            this.activeEmailGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.activeEmailGridControl.DataSource = this.bsAS11112KeeperActiveValid;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.activeEmailGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.activeEmailGridControl.Location = new System.Drawing.Point(3, 0);
            this.activeEmailGridControl.MainView = this.activeEmailGridView;
            this.activeEmailGridControl.MenuManager = this.barManager1;
            this.activeEmailGridControl.Name = "activeEmailGridControl";
            this.activeEmailGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.hplAssetList,
            this.ceSelected});
            this.activeEmailGridControl.Size = new System.Drawing.Size(1350, 196);
            this.activeEmailGridControl.TabIndex = 3;
            this.activeEmailGridControl.Tag = "Active_Keeper_Valid_Email";
            this.activeEmailGridControl.UseEmbeddedNavigator = true;
            this.activeEmailGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.activeEmailGridView,
            this.gridView1});
            // 
            // activeEmailGridView
            // 
            this.activeEmailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmailTrackerID,
            this.colSelectedForMail,
            this.colEmailTypeID,
            this.colEmailType,
            this.colEmailAddress,
            this.colFullName,
            this.colEmailable,
            this.colKeeperTypeID1,
            this.colKeeperType,
            this.colEmployeeNumber,
            this.colUserType,
            this.colAssetList,
            this.colLastUpdated,
            this.colLastBatchEmail,
            this.colEmailHasSendError,
            this.colSendReturnMessage,
            this.colIsCurrentKeeper,
            this.colMode,
            this.colRecordID,
            this.colTotalAssetCount});
            this.activeEmailGridView.GridControl = this.activeEmailGridControl;
            this.activeEmailGridView.Name = "activeEmailGridView";
            this.activeEmailGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.activeEmailGridView.OptionsFind.AlwaysVisible = true;
            this.activeEmailGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.activeEmailGridView.OptionsLayout.StoreAppearance = true;
            this.activeEmailGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.activeEmailGridView.OptionsSelection.MultiSelect = true;
            this.activeEmailGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.activeEmailGridView.OptionsView.ColumnAutoWidth = false;
            this.activeEmailGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.activeEmailGridView.OptionsView.ShowGroupPanel = false;
            this.activeEmailGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView_PopupMenuShowing);
            this.activeEmailGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.activeEmailGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.activeEmailGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEmailTrackerID
            // 
            this.colEmailTrackerID.FieldName = "EmailTrackerID";
            this.colEmailTrackerID.Name = "colEmailTrackerID";
            this.colEmailTrackerID.OptionsColumn.AllowEdit = false;
            this.colEmailTrackerID.OptionsColumn.AllowFocus = false;
            this.colEmailTrackerID.OptionsColumn.ReadOnly = true;
            this.colEmailTrackerID.OptionsColumn.ShowInCustomizationForm = false;
            this.colEmailTrackerID.OptionsColumn.ShowInExpressionEditor = false;
            this.colEmailTrackerID.Width = 98;
            // 
            // colSelectedForMail
            // 
            this.colSelectedForMail.ColumnEdit = this.ceSelected;
            this.colSelectedForMail.FieldName = "SelectedForMail";
            this.colSelectedForMail.Name = "colSelectedForMail";
            this.colSelectedForMail.Visible = true;
            this.colSelectedForMail.VisibleIndex = 0;
            this.colSelectedForMail.Width = 102;
            // 
            // ceSelected
            // 
            this.ceSelected.AutoHeight = false;
            this.ceSelected.Caption = "Check";
            this.ceSelected.Name = "ceSelected";
            this.ceSelected.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceSelected_EditValueChanging);
            // 
            // colEmailTypeID
            // 
            this.colEmailTypeID.FieldName = "EmailTypeID";
            this.colEmailTypeID.Name = "colEmailTypeID";
            this.colEmailTypeID.Width = 86;
            // 
            // colEmailType
            // 
            this.colEmailType.FieldName = "EmailType";
            this.colEmailType.Name = "colEmailType";
            this.colEmailType.OptionsColumn.AllowEdit = false;
            this.colEmailType.OptionsColumn.AllowFocus = false;
            this.colEmailType.OptionsColumn.ReadOnly = true;
            this.colEmailType.OptionsColumn.ShowInCustomizationForm = false;
            this.colEmailType.OptionsColumn.ShowInExpressionEditor = false;
            this.colEmailType.Visible = true;
            this.colEmailType.VisibleIndex = 1;
            this.colEmailType.Width = 152;
            // 
            // colEmailAddress
            // 
            this.colEmailAddress.FieldName = "EmailAddress";
            this.colEmailAddress.Name = "colEmailAddress";
            this.colEmailAddress.OptionsColumn.AllowEdit = false;
            this.colEmailAddress.OptionsColumn.AllowFocus = false;
            this.colEmailAddress.OptionsColumn.ReadOnly = true;
            this.colEmailAddress.Visible = true;
            this.colEmailAddress.VisibleIndex = 2;
            this.colEmailAddress.Width = 163;
            // 
            // colFullName
            // 
            this.colFullName.FieldName = "FullName";
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.AllowEdit = false;
            this.colFullName.OptionsColumn.AllowFocus = false;
            this.colFullName.OptionsColumn.ReadOnly = true;
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 3;
            this.colFullName.Width = 203;
            // 
            // colEmailable
            // 
            this.colEmailable.FieldName = "Emailable";
            this.colEmailable.Name = "colEmailable";
            this.colEmailable.OptionsColumn.AllowEdit = false;
            this.colEmailable.OptionsColumn.AllowFocus = false;
            this.colEmailable.OptionsColumn.ReadOnly = true;
            this.colEmailable.Visible = true;
            this.colEmailable.VisibleIndex = 6;
            // 
            // colKeeperTypeID1
            // 
            this.colKeeperTypeID1.FieldName = "KeeperTypeID";
            this.colKeeperTypeID1.Name = "colKeeperTypeID1";
            this.colKeeperTypeID1.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID1.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID1.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperTypeID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperTypeID1.Width = 120;
            // 
            // colKeeperType
            // 
            this.colKeeperType.FieldName = "KeeperType";
            this.colKeeperType.Name = "colKeeperType";
            this.colKeeperType.OptionsColumn.AllowEdit = false;
            this.colKeeperType.OptionsColumn.AllowFocus = false;
            this.colKeeperType.OptionsColumn.ReadOnly = true;
            this.colKeeperType.Visible = true;
            this.colKeeperType.VisibleIndex = 4;
            this.colKeeperType.Width = 168;
            // 
            // colEmployeeNumber
            // 
            this.colEmployeeNumber.FieldName = "EmployeeNumber";
            this.colEmployeeNumber.Name = "colEmployeeNumber";
            this.colEmployeeNumber.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber.Width = 107;
            // 
            // colUserType
            // 
            this.colUserType.FieldName = "UserType";
            this.colUserType.Name = "colUserType";
            this.colUserType.OptionsColumn.AllowEdit = false;
            this.colUserType.OptionsColumn.AllowFocus = false;
            this.colUserType.OptionsColumn.ReadOnly = true;
            this.colUserType.Visible = true;
            this.colUserType.VisibleIndex = 8;
            this.colUserType.Width = 145;
            // 
            // colAssetList
            // 
            this.colAssetList.FieldName = "AssetList";
            this.colAssetList.Name = "colAssetList";
            this.colAssetList.OptionsColumn.AllowEdit = false;
            this.colAssetList.OptionsColumn.AllowFocus = false;
            this.colAssetList.OptionsColumn.ReadOnly = true;
            this.colAssetList.Visible = true;
            this.colAssetList.VisibleIndex = 7;
            this.colAssetList.Width = 597;
            // 
            // colLastUpdated
            // 
            this.colLastUpdated.FieldName = "LastUpdated";
            this.colLastUpdated.Name = "colLastUpdated";
            this.colLastUpdated.OptionsColumn.AllowEdit = false;
            this.colLastUpdated.OptionsColumn.AllowFocus = false;
            this.colLastUpdated.OptionsColumn.ReadOnly = true;
            this.colLastUpdated.Visible = true;
            this.colLastUpdated.VisibleIndex = 10;
            this.colLastUpdated.Width = 85;
            // 
            // colLastBatchEmail
            // 
            this.colLastBatchEmail.FieldName = "LastBatchEmail";
            this.colLastBatchEmail.Name = "colLastBatchEmail";
            this.colLastBatchEmail.OptionsColumn.AllowEdit = false;
            this.colLastBatchEmail.OptionsColumn.AllowFocus = false;
            this.colLastBatchEmail.OptionsColumn.ReadOnly = true;
            this.colLastBatchEmail.Width = 98;
            // 
            // colEmailHasSendError
            // 
            this.colEmailHasSendError.FieldName = "EmailHasSendError";
            this.colEmailHasSendError.Name = "colEmailHasSendError";
            this.colEmailHasSendError.OptionsColumn.AllowEdit = false;
            this.colEmailHasSendError.OptionsColumn.AllowFocus = false;
            this.colEmailHasSendError.OptionsColumn.ReadOnly = true;
            this.colEmailHasSendError.Width = 163;
            // 
            // colSendReturnMessage
            // 
            this.colSendReturnMessage.FieldName = "SendReturnMessage";
            this.colSendReturnMessage.Name = "colSendReturnMessage";
            this.colSendReturnMessage.OptionsColumn.AllowEdit = false;
            this.colSendReturnMessage.OptionsColumn.AllowFocus = false;
            this.colSendReturnMessage.OptionsColumn.ReadOnly = true;
            this.colSendReturnMessage.Width = 126;
            // 
            // colIsCurrentKeeper
            // 
            this.colIsCurrentKeeper.FieldName = "IsCurrentKeeper";
            this.colIsCurrentKeeper.Name = "colIsCurrentKeeper";
            this.colIsCurrentKeeper.OptionsColumn.AllowEdit = false;
            this.colIsCurrentKeeper.OptionsColumn.AllowFocus = false;
            this.colIsCurrentKeeper.OptionsColumn.ReadOnly = true;
            this.colIsCurrentKeeper.Visible = true;
            this.colIsCurrentKeeper.VisibleIndex = 9;
            this.colIsCurrentKeeper.Width = 107;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colTotalAssetCount
            // 
            this.colTotalAssetCount.ColumnEdit = this.hplAssetList;
            this.colTotalAssetCount.FieldName = "TotalAssetCount";
            this.colTotalAssetCount.Name = "colTotalAssetCount";
            this.colTotalAssetCount.Visible = true;
            this.colTotalAssetCount.VisibleIndex = 5;
            this.colTotalAssetCount.Width = 107;
            // 
            // hplAssetList
            // 
            this.hplAssetList.AutoHeight = false;
            this.hplAssetList.Name = "hplAssetList";
            this.hplAssetList.SingleClick = true;
            this.hplAssetList.Click += new System.EventHandler(this.hplAssetList_Click);
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.activeEmailGridControl;
            this.gridView1.Name = "gridView1";
            // 
            // keeperListPage
            // 
            this.keeperListPage.Controls.Add(this.keeperGridControl);
            this.keeperListPage.Name = "keeperListPage";
            this.keeperListPage.Size = new System.Drawing.Size(1357, 606);
            this.keeperListPage.Text = "Keeper List";
            // 
            // keeperGridControl
            // 
            this.keeperGridControl.DataSource = this.spAS11111KeeperListBindingSource;
            this.keeperGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.keeperGridControl.Location = new System.Drawing.Point(0, 0);
            this.keeperGridControl.MainView = this.keeperGridView;
            this.keeperGridControl.MenuManager = this.barManager1;
            this.keeperGridControl.Name = "keeperGridControl";
            this.keeperGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEditDate,
            this.ArchiveCheckEdit,
            this.moneyTextEdit2,
            this.unresolvedCheckEdit});
            this.keeperGridControl.Size = new System.Drawing.Size(1357, 606);
            this.keeperGridControl.TabIndex = 2;
            this.keeperGridControl.Tag = "Keeper_List";
            this.keeperGridControl.UseEmbeddedNavigator = true;
            this.keeperGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.keeperGridView,
            this.gridView2});
            // 
            // keeperGridView
            // 
            this.keeperGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKeeperAllocationID,
            this.colKeeperTypeID,
            this.colKeeperType1,
            this.colEmployeeNumber1,
            this.colFullName1,
            this.colAllocationStatusID,
            this.colAllocationStatus,
            this.colAllocationDate,
            this.colAllocationEndDate,
            this.colUserType1,
            this.colEmail,
            this.colAssetMiniDescription,
            this.colAssetDetailedDescription,
            this.colLinkedDocumentCount});
            this.keeperGridView.GridControl = this.keeperGridControl;
            this.keeperGridView.Name = "keeperGridView";
            this.keeperGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.keeperGridView.OptionsFind.AlwaysVisible = true;
            this.keeperGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.keeperGridView.OptionsLayout.StoreAppearance = true;
            this.keeperGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.keeperGridView.OptionsSelection.MultiSelect = true;
            this.keeperGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.keeperGridView.OptionsView.ColumnAutoWidth = false;
            this.keeperGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.keeperGridView.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.keeperGridView_CustomRowCellEdit);
            this.keeperGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView_PopupMenuShowing);
            this.keeperGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.keeperGridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.keeperGridView_ShowingEditor);
            this.keeperGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.keeperGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colKeeperAllocationID
            // 
            this.colKeeperAllocationID.FieldName = "KeeperAllocationID";
            this.colKeeperAllocationID.Name = "colKeeperAllocationID";
            this.colKeeperAllocationID.OptionsColumn.AllowEdit = false;
            this.colKeeperAllocationID.OptionsColumn.AllowFocus = false;
            this.colKeeperAllocationID.OptionsColumn.ReadOnly = true;
            this.colKeeperAllocationID.Width = 118;
            // 
            // colKeeperTypeID
            // 
            this.colKeeperTypeID.FieldName = "KeeperTypeID";
            this.colKeeperTypeID.Name = "colKeeperTypeID";
            this.colKeeperTypeID.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID.Width = 96;
            // 
            // colKeeperType1
            // 
            this.colKeeperType1.FieldName = "KeeperType";
            this.colKeeperType1.Name = "colKeeperType1";
            this.colKeeperType1.OptionsColumn.AllowEdit = false;
            this.colKeeperType1.OptionsColumn.AllowFocus = false;
            this.colKeeperType1.OptionsColumn.ReadOnly = true;
            this.colKeeperType1.Visible = true;
            this.colKeeperType1.VisibleIndex = 0;
            this.colKeeperType1.Width = 82;
            // 
            // colEmployeeNumber1
            // 
            this.colEmployeeNumber1.FieldName = "EmployeeNumber";
            this.colEmployeeNumber1.Name = "colEmployeeNumber1";
            this.colEmployeeNumber1.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber1.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber1.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber1.Width = 107;
            // 
            // colFullName1
            // 
            this.colFullName1.FieldName = "FullName";
            this.colFullName1.Name = "colFullName1";
            this.colFullName1.OptionsColumn.AllowEdit = false;
            this.colFullName1.OptionsColumn.AllowFocus = false;
            this.colFullName1.OptionsColumn.ReadOnly = true;
            this.colFullName1.Visible = true;
            this.colFullName1.VisibleIndex = 1;
            this.colFullName1.Width = 242;
            // 
            // colAllocationStatusID
            // 
            this.colAllocationStatusID.FieldName = "AllocationStatusID";
            this.colAllocationStatusID.Name = "colAllocationStatusID";
            this.colAllocationStatusID.OptionsColumn.AllowEdit = false;
            this.colAllocationStatusID.OptionsColumn.AllowFocus = false;
            this.colAllocationStatusID.OptionsColumn.ReadOnly = true;
            this.colAllocationStatusID.Width = 115;
            // 
            // colAllocationStatus
            // 
            this.colAllocationStatus.FieldName = "AllocationStatus";
            this.colAllocationStatus.Name = "colAllocationStatus";
            this.colAllocationStatus.OptionsColumn.AllowEdit = false;
            this.colAllocationStatus.OptionsColumn.AllowFocus = false;
            this.colAllocationStatus.OptionsColumn.ReadOnly = true;
            this.colAllocationStatus.Visible = true;
            this.colAllocationStatus.VisibleIndex = 2;
            this.colAllocationStatus.Width = 101;
            // 
            // colAllocationDate
            // 
            this.colAllocationDate.FieldName = "AllocationDate";
            this.colAllocationDate.Name = "colAllocationDate";
            this.colAllocationDate.OptionsColumn.AllowEdit = false;
            this.colAllocationDate.OptionsColumn.AllowFocus = false;
            this.colAllocationDate.OptionsColumn.ReadOnly = true;
            this.colAllocationDate.Visible = true;
            this.colAllocationDate.VisibleIndex = 3;
            this.colAllocationDate.Width = 93;
            // 
            // colAllocationEndDate
            // 
            this.colAllocationEndDate.FieldName = "AllocationEndDate";
            this.colAllocationEndDate.Name = "colAllocationEndDate";
            this.colAllocationEndDate.OptionsColumn.AllowEdit = false;
            this.colAllocationEndDate.OptionsColumn.AllowFocus = false;
            this.colAllocationEndDate.OptionsColumn.ReadOnly = true;
            this.colAllocationEndDate.Visible = true;
            this.colAllocationEndDate.VisibleIndex = 4;
            this.colAllocationEndDate.Width = 114;
            // 
            // colUserType1
            // 
            this.colUserType1.FieldName = "UserType";
            this.colUserType1.Name = "colUserType1";
            this.colUserType1.OptionsColumn.AllowEdit = false;
            this.colUserType1.OptionsColumn.AllowFocus = false;
            this.colUserType1.OptionsColumn.ReadOnly = true;
            this.colUserType1.Visible = true;
            this.colUserType1.VisibleIndex = 5;
            this.colUserType1.Width = 153;
            // 
            // colEmail
            // 
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.AllowEdit = false;
            this.colEmail.OptionsColumn.AllowFocus = false;
            this.colEmail.OptionsColumn.ReadOnly = true;
            this.colEmail.Visible = true;
            this.colEmail.VisibleIndex = 6;
            this.colEmail.Width = 164;
            // 
            // colAssetMiniDescription
            // 
            this.colAssetMiniDescription.FieldName = "AssetMiniDescription";
            this.colAssetMiniDescription.Name = "colAssetMiniDescription";
            this.colAssetMiniDescription.OptionsColumn.AllowEdit = false;
            this.colAssetMiniDescription.OptionsColumn.AllowFocus = false;
            this.colAssetMiniDescription.OptionsColumn.ReadOnly = true;
            this.colAssetMiniDescription.Visible = true;
            this.colAssetMiniDescription.VisibleIndex = 7;
            this.colAssetMiniDescription.Width = 376;
            // 
            // colAssetDetailedDescription
            // 
            this.colAssetDetailedDescription.FieldName = "AssetDetailedDescription";
            this.colAssetDetailedDescription.Name = "colAssetDetailedDescription";
            this.colAssetDetailedDescription.OptionsColumn.AllowEdit = false;
            this.colAssetDetailedDescription.OptionsColumn.AllowFocus = false;
            this.colAssetDetailedDescription.OptionsColumn.ReadOnly = true;
            this.colAssetDetailedDescription.Visible = true;
            this.colAssetDetailedDescription.VisibleIndex = 8;
            this.colAssetDetailedDescription.Width = 757;
            // 
            // colLinkedDocumentCount
            // 
            this.colLinkedDocumentCount.Caption = "LinkedDocuments";
            this.colLinkedDocumentCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedDocumentCount.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount.Name = "colLinkedDocumentCount";
            this.colLinkedDocumentCount.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount.Visible = true;
            this.colLinkedDocumentCount.VisibleIndex = 9;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "d";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // ArchiveCheckEdit
            // 
            this.ArchiveCheckEdit.AutoHeight = false;
            this.ArchiveCheckEdit.Caption = "Check";
            this.ArchiveCheckEdit.Name = "ArchiveCheckEdit";
            // 
            // moneyTextEdit2
            // 
            this.moneyTextEdit2.AutoHeight = false;
            this.moneyTextEdit2.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit2.Name = "moneyTextEdit2";
            // 
            // unresolvedCheckEdit
            // 
            this.unresolvedCheckEdit.AutoHeight = false;
            this.unresolvedCheckEdit.Caption = "Check";
            this.unresolvedCheckEdit.Name = "unresolvedCheckEdit";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.keeperGridControl;
            this.gridView2.Name = "gridView2";
            // 
            // mailTabControl
            // 
            this.mailTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mailTabControl.Location = new System.Drawing.Point(0, 0);
            this.mailTabControl.Name = "mailTabControl";
            this.mailTabControl.SelectedTabPage = this.keeperListPage;
            this.mailTabControl.Size = new System.Drawing.Size(1362, 632);
            this.mailTabControl.TabIndex = 6;
            this.mailTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.keeperListPage,
            this.activeKeeperTabPage,
            this.activeInvalidEmailTabPage});
            this.mailTabControl.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.mailTabControl_SelectedPageChanged);
            // 
            // spAS11111RetrieveKeeperListBindingSource
            // 
            this.spAS11111RetrieveKeeperListBindingSource.DataMember = "sp_AS_11111_Retrieve_Keeper_List";
            this.spAS11111RetrieveKeeperListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11111_Retrieve_Keeper_ListTableAdapter
            // 
            this.sp_AS_11111_Retrieve_Keeper_ListTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // sp_AS_11000_PL_Email_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Email_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11102_Rental_ManagerTableAdapter1
            // 
            this.sp_AS_11102_Rental_ManagerTableAdapter1.ClearBeforeFill = true;
            // 
            // spAS11157EmailScheduleItemBindingSource
            // 
            this.spAS11157EmailScheduleItemBindingSource.DataMember = "sp_AS_11157_Email_Schedule_Item";
            this.spAS11157EmailScheduleItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11157_Email_Schedule_ItemTableAdapter
            // 
            this.sp_AS_11157_Email_Schedule_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.TimeEditStyle = DevExpress.XtraEditors.Repository.TimeEditStyle.TouchUI;
            this.repositoryItemDateEdit2.MinValue = new System.DateTime(2014, 8, 1, 10, 56, 41, 0);
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // frm_AS_Keeper_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1362, 632);
            this.Controls.Add(this.mailTabControl);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Keeper_Manager";
            this.Text = "Keeper Manager";
            this.Activated += new System.EventHandler(this.frm_AS_Keeper_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Keeper_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.mailTabControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11111KeeperListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAS11112KeeperActiveValid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.activeUntickedEmailGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAS11112KeeperActiveUnticked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.activeUntickedEmailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSelected1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hplActiveSuppressed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            this.activeInvalidEmailTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.activeInvalidGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAS11112KeeperActiveInvalid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.activeInvalidGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hplInvalidAssetCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.activeKeeperTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.activeEmailGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.activeEmailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hplAssetList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.keeperListPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArchiveCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unresolvedCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mailTabControl)).EndInit();
            this.mailTabControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spAS11111RetrieveKeeperListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11157EmailScheduleItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_AS_CoreTableAdapters.TableAdapterManager tableAdapterManager;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager1;
        private DevExpress.XtraBars.Bar bar;
        private DevExpress.XtraBars.BarButtonItem bbiSendMail;
        private System.Windows.Forms.BindingSource bsAS11112KeeperActiveValid;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11112_Keeper_Email_ItemTableAdapter sp_AS_11112_Keeper_Email_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11111KeeperListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11111_Keeper_ListTableAdapter sp_AS_11111_Keeper_ListTableAdapter;
        private DevExpress.XtraTab.XtraTabPage activeInvalidEmailTabPage;
        private DevExpress.XtraTab.XtraTabPage activeKeeperTabPage;
        private DevExpress.XtraGrid.GridControl activeEmailGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView activeEmailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailTrackerID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelectedForMail;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailType;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colUserType;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetList;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdated;
        private DevExpress.XtraGrid.Columns.GridColumn colLastBatchEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailHasSendError;
        private DevExpress.XtraGrid.Columns.GridColumn colSendReturnMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colIsCurrentKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAssetCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit hplAssetList;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceSelected;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabPage keeperListPage;
        private DevExpress.XtraGrid.GridControl keeperGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView keeperGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAllocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName1;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUserType1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetMiniDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAssetDetailedDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ArchiveCheckEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit unresolvedCheckEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraTab.XtraTabControl mailTabControl;
        private DevExpress.XtraGrid.GridControl activeInvalidGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView activeInvalidGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit hplInvalidAssetCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.BindingSource spAS11111RetrieveKeeperListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11111_Retrieve_Keeper_ListTableAdapter sp_AS_11111_Retrieve_Keeper_ListTableAdapter;
        private System.Windows.Forms.BindingSource bsAS11112KeeperActiveInvalid;
        private DevExpress.XtraBars.BarButtonItem bbiFindNewKeeper;
        private DevExpress.XtraBars.BarButtonItem bbiReset;
        private System.Windows.Forms.BindingSource bsAS11112KeeperActiveUnticked;
        private DevExpress.XtraGrid.GridControl activeUntickedEmailGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView activeUntickedEmailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceSelected1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit hplActiveSuppressed;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailable;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID1;
        private DevExpress.XtraBars.BarButtonItem bbiExportExcelOld;
        private DevExpress.XtraBars.BarButtonItem bbiExportExcel;
        private DevExpress.XtraBars.BarEditItem bbiAutoSend;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraBars.BarEditItem bbiNextEmailDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Email_TypeTableAdapter sp_AS_11000_PL_Email_TypeTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiClearFilter;
        private DevExpress.XtraBars.BarSubItem barSubSendMail;
        private DevExpress.XtraBars.BarButtonItem bbiSendSelected;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshAll;
        private DataSet_ATTableAdapters.QueriesTableAdapter queriesTableAdapter1;
        private DataSet_AS_CoreTableAdapters.sp_AS_11102_Rental_ManagerTableAdapter sp_AS_11102_Rental_ManagerTableAdapter1;
        private System.Windows.Forms.BindingSource spAS11157EmailScheduleItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11157_Email_Schedule_ItemTableAdapter sp_AS_11157_Email_Schedule_ItemTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnDown;
        private DevExpress.XtraEditors.SimpleButton btnMoveUP;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount;
    }
}

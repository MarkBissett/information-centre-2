﻿namespace WoodPlan5
{
    partial class frm_AS_Road_Tax_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Road_Tax_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11096RoadTaxItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.deTaxExpiryDate = new DevExpress.XtraEditors.DateEdit();
            this.lueTaxPeriodID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLGeneralBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnAmount = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTaxPeriodID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTaxExpiryDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp_AS_11096_Road_Tax_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11096_Road_Tax_ItemTableAdapter();
            this.sp_AS_11000_PL_GeneralTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_GeneralTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11096RoadTaxItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTaxExpiryDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTaxExpiryDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTaxPeriodID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLGeneralBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaxPeriodID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaxExpiryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(797, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 454);
            this.barDockControlBottom.Size = new System.Drawing.Size(797, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 428);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(797, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 428);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "editFormLayoutControlGroup";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(797, 428);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(777, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11096RoadTaxItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(12, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // spAS11096RoadTaxItemBindingSource
            // 
            this.spAS11096RoadTaxItemBindingSource.DataMember = "sp_AS_11096_Road_Tax_Item";
            this.spAS11096RoadTaxItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEquipmentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deTaxExpiryDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueTaxPeriodID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnAmount);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11096RoadTaxItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1935, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(797, 428);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11096RoadTaxItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(125, 35);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(271, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling1);
            this.txtEquipmentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEquipmentReference.TabIndex = 123;
            // 
            // deTaxExpiryDate
            // 
            this.deTaxExpiryDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11096RoadTaxItemBindingSource, "TaxExpiryDate", true));
            this.deTaxExpiryDate.EditValue = null;
            this.deTaxExpiryDate.Location = new System.Drawing.Point(513, 35);
            this.deTaxExpiryDate.MenuManager = this.barManager1;
            this.deTaxExpiryDate.Name = "deTaxExpiryDate";
            this.deTaxExpiryDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deTaxExpiryDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deTaxExpiryDate.Size = new System.Drawing.Size(272, 20);
            this.deTaxExpiryDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deTaxExpiryDate.TabIndex = 124;
            this.deTaxExpiryDate.Tag = "Tax Expiry Date";
            this.deTaxExpiryDate.Validating += new System.ComponentModel.CancelEventHandler(this.deTaxExpiryDate_Validating);
            // 
            // lueTaxPeriodID
            // 
            this.lueTaxPeriodID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11096RoadTaxItemBindingSource, "TaxPeriodID", true));
            this.lueTaxPeriodID.Location = new System.Drawing.Point(125, 59);
            this.lueTaxPeriodID.MenuManager = this.barManager1;
            this.lueTaxPeriodID.Name = "lueTaxPeriodID";
            this.lueTaxPeriodID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTaxPeriodID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Tax Period", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueTaxPeriodID.Properties.DataSource = this.spAS11000PLGeneralBindingSource;
            this.lueTaxPeriodID.Properties.DisplayMember = "Value";
            this.lueTaxPeriodID.Properties.NullText = "";
            this.lueTaxPeriodID.Properties.NullValuePrompt = "-- Please Select Tax Period --";
            this.lueTaxPeriodID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueTaxPeriodID.Properties.ValueMember = "PickListID";
            this.lueTaxPeriodID.Size = new System.Drawing.Size(271, 20);
            this.lueTaxPeriodID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueTaxPeriodID.TabIndex = 126;
            this.lueTaxPeriodID.Tag = "Tax Period";
            this.lueTaxPeriodID.Validating += new System.ComponentModel.CancelEventHandler(this.lueTaxPeriodID_Validating);
            // 
            // spAS11000PLGeneralBindingSource
            // 
            this.spAS11000PLGeneralBindingSource.DataMember = "sp_AS_11000_PL_General";
            this.spAS11000PLGeneralBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnAmount
            // 
            this.spnAmount.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11096RoadTaxItemBindingSource, "Amount", true));
            this.spnAmount.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnAmount.Location = new System.Drawing.Point(513, 59);
            this.spnAmount.MenuManager = this.barManager1;
            this.spnAmount.Name = "spnAmount";
            this.spnAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnAmount.Properties.DisplayFormat.FormatString = "c2";
            this.spnAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnAmount.Properties.MaxValue = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.spnAmount.Size = new System.Drawing.Size(272, 20);
            this.spnAmount.StyleController = this.editFormDataLayoutControlGroup;
            this.spnAmount.TabIndex = 127;
            this.spnAmount.Tag = "Amount";
            this.spnAmount.Validating += new System.ComponentModel.CancelEventHandler(this.spnAmount_Validating);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEquipmentReference,
            this.ItemForTaxPeriodID,
            this.ItemForTaxExpiryDate,
            this.ItemForAmount});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(777, 385);
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.txtEquipmentReference;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference :";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(388, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference :";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForTaxPeriodID
            // 
            this.ItemForTaxPeriodID.Control = this.lueTaxPeriodID;
            this.ItemForTaxPeriodID.CustomizationFormText = "Tax Period :";
            this.ItemForTaxPeriodID.Location = new System.Drawing.Point(0, 24);
            this.ItemForTaxPeriodID.Name = "ItemForTaxPeriodID";
            this.ItemForTaxPeriodID.Size = new System.Drawing.Size(388, 361);
            this.ItemForTaxPeriodID.Text = "Tax Period :";
            this.ItemForTaxPeriodID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForTaxExpiryDate
            // 
            this.ItemForTaxExpiryDate.Control = this.deTaxExpiryDate;
            this.ItemForTaxExpiryDate.CustomizationFormText = "Tax Expiry Date :";
            this.ItemForTaxExpiryDate.Location = new System.Drawing.Point(388, 0);
            this.ItemForTaxExpiryDate.Name = "ItemForTaxExpiryDate";
            this.ItemForTaxExpiryDate.Size = new System.Drawing.Size(389, 24);
            this.ItemForTaxExpiryDate.Text = "Tax Expiry Date :";
            this.ItemForTaxExpiryDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForAmount
            // 
            this.ItemForAmount.Control = this.spnAmount;
            this.ItemForAmount.CustomizationFormText = "Amount :";
            this.ItemForAmount.Location = new System.Drawing.Point(388, 24);
            this.ItemForAmount.Name = "ItemForAmount";
            this.ItemForAmount.Size = new System.Drawing.Size(389, 361);
            this.ItemForAmount.Text = "Amount :";
            this.ItemForAmount.TextSize = new System.Drawing.Size(110, 13);
            // 
            // sp_AS_11096_Road_Tax_ItemTableAdapter
            // 
            this.sp_AS_11096_Road_Tax_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_GeneralTableAdapter
            // 
            this.sp_AS_11000_PL_GeneralTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Road_Tax_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(797, 484);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Road_Tax_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Road Tax";
            this.Activated += new System.EventHandler(this.frm_AS_Road_Tax_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Road_Tax_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Road_Tax_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11096RoadTaxItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTaxExpiryDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTaxExpiryDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTaxPeriodID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLGeneralBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaxPeriodID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTaxExpiryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAmount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private System.Windows.Forms.BindingSource spAS11096RoadTaxItemBindingSource;
        private DevExpress.XtraEditors.DateEdit deTaxExpiryDate;
        private DevExpress.XtraEditors.LookUpEdit lueTaxPeriodID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTaxExpiryDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTaxPeriodID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11096_Road_Tax_ItemTableAdapter sp_AS_11096_Road_Tax_ItemTableAdapter;
        private DevExpress.XtraEditors.SpinEdit spnAmount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAmount;
        private System.Windows.Forms.BindingSource spAS11000PLGeneralBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_GeneralTableAdapter sp_AS_11000_PL_GeneralTableAdapter;


    }
}

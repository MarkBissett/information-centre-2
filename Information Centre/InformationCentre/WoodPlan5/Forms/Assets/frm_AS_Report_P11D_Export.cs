using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.Spreadsheet;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using System.IO;

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_AS_Report_P11D_Export : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInRecordIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;
        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;
        private string i_str_selected_department_ids = "";
        private string i_str_selected_departments = "";
        private string i_str_selected_employee_ids = "";
        private string i_str_selected_employees = "";

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState5;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        string _strExportPath = "";

        #endregion

        public frm_AS_Report_P11D_Export()
        {
            InitializeComponent();
        }

        private void frm_AS_Report_P11D_Export_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 99001;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            //dateEditStart.DateTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month - 1, 15);  // Last Month on 15th //
            //dateEditStart.DateTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 15).AddMonths(-1);  // Last Month on 15th //
            //dateEditEnd.DateTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month)).AddDays(1).AddMilliseconds(-1);  // Last day of this month //

            //Date range is tax year 4/5/year - 1 to current yesr
            dateEditStart.DateTime = new DateTime(DateTime.Today.Year - 1, 4, 5);
            dateEditEnd.DateTime = new DateTime(DateTime.Today.Year, 4, 5);

            beiDateRange.EditValue = PopupContainerEdit1_Get_Selected();

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            sp_AS_11174_P11D_RecordedTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11175_P11D_KeepersTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11176_P11D_CombinedTableAdapter.Connection.ConnectionString = strConnectionString;

            RefreshGridViewState1 = new RefreshGridState(gridView1, "P11DID");
            gridControl1.ForceInitialize();

            RefreshGridViewState3 = new RefreshGridState(gridView3, "P11DID");
            gridControl3.ForceInitialize();

            RefreshGridViewState5 = new RefreshGridState(gridView5, "P11DID");
            gridControl5.ForceInitialize();




            if (strPassedInRecordIDs != "")  // Opened in drill-down mode //
            {
                beiDateRange.EditValue = "Custom Filter";
                Load_Data();  // Load records //
            }

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _strExportPath = GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AS_P11DReportExportSavedFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Report Export Spreadsheet Path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Export Spreadsheet Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (string.IsNullOrWhiteSpace(_strExportPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The P11D Report Export Spreadsheet Path has not been set (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Export Spreadsheet Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                if (!_strExportPath.EndsWith("\\")) _strExportPath += "\\";
            }

            popupContainerControlDateRangeFilter.Size = new System.Drawing.Size(191, 112);

            emptyEditor = new RepositoryItem();
        }
        private System.Drawing.Rectangle GetLinksScreenRect(BarItemLink link)
        {
            System.Reflection.PropertyInfo info = typeof(BarItemLink).GetProperty("BarControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            Control c = (Control)info.GetValue(link, null);
            return c.RectangleToScreen(link.Bounds);
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInRecordIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_AS_Report_P11D_Export_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1) || !string.IsNullOrEmpty(i_str_AddedRecordIDs3))
                {
                    Load_Data();
                }
            }
            SetMenuStatus();
        }

        private void frm_AS_Report_P11D_Export_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInRecordIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "DepartmentFilter", i_str_selected_department_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "EmployeeFilter", i_str_selected_employee_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                bbiRefresh.PerformClick();

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                // Get BarButtons Location //
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, 0, 0);
                foreach (BarItemLink link in bar1.ItemLinks)
                {
                    if (link.Item.Name == "beiDateRange")  // Find the Correct button then call function to gets it position using Reflection (as this info is not publicly available //
                    {
                        rect = GetLinksScreenRect(link);
                        break;
                    }
                }
                if (rect.X > 0 && rect.Y > 0)
                {
                    // Draw Locus Effect on object so user can see it //
                    System.Drawing.Point screenPoint = new System.Drawing.Point(rect.X + rect.Width - 10, rect.Y + rect.Height - 10);
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                }

            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs1 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    view = (GridView)gridControl1.MainView;
                    break;
                case 3:
                    view = (GridView)gridControl3.MainView;
                    break;
                case 5:
                    view = (GridView)gridControl5.MainView;
                    break;
                default:
                    return;
            }
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            /*if (iBool_AllowAdd)
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                    
                alItems.Add("iBlockAdd");
                bbiBlockAdd.Enabled = true;
            }*/
            /*
            if (iBool_AllowEdit && intRowHandles.Length >= 1)
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
                if (intRowHandles.Length >= 2)
                {
                    alItems.Add("iBlockEdit");
                    bbiBlockEdit.Enabled = true;
                }
            }
            */
            /*(if (iBool_AllowDelete && intRowHandles.Length >= 1)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
            }*/

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            //gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            //gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;

            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            //gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            //gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;

            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            //gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            //gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0);
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;


        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            //sp_HR_00210_Report_Payroll_StartersTableAdapter.Fill(dataSet_HR_Reporting.sp_HR_00210_Report_Payroll_Starters, i_str_selected_department_ids, i_str_selected_employee_ids, i_dtStart, i_dtEnd);
            sp_AS_11176_P11D_CombinedTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11176_P11D_Combined, dateEditStart.DateTime.Date, dateEditEnd.DateTime.Date);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();

            view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            //sp_HR_00211_Report_Payroll_LeaversTableAdapter.Fill(dataSet_HR_Reporting.sp_HR_00211_Report_Payroll_Leavers, i_str_selected_department_ids, i_str_selected_employee_ids, i_dtStart, i_dtEnd);
            sp_AS_11174_P11D_RecordedTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11174_P11D_Recorded, dateEditStart.DateTime.Date, dateEditEnd.DateTime.Date);
            this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();

           
            view = (GridView)gridControl5.MainView;
            view.BeginUpdate();
            //sp_HR_00212_Report_Payroll_AbsencesTableAdapter.Fill(dataSet_HR_Reporting.sp_HR_00212_Report_Payroll_Absences, i_str_selected_department_ids, i_str_selected_employee_ids, i_dtStart, i_dtEnd);
            sp_AS_11175_P11D_KeepersTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11175_P11D_Keepers, dateEditStart.DateTime.Date, dateEditEnd.DateTime.Date);
            this.RefreshGridViewState5.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();
            
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Load_Employee_Filter()
        {
        }

        private void Clear_Employee_filter()
        {
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {

        }

        private void Block_Add()
        {

        }

        private void Block_Edit()
        {

        }

        private void Edit_Record()
        {

        }

        private void Delete_Record()
        {

        }

        private void View_Record()
        {

        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }


        #region Department Filter Panel

        private void btnDepartmentFilterOK_Click(object sender, EventArgs e)
        {

        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEditDepartment_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {

        }



        #endregion


        #region Employee Filter Panel

        private void btnEmployeeFilterOK_Click(object sender, EventArgs e)
        {
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEditEmployee_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
        }



        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();

            //Reload the main grid data
            Load_Data();
        }

        private void repositoryItemPopupContainerEditType_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            string strValue = "";
            if (dateEditStart.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditStart.DateTime;
            }
            if (dateEditEnd.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditEnd.DateTime;
            }
            if (dateEditStart.DateTime == DateTime.MinValue && dateEditEnd.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy"));
            }
            return strValue;
        }


        #endregion


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Combined P11D Data - Adjust any Filters and Cick Refresh button");
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1)
                {
                    //bsiDataset.Enabled = true;
                    //bbiDatasetManager.Enabled = true;
                    //bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            // The code in this event only fires if the Ctrl Key is held down by the end user when the Filter Editor is activated //
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //        
            if (Control.ModifierKeys != Keys.Control) return;  // CTRL key not held down so abort //
            GridView view = (GridView)sender;
            List<RepositoryItemComboBox> myRICBlist = new List<RepositoryItemComboBox>();
            foreach (GridColumn col in view.Columns)
            {
                if (col.Visible == false) continue;
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[col.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values == null || col.ColumnType.ToString() == "System.DateTime" || col.ColumnType.ToString() == "DateTime") continue;
                comboBox.Items.AddRange(values);
                myRICBlist.Add(comboBox);
                e.FilterControl.FilterColumns[col.FieldName].SetColumnEditor(myRICBlist[myRICBlist.Count - 1]);
            }
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView3

        private void gridView3_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No P11D Transaction Details - Adjust any filters then click Refresh button");
        }

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1)
                {
                    //bsiDataset.Enabled = true;
                    //bbiDatasetManager.Enabled = true;
                    //bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView3_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView3_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView3_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            // The code in this event only fires if the Ctrl Key is held down by the end user when the Filter Editor is activated //
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //        
            if (Control.ModifierKeys != Keys.Control) return;  // CTRL key not held down so abort //
            GridView view = (GridView)sender;
            List<RepositoryItemComboBox> myRICBlist = new List<RepositoryItemComboBox>();
            foreach (GridColumn col in view.Columns)
            {
                if (col.Visible == false) continue;
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[col.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values == null || col.ColumnType.ToString() == "System.DateTime" || col.ColumnType.ToString() == "DateTime") continue;
                comboBox.Items.AddRange(values);
                myRICBlist.Add(comboBox);
                e.FilterControl.FilterColumns[col.FieldName].SetColumnEditor(myRICBlist[myRICBlist.Count - 1]);
            }
        }

        private void gridView3_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView3_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        private void gridView3_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {

        }

        private void gridView3_ShowingEditor(object sender, CancelEventArgs e)
        {
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView5

        private void gridView5_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Keeper Allocations - Adjust any filters then click Refresh button");
        }

        private void gridView5_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 5;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1)
                {
                    //bsiDataset.Enabled = true;
                    //bbiDatasetManager.Enabled = true;
                    //bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView5_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView5_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView5_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            // The code in this event only fires if the Ctrl Key is held down by the end user when the Filter Editor is activated //
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //        
            if (Control.ModifierKeys != Keys.Control) return;  // CTRL key not held down so abort //
            GridView view = (GridView)sender;
            List<RepositoryItemComboBox> myRICBlist = new List<RepositoryItemComboBox>();
            foreach (GridColumn col in view.Columns)
            {
                if (col.Visible == false) continue;
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[col.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values == null || col.ColumnType.ToString() == "System.DateTime" || col.ColumnType.ToString() == "DateTime") continue;
                comboBox.Items.AddRange(values);
                myRICBlist.Add(comboBox);
                e.FilterControl.FilterColumns[col.FieldName].SetColumnEditor(myRICBlist[myRICBlist.Count - 1]);
            }
        }

        private void gridView5_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView5_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        private void gridView5_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
        }

        private void gridView5_ShowingEditor(object sender, CancelEventArgs e)
        {
        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void bbiExportToExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_strExportPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Report P11D Export Spreadsheet Path has not been set (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Report P11D Export", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strDateTime = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
            string strMergedFileName = Path.Combine(_strExportPath + "AS_Report_P11D_Export_" + strDateTime + ".xls");
            string strFileName1 = Path.Combine(_strExportPath + "Temp1_" + GlobalSettings.Username + "_" + strDateTime + ".xls");
            string strFileName2 = Path.Combine(_strExportPath + "Temp2_" + GlobalSettings.Username + "_" + strDateTime + ".xls");
            string strFileName3 = Path.Combine(_strExportPath + "Temp3_" + GlobalSettings.Username + "_" + strDateTime + ".xls");
            try
            {
                gridControl1.ExportToXls(strFileName1);
                gridControl3.ExportToXls(strFileName2);
                gridControl5.ExportToXls(strFileName3);

                string[] FilesToExport = new string[] { strFileName1, strFileName2, strFileName3 };
                MergeXlsxFilesDevExpress(strMergedFileName, FilesToExport);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while exporting the Report P11D Export to spreadsheet...\n\nError: " + ex.Message + ".\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Report P11D Export", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            #region CleanUp
            if (File.Exists(strFileName1))
            {
                try
                {
                    File.Delete(strFileName1);
                }
                catch (Exception) { }
            }
            if (File.Exists(strFileName2))
            {
                try
                {
                    File.Delete(strFileName2);
                }
                catch (Exception) { }
            }
            if (File.Exists(strFileName3))
            {
                try
                {
                    File.Delete(strFileName3);
                }
                catch (Exception) { }
            }
            #endregion

            try
            {
                System.Diagnostics.Process.Start(strMergedFileName);  // Open the generated file //
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the generated Report P11D Export spreadsheet: " + strMergedFileName + ".\n\nAn error may have occurred during it's creation or you may not have a viewer installed on your computer capable of loading the file.", "Report P11D Export", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public void MergeXlsxFilesDevExpress(string destXlsxFileName, params string[] sourceXlsxFileNames)
        {
            DevExpress.Spreadsheet.Workbook destWorkBook = new DevExpress.Spreadsheet.Workbook();
            destWorkBook.CreateNewDocument();

            int i = 0;
            foreach (var sourceXlsxFile in sourceXlsxFileNames)
            {
                DevExpress.Spreadsheet.Workbook sourceWorkBook = new DevExpress.Spreadsheet.Workbook();
                sourceWorkBook.LoadDocument(sourceXlsxFile);
                foreach (DevExpress.Spreadsheet.Worksheet sheet in sourceWorkBook.Worksheets)
                {
                    i++;
                    DevExpress.Spreadsheet.Worksheet temp = destWorkBook.Worksheets.Add();
                    temp.CopyFrom(sheet);
                    //var range = temp.GetUsedRange();
                    //temp.Columns.AutoFit(0, range.ColumnCount);

                    int lastCol = temp.GetUsedRange().RightColumnIndex;
                    int lastRow = temp.GetUsedRange().BottomRowIndex;

                    Range header = temp.Range.FromLTRB(0, 0, lastCol, 0); 
                    Formatting headerFormatting = header.BeginUpdateFormatting(); 
                    headerFormatting.Borders.SetAllBorders(DXColor.Black, BorderLineStyle.Thin); 
                    headerFormatting.Font.Name = "Calibri";
                    headerFormatting.Font.Color = DXColor.Black; 
                    headerFormatting.Font.Size = 9; 
                    headerFormatting.Fill.BackgroundColor = DXColor.LightGreen; 
                    //headerFormatting.Alignment.Horizontal = DevExpress.Spreadsheet.HorizontalAlignment.Center; 
                    //headerFormatting.Alignment.Vertical = VerticalAlignment.Bottom; 
                    //headerFormatting.Alignment.RotationAngle = 90; 
                    header.EndUpdateFormatting(headerFormatting);
                    header.RowHeight = 65;
                    header.AutoFitRows();

                    // Alternating rowcolor //
                    for (int ii = 1; ii <= lastRow; ii++) 
                    { 
                        if (ii % 2 == 0) temp.Range.FromLTRB(0, ii, lastCol, ii).FillColor = DXColor.FromArgb(235, 235, 235); 
                    }
                    Range body = temp.Range.FromLTRB(0, 1, lastCol, lastRow); 
                    Formatting bodyFormatting = body.BeginUpdateFormatting();
                    bodyFormatting.Borders.SetAllBorders(DXColor.Black, BorderLineStyle.Thin); 
                    bodyFormatting.Font.Name = "Calibri"; 
                    bodyFormatting.Font.Size = 9; 
                   // bodyFormatting.Alignment.Vertical = VerticalAlignment.Center; 
                    body.EndUpdateFormatting(bodyFormatting); 
                    body.RowHeight = 65;
                    temp.GetUsedRange().AutoFitColumns();

                    switch (i)
                    {
                        case 1:
                            temp.Name = "Combined";
                            break;
                        case 2:
                            temp.Name = "P11D_Transactions";
                            break;
                        case 3:
                            temp.Name = "Keeper_Allocations";
                            break;
                        default:
                            break;
                    }
                }
                sourceWorkBook.Dispose();
            }
            destWorkBook.Worksheets.RemoveAt(0);
            destWorkBook.Worksheets.ActiveWorksheet = destWorkBook.Worksheets["Combined"];

            destWorkBook.DocumentProperties.Title = "P11D Report for period " + dateEditStart.DateTime.ToShortDateString() +
                                                                       " to " + dateEditEnd.DateTime.ToShortDateString();

            destWorkBook.SaveDocument(destXlsxFileName);
            destWorkBook.Dispose();
        }


 

 


 

    }
}


﻿namespace WoodPlan5
{
    partial class frm_AS_Specifications_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Specifications_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.specificationGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11141HardwareSpecificationsItemBindingSource = new System.Windows.Forms.BindingSource();
            this.specificationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSpecificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProcessorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProcessorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClockSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRAM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHardDriveTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHardDriveType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGraphicsCardID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGraphicsCard = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecification = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NotesMemoExEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.webRepositoryItemHyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.lueSupplierType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.spAS11020SupplierListBindingSource = new System.Windows.Forms.BindingSource();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.dxErrorProviderSupplier = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp_AS_11020_Supplier_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter();
            this.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11141_Hardware_Specifications_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.specificationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11141HardwareSpecificationsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.specificationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoExEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.webRepositoryItemHyperLinkEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProviderSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.Manager = null;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 748);
            this.barDockControlBottom.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 748);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1372, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 748);
            // 
            // bbiSave
            // 
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.LargeGlyph")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.Manager = null;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiCancel});
            this.barManager1.MaxItemId = 31;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("save_16x16.png", "images/save/save_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/save/save_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "save_16x16.png");
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1675, 521, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1372, 748);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1372, 748);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // specificationGridControl
            // 
            this.specificationGridControl.DataSource = this.spAS11141HardwareSpecificationsItemBindingSource;
            this.specificationGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.specificationGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.specificationGridControl.EmbeddedNavigator.Buttons.Append.ImageIndex = 0;
            this.specificationGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.specificationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.specificationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.specificationGridControl.EmbeddedNavigator.Buttons.Edit.ImageIndex = 1;
            this.specificationGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.specificationGridControl.EmbeddedNavigator.Buttons.EndEdit.ImageIndex = 4;
            this.specificationGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.specificationGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.specificationGridControl.EmbeddedNavigator.Buttons.Remove.ImageIndex = 2;
            this.specificationGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "";
            this.specificationGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.specificationGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "", "delete")});
            this.specificationGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.supplierGridControl_EmbeddedNavigator_ButtonClick);
            this.specificationGridControl.Location = new System.Drawing.Point(0, 0);
            this.specificationGridControl.MainView = this.specificationGridView;
            this.specificationGridControl.Name = "specificationGridControl";
            this.specificationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.NotesMemoExEdit,
            this.webRepositoryItemHyperLinkEdit,
            this.lueSupplierType,
            this.TextEdit});
            this.specificationGridControl.Size = new System.Drawing.Size(1372, 748);
            this.specificationGridControl.TabIndex = 5;
            this.specificationGridControl.UseEmbeddedNavigator = true;
            this.specificationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.specificationGridView});
            // 
            // spAS11141HardwareSpecificationsItemBindingSource
            // 
            this.spAS11141HardwareSpecificationsItemBindingSource.DataMember = "sp_AS_11141_Hardware_Specifications_Item";
            this.spAS11141HardwareSpecificationsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // specificationGridView
            // 
            this.specificationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSpecificationID,
            this.colProcessorTypeID,
            this.colProcessorType,
            this.colClockSpeed,
            this.colRAM,
            this.colHardDriveTypeID,
            this.colHardDriveType,
            this.colGraphicsCardID,
            this.colGraphicsCard,
            this.colSpecification,
            this.colMode,
            this.colRecordID});
            this.specificationGridView.GridControl = this.specificationGridControl;
            this.specificationGridView.Name = "specificationGridView";
            this.specificationGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.specificationGridView.OptionsFind.AlwaysVisible = true;
            this.specificationGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.specificationGridView.OptionsLayout.StoreAppearance = true;
            this.specificationGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.specificationGridView.OptionsSelection.MultiSelect = true;
            this.specificationGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.specificationGridView.OptionsView.ColumnAutoWidth = false;
            this.specificationGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.specificationGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.supplierGridView_PopupMenuShowing);
            this.specificationGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.supplierGridView_SelectionChanged);
            this.specificationGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.supplierGridView_CustomDrawEmptyForeground);
            this.specificationGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.supplierGridView_CustomFilterDialog);
            this.specificationGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.supplierGridView_MouseUp);
            this.specificationGridView.DoubleClick += new System.EventHandler(this.supplierGridView_DoubleClick);
            // 
            // colSpecificationID
            // 
            this.colSpecificationID.FieldName = "SpecificationID";
            this.colSpecificationID.Name = "colSpecificationID";
            this.colSpecificationID.OptionsColumn.AllowEdit = false;
            this.colSpecificationID.OptionsColumn.AllowFocus = false;
            this.colSpecificationID.OptionsColumn.ReadOnly = true;
            this.colSpecificationID.Width = 95;
            // 
            // colProcessorTypeID
            // 
            this.colProcessorTypeID.FieldName = "ProcessorTypeID";
            this.colProcessorTypeID.Name = "colProcessorTypeID";
            this.colProcessorTypeID.OptionsColumn.AllowEdit = false;
            this.colProcessorTypeID.OptionsColumn.AllowFocus = false;
            this.colProcessorTypeID.OptionsColumn.ReadOnly = true;
            this.colProcessorTypeID.Width = 109;
            // 
            // colProcessorType
            // 
            this.colProcessorType.FieldName = "ProcessorType";
            this.colProcessorType.Name = "colProcessorType";
            this.colProcessorType.OptionsColumn.AllowEdit = false;
            this.colProcessorType.OptionsColumn.AllowFocus = false;
            this.colProcessorType.OptionsColumn.ReadOnly = true;
            this.colProcessorType.Visible = true;
            this.colProcessorType.VisibleIndex = 0;
            this.colProcessorType.Width = 259;
            // 
            // colClockSpeed
            // 
            this.colClockSpeed.Caption = "Clock Speed (GHz)";
            this.colClockSpeed.FieldName = "ClockSpeed";
            this.colClockSpeed.Name = "colClockSpeed";
            this.colClockSpeed.OptionsColumn.AllowEdit = false;
            this.colClockSpeed.OptionsColumn.AllowFocus = false;
            this.colClockSpeed.OptionsColumn.ReadOnly = true;
            this.colClockSpeed.Visible = true;
            this.colClockSpeed.VisibleIndex = 1;
            this.colClockSpeed.Width = 188;
            // 
            // colRAM
            // 
            this.colRAM.Caption = "RAM (GB)";
            this.colRAM.FieldName = "RAM";
            this.colRAM.Name = "colRAM";
            this.colRAM.OptionsColumn.AllowEdit = false;
            this.colRAM.OptionsColumn.AllowFocus = false;
            this.colRAM.OptionsColumn.ReadOnly = true;
            this.colRAM.Visible = true;
            this.colRAM.VisibleIndex = 2;
            this.colRAM.Width = 141;
            // 
            // colHardDriveTypeID
            // 
            this.colHardDriveTypeID.FieldName = "HardDriveTypeID";
            this.colHardDriveTypeID.Name = "colHardDriveTypeID";
            this.colHardDriveTypeID.OptionsColumn.AllowEdit = false;
            this.colHardDriveTypeID.OptionsColumn.AllowFocus = false;
            this.colHardDriveTypeID.OptionsColumn.ReadOnly = true;
            this.colHardDriveTypeID.Width = 113;
            // 
            // colHardDriveType
            // 
            this.colHardDriveType.FieldName = "HardDriveType";
            this.colHardDriveType.Name = "colHardDriveType";
            this.colHardDriveType.OptionsColumn.AllowEdit = false;
            this.colHardDriveType.OptionsColumn.AllowFocus = false;
            this.colHardDriveType.OptionsColumn.ReadOnly = true;
            this.colHardDriveType.Visible = true;
            this.colHardDriveType.VisibleIndex = 3;
            this.colHardDriveType.Width = 201;
            // 
            // colGraphicsCardID
            // 
            this.colGraphicsCardID.FieldName = "GraphicsCardID";
            this.colGraphicsCardID.Name = "colGraphicsCardID";
            this.colGraphicsCardID.OptionsColumn.AllowEdit = false;
            this.colGraphicsCardID.OptionsColumn.AllowFocus = false;
            this.colGraphicsCardID.OptionsColumn.ReadOnly = true;
            this.colGraphicsCardID.Width = 102;
            // 
            // colGraphicsCard
            // 
            this.colGraphicsCard.FieldName = "GraphicsCard";
            this.colGraphicsCard.Name = "colGraphicsCard";
            this.colGraphicsCard.OptionsColumn.AllowEdit = false;
            this.colGraphicsCard.OptionsColumn.AllowFocus = false;
            this.colGraphicsCard.OptionsColumn.ReadOnly = true;
            this.colGraphicsCard.Visible = true;
            this.colGraphicsCard.VisibleIndex = 4;
            this.colGraphicsCard.Width = 180;
            // 
            // colSpecification
            // 
            this.colSpecification.FieldName = "Specification";
            this.colSpecification.Name = "colSpecification";
            this.colSpecification.OptionsColumn.AllowEdit = false;
            this.colSpecification.OptionsColumn.AllowFocus = false;
            this.colSpecification.OptionsColumn.ReadOnly = true;
            this.colSpecification.Visible = true;
            this.colSpecification.VisibleIndex = 5;
            this.colSpecification.Width = 823;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            // 
            // NotesMemoExEdit
            // 
            this.NotesMemoExEdit.AutoHeight = false;
            this.NotesMemoExEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NotesMemoExEdit.Name = "NotesMemoExEdit";
            this.NotesMemoExEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.NotesMemoExEdit.ShowIcon = false;
            // 
            // webRepositoryItemHyperLinkEdit
            // 
            this.webRepositoryItemHyperLinkEdit.AutoHeight = false;
            this.webRepositoryItemHyperLinkEdit.Name = "webRepositoryItemHyperLinkEdit";
            this.webRepositoryItemHyperLinkEdit.SingleClick = true;
            // 
            // lueSupplierType
            // 
            this.lueSupplierType.AutoHeight = false;
            this.lueSupplierType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSupplierType.DisplayMember = "Value";
            this.lueSupplierType.Name = "lueSupplierType";
            this.lueSupplierType.ValueMember = "Pick_List_ID";
            // 
            // TextEdit
            // 
            this.TextEdit.AutoHeight = false;
            this.TextEdit.Name = "TextEdit";
            // 
            // spAS11020SupplierListBindingSource
            // 
            this.spAS11020SupplierListBindingSource.DataMember = "sp_AS_11020_Supplier_List";
            this.spAS11020SupplierListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // bbiCancel
            // 
            this.bbiCancel.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiCancel.Caption = "Cancel and Close";
            this.bbiCancel.DropDownEnabled = false;
            this.bbiCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCancel.Glyph")));
            this.bbiCancel.Id = 30;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // dxErrorProviderSupplier
            // 
            this.dxErrorProviderSupplier.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11020_Supplier_ListTableAdapter
            // 
            this.sp_AS_11020_Supplier_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11141_Hardware_Specifications_ItemTableAdapter
            // 
            this.sp_AS_11141_Hardware_Specifications_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Specifications_Manager
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1372, 748);
            this.Controls.Add(this.specificationGridControl);
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Specifications_Manager";
            this.Text = "Specification Manager";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.frm_AS_Specifications_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Specifications_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            this.Controls.SetChildIndex(this.specificationGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.specificationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11141HardwareSpecificationsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.specificationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoExEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.webRepositoryItemHyperLinkEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProviderSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl specificationGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView specificationGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit NotesMemoExEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit webRepositoryItemHyperLinkEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lueSupplierType;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProviderSupplier;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit TextEdit;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource spAS11020SupplierListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter sp_AS_11020_Supplier_ListTableAdapter;
        private System.Windows.Forms.BindingSource spAS11141HardwareSpecificationsItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11141_Hardware_Specifications_ItemTableAdapter sp_AS_11141_Hardware_Specifications_ItemTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecificationID;
        private DevExpress.XtraGrid.Columns.GridColumn colProcessorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colProcessorType;
        private DevExpress.XtraGrid.Columns.GridColumn colClockSpeed;
        private DevExpress.XtraGrid.Columns.GridColumn colRAM;
        private DevExpress.XtraGrid.Columns.GridColumn colHardDriveTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colHardDriveType;
        private DevExpress.XtraGrid.Columns.GridColumn colGraphicsCardID;
        private DevExpress.XtraGrid.Columns.GridColumn colGraphicsCard;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecification;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
    }
}

namespace WoodPlan5
{
    partial class frm_AS_Billing_Centre_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Billing_Centre_Manager));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.billingCentreCodeGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11038BillingCentreCodeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.billingCentreCodeGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ArchiveCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.equipmentChildTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.companyTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.companyGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11057CompanyItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.companyGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCompanyID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompany1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.departmentTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.departmentGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11060DepartmentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.departmentGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepartmentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.costCentreTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.costCentreGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11063CostCentreItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.costCentreGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCostCentreID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.tableAdapterManager1 = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter();
            this.sp_AS_11057_Company_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11057_Company_ItemTableAdapter();
            this.sp_AS_11060_Department_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11060_Department_ItemTableAdapter();
            this.sp_AS_11063_Cost_Centre_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11063_Cost_Centre_ItemTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.billingCentreCodeGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11038BillingCentreCodeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingCentreCodeGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArchiveCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentChildTabControl)).BeginInit();
            this.equipmentChildTabControl.SuspendLayout();
            this.companyTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.companyGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11057CompanyItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            this.departmentTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.departmentGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11060DepartmentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            this.costCentreTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.costCentreGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11063CostCentreItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.costCentreGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 740);
            this.barDockControlBottom.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 740);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1372, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 740);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.equipmentChildTabControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1372, 740);
            this.splitContainerControl1.SplitterPosition = 183;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.billingCentreCodeGridControl;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.billingCentreCodeGridControl);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1372, 551);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // billingCentreCodeGridControl
            // 
            this.billingCentreCodeGridControl.DataSource = this.spAS11038BillingCentreCodeItemBindingSource;
            this.billingCentreCodeGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.billingCentreCodeGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.billingCentreCodeGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, false, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.billingCentreCodeGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.billingCentreCodeGridControl.Location = new System.Drawing.Point(0, 0);
            this.billingCentreCodeGridControl.MainView = this.billingCentreCodeGridView;
            this.billingCentreCodeGridControl.MenuManager = this.barManager1;
            this.billingCentreCodeGridControl.Name = "billingCentreCodeGridControl";
            this.billingCentreCodeGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEditDate,
            this.ArchiveCheckEdit});
            this.billingCentreCodeGridControl.Size = new System.Drawing.Size(1372, 551);
            this.billingCentreCodeGridControl.TabIndex = 1;
            this.billingCentreCodeGridControl.UseEmbeddedNavigator = true;
            this.billingCentreCodeGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.billingCentreCodeGridView});
            // 
            // spAS11038BillingCentreCodeItemBindingSource
            // 
            this.spAS11038BillingCentreCodeItemBindingSource.DataMember = "sp_AS_11038_Billing_Centre_Code_Item";
            this.spAS11038BillingCentreCodeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("country_16x16.png", "images/miscellaneous/country_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/country_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "country_16x16.png");
            this.imageCollection1.InsertGalleryImage("insert_16x16.png", "images/actions/insert_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/insert_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "insert_16x16.png");
            // 
            // billingCentreCodeGridView
            // 
            this.billingCentreCodeGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingCentreCodeID,
            this.colBillingCentreCode,
            this.colCompanyID,
            this.colCompany,
            this.colCompanyCode,
            this.colDepartmentID,
            this.colDepartment,
            this.colDepartmentCode,
            this.colCostCentreID,
            this.colCostCentre,
            this.colCostCentreCode,
            this.colMode,
            this.colRecordID});
            this.billingCentreCodeGridView.GridControl = this.billingCentreCodeGridControl;
            this.billingCentreCodeGridView.Name = "billingCentreCodeGridView";
            this.billingCentreCodeGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.billingCentreCodeGridView.OptionsFind.AlwaysVisible = true;
            this.billingCentreCodeGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.billingCentreCodeGridView.OptionsLayout.StoreAppearance = true;
            this.billingCentreCodeGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.billingCentreCodeGridView.OptionsSelection.MultiSelect = true;
            this.billingCentreCodeGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.billingCentreCodeGridView.OptionsView.ColumnAutoWidth = false;
            this.billingCentreCodeGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.billingCentreCodeGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.BillingCentreCodeView_PopupMenuShowing);
            this.billingCentreCodeGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.billingCentreCodeGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.billingCentreCodeGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.billingCentreCodeGridView.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.billingCentreCodeGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.billingCentreCodeGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.billingCentreCodeGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colBillingCentreCodeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colBillingCentreCodeID.Width = 125;
            // 
            // colBillingCentreCode
            // 
            this.colBillingCentreCode.FieldName = "BillingCentreCode";
            this.colBillingCentreCode.Name = "colBillingCentreCode";
            this.colBillingCentreCode.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCode.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCode.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCode.Visible = true;
            this.colBillingCentreCode.VisibleIndex = 0;
            this.colBillingCentreCode.Width = 186;
            // 
            // colCompanyID
            // 
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.OptionsColumn.ShowInCustomizationForm = false;
            this.colCompanyID.OptionsColumn.ShowInExpressionEditor = false;
            this.colCompanyID.Width = 80;
            // 
            // colCompany
            // 
            this.colCompany.FieldName = "Company";
            this.colCompany.Name = "colCompany";
            this.colCompany.OptionsColumn.AllowEdit = false;
            this.colCompany.OptionsColumn.AllowFocus = false;
            this.colCompany.OptionsColumn.ReadOnly = true;
            this.colCompany.Visible = true;
            this.colCompany.VisibleIndex = 1;
            this.colCompany.Width = 240;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Visible = true;
            this.colCompanyCode.VisibleIndex = 2;
            this.colCompanyCode.Width = 87;
            // 
            // colDepartmentID
            // 
            this.colDepartmentID.FieldName = "DepartmentID";
            this.colDepartmentID.Name = "colDepartmentID";
            this.colDepartmentID.OptionsColumn.AllowEdit = false;
            this.colDepartmentID.OptionsColumn.AllowFocus = false;
            this.colDepartmentID.OptionsColumn.ReadOnly = true;
            this.colDepartmentID.OptionsColumn.ShowInCustomizationForm = false;
            this.colDepartmentID.OptionsColumn.ShowInExpressionEditor = false;
            this.colDepartmentID.Width = 92;
            // 
            // colDepartment
            // 
            this.colDepartment.FieldName = "Department";
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.OptionsColumn.AllowEdit = false;
            this.colDepartment.OptionsColumn.AllowFocus = false;
            this.colDepartment.OptionsColumn.ReadOnly = true;
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 3;
            this.colDepartment.Width = 369;
            // 
            // colDepartmentCode
            // 
            this.colDepartmentCode.FieldName = "DepartmentCode";
            this.colDepartmentCode.Name = "colDepartmentCode";
            this.colDepartmentCode.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode.Visible = true;
            this.colDepartmentCode.VisibleIndex = 4;
            this.colDepartmentCode.Width = 116;
            // 
            // colCostCentreID
            // 
            this.colCostCentreID.FieldName = "CostCentreID";
            this.colCostCentreID.Name = "colCostCentreID";
            this.colCostCentreID.OptionsColumn.AllowEdit = false;
            this.colCostCentreID.OptionsColumn.AllowFocus = false;
            this.colCostCentreID.OptionsColumn.ReadOnly = true;
            this.colCostCentreID.OptionsColumn.ShowInCustomizationForm = false;
            this.colCostCentreID.OptionsColumn.ShowInExpressionEditor = false;
            this.colCostCentreID.Width = 93;
            // 
            // colCostCentre
            // 
            this.colCostCentre.FieldName = "CostCentre";
            this.colCostCentre.Name = "colCostCentre";
            this.colCostCentre.OptionsColumn.AllowEdit = false;
            this.colCostCentre.OptionsColumn.AllowFocus = false;
            this.colCostCentre.OptionsColumn.ReadOnly = true;
            this.colCostCentre.Visible = true;
            this.colCostCentre.VisibleIndex = 5;
            this.colCostCentre.Width = 283;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode.Visible = true;
            this.colCostCentreCode.VisibleIndex = 6;
            this.colCostCentreCode.Width = 107;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "d";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // ArchiveCheckEdit
            // 
            this.ArchiveCheckEdit.AutoHeight = false;
            this.ArchiveCheckEdit.Caption = "Check";
            this.ArchiveCheckEdit.Name = "ArchiveCheckEdit";
            // 
            // equipmentChildTabControl
            // 
            this.equipmentChildTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.equipmentChildTabControl.Location = new System.Drawing.Point(0, 0);
            this.equipmentChildTabControl.Name = "equipmentChildTabControl";
            this.equipmentChildTabControl.SelectedTabPage = this.companyTabPage;
            this.equipmentChildTabControl.Size = new System.Drawing.Size(1372, 183);
            this.equipmentChildTabControl.TabIndex = 0;
            this.equipmentChildTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.companyTabPage,
            this.departmentTabPage,
            this.costCentreTabPage});
            // 
            // companyTabPage
            // 
            this.companyTabPage.Controls.Add(this.gridSplitContainer2);
            this.companyTabPage.Name = "companyTabPage";
            this.companyTabPage.Size = new System.Drawing.Size(1367, 157);
            this.companyTabPage.Text = "Company Details";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.companyGridControl;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.companyGridControl);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1367, 157);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // companyGridControl
            // 
            this.companyGridControl.DataSource = this.spAS11057CompanyItemBindingSource;
            this.companyGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.companyGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.companyGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.companyGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.companyGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.companyGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.companyGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.companyGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.companyGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.companyGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.companyGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.companyGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.companyGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.companyGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.companyGridControl.Location = new System.Drawing.Point(0, 0);
            this.companyGridControl.MainView = this.companyGridView;
            this.companyGridControl.MenuManager = this.barManager1;
            this.companyGridControl.Name = "companyGridControl";
            this.companyGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemMemoExEdit3});
            this.companyGridControl.Size = new System.Drawing.Size(1367, 157);
            this.companyGridControl.TabIndex = 0;
            this.companyGridControl.UseEmbeddedNavigator = true;
            this.companyGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.companyGridView});
            // 
            // spAS11057CompanyItemBindingSource
            // 
            this.spAS11057CompanyItemBindingSource.DataMember = "sp_AS_11057_Company_Item";
            this.spAS11057CompanyItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // companyGridView
            // 
            this.companyGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCompanyID1,
            this.colCompanyCode1,
            this.colCompany1,
            this.colMode1,
            this.colRecordID1});
            this.companyGridView.GridControl = this.companyGridControl;
            this.companyGridView.Name = "companyGridView";
            this.companyGridView.OptionsCustomization.AllowFilter = false;
            this.companyGridView.OptionsCustomization.AllowGroup = false;
            this.companyGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.companyGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.companyGridView.OptionsLayout.StoreAppearance = true;
            this.companyGridView.OptionsSelection.MultiSelect = true;
            this.companyGridView.OptionsView.ColumnAutoWidth = false;
            this.companyGridView.OptionsView.ShowGroupPanel = false;
            this.companyGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.companyGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.companyGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.companyGridView.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.companyGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.companyGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.companyGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colCompanyID1
            // 
            this.colCompanyID1.FieldName = "CompanyID";
            this.colCompanyID1.Name = "colCompanyID1";
            this.colCompanyID1.OptionsColumn.AllowEdit = false;
            this.colCompanyID1.OptionsColumn.AllowFocus = false;
            this.colCompanyID1.OptionsColumn.ReadOnly = true;
            this.colCompanyID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colCompanyID1.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colCompanyCode1
            // 
            this.colCompanyCode1.FieldName = "CompanyCode";
            this.colCompanyCode1.Name = "colCompanyCode1";
            this.colCompanyCode1.OptionsColumn.AllowEdit = false;
            this.colCompanyCode1.OptionsColumn.AllowFocus = false;
            this.colCompanyCode1.OptionsColumn.ReadOnly = true;
            this.colCompanyCode1.Visible = true;
            this.colCompanyCode1.VisibleIndex = 0;
            this.colCompanyCode1.Width = 151;
            // 
            // colCompany1
            // 
            this.colCompany1.FieldName = "Company";
            this.colCompany1.Name = "colCompany1";
            this.colCompany1.OptionsColumn.AllowEdit = false;
            this.colCompany1.OptionsColumn.AllowFocus = false;
            this.colCompany1.OptionsColumn.ReadOnly = true;
            this.colCompany1.Visible = true;
            this.colCompany1.VisibleIndex = 1;
            this.colCompany1.Width = 707;
            // 
            // colMode1
            // 
            this.colMode1.FieldName = "Mode";
            this.colMode1.Name = "colMode1";
            this.colMode1.OptionsColumn.AllowEdit = false;
            this.colMode1.OptionsColumn.AllowFocus = false;
            this.colMode1.OptionsColumn.ReadOnly = true;
            this.colMode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode1.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colRecordID1
            // 
            this.colRecordID1.FieldName = "RecordID";
            this.colRecordID1.Name = "colRecordID1";
            this.colRecordID1.OptionsColumn.AllowEdit = false;
            this.colRecordID1.OptionsColumn.AllowFocus = false;
            this.colRecordID1.OptionsColumn.ReadOnly = true;
            this.colRecordID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID1.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ReadOnly = true;
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ReadOnly = true;
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // departmentTabPage
            // 
            this.departmentTabPage.Controls.Add(this.gridSplitContainer3);
            this.departmentTabPage.Name = "departmentTabPage";
            this.departmentTabPage.Size = new System.Drawing.Size(1367, 157);
            this.departmentTabPage.Text = "Department Details";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.departmentGridControl;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.departmentGridControl);
            this.gridSplitContainer3.Size = new System.Drawing.Size(1367, 157);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // departmentGridControl
            // 
            this.departmentGridControl.DataSource = this.spAS11060DepartmentItemBindingSource;
            this.departmentGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.departmentGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.departmentGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.departmentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.departmentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.departmentGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.departmentGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.departmentGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.departmentGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.departmentGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.departmentGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.departmentGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.departmentGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.departmentGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.departmentGridControl.Location = new System.Drawing.Point(0, 0);
            this.departmentGridControl.MainView = this.departmentGridView;
            this.departmentGridControl.MenuManager = this.barManager1;
            this.departmentGridControl.Name = "departmentGridControl";
            this.departmentGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2});
            this.departmentGridControl.Size = new System.Drawing.Size(1367, 157);
            this.departmentGridControl.TabIndex = 0;
            this.departmentGridControl.UseEmbeddedNavigator = true;
            this.departmentGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.departmentGridView});
            // 
            // spAS11060DepartmentItemBindingSource
            // 
            this.spAS11060DepartmentItemBindingSource.DataMember = "sp_AS_11060_Department_Item";
            this.spAS11060DepartmentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // departmentGridView
            // 
            this.departmentGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepartmentID1,
            this.colDepartmentCode1,
            this.colDepartment1,
            this.colMode2,
            this.colRecordID2});
            this.departmentGridView.GridControl = this.departmentGridControl;
            this.departmentGridView.Name = "departmentGridView";
            this.departmentGridView.OptionsCustomization.AllowGroup = false;
            this.departmentGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.departmentGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.departmentGridView.OptionsLayout.StoreAppearance = true;
            this.departmentGridView.OptionsSelection.MultiSelect = true;
            this.departmentGridView.OptionsView.ColumnAutoWidth = false;
            this.departmentGridView.OptionsView.ShowGroupPanel = false;
            this.departmentGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.departmentGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.departmentGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.departmentGridView.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.departmentGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.departmentGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.departmentGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colDepartmentID1
            // 
            this.colDepartmentID1.FieldName = "DepartmentID";
            this.colDepartmentID1.Name = "colDepartmentID1";
            this.colDepartmentID1.OptionsColumn.AllowEdit = false;
            this.colDepartmentID1.OptionsColumn.AllowFocus = false;
            this.colDepartmentID1.OptionsColumn.ReadOnly = true;
            this.colDepartmentID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colDepartmentID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colDepartmentID1.Width = 93;
            // 
            // colDepartmentCode1
            // 
            this.colDepartmentCode1.FieldName = "DepartmentCode";
            this.colDepartmentCode1.Name = "colDepartmentCode1";
            this.colDepartmentCode1.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode1.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode1.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode1.Visible = true;
            this.colDepartmentCode1.VisibleIndex = 0;
            this.colDepartmentCode1.Width = 161;
            // 
            // colDepartment1
            // 
            this.colDepartment1.FieldName = "Department";
            this.colDepartment1.Name = "colDepartment1";
            this.colDepartment1.OptionsColumn.AllowEdit = false;
            this.colDepartment1.OptionsColumn.AllowFocus = false;
            this.colDepartment1.OptionsColumn.ReadOnly = true;
            this.colDepartment1.Visible = true;
            this.colDepartment1.VisibleIndex = 1;
            this.colDepartment1.Width = 768;
            // 
            // colMode2
            // 
            this.colMode2.FieldName = "Mode";
            this.colMode2.Name = "colMode2";
            this.colMode2.OptionsColumn.AllowEdit = false;
            this.colMode2.OptionsColumn.AllowFocus = false;
            this.colMode2.OptionsColumn.ReadOnly = true;
            this.colMode2.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode2.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colRecordID2
            // 
            this.colRecordID2.FieldName = "RecordID";
            this.colRecordID2.Name = "colRecordID2";
            this.colRecordID2.OptionsColumn.AllowEdit = false;
            this.colRecordID2.OptionsColumn.AllowFocus = false;
            this.colRecordID2.OptionsColumn.ReadOnly = true;
            this.colRecordID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID2.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            // 
            // costCentreTabPage
            // 
            this.costCentreTabPage.Controls.Add(this.costCentreGridControl);
            this.costCentreTabPage.Name = "costCentreTabPage";
            this.costCentreTabPage.Size = new System.Drawing.Size(1367, 157);
            this.costCentreTabPage.Text = "Cost Centre Details";
            // 
            // costCentreGridControl
            // 
            this.costCentreGridControl.DataSource = this.spAS11063CostCentreItemBindingSource;
            this.costCentreGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.costCentreGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.costCentreGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.costCentreGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.costCentreGridControl.Location = new System.Drawing.Point(0, 0);
            this.costCentreGridControl.MainView = this.costCentreGridView;
            this.costCentreGridControl.MenuManager = this.barManager1;
            this.costCentreGridControl.Name = "costCentreGridControl";
            this.costCentreGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemHyperLinkEdit3});
            this.costCentreGridControl.Size = new System.Drawing.Size(1367, 157);
            this.costCentreGridControl.TabIndex = 1;
            this.costCentreGridControl.UseEmbeddedNavigator = true;
            this.costCentreGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.costCentreGridView});
            // 
            // spAS11063CostCentreItemBindingSource
            // 
            this.spAS11063CostCentreItemBindingSource.DataMember = "sp_AS_11063_Cost_Centre_Item";
            this.spAS11063CostCentreItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // costCentreGridView
            // 
            this.costCentreGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCostCentreID1,
            this.colCostCentreCode1,
            this.colCostCentre1,
            this.colMode3,
            this.colRecordID3});
            this.costCentreGridView.GridControl = this.costCentreGridControl;
            this.costCentreGridView.Name = "costCentreGridView";
            this.costCentreGridView.OptionsCustomization.AllowGroup = false;
            this.costCentreGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.costCentreGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.costCentreGridView.OptionsLayout.StoreAppearance = true;
            this.costCentreGridView.OptionsSelection.MultiSelect = true;
            this.costCentreGridView.OptionsView.ColumnAutoWidth = false;
            this.costCentreGridView.OptionsView.ShowGroupPanel = false;
            this.costCentreGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.costCentreGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.costCentreGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.costCentreGridView.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.costCentreGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.costCentreGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.costCentreGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colCostCentreID1
            // 
            this.colCostCentreID1.FieldName = "CostCentreID";
            this.colCostCentreID1.Name = "colCostCentreID1";
            this.colCostCentreID1.OptionsColumn.AllowEdit = false;
            this.colCostCentreID1.OptionsColumn.AllowFocus = false;
            this.colCostCentreID1.OptionsColumn.ReadOnly = true;
            this.colCostCentreID1.Width = 94;
            // 
            // colCostCentreCode1
            // 
            this.colCostCentreCode1.FieldName = "CostCentreCode";
            this.colCostCentreCode1.Name = "colCostCentreCode1";
            this.colCostCentreCode1.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode1.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode1.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode1.Visible = true;
            this.colCostCentreCode1.VisibleIndex = 0;
            this.colCostCentreCode1.Width = 203;
            // 
            // colCostCentre1
            // 
            this.colCostCentre1.FieldName = "CostCentre";
            this.colCostCentre1.Name = "colCostCentre1";
            this.colCostCentre1.OptionsColumn.AllowEdit = false;
            this.colCostCentre1.OptionsColumn.AllowFocus = false;
            this.colCostCentre1.OptionsColumn.ReadOnly = true;
            this.colCostCentre1.Visible = true;
            this.colCostCentre1.VisibleIndex = 1;
            this.colCostCentre1.Width = 759;
            // 
            // colMode3
            // 
            this.colMode3.FieldName = "Mode";
            this.colMode3.Name = "colMode3";
            this.colMode3.OptionsColumn.AllowEdit = false;
            this.colMode3.OptionsColumn.AllowFocus = false;
            this.colMode3.OptionsColumn.ReadOnly = true;
            this.colMode3.Width = 43;
            // 
            // colRecordID3
            // 
            this.colRecordID3.FieldName = "RecordID";
            this.colRecordID3.Name = "colRecordID3";
            this.colRecordID3.OptionsColumn.AllowEdit = false;
            this.colRecordID3.OptionsColumn.AllowFocus = false;
            this.colRecordID3.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.Connection = null;
            this.tableAdapterManager1.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_AS_11038_Billing_Centre_Code_ItemTableAdapter
            // 
            this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11057_Company_ItemTableAdapter
            // 
            this.sp_AS_11057_Company_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11060_Department_ItemTableAdapter
            // 
            this.sp_AS_11060_Department_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11063_Cost_Centre_ItemTableAdapter
            // 
            this.sp_AS_11063_Cost_Centre_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // frm_AS_Billing_Centre_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1372, 740);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Billing_Centre_Manager";
            this.Text = "Billing Centre Code Manager";
            this.Activated += new System.EventHandler(this.frm_AS_Billing_Centre_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Billing_Centre_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.billingCentreCodeGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11038BillingCentreCodeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingCentreCodeGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArchiveCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentChildTabControl)).EndInit();
            this.equipmentChildTabControl.ResumeLayout(false);
            this.companyTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.companyGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11057CompanyItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            this.departmentTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.departmentGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11060DepartmentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            this.costCentreTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.costCentreGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11063CostCentreItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.costCentreGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl equipmentChildTabControl;
        private DevExpress.XtraTab.XtraTabPage companyTabPage;
        private DevExpress.XtraTab.XtraTabPage departmentTabPage;
        private DevExpress.XtraGrid.GridControl companyGridControl;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.GridControl departmentGridControl;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_AS_CoreTableAdapters.TableAdapterManager tableAdapterManager;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraGrid.Views.Grid.GridView companyGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView departmentGridView;
        private DevExpress.XtraTab.XtraTabPage costCentreTabPage;
        private DevExpress.XtraGrid.GridControl costCentreGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView costCentreGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.GridControl billingCentreCodeGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView billingCentreCodeGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ArchiveCheckEdit;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource spAS11038BillingCentreCodeItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter sp_AS_11038_Billing_Centre_Code_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11057CompanyItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11057_Company_ItemTableAdapter sp_AS_11057_Company_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11060DepartmentItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11063CostCentreItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11060_Department_ItemTableAdapter sp_AS_11060_Department_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11063_Cost_Centre_ItemTableAdapter sp_AS_11063_Cost_Centre_ItemTableAdapter;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompany1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode3;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID3;
    }
}

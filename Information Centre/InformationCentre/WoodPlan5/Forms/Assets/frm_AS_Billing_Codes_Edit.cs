﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;

namespace WoodPlan5
{
    public partial class frm_AS_Billing_Codes_Edit : BaseObjects.frmBase
    {
        public frm_AS_Billing_Codes_Edit()
        {
            InitializeComponent();            
        }

        private void frm_AS_Billing_Codes_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 110301;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            
            // connect adapters //
            sp_AS_11000_Duplicate_SearchTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11057_Company_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11060_Department_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11063_Cost_Centre_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            //Populate picklist Data
            PopulateBillingCentreCodePickList();

            switch (strFormMode.ToLower())
            {
                case "add":
                    addNewRow(FormMode.add);
                    break;
                case "blockadd":
                    addNewRow(FormMode.blockadd);
                    break;
                case "blockedit":
                    addNewRow(FormMode.blockedit);
                    this.dataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_Item.Rows[0].AcceptChanges(); 
                    break;
                case "edit":
                case "view":
                    //txtEquipment_EquipmentReference.Properties.ReadOnly = true;
                    try
                    {
                        LoadData();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
            }

            

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            PostOpen(null);
        }

        #region Instance Variables
        private string strSearchText ="";

        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        bool bool_FormLoading = true;

        public bool billingCentreCodeChanged = false;

        public enum FormMode { add, edit, view, delete, blockadd, blockedit };

        public FormMode formMode;
        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //
        public int intOriginalContractorID = 0;
        public int intSelectedContractorID = 0;
        public string strOriginalContractorIDs = "";
        public string strSelectedContractorIDs = "";
        public string strSelectedContractorName = "";

        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        #region Compulsory Implementation 
        
        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode.ToLower())
            {
                case "add":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.Appearance.ForeColor = color;
                        txtBillingCentreCode.Properties.ReadOnly = true;
                        
                        gridSplitContainer1.Visible = false;
                        gridControl1.Visible = false;
                        gridSplitContainer2.Visible = false;
                        gridControl2.Visible = false;
                        gridSplitContainer3.Visible = false;
                        gridControl3.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        txtBillingCentreCode.Properties.ReadOnly = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        ////barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        //barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Viewing";
                        bsiFormMode.ImageIndex = 4;  // View //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        //barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
               // equipmentDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
        
            bbiSave.Enabled = false;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        #endregion

        #region Form_Events

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) 
                this.Close();
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private void frm_AS_Billing_Codes_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProviderBillingCentreCode.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        private void frm_AS_Billing_Codes_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

          //  IdentifyChangedBindingSource();
        }

        #region EditorEvents


        private void lueCompany_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueDepartment_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueCostCentre_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueCommon_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;
            strSearchText = "";
            if (lueCompany.EditValue != null && lueDepartment.EditValue != null && lueCostCentre.EditValue != null)
            {
                txtBillingCentreCode.Text = lueCompany.Text + lueDepartment.Text + lueCostCentre.Text;
                              
                return;
            }
            string strCompany = "";
            string strDepartment = "";
            string strCostCentre = "";


            if (lueCompany.EditValue == null ? false : true)
            {
                strCompany = lueCompany.Text;
            }
            else
            {
                strCompany = "[Null Company Code]";
            }

            if (lueDepartment.EditValue == null ? false : true)
            {
                strDepartment = lueDepartment.Text;
            }
            else
            {
                strDepartment = "[Null Department Code]";
            }

            if (lueCostCentre.EditValue == null ? false : true)
            {
                strCostCentre = lueCostCentre.Text;
            }
            else
            {
                strCostCentre = "[Null Cost Centre Code]";
            }

            txtBillingCentreCode.Text = strCompany + strDepartment + strCostCentre;

        }

        #endregion
        

        #endregion

        #region Form_Functions

        private bool returnDuplicateStatus( FormMode mode, string searchText)
        {
            
            bool valid = false;

            DataTable dt = null;

            if (mode == FormMode.add)
            {
                string searchMode = "billingcentre";
                sp_AS_11000_Duplicate_SearchTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search, searchText, 0, searchMode);
                dt = this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search;
            }

            if (mode == FormMode.add )
            {
                if (dt.Rows.Count > 0)
                {
                    valid = false;
                }
                else
                {
                    valid = true;
                }
            }
            return valid;
        }
        
        private bool validateLookupEdit(LookUpEdit lookupEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;

            string ErrorText = "Please Select Value.";

            if (lookupEdit.Properties.GetDisplayText(lookupEdit.EditValue) == "" ? false : true)
            {
                ErrorText = "Please Select " + lookupEdit.Tag + ".";
            }
            if (lookupEdit.Properties.GetDisplayText(lookupEdit.EditValue) == "" || Convert.ToInt32(lookupEdit.EditValue) < 1)
            {
                dxErrorProviderBillingCentreCode.SetError(lookupEdit, ErrorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }
            else
            {
                dxErrorProviderBillingCentreCode.SetError(lookupEdit, "");
                return valid;
            }
        }
        
        private void ClearErrors(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.LookUpEdit) 
                    dxErrorProviderBillingCentreCode.SetError(item2, "");
                if (item2 is DevExpress.XtraEditors.TextEdit)
                    dxErrorProviderBillingCentreCode.SetError(item2, "");
                if (item2 is DevExpress.XtraEditors.DateEdit)
                    dxErrorProviderBillingCentreCode.SetError(item2, "");
                if (item2 is DevExpress.XtraEditors.ColorPickEdit)
                    dxErrorProviderBillingCentreCode.SetError(item2, "");
                if (item2 is DevExpress.XtraEditors.SpinEdit)
                    dxErrorProviderBillingCentreCode.SetError(item2, "");
                if (item2 is ContainerControl)
                    this.ClearErrors(item.Controls);
                if (item2 is DevExpress.XtraDataLayout.DataLayoutControl)
                    this.ClearErrors(item.Controls);

            }
        }

        private string[] splitStrRecords()
        {
            char[] delimiters = new char[] { ',' };
            string[] parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            return parts;
        }

        private void addNewRow(FormMode mode)
        {
            try
            {
                DataRow drNewRow = this.dataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_Item.NewRow();
                drNewRow["Mode"] = (mode.ToString()).ToLower();
                                
                //if (mode == FormMode.add || mode == FormMode.blockadd)
                //{
                //    drNewRow["RecordID"] = strRecordIDs;
                //    //drNewRow["Equipment_EquipmentReference"] = strGCReference;
                //}
                this.dataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_Item.Rows.Add(drNewRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp_AS_11057_Company_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11057_Company_Item, "", "");
            view.EndUpdate();
            GridView view2 = (GridView)gridControl2.MainView;
            view2.BeginUpdate();
            sp_AS_11060_Department_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11060_Department_Item, "", "");
            view2.EndUpdate();
            GridView view3 = (GridView)gridControl3.MainView;
            view3.BeginUpdate();
            sp_AS_11063_Cost_Centre_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11063_Cost_Centre_Item, "", "");
            view3.EndUpdate();
        }

        private void PopulateBillingCentreCodePickList()
        {
            sp_AS_11057_Company_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11057_Company_Item, "", "view"); 
            sp_AS_11060_Department_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11060_Department_Item, "", "view");
            sp_AS_11063_Cost_Centre_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11063_Cost_Centre_Item, "", "view");
        }

        private string SaveChangesExtracted(out bool shouldReturn)
        {
            shouldReturn = false;

            if (dxErrorProviderBillingCentreCode.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProviderBillingCentreCode, billingCentreCodeDataLayoutControl);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors +
                "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                shouldReturn = true;
                return "Error";
            }

            return String.Empty;
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
 
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //         
            EndEdit();
            this.Validate();
            this.ValidateChildren();

            
            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
                return result;
            strSearchText = lueCompany.EditValue + "," + lueDepartment.EditValue + "," + lueCostCentre.EditValue;
            if (!returnDuplicateStatus(formMode, strSearchText))
            {
                XtraMessageBox.Show("The new value is a duplicate of an existing record. Record not saved", "Duplicate exists", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Duplicate Error";
            }
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            EndEdit();
           
            CheckChangedTable(this.dataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_Item);//Billing Centre Code Check

            try
            {
                // Insert and Update queries defined in Table Adapter //
                if (billingCentreCodeChanged)
                    {
                        this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        billingCentreCodeChanged = false;
                    }
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            //// If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)spAS11057CompanyItemBindingSource.Current;
                if (currentRow != null)
                    strNewIDs = this.dataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_Item[0].BillingCentreCodeID + ";";
            }

            if (strNewIDs == "")
            {
                string[] t = splitStrRecords();
                strNewIDs = t[0];
            }
            //// Notify any open instances of Sequence Manager they will need to refresh their data on activating //
            if (Application.OpenForms[strCaller] != null)
            {
                (Application.OpenForms[strCaller] as frm_AS_Billing_Centre_Manager).UpdateFormRefreshStatus(1, strNewIDs, "", "");
                (Application.OpenForms[strCaller] as frm_AS_Billing_Centre_Manager).frmActivated();
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
                if (item2 is DevExpress.XtraDataLayout.DataLayoutControl) this.Attach_EditValueChanged_To_Children(item.Controls);
           
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }

        private Boolean SetChangesPendingLabel()
        {
            EndEdit();

            DataSet dsChanges = this.dataSet_AS_DataEntry.GetChanges();
            if (dsChanges != null) 
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;

                if (strFormMode.ToLower() == "add")
                {
                    bbiFormSave.Enabled = true;
                }
                return false;
            }

           
        }

        private void EndEdit()
        {
            spAS11038BillingCentreCodeItemBindingSource.EndEdit();          
        }

        private string CheckForPendingSave()
        {
            EndEdit();

            string strMessage = "";
            string strMessageBillingCodes = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_Item, " on the Billing Centre Code Edit Form ");//billingCentreCode Check
           

            if (strMessageBillingCodes != "")
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (strMessageBillingCodes != "") strMessage += strMessageBillingCodes;
            }
            return strMessage;
        }

        private string CheckTablePendingSave(DataTable dt, string formArea)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

  
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)" + formArea + "\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)" + formArea + "\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)" + formArea + "\n";
            }
            return strMessage;
        }

        private void CheckChangedTable(DataTable dt)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                IdentifyChangedBindingSource(dt);
            }
        }
        
        private void IdentifyChangedBindingSource(DataTable dt)
        {

            switch (dt.TableName)
            {
                case "sp_AS_11038_Billing_Centre_Code_Item"://Billing Centre Code
                    billingCentreCodeChanged = true;
                    break;
            }
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            bool_FormLoading = false;
        }

        #region ValidateMethod

        #endregion


        #endregion

        #region Data Navigator
                      
        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion

              
    }
}

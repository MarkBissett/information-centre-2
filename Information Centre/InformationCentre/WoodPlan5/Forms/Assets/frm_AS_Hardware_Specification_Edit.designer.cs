﻿namespace WoodPlan5
{
    partial class frm_AS_Hardware_Specification_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Hardware_Specification_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.ProcessorTypeIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11141HardwareSpecificationsItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLExtraBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RAMTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HardDriveTypeIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLExtra1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GraphicsCardIDLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLExtra2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ClockSpeedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForProcessorTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRAM = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHardDriveTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGraphicsCardID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClockSpeed = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11141_Hardware_Specifications_ItemTableAdapter();
            this.sp_AS_11000_PL_ExtraTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_ExtraTableAdapter();
            this.sp_AS_11000_PL_Extra1TableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Extra1TableAdapter();
            this.sp_AS_11000_PL_Extra2TableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Extra2TableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProcessorTypeIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11141HardwareSpecificationsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtraBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RAMTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HardDriveTypeIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtra1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GraphicsCardIDLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtra2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClockSpeedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProcessorTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRAM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHardDriveTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGraphicsCardID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClockSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1059, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 524);
            this.barDockControlBottom.Size = new System.Drawing.Size(1059, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 498);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1059, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 498);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup2,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1059, 498);
            this.editFormLayoutControlGroup.Text = "Root";
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(287, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(263, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.Location = new System.Drawing.Point(299, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ProcessorTypeIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.RAMTextEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.HardDriveTypeIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.GraphicsCardIDLookUpEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ClockSpeedTextEdit);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11141HardwareSpecificationsItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1825, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1059, 498);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // ProcessorTypeIDLookUpEdit
            // 
            this.ProcessorTypeIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11141HardwareSpecificationsItemBindingSource, "ProcessorTypeID", true));
            this.ProcessorTypeIDLookUpEdit.Location = new System.Drawing.Point(110, 35);
            this.ProcessorTypeIDLookUpEdit.MenuManager = this.barManager1;
            this.ProcessorTypeIDLookUpEdit.Name = "ProcessorTypeIDLookUpEdit";
            this.ProcessorTypeIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ProcessorTypeIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Processor Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.ProcessorTypeIDLookUpEdit.Properties.DataSource = this.spAS11000PLExtraBindingSource;
            this.ProcessorTypeIDLookUpEdit.Properties.DisplayMember = "Value";
            this.ProcessorTypeIDLookUpEdit.Properties.NullText = "";
            this.ProcessorTypeIDLookUpEdit.Properties.NullValuePrompt = "-- Please Select --";
            this.ProcessorTypeIDLookUpEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.ProcessorTypeIDLookUpEdit.Properties.ValueMember = "PickListID";
            this.ProcessorTypeIDLookUpEdit.Size = new System.Drawing.Size(937, 20);
            this.ProcessorTypeIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.ProcessorTypeIDLookUpEdit.TabIndex = 123;
            this.ProcessorTypeIDLookUpEdit.Tag = "Processor Type";
            this.ProcessorTypeIDLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpEdit_Validating);
            // 
            // spAS11141HardwareSpecificationsItemBindingSource
            // 
            this.spAS11141HardwareSpecificationsItemBindingSource.DataMember = "sp_AS_11141_Hardware_Specifications_Item";
            this.spAS11141HardwareSpecificationsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLExtraBindingSource
            // 
            this.spAS11000PLExtraBindingSource.DataMember = "sp_AS_11000_PL_Extra";
            this.spAS11000PLExtraBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // RAMTextEdit
            // 
            this.RAMTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11141HardwareSpecificationsItemBindingSource, "RAM", true));
            this.RAMTextEdit.Location = new System.Drawing.Point(110, 83);
            this.RAMTextEdit.MenuManager = this.barManager1;
            this.RAMTextEdit.Name = "RAMTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RAMTextEdit, true);
            this.RAMTextEdit.Size = new System.Drawing.Size(937, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RAMTextEdit, optionsSpelling1);
            this.RAMTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.RAMTextEdit.TabIndex = 125;
            this.RAMTextEdit.Tag = "RAM";
            this.RAMTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TextEdit_Validating);
            // 
            // HardDriveTypeIDLookUpEdit
            // 
            this.HardDriveTypeIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11141HardwareSpecificationsItemBindingSource, "HardDriveTypeID", true));
            this.HardDriveTypeIDLookUpEdit.Location = new System.Drawing.Point(110, 107);
            this.HardDriveTypeIDLookUpEdit.MenuManager = this.barManager1;
            this.HardDriveTypeIDLookUpEdit.Name = "HardDriveTypeIDLookUpEdit";
            this.HardDriveTypeIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HardDriveTypeIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Hard Drive", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.HardDriveTypeIDLookUpEdit.Properties.DataSource = this.spAS11000PLExtra1BindingSource;
            this.HardDriveTypeIDLookUpEdit.Properties.DisplayMember = "Value";
            this.HardDriveTypeIDLookUpEdit.Properties.NullText = "";
            this.HardDriveTypeIDLookUpEdit.Properties.NullValuePrompt = "-- Please Select --";
            this.HardDriveTypeIDLookUpEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.HardDriveTypeIDLookUpEdit.Properties.ValueMember = "PickListID";
            this.HardDriveTypeIDLookUpEdit.Size = new System.Drawing.Size(937, 20);
            this.HardDriveTypeIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.HardDriveTypeIDLookUpEdit.TabIndex = 126;
            this.HardDriveTypeIDLookUpEdit.Tag = "Hard Drive";
            this.HardDriveTypeIDLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpEdit_Validating);
            // 
            // spAS11000PLExtra1BindingSource
            // 
            this.spAS11000PLExtra1BindingSource.DataMember = "sp_AS_11000_PL_Extra1";
            this.spAS11000PLExtra1BindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // GraphicsCardIDLookUpEdit
            // 
            this.GraphicsCardIDLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11141HardwareSpecificationsItemBindingSource, "GraphicsCardID", true));
            this.GraphicsCardIDLookUpEdit.Location = new System.Drawing.Point(110, 131);
            this.GraphicsCardIDLookUpEdit.MenuManager = this.barManager1;
            this.GraphicsCardIDLookUpEdit.Name = "GraphicsCardIDLookUpEdit";
            this.GraphicsCardIDLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GraphicsCardIDLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Graphics Card", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.GraphicsCardIDLookUpEdit.Properties.DataSource = this.spAS11000PLExtra2BindingSource;
            this.GraphicsCardIDLookUpEdit.Properties.DisplayMember = "Value";
            this.GraphicsCardIDLookUpEdit.Properties.NullText = "";
            this.GraphicsCardIDLookUpEdit.Properties.NullValuePrompt = "-- Please Select --";
            this.GraphicsCardIDLookUpEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.GraphicsCardIDLookUpEdit.Properties.ValueMember = "PickListID";
            this.GraphicsCardIDLookUpEdit.Size = new System.Drawing.Size(937, 20);
            this.GraphicsCardIDLookUpEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.GraphicsCardIDLookUpEdit.TabIndex = 127;
            this.GraphicsCardIDLookUpEdit.Tag = "Graphics Card";
            this.GraphicsCardIDLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpEdit_Validating);
            // 
            // spAS11000PLExtra2BindingSource
            // 
            this.spAS11000PLExtra2BindingSource.DataMember = "sp_AS_11000_PL_Extra2";
            this.spAS11000PLExtra2BindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // ClockSpeedTextEdit
            // 
            this.ClockSpeedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11141HardwareSpecificationsItemBindingSource, "ClockSpeed", true));
            this.ClockSpeedTextEdit.Location = new System.Drawing.Point(110, 59);
            this.ClockSpeedTextEdit.MenuManager = this.barManager1;
            this.ClockSpeedTextEdit.Name = "ClockSpeedTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClockSpeedTextEdit, true);
            this.ClockSpeedTextEdit.Size = new System.Drawing.Size(937, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClockSpeedTextEdit, optionsSpelling2);
            this.ClockSpeedTextEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.ClockSpeedTextEdit.TabIndex = 128;
            this.ClockSpeedTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TextEdit_Validating);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForProcessorTypeID,
            this.ItemForRAM,
            this.ItemForHardDriveTypeID,
            this.ItemForGraphicsCardID,
            this.ItemForClockSpeed});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup2.Name = "autoGeneratedGroup1";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1039, 455);
            this.layoutControlGroup2.Text = "autoGeneratedGroup1";
            // 
            // ItemForProcessorTypeID
            // 
            this.ItemForProcessorTypeID.Control = this.ProcessorTypeIDLookUpEdit;
            this.ItemForProcessorTypeID.CustomizationFormText = "Processor Type :";
            this.ItemForProcessorTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForProcessorTypeID.Name = "ItemForProcessorTypeID";
            this.ItemForProcessorTypeID.Size = new System.Drawing.Size(1039, 24);
            this.ItemForProcessorTypeID.Text = "Processor Type :";
            this.ItemForProcessorTypeID.TextSize = new System.Drawing.Size(95, 13);
            // 
            // ItemForRAM
            // 
            this.ItemForRAM.Control = this.RAMTextEdit;
            this.ItemForRAM.CustomizationFormText = "RAM :";
            this.ItemForRAM.Location = new System.Drawing.Point(0, 48);
            this.ItemForRAM.Name = "ItemForRAM";
            this.ItemForRAM.Size = new System.Drawing.Size(1039, 24);
            this.ItemForRAM.Text = "RAM (GB) :";
            this.ItemForRAM.TextSize = new System.Drawing.Size(95, 13);
            // 
            // ItemForHardDriveTypeID
            // 
            this.ItemForHardDriveTypeID.Control = this.HardDriveTypeIDLookUpEdit;
            this.ItemForHardDriveTypeID.CustomizationFormText = "Hard Drive Type :";
            this.ItemForHardDriveTypeID.Location = new System.Drawing.Point(0, 72);
            this.ItemForHardDriveTypeID.Name = "ItemForHardDriveTypeID";
            this.ItemForHardDriveTypeID.Size = new System.Drawing.Size(1039, 24);
            this.ItemForHardDriveTypeID.Text = "Hard Drive Type :";
            this.ItemForHardDriveTypeID.TextSize = new System.Drawing.Size(95, 13);
            // 
            // ItemForGraphicsCardID
            // 
            this.ItemForGraphicsCardID.Control = this.GraphicsCardIDLookUpEdit;
            this.ItemForGraphicsCardID.CustomizationFormText = "Graphics Card :";
            this.ItemForGraphicsCardID.Location = new System.Drawing.Point(0, 96);
            this.ItemForGraphicsCardID.Name = "ItemForGraphicsCardID";
            this.ItemForGraphicsCardID.Size = new System.Drawing.Size(1039, 359);
            this.ItemForGraphicsCardID.Text = "Graphics Card :";
            this.ItemForGraphicsCardID.TextSize = new System.Drawing.Size(95, 13);
            // 
            // ItemForClockSpeed
            // 
            this.ItemForClockSpeed.Control = this.ClockSpeedTextEdit;
            this.ItemForClockSpeed.CustomizationFormText = "Clock Speed (GHz) :";
            this.ItemForClockSpeed.Location = new System.Drawing.Point(0, 24);
            this.ItemForClockSpeed.Name = "ItemForClockSpeed";
            this.ItemForClockSpeed.Size = new System.Drawing.Size(1039, 24);
            this.ItemForClockSpeed.Text = "Clock Speed (GHz) :";
            this.ItemForClockSpeed.TextSize = new System.Drawing.Size(95, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(287, 23);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(550, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(489, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11141_Hardware_Specifications_ItemTableAdapter
            // 
            this.sp_AS_11141_Hardware_Specifications_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_ExtraTableAdapter
            // 
            this.sp_AS_11000_PL_ExtraTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Extra1TableAdapter
            // 
            this.sp_AS_11000_PL_Extra1TableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Extra2TableAdapter
            // 
            this.sp_AS_11000_PL_Extra2TableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Hardware_Specification_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1059, 554);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1065, 586);
            this.Name = "frm_AS_Hardware_Specification_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Hardware Specifications";
            this.Activated += new System.EventHandler(this.frm_AS_Hardware_Specification_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Hardware_Specification_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Hardware_Specification_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProcessorTypeIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11141HardwareSpecificationsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtraBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RAMTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HardDriveTypeIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtra1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GraphicsCardIDLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtra2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClockSpeedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProcessorTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRAM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHardDriveTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGraphicsCardID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClockSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.LookUpEdit ProcessorTypeIDLookUpEdit;
        private System.Windows.Forms.BindingSource spAS11141HardwareSpecificationsItemBindingSource;
        private DevExpress.XtraEditors.TextEdit RAMTextEdit;
        private DevExpress.XtraEditors.LookUpEdit HardDriveTypeIDLookUpEdit;
        private DevExpress.XtraEditors.LookUpEdit GraphicsCardIDLookUpEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProcessorTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRAM;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHardDriveTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGraphicsCardID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11141_Hardware_Specifications_ItemTableAdapter sp_AS_11141_Hardware_Specifications_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLExtraBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLExtra1BindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLExtra2BindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_ExtraTableAdapter sp_AS_11000_PL_ExtraTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Extra1TableAdapter sp_AS_11000_PL_Extra1TableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Extra2TableAdapter sp_AS_11000_PL_Extra2TableAdapter;
        private DevExpress.XtraEditors.TextEdit ClockSpeedTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClockSpeed;


    }
}

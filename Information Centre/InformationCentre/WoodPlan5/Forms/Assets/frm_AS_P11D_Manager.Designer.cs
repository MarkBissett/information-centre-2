﻿namespace WoodPlan5
{
    partial class frm_AS_P11D_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_P11D_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.spAS11166P11DSentBindingSource = new System.Windows.Forms.BindingSource();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp_AS_11166_P11D_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11166_P11D_ItemTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiRefreshAll = new DevExpress.XtraBars.BarButtonItem();
            this.barSubSendMail = new DevExpress.XtraBars.BarSubItem();
            this.bbiSelectedUnsent = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAllUnsentEmails = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditEmailList = new DevExpress.XtraBars.BarButtonItem();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.unsentManagerGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11166P11DUnsentBindingSource = new System.Windows.Forms.BindingSource();
            this.unsentManagerGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colP11DID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManufacturerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAllocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colListPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdditionalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmissions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.emissionsTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEngineSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationPlate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMake = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelectedForEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceSelected = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.hplAssetList = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sentManagerGridControl = new DevExpress.XtraGrid.GridControl();
            this.sentManagerGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colP11DID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManufacturerID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAllocationID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeper1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperFullName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colListPrice1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdditionalCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmissions1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEngineSize1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationPlate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMake1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelectedForEmail1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceSelected1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colNotes1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.hplActiveSuppressed = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.emissionTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11166P11DSentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unsentManagerGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11166P11DUnsentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unsentManagerGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emissionsTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSelected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hplAssetList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sentManagerGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sentManagerGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSelected1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hplActiveSuppressed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emissionTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.Manager = null;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1372, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 748);
            this.barDockControlBottom.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 722);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1372, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 722);
            // 
            // bbiSave
            // 
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.LargeGlyph")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.Manager = null;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiCancel,
            this.bbiRefreshAll,
            this.barSubSendMail,
            this.bbiSelectedUnsent,
            this.bbiAllUnsentEmails,
            this.bbiEditEmailList});
            this.barManager1.MaxItemId = 36;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("save_16x16.png", "images/save/save_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/save/save_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "save_16x16.png");
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1419, 521, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1372, 722);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1372, 722);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spAS11166P11DSentBindingSource
            // 
            this.spAS11166P11DSentBindingSource.DataMember = "sp_AS_11166_P11D_Item";
            this.spAS11166P11DSentBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // bbiCancel
            // 
            this.bbiCancel.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiCancel.Caption = "Cancel and Close";
            this.bbiCancel.DropDownEnabled = false;
            this.bbiCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCancel.Glyph")));
            this.bbiCancel.Id = 30;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11166_P11D_ItemTableAdapter
            // 
            this.sp_AS_11166_P11D_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 2";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshAll),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubSendMail),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditEmailList)});
            this.bar2.Text = "Custom 2";
            // 
            // bbiRefreshAll
            // 
            this.bbiRefreshAll.Caption = "Refresh All";
            this.bbiRefreshAll.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefreshAll.Glyph")));
            this.bbiRefreshAll.Id = 31;
            this.bbiRefreshAll.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiRefreshAll.LargeGlyph")));
            this.bbiRefreshAll.Name = "bbiRefreshAll";
            this.bbiRefreshAll.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshAll_ItemClick);
            // 
            // barSubSendMail
            // 
            this.barSubSendMail.Caption = "Send Mail";
            this.barSubSendMail.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubSendMail.Glyph")));
            this.barSubSendMail.Id = 32;
            this.barSubSendMail.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barSubSendMail.LargeGlyph")));
            this.barSubSendMail.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectedUnsent),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAllUnsentEmails)});
            this.barSubSendMail.Name = "barSubSendMail";
            this.barSubSendMail.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiSelectedUnsent
            // 
            this.bbiSelectedUnsent.Caption = "Send Selected Unsent Emails";
            this.bbiSelectedUnsent.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSelectedUnsent.Glyph")));
            this.bbiSelectedUnsent.Id = 33;
            this.bbiSelectedUnsent.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSelectedUnsent.LargeGlyph")));
            this.bbiSelectedUnsent.Name = "bbiSelectedUnsent";
            this.bbiSelectedUnsent.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiSelectedUnsent.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectedUnsent_ItemClick);
            // 
            // bbiAllUnsentEmails
            // 
            this.bbiAllUnsentEmails.Caption = "Send All Unsent Emails";
            this.bbiAllUnsentEmails.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAllUnsentEmails.Glyph")));
            this.bbiAllUnsentEmails.Id = 34;
            this.bbiAllUnsentEmails.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiAllUnsentEmails.LargeGlyph")));
            this.bbiAllUnsentEmails.Name = "bbiAllUnsentEmails";
            this.bbiAllUnsentEmails.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiAllUnsentEmails.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAllUnsentEmails_ItemClick);
            // 
            // bbiEditEmailList
            // 
            this.bbiEditEmailList.Caption = "Edit Email List";
            this.bbiEditEmailList.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiEditEmailList.Glyph")));
            this.bbiEditEmailList.Id = 35;
            this.bbiEditEmailList.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiEditEmailList.LargeGlyph")));
            this.bbiEditEmailList.Name = "bbiEditEmailList";
            this.bbiEditEmailList.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiEditEmailList.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiEditEmailList.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditEmailList_ItemClick);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.unsentManagerGridControl);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Unsent Email List";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.sentManagerGridControl);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Sent Email List";
            this.splitContainerControl1.Size = new System.Drawing.Size(1372, 722);
            this.splitContainerControl1.SplitterPosition = 289;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // unsentManagerGridControl
            // 
            this.unsentManagerGridControl.DataSource = this.spAS11166P11DUnsentBindingSource;
            this.unsentManagerGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.unsentManagerGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.unsentManagerGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit P11D Record", "edit")});
            this.unsentManagerGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.p11dGridControl_EmbeddedNavigator_ButtonClick);
            this.unsentManagerGridControl.Location = new System.Drawing.Point(0, 0);
            this.unsentManagerGridControl.MainView = this.unsentManagerGridView;
            this.unsentManagerGridControl.MenuManager = this.barManager1;
            this.unsentManagerGridControl.Name = "unsentManagerGridControl";
            this.unsentManagerGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.hplAssetList,
            this.ceSelected,
            this.emissionsTextEdit});
            this.unsentManagerGridControl.Size = new System.Drawing.Size(1368, 265);
            this.unsentManagerGridControl.TabIndex = 3;
            this.unsentManagerGridControl.Tag = "Active_Keeper_Valid_Email";
            this.unsentManagerGridControl.UseEmbeddedNavigator = true;
            this.unsentManagerGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.unsentManagerGridView,
            this.gridView1});
            // 
            // spAS11166P11DUnsentBindingSource
            // 
            this.spAS11166P11DUnsentBindingSource.DataMember = "sp_AS_11166_P11D_Item";
            this.spAS11166P11DUnsentBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // unsentManagerGridView
            // 
            this.unsentManagerGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colP11DID,
            this.colEquipmentID,
            this.colEquipmentReference,
            this.colManufacturerID,
            this.colKeeperAllocationID,
            this.colTransactionDate,
            this.colKeeperTypeID,
            this.colKeeperType,
            this.colKeeperID,
            this.colKeeper,
            this.colKeeperFullName,
            this.colAllocationDate,
            this.colAllocationEndDate,
            this.colAllocationStatusID,
            this.colAllocationStatus,
            this.colListPrice,
            this.colAdditionalCost,
            this.colEmissions,
            this.colEngineSize,
            this.colFuelType,
            this.colRegistrationPlate,
            this.colRegistrationDate,
            this.colMake,
            this.colModel,
            this.colLastUpdated,
            this.colSelectedForEmail,
            this.colNotes,
            this.colMode,
            this.colRecordID});
            this.unsentManagerGridView.GridControl = this.unsentManagerGridControl;
            this.unsentManagerGridView.Name = "unsentManagerGridView";
            this.unsentManagerGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.unsentManagerGridView.OptionsFind.AlwaysVisible = true;
            this.unsentManagerGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.unsentManagerGridView.OptionsLayout.StoreAppearance = true;
            this.unsentManagerGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.unsentManagerGridView.OptionsSelection.MultiSelect = true;
            this.unsentManagerGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.unsentManagerGridView.OptionsView.ColumnAutoWidth = false;
            this.unsentManagerGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.unsentManagerGridView.OptionsView.ShowGroupPanel = false;
            this.unsentManagerGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.p11dGridView_PopupMenuShowing);
            this.unsentManagerGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.managerGridView_SelectionChanged);
            this.unsentManagerGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.managerGridView_CustomDrawEmptyForeground);
            this.unsentManagerGridView.DoubleClick += new System.EventHandler(this.managerGridView_DoubleClick);
            this.unsentManagerGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colP11DID
            // 
            this.colP11DID.FieldName = "P11DID";
            this.colP11DID.Name = "colP11DID";
            this.colP11DID.OptionsColumn.AllowEdit = false;
            this.colP11DID.OptionsColumn.AllowFocus = false;
            this.colP11DID.OptionsColumn.ReadOnly = true;
            // 
            // colEquipmentID
            // 
            this.colEquipmentID.FieldName = "EquipmentID";
            this.colEquipmentID.Name = "colEquipmentID";
            this.colEquipmentID.OptionsColumn.AllowEdit = false;
            this.colEquipmentID.OptionsColumn.AllowFocus = false;
            this.colEquipmentID.OptionsColumn.ReadOnly = true;
            // 
            // colEquipmentReference
            // 
            this.colEquipmentReference.FieldName = "EquipmentReference";
            this.colEquipmentReference.Name = "colEquipmentReference";
            this.colEquipmentReference.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference.Visible = true;
            this.colEquipmentReference.VisibleIndex = 1;
            this.colEquipmentReference.Width = 124;
            // 
            // colManufacturerID
            // 
            this.colManufacturerID.FieldName = "ManufacturerID";
            this.colManufacturerID.Name = "colManufacturerID";
            this.colManufacturerID.OptionsColumn.AllowEdit = false;
            this.colManufacturerID.OptionsColumn.AllowFocus = false;
            this.colManufacturerID.OptionsColumn.ReadOnly = true;
            this.colManufacturerID.Visible = true;
            this.colManufacturerID.VisibleIndex = 2;
            this.colManufacturerID.Width = 100;
            // 
            // colKeeperAllocationID
            // 
            this.colKeeperAllocationID.FieldName = "KeeperAllocationID";
            this.colKeeperAllocationID.Name = "colKeeperAllocationID";
            this.colKeeperAllocationID.OptionsColumn.AllowEdit = false;
            this.colKeeperAllocationID.OptionsColumn.AllowFocus = false;
            this.colKeeperAllocationID.OptionsColumn.ReadOnly = true;
            this.colKeeperAllocationID.Width = 118;
            // 
            // colTransactionDate
            // 
            this.colTransactionDate.FieldName = "TransactionDate";
            this.colTransactionDate.Name = "colTransactionDate";
            this.colTransactionDate.OptionsColumn.AllowEdit = false;
            this.colTransactionDate.OptionsColumn.AllowFocus = false;
            this.colTransactionDate.OptionsColumn.ReadOnly = true;
            this.colTransactionDate.Visible = true;
            this.colTransactionDate.VisibleIndex = 3;
            this.colTransactionDate.Width = 103;
            // 
            // colKeeperTypeID
            // 
            this.colKeeperTypeID.FieldName = "KeeperTypeID";
            this.colKeeperTypeID.Name = "colKeeperTypeID";
            this.colKeeperTypeID.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID.Width = 96;
            // 
            // colKeeperType
            // 
            this.colKeeperType.FieldName = "KeeperType";
            this.colKeeperType.Name = "colKeeperType";
            this.colKeeperType.OptionsColumn.AllowEdit = false;
            this.colKeeperType.OptionsColumn.AllowFocus = false;
            this.colKeeperType.OptionsColumn.ReadOnly = true;
            this.colKeeperType.Visible = true;
            this.colKeeperType.VisibleIndex = 4;
            this.colKeeperType.Width = 82;
            // 
            // colKeeperID
            // 
            this.colKeeperID.FieldName = "KeeperID";
            this.colKeeperID.Name = "colKeeperID";
            this.colKeeperID.OptionsColumn.AllowEdit = false;
            this.colKeeperID.OptionsColumn.AllowFocus = false;
            this.colKeeperID.OptionsColumn.ReadOnly = true;
            // 
            // colKeeper
            // 
            this.colKeeper.FieldName = "Keeper";
            this.colKeeper.Name = "colKeeper";
            this.colKeeper.OptionsColumn.AllowEdit = false;
            this.colKeeper.OptionsColumn.AllowFocus = false;
            this.colKeeper.OptionsColumn.ReadOnly = true;
            this.colKeeper.Visible = true;
            this.colKeeper.VisibleIndex = 5;
            // 
            // colKeeperFullName
            // 
            this.colKeeperFullName.FieldName = "KeeperFullName";
            this.colKeeperFullName.Name = "colKeeperFullName";
            this.colKeeperFullName.OptionsColumn.AllowEdit = false;
            this.colKeeperFullName.OptionsColumn.AllowFocus = false;
            this.colKeeperFullName.OptionsColumn.ReadOnly = true;
            this.colKeeperFullName.Visible = true;
            this.colKeeperFullName.VisibleIndex = 6;
            this.colKeeperFullName.Width = 104;
            // 
            // colAllocationDate
            // 
            this.colAllocationDate.FieldName = "AllocationDate";
            this.colAllocationDate.Name = "colAllocationDate";
            this.colAllocationDate.OptionsColumn.AllowEdit = false;
            this.colAllocationDate.OptionsColumn.AllowFocus = false;
            this.colAllocationDate.OptionsColumn.ReadOnly = true;
            this.colAllocationDate.Visible = true;
            this.colAllocationDate.VisibleIndex = 7;
            this.colAllocationDate.Width = 93;
            // 
            // colAllocationEndDate
            // 
            this.colAllocationEndDate.FieldName = "AllocationEndDate";
            this.colAllocationEndDate.Name = "colAllocationEndDate";
            this.colAllocationEndDate.OptionsColumn.AllowEdit = false;
            this.colAllocationEndDate.OptionsColumn.AllowFocus = false;
            this.colAllocationEndDate.OptionsColumn.ReadOnly = true;
            this.colAllocationEndDate.Visible = true;
            this.colAllocationEndDate.VisibleIndex = 8;
            this.colAllocationEndDate.Width = 114;
            // 
            // colAllocationStatusID
            // 
            this.colAllocationStatusID.FieldName = "AllocationStatusID";
            this.colAllocationStatusID.Name = "colAllocationStatusID";
            this.colAllocationStatusID.OptionsColumn.AllowEdit = false;
            this.colAllocationStatusID.OptionsColumn.AllowFocus = false;
            this.colAllocationStatusID.OptionsColumn.ReadOnly = true;
            this.colAllocationStatusID.Width = 115;
            // 
            // colAllocationStatus
            // 
            this.colAllocationStatus.FieldName = "AllocationStatus";
            this.colAllocationStatus.Name = "colAllocationStatus";
            this.colAllocationStatus.OptionsColumn.AllowEdit = false;
            this.colAllocationStatus.OptionsColumn.AllowFocus = false;
            this.colAllocationStatus.OptionsColumn.ReadOnly = true;
            this.colAllocationStatus.Visible = true;
            this.colAllocationStatus.VisibleIndex = 9;
            this.colAllocationStatus.Width = 101;
            // 
            // colListPrice
            // 
            this.colListPrice.DisplayFormat.FormatString = "c2";
            this.colListPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colListPrice.FieldName = "ListPrice";
            this.colListPrice.Name = "colListPrice";
            this.colListPrice.OptionsColumn.AllowEdit = false;
            this.colListPrice.OptionsColumn.AllowFocus = false;
            this.colListPrice.OptionsColumn.ReadOnly = true;
            this.colListPrice.Visible = true;
            this.colListPrice.VisibleIndex = 10;
            // 
            // colAdditionalCost
            // 
            this.colAdditionalCost.DisplayFormat.FormatString = "c2";
            this.colAdditionalCost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colAdditionalCost.FieldName = "AdditionalCost";
            this.colAdditionalCost.Name = "colAdditionalCost";
            this.colAdditionalCost.OptionsColumn.AllowEdit = false;
            this.colAdditionalCost.OptionsColumn.AllowFocus = false;
            this.colAdditionalCost.OptionsColumn.ReadOnly = true;
            this.colAdditionalCost.Visible = true;
            this.colAdditionalCost.VisibleIndex = 11;
            this.colAdditionalCost.Width = 93;
            // 
            // colEmissions
            // 
            this.colEmissions.ColumnEdit = this.emissionsTextEdit;
            this.colEmissions.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colEmissions.FieldName = "Emissions";
            this.colEmissions.Name = "colEmissions";
            this.colEmissions.OptionsColumn.AllowEdit = false;
            this.colEmissions.OptionsColumn.AllowFocus = false;
            this.colEmissions.OptionsColumn.ReadOnly = true;
            this.colEmissions.Visible = true;
            this.colEmissions.VisibleIndex = 12;
            // 
            // emissionsTextEdit
            // 
            this.emissionsTextEdit.AutoHeight = false;
            this.emissionsTextEdit.Mask.EditMask = "#######0 g/km";
            this.emissionsTextEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Custom;
            this.emissionsTextEdit.Mask.UseMaskAsDisplayFormat = true;
            this.emissionsTextEdit.Name = "emissionsTextEdit";
            // 
            // colEngineSize
            // 
            this.colEngineSize.FieldName = "EngineSize";
            this.colEngineSize.Name = "colEngineSize";
            this.colEngineSize.OptionsColumn.AllowEdit = false;
            this.colEngineSize.OptionsColumn.AllowFocus = false;
            this.colEngineSize.OptionsColumn.ReadOnly = true;
            this.colEngineSize.Visible = true;
            this.colEngineSize.VisibleIndex = 13;
            // 
            // colFuelType
            // 
            this.colFuelType.FieldName = "FuelType";
            this.colFuelType.Name = "colFuelType";
            this.colFuelType.OptionsColumn.AllowEdit = false;
            this.colFuelType.OptionsColumn.AllowFocus = false;
            this.colFuelType.OptionsColumn.ReadOnly = true;
            this.colFuelType.Visible = true;
            this.colFuelType.VisibleIndex = 14;
            // 
            // colRegistrationPlate
            // 
            this.colRegistrationPlate.FieldName = "RegistrationPlate";
            this.colRegistrationPlate.Name = "colRegistrationPlate";
            this.colRegistrationPlate.OptionsColumn.AllowEdit = false;
            this.colRegistrationPlate.OptionsColumn.AllowFocus = false;
            this.colRegistrationPlate.OptionsColumn.ReadOnly = true;
            this.colRegistrationPlate.Visible = true;
            this.colRegistrationPlate.VisibleIndex = 15;
            this.colRegistrationPlate.Width = 106;
            // 
            // colRegistrationDate
            // 
            this.colRegistrationDate.FieldName = "RegistrationDate";
            this.colRegistrationDate.Name = "colRegistrationDate";
            this.colRegistrationDate.OptionsColumn.AllowEdit = false;
            this.colRegistrationDate.OptionsColumn.AllowFocus = false;
            this.colRegistrationDate.OptionsColumn.ReadOnly = true;
            this.colRegistrationDate.Visible = true;
            this.colRegistrationDate.VisibleIndex = 16;
            this.colRegistrationDate.Width = 105;
            // 
            // colMake
            // 
            this.colMake.FieldName = "Make";
            this.colMake.Name = "colMake";
            this.colMake.OptionsColumn.AllowEdit = false;
            this.colMake.OptionsColumn.AllowFocus = false;
            this.colMake.OptionsColumn.ReadOnly = true;
            this.colMake.Visible = true;
            this.colMake.VisibleIndex = 17;
            // 
            // colModel
            // 
            this.colModel.FieldName = "Model";
            this.colModel.Name = "colModel";
            this.colModel.OptionsColumn.AllowEdit = false;
            this.colModel.OptionsColumn.AllowFocus = false;
            this.colModel.OptionsColumn.ReadOnly = true;
            this.colModel.Visible = true;
            this.colModel.VisibleIndex = 18;
            // 
            // colLastUpdated
            // 
            this.colLastUpdated.FieldName = "LastUpdated";
            this.colLastUpdated.Name = "colLastUpdated";
            this.colLastUpdated.OptionsColumn.AllowEdit = false;
            this.colLastUpdated.OptionsColumn.AllowFocus = false;
            this.colLastUpdated.OptionsColumn.ReadOnly = true;
            this.colLastUpdated.Visible = true;
            this.colLastUpdated.VisibleIndex = 19;
            this.colLastUpdated.Width = 85;
            // 
            // colSelectedForEmail
            // 
            this.colSelectedForEmail.ColumnEdit = this.ceSelected;
            this.colSelectedForEmail.FieldName = "SelectedForEmail";
            this.colSelectedForEmail.Name = "colSelectedForEmail";
            this.colSelectedForEmail.Visible = true;
            this.colSelectedForEmail.VisibleIndex = 0;
            this.colSelectedForEmail.Width = 108;
            // 
            // ceSelected
            // 
            this.ceSelected.AutoHeight = false;
            this.ceSelected.Caption = "Check";
            this.ceSelected.Name = "ceSelected";
            this.ceSelected.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceSelected_EditValueChanging);
            // 
            // colNotes
            // 
            this.colNotes.FieldName = "Notes";
            this.colNotes.Name = "colNotes";
            this.colNotes.OptionsColumn.AllowEdit = false;
            this.colNotes.OptionsColumn.AllowFocus = false;
            this.colNotes.OptionsColumn.ReadOnly = true;
            this.colNotes.Visible = true;
            this.colNotes.VisibleIndex = 20;
            this.colNotes.Width = 423;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // hplAssetList
            // 
            this.hplAssetList.AutoHeight = false;
            this.hplAssetList.Name = "hplAssetList";
            this.hplAssetList.SingleClick = true;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.unsentManagerGridControl;
            this.gridView1.Name = "gridView1";
            // 
            // sentManagerGridControl
            // 
            this.sentManagerGridControl.DataSource = this.spAS11166P11DSentBindingSource;
            this.sentManagerGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.sentManagerGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.sentManagerGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit P11D Record", "edit")});
            this.sentManagerGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.p11dGridControl_EmbeddedNavigator_ButtonClick);
            this.sentManagerGridControl.Location = new System.Drawing.Point(0, 0);
            this.sentManagerGridControl.MainView = this.sentManagerGridView;
            this.sentManagerGridControl.MenuManager = this.barManager1;
            this.sentManagerGridControl.Name = "sentManagerGridControl";
            this.sentManagerGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.hplActiveSuppressed,
            this.ceSelected1,
            this.emissionTextEdit});
            this.sentManagerGridControl.Size = new System.Drawing.Size(1368, 403);
            this.sentManagerGridControl.TabIndex = 4;
            this.sentManagerGridControl.Tag = "Active_UnTickedKeeper";
            this.sentManagerGridControl.UseEmbeddedNavigator = true;
            this.sentManagerGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.sentManagerGridView,
            this.gridView5});
            // 
            // sentManagerGridView
            // 
            this.sentManagerGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colP11DID1,
            this.colEquipmentID1,
            this.colEquipmentReference1,
            this.colManufacturerID1,
            this.colKeeperAllocationID1,
            this.colTransactionDate1,
            this.colKeeperTypeID1,
            this.colKeeperType1,
            this.colKeeperID1,
            this.colKeeper1,
            this.colKeeperFullName1,
            this.colAllocationDate1,
            this.colAllocationEndDate1,
            this.colAllocationStatusID1,
            this.colAllocationStatus1,
            this.colListPrice1,
            this.colAdditionalCost1,
            this.colEmissions1,
            this.colEngineSize1,
            this.colFuelType1,
            this.colRegistrationPlate1,
            this.colRegistrationDate1,
            this.colMake1,
            this.colModel1,
            this.colLastUpdated1,
            this.colSelectedForEmail1,
            this.colNotes1,
            this.colMode1,
            this.colRecordID1});
            this.sentManagerGridView.GridControl = this.sentManagerGridControl;
            this.sentManagerGridView.Name = "sentManagerGridView";
            this.sentManagerGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.sentManagerGridView.OptionsFind.AlwaysVisible = true;
            this.sentManagerGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.sentManagerGridView.OptionsLayout.StoreAppearance = true;
            this.sentManagerGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.sentManagerGridView.OptionsSelection.MultiSelect = true;
            this.sentManagerGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.sentManagerGridView.OptionsView.ColumnAutoWidth = false;
            this.sentManagerGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.sentManagerGridView.OptionsView.ShowGroupPanel = false;
            this.sentManagerGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.p11dGridView_PopupMenuShowing);
            this.sentManagerGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.managerGridView_SelectionChanged);
            this.sentManagerGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.managerGridView_CustomDrawEmptyForeground);
            this.sentManagerGridView.DoubleClick += new System.EventHandler(this.managerGridView_DoubleClick);
            this.sentManagerGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colP11DID1
            // 
            this.colP11DID1.FieldName = "P11DID";
            this.colP11DID1.Name = "colP11DID1";
            this.colP11DID1.OptionsColumn.AllowEdit = false;
            this.colP11DID1.OptionsColumn.AllowFocus = false;
            this.colP11DID1.OptionsColumn.ReadOnly = true;
            // 
            // colEquipmentID1
            // 
            this.colEquipmentID1.FieldName = "EquipmentID";
            this.colEquipmentID1.Name = "colEquipmentID1";
            this.colEquipmentID1.OptionsColumn.AllowEdit = false;
            this.colEquipmentID1.OptionsColumn.AllowFocus = false;
            this.colEquipmentID1.OptionsColumn.ReadOnly = true;
            // 
            // colEquipmentReference1
            // 
            this.colEquipmentReference1.FieldName = "EquipmentReference";
            this.colEquipmentReference1.Name = "colEquipmentReference1";
            this.colEquipmentReference1.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference1.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference1.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference1.Visible = true;
            this.colEquipmentReference1.VisibleIndex = 1;
            this.colEquipmentReference1.Width = 124;
            // 
            // colManufacturerID1
            // 
            this.colManufacturerID1.FieldName = "ManufacturerID";
            this.colManufacturerID1.Name = "colManufacturerID1";
            this.colManufacturerID1.OptionsColumn.AllowEdit = false;
            this.colManufacturerID1.OptionsColumn.AllowFocus = false;
            this.colManufacturerID1.OptionsColumn.ReadOnly = true;
            this.colManufacturerID1.Visible = true;
            this.colManufacturerID1.VisibleIndex = 2;
            this.colManufacturerID1.Width = 100;
            // 
            // colKeeperAllocationID1
            // 
            this.colKeeperAllocationID1.FieldName = "KeeperAllocationID";
            this.colKeeperAllocationID1.Name = "colKeeperAllocationID1";
            this.colKeeperAllocationID1.OptionsColumn.AllowEdit = false;
            this.colKeeperAllocationID1.OptionsColumn.AllowFocus = false;
            this.colKeeperAllocationID1.OptionsColumn.ReadOnly = true;
            this.colKeeperAllocationID1.Width = 118;
            // 
            // colTransactionDate1
            // 
            this.colTransactionDate1.FieldName = "TransactionDate";
            this.colTransactionDate1.Name = "colTransactionDate1";
            this.colTransactionDate1.OptionsColumn.AllowEdit = false;
            this.colTransactionDate1.OptionsColumn.AllowFocus = false;
            this.colTransactionDate1.OptionsColumn.ReadOnly = true;
            this.colTransactionDate1.Visible = true;
            this.colTransactionDate1.VisibleIndex = 3;
            this.colTransactionDate1.Width = 103;
            // 
            // colKeeperTypeID1
            // 
            this.colKeeperTypeID1.FieldName = "KeeperTypeID";
            this.colKeeperTypeID1.Name = "colKeeperTypeID1";
            this.colKeeperTypeID1.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID1.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID1.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID1.Width = 96;
            // 
            // colKeeperType1
            // 
            this.colKeeperType1.FieldName = "KeeperType";
            this.colKeeperType1.Name = "colKeeperType1";
            this.colKeeperType1.OptionsColumn.AllowEdit = false;
            this.colKeeperType1.OptionsColumn.AllowFocus = false;
            this.colKeeperType1.OptionsColumn.ReadOnly = true;
            this.colKeeperType1.Visible = true;
            this.colKeeperType1.VisibleIndex = 4;
            this.colKeeperType1.Width = 82;
            // 
            // colKeeperID1
            // 
            this.colKeeperID1.FieldName = "KeeperID";
            this.colKeeperID1.Name = "colKeeperID1";
            this.colKeeperID1.OptionsColumn.AllowEdit = false;
            this.colKeeperID1.OptionsColumn.AllowFocus = false;
            this.colKeeperID1.OptionsColumn.ReadOnly = true;
            // 
            // colKeeper1
            // 
            this.colKeeper1.FieldName = "Keeper";
            this.colKeeper1.Name = "colKeeper1";
            this.colKeeper1.OptionsColumn.AllowEdit = false;
            this.colKeeper1.OptionsColumn.AllowFocus = false;
            this.colKeeper1.OptionsColumn.ReadOnly = true;
            this.colKeeper1.Visible = true;
            this.colKeeper1.VisibleIndex = 5;
            // 
            // colKeeperFullName1
            // 
            this.colKeeperFullName1.FieldName = "KeeperFullName";
            this.colKeeperFullName1.Name = "colKeeperFullName1";
            this.colKeeperFullName1.OptionsColumn.AllowEdit = false;
            this.colKeeperFullName1.OptionsColumn.AllowFocus = false;
            this.colKeeperFullName1.OptionsColumn.ReadOnly = true;
            this.colKeeperFullName1.Visible = true;
            this.colKeeperFullName1.VisibleIndex = 6;
            this.colKeeperFullName1.Width = 104;
            // 
            // colAllocationDate1
            // 
            this.colAllocationDate1.FieldName = "AllocationDate";
            this.colAllocationDate1.Name = "colAllocationDate1";
            this.colAllocationDate1.OptionsColumn.AllowEdit = false;
            this.colAllocationDate1.OptionsColumn.AllowFocus = false;
            this.colAllocationDate1.OptionsColumn.ReadOnly = true;
            this.colAllocationDate1.Visible = true;
            this.colAllocationDate1.VisibleIndex = 7;
            this.colAllocationDate1.Width = 93;
            // 
            // colAllocationEndDate1
            // 
            this.colAllocationEndDate1.FieldName = "AllocationEndDate";
            this.colAllocationEndDate1.Name = "colAllocationEndDate1";
            this.colAllocationEndDate1.OptionsColumn.AllowEdit = false;
            this.colAllocationEndDate1.OptionsColumn.AllowFocus = false;
            this.colAllocationEndDate1.OptionsColumn.ReadOnly = true;
            this.colAllocationEndDate1.Visible = true;
            this.colAllocationEndDate1.VisibleIndex = 8;
            this.colAllocationEndDate1.Width = 114;
            // 
            // colAllocationStatusID1
            // 
            this.colAllocationStatusID1.FieldName = "AllocationStatusID";
            this.colAllocationStatusID1.Name = "colAllocationStatusID1";
            this.colAllocationStatusID1.OptionsColumn.AllowEdit = false;
            this.colAllocationStatusID1.OptionsColumn.AllowFocus = false;
            this.colAllocationStatusID1.OptionsColumn.ReadOnly = true;
            this.colAllocationStatusID1.Width = 115;
            // 
            // colAllocationStatus1
            // 
            this.colAllocationStatus1.FieldName = "AllocationStatus";
            this.colAllocationStatus1.Name = "colAllocationStatus1";
            this.colAllocationStatus1.OptionsColumn.AllowEdit = false;
            this.colAllocationStatus1.OptionsColumn.AllowFocus = false;
            this.colAllocationStatus1.OptionsColumn.ReadOnly = true;
            this.colAllocationStatus1.Visible = true;
            this.colAllocationStatus1.VisibleIndex = 9;
            this.colAllocationStatus1.Width = 101;
            // 
            // colListPrice1
            // 
            this.colListPrice1.DisplayFormat.FormatString = "c2";
            this.colListPrice1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colListPrice1.FieldName = "ListPrice";
            this.colListPrice1.Name = "colListPrice1";
            this.colListPrice1.OptionsColumn.AllowEdit = false;
            this.colListPrice1.OptionsColumn.AllowFocus = false;
            this.colListPrice1.OptionsColumn.ReadOnly = true;
            this.colListPrice1.Visible = true;
            this.colListPrice1.VisibleIndex = 10;
            // 
            // colAdditionalCost1
            // 
            this.colAdditionalCost1.DisplayFormat.FormatString = "c2";
            this.colAdditionalCost1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colAdditionalCost1.FieldName = "AdditionalCost";
            this.colAdditionalCost1.Name = "colAdditionalCost1";
            this.colAdditionalCost1.OptionsColumn.AllowEdit = false;
            this.colAdditionalCost1.OptionsColumn.AllowFocus = false;
            this.colAdditionalCost1.OptionsColumn.ReadOnly = true;
            this.colAdditionalCost1.Visible = true;
            this.colAdditionalCost1.VisibleIndex = 11;
            this.colAdditionalCost1.Width = 93;
            // 
            // colEmissions1
            // 
            this.colEmissions1.FieldName = "Emissions";
            this.colEmissions1.Name = "colEmissions1";
            this.colEmissions1.OptionsColumn.AllowEdit = false;
            this.colEmissions1.OptionsColumn.AllowFocus = false;
            this.colEmissions1.OptionsColumn.ReadOnly = true;
            this.colEmissions1.Visible = true;
            this.colEmissions1.VisibleIndex = 12;
            // 
            // colEngineSize1
            // 
            this.colEngineSize1.FieldName = "EngineSize";
            this.colEngineSize1.Name = "colEngineSize1";
            this.colEngineSize1.OptionsColumn.AllowEdit = false;
            this.colEngineSize1.OptionsColumn.AllowFocus = false;
            this.colEngineSize1.OptionsColumn.ReadOnly = true;
            this.colEngineSize1.Visible = true;
            this.colEngineSize1.VisibleIndex = 13;
            // 
            // colFuelType1
            // 
            this.colFuelType1.FieldName = "FuelType";
            this.colFuelType1.Name = "colFuelType1";
            this.colFuelType1.OptionsColumn.AllowEdit = false;
            this.colFuelType1.OptionsColumn.AllowFocus = false;
            this.colFuelType1.OptionsColumn.ReadOnly = true;
            this.colFuelType1.Visible = true;
            this.colFuelType1.VisibleIndex = 14;
            // 
            // colRegistrationPlate1
            // 
            this.colRegistrationPlate1.FieldName = "RegistrationPlate";
            this.colRegistrationPlate1.Name = "colRegistrationPlate1";
            this.colRegistrationPlate1.OptionsColumn.AllowEdit = false;
            this.colRegistrationPlate1.OptionsColumn.AllowFocus = false;
            this.colRegistrationPlate1.OptionsColumn.ReadOnly = true;
            this.colRegistrationPlate1.Visible = true;
            this.colRegistrationPlate1.VisibleIndex = 15;
            this.colRegistrationPlate1.Width = 106;
            // 
            // colRegistrationDate1
            // 
            this.colRegistrationDate1.FieldName = "RegistrationDate";
            this.colRegistrationDate1.Name = "colRegistrationDate1";
            this.colRegistrationDate1.OptionsColumn.AllowEdit = false;
            this.colRegistrationDate1.OptionsColumn.AllowFocus = false;
            this.colRegistrationDate1.OptionsColumn.ReadOnly = true;
            this.colRegistrationDate1.Visible = true;
            this.colRegistrationDate1.VisibleIndex = 16;
            this.colRegistrationDate1.Width = 105;
            // 
            // colMake1
            // 
            this.colMake1.FieldName = "Make";
            this.colMake1.Name = "colMake1";
            this.colMake1.OptionsColumn.AllowEdit = false;
            this.colMake1.OptionsColumn.AllowFocus = false;
            this.colMake1.OptionsColumn.ReadOnly = true;
            this.colMake1.Visible = true;
            this.colMake1.VisibleIndex = 17;
            // 
            // colModel1
            // 
            this.colModel1.FieldName = "Model";
            this.colModel1.Name = "colModel1";
            this.colModel1.OptionsColumn.AllowEdit = false;
            this.colModel1.OptionsColumn.AllowFocus = false;
            this.colModel1.OptionsColumn.ReadOnly = true;
            this.colModel1.Visible = true;
            this.colModel1.VisibleIndex = 18;
            // 
            // colLastUpdated1
            // 
            this.colLastUpdated1.FieldName = "LastUpdated";
            this.colLastUpdated1.Name = "colLastUpdated1";
            this.colLastUpdated1.OptionsColumn.AllowEdit = false;
            this.colLastUpdated1.OptionsColumn.AllowFocus = false;
            this.colLastUpdated1.OptionsColumn.ReadOnly = true;
            this.colLastUpdated1.Visible = true;
            this.colLastUpdated1.VisibleIndex = 19;
            this.colLastUpdated1.Width = 85;
            // 
            // colSelectedForEmail1
            // 
            this.colSelectedForEmail1.ColumnEdit = this.ceSelected1;
            this.colSelectedForEmail1.FieldName = "SelectedForEmail";
            this.colSelectedForEmail1.Name = "colSelectedForEmail1";
            this.colSelectedForEmail1.Visible = true;
            this.colSelectedForEmail1.VisibleIndex = 0;
            this.colSelectedForEmail1.Width = 108;
            // 
            // ceSelected1
            // 
            this.ceSelected1.AutoHeight = false;
            this.ceSelected1.Caption = "Check";
            this.ceSelected1.Name = "ceSelected1";
            this.ceSelected1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceSelected_EditValueChanging);
            // 
            // colNotes1
            // 
            this.colNotes1.FieldName = "Notes";
            this.colNotes1.Name = "colNotes1";
            this.colNotes1.OptionsColumn.AllowEdit = false;
            this.colNotes1.OptionsColumn.AllowFocus = false;
            this.colNotes1.OptionsColumn.ReadOnly = true;
            this.colNotes1.Visible = true;
            this.colNotes1.VisibleIndex = 20;
            this.colNotes1.Width = 394;
            // 
            // colMode1
            // 
            this.colMode1.FieldName = "Mode";
            this.colMode1.Name = "colMode1";
            this.colMode1.OptionsColumn.AllowEdit = false;
            this.colMode1.OptionsColumn.AllowFocus = false;
            this.colMode1.OptionsColumn.ReadOnly = true;
            // 
            // colRecordID1
            // 
            this.colRecordID1.FieldName = "RecordID";
            this.colRecordID1.Name = "colRecordID1";
            this.colRecordID1.OptionsColumn.AllowEdit = false;
            this.colRecordID1.OptionsColumn.AllowFocus = false;
            this.colRecordID1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // hplActiveSuppressed
            // 
            this.hplActiveSuppressed.AutoHeight = false;
            this.hplActiveSuppressed.Name = "hplActiveSuppressed";
            this.hplActiveSuppressed.SingleClick = true;
            // 
            // emissionTextEdit
            // 
            this.emissionTextEdit.AutoHeight = false;
            this.emissionTextEdit.Mask.EditMask = "#######0 g/km";
            this.emissionTextEdit.Mask.UseMaskAsDisplayFormat = true;
            this.emissionTextEdit.Name = "emissionTextEdit";
            // 
            // gridView5
            // 
            this.gridView5.GridControl = this.sentManagerGridControl;
            this.gridView5.Name = "gridView5";
            // 
            // frm_AS_P11D_Manager
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1372, 748);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_P11D_Manager";
            this.Text = "P11D Manager";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.frm_AS_Supplier_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_P11D_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11166P11DSentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.unsentManagerGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11166P11DUnsentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unsentManagerGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emissionsTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSelected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hplAssetList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sentManagerGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sentManagerGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSelected1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hplActiveSuppressed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emissionTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource spAS11166P11DSentBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11166_P11D_ItemTableAdapter sp_AS_11166_P11D_ItemTableAdapter;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshAll;
        private DevExpress.XtraBars.BarSubItem barSubSendMail;
        private DevExpress.XtraBars.BarButtonItem bbiSelectedUnsent;
        private DevExpress.XtraBars.BarButtonItem bbiAllUnsentEmails;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl unsentManagerGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView unsentManagerGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceSelected;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit hplAssetList;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl sentManagerGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView sentManagerGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceSelected1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit hplActiveSuppressed;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private System.Windows.Forms.BindingSource spAS11166P11DUnsentBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colP11DID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colManufacturerID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAllocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colListPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colAdditionalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEmissions;
        private DevExpress.XtraGrid.Columns.GridColumn colEngineSize;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelType;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationPlate;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMake;
        private DevExpress.XtraGrid.Columns.GridColumn colModel;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdated;
        private DevExpress.XtraGrid.Columns.GridColumn colSelectedForEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colP11DID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colManufacturerID1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAllocationID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperID1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeper1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperFullName1;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colListPrice1;
        private DevExpress.XtraGrid.Columns.GridColumn colAdditionalCost1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmissions1;
        private DevExpress.XtraGrid.Columns.GridColumn colEngineSize1;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelType1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationPlate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colMake1;
        private DevExpress.XtraGrid.Columns.GridColumn colModel1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdated1;
        private DevExpress.XtraGrid.Columns.GridColumn colSelectedForEmail1;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID1;
        private DevExpress.XtraBars.BarButtonItem bbiEditEmailList;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit emissionsTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit emissionTextEdit;
    }
}

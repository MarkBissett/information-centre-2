﻿namespace WoodPlan5.Forms.Assets
{
    public interface IWizardViewModel
    {
        void PageCompleted();
        bool CanPrev();
        void Prev();
        bool CanNext();
        void Next();
        IWizardPageViewModel CurrentPage { get; }
        bool CanClose();
        void Close(bool isClosing);

        //string UserName { get; set; }
    }

    public interface IWizardPageViewModel
    {
        bool pageIsValid { get; }
        bool IsComplete { get; }
        bool CanReturn { get; }
    }
}

namespace WoodPlan5
{
    partial class frm_AS_Rental_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Rental_Manager));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.rentalGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11102RentalManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_Core = new WoodPlan5.DataSet_AS_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.rentalGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMake = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModelSpecifications = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUniqueReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomerReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colListPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalSpecification = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEngineSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmissions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnresolvedWarning = new DevExpress.XtraGrid.Columns.GridColumn();
            this.unresolvedCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colAvailability = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActiveKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ArchiveCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.equipmentChildTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.rentalDetailsTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.rentalDetailsGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11108RentalDetailsItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.rentalDetailsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRentalDetailsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalCategory1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalSpecificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRentalSpecification1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCustomerReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSetAsMain = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceSetAsMain = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colNotes6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.memNotes = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NoDetailsTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.roadTaxTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.roadTaxGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11096RoadTaxItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.roadTaxGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRoadTaxID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaxExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTaxPeriodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaxPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.trackerTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.trackerGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11023TrackerListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.trackerGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTrackerInformationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrackerVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrackerReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMake1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOdometerMetres = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOdometerMiles = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMPG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatestStartJourney = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatestEndJourney = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedKPH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedMPH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlaceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostSect = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGeofenceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrict = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.keeperTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.keeperGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11044KeeperAllocationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.keeperGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKeeperAllocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueKeeperNotes = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.transactionTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.transactionGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11041TransactionItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.transactionsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransactionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedOnExchequer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetTransactionPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceOnExchequer = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.notificationTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.notificationGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11126NotificationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.notificationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNotificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotificationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriorityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateToRemind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.coverTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.coverGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11075CoverItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.coverGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCoverID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEquipmentReference16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoverType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoverTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolicy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnnualCoverCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRenewalDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExcess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.serviceIntervalTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.serviceIntervalGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11054ServiceDataItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.serviceIntervalGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colServiceDataID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceIntervalScheduleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceUnitsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeUnitsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarrantyPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarrantyEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarrantyEndDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceDataNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceDueDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlertWithinXDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlertWithinXTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIntervalScheduleNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsCurrentInterval = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.workDetailTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.workDetailGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11078WorkDetailItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.workDetailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkDetailID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentMileage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkCompletionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceIntervalID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.incidentTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.incidentGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11081IncidentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.incidentGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateHappened = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRepairDueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRepairDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeverityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeverity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWitness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAtFaultID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAtFault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFollowUpDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNotes4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.incidentNotesMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colMode3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.purposeTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.purposeGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11084PurposeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.purposeGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentPurposeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurpose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurposeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.fuelCardTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.fuelCardGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11089FuelCardItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fuelCardGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFuelCardID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCardNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationMark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeper1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAllocationID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCardStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCardStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit19 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.speedingTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.speedingGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11105SpeedingItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.speedingGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSpeedingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedMPH1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravelTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlaceName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrict1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReported = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceReported = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMode17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LinkedDocumentsTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlLinkedDocs = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridViewLinkedDocs = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocs = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLinkedDocDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEditLinkedDocs = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDocumentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spAS11050DepreciationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11005VehicleItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11047EquipmentBillingItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.tableAdapterManager1 = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.spAS11002EquipmentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11002_Equipment_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter();
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter();
            this.sp_AS_11041_Transaction_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter();
            this.sp_AS_11047_Equipment_Billing_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11047_Equipment_Billing_ItemTableAdapter();
            this.sp_AS_11050_Depreciation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11050_Depreciation_ItemTableAdapter();
            this.sp_AS_11075_Cover_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11075_Cover_ItemTableAdapter();
            this.sp_AS_11078_Work_Detail_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter();
            this.sp_AS_11084_Purpose_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11084_Purpose_ItemTableAdapter();
            this.sp_AS_11089_Fuel_Card_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11089_Fuel_Card_ItemTableAdapter();
            this.sp_AS_11044_Keeper_Allocation_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ListTableAdapter();
            this.sp_AS_11081_Incident_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter();
            this.sp_AS_11005_Vehicle_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.spAS11023VerilocationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11023_Verilocation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11023_Verilocation_ItemTableAdapter();
            this.sp_AS_11023_Tracker_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11023_Tracker_ListTableAdapter();
            this.sp_AS_11096_Road_Tax_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11096_Road_Tax_ItemTableAdapter();
            this.spAS11000PLGeneralBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_PL_GeneralTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_GeneralTableAdapter();
            this.sp_AS_11105_Speeding_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11105_Speeding_ItemTableAdapter();
            this.sp_AS_11000_PL_Incident_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_StatusTableAdapter();
            this.sp_AS_11000_PL_Incident_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_TypeTableAdapter();
            this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_Repair_DueTableAdapter();
            this.sp_AS_11000_PL_SeverityTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_SeverityTableAdapter();
            this.sp_AS_11000_PL_Legal_ActionTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Legal_ActionTableAdapter();
            this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_At_FaultTableAdapter();
            this.sp_AS_11108_Rental_Details_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11108_Rental_Details_ItemTableAdapter();
            this.spAS11102RentalItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11102_Rental_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11102_Rental_ItemTableAdapter();
            this.sp_AS_11102_Rental_ManagerTableAdapter = new WoodPlan5.DataSet_AS_CoreTableAdapters.sp_AS_11102_Rental_ManagerTableAdapter();
            this.sp_AS_11126_Notification_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11126_Notification_ItemTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExport = new DevExpress.XtraBars.BarSubItem();
            this.bbiExportExcel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportExcelOld = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEmail = new DevExpress.XtraBars.BarButtonItem();
            this.sp_AS_11054_Service_Data_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Data_ItemTableAdapter();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rentalGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11102RentalManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentalGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unresolvedCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArchiveCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentChildTabControl)).BeginInit();
            this.equipmentChildTabControl.SuspendLayout();
            this.rentalDetailsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rentalDetailsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11108RentalDetailsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentalDetailsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSetAsMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes)).BeginInit();
            this.NoDetailsTabPage.SuspendLayout();
            this.roadTaxTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11096RoadTaxItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit4)).BeginInit();
            this.trackerTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11023TrackerListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            this.keeperTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperNotes)).BeginInit();
            this.transactionTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transactionGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOnExchequer)).BeginInit();
            this.notificationTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11126NotificationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).BeginInit();
            this.coverTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.coverGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11075CoverItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coverGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit1)).BeginInit();
            this.serviceIntervalTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serviceIntervalGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceDataItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceIntervalGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            this.workDetailTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).BeginInit();
            this.incidentTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentNotesMemoEdit)).BeginInit();
            this.purposeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11084PurposeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).BeginInit();
            this.fuelCardTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11089FuelCardItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit19)).BeginInit();
            this.speedingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11105SpeedingItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceReported)).BeginInit();
            this.LinkedDocumentsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLinkedDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLinkedDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLinkedDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditLinkedDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11050DepreciationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11005VehicleItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11047EquipmentBillingItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11002EquipmentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11023VerilocationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLGeneralBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11102RentalItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1362, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 602);
            this.barDockControlBottom.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 560);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1362, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 560);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem2,
            this.barButtonItem4,
            this.barEditItem1,
            this.bbiRefresh,
            this.bbiExport,
            this.bbiExportExcelOld,
            this.bbiExportExcel,
            this.bbiEmail});
            this.barManager1.MaxItemId = 41;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 42);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.equipmentChildTabControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1362, 560);
            this.splitContainerControl1.SplitterPosition = 194;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.rentalGridControl;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.rentalGridControl);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1362, 360);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // rentalGridControl
            // 
            this.rentalGridControl.DataSource = this.spAS11102RentalManagerBindingSource;
            this.rentalGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rentalGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.rentalGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.rentalGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.rentalGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.rentalGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.rentalGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.rentalGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.rentalGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.rentalGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.rentalGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.rentalGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.rentalGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, false, true, "Show On Map", "showOnMap")});
            this.rentalGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.rentalGridControl.Location = new System.Drawing.Point(0, 0);
            this.rentalGridControl.MainView = this.rentalGridView;
            this.rentalGridControl.MenuManager = this.barManager1;
            this.rentalGridControl.Name = "rentalGridControl";
            this.rentalGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEditDate,
            this.ArchiveCheckEdit,
            this.moneyTextEdit2,
            this.unresolvedCheckEdit});
            this.rentalGridControl.Size = new System.Drawing.Size(1362, 360);
            this.rentalGridControl.TabIndex = 1;
            this.rentalGridControl.Tag = "Rental Manager";
            this.rentalGridControl.UseEmbeddedNavigator = true;
            this.rentalGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.rentalGridView});
            // 
            // spAS11102RentalManagerBindingSource
            // 
            this.spAS11102RentalManagerBindingSource.DataMember = "sp_AS_11102_Rental_Manager";
            this.spAS11102RentalManagerBindingSource.DataSource = this.dataSet_AS_Core;
            // 
            // dataSet_AS_Core
            // 
            this.dataSet_AS_Core.DataSetName = "DataSet_AS_Core";
            this.dataSet_AS_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("country_16x16.png", "images/miscellaneous/country_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/country_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "country_16x16.png");
            this.imageCollection1.InsertGalleryImage("insert_16x16.png", "images/actions/insert_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/insert_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "insert_16x16.png");
            // 
            // rentalGridView
            // 
            this.rentalGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentID1,
            this.colEquipmentReference1,
            this.colEquipmentCategoryID,
            this.colEquipmentCategory,
            this.colMake,
            this.colModel,
            this.colModelSpecifications,
            this.colOwnershipStatus,
            this.colSupplier,
            this.colUniqueReference,
            this.colCustomerReference,
            this.colListPrice,
            this.colRate,
            this.colRegistrationDate,
            this.colStartDate,
            this.colEndDate,
            this.colActualEndDate1,
            this.colRentalCategory,
            this.colRentalStatus,
            this.colRentalSpecification,
            this.colEngineSize,
            this.colFuelType,
            this.colEmissions,
            this.colUnresolvedWarning,
            this.colAvailability,
            this.colLastKeeper,
            this.colActiveKeeper,
            this.colNotes,
            this.colDateAdded,
            this.colMode1,
            this.colRecordID1});
            this.rentalGridView.GridControl = this.rentalGridControl;
            this.rentalGridView.Name = "rentalGridView";
            this.rentalGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.rentalGridView.OptionsFind.AlwaysVisible = true;
            this.rentalGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.rentalGridView.OptionsLayout.StoreAppearance = true;
            this.rentalGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.rentalGridView.OptionsSelection.MultiSelect = true;
            this.rentalGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.rentalGridView.OptionsView.ColumnAutoWidth = false;
            this.rentalGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.rentalGridView.OptionsView.ShowGroupPanel = false;
            this.rentalGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.rentalGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.rentalGridView_RowStyle);
            this.rentalGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.rentalGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.equipmentView_SelectionChanged);
            this.rentalGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.rentalGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.rentalGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.rentalGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentID1
            // 
            this.colEquipmentID1.FieldName = "EquipmentID";
            this.colEquipmentID1.Name = "colEquipmentID1";
            this.colEquipmentID1.OptionsColumn.AllowEdit = false;
            this.colEquipmentID1.OptionsColumn.AllowFocus = false;
            this.colEquipmentID1.OptionsColumn.ReadOnly = true;
            this.colEquipmentID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID1.Width = 85;
            // 
            // colEquipmentReference1
            // 
            this.colEquipmentReference1.FieldName = "EquipmentReference";
            this.colEquipmentReference1.Name = "colEquipmentReference1";
            this.colEquipmentReference1.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference1.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference1.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference1.Visible = true;
            this.colEquipmentReference1.VisibleIndex = 0;
            this.colEquipmentReference1.Width = 124;
            // 
            // colEquipmentCategoryID
            // 
            this.colEquipmentCategoryID.FieldName = "EquipmentCategoryID";
            this.colEquipmentCategoryID.Name = "colEquipmentCategoryID";
            this.colEquipmentCategoryID.OptionsColumn.AllowEdit = false;
            this.colEquipmentCategoryID.OptionsColumn.AllowFocus = false;
            this.colEquipmentCategoryID.OptionsColumn.ReadOnly = true;
            this.colEquipmentCategoryID.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentCategoryID.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentCategoryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentCategoryID.Width = 133;
            // 
            // colEquipmentCategory
            // 
            this.colEquipmentCategory.FieldName = "EquipmentCategory";
            this.colEquipmentCategory.Name = "colEquipmentCategory";
            this.colEquipmentCategory.OptionsColumn.AllowEdit = false;
            this.colEquipmentCategory.OptionsColumn.AllowFocus = false;
            this.colEquipmentCategory.OptionsColumn.ReadOnly = true;
            this.colEquipmentCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentCategory.Visible = true;
            this.colEquipmentCategory.VisibleIndex = 1;
            this.colEquipmentCategory.Width = 119;
            // 
            // colMake
            // 
            this.colMake.FieldName = "Make";
            this.colMake.Name = "colMake";
            this.colMake.OptionsColumn.AllowEdit = false;
            this.colMake.OptionsColumn.AllowFocus = false;
            this.colMake.OptionsColumn.ReadOnly = true;
            this.colMake.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMake.Visible = true;
            this.colMake.VisibleIndex = 2;
            this.colMake.Width = 173;
            // 
            // colModel
            // 
            this.colModel.FieldName = "Model";
            this.colModel.Name = "colModel";
            this.colModel.OptionsColumn.AllowEdit = false;
            this.colModel.OptionsColumn.AllowFocus = false;
            this.colModel.OptionsColumn.ReadOnly = true;
            this.colModel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colModel.Visible = true;
            this.colModel.VisibleIndex = 3;
            this.colModel.Width = 179;
            // 
            // colModelSpecifications
            // 
            this.colModelSpecifications.FieldName = "ModelSpecifications";
            this.colModelSpecifications.Name = "colModelSpecifications";
            this.colModelSpecifications.OptionsColumn.AllowEdit = false;
            this.colModelSpecifications.OptionsColumn.AllowFocus = false;
            this.colModelSpecifications.OptionsColumn.ReadOnly = true;
            this.colModelSpecifications.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colModelSpecifications.Visible = true;
            this.colModelSpecifications.VisibleIndex = 4;
            this.colModelSpecifications.Width = 117;
            // 
            // colOwnershipStatus
            // 
            this.colOwnershipStatus.Caption = "Rental Type";
            this.colOwnershipStatus.FieldName = "OwnershipStatus";
            this.colOwnershipStatus.Name = "colOwnershipStatus";
            this.colOwnershipStatus.OptionsColumn.AllowEdit = false;
            this.colOwnershipStatus.OptionsColumn.AllowFocus = false;
            this.colOwnershipStatus.OptionsColumn.ReadOnly = true;
            this.colOwnershipStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOwnershipStatus.Visible = true;
            this.colOwnershipStatus.VisibleIndex = 5;
            this.colOwnershipStatus.Width = 106;
            // 
            // colSupplier
            // 
            this.colSupplier.FieldName = "Supplier";
            this.colSupplier.Name = "colSupplier";
            this.colSupplier.OptionsColumn.AllowEdit = false;
            this.colSupplier.OptionsColumn.AllowFocus = false;
            this.colSupplier.OptionsColumn.ReadOnly = true;
            this.colSupplier.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplier.Visible = true;
            this.colSupplier.VisibleIndex = 6;
            this.colSupplier.Width = 154;
            // 
            // colUniqueReference
            // 
            this.colUniqueReference.FieldName = "UniqueReference";
            this.colUniqueReference.Name = "colUniqueReference";
            this.colUniqueReference.OptionsColumn.AllowEdit = false;
            this.colUniqueReference.OptionsColumn.AllowFocus = false;
            this.colUniqueReference.OptionsColumn.ReadOnly = true;
            this.colUniqueReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUniqueReference.Visible = true;
            this.colUniqueReference.VisibleIndex = 7;
            this.colUniqueReference.Width = 107;
            // 
            // colCustomerReference
            // 
            this.colCustomerReference.FieldName = "CustomerReference";
            this.colCustomerReference.Name = "colCustomerReference";
            this.colCustomerReference.OptionsColumn.AllowEdit = false;
            this.colCustomerReference.OptionsColumn.AllowFocus = false;
            this.colCustomerReference.OptionsColumn.ReadOnly = true;
            this.colCustomerReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCustomerReference.Visible = true;
            this.colCustomerReference.VisibleIndex = 8;
            this.colCustomerReference.Width = 120;
            // 
            // colListPrice
            // 
            this.colListPrice.Caption = "List Price";
            this.colListPrice.ColumnEdit = this.moneyTextEdit2;
            this.colListPrice.FieldName = "ListPrice";
            this.colListPrice.Name = "colListPrice";
            this.colListPrice.OptionsColumn.AllowEdit = false;
            this.colListPrice.OptionsColumn.AllowFocus = false;
            this.colListPrice.OptionsColumn.ReadOnly = true;
            this.colListPrice.Visible = true;
            this.colListPrice.VisibleIndex = 9;
            // 
            // moneyTextEdit2
            // 
            this.moneyTextEdit2.AutoHeight = false;
            this.moneyTextEdit2.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit2.Name = "moneyTextEdit2";
            // 
            // colRate
            // 
            this.colRate.Caption = "Daily Rate";
            this.colRate.ColumnEdit = this.moneyTextEdit2;
            this.colRate.FieldName = "Rate";
            this.colRate.Name = "colRate";
            this.colRate.OptionsColumn.AllowEdit = false;
            this.colRate.OptionsColumn.AllowFocus = false;
            this.colRate.OptionsColumn.ReadOnly = true;
            this.colRate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRate.Visible = true;
            this.colRate.VisibleIndex = 10;
            // 
            // colRegistrationDate
            // 
            this.colRegistrationDate.FieldName = "RegistrationDate";
            this.colRegistrationDate.Name = "colRegistrationDate";
            this.colRegistrationDate.OptionsColumn.AllowEdit = false;
            this.colRegistrationDate.OptionsColumn.AllowFocus = false;
            this.colRegistrationDate.OptionsColumn.ReadOnly = true;
            this.colRegistrationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistrationDate.Visible = true;
            this.colRegistrationDate.VisibleIndex = 11;
            // 
            // colStartDate
            // 
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 12;
            // 
            // colEndDate
            // 
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 13;
            // 
            // colActualEndDate1
            // 
            this.colActualEndDate1.FieldName = "ActualEndDate";
            this.colActualEndDate1.Name = "colActualEndDate1";
            this.colActualEndDate1.OptionsColumn.AllowEdit = false;
            this.colActualEndDate1.OptionsColumn.AllowFocus = false;
            this.colActualEndDate1.OptionsColumn.ReadOnly = true;
            this.colActualEndDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualEndDate1.Visible = true;
            this.colActualEndDate1.VisibleIndex = 14;
            this.colActualEndDate1.Width = 98;
            // 
            // colRentalCategory
            // 
            this.colRentalCategory.FieldName = "RentalCategory";
            this.colRentalCategory.Name = "colRentalCategory";
            this.colRentalCategory.OptionsColumn.AllowEdit = false;
            this.colRentalCategory.OptionsColumn.AllowFocus = false;
            this.colRentalCategory.OptionsColumn.ReadOnly = true;
            this.colRentalCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRentalCategory.Visible = true;
            this.colRentalCategory.VisibleIndex = 15;
            this.colRentalCategory.Width = 100;
            // 
            // colRentalStatus
            // 
            this.colRentalStatus.FieldName = "RentalStatus";
            this.colRentalStatus.Name = "colRentalStatus";
            this.colRentalStatus.OptionsColumn.AllowEdit = false;
            this.colRentalStatus.OptionsColumn.AllowFocus = false;
            this.colRentalStatus.OptionsColumn.ReadOnly = true;
            this.colRentalStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRentalStatus.Visible = true;
            this.colRentalStatus.VisibleIndex = 16;
            this.colRentalStatus.Width = 86;
            // 
            // colRentalSpecification
            // 
            this.colRentalSpecification.FieldName = "RentalSpecification";
            this.colRentalSpecification.Name = "colRentalSpecification";
            this.colRentalSpecification.OptionsColumn.AllowEdit = false;
            this.colRentalSpecification.OptionsColumn.AllowFocus = false;
            this.colRentalSpecification.OptionsColumn.ReadOnly = true;
            this.colRentalSpecification.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRentalSpecification.Visible = true;
            this.colRentalSpecification.VisibleIndex = 17;
            this.colRentalSpecification.Width = 115;
            // 
            // colEngineSize
            // 
            this.colEngineSize.FieldName = "EngineSize";
            this.colEngineSize.Name = "colEngineSize";
            this.colEngineSize.OptionsColumn.AllowEdit = false;
            this.colEngineSize.OptionsColumn.AllowFocus = false;
            this.colEngineSize.OptionsColumn.ReadOnly = true;
            this.colEngineSize.Visible = true;
            this.colEngineSize.VisibleIndex = 20;
            // 
            // colFuelType
            // 
            this.colFuelType.FieldName = "FuelType";
            this.colFuelType.Name = "colFuelType";
            this.colFuelType.OptionsColumn.AllowEdit = false;
            this.colFuelType.OptionsColumn.AllowFocus = false;
            this.colFuelType.OptionsColumn.ReadOnly = true;
            this.colFuelType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFuelType.Visible = true;
            this.colFuelType.VisibleIndex = 21;
            // 
            // colEmissions
            // 
            this.colEmissions.FieldName = "Emissions";
            this.colEmissions.Name = "colEmissions";
            this.colEmissions.OptionsColumn.AllowEdit = false;
            this.colEmissions.OptionsColumn.AllowFocus = false;
            this.colEmissions.OptionsColumn.ReadOnly = true;
            this.colEmissions.Visible = true;
            this.colEmissions.VisibleIndex = 22;
            // 
            // colUnresolvedWarning
            // 
            this.colUnresolvedWarning.ColumnEdit = this.unresolvedCheckEdit;
            this.colUnresolvedWarning.FieldName = "UnresolvedWarning";
            this.colUnresolvedWarning.Name = "colUnresolvedWarning";
            this.colUnresolvedWarning.OptionsColumn.AllowEdit = false;
            this.colUnresolvedWarning.OptionsColumn.AllowFocus = false;
            this.colUnresolvedWarning.OptionsColumn.ReadOnly = true;
            this.colUnresolvedWarning.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUnresolvedWarning.Visible = true;
            this.colUnresolvedWarning.VisibleIndex = 18;
            this.colUnresolvedWarning.Width = 118;
            // 
            // unresolvedCheckEdit
            // 
            this.unresolvedCheckEdit.AutoHeight = false;
            this.unresolvedCheckEdit.Caption = "Check";
            this.unresolvedCheckEdit.Name = "unresolvedCheckEdit";
            // 
            // colAvailability
            // 
            this.colAvailability.FieldName = "Availability";
            this.colAvailability.Name = "colAvailability";
            this.colAvailability.OptionsColumn.AllowEdit = false;
            this.colAvailability.OptionsColumn.AllowFocus = false;
            this.colAvailability.OptionsColumn.ReadOnly = true;
            this.colAvailability.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAvailability.Visible = true;
            this.colAvailability.VisibleIndex = 19;
            this.colAvailability.Width = 64;
            // 
            // colLastKeeper
            // 
            this.colLastKeeper.FieldName = "LastKeeper";
            this.colLastKeeper.Name = "colLastKeeper";
            this.colLastKeeper.OptionsColumn.AllowEdit = false;
            this.colLastKeeper.OptionsColumn.AllowFocus = false;
            this.colLastKeeper.OptionsColumn.ReadOnly = true;
            this.colLastKeeper.Visible = true;
            this.colLastKeeper.VisibleIndex = 23;
            this.colLastKeeper.Width = 104;
            // 
            // colActiveKeeper
            // 
            this.colActiveKeeper.FieldName = "ActiveKeeper";
            this.colActiveKeeper.Name = "colActiveKeeper";
            this.colActiveKeeper.OptionsColumn.AllowEdit = false;
            this.colActiveKeeper.OptionsColumn.AllowFocus = false;
            this.colActiveKeeper.OptionsColumn.ReadOnly = true;
            this.colActiveKeeper.Visible = true;
            this.colActiveKeeper.VisibleIndex = 24;
            this.colActiveKeeper.Width = 113;
            // 
            // colNotes
            // 
            this.colNotes.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colNotes.FieldName = "Notes";
            this.colNotes.Name = "colNotes";
            this.colNotes.OptionsColumn.AllowEdit = false;
            this.colNotes.OptionsColumn.AllowFocus = false;
            this.colNotes.OptionsColumn.ReadOnly = true;
            this.colNotes.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes.Visible = true;
            this.colNotes.VisibleIndex = 25;
            this.colNotes.Width = 474;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colDateAdded
            // 
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.OptionsColumn.ShowInCustomizationForm = false;
            this.colDateAdded.OptionsColumn.ShowInExpressionEditor = false;
            this.colDateAdded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateAdded.Width = 78;
            // 
            // colMode1
            // 
            this.colMode1.FieldName = "Mode";
            this.colMode1.Name = "colMode1";
            this.colMode1.OptionsColumn.AllowEdit = false;
            this.colMode1.OptionsColumn.AllowFocus = false;
            this.colMode1.OptionsColumn.ReadOnly = true;
            this.colMode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode1.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID1
            // 
            this.colRecordID1.FieldName = "RecordID";
            this.colRecordID1.Name = "colRecordID1";
            this.colRecordID1.OptionsColumn.AllowEdit = false;
            this.colRecordID1.OptionsColumn.AllowFocus = false;
            this.colRecordID1.OptionsColumn.ReadOnly = true;
            this.colRecordID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "d";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // ArchiveCheckEdit
            // 
            this.ArchiveCheckEdit.AutoHeight = false;
            this.ArchiveCheckEdit.Caption = "Check";
            this.ArchiveCheckEdit.Name = "ArchiveCheckEdit";
            // 
            // equipmentChildTabControl
            // 
            this.equipmentChildTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.equipmentChildTabControl.Location = new System.Drawing.Point(0, 0);
            this.equipmentChildTabControl.Name = "equipmentChildTabControl";
            this.equipmentChildTabControl.SelectedTabPage = this.rentalDetailsTabPage;
            this.equipmentChildTabControl.Size = new System.Drawing.Size(1362, 194);
            this.equipmentChildTabControl.TabIndex = 0;
            this.equipmentChildTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.NoDetailsTabPage,
            this.rentalDetailsTabPage,
            this.roadTaxTabPage,
            this.trackerTabPage,
            this.keeperTabPage,
            this.transactionTabPage,
            this.notificationTabPage,
            this.coverTabPage,
            this.serviceIntervalTabPage,
            this.workDetailTabPage,
            this.incidentTabPage,
            this.purposeTabPage,
            this.fuelCardTabPage,
            this.speedingTabPage,
            this.LinkedDocumentsTabPage});
            // 
            // rentalDetailsTabPage
            // 
            this.rentalDetailsTabPage.Controls.Add(this.gridSplitContainer2);
            this.rentalDetailsTabPage.Name = "rentalDetailsTabPage";
            this.rentalDetailsTabPage.PageVisible = false;
            this.rentalDetailsTabPage.Size = new System.Drawing.Size(1357, 168);
            this.rentalDetailsTabPage.Text = "Rental Details";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.rentalDetailsGridControl;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.rentalDetailsGridControl);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1357, 168);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // rentalDetailsGridControl
            // 
            this.rentalDetailsGridControl.DataSource = this.spAS11108RentalDetailsItemBindingSource;
            this.rentalDetailsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.rentalDetailsGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.rentalDetailsGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.rentalDetailsGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.rentalDetailsGridControl.Location = new System.Drawing.Point(0, 0);
            this.rentalDetailsGridControl.MainView = this.rentalDetailsGridView;
            this.rentalDetailsGridControl.MenuManager = this.barManager1;
            this.rentalDetailsGridControl.Name = "rentalDetailsGridControl";
            this.rentalDetailsGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.memNotes,
            this.ceSetAsMain,
            this.moneyEdit});
            this.rentalDetailsGridControl.Size = new System.Drawing.Size(1357, 168);
            this.rentalDetailsGridControl.TabIndex = 0;
            this.rentalDetailsGridControl.Tag = "Rental Details";
            this.rentalDetailsGridControl.UseEmbeddedNavigator = true;
            this.rentalDetailsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.rentalDetailsGridView});
            // 
            // spAS11108RentalDetailsItemBindingSource
            // 
            this.spAS11108RentalDetailsItemBindingSource.DataMember = "sp_AS_11108_Rental_Details_Item";
            this.spAS11108RentalDetailsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rentalDetailsGridView
            // 
            this.rentalDetailsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRentalDetailsID,
            this.colEquipmentID,
            this.colEquipmentReference,
            this.colRentalCategoryID,
            this.colRentalCategory1,
            this.colRentalStatusID,
            this.colRentalStatus1,
            this.colRentalSpecificationID,
            this.colRentalSpecification1,
            this.colRate1,
            this.colCustomerReference1,
            this.colStartDate1,
            this.colActualEndDate,
            this.colEndDate1,
            this.colSetAsMain,
            this.colNotes6,
            this.colMode,
            this.colRecordID});
            this.rentalDetailsGridView.GridControl = this.rentalDetailsGridControl;
            this.rentalDetailsGridView.Name = "rentalDetailsGridView";
            this.rentalDetailsGridView.OptionsCustomization.AllowFilter = false;
            this.rentalDetailsGridView.OptionsCustomization.AllowGroup = false;
            this.rentalDetailsGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.rentalDetailsGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.rentalDetailsGridView.OptionsLayout.StoreAppearance = true;
            this.rentalDetailsGridView.OptionsSelection.MultiSelect = true;
            this.rentalDetailsGridView.OptionsView.ColumnAutoWidth = false;
            this.rentalDetailsGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.rentalDetailsGridView.OptionsView.ShowGroupPanel = false;
            this.rentalDetailsGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.rentalDetailsGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.rentalDetailsGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.rentalDetailsGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.rentalDetailsGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.rentalDetailsGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.rentalDetailsGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colRentalDetailsID
            // 
            this.colRentalDetailsID.FieldName = "RentalDetailsID";
            this.colRentalDetailsID.Name = "colRentalDetailsID";
            this.colRentalDetailsID.OptionsColumn.AllowEdit = false;
            this.colRentalDetailsID.OptionsColumn.AllowFocus = false;
            this.colRentalDetailsID.OptionsColumn.ReadOnly = true;
            this.colRentalDetailsID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRentalDetailsID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRentalDetailsID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID
            // 
            this.colEquipmentID.FieldName = "EquipmentID";
            this.colEquipmentID.Name = "colEquipmentID";
            this.colEquipmentID.OptionsColumn.AllowEdit = false;
            this.colEquipmentID.OptionsColumn.AllowFocus = false;
            this.colEquipmentID.OptionsColumn.ReadOnly = true;
            this.colEquipmentID.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentReference
            // 
            this.colEquipmentReference.FieldName = "EquipmentReference";
            this.colEquipmentReference.Name = "colEquipmentReference";
            this.colEquipmentReference.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference.Visible = true;
            this.colEquipmentReference.VisibleIndex = 0;
            this.colEquipmentReference.Width = 115;
            // 
            // colRentalCategoryID
            // 
            this.colRentalCategoryID.FieldName = "RentalCategoryID";
            this.colRentalCategoryID.Name = "colRentalCategoryID";
            this.colRentalCategoryID.OptionsColumn.AllowEdit = false;
            this.colRentalCategoryID.OptionsColumn.AllowFocus = false;
            this.colRentalCategoryID.OptionsColumn.ReadOnly = true;
            this.colRentalCategoryID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRentalCategoryID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRentalCategoryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRentalCategoryID.Width = 105;
            // 
            // colRentalCategory1
            // 
            this.colRentalCategory1.FieldName = "RentalCategory";
            this.colRentalCategory1.Name = "colRentalCategory1";
            this.colRentalCategory1.OptionsColumn.AllowEdit = false;
            this.colRentalCategory1.OptionsColumn.AllowFocus = false;
            this.colRentalCategory1.OptionsColumn.ReadOnly = true;
            this.colRentalCategory1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRentalCategory1.Visible = true;
            this.colRentalCategory1.VisibleIndex = 1;
            this.colRentalCategory1.Width = 120;
            // 
            // colRentalStatusID
            // 
            this.colRentalStatusID.FieldName = "RentalStatusID";
            this.colRentalStatusID.Name = "colRentalStatusID";
            this.colRentalStatusID.OptionsColumn.AllowEdit = false;
            this.colRentalStatusID.OptionsColumn.AllowFocus = false;
            this.colRentalStatusID.OptionsColumn.ReadOnly = true;
            this.colRentalStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRentalStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRentalStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRentalStatusID.Width = 91;
            // 
            // colRentalStatus1
            // 
            this.colRentalStatus1.FieldName = "RentalStatus";
            this.colRentalStatus1.Name = "colRentalStatus1";
            this.colRentalStatus1.OptionsColumn.AllowEdit = false;
            this.colRentalStatus1.OptionsColumn.AllowFocus = false;
            this.colRentalStatus1.OptionsColumn.ReadOnly = true;
            this.colRentalStatus1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRentalStatus1.Visible = true;
            this.colRentalStatus1.VisibleIndex = 2;
            this.colRentalStatus1.Width = 120;
            // 
            // colRentalSpecificationID
            // 
            this.colRentalSpecificationID.FieldName = "RentalSpecificationID";
            this.colRentalSpecificationID.Name = "colRentalSpecificationID";
            this.colRentalSpecificationID.OptionsColumn.AllowEdit = false;
            this.colRentalSpecificationID.OptionsColumn.AllowFocus = false;
            this.colRentalSpecificationID.OptionsColumn.ReadOnly = true;
            this.colRentalSpecificationID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRentalSpecificationID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRentalSpecificationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRentalSpecificationID.Width = 120;
            // 
            // colRentalSpecification1
            // 
            this.colRentalSpecification1.FieldName = "RentalSpecification";
            this.colRentalSpecification1.Name = "colRentalSpecification1";
            this.colRentalSpecification1.OptionsColumn.AllowEdit = false;
            this.colRentalSpecification1.OptionsColumn.AllowFocus = false;
            this.colRentalSpecification1.OptionsColumn.ReadOnly = true;
            this.colRentalSpecification1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRentalSpecification1.Visible = true;
            this.colRentalSpecification1.VisibleIndex = 3;
            this.colRentalSpecification1.Width = 130;
            // 
            // colRate1
            // 
            this.colRate1.ColumnEdit = this.moneyEdit;
            this.colRate1.FieldName = "Rate";
            this.colRate1.Name = "colRate1";
            this.colRate1.OptionsColumn.AllowEdit = false;
            this.colRate1.OptionsColumn.AllowFocus = false;
            this.colRate1.OptionsColumn.ReadOnly = true;
            this.colRate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRate1.Visible = true;
            this.colRate1.VisibleIndex = 4;
            // 
            // moneyEdit
            // 
            this.moneyEdit.AutoHeight = false;
            this.moneyEdit.DisplayFormat.FormatString = "c2";
            this.moneyEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyEdit.Name = "moneyEdit";
            // 
            // colCustomerReference1
            // 
            this.colCustomerReference1.FieldName = "CustomerReference";
            this.colCustomerReference1.Name = "colCustomerReference1";
            this.colCustomerReference1.OptionsColumn.AllowEdit = false;
            this.colCustomerReference1.OptionsColumn.AllowFocus = false;
            this.colCustomerReference1.OptionsColumn.ReadOnly = true;
            this.colCustomerReference1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCustomerReference1.Visible = true;
            this.colCustomerReference1.VisibleIndex = 5;
            this.colCustomerReference1.Width = 111;
            // 
            // colStartDate1
            // 
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 6;
            // 
            // colActualEndDate
            // 
            this.colActualEndDate.FieldName = "ActualEndDate";
            this.colActualEndDate.Name = "colActualEndDate";
            this.colActualEndDate.OptionsColumn.AllowEdit = false;
            this.colActualEndDate.OptionsColumn.AllowFocus = false;
            this.colActualEndDate.OptionsColumn.ReadOnly = true;
            this.colActualEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualEndDate.Visible = true;
            this.colActualEndDate.VisibleIndex = 8;
            this.colActualEndDate.Width = 89;
            // 
            // colEndDate1
            // 
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 7;
            // 
            // colSetAsMain
            // 
            this.colSetAsMain.ColumnEdit = this.ceSetAsMain;
            this.colSetAsMain.FieldName = "SetAsMain";
            this.colSetAsMain.Name = "colSetAsMain";
            this.colSetAsMain.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSetAsMain.Visible = true;
            this.colSetAsMain.VisibleIndex = 9;
            // 
            // ceSetAsMain
            // 
            this.ceSetAsMain.AutoHeight = false;
            this.ceSetAsMain.Caption = "Check";
            this.ceSetAsMain.Name = "ceSetAsMain";
            this.ceSetAsMain.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceSetAsMain_EditValueChanging);
            // 
            // colNotes6
            // 
            this.colNotes6.ColumnEdit = this.memNotes;
            this.colNotes6.FieldName = "Notes";
            this.colNotes6.Name = "colNotes6";
            this.colNotes6.OptionsColumn.AllowEdit = false;
            this.colNotes6.OptionsColumn.AllowFocus = false;
            this.colNotes6.OptionsColumn.ReadOnly = true;
            this.colNotes6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes6.Visible = true;
            this.colNotes6.VisibleIndex = 10;
            this.colNotes6.Width = 437;
            // 
            // memNotes
            // 
            this.memNotes.AutoHeight = false;
            this.memNotes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memNotes.Name = "memNotes";
            this.memNotes.ReadOnly = true;
            this.memNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.memNotes.ShowIcon = false;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // NoDetailsTabPage
            // 
            this.NoDetailsTabPage.AutoScroll = true;
            this.NoDetailsTabPage.Controls.Add(this.label1);
            this.NoDetailsTabPage.Name = "NoDetailsTabPage";
            this.NoDetailsTabPage.Size = new System.Drawing.Size(1357, 168);
            this.NoDetailsTabPage.Text = "No Details Available";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(556, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Equipment Detailed View Not Available";
            // 
            // roadTaxTabPage
            // 
            this.roadTaxTabPage.Controls.Add(this.roadTaxGridControl);
            this.roadTaxTabPage.Name = "roadTaxTabPage";
            this.roadTaxTabPage.Size = new System.Drawing.Size(1357, 168);
            this.roadTaxTabPage.Text = "Road Tax";
            // 
            // roadTaxGridControl
            // 
            this.roadTaxGridControl.DataSource = this.spAS11096RoadTaxItemBindingSource;
            this.roadTaxGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.roadTaxGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.roadTaxGridControl.Location = new System.Drawing.Point(0, 0);
            this.roadTaxGridControl.MainView = this.roadTaxGridView;
            this.roadTaxGridControl.MenuManager = this.barManager1;
            this.roadTaxGridControl.Name = "roadTaxGridControl";
            this.roadTaxGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.moneyTextEdit4});
            this.roadTaxGridControl.Size = new System.Drawing.Size(1357, 168);
            this.roadTaxGridControl.TabIndex = 1;
            this.roadTaxGridControl.Tag = "Road Tax";
            this.roadTaxGridControl.UseEmbeddedNavigator = true;
            this.roadTaxGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.roadTaxGridView});
            // 
            // spAS11096RoadTaxItemBindingSource
            // 
            this.spAS11096RoadTaxItemBindingSource.DataMember = "sp_AS_11096_Road_Tax_Item";
            this.spAS11096RoadTaxItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // roadTaxGridView
            // 
            this.roadTaxGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRoadTaxID,
            this.colEquipmentReference12,
            this.colEquipmentID14,
            this.colTaxExpiryDate,
            this.colAmount,
            this.colTaxPeriodID,
            this.colTaxPeriod,
            this.colMode13,
            this.colRecordID13});
            this.roadTaxGridView.GridControl = this.roadTaxGridControl;
            this.roadTaxGridView.Name = "roadTaxGridView";
            this.roadTaxGridView.OptionsCustomization.AllowFilter = false;
            this.roadTaxGridView.OptionsCustomization.AllowGroup = false;
            this.roadTaxGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.roadTaxGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.roadTaxGridView.OptionsLayout.StoreAppearance = true;
            this.roadTaxGridView.OptionsSelection.MultiSelect = true;
            this.roadTaxGridView.OptionsView.ColumnAutoWidth = false;
            this.roadTaxGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.roadTaxGridView.OptionsView.ShowGroupPanel = false;
            this.roadTaxGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.roadTaxGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.roadTaxGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.roadTaxGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.roadTaxGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.roadTaxGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.roadTaxGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colRoadTaxID
            // 
            this.colRoadTaxID.FieldName = "RoadTaxID";
            this.colRoadTaxID.Name = "colRoadTaxID";
            this.colRoadTaxID.OptionsColumn.AllowEdit = false;
            this.colRoadTaxID.OptionsColumn.AllowFocus = false;
            this.colRoadTaxID.OptionsColumn.ReadOnly = true;
            this.colRoadTaxID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRoadTaxID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRoadTaxID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadTaxID.Width = 157;
            // 
            // colEquipmentReference12
            // 
            this.colEquipmentReference12.FieldName = "EquipmentReference";
            this.colEquipmentReference12.Name = "colEquipmentReference12";
            this.colEquipmentReference12.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference12.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference12.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference12.Visible = true;
            this.colEquipmentReference12.VisibleIndex = 0;
            this.colEquipmentReference12.Width = 187;
            // 
            // colEquipmentID14
            // 
            this.colEquipmentID14.FieldName = "EquipmentID";
            this.colEquipmentID14.Name = "colEquipmentID14";
            this.colEquipmentID14.OptionsColumn.AllowEdit = false;
            this.colEquipmentID14.OptionsColumn.AllowFocus = false;
            this.colEquipmentID14.OptionsColumn.ReadOnly = true;
            this.colEquipmentID14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID14.Width = 154;
            // 
            // colTaxExpiryDate
            // 
            this.colTaxExpiryDate.FieldName = "TaxExpiryDate";
            this.colTaxExpiryDate.Name = "colTaxExpiryDate";
            this.colTaxExpiryDate.OptionsColumn.AllowEdit = false;
            this.colTaxExpiryDate.OptionsColumn.AllowFocus = false;
            this.colTaxExpiryDate.OptionsColumn.ReadOnly = true;
            this.colTaxExpiryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaxExpiryDate.Visible = true;
            this.colTaxExpiryDate.VisibleIndex = 1;
            this.colTaxExpiryDate.Width = 203;
            // 
            // colAmount
            // 
            this.colAmount.ColumnEdit = this.moneyTextEdit4;
            this.colAmount.FieldName = "Amount";
            this.colAmount.Name = "colAmount";
            this.colAmount.OptionsColumn.AllowEdit = false;
            this.colAmount.OptionsColumn.AllowFocus = false;
            this.colAmount.OptionsColumn.ReadOnly = true;
            this.colAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAmount.Visible = true;
            this.colAmount.VisibleIndex = 2;
            this.colAmount.Width = 222;
            // 
            // moneyTextEdit4
            // 
            this.moneyTextEdit4.AutoHeight = false;
            this.moneyTextEdit4.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit4.Name = "moneyTextEdit4";
            // 
            // colTaxPeriodID
            // 
            this.colTaxPeriodID.FieldName = "TaxPeriodID";
            this.colTaxPeriodID.Name = "colTaxPeriodID";
            this.colTaxPeriodID.OptionsColumn.AllowEdit = false;
            this.colTaxPeriodID.OptionsColumn.AllowFocus = false;
            this.colTaxPeriodID.OptionsColumn.ReadOnly = true;
            this.colTaxPeriodID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTaxPeriodID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTaxPeriodID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaxPeriodID.Width = 77;
            // 
            // colTaxPeriod
            // 
            this.colTaxPeriod.FieldName = "TaxPeriod";
            this.colTaxPeriod.Name = "colTaxPeriod";
            this.colTaxPeriod.OptionsColumn.AllowEdit = false;
            this.colTaxPeriod.OptionsColumn.AllowFocus = false;
            this.colTaxPeriod.OptionsColumn.ReadOnly = true;
            this.colTaxPeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaxPeriod.Visible = true;
            this.colTaxPeriod.VisibleIndex = 3;
            this.colTaxPeriod.Width = 367;
            // 
            // colMode13
            // 
            this.colMode13.FieldName = "Mode";
            this.colMode13.Name = "colMode13";
            this.colMode13.OptionsColumn.AllowEdit = false;
            this.colMode13.OptionsColumn.AllowFocus = false;
            this.colMode13.OptionsColumn.ReadOnly = true;
            this.colMode13.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode13.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID13
            // 
            this.colRecordID13.FieldName = "RecordID";
            this.colRecordID13.Name = "colRecordID13";
            this.colRecordID13.OptionsColumn.AllowEdit = false;
            this.colRecordID13.OptionsColumn.AllowFocus = false;
            this.colRecordID13.OptionsColumn.ReadOnly = true;
            this.colRecordID13.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID13.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // trackerTabPage
            // 
            this.trackerTabPage.Controls.Add(this.trackerGridControl);
            this.trackerTabPage.Name = "trackerTabPage";
            this.trackerTabPage.Size = new System.Drawing.Size(1357, 168);
            this.trackerTabPage.Text = "Verilocation";
            // 
            // trackerGridControl
            // 
            this.trackerGridControl.DataSource = this.spAS11023TrackerListBindingSource;
            this.trackerGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.trackerGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.trackerGridControl.Location = new System.Drawing.Point(0, 0);
            this.trackerGridControl.MainView = this.trackerGridView;
            this.trackerGridControl.MenuManager = this.barManager1;
            this.trackerGridControl.Name = "trackerGridControl";
            this.trackerGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemMemoExEdit10});
            this.trackerGridControl.Size = new System.Drawing.Size(1357, 168);
            this.trackerGridControl.TabIndex = 1;
            this.trackerGridControl.Tag = "Verilocation Data";
            this.trackerGridControl.UseEmbeddedNavigator = true;
            this.trackerGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.trackerGridView});
            // 
            // spAS11023TrackerListBindingSource
            // 
            this.spAS11023TrackerListBindingSource.DataMember = "sp_AS_11023_Tracker_List";
            this.spAS11023TrackerListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // trackerGridView
            // 
            this.trackerGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTrackerInformationID,
            this.colEquipmentReference6,
            this.colRegistration,
            this.colTrackerVID,
            this.colTrackerReference,
            this.colEquipmentID8,
            this.colMake1,
            this.colModel1,
            this.colOdometerMetres,
            this.colOdometerMiles,
            this.colReason,
            this.colMPG,
            this.colLatitude,
            this.colLatestStartJourney,
            this.colLatestEndJourney,
            this.colLongitude,
            this.colSpeedKPH,
            this.colSpeedMPH,
            this.colAverageSpeed,
            this.colPlaceName,
            this.colRoadName,
            this.colPostSect,
            this.colGeofenceName,
            this.colDistrict,
            this.colLastUpdate,
            this.colDriverName,
            this.colDriverMobile,
            this.colDriverEmail,
            this.colMode4,
            this.colRecordID4});
            this.trackerGridView.GridControl = this.trackerGridControl;
            this.trackerGridView.Name = "trackerGridView";
            this.trackerGridView.OptionsCustomization.AllowFilter = false;
            this.trackerGridView.OptionsCustomization.AllowGroup = false;
            this.trackerGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.trackerGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.trackerGridView.OptionsLayout.StoreAppearance = true;
            this.trackerGridView.OptionsSelection.MultiSelect = true;
            this.trackerGridView.OptionsView.ColumnAutoWidth = false;
            this.trackerGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.trackerGridView.OptionsView.ShowGroupPanel = false;
            this.trackerGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.trackerGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.trackerGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.trackerGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.trackerGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.trackerGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.trackerGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colTrackerInformationID
            // 
            this.colTrackerInformationID.FieldName = "TrackerInformationID";
            this.colTrackerInformationID.Name = "colTrackerInformationID";
            this.colTrackerInformationID.OptionsColumn.AllowEdit = false;
            this.colTrackerInformationID.OptionsColumn.AllowFocus = false;
            this.colTrackerInformationID.OptionsColumn.ReadOnly = true;
            this.colTrackerInformationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTrackerInformationID.Width = 121;
            // 
            // colEquipmentReference6
            // 
            this.colEquipmentReference6.FieldName = "EquipmentReference";
            this.colEquipmentReference6.Name = "colEquipmentReference6";
            this.colEquipmentReference6.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference6.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference6.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference6.Visible = true;
            this.colEquipmentReference6.VisibleIndex = 0;
            this.colEquipmentReference6.Width = 115;
            // 
            // colRegistration
            // 
            this.colRegistration.FieldName = "Registration";
            this.colRegistration.Name = "colRegistration";
            this.colRegistration.OptionsColumn.AllowEdit = false;
            this.colRegistration.OptionsColumn.AllowFocus = false;
            this.colRegistration.OptionsColumn.ReadOnly = true;
            this.colRegistration.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistration.Visible = true;
            this.colRegistration.VisibleIndex = 1;
            // 
            // colTrackerVID
            // 
            this.colTrackerVID.FieldName = "TrackerVID";
            this.colTrackerVID.Name = "colTrackerVID";
            this.colTrackerVID.OptionsColumn.AllowEdit = false;
            this.colTrackerVID.OptionsColumn.AllowFocus = false;
            this.colTrackerVID.OptionsColumn.ReadOnly = true;
            this.colTrackerVID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTrackerVID.Visible = true;
            this.colTrackerVID.VisibleIndex = 2;
            // 
            // colTrackerReference
            // 
            this.colTrackerReference.FieldName = "TrackerReference";
            this.colTrackerReference.Name = "colTrackerReference";
            this.colTrackerReference.OptionsColumn.AllowEdit = false;
            this.colTrackerReference.OptionsColumn.AllowFocus = false;
            this.colTrackerReference.OptionsColumn.ReadOnly = true;
            this.colTrackerReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTrackerReference.Visible = true;
            this.colTrackerReference.VisibleIndex = 3;
            this.colTrackerReference.Width = 162;
            // 
            // colEquipmentID8
            // 
            this.colEquipmentID8.FieldName = "EquipmentID";
            this.colEquipmentID8.Name = "colEquipmentID8";
            this.colEquipmentID8.OptionsColumn.AllowEdit = false;
            this.colEquipmentID8.OptionsColumn.AllowFocus = false;
            this.colEquipmentID8.OptionsColumn.ReadOnly = true;
            this.colEquipmentID8.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID8.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID8.Width = 76;
            // 
            // colMake1
            // 
            this.colMake1.FieldName = "Make";
            this.colMake1.Name = "colMake1";
            this.colMake1.OptionsColumn.AllowEdit = false;
            this.colMake1.OptionsColumn.AllowFocus = false;
            this.colMake1.OptionsColumn.ReadOnly = true;
            this.colMake1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMake1.Visible = true;
            this.colMake1.VisibleIndex = 5;
            // 
            // colModel1
            // 
            this.colModel1.FieldName = "Model";
            this.colModel1.Name = "colModel1";
            this.colModel1.OptionsColumn.AllowEdit = false;
            this.colModel1.OptionsColumn.AllowFocus = false;
            this.colModel1.OptionsColumn.ReadOnly = true;
            this.colModel1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colModel1.Visible = true;
            this.colModel1.VisibleIndex = 6;
            // 
            // colOdometerMetres
            // 
            this.colOdometerMetres.FieldName = "OdometerMetres";
            this.colOdometerMetres.Name = "colOdometerMetres";
            this.colOdometerMetres.OptionsColumn.AllowEdit = false;
            this.colOdometerMetres.OptionsColumn.AllowFocus = false;
            this.colOdometerMetres.OptionsColumn.ReadOnly = true;
            this.colOdometerMetres.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOdometerMetres.Visible = true;
            this.colOdometerMetres.VisibleIndex = 7;
            this.colOdometerMetres.Width = 96;
            // 
            // colOdometerMiles
            // 
            this.colOdometerMiles.FieldName = "OdometerMiles";
            this.colOdometerMiles.Name = "colOdometerMiles";
            this.colOdometerMiles.OptionsColumn.AllowEdit = false;
            this.colOdometerMiles.OptionsColumn.AllowFocus = false;
            this.colOdometerMiles.OptionsColumn.ReadOnly = true;
            this.colOdometerMiles.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOdometerMiles.Visible = true;
            this.colOdometerMiles.VisibleIndex = 8;
            this.colOdometerMiles.Width = 86;
            // 
            // colReason
            // 
            this.colReason.FieldName = "Reason";
            this.colReason.Name = "colReason";
            this.colReason.OptionsColumn.AllowEdit = false;
            this.colReason.OptionsColumn.AllowFocus = false;
            this.colReason.OptionsColumn.ReadOnly = true;
            this.colReason.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReason.Visible = true;
            this.colReason.VisibleIndex = 9;
            // 
            // colMPG
            // 
            this.colMPG.FieldName = "MPG";
            this.colMPG.Name = "colMPG";
            this.colMPG.OptionsColumn.AllowEdit = false;
            this.colMPG.OptionsColumn.AllowFocus = false;
            this.colMPG.OptionsColumn.ReadOnly = true;
            this.colMPG.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMPG.Visible = true;
            this.colMPG.VisibleIndex = 10;
            // 
            // colLatitude
            // 
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            this.colLatitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatitude.Visible = true;
            this.colLatitude.VisibleIndex = 11;
            // 
            // colLatestStartJourney
            // 
            this.colLatestStartJourney.FieldName = "LatestStartJourney";
            this.colLatestStartJourney.Name = "colLatestStartJourney";
            this.colLatestStartJourney.OptionsColumn.AllowEdit = false;
            this.colLatestStartJourney.OptionsColumn.AllowFocus = false;
            this.colLatestStartJourney.OptionsColumn.ShowInCustomizationForm = false;
            this.colLatestStartJourney.OptionsColumn.ShowInExpressionEditor = false;
            this.colLatestStartJourney.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatestStartJourney.Width = 166;
            // 
            // colLatestEndJourney
            // 
            this.colLatestEndJourney.FieldName = "LatestEndJourney";
            this.colLatestEndJourney.Name = "colLatestEndJourney";
            this.colLatestEndJourney.OptionsColumn.AllowEdit = false;
            this.colLatestEndJourney.OptionsColumn.AllowFocus = false;
            this.colLatestEndJourney.OptionsColumn.ShowInCustomizationForm = false;
            this.colLatestEndJourney.OptionsColumn.ShowInExpressionEditor = false;
            this.colLatestEndJourney.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatestEndJourney.Width = 160;
            // 
            // colLongitude
            // 
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            this.colLongitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLongitude.Visible = true;
            this.colLongitude.VisibleIndex = 12;
            // 
            // colSpeedKPH
            // 
            this.colSpeedKPH.FieldName = "SpeedKPH";
            this.colSpeedKPH.Name = "colSpeedKPH";
            this.colSpeedKPH.OptionsColumn.AllowEdit = false;
            this.colSpeedKPH.OptionsColumn.AllowFocus = false;
            this.colSpeedKPH.OptionsColumn.ReadOnly = true;
            this.colSpeedKPH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeedKPH.Visible = true;
            this.colSpeedKPH.VisibleIndex = 13;
            // 
            // colSpeedMPH
            // 
            this.colSpeedMPH.FieldName = "SpeedMPH";
            this.colSpeedMPH.Name = "colSpeedMPH";
            this.colSpeedMPH.OptionsColumn.AllowEdit = false;
            this.colSpeedMPH.OptionsColumn.AllowFocus = false;
            this.colSpeedMPH.OptionsColumn.ReadOnly = true;
            this.colSpeedMPH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeedMPH.Visible = true;
            this.colSpeedMPH.VisibleIndex = 14;
            // 
            // colAverageSpeed
            // 
            this.colAverageSpeed.FieldName = "AverageSpeed";
            this.colAverageSpeed.Name = "colAverageSpeed";
            this.colAverageSpeed.OptionsColumn.AllowEdit = false;
            this.colAverageSpeed.OptionsColumn.AllowFocus = false;
            this.colAverageSpeed.OptionsColumn.ReadOnly = true;
            this.colAverageSpeed.OptionsColumn.ShowInCustomizationForm = false;
            this.colAverageSpeed.OptionsColumn.ShowInExpressionEditor = false;
            this.colAverageSpeed.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAverageSpeed.Width = 86;
            // 
            // colPlaceName
            // 
            this.colPlaceName.FieldName = "PlaceName";
            this.colPlaceName.Name = "colPlaceName";
            this.colPlaceName.OptionsColumn.AllowEdit = false;
            this.colPlaceName.OptionsColumn.AllowFocus = false;
            this.colPlaceName.OptionsColumn.ReadOnly = true;
            this.colPlaceName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlaceName.Visible = true;
            this.colPlaceName.VisibleIndex = 15;
            // 
            // colRoadName
            // 
            this.colRoadName.FieldName = "RoadName";
            this.colRoadName.Name = "colRoadName";
            this.colRoadName.OptionsColumn.AllowEdit = false;
            this.colRoadName.OptionsColumn.AllowFocus = false;
            this.colRoadName.OptionsColumn.ReadOnly = true;
            this.colRoadName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadName.Visible = true;
            this.colRoadName.VisibleIndex = 16;
            // 
            // colPostSect
            // 
            this.colPostSect.FieldName = "PostSect";
            this.colPostSect.Name = "colPostSect";
            this.colPostSect.OptionsColumn.AllowEdit = false;
            this.colPostSect.OptionsColumn.AllowFocus = false;
            this.colPostSect.OptionsColumn.ReadOnly = true;
            this.colPostSect.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPostSect.Visible = true;
            this.colPostSect.VisibleIndex = 17;
            // 
            // colGeofenceName
            // 
            this.colGeofenceName.FieldName = "GeofenceName";
            this.colGeofenceName.Name = "colGeofenceName";
            this.colGeofenceName.OptionsColumn.AllowEdit = false;
            this.colGeofenceName.OptionsColumn.AllowFocus = false;
            this.colGeofenceName.OptionsColumn.ReadOnly = true;
            this.colGeofenceName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGeofenceName.Visible = true;
            this.colGeofenceName.VisibleIndex = 18;
            this.colGeofenceName.Width = 88;
            // 
            // colDistrict
            // 
            this.colDistrict.FieldName = "District";
            this.colDistrict.Name = "colDistrict";
            this.colDistrict.OptionsColumn.AllowEdit = false;
            this.colDistrict.OptionsColumn.AllowFocus = false;
            this.colDistrict.OptionsColumn.ReadOnly = true;
            this.colDistrict.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistrict.Visible = true;
            this.colDistrict.VisibleIndex = 19;
            // 
            // colLastUpdate
            // 
            this.colLastUpdate.FieldName = "LastUpdate";
            this.colLastUpdate.Name = "colLastUpdate";
            this.colLastUpdate.OptionsColumn.AllowEdit = false;
            this.colLastUpdate.OptionsColumn.AllowFocus = false;
            this.colLastUpdate.OptionsColumn.ReadOnly = true;
            this.colLastUpdate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastUpdate.Visible = true;
            this.colLastUpdate.VisibleIndex = 20;
            // 
            // colDriverName
            // 
            this.colDriverName.FieldName = "DriverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.OptionsColumn.AllowEdit = false;
            this.colDriverName.OptionsColumn.AllowFocus = false;
            this.colDriverName.OptionsColumn.ReadOnly = true;
            this.colDriverName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDriverName.Visible = true;
            this.colDriverName.VisibleIndex = 4;
            // 
            // colDriverMobile
            // 
            this.colDriverMobile.FieldName = "DriverMobile";
            this.colDriverMobile.Name = "colDriverMobile";
            this.colDriverMobile.OptionsColumn.AllowEdit = false;
            this.colDriverMobile.OptionsColumn.AllowFocus = false;
            this.colDriverMobile.OptionsColumn.ReadOnly = true;
            this.colDriverMobile.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDriverMobile.Visible = true;
            this.colDriverMobile.VisibleIndex = 21;
            // 
            // colDriverEmail
            // 
            this.colDriverEmail.FieldName = "DriverEmail";
            this.colDriverEmail.Name = "colDriverEmail";
            this.colDriverEmail.OptionsColumn.AllowEdit = false;
            this.colDriverEmail.OptionsColumn.AllowFocus = false;
            this.colDriverEmail.OptionsColumn.ReadOnly = true;
            this.colDriverEmail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDriverEmail.Visible = true;
            this.colDriverEmail.VisibleIndex = 22;
            // 
            // colMode4
            // 
            this.colMode4.FieldName = "Mode";
            this.colMode4.Name = "colMode4";
            this.colMode4.OptionsColumn.AllowEdit = false;
            this.colMode4.OptionsColumn.AllowFocus = false;
            this.colMode4.OptionsColumn.ReadOnly = true;
            this.colMode4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID4
            // 
            this.colRecordID4.FieldName = "RecordID";
            this.colRecordID4.Name = "colRecordID4";
            this.colRecordID4.OptionsColumn.AllowEdit = false;
            this.colRecordID4.OptionsColumn.AllowFocus = false;
            this.colRecordID4.OptionsColumn.ReadOnly = true;
            this.colRecordID4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ReadOnly = true;
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ReadOnly = true;
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // keeperTabPage
            // 
            this.keeperTabPage.AutoScroll = true;
            this.keeperTabPage.Controls.Add(this.keeperGridControl);
            this.keeperTabPage.Name = "keeperTabPage";
            this.keeperTabPage.Size = new System.Drawing.Size(1357, 168);
            this.keeperTabPage.Text = "Keeper Allocations";
            // 
            // keeperGridControl
            // 
            this.keeperGridControl.DataSource = this.spAS11044KeeperAllocationItemBindingSource;
            this.keeperGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.keeperGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.keeperGridControl.Location = new System.Drawing.Point(0, 0);
            this.keeperGridControl.MainView = this.keeperGridView;
            this.keeperGridControl.MenuManager = this.barManager1;
            this.keeperGridControl.Name = "keeperGridControl";
            this.keeperGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lueKeeperNotes});
            this.keeperGridControl.Size = new System.Drawing.Size(1357, 168);
            this.keeperGridControl.TabIndex = 2;
            this.keeperGridControl.Tag = "Keeper Allocations";
            this.keeperGridControl.UseEmbeddedNavigator = true;
            this.keeperGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.keeperGridView});
            // 
            // spAS11044KeeperAllocationItemBindingSource
            // 
            this.spAS11044KeeperAllocationItemBindingSource.DataMember = "sp_AS_11044_Keeper_Allocation_Item";
            this.spAS11044KeeperAllocationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // keeperGridView
            // 
            this.keeperGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKeeperAllocationID,
            this.colEquipmentReference9,
            this.colKeeperTypeID,
            this.colKeeperType,
            this.colKeeperID,
            this.colKeeper,
            this.colEquipmentID11,
            this.colAllocationStatusID,
            this.colAllocationStatus,
            this.colAllocationDate,
            this.colAllocationEndDate,
            this.colNotes1,
            this.colMode7,
            this.colRecordID7});
            this.keeperGridView.GridControl = this.keeperGridControl;
            this.keeperGridView.Name = "keeperGridView";
            this.keeperGridView.OptionsCustomization.AllowGroup = false;
            this.keeperGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.keeperGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.keeperGridView.OptionsLayout.StoreAppearance = true;
            this.keeperGridView.OptionsSelection.MultiSelect = true;
            this.keeperGridView.OptionsView.ColumnAutoWidth = false;
            this.keeperGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.keeperGridView.OptionsView.ShowGroupPanel = false;
            this.keeperGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.keeperGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.keeperGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.keeperGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.keeperGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.keeperGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.keeperGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colKeeperAllocationID
            // 
            this.colKeeperAllocationID.FieldName = "KeeperAllocationID";
            this.colKeeperAllocationID.Name = "colKeeperAllocationID";
            this.colKeeperAllocationID.OptionsColumn.AllowEdit = false;
            this.colKeeperAllocationID.OptionsColumn.AllowFocus = false;
            this.colKeeperAllocationID.OptionsColumn.ReadOnly = true;
            this.colKeeperAllocationID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAllocationID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAllocationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAllocationID.Width = 119;
            // 
            // colEquipmentReference9
            // 
            this.colEquipmentReference9.FieldName = "EquipmentReference";
            this.colEquipmentReference9.Name = "colEquipmentReference9";
            this.colEquipmentReference9.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference9.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference9.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference9.Visible = true;
            this.colEquipmentReference9.VisibleIndex = 0;
            this.colEquipmentReference9.Width = 125;
            // 
            // colKeeperTypeID
            // 
            this.colKeeperTypeID.FieldName = "KeeperTypeID";
            this.colKeeperTypeID.Name = "colKeeperTypeID";
            this.colKeeperTypeID.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperTypeID.Width = 97;
            // 
            // colKeeperType
            // 
            this.colKeeperType.FieldName = "KeeperType";
            this.colKeeperType.Name = "colKeeperType";
            this.colKeeperType.OptionsColumn.AllowEdit = false;
            this.colKeeperType.OptionsColumn.AllowFocus = false;
            this.colKeeperType.OptionsColumn.ReadOnly = true;
            this.colKeeperType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperType.Visible = true;
            this.colKeeperType.VisibleIndex = 1;
            this.colKeeperType.Width = 142;
            // 
            // colKeeperID
            // 
            this.colKeeperID.FieldName = "KeeperID";
            this.colKeeperID.Name = "colKeeperID";
            this.colKeeperID.OptionsColumn.AllowEdit = false;
            this.colKeeperID.OptionsColumn.AllowFocus = false;
            this.colKeeperID.OptionsColumn.ReadOnly = true;
            this.colKeeperID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colKeeper
            // 
            this.colKeeper.FieldName = "Keeper";
            this.colKeeper.Name = "colKeeper";
            this.colKeeper.OptionsColumn.AllowEdit = false;
            this.colKeeper.OptionsColumn.AllowFocus = false;
            this.colKeeper.OptionsColumn.ReadOnly = true;
            this.colKeeper.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeper.Visible = true;
            this.colKeeper.VisibleIndex = 2;
            this.colKeeper.Width = 326;
            // 
            // colEquipmentID11
            // 
            this.colEquipmentID11.FieldName = "EquipmentID";
            this.colEquipmentID11.Name = "colEquipmentID11";
            this.colEquipmentID11.OptionsColumn.AllowEdit = false;
            this.colEquipmentID11.OptionsColumn.AllowFocus = false;
            this.colEquipmentID11.OptionsColumn.ReadOnly = true;
            this.colEquipmentID11.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID11.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID11.Width = 86;
            // 
            // colAllocationStatusID
            // 
            this.colAllocationStatusID.FieldName = "AllocationStatusID";
            this.colAllocationStatusID.Name = "colAllocationStatusID";
            this.colAllocationStatusID.OptionsColumn.AllowEdit = false;
            this.colAllocationStatusID.OptionsColumn.AllowFocus = false;
            this.colAllocationStatusID.OptionsColumn.ReadOnly = true;
            this.colAllocationStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colAllocationStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colAllocationStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationStatusID.Width = 116;
            // 
            // colAllocationStatus
            // 
            this.colAllocationStatus.FieldName = "AllocationStatus";
            this.colAllocationStatus.Name = "colAllocationStatus";
            this.colAllocationStatus.OptionsColumn.AllowEdit = false;
            this.colAllocationStatus.OptionsColumn.AllowFocus = false;
            this.colAllocationStatus.OptionsColumn.ReadOnly = true;
            this.colAllocationStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationStatus.Visible = true;
            this.colAllocationStatus.VisibleIndex = 3;
            this.colAllocationStatus.Width = 176;
            // 
            // colAllocationDate
            // 
            this.colAllocationDate.FieldName = "AllocationDate";
            this.colAllocationDate.Name = "colAllocationDate";
            this.colAllocationDate.OptionsColumn.AllowEdit = false;
            this.colAllocationDate.OptionsColumn.AllowFocus = false;
            this.colAllocationDate.OptionsColumn.ReadOnly = true;
            this.colAllocationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationDate.Visible = true;
            this.colAllocationDate.VisibleIndex = 4;
            this.colAllocationDate.Width = 126;
            // 
            // colAllocationEndDate
            // 
            this.colAllocationEndDate.FieldName = "AllocationEndDate";
            this.colAllocationEndDate.Name = "colAllocationEndDate";
            this.colAllocationEndDate.OptionsColumn.AllowEdit = false;
            this.colAllocationEndDate.OptionsColumn.AllowFocus = false;
            this.colAllocationEndDate.OptionsColumn.ReadOnly = true;
            this.colAllocationEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationEndDate.Visible = true;
            this.colAllocationEndDate.VisibleIndex = 5;
            this.colAllocationEndDate.Width = 115;
            // 
            // colNotes1
            // 
            this.colNotes1.FieldName = "Notes";
            this.colNotes1.Name = "colNotes1";
            this.colNotes1.OptionsColumn.AllowEdit = false;
            this.colNotes1.OptionsColumn.AllowFocus = false;
            this.colNotes1.OptionsColumn.ReadOnly = true;
            this.colNotes1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes1.Visible = true;
            this.colNotes1.VisibleIndex = 6;
            this.colNotes1.Width = 419;
            // 
            // colMode7
            // 
            this.colMode7.FieldName = "Mode";
            this.colMode7.Name = "colMode7";
            this.colMode7.OptionsColumn.AllowEdit = false;
            this.colMode7.OptionsColumn.AllowFocus = false;
            this.colMode7.OptionsColumn.ReadOnly = true;
            this.colMode7.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode7.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID7
            // 
            this.colRecordID7.FieldName = "RecordID";
            this.colRecordID7.Name = "colRecordID7";
            this.colRecordID7.OptionsColumn.AllowEdit = false;
            this.colRecordID7.OptionsColumn.AllowFocus = false;
            this.colRecordID7.OptionsColumn.ReadOnly = true;
            this.colRecordID7.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID7.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // lueKeeperNotes
            // 
            this.lueKeeperNotes.Name = "lueKeeperNotes";
            // 
            // transactionTabPage
            // 
            this.transactionTabPage.AutoScroll = true;
            this.transactionTabPage.Controls.Add(this.transactionGridControl);
            this.transactionTabPage.Name = "transactionTabPage";
            this.transactionTabPage.Size = new System.Drawing.Size(1357, 168);
            this.transactionTabPage.Text = "Transactions";
            // 
            // transactionGridControl
            // 
            this.transactionGridControl.DataSource = this.spAS11041TransactionItemBindingSource;
            this.transactionGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.transactionGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.transactionGridControl.Location = new System.Drawing.Point(0, 0);
            this.transactionGridControl.MainView = this.transactionsGridView;
            this.transactionGridControl.MenuManager = this.barManager1;
            this.transactionGridControl.Name = "transactionGridControl";
            this.transactionGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceOnExchequer,
            this.moneyTextEdit});
            this.transactionGridControl.Size = new System.Drawing.Size(1357, 168);
            this.transactionGridControl.TabIndex = 3;
            this.transactionGridControl.Tag = "Transaction Details";
            this.transactionGridControl.UseEmbeddedNavigator = true;
            this.transactionGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.transactionsGridView});
            // 
            // spAS11041TransactionItemBindingSource
            // 
            this.spAS11041TransactionItemBindingSource.DataMember = "sp_AS_11041_Transaction_Item";
            this.spAS11041TransactionItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // transactionsGridView
            // 
            this.transactionsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransactionID1,
            this.colEquipmentReference10,
            this.colEquipmentID12,
            this.colTransactionTypeID,
            this.colTransactionType,
            this.colTransactionDate,
            this.colTransactionOrderNumber,
            this.colPurchaseInvoice,
            this.colDescription1,
            this.colCompletedOnExchequer,
            this.colNetTransactionPrice,
            this.colVAT,
            this.colMode8,
            this.colRecordID8});
            this.transactionsGridView.GridControl = this.transactionGridControl;
            this.transactionsGridView.Name = "transactionsGridView";
            this.transactionsGridView.OptionsCustomization.AllowGroup = false;
            this.transactionsGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.transactionsGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.transactionsGridView.OptionsLayout.StoreAppearance = true;
            this.transactionsGridView.OptionsSelection.MultiSelect = true;
            this.transactionsGridView.OptionsView.ColumnAutoWidth = false;
            this.transactionsGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.transactionsGridView.OptionsView.ShowGroupPanel = false;
            this.transactionsGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.transactionsGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.transactionsGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.transactionsGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.transactionsGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.transactionsGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.transactionsGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colTransactionID1
            // 
            this.colTransactionID1.FieldName = "TransactionID";
            this.colTransactionID1.Name = "colTransactionID1";
            this.colTransactionID1.OptionsColumn.AllowEdit = false;
            this.colTransactionID1.OptionsColumn.AllowFocus = false;
            this.colTransactionID1.OptionsColumn.ReadOnly = true;
            this.colTransactionID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionID1.Width = 92;
            // 
            // colEquipmentReference10
            // 
            this.colEquipmentReference10.FieldName = "EquipmentReference";
            this.colEquipmentReference10.Name = "colEquipmentReference10";
            this.colEquipmentReference10.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference10.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference10.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference10.Visible = true;
            this.colEquipmentReference10.VisibleIndex = 0;
            this.colEquipmentReference10.Width = 125;
            // 
            // colEquipmentID12
            // 
            this.colEquipmentID12.FieldName = "EquipmentID";
            this.colEquipmentID12.Name = "colEquipmentID12";
            this.colEquipmentID12.OptionsColumn.AllowEdit = false;
            this.colEquipmentID12.OptionsColumn.AllowFocus = false;
            this.colEquipmentID12.OptionsColumn.ReadOnly = true;
            this.colEquipmentID12.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID12.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID12.Width = 86;
            // 
            // colTransactionTypeID
            // 
            this.colTransactionTypeID.FieldName = "TransactionTypeID";
            this.colTransactionTypeID.Name = "colTransactionTypeID";
            this.colTransactionTypeID.OptionsColumn.AllowEdit = false;
            this.colTransactionTypeID.OptionsColumn.AllowFocus = false;
            this.colTransactionTypeID.OptionsColumn.ReadOnly = true;
            this.colTransactionTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionTypeID.Width = 119;
            // 
            // colTransactionType
            // 
            this.colTransactionType.FieldName = "TransactionType";
            this.colTransactionType.Name = "colTransactionType";
            this.colTransactionType.OptionsColumn.AllowEdit = false;
            this.colTransactionType.OptionsColumn.AllowFocus = false;
            this.colTransactionType.OptionsColumn.ReadOnly = true;
            this.colTransactionType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionType.Visible = true;
            this.colTransactionType.VisibleIndex = 1;
            this.colTransactionType.Width = 105;
            // 
            // colTransactionDate
            // 
            this.colTransactionDate.FieldName = "TransactionDate";
            this.colTransactionDate.Name = "colTransactionDate";
            this.colTransactionDate.OptionsColumn.AllowEdit = false;
            this.colTransactionDate.OptionsColumn.AllowFocus = false;
            this.colTransactionDate.OptionsColumn.ReadOnly = true;
            this.colTransactionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionDate.Visible = true;
            this.colTransactionDate.VisibleIndex = 2;
            this.colTransactionDate.Width = 104;
            // 
            // colTransactionOrderNumber
            // 
            this.colTransactionOrderNumber.FieldName = "TransactionOrderNumber";
            this.colTransactionOrderNumber.Name = "colTransactionOrderNumber";
            this.colTransactionOrderNumber.OptionsColumn.AllowEdit = false;
            this.colTransactionOrderNumber.OptionsColumn.AllowFocus = false;
            this.colTransactionOrderNumber.OptionsColumn.ReadOnly = true;
            this.colTransactionOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionOrderNumber.Visible = true;
            this.colTransactionOrderNumber.VisibleIndex = 3;
            this.colTransactionOrderNumber.Width = 149;
            // 
            // colPurchaseInvoice
            // 
            this.colPurchaseInvoice.FieldName = "PurchaseInvoice";
            this.colPurchaseInvoice.Name = "colPurchaseInvoice";
            this.colPurchaseInvoice.OptionsColumn.AllowEdit = false;
            this.colPurchaseInvoice.OptionsColumn.AllowFocus = false;
            this.colPurchaseInvoice.OptionsColumn.ReadOnly = true;
            this.colPurchaseInvoice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPurchaseInvoice.Visible = true;
            this.colPurchaseInvoice.VisibleIndex = 4;
            this.colPurchaseInvoice.Width = 104;
            // 
            // colDescription1
            // 
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 5;
            this.colDescription1.Width = 425;
            // 
            // colCompletedOnExchequer
            // 
            this.colCompletedOnExchequer.FieldName = "CompletedOnExchequer";
            this.colCompletedOnExchequer.Name = "colCompletedOnExchequer";
            this.colCompletedOnExchequer.OptionsColumn.AllowEdit = false;
            this.colCompletedOnExchequer.OptionsColumn.AllowFocus = false;
            this.colCompletedOnExchequer.OptionsColumn.ReadOnly = true;
            this.colCompletedOnExchequer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompletedOnExchequer.Width = 144;
            // 
            // colNetTransactionPrice
            // 
            this.colNetTransactionPrice.ColumnEdit = this.moneyTextEdit;
            this.colNetTransactionPrice.FieldName = "NetTransactionPrice";
            this.colNetTransactionPrice.Name = "colNetTransactionPrice";
            this.colNetTransactionPrice.OptionsColumn.AllowEdit = false;
            this.colNetTransactionPrice.OptionsColumn.AllowFocus = false;
            this.colNetTransactionPrice.OptionsColumn.ReadOnly = true;
            this.colNetTransactionPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNetTransactionPrice.Visible = true;
            this.colNetTransactionPrice.VisibleIndex = 6;
            this.colNetTransactionPrice.Width = 124;
            // 
            // moneyTextEdit
            // 
            this.moneyTextEdit.AutoHeight = false;
            this.moneyTextEdit.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit.Name = "moneyTextEdit";
            // 
            // colVAT
            // 
            this.colVAT.ColumnEdit = this.moneyTextEdit;
            this.colVAT.FieldName = "VAT";
            this.colVAT.Name = "colVAT";
            this.colVAT.OptionsColumn.AllowEdit = false;
            this.colVAT.OptionsColumn.AllowFocus = false;
            this.colVAT.OptionsColumn.ReadOnly = true;
            this.colVAT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 7;
            // 
            // colMode8
            // 
            this.colMode8.FieldName = "Mode";
            this.colMode8.Name = "colMode8";
            this.colMode8.OptionsColumn.AllowEdit = false;
            this.colMode8.OptionsColumn.AllowFocus = false;
            this.colMode8.OptionsColumn.ReadOnly = true;
            this.colMode8.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode8.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID8
            // 
            this.colRecordID8.FieldName = "RecordID";
            this.colRecordID8.Name = "colRecordID8";
            this.colRecordID8.OptionsColumn.AllowEdit = false;
            this.colRecordID8.OptionsColumn.AllowFocus = false;
            this.colRecordID8.OptionsColumn.ReadOnly = true;
            this.colRecordID8.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID8.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ceOnExchequer
            // 
            this.ceOnExchequer.AutoHeight = false;
            this.ceOnExchequer.Caption = "Check";
            this.ceOnExchequer.Name = "ceOnExchequer";
            // 
            // notificationTabPage
            // 
            this.notificationTabPage.Controls.Add(this.notificationGridControl);
            this.notificationTabPage.Name = "notificationTabPage";
            this.notificationTabPage.Size = new System.Drawing.Size(1357, 168);
            this.notificationTabPage.Text = "Notification Details";
            // 
            // notificationGridControl
            // 
            this.notificationGridControl.DataSource = this.spAS11126NotificationItemBindingSource;
            this.notificationGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.notificationGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.notificationGridControl.Location = new System.Drawing.Point(0, 0);
            this.notificationGridControl.MainView = this.notificationGridView;
            this.notificationGridControl.MenuManager = this.barManager1;
            this.notificationGridControl.Name = "notificationGridControl";
            this.notificationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemMemoExEdit15});
            this.notificationGridControl.Size = new System.Drawing.Size(1357, 168);
            this.notificationGridControl.TabIndex = 2;
            this.notificationGridControl.Tag = "Notification Details";
            this.notificationGridControl.UseEmbeddedNavigator = true;
            this.notificationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.notificationGridView});
            // 
            // spAS11126NotificationItemBindingSource
            // 
            this.spAS11126NotificationItemBindingSource.DataMember = "sp_AS_11126_Notification_Item";
            this.spAS11126NotificationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // notificationGridView
            // 
            this.notificationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNotificationID,
            this.colNotificationTypeID,
            this.colNotificationType,
            this.colLinkedToRecordID,
            this.colLinkedToRecordType,
            this.gridColumn1,
            this.colPriorityID,
            this.colPriority,
            this.colDateToRemind,
            this.colDateCreated,
            this.colKeeperTypeID1,
            this.colKeeperType1,
            this.colKeeperID1,
            this.colFullName,
            this.colMessage,
            this.colStatusID1,
            this.colStatus1,
            this.gridColumn2,
            this.gridColumn3});
            this.notificationGridView.GridControl = this.notificationGridControl;
            this.notificationGridView.Name = "notificationGridView";
            this.notificationGridView.OptionsCustomization.AllowFilter = false;
            this.notificationGridView.OptionsCustomization.AllowGroup = false;
            this.notificationGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.notificationGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.notificationGridView.OptionsLayout.StoreAppearance = true;
            this.notificationGridView.OptionsSelection.MultiSelect = true;
            this.notificationGridView.OptionsView.ColumnAutoWidth = false;
            this.notificationGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.notificationGridView.OptionsView.ShowGroupPanel = false;
            this.notificationGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.notificationGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.notificationGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.notificationGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.notificationGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.notificationGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.notificationGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colNotificationID
            // 
            this.colNotificationID.FieldName = "NotificationID";
            this.colNotificationID.Name = "colNotificationID";
            this.colNotificationID.OptionsColumn.AllowEdit = false;
            this.colNotificationID.OptionsColumn.AllowFocus = false;
            this.colNotificationID.OptionsColumn.ReadOnly = true;
            this.colNotificationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotificationID.Width = 80;
            // 
            // colNotificationTypeID
            // 
            this.colNotificationTypeID.FieldName = "NotificationTypeID";
            this.colNotificationTypeID.Name = "colNotificationTypeID";
            this.colNotificationTypeID.OptionsColumn.AllowEdit = false;
            this.colNotificationTypeID.OptionsColumn.AllowFocus = false;
            this.colNotificationTypeID.OptionsColumn.ReadOnly = true;
            this.colNotificationTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotificationTypeID.Width = 107;
            // 
            // colNotificationType
            // 
            this.colNotificationType.FieldName = "NotificationType";
            this.colNotificationType.Name = "colNotificationType";
            this.colNotificationType.OptionsColumn.AllowEdit = false;
            this.colNotificationType.OptionsColumn.AllowFocus = false;
            this.colNotificationType.OptionsColumn.ReadOnly = true;
            this.colNotificationType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotificationType.Visible = true;
            this.colNotificationType.VisibleIndex = 1;
            this.colNotificationType.Width = 157;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToRecordID.Width = 108;
            // 
            // colLinkedToRecordType
            // 
            this.colLinkedToRecordType.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType.Name = "colLinkedToRecordType";
            this.colLinkedToRecordType.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToRecordType.Width = 121;
            // 
            // gridColumn1
            // 
            this.gridColumn1.FieldName = "EquipmentReference";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 117;
            // 
            // colPriorityID
            // 
            this.colPriorityID.FieldName = "PriorityID";
            this.colPriorityID.Name = "colPriorityID";
            this.colPriorityID.OptionsColumn.AllowEdit = false;
            this.colPriorityID.OptionsColumn.AllowFocus = false;
            this.colPriorityID.OptionsColumn.ReadOnly = true;
            this.colPriorityID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPriority
            // 
            this.colPriority.FieldName = "Priority";
            this.colPriority.Name = "colPriority";
            this.colPriority.OptionsColumn.AllowEdit = false;
            this.colPriority.OptionsColumn.AllowFocus = false;
            this.colPriority.OptionsColumn.ReadOnly = true;
            this.colPriority.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPriority.Visible = true;
            this.colPriority.VisibleIndex = 2;
            this.colPriority.Width = 133;
            // 
            // colDateToRemind
            // 
            this.colDateToRemind.FieldName = "DateToRemind";
            this.colDateToRemind.Name = "colDateToRemind";
            this.colDateToRemind.OptionsColumn.AllowEdit = false;
            this.colDateToRemind.OptionsColumn.AllowFocus = false;
            this.colDateToRemind.OptionsColumn.ReadOnly = true;
            this.colDateToRemind.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateToRemind.Visible = true;
            this.colDateToRemind.VisibleIndex = 3;
            this.colDateToRemind.Width = 88;
            // 
            // colDateCreated
            // 
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 4;
            this.colDateCreated.Width = 77;
            // 
            // colKeeperTypeID1
            // 
            this.colKeeperTypeID1.FieldName = "KeeperTypeID";
            this.colKeeperTypeID1.Name = "colKeeperTypeID1";
            this.colKeeperTypeID1.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID1.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID1.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperTypeID1.Width = 87;
            // 
            // colKeeperType1
            // 
            this.colKeeperType1.FieldName = "KeeperType";
            this.colKeeperType1.Name = "colKeeperType1";
            this.colKeeperType1.OptionsColumn.AllowEdit = false;
            this.colKeeperType1.OptionsColumn.AllowFocus = false;
            this.colKeeperType1.OptionsColumn.ReadOnly = true;
            this.colKeeperType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperType1.Visible = true;
            this.colKeeperType1.VisibleIndex = 5;
            this.colKeeperType1.Width = 148;
            // 
            // colKeeperID1
            // 
            this.colKeeperID1.FieldName = "KeeperID";
            this.colKeeperID1.Name = "colKeeperID1";
            this.colKeeperID1.OptionsColumn.AllowEdit = false;
            this.colKeeperID1.OptionsColumn.AllowFocus = false;
            this.colKeeperID1.OptionsColumn.ReadOnly = true;
            this.colKeeperID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colFullName
            // 
            this.colFullName.FieldName = "FullName";
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.AllowEdit = false;
            this.colFullName.OptionsColumn.AllowFocus = false;
            this.colFullName.OptionsColumn.ReadOnly = true;
            this.colFullName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 6;
            this.colFullName.Width = 202;
            // 
            // colMessage
            // 
            this.colMessage.FieldName = "Message";
            this.colMessage.Name = "colMessage";
            this.colMessage.OptionsColumn.AllowEdit = false;
            this.colMessage.OptionsColumn.AllowFocus = false;
            this.colMessage.OptionsColumn.ReadOnly = true;
            this.colMessage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMessage.Visible = true;
            this.colMessage.VisibleIndex = 8;
            this.colMessage.Width = 728;
            // 
            // colStatusID1
            // 
            this.colStatusID1.FieldName = "StatusID";
            this.colStatusID1.Name = "colStatusID1";
            this.colStatusID1.OptionsColumn.AllowEdit = false;
            this.colStatusID1.OptionsColumn.AllowFocus = false;
            this.colStatusID1.OptionsColumn.ReadOnly = true;
            this.colStatusID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus1
            // 
            this.colStatus1.FieldName = "Status";
            this.colStatus1.Name = "colStatus1";
            this.colStatus1.OptionsColumn.AllowEdit = false;
            this.colStatus1.OptionsColumn.AllowFocus = false;
            this.colStatus1.OptionsColumn.ReadOnly = true;
            this.colStatus1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus1.Visible = true;
            this.colStatus1.VisibleIndex = 7;
            this.colStatus1.Width = 128;
            // 
            // gridColumn2
            // 
            this.gridColumn2.FieldName = "Mode";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn3
            // 
            this.gridColumn3.FieldName = "RecordID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ReadOnly = true;
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit15
            // 
            this.repositoryItemMemoExEdit15.AutoHeight = false;
            this.repositoryItemMemoExEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit15.Name = "repositoryItemMemoExEdit15";
            this.repositoryItemMemoExEdit15.ReadOnly = true;
            this.repositoryItemMemoExEdit15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit15.ShowIcon = false;
            // 
            // coverTabPage
            // 
            this.coverTabPage.Controls.Add(this.coverGridControl);
            this.coverTabPage.Name = "coverTabPage";
            this.coverTabPage.Size = new System.Drawing.Size(1357, 168);
            this.coverTabPage.Text = "Policy and Insurance Cover Details";
            // 
            // coverGridControl
            // 
            this.coverGridControl.DataSource = this.spAS11075CoverItemBindingSource;
            this.coverGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.coverGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.coverGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.coverGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.coverGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.coverGridControl.Location = new System.Drawing.Point(0, 0);
            this.coverGridControl.MainView = this.coverGridView;
            this.coverGridControl.MenuManager = this.barManager1;
            this.coverGridControl.Name = "coverGridControl";
            this.coverGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit9,
            this.moneyTextEdit1});
            this.coverGridControl.Size = new System.Drawing.Size(1357, 168);
            this.coverGridControl.TabIndex = 1;
            this.coverGridControl.Tag = "Cover Details";
            this.coverGridControl.UseEmbeddedNavigator = true;
            this.coverGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.coverGridView});
            // 
            // spAS11075CoverItemBindingSource
            // 
            this.spAS11075CoverItemBindingSource.DataMember = "sp_AS_11075_Cover_Item";
            this.spAS11075CoverItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // coverGridView
            // 
            this.coverGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCoverID,
            this.colEquipmentReference16,
            this.colEquipmentID3,
            this.colCoverType,
            this.colCoverTypeID,
            this.colSupplierID1,
            this.colSupplierReference1,
            this.colPolicy,
            this.colAnnualCoverCost,
            this.colRenewalDate,
            this.colNotes3,
            this.colExcess,
            this.colMode11,
            this.colRecordID11});
            this.coverGridView.GridControl = this.coverGridControl;
            this.coverGridView.Name = "coverGridView";
            this.coverGridView.OptionsCustomization.AllowFilter = false;
            this.coverGridView.OptionsCustomization.AllowGroup = false;
            this.coverGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.coverGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.coverGridView.OptionsLayout.StoreAppearance = true;
            this.coverGridView.OptionsSelection.MultiSelect = true;
            this.coverGridView.OptionsView.ColumnAutoWidth = false;
            this.coverGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.coverGridView.OptionsView.ShowGroupPanel = false;
            this.coverGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.coverGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.coverGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.coverGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.coverGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.coverGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.coverGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colCoverID
            // 
            this.colCoverID.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colCoverID.FieldName = "CoverID";
            this.colCoverID.Name = "colCoverID";
            this.colCoverID.OptionsColumn.AllowEdit = false;
            this.colCoverID.OptionsColumn.AllowFocus = false;
            this.colCoverID.OptionsColumn.ReadOnly = true;
            this.colCoverID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ReadOnly = true;
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // colEquipmentReference16
            // 
            this.colEquipmentReference16.FieldName = "EquipmentReference";
            this.colEquipmentReference16.Name = "colEquipmentReference16";
            this.colEquipmentReference16.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference16.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference16.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference16.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference16.Visible = true;
            this.colEquipmentReference16.VisibleIndex = 0;
            this.colEquipmentReference16.Width = 148;
            // 
            // colEquipmentID3
            // 
            this.colEquipmentID3.FieldName = "EquipmentID";
            this.colEquipmentID3.Name = "colEquipmentID3";
            this.colEquipmentID3.OptionsColumn.AllowEdit = false;
            this.colEquipmentID3.OptionsColumn.AllowFocus = false;
            this.colEquipmentID3.OptionsColumn.ReadOnly = true;
            this.colEquipmentID3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID3.Width = 76;
            // 
            // colCoverType
            // 
            this.colCoverType.FieldName = "CoverType";
            this.colCoverType.Name = "colCoverType";
            this.colCoverType.OptionsColumn.AllowEdit = false;
            this.colCoverType.OptionsColumn.AllowFocus = false;
            this.colCoverType.OptionsColumn.ReadOnly = true;
            this.colCoverType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCoverType.Visible = true;
            this.colCoverType.VisibleIndex = 1;
            // 
            // colCoverTypeID
            // 
            this.colCoverTypeID.FieldName = "CoverTypeID";
            this.colCoverTypeID.Name = "colCoverTypeID";
            this.colCoverTypeID.OptionsColumn.AllowEdit = false;
            this.colCoverTypeID.OptionsColumn.AllowFocus = false;
            this.colCoverTypeID.OptionsColumn.ReadOnly = true;
            this.colCoverTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCoverTypeID.Width = 82;
            // 
            // colSupplierID1
            // 
            this.colSupplierID1.FieldName = "SupplierID";
            this.colSupplierID1.Name = "colSupplierID1";
            this.colSupplierID1.OptionsColumn.AllowEdit = false;
            this.colSupplierID1.OptionsColumn.AllowFocus = false;
            this.colSupplierID1.OptionsColumn.ReadOnly = true;
            this.colSupplierID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSupplierReference1
            // 
            this.colSupplierReference1.FieldName = "SupplierReference";
            this.colSupplierReference1.Name = "colSupplierReference1";
            this.colSupplierReference1.OptionsColumn.AllowEdit = false;
            this.colSupplierReference1.OptionsColumn.AllowFocus = false;
            this.colSupplierReference1.OptionsColumn.ReadOnly = true;
            this.colSupplierReference1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference1.Visible = true;
            this.colSupplierReference1.VisibleIndex = 2;
            this.colSupplierReference1.Width = 154;
            // 
            // colPolicy
            // 
            this.colPolicy.FieldName = "Policy";
            this.colPolicy.Name = "colPolicy";
            this.colPolicy.OptionsColumn.AllowEdit = false;
            this.colPolicy.OptionsColumn.AllowFocus = false;
            this.colPolicy.OptionsColumn.ReadOnly = true;
            this.colPolicy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPolicy.Visible = true;
            this.colPolicy.VisibleIndex = 3;
            // 
            // colAnnualCoverCost
            // 
            this.colAnnualCoverCost.ColumnEdit = this.moneyTextEdit1;
            this.colAnnualCoverCost.FieldName = "AnnualCoverCost";
            this.colAnnualCoverCost.Name = "colAnnualCoverCost";
            this.colAnnualCoverCost.OptionsColumn.AllowEdit = false;
            this.colAnnualCoverCost.OptionsColumn.AllowFocus = false;
            this.colAnnualCoverCost.OptionsColumn.ReadOnly = true;
            this.colAnnualCoverCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAnnualCoverCost.Visible = true;
            this.colAnnualCoverCost.VisibleIndex = 4;
            this.colAnnualCoverCost.Width = 149;
            // 
            // moneyTextEdit1
            // 
            this.moneyTextEdit1.AutoHeight = false;
            this.moneyTextEdit1.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit1.Name = "moneyTextEdit1";
            // 
            // colRenewalDate
            // 
            this.colRenewalDate.FieldName = "RenewalDate";
            this.colRenewalDate.Name = "colRenewalDate";
            this.colRenewalDate.OptionsColumn.AllowEdit = false;
            this.colRenewalDate.OptionsColumn.AllowFocus = false;
            this.colRenewalDate.OptionsColumn.ReadOnly = true;
            this.colRenewalDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRenewalDate.Visible = true;
            this.colRenewalDate.VisibleIndex = 5;
            this.colRenewalDate.Width = 143;
            // 
            // colNotes3
            // 
            this.colNotes3.FieldName = "Notes";
            this.colNotes3.Name = "colNotes3";
            this.colNotes3.OptionsColumn.AllowEdit = false;
            this.colNotes3.OptionsColumn.AllowFocus = false;
            this.colNotes3.OptionsColumn.ReadOnly = true;
            this.colNotes3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes3.Visible = true;
            this.colNotes3.VisibleIndex = 7;
            this.colNotes3.Width = 335;
            // 
            // colExcess
            // 
            this.colExcess.ColumnEdit = this.moneyTextEdit1;
            this.colExcess.FieldName = "Excess";
            this.colExcess.Name = "colExcess";
            this.colExcess.OptionsColumn.AllowEdit = false;
            this.colExcess.OptionsColumn.AllowFocus = false;
            this.colExcess.OptionsColumn.ReadOnly = true;
            this.colExcess.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExcess.Visible = true;
            this.colExcess.VisibleIndex = 6;
            this.colExcess.Width = 91;
            // 
            // colMode11
            // 
            this.colMode11.FieldName = "Mode";
            this.colMode11.Name = "colMode11";
            this.colMode11.OptionsColumn.AllowEdit = false;
            this.colMode11.OptionsColumn.AllowFocus = false;
            this.colMode11.OptionsColumn.ReadOnly = true;
            this.colMode11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID11
            // 
            this.colRecordID11.FieldName = "RecordID";
            this.colRecordID11.Name = "colRecordID11";
            this.colRecordID11.OptionsColumn.AllowEdit = false;
            this.colRecordID11.OptionsColumn.AllowFocus = false;
            this.colRecordID11.OptionsColumn.ReadOnly = true;
            this.colRecordID11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // serviceIntervalTabPage
            // 
            this.serviceIntervalTabPage.Controls.Add(this.serviceIntervalGridControl);
            this.serviceIntervalTabPage.Name = "serviceIntervalTabPage";
            this.serviceIntervalTabPage.Size = new System.Drawing.Size(1357, 168);
            this.serviceIntervalTabPage.Text = "Service Data";
            // 
            // serviceIntervalGridControl
            // 
            this.serviceIntervalGridControl.DataSource = this.spAS11054ServiceDataItemBindingSource;
            this.serviceIntervalGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.serviceIntervalGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.serviceIntervalGridControl.Location = new System.Drawing.Point(0, 0);
            this.serviceIntervalGridControl.MainView = this.serviceIntervalGridView;
            this.serviceIntervalGridControl.MenuManager = this.barManager1;
            this.serviceIntervalGridControl.Name = "serviceIntervalGridControl";
            this.serviceIntervalGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit11,
            this.repositoryItemMemoExEdit12});
            this.serviceIntervalGridControl.Size = new System.Drawing.Size(1357, 168);
            this.serviceIntervalGridControl.TabIndex = 1;
            this.serviceIntervalGridControl.Tag = "Service Interval Details";
            this.serviceIntervalGridControl.UseEmbeddedNavigator = true;
            this.serviceIntervalGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.serviceIntervalGridView});
            // 
            // spAS11054ServiceDataItemBindingSource
            // 
            this.spAS11054ServiceDataItemBindingSource.DataMember = "sp_AS_11054_Service_Data_Item";
            this.spAS11054ServiceDataItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // serviceIntervalGridView
            // 
            this.serviceIntervalGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colServiceDataID,
            this.colServiceIntervalScheduleID,
            this.colEquipmentID4,
            this.colEquipmentReference2,
            this.colServiceTypeID,
            this.colServiceType1,
            this.colDistanceUnitsID,
            this.colDistanceUnits,
            this.colDistanceFrequency,
            this.colTimeUnitsID,
            this.colTimeUnits,
            this.colTimeFrequency,
            this.colWarrantyPeriod,
            this.colWarrantyEndDate,
            this.colWarrantyEndDistance,
            this.colServiceDataNotes,
            this.colServiceStatus,
            this.colServiceDueDistance,
            this.colServiceDueDate,
            this.colAlertWithinXDistance,
            this.colAlertWithinXTime,
            this.colWorkType1,
            this.colIntervalScheduleNotes,
            this.colIsCurrentInterval,
            this.colMode2,
            this.colRecordID2});
            this.serviceIntervalGridView.GridControl = this.serviceIntervalGridControl;
            this.serviceIntervalGridView.Name = "serviceIntervalGridView";
            this.serviceIntervalGridView.OptionsCustomization.AllowFilter = false;
            this.serviceIntervalGridView.OptionsCustomization.AllowGroup = false;
            this.serviceIntervalGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.serviceIntervalGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.serviceIntervalGridView.OptionsLayout.StoreAppearance = true;
            this.serviceIntervalGridView.OptionsSelection.MultiSelect = true;
            this.serviceIntervalGridView.OptionsView.ColumnAutoWidth = false;
            this.serviceIntervalGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.serviceIntervalGridView.OptionsView.ShowGroupPanel = false;
            this.serviceIntervalGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.serviceIntervalGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.serviceIntervalGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.serviceIntervalGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.serviceIntervalGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.serviceIntervalGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.serviceIntervalGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colServiceDataID
            // 
            this.colServiceDataID.FieldName = "ServiceDataID";
            this.colServiceDataID.Name = "colServiceDataID";
            this.colServiceDataID.OptionsColumn.AllowEdit = false;
            this.colServiceDataID.OptionsColumn.AllowFocus = false;
            this.colServiceDataID.OptionsColumn.ReadOnly = true;
            this.colServiceDataID.OptionsColumn.ShowInCustomizationForm = false;
            this.colServiceDataID.OptionsColumn.ShowInExpressionEditor = false;
            this.colServiceDataID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceDataID.Width = 87;
            // 
            // colServiceIntervalScheduleID
            // 
            this.colServiceIntervalScheduleID.FieldName = "ServiceIntervalScheduleID";
            this.colServiceIntervalScheduleID.Name = "colServiceIntervalScheduleID";
            this.colServiceIntervalScheduleID.OptionsColumn.AllowEdit = false;
            this.colServiceIntervalScheduleID.OptionsColumn.AllowFocus = false;
            this.colServiceIntervalScheduleID.OptionsColumn.ReadOnly = true;
            this.colServiceIntervalScheduleID.OptionsColumn.ShowInCustomizationForm = false;
            this.colServiceIntervalScheduleID.OptionsColumn.ShowInExpressionEditor = false;
            this.colServiceIntervalScheduleID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceIntervalScheduleID.Width = 148;
            // 
            // colEquipmentID4
            // 
            this.colEquipmentID4.FieldName = "EquipmentID";
            this.colEquipmentID4.Name = "colEquipmentID4";
            this.colEquipmentID4.OptionsColumn.AllowEdit = false;
            this.colEquipmentID4.OptionsColumn.AllowFocus = false;
            this.colEquipmentID4.OptionsColumn.ReadOnly = true;
            this.colEquipmentID4.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID4.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID4.Width = 76;
            // 
            // colEquipmentReference2
            // 
            this.colEquipmentReference2.FieldName = "EquipmentReference";
            this.colEquipmentReference2.Name = "colEquipmentReference2";
            this.colEquipmentReference2.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference2.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference2.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference2.Visible = true;
            this.colEquipmentReference2.VisibleIndex = 0;
            this.colEquipmentReference2.Width = 115;
            // 
            // colServiceTypeID
            // 
            this.colServiceTypeID.FieldName = "ServiceTypeID";
            this.colServiceTypeID.Name = "colServiceTypeID";
            this.colServiceTypeID.OptionsColumn.AllowEdit = false;
            this.colServiceTypeID.OptionsColumn.AllowFocus = false;
            this.colServiceTypeID.OptionsColumn.ReadOnly = true;
            this.colServiceTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colServiceTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colServiceTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceTypeID.Width = 88;
            // 
            // colServiceType1
            // 
            this.colServiceType1.FieldName = "ServiceType";
            this.colServiceType1.Name = "colServiceType1";
            this.colServiceType1.OptionsColumn.AllowEdit = false;
            this.colServiceType1.OptionsColumn.AllowFocus = false;
            this.colServiceType1.OptionsColumn.ReadOnly = true;
            this.colServiceType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceType1.Visible = true;
            this.colServiceType1.VisibleIndex = 1;
            // 
            // colDistanceUnitsID
            // 
            this.colDistanceUnitsID.FieldName = "DistanceUnitsID";
            this.colDistanceUnitsID.Name = "colDistanceUnitsID";
            this.colDistanceUnitsID.OptionsColumn.AllowEdit = false;
            this.colDistanceUnitsID.OptionsColumn.AllowFocus = false;
            this.colDistanceUnitsID.OptionsColumn.ReadOnly = true;
            this.colDistanceUnitsID.OptionsColumn.ShowInCustomizationForm = false;
            this.colDistanceUnitsID.OptionsColumn.ShowInExpressionEditor = false;
            this.colDistanceUnitsID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistanceUnitsID.Width = 94;
            // 
            // colDistanceUnits
            // 
            this.colDistanceUnits.FieldName = "DistanceUnits";
            this.colDistanceUnits.Name = "colDistanceUnits";
            this.colDistanceUnits.OptionsColumn.AllowEdit = false;
            this.colDistanceUnits.OptionsColumn.AllowFocus = false;
            this.colDistanceUnits.OptionsColumn.ReadOnly = true;
            this.colDistanceUnits.OptionsColumn.ShowInCustomizationForm = false;
            this.colDistanceUnits.OptionsColumn.ShowInExpressionEditor = false;
            this.colDistanceUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistanceUnits.Width = 80;
            // 
            // colDistanceFrequency
            // 
            this.colDistanceFrequency.FieldName = "DistanceFrequency";
            this.colDistanceFrequency.Name = "colDistanceFrequency";
            this.colDistanceFrequency.OptionsColumn.AllowEdit = false;
            this.colDistanceFrequency.OptionsColumn.AllowFocus = false;
            this.colDistanceFrequency.OptionsColumn.ReadOnly = true;
            this.colDistanceFrequency.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistanceFrequency.Visible = true;
            this.colDistanceFrequency.VisibleIndex = 2;
            this.colDistanceFrequency.Width = 107;
            // 
            // colTimeUnitsID
            // 
            this.colTimeUnitsID.FieldName = "TimeUnitsID";
            this.colTimeUnitsID.Name = "colTimeUnitsID";
            this.colTimeUnitsID.OptionsColumn.AllowEdit = false;
            this.colTimeUnitsID.OptionsColumn.AllowFocus = false;
            this.colTimeUnitsID.OptionsColumn.ReadOnly = true;
            this.colTimeUnitsID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTimeUnitsID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTimeUnitsID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colTimeUnits
            // 
            this.colTimeUnits.FieldName = "TimeUnits";
            this.colTimeUnits.Name = "colTimeUnits";
            this.colTimeUnits.OptionsColumn.AllowEdit = false;
            this.colTimeUnits.OptionsColumn.AllowFocus = false;
            this.colTimeUnits.OptionsColumn.ReadOnly = true;
            this.colTimeUnits.OptionsColumn.ShowInCustomizationForm = false;
            this.colTimeUnits.OptionsColumn.ShowInExpressionEditor = false;
            this.colTimeUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colTimeFrequency
            // 
            this.colTimeFrequency.FieldName = "TimeFrequency";
            this.colTimeFrequency.Name = "colTimeFrequency";
            this.colTimeFrequency.OptionsColumn.AllowEdit = false;
            this.colTimeFrequency.OptionsColumn.AllowFocus = false;
            this.colTimeFrequency.OptionsColumn.ReadOnly = true;
            this.colTimeFrequency.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTimeFrequency.Visible = true;
            this.colTimeFrequency.VisibleIndex = 3;
            this.colTimeFrequency.Width = 88;
            // 
            // colWarrantyPeriod
            // 
            this.colWarrantyPeriod.FieldName = "WarrantyPeriod";
            this.colWarrantyPeriod.Name = "colWarrantyPeriod";
            this.colWarrantyPeriod.OptionsColumn.AllowEdit = false;
            this.colWarrantyPeriod.OptionsColumn.AllowFocus = false;
            this.colWarrantyPeriod.OptionsColumn.ReadOnly = true;
            this.colWarrantyPeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWarrantyPeriod.Visible = true;
            this.colWarrantyPeriod.VisibleIndex = 4;
            this.colWarrantyPeriod.Width = 91;
            // 
            // colWarrantyEndDate
            // 
            this.colWarrantyEndDate.FieldName = "WarrantyEndDate";
            this.colWarrantyEndDate.Name = "colWarrantyEndDate";
            this.colWarrantyEndDate.OptionsColumn.AllowEdit = false;
            this.colWarrantyEndDate.OptionsColumn.AllowFocus = false;
            this.colWarrantyEndDate.OptionsColumn.ReadOnly = true;
            this.colWarrantyEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWarrantyEndDate.Visible = true;
            this.colWarrantyEndDate.VisibleIndex = 5;
            this.colWarrantyEndDate.Width = 105;
            // 
            // colWarrantyEndDistance
            // 
            this.colWarrantyEndDistance.FieldName = "WarrantyEndDistance";
            this.colWarrantyEndDistance.Name = "colWarrantyEndDistance";
            this.colWarrantyEndDistance.OptionsColumn.AllowEdit = false;
            this.colWarrantyEndDistance.OptionsColumn.AllowFocus = false;
            this.colWarrantyEndDistance.OptionsColumn.ReadOnly = true;
            this.colWarrantyEndDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWarrantyEndDistance.Visible = true;
            this.colWarrantyEndDistance.VisibleIndex = 6;
            this.colWarrantyEndDistance.Width = 123;
            // 
            // colServiceDataNotes
            // 
            this.colServiceDataNotes.FieldName = "ServiceDataNotes";
            this.colServiceDataNotes.Name = "colServiceDataNotes";
            this.colServiceDataNotes.OptionsColumn.AllowEdit = false;
            this.colServiceDataNotes.OptionsColumn.AllowFocus = false;
            this.colServiceDataNotes.OptionsColumn.ReadOnly = true;
            this.colServiceDataNotes.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceDataNotes.Visible = true;
            this.colServiceDataNotes.VisibleIndex = 15;
            this.colServiceDataNotes.Width = 232;
            // 
            // colServiceStatus
            // 
            this.colServiceStatus.FieldName = "ServiceStatus";
            this.colServiceStatus.Name = "colServiceStatus";
            this.colServiceStatus.OptionsColumn.AllowEdit = false;
            this.colServiceStatus.OptionsColumn.AllowFocus = false;
            this.colServiceStatus.OptionsColumn.ReadOnly = true;
            this.colServiceStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceStatus.Visible = true;
            this.colServiceStatus.VisibleIndex = 10;
            this.colServiceStatus.Width = 81;
            // 
            // colServiceDueDistance
            // 
            this.colServiceDueDistance.FieldName = "ServiceDueDistance";
            this.colServiceDueDistance.Name = "colServiceDueDistance";
            this.colServiceDueDistance.OptionsColumn.AllowEdit = false;
            this.colServiceDueDistance.OptionsColumn.AllowFocus = false;
            this.colServiceDueDistance.OptionsColumn.ReadOnly = true;
            this.colServiceDueDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceDueDistance.Visible = true;
            this.colServiceDueDistance.VisibleIndex = 11;
            this.colServiceDueDistance.Width = 113;
            // 
            // colServiceDueDate
            // 
            this.colServiceDueDate.FieldName = "ServiceDueDate";
            this.colServiceDueDate.Name = "colServiceDueDate";
            this.colServiceDueDate.OptionsColumn.AllowEdit = false;
            this.colServiceDueDate.OptionsColumn.AllowFocus = false;
            this.colServiceDueDate.OptionsColumn.ReadOnly = true;
            this.colServiceDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceDueDate.Visible = true;
            this.colServiceDueDate.VisibleIndex = 12;
            this.colServiceDueDate.Width = 95;
            // 
            // colAlertWithinXDistance
            // 
            this.colAlertWithinXDistance.FieldName = "AlertWithinXDistance";
            this.colAlertWithinXDistance.Name = "colAlertWithinXDistance";
            this.colAlertWithinXDistance.OptionsColumn.AllowEdit = false;
            this.colAlertWithinXDistance.OptionsColumn.AllowFocus = false;
            this.colAlertWithinXDistance.OptionsColumn.ReadOnly = true;
            this.colAlertWithinXDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAlertWithinXDistance.Visible = true;
            this.colAlertWithinXDistance.VisibleIndex = 8;
            this.colAlertWithinXDistance.Width = 118;
            // 
            // colAlertWithinXTime
            // 
            this.colAlertWithinXTime.FieldName = "AlertWithinXTime";
            this.colAlertWithinXTime.Name = "colAlertWithinXTime";
            this.colAlertWithinXTime.OptionsColumn.AllowEdit = false;
            this.colAlertWithinXTime.OptionsColumn.AllowFocus = false;
            this.colAlertWithinXTime.OptionsColumn.ReadOnly = true;
            this.colAlertWithinXTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAlertWithinXTime.Visible = true;
            this.colAlertWithinXTime.VisibleIndex = 9;
            this.colAlertWithinXTime.Width = 99;
            // 
            // colWorkType1
            // 
            this.colWorkType1.FieldName = "WorkType";
            this.colWorkType1.Name = "colWorkType1";
            this.colWorkType1.OptionsColumn.AllowEdit = false;
            this.colWorkType1.OptionsColumn.AllowFocus = false;
            this.colWorkType1.OptionsColumn.ReadOnly = true;
            this.colWorkType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkType1.Visible = true;
            this.colWorkType1.VisibleIndex = 13;
            // 
            // colIntervalScheduleNotes
            // 
            this.colIntervalScheduleNotes.FieldName = "IntervalScheduleNotes";
            this.colIntervalScheduleNotes.Name = "colIntervalScheduleNotes";
            this.colIntervalScheduleNotes.OptionsColumn.AllowEdit = false;
            this.colIntervalScheduleNotes.OptionsColumn.AllowFocus = false;
            this.colIntervalScheduleNotes.OptionsColumn.ReadOnly = true;
            this.colIntervalScheduleNotes.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIntervalScheduleNotes.Visible = true;
            this.colIntervalScheduleNotes.VisibleIndex = 14;
            this.colIntervalScheduleNotes.Width = 219;
            // 
            // colIsCurrentInterval
            // 
            this.colIsCurrentInterval.FieldName = "IsCurrentInterval";
            this.colIsCurrentInterval.Name = "colIsCurrentInterval";
            this.colIsCurrentInterval.OptionsColumn.AllowEdit = false;
            this.colIsCurrentInterval.OptionsColumn.AllowFocus = false;
            this.colIsCurrentInterval.OptionsColumn.ReadOnly = true;
            this.colIsCurrentInterval.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIsCurrentInterval.Visible = true;
            this.colIsCurrentInterval.VisibleIndex = 7;
            this.colIsCurrentInterval.Width = 102;
            // 
            // colMode2
            // 
            this.colMode2.FieldName = "Mode";
            this.colMode2.Name = "colMode2";
            this.colMode2.OptionsColumn.AllowEdit = false;
            this.colMode2.OptionsColumn.AllowFocus = false;
            this.colMode2.OptionsColumn.ReadOnly = true;
            this.colMode2.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode2.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID2
            // 
            this.colRecordID2.FieldName = "RecordID";
            this.colRecordID2.Name = "colRecordID2";
            this.colRecordID2.OptionsColumn.AllowEdit = false;
            this.colRecordID2.OptionsColumn.AllowFocus = false;
            this.colRecordID2.OptionsColumn.ReadOnly = true;
            this.colRecordID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID2.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ReadOnly = true;
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ReadOnly = true;
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // workDetailTabPage
            // 
            this.workDetailTabPage.Controls.Add(this.workDetailGridControl);
            this.workDetailTabPage.Name = "workDetailTabPage";
            this.workDetailTabPage.Size = new System.Drawing.Size(1357, 168);
            this.workDetailTabPage.Text = "Work Order Details";
            // 
            // workDetailGridControl
            // 
            this.workDetailGridControl.DataSource = this.spAS11078WorkDetailItemBindingSource;
            this.workDetailGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.workDetailGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.workDetailGridControl.Location = new System.Drawing.Point(0, 0);
            this.workDetailGridControl.MainView = this.workDetailGridView;
            this.workDetailGridControl.MenuManager = this.barManager1;
            this.workDetailGridControl.Name = "workDetailGridControl";
            this.workDetailGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit13,
            this.repositoryItemMemoExEdit14});
            this.workDetailGridControl.Size = new System.Drawing.Size(1357, 168);
            this.workDetailGridControl.TabIndex = 1;
            this.workDetailGridControl.Tag = "Work Order Details";
            this.workDetailGridControl.UseEmbeddedNavigator = true;
            this.workDetailGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.workDetailGridView});
            // 
            // spAS11078WorkDetailItemBindingSource
            // 
            this.spAS11078WorkDetailItemBindingSource.DataMember = "sp_AS_11078_Work_Detail_Item";
            this.spAS11078WorkDetailItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // workDetailGridView
            // 
            this.workDetailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkDetailID,
            this.colEquipmentID2,
            this.colEquipmentReference11,
            this.colWorkType,
            this.colWorkTypeID,
            this.colDateRaised,
            this.colSupplierID,
            this.colSupplierReference,
            this.colStatusID,
            this.colStatus,
            this.colDescription2,
            this.colTransactionID,
            this.colOrderNumber,
            this.colCurrentMileage,
            this.colWorkCompletionDate,
            this.colServiceIntervalID,
            this.colServiceType,
            this.colNotes2,
            this.colMode10,
            this.colRecordID10});
            this.workDetailGridView.GridControl = this.workDetailGridControl;
            this.workDetailGridView.Name = "workDetailGridView";
            this.workDetailGridView.OptionsCustomization.AllowFilter = false;
            this.workDetailGridView.OptionsCustomization.AllowGroup = false;
            this.workDetailGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.workDetailGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.workDetailGridView.OptionsLayout.StoreAppearance = true;
            this.workDetailGridView.OptionsSelection.MultiSelect = true;
            this.workDetailGridView.OptionsView.ColumnAutoWidth = false;
            this.workDetailGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.workDetailGridView.OptionsView.ShowGroupPanel = false;
            this.workDetailGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.workDetailGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.workDetailGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.workDetailGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.workDetailGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.workDetailGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.workDetailGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colWorkDetailID
            // 
            this.colWorkDetailID.FieldName = "WorkDetailID";
            this.colWorkDetailID.Name = "colWorkDetailID";
            this.colWorkDetailID.OptionsColumn.AllowEdit = false;
            this.colWorkDetailID.OptionsColumn.AllowFocus = false;
            this.colWorkDetailID.OptionsColumn.ReadOnly = true;
            this.colWorkDetailID.OptionsColumn.ShowInCustomizationForm = false;
            this.colWorkDetailID.OptionsColumn.ShowInExpressionEditor = false;
            this.colWorkDetailID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkDetailID.Width = 81;
            // 
            // colEquipmentID2
            // 
            this.colEquipmentID2.FieldName = "EquipmentID";
            this.colEquipmentID2.Name = "colEquipmentID2";
            this.colEquipmentID2.OptionsColumn.AllowEdit = false;
            this.colEquipmentID2.OptionsColumn.AllowFocus = false;
            this.colEquipmentID2.OptionsColumn.ReadOnly = true;
            this.colEquipmentID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID2.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID2.Width = 76;
            // 
            // colEquipmentReference11
            // 
            this.colEquipmentReference11.FieldName = "EquipmentReference";
            this.colEquipmentReference11.Name = "colEquipmentReference11";
            this.colEquipmentReference11.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference11.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference11.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference11.Visible = true;
            this.colEquipmentReference11.VisibleIndex = 0;
            this.colEquipmentReference11.Width = 115;
            // 
            // colWorkType
            // 
            this.colWorkType.FieldName = "WorkType";
            this.colWorkType.Name = "colWorkType";
            this.colWorkType.OptionsColumn.AllowEdit = false;
            this.colWorkType.OptionsColumn.AllowFocus = false;
            this.colWorkType.OptionsColumn.ReadOnly = true;
            this.colWorkType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkType.Visible = true;
            this.colWorkType.VisibleIndex = 1;
            this.colWorkType.Width = 161;
            // 
            // colWorkTypeID
            // 
            this.colWorkTypeID.FieldName = "WorkTypeID";
            this.colWorkTypeID.Name = "colWorkTypeID";
            this.colWorkTypeID.OptionsColumn.AllowEdit = false;
            this.colWorkTypeID.OptionsColumn.AllowFocus = false;
            this.colWorkTypeID.OptionsColumn.ReadOnly = true;
            this.colWorkTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colWorkTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colWorkTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkTypeID.Width = 78;
            // 
            // colDateRaised
            // 
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 2;
            // 
            // colSupplierID
            // 
            this.colSupplierID.FieldName = "SupplierID";
            this.colSupplierID.Name = "colSupplierID";
            this.colSupplierID.OptionsColumn.AllowEdit = false;
            this.colSupplierID.OptionsColumn.AllowFocus = false;
            this.colSupplierID.OptionsColumn.ReadOnly = true;
            this.colSupplierID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSupplierID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSupplierID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSupplierReference
            // 
            this.colSupplierReference.FieldName = "SupplierReference";
            this.colSupplierReference.Name = "colSupplierReference";
            this.colSupplierReference.OptionsColumn.AllowEdit = false;
            this.colSupplierReference.OptionsColumn.AllowFocus = false;
            this.colSupplierReference.OptionsColumn.ReadOnly = true;
            this.colSupplierReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference.Visible = true;
            this.colSupplierReference.VisibleIndex = 3;
            this.colSupplierReference.Width = 103;
            // 
            // colStatusID
            // 
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 4;
            // 
            // colDescription2
            // 
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 5;
            this.colDescription2.Width = 304;
            // 
            // colTransactionID
            // 
            this.colTransactionID.FieldName = "TransactionID";
            this.colTransactionID.Name = "colTransactionID";
            this.colTransactionID.OptionsColumn.AllowEdit = false;
            this.colTransactionID.OptionsColumn.AllowFocus = false;
            this.colTransactionID.OptionsColumn.ReadOnly = true;
            this.colTransactionID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionID.Width = 82;
            // 
            // colOrderNumber
            // 
            this.colOrderNumber.FieldName = "OrderNumber";
            this.colOrderNumber.Name = "colOrderNumber";
            this.colOrderNumber.OptionsColumn.AllowEdit = false;
            this.colOrderNumber.OptionsColumn.AllowFocus = false;
            this.colOrderNumber.OptionsColumn.ReadOnly = true;
            this.colOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOrderNumber.Visible = true;
            this.colOrderNumber.VisibleIndex = 6;
            this.colOrderNumber.Width = 109;
            // 
            // colCurrentMileage
            // 
            this.colCurrentMileage.FieldName = "CurrentMileage";
            this.colCurrentMileage.Name = "colCurrentMileage";
            this.colCurrentMileage.OptionsColumn.AllowEdit = false;
            this.colCurrentMileage.OptionsColumn.AllowFocus = false;
            this.colCurrentMileage.OptionsColumn.ReadOnly = true;
            this.colCurrentMileage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentMileage.Visible = true;
            this.colCurrentMileage.VisibleIndex = 7;
            this.colCurrentMileage.Width = 116;
            // 
            // colWorkCompletionDate
            // 
            this.colWorkCompletionDate.FieldName = "WorkCompletionDate";
            this.colWorkCompletionDate.Name = "colWorkCompletionDate";
            this.colWorkCompletionDate.OptionsColumn.AllowEdit = false;
            this.colWorkCompletionDate.OptionsColumn.AllowFocus = false;
            this.colWorkCompletionDate.OptionsColumn.ReadOnly = true;
            this.colWorkCompletionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkCompletionDate.Visible = true;
            this.colWorkCompletionDate.VisibleIndex = 8;
            this.colWorkCompletionDate.Width = 119;
            // 
            // colServiceIntervalID
            // 
            this.colServiceIntervalID.FieldName = "ServiceIntervalID";
            this.colServiceIntervalID.Name = "colServiceIntervalID";
            this.colServiceIntervalID.OptionsColumn.AllowEdit = false;
            this.colServiceIntervalID.OptionsColumn.AllowFocus = false;
            this.colServiceIntervalID.OptionsColumn.ReadOnly = true;
            this.colServiceIntervalID.OptionsColumn.ShowInCustomizationForm = false;
            this.colServiceIntervalID.OptionsColumn.ShowInExpressionEditor = false;
            this.colServiceIntervalID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceIntervalID.Width = 102;
            // 
            // colServiceType
            // 
            this.colServiceType.FieldName = "ServiceType";
            this.colServiceType.Name = "colServiceType";
            this.colServiceType.OptionsColumn.AllowEdit = false;
            this.colServiceType.OptionsColumn.AllowFocus = false;
            this.colServiceType.OptionsColumn.ReadOnly = true;
            this.colServiceType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceType.Visible = true;
            this.colServiceType.VisibleIndex = 9;
            // 
            // colNotes2
            // 
            this.colNotes2.FieldName = "Notes";
            this.colNotes2.Name = "colNotes2";
            this.colNotes2.OptionsColumn.AllowEdit = false;
            this.colNotes2.OptionsColumn.AllowFocus = false;
            this.colNotes2.OptionsColumn.ReadOnly = true;
            this.colNotes2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes2.Visible = true;
            this.colNotes2.VisibleIndex = 10;
            this.colNotes2.Width = 570;
            // 
            // colMode10
            // 
            this.colMode10.FieldName = "Mode";
            this.colMode10.Name = "colMode10";
            this.colMode10.OptionsColumn.AllowEdit = false;
            this.colMode10.OptionsColumn.AllowFocus = false;
            this.colMode10.OptionsColumn.ReadOnly = true;
            this.colMode10.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode10.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID10
            // 
            this.colRecordID10.FieldName = "RecordID";
            this.colRecordID10.Name = "colRecordID10";
            this.colRecordID10.OptionsColumn.AllowEdit = false;
            this.colRecordID10.OptionsColumn.AllowFocus = false;
            this.colRecordID10.OptionsColumn.ReadOnly = true;
            this.colRecordID10.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID10.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit13
            // 
            this.repositoryItemMemoExEdit13.AutoHeight = false;
            this.repositoryItemMemoExEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit13.Name = "repositoryItemMemoExEdit13";
            this.repositoryItemMemoExEdit13.ReadOnly = true;
            this.repositoryItemMemoExEdit13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit13.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit14
            // 
            this.repositoryItemMemoExEdit14.AutoHeight = false;
            this.repositoryItemMemoExEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit14.Name = "repositoryItemMemoExEdit14";
            this.repositoryItemMemoExEdit14.ReadOnly = true;
            this.repositoryItemMemoExEdit14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit14.ShowIcon = false;
            // 
            // incidentTabPage
            // 
            this.incidentTabPage.Controls.Add(this.incidentGridControl);
            this.incidentTabPage.Name = "incidentTabPage";
            this.incidentTabPage.Size = new System.Drawing.Size(1357, 168);
            this.incidentTabPage.Text = "Incident Details";
            // 
            // incidentGridControl
            // 
            this.incidentGridControl.DataSource = this.spAS11081IncidentItemBindingSource;
            this.incidentGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.incidentGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.incidentGridControl.Location = new System.Drawing.Point(0, 0);
            this.incidentGridControl.MainView = this.incidentGridView;
            this.incidentGridControl.MenuManager = this.barManager1;
            this.incidentGridControl.Name = "incidentGridControl";
            this.incidentGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.incidentNotesMemoEdit,
            this.moneyTextEdit5});
            this.incidentGridControl.Size = new System.Drawing.Size(1357, 168);
            this.incidentGridControl.TabIndex = 2;
            this.incidentGridControl.Tag = "Incident Details";
            this.incidentGridControl.UseEmbeddedNavigator = true;
            this.incidentGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.incidentGridView});
            // 
            // spAS11081IncidentItemBindingSource
            // 
            this.spAS11081IncidentItemBindingSource.DataMember = "sp_AS_11081_Incident_Item";
            this.spAS11081IncidentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // incidentGridView
            // 
            this.incidentGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIncidentID,
            this.colEquipmentID7,
            this.colEquipmentReference5,
            this.colIncidentStatusID,
            this.colIncidentStatus,
            this.colIncidentTypeID,
            this.colIncidentType,
            this.colIncidentReference,
            this.colDateHappened,
            this.colLocation,
            this.colRepairDueID,
            this.colRepairDue,
            this.colSeverityID,
            this.colSeverity,
            this.colWitness,
            this.colKeeperAtFaultID,
            this.colKeeperAtFault,
            this.colLegalActionID,
            this.colLegalAction,
            this.colFollowUpDate,
            this.colCost,
            this.colNotes4,
            this.colMode3,
            this.colRecordID3});
            this.incidentGridView.GridControl = this.incidentGridControl;
            this.incidentGridView.Name = "incidentGridView";
            this.incidentGridView.OptionsCustomization.AllowFilter = false;
            this.incidentGridView.OptionsCustomization.AllowGroup = false;
            this.incidentGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.incidentGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.incidentGridView.OptionsLayout.StoreAppearance = true;
            this.incidentGridView.OptionsSelection.MultiSelect = true;
            this.incidentGridView.OptionsView.ColumnAutoWidth = false;
            this.incidentGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.incidentGridView.OptionsView.ShowGroupPanel = false;
            this.incidentGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.incidentGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.incidentGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.incidentGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.incidentGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.incidentGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.incidentGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colIncidentID
            // 
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            this.colIncidentID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID7
            // 
            this.colEquipmentID7.FieldName = "EquipmentID";
            this.colEquipmentID7.Name = "colEquipmentID7";
            this.colEquipmentID7.OptionsColumn.AllowEdit = false;
            this.colEquipmentID7.OptionsColumn.AllowFocus = false;
            this.colEquipmentID7.OptionsColumn.ReadOnly = true;
            this.colEquipmentID7.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID7.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID7.Width = 76;
            // 
            // colEquipmentReference5
            // 
            this.colEquipmentReference5.FieldName = "EquipmentReference";
            this.colEquipmentReference5.Name = "colEquipmentReference5";
            this.colEquipmentReference5.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference5.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference5.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference5.Visible = true;
            this.colEquipmentReference5.VisibleIndex = 0;
            this.colEquipmentReference5.Width = 115;
            // 
            // colIncidentStatusID
            // 
            this.colIncidentStatusID.FieldName = "IncidentStatusID";
            this.colIncidentStatusID.Name = "colIncidentStatusID";
            this.colIncidentStatusID.OptionsColumn.AllowEdit = false;
            this.colIncidentStatusID.OptionsColumn.AllowFocus = false;
            this.colIncidentStatusID.OptionsColumn.ReadOnly = true;
            this.colIncidentStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentStatusID.Width = 99;
            // 
            // colIncidentStatus
            // 
            this.colIncidentStatus.FieldName = "IncidentStatus";
            this.colIncidentStatus.Name = "colIncidentStatus";
            this.colIncidentStatus.OptionsColumn.AllowEdit = false;
            this.colIncidentStatus.OptionsColumn.AllowFocus = false;
            this.colIncidentStatus.OptionsColumn.ReadOnly = true;
            this.colIncidentStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentStatus.Visible = true;
            this.colIncidentStatus.VisibleIndex = 2;
            this.colIncidentStatus.Width = 96;
            // 
            // colIncidentTypeID
            // 
            this.colIncidentTypeID.FieldName = "IncidentTypeID";
            this.colIncidentTypeID.Name = "colIncidentTypeID";
            this.colIncidentTypeID.OptionsColumn.AllowEdit = false;
            this.colIncidentTypeID.OptionsColumn.AllowFocus = false;
            this.colIncidentTypeID.OptionsColumn.ReadOnly = true;
            this.colIncidentTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentTypeID.Width = 92;
            // 
            // colIncidentType
            // 
            this.colIncidentType.FieldName = "IncidentType";
            this.colIncidentType.Name = "colIncidentType";
            this.colIncidentType.OptionsColumn.AllowEdit = false;
            this.colIncidentType.OptionsColumn.AllowFocus = false;
            this.colIncidentType.OptionsColumn.ReadOnly = true;
            this.colIncidentType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentType.Visible = true;
            this.colIncidentType.VisibleIndex = 3;
            this.colIncidentType.Width = 97;
            // 
            // colIncidentReference
            // 
            this.colIncidentReference.FieldName = "IncidentReference";
            this.colIncidentReference.Name = "colIncidentReference";
            this.colIncidentReference.OptionsColumn.AllowEdit = false;
            this.colIncidentReference.OptionsColumn.AllowFocus = false;
            this.colIncidentReference.OptionsColumn.ReadOnly = true;
            this.colIncidentReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentReference.Visible = true;
            this.colIncidentReference.VisibleIndex = 1;
            this.colIncidentReference.Width = 104;
            // 
            // colDateHappened
            // 
            this.colDateHappened.FieldName = "DateHappened";
            this.colDateHappened.Name = "colDateHappened";
            this.colDateHappened.OptionsColumn.AllowEdit = false;
            this.colDateHappened.OptionsColumn.AllowFocus = false;
            this.colDateHappened.OptionsColumn.ReadOnly = true;
            this.colDateHappened.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateHappened.Visible = true;
            this.colDateHappened.VisibleIndex = 4;
            this.colDateHappened.Width = 87;
            // 
            // colLocation
            // 
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 5;
            this.colLocation.Width = 233;
            // 
            // colRepairDueID
            // 
            this.colRepairDueID.FieldName = "RepairDueID";
            this.colRepairDueID.Name = "colRepairDueID";
            this.colRepairDueID.OptionsColumn.AllowEdit = false;
            this.colRepairDueID.OptionsColumn.AllowFocus = false;
            this.colRepairDueID.OptionsColumn.ReadOnly = true;
            this.colRepairDueID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRepairDueID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRepairDueID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRepairDueID.Width = 79;
            // 
            // colRepairDue
            // 
            this.colRepairDue.FieldName = "RepairDue";
            this.colRepairDue.Name = "colRepairDue";
            this.colRepairDue.OptionsColumn.AllowEdit = false;
            this.colRepairDue.OptionsColumn.AllowFocus = false;
            this.colRepairDue.OptionsColumn.ReadOnly = true;
            this.colRepairDue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRepairDue.Visible = true;
            this.colRepairDue.VisibleIndex = 6;
            this.colRepairDue.Width = 91;
            // 
            // colSeverityID
            // 
            this.colSeverityID.FieldName = "SeverityID";
            this.colSeverityID.Name = "colSeverityID";
            this.colSeverityID.OptionsColumn.AllowEdit = false;
            this.colSeverityID.OptionsColumn.AllowFocus = false;
            this.colSeverityID.OptionsColumn.ReadOnly = true;
            this.colSeverityID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSeverityID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSeverityID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSeverity
            // 
            this.colSeverity.FieldName = "Severity";
            this.colSeverity.Name = "colSeverity";
            this.colSeverity.OptionsColumn.AllowEdit = false;
            this.colSeverity.OptionsColumn.AllowFocus = false;
            this.colSeverity.OptionsColumn.ReadOnly = true;
            this.colSeverity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSeverity.Visible = true;
            this.colSeverity.VisibleIndex = 7;
            this.colSeverity.Width = 92;
            // 
            // colWitness
            // 
            this.colWitness.FieldName = "Witness";
            this.colWitness.Name = "colWitness";
            this.colWitness.OptionsColumn.AllowEdit = false;
            this.colWitness.OptionsColumn.AllowFocus = false;
            this.colWitness.OptionsColumn.ReadOnly = true;
            this.colWitness.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWitness.Visible = true;
            this.colWitness.VisibleIndex = 8;
            this.colWitness.Width = 248;
            // 
            // colKeeperAtFaultID
            // 
            this.colKeeperAtFaultID.FieldName = "KeeperAtFaultID";
            this.colKeeperAtFaultID.Name = "colKeeperAtFaultID";
            this.colKeeperAtFaultID.OptionsColumn.AllowEdit = false;
            this.colKeeperAtFaultID.OptionsColumn.AllowFocus = false;
            this.colKeeperAtFaultID.OptionsColumn.ReadOnly = true;
            this.colKeeperAtFaultID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAtFaultID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAtFaultID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAtFaultID.Width = 101;
            // 
            // colKeeperAtFault
            // 
            this.colKeeperAtFault.FieldName = "KeeperAtFault";
            this.colKeeperAtFault.Name = "colKeeperAtFault";
            this.colKeeperAtFault.OptionsColumn.AllowEdit = false;
            this.colKeeperAtFault.OptionsColumn.AllowFocus = false;
            this.colKeeperAtFault.OptionsColumn.ReadOnly = true;
            this.colKeeperAtFault.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAtFault.Visible = true;
            this.colKeeperAtFault.VisibleIndex = 9;
            this.colKeeperAtFault.Width = 93;
            // 
            // colLegalActionID
            // 
            this.colLegalActionID.FieldName = "LegalActionID";
            this.colLegalActionID.Name = "colLegalActionID";
            this.colLegalActionID.OptionsColumn.AllowEdit = false;
            this.colLegalActionID.OptionsColumn.AllowFocus = false;
            this.colLegalActionID.OptionsColumn.ReadOnly = true;
            this.colLegalActionID.OptionsColumn.ShowInCustomizationForm = false;
            this.colLegalActionID.OptionsColumn.ShowInExpressionEditor = false;
            this.colLegalActionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLegalActionID.Width = 84;
            // 
            // colLegalAction
            // 
            this.colLegalAction.FieldName = "LegalAction";
            this.colLegalAction.Name = "colLegalAction";
            this.colLegalAction.OptionsColumn.AllowEdit = false;
            this.colLegalAction.OptionsColumn.AllowFocus = false;
            this.colLegalAction.OptionsColumn.ReadOnly = true;
            this.colLegalAction.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLegalAction.Visible = true;
            this.colLegalAction.VisibleIndex = 10;
            this.colLegalAction.Width = 119;
            // 
            // colFollowUpDate
            // 
            this.colFollowUpDate.FieldName = "FollowUpDate";
            this.colFollowUpDate.Name = "colFollowUpDate";
            this.colFollowUpDate.OptionsColumn.AllowEdit = false;
            this.colFollowUpDate.OptionsColumn.AllowFocus = false;
            this.colFollowUpDate.OptionsColumn.ReadOnly = true;
            this.colFollowUpDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFollowUpDate.Visible = true;
            this.colFollowUpDate.VisibleIndex = 11;
            this.colFollowUpDate.Width = 99;
            // 
            // colCost
            // 
            this.colCost.ColumnEdit = this.moneyTextEdit5;
            this.colCost.FieldName = "Cost";
            this.colCost.Name = "colCost";
            this.colCost.OptionsColumn.AllowEdit = false;
            this.colCost.OptionsColumn.AllowFocus = false;
            this.colCost.OptionsColumn.ReadOnly = true;
            this.colCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCost.Visible = true;
            this.colCost.VisibleIndex = 12;
            this.colCost.Width = 84;
            // 
            // moneyTextEdit5
            // 
            this.moneyTextEdit5.AutoHeight = false;
            this.moneyTextEdit5.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit5.Name = "moneyTextEdit5";
            // 
            // colNotes4
            // 
            this.colNotes4.ColumnEdit = this.incidentNotesMemoEdit;
            this.colNotes4.FieldName = "Notes";
            this.colNotes4.Name = "colNotes4";
            this.colNotes4.OptionsColumn.AllowEdit = false;
            this.colNotes4.OptionsColumn.AllowFocus = false;
            this.colNotes4.OptionsColumn.ReadOnly = true;
            this.colNotes4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes4.Visible = true;
            this.colNotes4.VisibleIndex = 13;
            this.colNotes4.Width = 472;
            // 
            // incidentNotesMemoEdit
            // 
            this.incidentNotesMemoEdit.Name = "incidentNotesMemoEdit";
            // 
            // colMode3
            // 
            this.colMode3.FieldName = "Mode";
            this.colMode3.Name = "colMode3";
            this.colMode3.OptionsColumn.AllowEdit = false;
            this.colMode3.OptionsColumn.AllowFocus = false;
            this.colMode3.OptionsColumn.ReadOnly = true;
            this.colMode3.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode3.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID3
            // 
            this.colRecordID3.FieldName = "RecordID";
            this.colRecordID3.Name = "colRecordID3";
            this.colRecordID3.OptionsColumn.AllowEdit = false;
            this.colRecordID3.OptionsColumn.AllowFocus = false;
            this.colRecordID3.OptionsColumn.ReadOnly = true;
            this.colRecordID3.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID3.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // purposeTabPage
            // 
            this.purposeTabPage.Controls.Add(this.purposeGridControl);
            this.purposeTabPage.Name = "purposeTabPage";
            this.purposeTabPage.Size = new System.Drawing.Size(1357, 168);
            this.purposeTabPage.Text = "Equipment Purpose";
            // 
            // purposeGridControl
            // 
            this.purposeGridControl.DataSource = this.spAS11084PurposeItemBindingSource;
            this.purposeGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.purposeGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.purposeGridControl.Location = new System.Drawing.Point(0, 0);
            this.purposeGridControl.MainView = this.purposeGridView;
            this.purposeGridControl.MenuManager = this.barManager1;
            this.purposeGridControl.Name = "purposeGridControl";
            this.purposeGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit16,
            this.repositoryItemMemoExEdit17});
            this.purposeGridControl.Size = new System.Drawing.Size(1357, 168);
            this.purposeGridControl.TabIndex = 3;
            this.purposeGridControl.Tag = "Asset Purpose";
            this.purposeGridControl.UseEmbeddedNavigator = true;
            this.purposeGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.purposeGridView});
            // 
            // spAS11084PurposeItemBindingSource
            // 
            this.spAS11084PurposeItemBindingSource.DataMember = "sp_AS_11084_Purpose_Item";
            this.spAS11084PurposeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // purposeGridView
            // 
            this.purposeGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentPurposeID,
            this.colEquipmentID15,
            this.colEquipmentReference13,
            this.colPurpose,
            this.colPurposeID,
            this.colMode14,
            this.colRecordID14});
            this.purposeGridView.GridControl = this.purposeGridControl;
            this.purposeGridView.Name = "purposeGridView";
            this.purposeGridView.OptionsCustomization.AllowFilter = false;
            this.purposeGridView.OptionsCustomization.AllowGroup = false;
            this.purposeGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.purposeGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.purposeGridView.OptionsLayout.StoreAppearance = true;
            this.purposeGridView.OptionsSelection.MultiSelect = true;
            this.purposeGridView.OptionsView.ColumnAutoWidth = false;
            this.purposeGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.purposeGridView.OptionsView.ShowGroupPanel = false;
            this.purposeGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.purposeGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.purposeGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.purposeGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.purposeGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.purposeGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.purposeGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentPurposeID
            // 
            this.colEquipmentPurposeID.FieldName = "EquipmentPurposeID";
            this.colEquipmentPurposeID.Name = "colEquipmentPurposeID";
            this.colEquipmentPurposeID.OptionsColumn.AllowEdit = false;
            this.colEquipmentPurposeID.OptionsColumn.AllowFocus = false;
            this.colEquipmentPurposeID.OptionsColumn.ReadOnly = true;
            this.colEquipmentPurposeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentPurposeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentPurposeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentPurposeID.Width = 118;
            // 
            // colEquipmentID15
            // 
            this.colEquipmentID15.FieldName = "EquipmentID";
            this.colEquipmentID15.Name = "colEquipmentID15";
            this.colEquipmentID15.OptionsColumn.AllowEdit = false;
            this.colEquipmentID15.OptionsColumn.AllowFocus = false;
            this.colEquipmentID15.OptionsColumn.ReadOnly = true;
            this.colEquipmentID15.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID15.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID15.Width = 76;
            // 
            // colEquipmentReference13
            // 
            this.colEquipmentReference13.FieldName = "EquipmentReference";
            this.colEquipmentReference13.Name = "colEquipmentReference13";
            this.colEquipmentReference13.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference13.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference13.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference13.Visible = true;
            this.colEquipmentReference13.VisibleIndex = 0;
            this.colEquipmentReference13.Width = 115;
            // 
            // colPurpose
            // 
            this.colPurpose.FieldName = "Purpose";
            this.colPurpose.Name = "colPurpose";
            this.colPurpose.OptionsColumn.AllowEdit = false;
            this.colPurpose.OptionsColumn.AllowFocus = false;
            this.colPurpose.OptionsColumn.ReadOnly = true;
            this.colPurpose.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPurpose.Visible = true;
            this.colPurpose.VisibleIndex = 1;
            this.colPurpose.Width = 440;
            // 
            // colPurposeID
            // 
            this.colPurposeID.FieldName = "PurposeID";
            this.colPurposeID.Name = "colPurposeID";
            this.colPurposeID.OptionsColumn.AllowEdit = false;
            this.colPurposeID.OptionsColumn.AllowFocus = false;
            this.colPurposeID.OptionsColumn.ReadOnly = true;
            this.colPurposeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colPurposeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colPurposeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode14
            // 
            this.colMode14.FieldName = "Mode";
            this.colMode14.Name = "colMode14";
            this.colMode14.OptionsColumn.AllowEdit = false;
            this.colMode14.OptionsColumn.AllowFocus = false;
            this.colMode14.OptionsColumn.ReadOnly = true;
            this.colMode14.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode14.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID14
            // 
            this.colRecordID14.FieldName = "RecordID";
            this.colRecordID14.Name = "colRecordID14";
            this.colRecordID14.OptionsColumn.AllowEdit = false;
            this.colRecordID14.OptionsColumn.AllowFocus = false;
            this.colRecordID14.OptionsColumn.ReadOnly = true;
            this.colRecordID14.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID14.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit16
            // 
            this.repositoryItemMemoExEdit16.AutoHeight = false;
            this.repositoryItemMemoExEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit16.Name = "repositoryItemMemoExEdit16";
            this.repositoryItemMemoExEdit16.ReadOnly = true;
            this.repositoryItemMemoExEdit16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit16.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit17
            // 
            this.repositoryItemMemoExEdit17.AutoHeight = false;
            this.repositoryItemMemoExEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit17.Name = "repositoryItemMemoExEdit17";
            this.repositoryItemMemoExEdit17.ReadOnly = true;
            this.repositoryItemMemoExEdit17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit17.ShowIcon = false;
            // 
            // fuelCardTabPage
            // 
            this.fuelCardTabPage.Controls.Add(this.fuelCardGridControl);
            this.fuelCardTabPage.Name = "fuelCardTabPage";
            this.fuelCardTabPage.Size = new System.Drawing.Size(1357, 168);
            this.fuelCardTabPage.Text = "Fuel Card";
            // 
            // fuelCardGridControl
            // 
            this.fuelCardGridControl.DataSource = this.spAS11089FuelCardItemBindingSource;
            this.fuelCardGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.fuelCardGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.fuelCardGridControl.Location = new System.Drawing.Point(0, 0);
            this.fuelCardGridControl.MainView = this.fuelCardGridView;
            this.fuelCardGridControl.MenuManager = this.barManager1;
            this.fuelCardGridControl.Name = "fuelCardGridControl";
            this.fuelCardGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit18,
            this.repositoryItemMemoExEdit19});
            this.fuelCardGridControl.Size = new System.Drawing.Size(1357, 168);
            this.fuelCardGridControl.TabIndex = 3;
            this.fuelCardGridControl.Tag = "Fuel Card Details";
            this.fuelCardGridControl.UseEmbeddedNavigator = true;
            this.fuelCardGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.fuelCardGridView});
            // 
            // spAS11089FuelCardItemBindingSource
            // 
            this.spAS11089FuelCardItemBindingSource.DataMember = "sp_AS_11089_Fuel_Card_Item";
            this.spAS11089FuelCardItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // fuelCardGridView
            // 
            this.fuelCardGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFuelCardID,
            this.colCardNumber,
            this.colRegistrationMark,
            this.colEquipmentReference14,
            this.colEquipmentID16,
            this.colKeeper1,
            this.colKeeperAllocationID1,
            this.colExpiryDate,
            this.colSupplierReference2,
            this.colSupplierID2,
            this.colCardStatus,
            this.colCardStatusID,
            this.colNotes5,
            this.colMode15,
            this.colRecordID15});
            this.fuelCardGridView.GridControl = this.fuelCardGridControl;
            this.fuelCardGridView.Name = "fuelCardGridView";
            this.fuelCardGridView.OptionsCustomization.AllowFilter = false;
            this.fuelCardGridView.OptionsCustomization.AllowGroup = false;
            this.fuelCardGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.fuelCardGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.fuelCardGridView.OptionsLayout.StoreAppearance = true;
            this.fuelCardGridView.OptionsSelection.MultiSelect = true;
            this.fuelCardGridView.OptionsView.ColumnAutoWidth = false;
            this.fuelCardGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.fuelCardGridView.OptionsView.ShowGroupPanel = false;
            this.fuelCardGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.fuelCardGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.fuelCardGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.fuelCardGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.fuelCardGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.fuelCardGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.fuelCardGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colFuelCardID
            // 
            this.colFuelCardID.FieldName = "FuelCardID";
            this.colFuelCardID.Name = "colFuelCardID";
            this.colFuelCardID.OptionsColumn.AllowEdit = false;
            this.colFuelCardID.OptionsColumn.AllowFocus = false;
            this.colFuelCardID.OptionsColumn.ReadOnly = true;
            this.colFuelCardID.OptionsColumn.ShowInCustomizationForm = false;
            this.colFuelCardID.OptionsColumn.ShowInExpressionEditor = false;
            this.colFuelCardID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCardNumber
            // 
            this.colCardNumber.FieldName = "CardNumber";
            this.colCardNumber.Name = "colCardNumber";
            this.colCardNumber.OptionsColumn.AllowEdit = false;
            this.colCardNumber.OptionsColumn.AllowFocus = false;
            this.colCardNumber.OptionsColumn.ReadOnly = true;
            this.colCardNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCardNumber.Visible = true;
            this.colCardNumber.VisibleIndex = 1;
            this.colCardNumber.Width = 191;
            // 
            // colRegistrationMark
            // 
            this.colRegistrationMark.FieldName = "Registration Mark";
            this.colRegistrationMark.Name = "colRegistrationMark";
            this.colRegistrationMark.OptionsColumn.AllowEdit = false;
            this.colRegistrationMark.OptionsColumn.AllowFocus = false;
            this.colRegistrationMark.OptionsColumn.ReadOnly = true;
            this.colRegistrationMark.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistrationMark.Visible = true;
            this.colRegistrationMark.VisibleIndex = 2;
            this.colRegistrationMark.Width = 183;
            // 
            // colEquipmentReference14
            // 
            this.colEquipmentReference14.FieldName = "EquipmentReference";
            this.colEquipmentReference14.Name = "colEquipmentReference14";
            this.colEquipmentReference14.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference14.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference14.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference14.Visible = true;
            this.colEquipmentReference14.VisibleIndex = 0;
            this.colEquipmentReference14.Width = 115;
            // 
            // colEquipmentID16
            // 
            this.colEquipmentID16.FieldName = "EquipmentID";
            this.colEquipmentID16.Name = "colEquipmentID16";
            this.colEquipmentID16.OptionsColumn.AllowEdit = false;
            this.colEquipmentID16.OptionsColumn.AllowFocus = false;
            this.colEquipmentID16.OptionsColumn.ReadOnly = true;
            this.colEquipmentID16.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID16.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID16.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID16.Width = 76;
            // 
            // colKeeper1
            // 
            this.colKeeper1.FieldName = "Keeper";
            this.colKeeper1.Name = "colKeeper1";
            this.colKeeper1.OptionsColumn.AllowEdit = false;
            this.colKeeper1.OptionsColumn.AllowFocus = false;
            this.colKeeper1.OptionsColumn.ReadOnly = true;
            this.colKeeper1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeper1.Visible = true;
            this.colKeeper1.VisibleIndex = 3;
            this.colKeeper1.Width = 254;
            // 
            // colKeeperAllocationID1
            // 
            this.colKeeperAllocationID1.FieldName = "KeeperAllocationID";
            this.colKeeperAllocationID1.Name = "colKeeperAllocationID1";
            this.colKeeperAllocationID1.OptionsColumn.AllowEdit = false;
            this.colKeeperAllocationID1.OptionsColumn.AllowFocus = false;
            this.colKeeperAllocationID1.OptionsColumn.ReadOnly = true;
            this.colKeeperAllocationID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAllocationID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAllocationID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAllocationID1.Width = 109;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.OptionsColumn.AllowEdit = false;
            this.colExpiryDate.OptionsColumn.AllowFocus = false;
            this.colExpiryDate.OptionsColumn.ReadOnly = true;
            this.colExpiryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 4;
            this.colExpiryDate.Width = 119;
            // 
            // colSupplierReference2
            // 
            this.colSupplierReference2.FieldName = "SupplierReference";
            this.colSupplierReference2.Name = "colSupplierReference2";
            this.colSupplierReference2.OptionsColumn.AllowEdit = false;
            this.colSupplierReference2.OptionsColumn.AllowFocus = false;
            this.colSupplierReference2.OptionsColumn.ReadOnly = true;
            this.colSupplierReference2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference2.Visible = true;
            this.colSupplierReference2.VisibleIndex = 5;
            this.colSupplierReference2.Width = 262;
            // 
            // colSupplierID2
            // 
            this.colSupplierID2.FieldName = "SupplierID";
            this.colSupplierID2.Name = "colSupplierID2";
            this.colSupplierID2.OptionsColumn.AllowEdit = false;
            this.colSupplierID2.OptionsColumn.AllowFocus = false;
            this.colSupplierID2.OptionsColumn.ReadOnly = true;
            this.colSupplierID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colSupplierID2.OptionsColumn.ShowInExpressionEditor = false;
            this.colSupplierID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCardStatus
            // 
            this.colCardStatus.FieldName = "CardStatus";
            this.colCardStatus.Name = "colCardStatus";
            this.colCardStatus.OptionsColumn.AllowEdit = false;
            this.colCardStatus.OptionsColumn.AllowFocus = false;
            this.colCardStatus.OptionsColumn.ReadOnly = true;
            this.colCardStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCardStatus.Visible = true;
            this.colCardStatus.VisibleIndex = 6;
            // 
            // colCardStatusID
            // 
            this.colCardStatusID.FieldName = "CardStatusID";
            this.colCardStatusID.Name = "colCardStatusID";
            this.colCardStatusID.OptionsColumn.AllowEdit = false;
            this.colCardStatusID.OptionsColumn.AllowFocus = false;
            this.colCardStatusID.OptionsColumn.ReadOnly = true;
            this.colCardStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colCardStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colCardStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCardStatusID.Width = 83;
            // 
            // colNotes5
            // 
            this.colNotes5.FieldName = "Notes";
            this.colNotes5.Name = "colNotes5";
            this.colNotes5.OptionsColumn.AllowEdit = false;
            this.colNotes5.OptionsColumn.AllowFocus = false;
            this.colNotes5.OptionsColumn.ReadOnly = true;
            this.colNotes5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes5.Visible = true;
            this.colNotes5.VisibleIndex = 7;
            this.colNotes5.Width = 633;
            // 
            // colMode15
            // 
            this.colMode15.FieldName = "Mode";
            this.colMode15.Name = "colMode15";
            this.colMode15.OptionsColumn.AllowEdit = false;
            this.colMode15.OptionsColumn.AllowFocus = false;
            this.colMode15.OptionsColumn.ReadOnly = true;
            this.colMode15.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode15.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID15
            // 
            this.colRecordID15.FieldName = "RecordID";
            this.colRecordID15.Name = "colRecordID15";
            this.colRecordID15.OptionsColumn.AllowEdit = false;
            this.colRecordID15.OptionsColumn.AllowFocus = false;
            this.colRecordID15.OptionsColumn.ReadOnly = true;
            this.colRecordID15.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID15.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit18
            // 
            this.repositoryItemMemoExEdit18.AutoHeight = false;
            this.repositoryItemMemoExEdit18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit18.Name = "repositoryItemMemoExEdit18";
            this.repositoryItemMemoExEdit18.ReadOnly = true;
            this.repositoryItemMemoExEdit18.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit18.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit19
            // 
            this.repositoryItemMemoExEdit19.AutoHeight = false;
            this.repositoryItemMemoExEdit19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit19.Name = "repositoryItemMemoExEdit19";
            this.repositoryItemMemoExEdit19.ReadOnly = true;
            this.repositoryItemMemoExEdit19.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit19.ShowIcon = false;
            // 
            // speedingTabPage
            // 
            this.speedingTabPage.Controls.Add(this.speedingGridControl);
            this.speedingTabPage.Name = "speedingTabPage";
            this.speedingTabPage.Size = new System.Drawing.Size(1357, 168);
            this.speedingTabPage.Text = "Speeding Data";
            // 
            // speedingGridControl
            // 
            this.speedingGridControl.DataSource = this.spAS11105SpeedingItemBindingSource;
            this.speedingGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Add To Incident", "insert"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, false, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, false, "Delete Selected Record(s)", "delete")});
            this.speedingGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.speedingGridControl.Location = new System.Drawing.Point(0, 0);
            this.speedingGridControl.MainView = this.speedingGridView;
            this.speedingGridControl.MenuManager = this.barManager1;
            this.speedingGridControl.Name = "speedingGridControl";
            this.speedingGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceReported});
            this.speedingGridControl.Size = new System.Drawing.Size(1357, 168);
            this.speedingGridControl.TabIndex = 2;
            this.speedingGridControl.Tag = "Speeding Data";
            this.speedingGridControl.UseEmbeddedNavigator = true;
            this.speedingGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.speedingGridView});
            // 
            // spAS11105SpeedingItemBindingSource
            // 
            this.spAS11105SpeedingItemBindingSource.DataMember = "sp_AS_11105_Speeding_Item";
            this.spAS11105SpeedingItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // speedingGridView
            // 
            this.speedingGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSpeedingID,
            this.colEquipmentReference17,
            this.colVID,
            this.colEquipmentID17,
            this.colSpeed,
            this.colSpeedMPH1,
            this.colTravelTime,
            this.colRoadTypeID,
            this.colRoadType,
            this.colPlaceName1,
            this.colDistrict1,
            this.colReported,
            this.colMode17,
            this.colRecordID17});
            this.speedingGridView.GridControl = this.speedingGridControl;
            this.speedingGridView.Name = "speedingGridView";
            this.speedingGridView.OptionsCustomization.AllowFilter = false;
            this.speedingGridView.OptionsCustomization.AllowGroup = false;
            this.speedingGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.speedingGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.speedingGridView.OptionsLayout.StoreAppearance = true;
            this.speedingGridView.OptionsSelection.MultiSelect = true;
            this.speedingGridView.OptionsView.ColumnAutoWidth = false;
            this.speedingGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.speedingGridView.OptionsView.ShowGroupPanel = false;
            this.speedingGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.speedingGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.speedingGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.speedingGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.speedingGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.speedingGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.speedingGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colSpeedingID
            // 
            this.colSpeedingID.FieldName = "SpeedingID";
            this.colSpeedingID.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colSpeedingID.ImageOptions.Image")));
            this.colSpeedingID.Name = "colSpeedingID";
            this.colSpeedingID.OptionsColumn.AllowEdit = false;
            this.colSpeedingID.OptionsColumn.AllowFocus = false;
            this.colSpeedingID.OptionsColumn.ReadOnly = true;
            this.colSpeedingID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSpeedingID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSpeedingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentReference17
            // 
            this.colEquipmentReference17.FieldName = "EquipmentReference";
            this.colEquipmentReference17.Name = "colEquipmentReference17";
            this.colEquipmentReference17.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference17.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference17.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference17.Visible = true;
            this.colEquipmentReference17.VisibleIndex = 0;
            this.colEquipmentReference17.Width = 138;
            // 
            // colVID
            // 
            this.colVID.FieldName = "VID";
            this.colVID.Name = "colVID";
            this.colVID.OptionsColumn.AllowEdit = false;
            this.colVID.OptionsColumn.AllowFocus = false;
            this.colVID.OptionsColumn.ReadOnly = true;
            this.colVID.OptionsColumn.ShowInCustomizationForm = false;
            this.colVID.OptionsColumn.ShowInExpressionEditor = false;
            this.colVID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID17
            // 
            this.colEquipmentID17.FieldName = "EquipmentID";
            this.colEquipmentID17.Name = "colEquipmentID17";
            this.colEquipmentID17.OptionsColumn.AllowEdit = false;
            this.colEquipmentID17.OptionsColumn.AllowFocus = false;
            this.colEquipmentID17.OptionsColumn.ReadOnly = true;
            this.colEquipmentID17.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID17.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID17.Width = 76;
            // 
            // colSpeed
            // 
            this.colSpeed.FieldName = "Speed";
            this.colSpeed.Name = "colSpeed";
            this.colSpeed.OptionsColumn.AllowEdit = false;
            this.colSpeed.OptionsColumn.AllowFocus = false;
            this.colSpeed.OptionsColumn.ReadOnly = true;
            this.colSpeed.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSpeedMPH1
            // 
            this.colSpeedMPH1.FieldName = "SpeedMPH";
            this.colSpeedMPH1.Name = "colSpeedMPH1";
            this.colSpeedMPH1.OptionsColumn.AllowEdit = false;
            this.colSpeedMPH1.OptionsColumn.AllowFocus = false;
            this.colSpeedMPH1.OptionsColumn.ReadOnly = true;
            this.colSpeedMPH1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeedMPH1.Visible = true;
            this.colSpeedMPH1.VisibleIndex = 1;
            this.colSpeedMPH1.Width = 87;
            // 
            // colTravelTime
            // 
            this.colTravelTime.FieldName = "TravelTime";
            this.colTravelTime.Name = "colTravelTime";
            this.colTravelTime.OptionsColumn.AllowEdit = false;
            this.colTravelTime.OptionsColumn.AllowFocus = false;
            this.colTravelTime.OptionsColumn.ReadOnly = true;
            this.colTravelTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTravelTime.Visible = true;
            this.colTravelTime.VisibleIndex = 2;
            // 
            // colRoadTypeID
            // 
            this.colRoadTypeID.FieldName = "RoadTypeID";
            this.colRoadTypeID.Name = "colRoadTypeID";
            this.colRoadTypeID.OptionsColumn.AllowEdit = false;
            this.colRoadTypeID.OptionsColumn.AllowFocus = false;
            this.colRoadTypeID.OptionsColumn.ReadOnly = true;
            this.colRoadTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRoadTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRoadTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadTypeID.Width = 78;
            // 
            // colRoadType
            // 
            this.colRoadType.FieldName = "RoadType";
            this.colRoadType.Name = "colRoadType";
            this.colRoadType.OptionsColumn.AllowEdit = false;
            this.colRoadType.OptionsColumn.AllowFocus = false;
            this.colRoadType.OptionsColumn.ReadOnly = true;
            this.colRoadType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadType.Visible = true;
            this.colRoadType.VisibleIndex = 3;
            this.colRoadType.Width = 121;
            // 
            // colPlaceName1
            // 
            this.colPlaceName1.FieldName = "PlaceName";
            this.colPlaceName1.Name = "colPlaceName1";
            this.colPlaceName1.OptionsColumn.AllowEdit = false;
            this.colPlaceName1.OptionsColumn.AllowFocus = false;
            this.colPlaceName1.OptionsColumn.ReadOnly = true;
            this.colPlaceName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlaceName1.Visible = true;
            this.colPlaceName1.VisibleIndex = 4;
            this.colPlaceName1.Width = 174;
            // 
            // colDistrict1
            // 
            this.colDistrict1.FieldName = "District";
            this.colDistrict1.Name = "colDistrict1";
            this.colDistrict1.OptionsColumn.AllowEdit = false;
            this.colDistrict1.OptionsColumn.AllowFocus = false;
            this.colDistrict1.OptionsColumn.ReadOnly = true;
            this.colDistrict1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistrict1.Visible = true;
            this.colDistrict1.VisibleIndex = 5;
            this.colDistrict1.Width = 118;
            // 
            // colReported
            // 
            this.colReported.ColumnEdit = this.ceReported;
            this.colReported.FieldName = "Reported";
            this.colReported.Name = "colReported";
            this.colReported.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReported.Visible = true;
            this.colReported.VisibleIndex = 6;
            this.colReported.Width = 64;
            // 
            // ceReported
            // 
            this.ceReported.AutoHeight = false;
            this.ceReported.Caption = "Check";
            this.ceReported.Name = "ceReported";
            this.ceReported.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.ceReported_EditValueChanging);
            // 
            // colMode17
            // 
            this.colMode17.FieldName = "Mode";
            this.colMode17.Name = "colMode17";
            this.colMode17.OptionsColumn.AllowEdit = false;
            this.colMode17.OptionsColumn.AllowFocus = false;
            this.colMode17.OptionsColumn.ReadOnly = true;
            this.colMode17.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode17.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID17
            // 
            this.colRecordID17.FieldName = "RecordID";
            this.colRecordID17.Name = "colRecordID17";
            this.colRecordID17.OptionsColumn.AllowEdit = false;
            this.colRecordID17.OptionsColumn.AllowFocus = false;
            this.colRecordID17.OptionsColumn.ReadOnly = true;
            this.colRecordID17.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID17.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // LinkedDocumentsTabPage
            // 
            this.LinkedDocumentsTabPage.Controls.Add(this.gridControlLinkedDocs);
            this.LinkedDocumentsTabPage.Name = "LinkedDocumentsTabPage";
            this.LinkedDocumentsTabPage.Size = new System.Drawing.Size(1357, 168);
            this.LinkedDocumentsTabPage.Text = "Linked Documents";
            // 
            // gridControlLinkedDocs
            // 
            this.gridControlLinkedDocs.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControlLinkedDocs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControlLinkedDocs.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlLinkedDocs_EmbeddedNavigator_ButtonClick);
            this.gridControlLinkedDocs.Location = new System.Drawing.Point(0, 0);
            this.gridControlLinkedDocs.MainView = this.gridViewLinkedDocs;
            this.gridControlLinkedDocs.MenuManager = this.barManager1;
            this.gridControlLinkedDocs.Name = "gridControlLinkedDocs";
            this.gridControlLinkedDocs.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEditLinkedDocs,
            this.repositoryItemHyperLinkEditLinkedDocs,
            this.repositoryItemTextEditLinkedDocDate});
            this.gridControlLinkedDocs.Size = new System.Drawing.Size(1357, 168);
            this.gridControlLinkedDocs.TabIndex = 2;
            this.gridControlLinkedDocs.UseEmbeddedNavigator = true;
            this.gridControlLinkedDocs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLinkedDocs});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewLinkedDocs
            // 
            this.gridViewLinkedDocs.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.gridColumn13,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.gridColumn14,
            this.colAddedByStaffID,
            this.gridColumn15,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks,
            this.colDocumentType});
            this.gridViewLinkedDocs.GridControl = this.gridControlLinkedDocs;
            this.gridViewLinkedDocs.GroupCount = 1;
            this.gridViewLinkedDocs.Name = "gridViewLinkedDocs";
            this.gridViewLinkedDocs.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewLinkedDocs.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewLinkedDocs.OptionsLayout.StoreAppearance = true;
            this.gridViewLinkedDocs.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewLinkedDocs.OptionsSelection.MultiSelect = true;
            this.gridViewLinkedDocs.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewLinkedDocs.OptionsView.ColumnAutoWidth = false;
            this.gridViewLinkedDocs.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewLinkedDocs.OptionsView.ShowGroupPanel = false;
            this.gridViewLinkedDocs.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn14, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewLinkedDocs.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridViewLinkedDocs_PopupMenuShowing);
            this.gridViewLinkedDocs.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewLinkedDocs_SelectionChanged);
            this.gridViewLinkedDocs.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.gridViewLinkedDocs.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridViewLinkedDocs_CustomFilterDialog);
            this.gridViewLinkedDocs.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridViewLinkedDocs_FilterEditorCreated);
            this.gridViewLinkedDocs.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewLinkedDocs_MouseUp);
            this.gridViewLinkedDocs.DoubleClick += new System.EventHandler(this.gridViewLinkedDocs_DoubleClick);
            this.gridViewLinkedDocs.GotFocus += new System.EventHandler(this.gridViewLinkedDocs_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Linked Record ID";
            this.gridColumn13.FieldName = "LinkedToRecordID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocs;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 3;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEditLinkedDocs
            // 
            this.repositoryItemHyperLinkEditLinkedDocs.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocs.Name = "repositoryItemHyperLinkEditLinkedDocs";
            this.repositoryItemHyperLinkEditLinkedDocs.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocs.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocs_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "File Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 4;
            this.colDocumentExtension.Width = 84;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Description";
            this.gridColumn14.FieldName = "Description";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            this.gridColumn14.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Date Added";
            this.gridColumn15.ColumnEdit = this.repositoryItemTextEditLinkedDocDate;
            this.gridColumn15.FieldName = "DateAdded";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 91;
            // 
            // repositoryItemTextEditLinkedDocDate
            // 
            this.repositoryItemTextEditLinkedDocDate.AutoHeight = false;
            this.repositoryItemTextEditLinkedDocDate.Mask.EditMask = "g";
            this.repositoryItemTextEditLinkedDocDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditLinkedDocDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLinkedDocDate.Name = "repositoryItemTextEditLinkedDocDate";
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 5;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEditLinkedDocs;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 6;
            // 
            // repositoryItemMemoExEditLinkedDocs
            // 
            this.repositoryItemMemoExEditLinkedDocs.AutoHeight = false;
            this.repositoryItemMemoExEditLinkedDocs.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEditLinkedDocs.Name = "repositoryItemMemoExEditLinkedDocs";
            this.repositoryItemMemoExEditLinkedDocs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEditLinkedDocs.ShowIcon = false;
            // 
            // colDocumentType
            // 
            this.colDocumentType.FieldName = "DocumentType";
            this.colDocumentType.Name = "colDocumentType";
            this.colDocumentType.OptionsColumn.AllowEdit = false;
            this.colDocumentType.OptionsColumn.AllowFocus = false;
            this.colDocumentType.OptionsColumn.ReadOnly = true;
            this.colDocumentType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDocumentType.Visible = true;
            this.colDocumentType.VisibleIndex = 2;
            this.colDocumentType.Width = 159;
            // 
            // spAS11050DepreciationItemBindingSource
            // 
            this.spAS11050DepreciationItemBindingSource.DataMember = "sp_AS_11050_Depreciation_Item";
            this.spAS11050DepreciationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11005VehicleItemBindingSource
            // 
            this.spAS11005VehicleItemBindingSource.DataMember = "sp_AS_11005_Vehicle_Item";
            this.spAS11005VehicleItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11047EquipmentBillingItemBindingSource
            // 
            this.spAS11047EquipmentBillingItemBindingSource.DataMember = "sp_AS_11047_Equipment_Billing_Item";
            this.spAS11047EquipmentBillingItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControlLinkedDocs;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.Connection = null;
            this.tableAdapterManager1.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // spAS11002EquipmentItemBindingSource
            // 
            this.spAS11002EquipmentItemBindingSource.DataMember = "sp_AS_11002_Equipment_Item";
            this.spAS11002EquipmentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11002_Equipment_ItemTableAdapter
            // 
            this.sp_AS_11002_Equipment_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keeper_Allocation_ItemTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11041_Transaction_ItemTableAdapter
            // 
            this.sp_AS_11041_Transaction_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11047_Equipment_Billing_ItemTableAdapter
            // 
            this.sp_AS_11047_Equipment_Billing_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11050_Depreciation_ItemTableAdapter
            // 
            this.sp_AS_11050_Depreciation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11075_Cover_ItemTableAdapter
            // 
            this.sp_AS_11075_Cover_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11078_Work_Detail_ItemTableAdapter
            // 
            this.sp_AS_11078_Work_Detail_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11084_Purpose_ItemTableAdapter
            // 
            this.sp_AS_11084_Purpose_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11089_Fuel_Card_ItemTableAdapter
            // 
            this.sp_AS_11089_Fuel_Card_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keeper_Allocation_ListTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11081_Incident_ItemTableAdapter
            // 
            this.sp_AS_11081_Incident_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11005_Vehicle_ItemTableAdapter
            // 
            this.sp_AS_11005_Vehicle_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Description = "No Date Filter";
            this.barEditItem1.Edit = this.repositoryItemPopupContainerEdit1;
            this.barEditItem1.EditValue = "No Date Filter";
            this.barEditItem1.EditWidth = 177;
            this.barEditItem1.Id = 35;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Update Tracker";
            this.barButtonItem2.Id = 28;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Auto Match Trackers";
            this.barButtonItem4.Id = 30;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // spAS11023VerilocationItemBindingSource
            // 
            this.spAS11023VerilocationItemBindingSource.DataMember = "sp_AS_11023_Verilocation_Item";
            this.spAS11023VerilocationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11023_Verilocation_ItemTableAdapter
            // 
            this.sp_AS_11023_Verilocation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11023_Tracker_ListTableAdapter
            // 
            this.sp_AS_11023_Tracker_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11096_Road_Tax_ItemTableAdapter
            // 
            this.sp_AS_11096_Road_Tax_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11000PLGeneralBindingSource
            // 
            this.spAS11000PLGeneralBindingSource.DataMember = "sp_AS_11000_PL_General";
            this.spAS11000PLGeneralBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_PL_GeneralTableAdapter
            // 
            this.sp_AS_11000_PL_GeneralTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11105_Speeding_ItemTableAdapter
            // 
            this.sp_AS_11105_Speeding_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_Repair_DueTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_SeverityTableAdapter
            // 
            this.sp_AS_11000_PL_SeverityTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Legal_ActionTableAdapter
            // 
            this.sp_AS_11000_PL_Legal_ActionTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Keeper_At_FaultTableAdapter
            // 
            this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11108_Rental_Details_ItemTableAdapter
            // 
            this.sp_AS_11108_Rental_Details_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11102RentalItemBindingSource
            // 
            this.spAS11102RentalItemBindingSource.DataMember = "sp_AS_11102_Rental_Item";
            this.spAS11102RentalItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11102_Rental_ItemTableAdapter
            // 
            this.sp_AS_11102_Rental_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11102_Rental_ManagerTableAdapter
            // 
            this.sp_AS_11102_Rental_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11126_Notification_ItemTableAdapter
            // 
            this.sp_AS_11126_Notification_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExport, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEmail, true)});
            this.bar1.Text = "Custom 2";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh Rentals";
            this.bbiRefresh.Id = 36;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.LargeImage")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiExport
            // 
            this.bbiExport.Caption = "Export";
            this.bbiExport.Id = 37;
            this.bbiExport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExport.ImageOptions.Image")));
            this.bbiExport.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiExport.ImageOptions.LargeImage")));
            this.bbiExport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportExcel, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportExcelOld, true)});
            this.bbiExport.Name = "bbiExport";
            this.bbiExport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiExportExcel
            // 
            this.bbiExportExcel.Caption = "Export To Excel";
            this.bbiExportExcel.Id = 39;
            this.bbiExportExcel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExportExcel.ImageOptions.Image")));
            this.bbiExportExcel.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiExportExcel.ImageOptions.LargeImage")));
            this.bbiExportExcel.Name = "bbiExportExcel";
            this.bbiExportExcel.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiExportExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportExcel_ItemClick);
            // 
            // bbiExportExcelOld
            // 
            this.bbiExportExcelOld.Caption = "Export To Excel Old";
            this.bbiExportExcelOld.Id = 38;
            this.bbiExportExcelOld.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExportExcelOld.ImageOptions.Image")));
            this.bbiExportExcelOld.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiExportExcelOld.ImageOptions.LargeImage")));
            this.bbiExportExcelOld.Name = "bbiExportExcelOld";
            this.bbiExportExcelOld.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiExportExcelOld.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportExcelOld_ItemClick);
            // 
            // bbiEmail
            // 
            this.bbiEmail.Caption = "Email Keepers";
            this.bbiEmail.Id = 40;
            this.bbiEmail.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Send_To_Employee_32x32;
            this.bbiEmail.Name = "bbiEmail";
            this.bbiEmail.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiEmail.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEmail_ItemClick);
            // 
            // sp_AS_11054_Service_Data_ItemTableAdapter
            // 
            this.sp_AS_11054_Service_Data_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Rental_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1362, 602);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Rental_Manager";
            this.Text = "Rental Manager";
            this.Activated += new System.EventHandler(this.frm_AS_Rental_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Rental_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rentalGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11102RentalManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentalGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unresolvedCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArchiveCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentChildTabControl)).EndInit();
            this.equipmentChildTabControl.ResumeLayout(false);
            this.rentalDetailsTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rentalDetailsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11108RentalDetailsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentalDetailsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSetAsMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes)).EndInit();
            this.NoDetailsTabPage.ResumeLayout(false);
            this.NoDetailsTabPage.PerformLayout();
            this.roadTaxTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11096RoadTaxItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit4)).EndInit();
            this.trackerTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11023TrackerListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            this.keeperTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperNotes)).EndInit();
            this.transactionTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.transactionGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOnExchequer)).EndInit();
            this.notificationTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11126NotificationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).EndInit();
            this.coverTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.coverGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11075CoverItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coverGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit1)).EndInit();
            this.serviceIntervalTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.serviceIntervalGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceDataItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceIntervalGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            this.workDetailTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).EndInit();
            this.incidentTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentNotesMemoEdit)).EndInit();
            this.purposeTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11084PurposeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).EndInit();
            this.fuelCardTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11089FuelCardItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit19)).EndInit();
            this.speedingTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11105SpeedingItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceReported)).EndInit();
            this.LinkedDocumentsTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLinkedDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLinkedDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLinkedDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditLinkedDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11050DepreciationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11005VehicleItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11047EquipmentBillingItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11002EquipmentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11023VerilocationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLGeneralBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11102RentalItemBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl equipmentChildTabControl;
        private DevExpress.XtraTab.XtraTabPage rentalDetailsTabPage;
        private DevExpress.XtraGrid.GridControl rentalDetailsGridControl;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit memNotes;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_AS_CoreTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraTab.XtraTabPage NoDetailsTabPage;
        private System.Windows.Forms.Label label1;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraGrid.Views.Grid.GridView rentalDetailsGridView;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DataSet_AS_Core dataSet_AS_Core;
        private DevExpress.XtraGrid.GridControl rentalGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView rentalGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ArchiveCheckEdit;
        private DevExpress.XtraTab.XtraTabPage keeperTabPage;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager1;
        private DevExpress.XtraGrid.GridControl keeperGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView keeperGridView;
        private DevExpress.XtraTab.XtraTabPage transactionTabPage;
        private DevExpress.XtraGrid.GridControl transactionGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView transactionsGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceOnExchequer;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit lueKeeperNotes;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit;
        private DevExpress.XtraTab.XtraTabPage coverTabPage;
        private DevExpress.XtraTab.XtraTabPage serviceIntervalTabPage;
        private DevExpress.XtraTab.XtraTabPage workDetailTabPage;
        private DevExpress.XtraGrid.GridControl coverGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView coverGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraGrid.GridControl serviceIntervalGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView serviceIntervalGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraGrid.GridControl workDetailGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView workDetailGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit13;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit14;
        private System.Windows.Forms.BindingSource spAS11002EquipmentItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter sp_AS_11002_Equipment_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11044KeeperAllocationItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAllocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference9;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID11;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode7;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID7;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter sp_AS_11044_Keeper_Allocation_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11041TransactionItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference10;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID12;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionType;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedOnExchequer;
        private DevExpress.XtraGrid.Columns.GridColumn colNetTransactionPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colMode8;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID8;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter sp_AS_11041_Transaction_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11047EquipmentBillingItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11050DepreciationItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11075CoverItemBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit1;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11047_Equipment_Billing_ItemTableAdapter sp_AS_11047_Equipment_Billing_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11050_Depreciation_ItemTableAdapter sp_AS_11050_Depreciation_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11075_Cover_ItemTableAdapter sp_AS_11075_Cover_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11078WorkDetailItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkDetailID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID2;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference11;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentMileage;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkCompletionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceIntervalID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceType;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes2;
        private DevExpress.XtraGrid.Columns.GridColumn colMode10;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID10;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter sp_AS_11078_Work_Detail_ItemTableAdapter;
        private DevExpress.XtraTab.XtraTabPage incidentTabPage;
        private DevExpress.XtraGrid.GridControl incidentGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView incidentGridView;
        private DevExpress.XtraTab.XtraTabPage purposeTabPage;
        private DevExpress.XtraGrid.GridControl purposeGridControl;
        private System.Windows.Forms.BindingSource spAS11084PurposeItemBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView purposeGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentPurposeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID15;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference13;
        private DevExpress.XtraGrid.Columns.GridColumn colPurpose;
        private DevExpress.XtraGrid.Columns.GridColumn colPurposeID;
        private DevExpress.XtraGrid.Columns.GridColumn colMode14;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit16;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit17;
        private DevExpress.XtraTab.XtraTabPage fuelCardTabPage;
        private DevExpress.XtraGrid.GridControl fuelCardGridControl;
        private System.Windows.Forms.BindingSource spAS11089FuelCardItemBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView fuelCardGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit18;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit19;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11084_Purpose_ItemTableAdapter sp_AS_11084_Purpose_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11089_Fuel_Card_ItemTableAdapter sp_AS_11089_Fuel_Card_ItemTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelCardID;
        private DevExpress.XtraGrid.Columns.GridColumn colCardNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationMark;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference14;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID16;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeper1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAllocationID1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference2;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID2;
        private DevExpress.XtraGrid.Columns.GridColumn colCardStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCardStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes5;
        private DevExpress.XtraGrid.Columns.GridColumn colMode15;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID15;
        private DevExpress.XtraGrid.Columns.GridColumn colCoverID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference16;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID3;
        private DevExpress.XtraGrid.Columns.GridColumn colCoverType;
        private DevExpress.XtraGrid.Columns.GridColumn colCoverTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colPolicy;
        private DevExpress.XtraGrid.Columns.GridColumn colAnnualCoverCost;
        private DevExpress.XtraGrid.Columns.GridColumn colRenewalDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes3;
        private DevExpress.XtraGrid.Columns.GridColumn colExcess;
        private DevExpress.XtraGrid.Columns.GridColumn colMode11;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID11;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ListTableAdapter sp_AS_11044_Keeper_Allocation_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit2;
        private System.Windows.Forms.BindingSource spAS11081IncidentItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID7;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference5;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentType;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colDateHappened;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colRepairDueID;
        private DevExpress.XtraGrid.Columns.GridColumn colRepairDue;
        private DevExpress.XtraGrid.Columns.GridColumn colSeverityID;
        private DevExpress.XtraGrid.Columns.GridColumn colSeverity;
        private DevExpress.XtraGrid.Columns.GridColumn colWitness;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAtFaultID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAtFault;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalAction;
        private DevExpress.XtraGrid.Columns.GridColumn colFollowUpDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCost;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit incidentNotesMemoEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colMode3;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID3;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter sp_AS_11081_Incident_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11005VehicleItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter sp_AS_11005_Vehicle_ItemTableAdapter;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraTab.XtraTabPage trackerTabPage;
        private DevExpress.XtraGrid.GridControl trackerGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView trackerGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private System.Windows.Forms.BindingSource spAS11023TrackerListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTrackerInformationID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference6;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistration;
        private DevExpress.XtraGrid.Columns.GridColumn colTrackerVID;
        private DevExpress.XtraGrid.Columns.GridColumn colTrackerReference;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID8;
        private DevExpress.XtraGrid.Columns.GridColumn colMake1;
        private DevExpress.XtraGrid.Columns.GridColumn colModel1;
        private DevExpress.XtraGrid.Columns.GridColumn colOdometerMetres;
        private DevExpress.XtraGrid.Columns.GridColumn colOdometerMiles;
        private DevExpress.XtraGrid.Columns.GridColumn colReason;
        private DevExpress.XtraGrid.Columns.GridColumn colMPG;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedKPH;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedMPH;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageSpeed;
        private DevExpress.XtraGrid.Columns.GridColumn colPlaceName;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadName;
        private DevExpress.XtraGrid.Columns.GridColumn colPostSect;
        private DevExpress.XtraGrid.Columns.GridColumn colGeofenceName;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrict;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdate;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colMode4;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID4;
        private System.Windows.Forms.BindingSource spAS11023VerilocationItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11023_Verilocation_ItemTableAdapter sp_AS_11023_Verilocation_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11023_Tracker_ListTableAdapter sp_AS_11023_Tracker_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLatestStartJourney;
        private DevExpress.XtraGrid.Columns.GridColumn colLatestEndJourney;
        private DevExpress.XtraTab.XtraTabPage roadTaxTabPage;
        private DevExpress.XtraGrid.GridControl roadTaxGridControl;
        private System.Windows.Forms.BindingSource spAS11096RoadTaxItemBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView roadTaxGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadTaxID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference12;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID14;
        private DevExpress.XtraGrid.Columns.GridColumn colTaxExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTaxPeriodID;
        private DevExpress.XtraGrid.Columns.GridColumn colTaxPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colMode13;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID13;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11096_Road_Tax_ItemTableAdapter sp_AS_11096_Road_Tax_ItemTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit4;
        private DevExpress.XtraTab.XtraTabPage speedingTabPage;
        private DevExpress.XtraGrid.GridControl speedingGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView speedingGridView;
        private System.Windows.Forms.BindingSource spAS11000PLGeneralBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_GeneralTableAdapter sp_AS_11000_PL_GeneralTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit unresolvedCheckEdit;
        private System.Windows.Forms.BindingSource spAS11105SpeedingItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedingID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference17;
        private DevExpress.XtraGrid.Columns.GridColumn colVID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID17;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeed;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedMPH1;
        private DevExpress.XtraGrid.Columns.GridColumn colTravelTime;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadType;
        private DevExpress.XtraGrid.Columns.GridColumn colPlaceName1;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrict1;
        private DevExpress.XtraGrid.Columns.GridColumn colReported;
        private DevExpress.XtraGrid.Columns.GridColumn colMode17;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID17;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11105_Speeding_ItemTableAdapter sp_AS_11105_Speeding_ItemTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceReported;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit5;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_StatusTableAdapter sp_AS_11000_PL_Incident_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_TypeTableAdapter sp_AS_11000_PL_Incident_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_Repair_DueTableAdapter sp_AS_11000_PL_Incident_Repair_DueTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_SeverityTableAdapter sp_AS_11000_PL_SeverityTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Legal_ActionTableAdapter sp_AS_11000_PL_Legal_ActionTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_At_FaultTableAdapter sp_AS_11000_PL_Keeper_At_FaultTableAdapter;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private System.Windows.Forms.BindingSource spAS11108RentalDetailsItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11108_Rental_Details_ItemTableAdapter sp_AS_11108_Rental_Details_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11102RentalItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11102_Rental_ItemTableAdapter sp_AS_11102_Rental_ItemTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalDetailsID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalCategory1;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalSpecificationID;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalSpecification1;
        private DevExpress.XtraGrid.Columns.GridColumn colRate1;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomerReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colSetAsMain;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes6;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceSetAsMain;
        private System.Windows.Forms.BindingSource spAS11102RentalManagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colMake;
        private DevExpress.XtraGrid.Columns.GridColumn colModel;
        private DevExpress.XtraGrid.Columns.GridColumn colModelSpecifications;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplier;
        private DevExpress.XtraGrid.Columns.GridColumn colUniqueReference;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomerReference;
        private DevExpress.XtraGrid.Columns.GridColumn colRate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colRentalSpecification;
        private DevExpress.XtraGrid.Columns.GridColumn colUnresolvedWarning;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID1;
        private DataSet_AS_CoreTableAdapters.sp_AS_11102_Rental_ManagerTableAdapter sp_AS_11102_Rental_ManagerTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyEdit;
        private DevExpress.XtraTab.XtraTabPage notificationTabPage;
        private System.Windows.Forms.BindingSource spAS11126NotificationItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11126_Notification_ItemTableAdapter sp_AS_11126_Notification_ItemTableAdapter;
        private DevExpress.XtraGrid.GridControl notificationGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView notificationGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationID;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colPriorityID;
        private DevExpress.XtraGrid.Columns.GridColumn colPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colDateToRemind;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperID1;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit15;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarSubItem bbiExport;
        private DevExpress.XtraBars.BarButtonItem bbiExportExcel;
        private DevExpress.XtraBars.BarButtonItem bbiExportExcelOld;
        private System.Windows.Forms.BindingSource spAS11054ServiceDataItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Data_ItemTableAdapter sp_AS_11054_Service_Data_ItemTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDataID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceIntervalScheduleID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID4;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference2;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceType1;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceUnitsID;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeUnitsID;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colWarrantyPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colWarrantyEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWarrantyEndDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDataNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDueDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAlertWithinXDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colAlertWithinXTime;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType1;
        private DevExpress.XtraGrid.Columns.GridColumn colIntervalScheduleNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colIsCurrentInterval;
        private DevExpress.XtraGrid.Columns.GridColumn colMode2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID2;
        private DevExpress.XtraTab.XtraTabPage LinkedDocumentsTabPage;
        private DevExpress.XtraGrid.GridControl gridControlLinkedDocs;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLinkedDocs;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocs;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLinkedDocDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEditLinkedDocs;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentType;
        private DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAvailability;
        private DevExpress.XtraGrid.Columns.GridColumn colLastKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colListPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEngineSize;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelType;
        private DevExpress.XtraGrid.Columns.GridColumn colEmissions;
        private DevExpress.XtraBars.BarButtonItem bbiEmail;
    }
}

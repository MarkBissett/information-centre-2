﻿namespace WoodPlan5
{
    partial class frm_AS_Software_Assign_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Software_Assign_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.lueSoftwareID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11129SoftwareAssignItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11035SoftwareItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueHardwareID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11032HardwareItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deInstallationDate = new DevExpress.XtraEditors.DateEdit();
            this.deUninstallDate = new DevExpress.XtraEditors.DateEdit();
            this.lueInstallationStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLExtraBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.memNotes = new DevExpress.XtraEditors.MemoEdit();
            this.txtSoftwareReference = new DevExpress.XtraEditors.TextEdit();
            this.txtSoftwareDescription = new DevExpress.XtraEditors.TextEdit();
            this.txtHardwareReference = new DevExpress.XtraEditors.TextEdit();
            this.txtHardwareDescription = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSoftwareID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInstallationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInstallationStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHardwareDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHardwareReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHardwareID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUninstallDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSoftwareReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSoftwareDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11129_Software_Assign_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11129_Software_Assign_ItemTableAdapter();
            this.sp_AS_11032_Hardware_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11032_Hardware_ItemTableAdapter();
            this.sp_AS_11035_Software_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11035_Software_ItemTableAdapter();
            this.sp_AS_11000_PL_ExtraTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_ExtraTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11129SoftwareAssignItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11035SoftwareItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHardwareID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11032HardwareItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInstallationDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInstallationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deUninstallDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deUninstallDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueInstallationStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtraBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoftwareReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoftwareDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHardwareReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHardwareDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoftwareID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInstallationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInstallationStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHardwareDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHardwareReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHardwareID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUninstallDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoftwareReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoftwareDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(797, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 454);
            this.barDockControlBottom.Size = new System.Drawing.Size(797, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 428);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(797, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 428);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.ItemForSoftwareReference,
            this.ItemForSoftwareDescription,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(797, 428);
            this.editFormLayoutControlGroup.Text = "Root";
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(126, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(263, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.Location = new System.Drawing.Point(138, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueSoftwareID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueHardwareID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deInstallationDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deUninstallDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueInstallationStatusID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memNotes);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtSoftwareReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtSoftwareDescription);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtHardwareReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtHardwareDescription);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11129SoftwareAssignItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1935, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(797, 428);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // lueSoftwareID
            // 
            this.lueSoftwareID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11129SoftwareAssignItemBindingSource, "SoftwareID", true));
            this.lueSoftwareID.Location = new System.Drawing.Point(125, 83);
            this.lueSoftwareID.MenuManager = this.barManager1;
            this.lueSoftwareID.Name = "lueSoftwareID";
            this.lueSoftwareID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSoftwareID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EquipmentReference", "Equipment Reference", 113, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Product", "Product", 47, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Version", "Version", 45, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Year", "Year", 32, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Licence", "Licence", 45, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AcquisationMethod", "Acquisation Method", 104, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("QuantityLimit", "Quantity Limit", 76, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LicenceKey", "Licence Key", 66, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LicenceType", "Licence Type", 72, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ValidUntil", "Valid Until", 56, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSoftwareID.Properties.DataSource = this.spAS11035SoftwareItemBindingSource;
            this.lueSoftwareID.Properties.DisplayMember = "Product";
            this.lueSoftwareID.Properties.NullText = "";
            this.lueSoftwareID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueSoftwareID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSoftwareID.Properties.ValueMember = "EquipmentID";
            this.lueSoftwareID.Size = new System.Drawing.Size(271, 20);
            this.lueSoftwareID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueSoftwareID.TabIndex = 123;
            // 
            // spAS11129SoftwareAssignItemBindingSource
            // 
            this.spAS11129SoftwareAssignItemBindingSource.DataMember = "sp_AS_11129_Software_Assign_Item";
            this.spAS11129SoftwareAssignItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11035SoftwareItemBindingSource
            // 
            this.spAS11035SoftwareItemBindingSource.DataMember = "sp_AS_11035_Software_Item";
            this.spAS11035SoftwareItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueHardwareID
            // 
            this.lueHardwareID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11129SoftwareAssignItemBindingSource, "HardwareID", true));
            this.lueHardwareID.Location = new System.Drawing.Point(513, 83);
            this.lueHardwareID.MenuManager = this.barManager1;
            this.lueHardwareID.Name = "lueHardwareID";
            this.lueHardwareID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueHardwareID.Properties.DataSource = this.spAS11032HardwareItemBindingSource;
            this.lueHardwareID.Properties.DisplayMember = "EquipmentReference";
            this.lueHardwareID.Properties.NullText = "";
            this.lueHardwareID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueHardwareID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueHardwareID.Properties.ValueMember = "EquipmentID";
            this.lueHardwareID.Size = new System.Drawing.Size(272, 20);
            this.lueHardwareID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueHardwareID.TabIndex = 124;
            // 
            // spAS11032HardwareItemBindingSource
            // 
            this.spAS11032HardwareItemBindingSource.DataMember = "sp_AS_11032_Hardware_Item";
            this.spAS11032HardwareItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deInstallationDate
            // 
            this.deInstallationDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11129SoftwareAssignItemBindingSource, "InstallationDate", true));
            this.deInstallationDate.EditValue = null;
            this.deInstallationDate.Location = new System.Drawing.Point(125, 107);
            this.deInstallationDate.MenuManager = this.barManager1;
            this.deInstallationDate.Name = "deInstallationDate";
            this.deInstallationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deInstallationDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deInstallationDate.Size = new System.Drawing.Size(271, 20);
            this.deInstallationDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deInstallationDate.TabIndex = 125;
            // 
            // deUninstallDate
            // 
            this.deUninstallDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11129SoftwareAssignItemBindingSource, "UninstallDate", true));
            this.deUninstallDate.EditValue = null;
            this.deUninstallDate.Location = new System.Drawing.Point(513, 107);
            this.deUninstallDate.MenuManager = this.barManager1;
            this.deUninstallDate.Name = "deUninstallDate";
            this.deUninstallDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deUninstallDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deUninstallDate.Size = new System.Drawing.Size(272, 20);
            this.deUninstallDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deUninstallDate.TabIndex = 126;
            // 
            // lueInstallationStatusID
            // 
            this.lueInstallationStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11129SoftwareAssignItemBindingSource, "InstallationStatusID", true));
            this.lueInstallationStatusID.Location = new System.Drawing.Point(125, 131);
            this.lueInstallationStatusID.MenuManager = this.barManager1;
            this.lueInstallationStatusID.Name = "lueInstallationStatusID";
            this.lueInstallationStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueInstallationStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Installation Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueInstallationStatusID.Properties.DataSource = this.spAS11000PLExtraBindingSource;
            this.lueInstallationStatusID.Properties.DisplayMember = "Value";
            this.lueInstallationStatusID.Properties.NullText = "";
            this.lueInstallationStatusID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueInstallationStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueInstallationStatusID.Properties.ValueMember = "PickListID";
            this.lueInstallationStatusID.Size = new System.Drawing.Size(660, 20);
            this.lueInstallationStatusID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueInstallationStatusID.TabIndex = 127;
            // 
            // spAS11000PLExtraBindingSource
            // 
            this.spAS11000PLExtraBindingSource.DataMember = "sp_AS_11000_PL_Extra";
            this.spAS11000PLExtraBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // memNotes
            // 
            this.memNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11129SoftwareAssignItemBindingSource, "Notes", true));
            this.memNotes.Location = new System.Drawing.Point(24, 188);
            this.memNotes.MenuManager = this.barManager1;
            this.memNotes.Name = "memNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memNotes, true);
            this.memNotes.Size = new System.Drawing.Size(749, 216);
            this.scSpellChecker.SetSpellCheckerOptions(this.memNotes, optionsSpelling1);
            this.memNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.memNotes.TabIndex = 128;
            this.memNotes.UseOptimizedRendering = true;
            // 
            // txtSoftwareReference
            // 
            this.txtSoftwareReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11129SoftwareAssignItemBindingSource, "SoftwareReference", true));
            this.txtSoftwareReference.Location = new System.Drawing.Point(125, 35);
            this.txtSoftwareReference.MenuManager = this.barManager1;
            this.txtSoftwareReference.Name = "txtSoftwareReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtSoftwareReference, true);
            this.txtSoftwareReference.Size = new System.Drawing.Size(272, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtSoftwareReference, optionsSpelling2);
            this.txtSoftwareReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtSoftwareReference.TabIndex = 129;
            // 
            // txtSoftwareDescription
            // 
            this.txtSoftwareDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11129SoftwareAssignItemBindingSource, "SoftwareDescription", true));
            this.txtSoftwareDescription.Location = new System.Drawing.Point(514, 35);
            this.txtSoftwareDescription.MenuManager = this.barManager1;
            this.txtSoftwareDescription.Name = "txtSoftwareDescription";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtSoftwareDescription, true);
            this.txtSoftwareDescription.Size = new System.Drawing.Size(271, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtSoftwareDescription, optionsSpelling3);
            this.txtSoftwareDescription.StyleController = this.editFormDataLayoutControlGroup;
            this.txtSoftwareDescription.TabIndex = 130;
            // 
            // txtHardwareReference
            // 
            this.txtHardwareReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11129SoftwareAssignItemBindingSource, "HardwareReference", true));
            this.txtHardwareReference.Location = new System.Drawing.Point(125, 59);
            this.txtHardwareReference.MenuManager = this.barManager1;
            this.txtHardwareReference.Name = "txtHardwareReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtHardwareReference, true);
            this.txtHardwareReference.Size = new System.Drawing.Size(271, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtHardwareReference, optionsSpelling4);
            this.txtHardwareReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtHardwareReference.TabIndex = 131;
            // 
            // txtHardwareDescription
            // 
            this.txtHardwareDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11129SoftwareAssignItemBindingSource, "HardwareDescription", true));
            this.txtHardwareDescription.Location = new System.Drawing.Point(513, 59);
            this.txtHardwareDescription.MenuManager = this.barManager1;
            this.txtHardwareDescription.Name = "txtHardwareDescription";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtHardwareDescription, true);
            this.txtHardwareDescription.Size = new System.Drawing.Size(272, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtHardwareDescription, optionsSpelling5);
            this.txtHardwareDescription.StyleController = this.editFormDataLayoutControlGroup;
            this.txtHardwareDescription.TabIndex = 132;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSoftwareID,
            this.ItemForInstallationDate,
            this.ItemForInstallationStatusID,
            this.tabbedControlGroup1,
            this.ItemForHardwareDescription,
            this.ItemForHardwareReference,
            this.ItemForHardwareID,
            this.ItemForUninstallDate});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 47);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(777, 361);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // ItemForSoftwareID
            // 
            this.ItemForSoftwareID.Control = this.lueSoftwareID;
            this.ItemForSoftwareID.CustomizationFormText = "Software :";
            this.ItemForSoftwareID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSoftwareID.Name = "ItemForSoftwareID";
            this.ItemForSoftwareID.Size = new System.Drawing.Size(388, 24);
            this.ItemForSoftwareID.Text = "Software :";
            this.ItemForSoftwareID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForInstallationDate
            // 
            this.ItemForInstallationDate.Control = this.deInstallationDate;
            this.ItemForInstallationDate.CustomizationFormText = "Installation Date :";
            this.ItemForInstallationDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForInstallationDate.Name = "ItemForInstallationDate";
            this.ItemForInstallationDate.Size = new System.Drawing.Size(388, 24);
            this.ItemForInstallationDate.Text = "Installation Date :";
            this.ItemForInstallationDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForInstallationStatusID
            // 
            this.ItemForInstallationStatusID.Control = this.lueInstallationStatusID;
            this.ItemForInstallationStatusID.CustomizationFormText = "Installation Status :";
            this.ItemForInstallationStatusID.Location = new System.Drawing.Point(0, 72);
            this.ItemForInstallationStatusID.Name = "ItemForInstallationStatusID";
            this.ItemForInstallationStatusID.Size = new System.Drawing.Size(777, 24);
            this.ItemForInstallationStatusID.Text = "Installation Status :";
            this.ItemForInstallationStatusID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 96);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(777, 265);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Notes";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(753, 220);
            this.layoutControlGroup2.Text = "Notes";
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.memNotes;
            this.ItemForNotes.CustomizationFormText = "Notes";
            this.ItemForNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(753, 220);
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNotes.TextToControlDistance = 0;
            this.ItemForNotes.TextVisible = false;
            // 
            // ItemForHardwareDescription
            // 
            this.ItemForHardwareDescription.Control = this.txtHardwareDescription;
            this.ItemForHardwareDescription.CustomizationFormText = "Hardware Description :";
            this.ItemForHardwareDescription.Location = new System.Drawing.Point(388, 0);
            this.ItemForHardwareDescription.Name = "ItemForHardwareDescription";
            this.ItemForHardwareDescription.Size = new System.Drawing.Size(389, 24);
            this.ItemForHardwareDescription.Text = "Hardware Description :";
            this.ItemForHardwareDescription.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForHardwareReference
            // 
            this.ItemForHardwareReference.Control = this.txtHardwareReference;
            this.ItemForHardwareReference.CustomizationFormText = "Hardware Reference :";
            this.ItemForHardwareReference.Location = new System.Drawing.Point(0, 0);
            this.ItemForHardwareReference.Name = "ItemForHardwareReference";
            this.ItemForHardwareReference.Size = new System.Drawing.Size(388, 24);
            this.ItemForHardwareReference.Text = "Hardware Reference :";
            this.ItemForHardwareReference.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForHardwareID
            // 
            this.ItemForHardwareID.Control = this.lueHardwareID;
            this.ItemForHardwareID.CustomizationFormText = "Hardware :";
            this.ItemForHardwareID.Location = new System.Drawing.Point(388, 24);
            this.ItemForHardwareID.Name = "ItemForHardwareID";
            this.ItemForHardwareID.Size = new System.Drawing.Size(389, 24);
            this.ItemForHardwareID.Text = "Hardware :";
            this.ItemForHardwareID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForUninstallDate
            // 
            this.ItemForUninstallDate.Control = this.deUninstallDate;
            this.ItemForUninstallDate.CustomizationFormText = "Uninstall Date :";
            this.ItemForUninstallDate.Location = new System.Drawing.Point(388, 48);
            this.ItemForUninstallDate.Name = "ItemForUninstallDate";
            this.ItemForUninstallDate.Size = new System.Drawing.Size(389, 24);
            this.ItemForUninstallDate.Text = "Uninstall Date :";
            this.ItemForUninstallDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForSoftwareReference
            // 
            this.ItemForSoftwareReference.Control = this.txtSoftwareReference;
            this.ItemForSoftwareReference.CustomizationFormText = "Software Reference :";
            this.ItemForSoftwareReference.Location = new System.Drawing.Point(0, 23);
            this.ItemForSoftwareReference.Name = "ItemForSoftwareReference";
            this.ItemForSoftwareReference.Size = new System.Drawing.Size(389, 24);
            this.ItemForSoftwareReference.Text = "Software Reference :";
            this.ItemForSoftwareReference.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForSoftwareDescription
            // 
            this.ItemForSoftwareDescription.Control = this.txtSoftwareDescription;
            this.ItemForSoftwareDescription.CustomizationFormText = "Software Description :";
            this.ItemForSoftwareDescription.Location = new System.Drawing.Point(389, 23);
            this.ItemForSoftwareDescription.Name = "ItemForSoftwareDescription";
            this.ItemForSoftwareDescription.Size = new System.Drawing.Size(388, 24);
            this.ItemForSoftwareDescription.Text = "Software Description :";
            this.ItemForSoftwareDescription.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(389, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(388, 23);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(126, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11129_Software_Assign_ItemTableAdapter
            // 
            this.sp_AS_11129_Software_Assign_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11032_Hardware_ItemTableAdapter
            // 
            this.sp_AS_11032_Hardware_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11035_Software_ItemTableAdapter
            // 
            this.sp_AS_11035_Software_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_ExtraTableAdapter
            // 
            this.sp_AS_11000_PL_ExtraTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Software_Assign_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(797, 484);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Software_Assign_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Installation Details";
            this.Activated += new System.EventHandler(this.frm_AS_Software_Assign_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Software_Assign_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Software_Assign_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueSoftwareID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11129SoftwareAssignItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11035SoftwareItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueHardwareID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11032HardwareItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInstallationDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deInstallationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deUninstallDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deUninstallDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueInstallationStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExtraBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoftwareReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoftwareDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHardwareReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHardwareDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoftwareID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInstallationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInstallationStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHardwareDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHardwareReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHardwareID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUninstallDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoftwareReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoftwareDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LookUpEdit lueSoftwareID;
        private System.Windows.Forms.BindingSource spAS11129SoftwareAssignItemBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueHardwareID;
        private DevExpress.XtraEditors.DateEdit deInstallationDate;
        private DevExpress.XtraEditors.DateEdit deUninstallDate;
        private DevExpress.XtraEditors.LookUpEdit lueInstallationStatusID;
        private DevExpress.XtraEditors.MemoEdit memNotes;
        private DevExpress.XtraEditors.TextEdit txtSoftwareReference;
        private DevExpress.XtraEditors.TextEdit txtSoftwareDescription;
        private DevExpress.XtraEditors.TextEdit txtHardwareReference;
        private DevExpress.XtraEditors.TextEdit txtHardwareDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSoftwareID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInstallationDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInstallationStatusID;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHardwareDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHardwareReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHardwareID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUninstallDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSoftwareReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSoftwareDescription;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11129_Software_Assign_ItemTableAdapter sp_AS_11129_Software_Assign_ItemTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource spAS11035SoftwareItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11032HardwareItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLExtraBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11032_Hardware_ItemTableAdapter sp_AS_11032_Hardware_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11035_Software_ItemTableAdapter sp_AS_11035_Software_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_ExtraTableAdapter sp_AS_11000_PL_ExtraTableAdapter;


    }
}

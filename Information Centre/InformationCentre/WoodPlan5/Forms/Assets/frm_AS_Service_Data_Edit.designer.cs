﻿namespace WoodPlan5
{
    partial class frm_AS_Service_Data_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Service_Data_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForServiceStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.lueServiceStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11163ServiceScheduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLServiceStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.btnAddLinkedRecord = new DevExpress.XtraEditors.SimpleButton();
            this.ceIsCurrentInterval = new DevExpress.XtraEditors.CheckEdit();
            this.serviceIntervalNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11054ServiceDataItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnServiceDueDistance = new DevExpress.XtraEditors.SpinEdit();
            this.deServiceDueDate = new DevExpress.XtraEditors.DateEdit();
            this.lueAlertDistanceUnitsID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLUnitDistTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnAlertWithinXDistance = new DevExpress.XtraEditors.SpinEdit();
            this.lueAlertTimeUnitsID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLUnitTimeTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnAlertWithinXTime = new DevExpress.XtraEditors.SpinEdit();
            this.lueWorkDetailID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11078WorkDetailItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.memIntervalScheduleNotes = new DevExpress.XtraEditors.MemoEdit();
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.lueServiceTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLServiceTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueDistanceUnitsID = new DevExpress.XtraEditors.LookUpEdit();
            this.spnDistanceFrequency = new DevExpress.XtraEditors.SpinEdit();
            this.lueTimeUnitsID = new DevExpress.XtraEditors.LookUpEdit();
            this.spnTimeFrequency = new DevExpress.XtraEditors.SpinEdit();
            this.spnWarrantyPeriod = new DevExpress.XtraEditors.SpinEdit();
            this.deWarrantyEndDate = new DevExpress.XtraEditors.DateEdit();
            this.spnWarrantyEndDistance = new DevExpress.XtraEditors.SpinEdit();
            this.memServiceDataNotes = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForWorkDetailID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroup4 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForIntervalScheduleNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAlertWithinXDistance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAlertWithinXTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAlertDistanceUnitsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAlertTimeUnitsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForServiceDueDistance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForServiceDueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroup3 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForServiceDataNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDistanceUnitsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForServiceTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTimeUnitsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTimeFrequency = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWarrantyPeriod = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWarrantyEndDistance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDistanceFrequency = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWarrantyEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.sp_AS_11163_Service_ScheduleTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11163_Service_ScheduleTableAdapter();
            this.sp_AS_11054_Service_Data_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Data_ItemTableAdapter();
            this.sp_AS_11000_PL_Service_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Service_TypeTableAdapter();
            this.sp_AS_11000_PL_Unit_Dist_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_Dist_TypeTableAdapter();
            this.sp_AS_11000_PL_Unit_Time_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_Time_TypeTableAdapter();
            this.sp_AS_11000_PL_Service_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Service_StatusTableAdapter();
            this.sp_AS_11078_Work_Detail_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueServiceStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11163ServiceScheduleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLServiceStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsCurrentInterval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceDataItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnServiceDueDistance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAlertDistanceUnitsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitDistTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAlertWithinXDistance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAlertTimeUnitsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitTimeTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAlertWithinXTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWorkDetailID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memIntervalScheduleNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueServiceTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLServiceTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDistanceUnitsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnDistanceFrequency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTimeUnitsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnTimeFrequency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnWarrantyPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deWarrantyEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deWarrantyEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnWarrantyEndDistance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memServiceDataNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkDetailID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIntervalScheduleNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertWithinXDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertWithinXTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertDistanceUnitsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertTimeUnitsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDataNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDistanceUnitsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimeUnitsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimeFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarrantyPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarrantyEndDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDistanceFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarrantyEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1298, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 726);
            this.barDockControlBottom.Size = new System.Drawing.Size(1298, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 700);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1298, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 700);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlGroup3});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1281, 846);
            this.editFormLayoutControlGroup.Text = "Root";
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.tabbedControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1261, 825);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 431);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1261, 394);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup2.CaptionImage")));
            this.layoutControlGroup2.CustomizationFormText = "Service Interval Schedule";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForServiceStatusID,
            this.ItemForWorkDetailID,
            this.layoutControlItem2,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.tabbedControlGroup4,
            this.ItemForAlertWithinXDistance,
            this.ItemForAlertWithinXTime,
            this.ItemForAlertDistanceUnitsID,
            this.ItemForAlertTimeUnitsID,
            this.layoutControlItem4,
            this.ItemForServiceDueDistance,
            this.ItemForServiceDueDate,
            this.layoutControlItem3,
            this.emptySpaceItem5});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1237, 346);
            this.layoutControlGroup2.Text = "Service Interval Schedule";
            // 
            // ItemForServiceStatusID
            // 
            this.ItemForServiceStatusID.Control = this.lueServiceStatusID;
            this.ItemForServiceStatusID.CustomizationFormText = "Service Status ID";
            this.ItemForServiceStatusID.Location = new System.Drawing.Point(0, 26);
            this.ItemForServiceStatusID.Name = "ItemForServiceStatusID";
            this.ItemForServiceStatusID.Size = new System.Drawing.Size(618, 24);
            this.ItemForServiceStatusID.Text = "Service Status :";
            this.ItemForServiceStatusID.TextSize = new System.Drawing.Size(118, 13);
            // 
            // lueServiceStatusID
            // 
            this.lueServiceStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "ServiceStatusID", true));
            this.lueServiceStatusID.Location = new System.Drawing.Point(145, 505);
            this.lueServiceStatusID.MenuManager = this.barManager1;
            this.lueServiceStatusID.Name = "lueServiceStatusID";
            this.lueServiceStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueServiceStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Service Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueServiceStatusID.Properties.DataSource = this.spAS11000PLServiceStatusBindingSource;
            this.lueServiceStatusID.Properties.DisplayMember = "Value";
            this.lueServiceStatusID.Properties.NullText = "";
            this.lueServiceStatusID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueServiceStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueServiceStatusID.Properties.ValueMember = "PickListID";
            this.lueServiceStatusID.Size = new System.Drawing.Size(493, 20);
            this.lueServiceStatusID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueServiceStatusID.TabIndex = 123;
            this.lueServiceStatusID.Tag = "Service Status";
            this.lueServiceStatusID.EditValueChanged += new System.EventHandler(this.lueServiceStatusID_EditValueChanged);
            this.lueServiceStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpEdit_Validating);
            // 
            // spAS11163ServiceScheduleBindingSource
            // 
            this.spAS11163ServiceScheduleBindingSource.DataMember = "sp_AS_11163_Service_Schedule";
            this.spAS11163ServiceScheduleBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLServiceStatusBindingSource
            // 
            this.spAS11000PLServiceStatusBindingSource.DataMember = "sp_AS_11000_PL_Service_Status";
            this.spAS11000PLServiceStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.btnAddLinkedRecord);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceIsCurrentInterval);
            this.editFormDataLayoutControlGroup.Controls.Add(this.serviceIntervalNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueServiceStatusID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnServiceDueDistance);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deServiceDueDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueAlertDistanceUnitsID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnAlertWithinXDistance);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueAlertTimeUnitsID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnAlertWithinXTime);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueWorkDetailID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memIntervalScheduleNotes);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEquipmentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueServiceTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueDistanceUnitsID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnDistanceFrequency);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueTimeUnitsID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnTimeFrequency);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnWarrantyPeriod);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deWarrantyEndDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnWarrantyEndDistance);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memServiceDataNotes);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11054ServiceDataItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2709, 277, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1298, 700);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // btnAddLinkedRecord
            // 
            this.btnAddLinkedRecord.Enabled = false;
            this.btnAddLinkedRecord.Image = ((System.Drawing.Image)(resources.GetObject("btnAddLinkedRecord.Image")));
            this.btnAddLinkedRecord.Location = new System.Drawing.Point(742, 479);
            this.btnAddLinkedRecord.Name = "btnAddLinkedRecord";
            this.btnAddLinkedRecord.Size = new System.Drawing.Size(161, 22);
            this.btnAddLinkedRecord.StyleController = this.editFormDataLayoutControlGroup;
            this.btnAddLinkedRecord.TabIndex = 144;
            this.btnAddLinkedRecord.Text = "Add Linked Service Interval";
            this.btnAddLinkedRecord.Click += new System.EventHandler(this.btnAddLinkedRecord_Click);
            // 
            // ceIsCurrentInterval
            // 
            this.ceIsCurrentInterval.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "IsCurrentInterval", true));
            this.ceIsCurrentInterval.Location = new System.Drawing.Point(642, 505);
            this.ceIsCurrentInterval.MenuManager = this.barManager1;
            this.ceIsCurrentInterval.Name = "ceIsCurrentInterval";
            this.ceIsCurrentInterval.Properties.Caption = "Is Current Interval";
            this.ceIsCurrentInterval.Size = new System.Drawing.Size(615, 19);
            this.ceIsCurrentInterval.StyleController = this.editFormDataLayoutControlGroup;
            this.ceIsCurrentInterval.TabIndex = 143;
            // 
            // serviceIntervalNavigator
            // 
            this.serviceIntervalNavigator.Buttons.Append.Visible = false;
            this.serviceIntervalNavigator.Buttons.CancelEdit.Visible = false;
            this.serviceIntervalNavigator.Buttons.EndEdit.Visible = false;
            this.serviceIntervalNavigator.Buttons.Remove.Visible = false;
            this.serviceIntervalNavigator.DataSource = this.spAS11163ServiceScheduleBindingSource;
            this.serviceIntervalNavigator.Location = new System.Drawing.Point(483, 479);
            this.serviceIntervalNavigator.Name = "serviceIntervalNavigator";
            this.serviceIntervalNavigator.Size = new System.Drawing.Size(255, 19);
            this.serviceIntervalNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.serviceIntervalNavigator.TabIndex = 142;
            this.serviceIntervalNavigator.Text = "depreciationDataNavigator";
            this.serviceIntervalNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11054ServiceDataItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(474, 48);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(255, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // spAS11054ServiceDataItemBindingSource
            // 
            this.spAS11054ServiceDataItemBindingSource.DataMember = "sp_AS_11054_Service_Data_Item";
            this.spAS11054ServiceDataItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            this.spAS11054ServiceDataItemBindingSource.CurrentChanged += new System.EventHandler(this.spAS11054ServiceDataItemBindingSource_CurrentChanged);
            // 
            // spnServiceDueDistance
            // 
            this.spnServiceDueDistance.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "ServiceDueDistance", true));
            this.spnServiceDueDistance.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnServiceDueDistance.Location = new System.Drawing.Point(145, 529);
            this.spnServiceDueDistance.MenuManager = this.barManager1;
            this.spnServiceDueDistance.Name = "spnServiceDueDistance";
            this.spnServiceDueDistance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnServiceDueDistance.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spnServiceDueDistance.Size = new System.Drawing.Size(493, 20);
            this.spnServiceDueDistance.StyleController = this.editFormDataLayoutControlGroup;
            this.spnServiceDueDistance.TabIndex = 124;
            this.spnServiceDueDistance.Validating += new System.ComponentModel.CancelEventHandler(this.spnDistanceFrequency_Validating);
            // 
            // deServiceDueDate
            // 
            this.deServiceDueDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "ServiceDueDate", true));
            this.deServiceDueDate.EditValue = null;
            this.deServiceDueDate.Location = new System.Drawing.Point(763, 529);
            this.deServiceDueDate.MenuManager = this.barManager1;
            this.deServiceDueDate.Name = "deServiceDueDate";
            this.deServiceDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deServiceDueDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deServiceDueDate.Size = new System.Drawing.Size(494, 20);
            this.deServiceDueDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deServiceDueDate.TabIndex = 125;
            this.deServiceDueDate.Tag = "Service Due Date";
            this.deServiceDueDate.Validating += new System.ComponentModel.CancelEventHandler(this.DateEdit_Validating);
            // 
            // lueAlertDistanceUnitsID
            // 
            this.lueAlertDistanceUnitsID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "AlertDistanceUnitsID", true));
            this.lueAlertDistanceUnitsID.Location = new System.Drawing.Point(145, 553);
            this.lueAlertDistanceUnitsID.MenuManager = this.barManager1;
            this.lueAlertDistanceUnitsID.Name = "lueAlertDistanceUnitsID";
            this.lueAlertDistanceUnitsID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAlertDistanceUnitsID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Distance Units", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAlertDistanceUnitsID.Properties.DataSource = this.spAS11000PLUnitDistTypeBindingSource;
            this.lueAlertDistanceUnitsID.Properties.DisplayMember = "Value";
            this.lueAlertDistanceUnitsID.Properties.NullText = "";
            this.lueAlertDistanceUnitsID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueAlertDistanceUnitsID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueAlertDistanceUnitsID.Properties.ValueMember = "PickListID";
            this.lueAlertDistanceUnitsID.Size = new System.Drawing.Size(493, 20);
            this.lueAlertDistanceUnitsID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueAlertDistanceUnitsID.TabIndex = 126;
            this.lueAlertDistanceUnitsID.Tag = "Alert Distance Units";
            this.lueAlertDistanceUnitsID.EditValueChanged += new System.EventHandler(this.lueUnitsID_EditValueChanged);
            this.lueAlertDistanceUnitsID.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpEdit_Validating);
            // 
            // spAS11000PLUnitDistTypeBindingSource
            // 
            this.spAS11000PLUnitDistTypeBindingSource.DataMember = "sp_AS_11000_PL_Unit_Dist_Type";
            this.spAS11000PLUnitDistTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnAlertWithinXDistance
            // 
            this.spnAlertWithinXDistance.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "AlertWithinXDistance", true));
            this.spnAlertWithinXDistance.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnAlertWithinXDistance.Location = new System.Drawing.Point(763, 553);
            this.spnAlertWithinXDistance.MenuManager = this.barManager1;
            this.spnAlertWithinXDistance.Name = "spnAlertWithinXDistance";
            this.spnAlertWithinXDistance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnAlertWithinXDistance.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spnAlertWithinXDistance.Size = new System.Drawing.Size(494, 20);
            this.spnAlertWithinXDistance.StyleController = this.editFormDataLayoutControlGroup;
            this.spnAlertWithinXDistance.TabIndex = 127;
            this.spnAlertWithinXDistance.Validating += new System.ComponentModel.CancelEventHandler(this.spnDistanceFrequency_Validating);
            // 
            // lueAlertTimeUnitsID
            // 
            this.lueAlertTimeUnitsID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "AlertTimeUnitsID", true));
            this.lueAlertTimeUnitsID.Location = new System.Drawing.Point(145, 577);
            this.lueAlertTimeUnitsID.MenuManager = this.barManager1;
            this.lueAlertTimeUnitsID.Name = "lueAlertTimeUnitsID";
            this.lueAlertTimeUnitsID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAlertTimeUnitsID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Time Units", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAlertTimeUnitsID.Properties.DataSource = this.spAS11000PLUnitTimeTypeBindingSource;
            this.lueAlertTimeUnitsID.Properties.DisplayMember = "Value";
            this.lueAlertTimeUnitsID.Properties.NullText = "";
            this.lueAlertTimeUnitsID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueAlertTimeUnitsID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueAlertTimeUnitsID.Properties.ValueMember = "PickListID";
            this.lueAlertTimeUnitsID.Size = new System.Drawing.Size(493, 20);
            this.lueAlertTimeUnitsID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueAlertTimeUnitsID.TabIndex = 128;
            this.lueAlertTimeUnitsID.Tag = "Alert Time Units";
            this.lueAlertTimeUnitsID.EditValueChanged += new System.EventHandler(this.lueUnitsID_EditValueChanged);
            this.lueAlertTimeUnitsID.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpEdit_Validating);
            // 
            // spAS11000PLUnitTimeTypeBindingSource
            // 
            this.spAS11000PLUnitTimeTypeBindingSource.DataMember = "sp_AS_11000_PL_Unit_Time_Type";
            this.spAS11000PLUnitTimeTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnAlertWithinXTime
            // 
            this.spnAlertWithinXTime.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "AlertWithinXTime", true));
            this.spnAlertWithinXTime.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnAlertWithinXTime.Location = new System.Drawing.Point(763, 577);
            this.spnAlertWithinXTime.MenuManager = this.barManager1;
            this.spnAlertWithinXTime.Name = "spnAlertWithinXTime";
            this.spnAlertWithinXTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnAlertWithinXTime.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spnAlertWithinXTime.Size = new System.Drawing.Size(494, 20);
            this.spnAlertWithinXTime.StyleController = this.editFormDataLayoutControlGroup;
            this.spnAlertWithinXTime.TabIndex = 129;
            this.spnAlertWithinXTime.Validating += new System.ComponentModel.CancelEventHandler(this.spnDistanceFrequency_Validating);
            // 
            // lueWorkDetailID
            // 
            this.lueWorkDetailID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "WorkDetailID", true));
            this.lueWorkDetailID.Location = new System.Drawing.Point(145, 601);
            this.lueWorkDetailID.MenuManager = this.barManager1;
            this.lueWorkDetailID.Name = "lueWorkDetailID";
            this.lueWorkDetailID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueWorkDetailID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Add Work Order", "Add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueWorkDetailID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Refresh Work Order Details", "Refresh", null, true)});
            this.lueWorkDetailID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WorkType", "Work Type", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DateRaised", "Date Raised", 100, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierReference", "Supplier Reference", 101, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Status", "Status", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OrderNumber", "Order Number", 100, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WorkCompletionDate", "Work Completion Date", 100, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueWorkDetailID.Properties.DataSource = this.spAS11078WorkDetailItemBindingSource;
            this.lueWorkDetailID.Properties.DisplayMember = "WorkType";
            this.lueWorkDetailID.Properties.NullText = "";
            this.lueWorkDetailID.Properties.NullValuePrompt = "-- Please Select Work Order --";
            this.lueWorkDetailID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueWorkDetailID.Properties.PopupWidth = 600;
            this.lueWorkDetailID.Properties.ValueMember = "WorkDetailID";
            this.lueWorkDetailID.Size = new System.Drawing.Size(493, 22);
            this.lueWorkDetailID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueWorkDetailID.TabIndex = 130;
            this.lueWorkDetailID.Tag = "Work Order";
            this.lueWorkDetailID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lueWorkDetailID_ButtonPressed);
            this.lueWorkDetailID.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpEdit_Validating);
            // 
            // spAS11078WorkDetailItemBindingSource
            // 
            this.spAS11078WorkDetailItemBindingSource.DataMember = "sp_AS_11078_Work_Detail_Item";
            this.spAS11078WorkDetailItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // memIntervalScheduleNotes
            // 
            this.memIntervalScheduleNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "IntervalScheduleNotes", true));
            this.memIntervalScheduleNotes.Location = new System.Drawing.Point(36, 663);
            this.memIntervalScheduleNotes.MenuManager = this.barManager1;
            this.memIntervalScheduleNotes.Name = "memIntervalScheduleNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memIntervalScheduleNotes, true);
            this.memIntervalScheduleNotes.Size = new System.Drawing.Size(1209, 146);
            this.scSpellChecker.SetSpellCheckerOptions(this.memIntervalScheduleNotes, optionsSpelling1);
            this.memIntervalScheduleNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.memIntervalScheduleNotes.TabIndex = 131;
            this.memIntervalScheduleNotes.UseOptimizedRendering = true;
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceDataItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(145, 71);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(493, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling2);
            this.txtEquipmentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEquipmentReference.TabIndex = 132;
            // 
            // lueServiceTypeID
            // 
            this.lueServiceTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceDataItemBindingSource, "ServiceTypeID", true));
            this.lueServiceTypeID.Location = new System.Drawing.Point(145, 95);
            this.lueServiceTypeID.MenuManager = this.barManager1;
            this.lueServiceTypeID.Name = "lueServiceTypeID";
            this.lueServiceTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueServiceTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Service Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueServiceTypeID.Properties.DataSource = this.spAS11000PLServiceTypeBindingSource;
            this.lueServiceTypeID.Properties.DisplayMember = "Value";
            this.lueServiceTypeID.Properties.NullText = "";
            this.lueServiceTypeID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueServiceTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueServiceTypeID.Properties.ValueMember = "PickListID";
            this.lueServiceTypeID.Size = new System.Drawing.Size(493, 20);
            this.lueServiceTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueServiceTypeID.TabIndex = 133;
            this.lueServiceTypeID.Tag = "Service Type";
            this.lueServiceTypeID.EditValueChanged += new System.EventHandler(this.lueServiceTypeID_EditValueChanged);
            this.lueServiceTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpEdit_Validating);
            // 
            // spAS11000PLServiceTypeBindingSource
            // 
            this.spAS11000PLServiceTypeBindingSource.DataMember = "sp_AS_11000_PL_Service_Type";
            this.spAS11000PLServiceTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueDistanceUnitsID
            // 
            this.lueDistanceUnitsID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceDataItemBindingSource, "DistanceUnitsID", true));
            this.lueDistanceUnitsID.Location = new System.Drawing.Point(145, 119);
            this.lueDistanceUnitsID.MenuManager = this.barManager1;
            this.lueDistanceUnitsID.Name = "lueDistanceUnitsID";
            this.lueDistanceUnitsID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDistanceUnitsID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Distance Units", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueDistanceUnitsID.Properties.DataSource = this.spAS11000PLUnitDistTypeBindingSource;
            this.lueDistanceUnitsID.Properties.DisplayMember = "Value";
            this.lueDistanceUnitsID.Properties.NullText = "";
            this.lueDistanceUnitsID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueDistanceUnitsID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueDistanceUnitsID.Properties.ValueMember = "PickListID";
            this.lueDistanceUnitsID.Size = new System.Drawing.Size(493, 20);
            this.lueDistanceUnitsID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueDistanceUnitsID.TabIndex = 134;
            this.lueDistanceUnitsID.Tag = "Distance Units";
            this.lueDistanceUnitsID.EditValueChanged += new System.EventHandler(this.lueUnitsID_EditValueChanged);
            this.lueDistanceUnitsID.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpEdit_Validating);
            // 
            // spnDistanceFrequency
            // 
            this.spnDistanceFrequency.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceDataItemBindingSource, "DistanceFrequency", true));
            this.spnDistanceFrequency.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnDistanceFrequency.Location = new System.Drawing.Point(145, 167);
            this.spnDistanceFrequency.MenuManager = this.barManager1;
            this.spnDistanceFrequency.Name = "spnDistanceFrequency";
            this.spnDistanceFrequency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnDistanceFrequency.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spnDistanceFrequency.Size = new System.Drawing.Size(493, 20);
            this.spnDistanceFrequency.StyleController = this.editFormDataLayoutControlGroup;
            this.spnDistanceFrequency.TabIndex = 135;
            this.spnDistanceFrequency.Validating += new System.ComponentModel.CancelEventHandler(this.spnDistanceFrequency_Validating);
            // 
            // lueTimeUnitsID
            // 
            this.lueTimeUnitsID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceDataItemBindingSource, "TimeUnitsID", true));
            this.lueTimeUnitsID.Location = new System.Drawing.Point(763, 119);
            this.lueTimeUnitsID.MenuManager = this.barManager1;
            this.lueTimeUnitsID.Name = "lueTimeUnitsID";
            this.lueTimeUnitsID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTimeUnitsID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Time Units", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueTimeUnitsID.Properties.DataSource = this.spAS11000PLUnitTimeTypeBindingSource;
            this.lueTimeUnitsID.Properties.DisplayMember = "Value";
            this.lueTimeUnitsID.Properties.NullText = "";
            this.lueTimeUnitsID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueTimeUnitsID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueTimeUnitsID.Properties.ValueMember = "PickListID";
            this.lueTimeUnitsID.Size = new System.Drawing.Size(494, 20);
            this.lueTimeUnitsID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueTimeUnitsID.TabIndex = 136;
            this.lueTimeUnitsID.Tag = "Time Units";
            this.lueTimeUnitsID.EditValueChanged += new System.EventHandler(this.lueUnitsID_EditValueChanged);
            this.lueTimeUnitsID.Validating += new System.ComponentModel.CancelEventHandler(this.LookUpEdit_Validating);
            // 
            // spnTimeFrequency
            // 
            this.spnTimeFrequency.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceDataItemBindingSource, "TimeFrequency", true));
            this.spnTimeFrequency.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnTimeFrequency.Location = new System.Drawing.Point(763, 167);
            this.spnTimeFrequency.MenuManager = this.barManager1;
            this.spnTimeFrequency.Name = "spnTimeFrequency";
            this.spnTimeFrequency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnTimeFrequency.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spnTimeFrequency.Size = new System.Drawing.Size(494, 20);
            this.spnTimeFrequency.StyleController = this.editFormDataLayoutControlGroup;
            this.spnTimeFrequency.TabIndex = 137;
            this.spnTimeFrequency.Validating += new System.ComponentModel.CancelEventHandler(this.spnDistanceFrequency_Validating);
            // 
            // spnWarrantyPeriod
            // 
            this.spnWarrantyPeriod.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceDataItemBindingSource, "WarrantyPeriod", true));
            this.spnWarrantyPeriod.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnWarrantyPeriod.Location = new System.Drawing.Point(763, 143);
            this.spnWarrantyPeriod.MenuManager = this.barManager1;
            this.spnWarrantyPeriod.Name = "spnWarrantyPeriod";
            this.spnWarrantyPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnWarrantyPeriod.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spnWarrantyPeriod.Size = new System.Drawing.Size(494, 20);
            this.spnWarrantyPeriod.StyleController = this.editFormDataLayoutControlGroup;
            this.spnWarrantyPeriod.TabIndex = 138;
            this.spnWarrantyPeriod.Validating += new System.ComponentModel.CancelEventHandler(this.spnDistanceFrequency_Validating);
            // 
            // deWarrantyEndDate
            // 
            this.deWarrantyEndDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceDataItemBindingSource, "WarrantyEndDate", true));
            this.deWarrantyEndDate.EditValue = null;
            this.deWarrantyEndDate.Location = new System.Drawing.Point(763, 95);
            this.deWarrantyEndDate.MenuManager = this.barManager1;
            this.deWarrantyEndDate.Name = "deWarrantyEndDate";
            this.deWarrantyEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deWarrantyEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deWarrantyEndDate.Size = new System.Drawing.Size(494, 20);
            this.deWarrantyEndDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deWarrantyEndDate.TabIndex = 139;
            this.deWarrantyEndDate.Tag = "Warranty End Date";
            this.deWarrantyEndDate.Validating += new System.ComponentModel.CancelEventHandler(this.DateEdit_Validating);
            // 
            // spnWarrantyEndDistance
            // 
            this.spnWarrantyEndDistance.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceDataItemBindingSource, "WarrantyEndDistance", true));
            this.spnWarrantyEndDistance.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnWarrantyEndDistance.Location = new System.Drawing.Point(145, 143);
            this.spnWarrantyEndDistance.MenuManager = this.barManager1;
            this.spnWarrantyEndDistance.Name = "spnWarrantyEndDistance";
            this.spnWarrantyEndDistance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnWarrantyEndDistance.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spnWarrantyEndDistance.Size = new System.Drawing.Size(493, 20);
            this.spnWarrantyEndDistance.StyleController = this.editFormDataLayoutControlGroup;
            this.spnWarrantyEndDistance.TabIndex = 140;
            this.spnWarrantyEndDistance.Validating += new System.ComponentModel.CancelEventHandler(this.spnDistanceFrequency_Validating);
            // 
            // memServiceDataNotes
            // 
            this.memServiceDataNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11054ServiceDataItemBindingSource, "ServiceDataNotes", true));
            this.memServiceDataNotes.Location = new System.Drawing.Point(36, 247);
            this.memServiceDataNotes.MenuManager = this.barManager1;
            this.memServiceDataNotes.Name = "memServiceDataNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memServiceDataNotes, true);
            this.memServiceDataNotes.Size = new System.Drawing.Size(1209, 168);
            this.scSpellChecker.SetSpellCheckerOptions(this.memServiceDataNotes, optionsSpelling3);
            this.memServiceDataNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.memServiceDataNotes.TabIndex = 141;
            this.memServiceDataNotes.UseOptimizedRendering = true;
            // 
            // ItemForWorkDetailID
            // 
            this.ItemForWorkDetailID.Control = this.lueWorkDetailID;
            this.ItemForWorkDetailID.CustomizationFormText = "Work Detail ID";
            this.ItemForWorkDetailID.Location = new System.Drawing.Point(0, 122);
            this.ItemForWorkDetailID.Name = "ItemForWorkDetailID";
            this.ItemForWorkDetailID.Size = new System.Drawing.Size(618, 26);
            this.ItemForWorkDetailID.Text = "Work Order :";
            this.ItemForWorkDetailID.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.serviceIntervalNavigator;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(459, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(259, 26);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(459, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(883, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(354, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroup4
            // 
            this.tabbedControlGroup4.CustomizationFormText = "tabbedControlGroup4";
            this.tabbedControlGroup4.Location = new System.Drawing.Point(0, 148);
            this.tabbedControlGroup4.Name = "tabbedControlGroup4";
            this.tabbedControlGroup4.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup4.SelectedTabPageIndex = 0;
            this.tabbedControlGroup4.Size = new System.Drawing.Size(1237, 198);
            this.tabbedControlGroup4.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5});
            this.tabbedControlGroup4.Text = "tabbedControlGroup4";
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImage")));
            this.layoutControlGroup5.ContentImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.layoutControlGroup5.CustomizationFormText = "Interval Schedule Notes";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIntervalScheduleNotes});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1213, 150);
            this.layoutControlGroup5.Text = "Interval Schedule Notes";
            // 
            // ItemForIntervalScheduleNotes
            // 
            this.ItemForIntervalScheduleNotes.Control = this.memIntervalScheduleNotes;
            this.ItemForIntervalScheduleNotes.CustomizationFormText = "Interval Schedule Notes";
            this.ItemForIntervalScheduleNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForIntervalScheduleNotes.MaxSize = new System.Drawing.Size(0, 150);
            this.ItemForIntervalScheduleNotes.MinSize = new System.Drawing.Size(135, 150);
            this.ItemForIntervalScheduleNotes.Name = "ItemForIntervalScheduleNotes";
            this.ItemForIntervalScheduleNotes.Size = new System.Drawing.Size(1213, 150);
            this.ItemForIntervalScheduleNotes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIntervalScheduleNotes.Text = "Interval Schedule Notes";
            this.ItemForIntervalScheduleNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForIntervalScheduleNotes.TextToControlDistance = 0;
            this.ItemForIntervalScheduleNotes.TextVisible = false;
            // 
            // ItemForAlertWithinXDistance
            // 
            this.ItemForAlertWithinXDistance.Control = this.spnAlertWithinXDistance;
            this.ItemForAlertWithinXDistance.CustomizationFormText = "Alert Within XDistance";
            this.ItemForAlertWithinXDistance.Location = new System.Drawing.Point(618, 74);
            this.ItemForAlertWithinXDistance.Name = "ItemForAlertWithinXDistance";
            this.ItemForAlertWithinXDistance.Size = new System.Drawing.Size(619, 24);
            this.ItemForAlertWithinXDistance.Text = "Alert Within X Distance :";
            this.ItemForAlertWithinXDistance.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForAlertWithinXTime
            // 
            this.ItemForAlertWithinXTime.Control = this.spnAlertWithinXTime;
            this.ItemForAlertWithinXTime.CustomizationFormText = "Alert Within XTime";
            this.ItemForAlertWithinXTime.Location = new System.Drawing.Point(618, 98);
            this.ItemForAlertWithinXTime.Name = "ItemForAlertWithinXTime";
            this.ItemForAlertWithinXTime.Size = new System.Drawing.Size(619, 24);
            this.ItemForAlertWithinXTime.Text = "Alert Within X Time :";
            this.ItemForAlertWithinXTime.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForAlertDistanceUnitsID
            // 
            this.ItemForAlertDistanceUnitsID.Control = this.lueAlertDistanceUnitsID;
            this.ItemForAlertDistanceUnitsID.CustomizationFormText = "Alert Distance Units ID";
            this.ItemForAlertDistanceUnitsID.Location = new System.Drawing.Point(0, 74);
            this.ItemForAlertDistanceUnitsID.Name = "ItemForAlertDistanceUnitsID";
            this.ItemForAlertDistanceUnitsID.Size = new System.Drawing.Size(618, 24);
            this.ItemForAlertDistanceUnitsID.Text = "Alert Distance Units :";
            this.ItemForAlertDistanceUnitsID.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForAlertTimeUnitsID
            // 
            this.ItemForAlertTimeUnitsID.Control = this.lueAlertTimeUnitsID;
            this.ItemForAlertTimeUnitsID.CustomizationFormText = "Alert Time Units ID";
            this.ItemForAlertTimeUnitsID.Location = new System.Drawing.Point(0, 98);
            this.ItemForAlertTimeUnitsID.Name = "ItemForAlertTimeUnitsID";
            this.ItemForAlertTimeUnitsID.Size = new System.Drawing.Size(618, 24);
            this.ItemForAlertTimeUnitsID.Text = "Alert Time Units :";
            this.ItemForAlertTimeUnitsID.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.ceIsCurrentInterval;
            this.layoutControlItem4.CustomizationFormText = "Is Current Interval:";
            this.layoutControlItem4.Location = new System.Drawing.Point(618, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(619, 24);
            this.layoutControlItem4.Text = "Is Current Interval:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // ItemForServiceDueDistance
            // 
            this.ItemForServiceDueDistance.Control = this.spnServiceDueDistance;
            this.ItemForServiceDueDistance.CustomizationFormText = "Service Due Distance";
            this.ItemForServiceDueDistance.Location = new System.Drawing.Point(0, 50);
            this.ItemForServiceDueDistance.Name = "ItemForServiceDueDistance";
            this.ItemForServiceDueDistance.Size = new System.Drawing.Size(618, 24);
            this.ItemForServiceDueDistance.Text = "Service Due Distance :";
            this.ItemForServiceDueDistance.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForServiceDueDate
            // 
            this.ItemForServiceDueDate.Control = this.deServiceDueDate;
            this.ItemForServiceDueDate.CustomizationFormText = "Service Due Date";
            this.ItemForServiceDueDate.Location = new System.Drawing.Point(618, 50);
            this.ItemForServiceDueDate.Name = "ItemForServiceDueDate";
            this.ItemForServiceDueDate.Size = new System.Drawing.Size(619, 24);
            this.ItemForServiceDueDate.Text = "Service Due Date :";
            this.ItemForServiceDueDate.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnAddLinkedRecord;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(718, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(165, 26);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(618, 122);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(619, 26);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(1261, 431);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            this.tabbedControlGroup2.Text = "tabbedControlGroup2";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Service Data";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.tabbedControlGroup3,
            this.ItemForEquipmentReference,
            this.ItemForDistanceUnitsID,
            this.ItemForServiceTypeID,
            this.ItemForTimeUnitsID,
            this.ItemForTimeFrequency,
            this.emptySpaceItem6,
            this.ItemForWarrantyPeriod,
            this.ItemForWarrantyEndDistance,
            this.ItemForDistanceFrequency,
            this.emptySpaceItem7,
            this.ItemForWarrantyEndDate,
            this.emptySpaceItem8});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1237, 383);
            this.layoutControlGroup4.Text = "Service Data";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(450, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(259, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(450, 23);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(709, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(528, 23);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroup3
            // 
            this.tabbedControlGroup3.CustomizationFormText = "tabbedControlGroup3";
            this.tabbedControlGroup3.Location = new System.Drawing.Point(0, 163);
            this.tabbedControlGroup3.Name = "tabbedControlGroup3";
            this.tabbedControlGroup3.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup3.SelectedTabPageIndex = 0;
            this.tabbedControlGroup3.Size = new System.Drawing.Size(1237, 220);
            this.tabbedControlGroup3.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6});
            this.tabbedControlGroup3.Text = "tabbedControlGroup3";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup6.CaptionImage")));
            this.layoutControlGroup6.CustomizationFormText = "Service Data Notes";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForServiceDataNotes});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1213, 172);
            this.layoutControlGroup6.Text = "Service Data Notes";
            // 
            // ItemForServiceDataNotes
            // 
            this.ItemForServiceDataNotes.Control = this.memServiceDataNotes;
            this.ItemForServiceDataNotes.CustomizationFormText = "Service Data Notes";
            this.ItemForServiceDataNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForServiceDataNotes.MaxSize = new System.Drawing.Size(0, 172);
            this.ItemForServiceDataNotes.MinSize = new System.Drawing.Size(135, 172);
            this.ItemForServiceDataNotes.Name = "ItemForServiceDataNotes";
            this.ItemForServiceDataNotes.Size = new System.Drawing.Size(1213, 172);
            this.ItemForServiceDataNotes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForServiceDataNotes.Text = "Service Data Notes";
            this.ItemForServiceDataNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForServiceDataNotes.TextToControlDistance = 0;
            this.ItemForServiceDataNotes.TextVisible = false;
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.txtEquipmentReference;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 23);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(618, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference :";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForDistanceUnitsID
            // 
            this.ItemForDistanceUnitsID.Control = this.lueDistanceUnitsID;
            this.ItemForDistanceUnitsID.CustomizationFormText = "Distance Units ID";
            this.ItemForDistanceUnitsID.Location = new System.Drawing.Point(0, 71);
            this.ItemForDistanceUnitsID.Name = "ItemForDistanceUnitsID";
            this.ItemForDistanceUnitsID.Size = new System.Drawing.Size(618, 24);
            this.ItemForDistanceUnitsID.Text = "Distance Units :";
            this.ItemForDistanceUnitsID.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForServiceTypeID
            // 
            this.ItemForServiceTypeID.Control = this.lueServiceTypeID;
            this.ItemForServiceTypeID.CustomizationFormText = "Service Type ID";
            this.ItemForServiceTypeID.Location = new System.Drawing.Point(0, 47);
            this.ItemForServiceTypeID.Name = "ItemForServiceTypeID";
            this.ItemForServiceTypeID.Size = new System.Drawing.Size(618, 24);
            this.ItemForServiceTypeID.Text = "Service Type :";
            this.ItemForServiceTypeID.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForTimeUnitsID
            // 
            this.ItemForTimeUnitsID.Control = this.lueTimeUnitsID;
            this.ItemForTimeUnitsID.CustomizationFormText = "Time Units ID";
            this.ItemForTimeUnitsID.Location = new System.Drawing.Point(618, 71);
            this.ItemForTimeUnitsID.Name = "ItemForTimeUnitsID";
            this.ItemForTimeUnitsID.Size = new System.Drawing.Size(619, 24);
            this.ItemForTimeUnitsID.Text = "Time Units :";
            this.ItemForTimeUnitsID.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForTimeFrequency
            // 
            this.ItemForTimeFrequency.Control = this.spnTimeFrequency;
            this.ItemForTimeFrequency.CustomizationFormText = "Time Frequency";
            this.ItemForTimeFrequency.Location = new System.Drawing.Point(618, 119);
            this.ItemForTimeFrequency.Name = "ItemForTimeFrequency";
            this.ItemForTimeFrequency.Size = new System.Drawing.Size(619, 24);
            this.ItemForTimeFrequency.Text = "Time Frequency :";
            this.ItemForTimeFrequency.TextSize = new System.Drawing.Size(118, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 153);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1237, 10);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWarrantyPeriod
            // 
            this.ItemForWarrantyPeriod.Control = this.spnWarrantyPeriod;
            this.ItemForWarrantyPeriod.CustomizationFormText = "Warranty Period";
            this.ItemForWarrantyPeriod.Location = new System.Drawing.Point(618, 95);
            this.ItemForWarrantyPeriod.Name = "ItemForWarrantyPeriod";
            this.ItemForWarrantyPeriod.Size = new System.Drawing.Size(619, 24);
            this.ItemForWarrantyPeriod.Text = "Warranty Period :";
            this.ItemForWarrantyPeriod.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForWarrantyEndDistance
            // 
            this.ItemForWarrantyEndDistance.Control = this.spnWarrantyEndDistance;
            this.ItemForWarrantyEndDistance.CustomizationFormText = "Warranty End Distance";
            this.ItemForWarrantyEndDistance.Location = new System.Drawing.Point(0, 95);
            this.ItemForWarrantyEndDistance.Name = "ItemForWarrantyEndDistance";
            this.ItemForWarrantyEndDistance.Size = new System.Drawing.Size(618, 24);
            this.ItemForWarrantyEndDistance.Text = "Warranty End Distance :";
            this.ItemForWarrantyEndDistance.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForDistanceFrequency
            // 
            this.ItemForDistanceFrequency.Control = this.spnDistanceFrequency;
            this.ItemForDistanceFrequency.CustomizationFormText = "Distance Frequency";
            this.ItemForDistanceFrequency.Location = new System.Drawing.Point(0, 119);
            this.ItemForDistanceFrequency.Name = "ItemForDistanceFrequency";
            this.ItemForDistanceFrequency.Size = new System.Drawing.Size(618, 24);
            this.ItemForDistanceFrequency.Text = "Distance Frequency :";
            this.ItemForDistanceFrequency.TextSize = new System.Drawing.Size(118, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 143);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(1237, 10);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWarrantyEndDate
            // 
            this.ItemForWarrantyEndDate.Control = this.deWarrantyEndDate;
            this.ItemForWarrantyEndDate.CustomizationFormText = "Warranty End Date";
            this.ItemForWarrantyEndDate.Location = new System.Drawing.Point(618, 47);
            this.ItemForWarrantyEndDate.Name = "ItemForWarrantyEndDate";
            this.ItemForWarrantyEndDate.Size = new System.Drawing.Size(619, 24);
            this.ItemForWarrantyEndDate.Text = "Warranty End Date :";
            this.ItemForWarrantyEndDate.TextSize = new System.Drawing.Size(118, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(618, 23);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(619, 24);
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 825);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1261, 1);
            this.layoutControlGroup3.Text = "autoGeneratedGroup1";
            // 
            // sp_AS_11163_Service_ScheduleTableAdapter
            // 
            this.sp_AS_11163_Service_ScheduleTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11054_Service_Data_ItemTableAdapter
            // 
            this.sp_AS_11054_Service_Data_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Service_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Service_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Unit_Dist_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Unit_Dist_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Unit_Time_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Unit_Time_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Service_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Service_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11078_Work_Detail_ItemTableAdapter
            // 
            this.sp_AS_11078_Work_Detail_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Service_Data_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1298, 756);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Service_Data_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Service Data";
            this.Activated += new System.EventHandler(this.frm_AS_Service_Data_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Service_Data_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Service_Data_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueServiceStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11163ServiceScheduleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLServiceStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ceIsCurrentInterval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceDataItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnServiceDueDistance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAlertDistanceUnitsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitDistTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAlertWithinXDistance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAlertTimeUnitsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitTimeTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAlertWithinXTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWorkDetailID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memIntervalScheduleNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueServiceTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLServiceTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDistanceUnitsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnDistanceFrequency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTimeUnitsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnTimeFrequency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnWarrantyPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deWarrantyEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deWarrantyEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnWarrantyEndDistance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memServiceDataNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkDetailID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIntervalScheduleNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertWithinXDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertWithinXTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertDistanceUnitsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertTimeUnitsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDataNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDistanceUnitsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimeUnitsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimeFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarrantyPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarrantyEndDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDistanceFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarrantyEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LookUpEdit lueServiceStatusID;
        private System.Windows.Forms.BindingSource spAS11163ServiceScheduleBindingSource;
        private DevExpress.XtraEditors.SpinEdit spnServiceDueDistance;
        private DevExpress.XtraEditors.DateEdit deServiceDueDate;
        private DevExpress.XtraEditors.LookUpEdit lueAlertDistanceUnitsID;
        private DevExpress.XtraEditors.SpinEdit spnAlertWithinXDistance;
        private DevExpress.XtraEditors.LookUpEdit lueAlertTimeUnitsID;
        private DevExpress.XtraEditors.SpinEdit spnAlertWithinXTime;
        private DevExpress.XtraEditors.LookUpEdit lueWorkDetailID;
        private DevExpress.XtraEditors.MemoEdit memIntervalScheduleNotes;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private System.Windows.Forms.BindingSource spAS11054ServiceDataItemBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueServiceTypeID;
        private DevExpress.XtraEditors.LookUpEdit lueDistanceUnitsID;
        private DevExpress.XtraEditors.SpinEdit spnDistanceFrequency;
        private DevExpress.XtraEditors.LookUpEdit lueTimeUnitsID;
        private DevExpress.XtraEditors.SpinEdit spnTimeFrequency;
        private DevExpress.XtraEditors.SpinEdit spnWarrantyPeriod;
        private DevExpress.XtraEditors.DateEdit deWarrantyEndDate;
        private DevExpress.XtraEditors.SpinEdit spnWarrantyEndDistance;
        private DevExpress.XtraEditors.MemoEdit memServiceDataNotes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11163_Service_ScheduleTableAdapter sp_AS_11163_Service_ScheduleTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Data_ItemTableAdapter sp_AS_11054_Service_Data_ItemTableAdapter;
        private DevExpress.XtraEditors.DataNavigator serviceIntervalNavigator;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceDueDistance;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceDueDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAlertDistanceUnitsID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAlertWithinXDistance;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAlertTimeUnitsID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAlertWithinXTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkDetailID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIntervalScheduleNotes;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarrantyPeriod;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTimeFrequency;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTimeUnitsID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDistanceFrequency;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDistanceUnitsID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarrantyEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarrantyEndDistance;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceDataNotes;
        private System.Windows.Forms.BindingSource spAS11000PLServiceTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Service_TypeTableAdapter sp_AS_11000_PL_Service_TypeTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLUnitDistTypeBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLUnitTimeTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_Dist_TypeTableAdapter sp_AS_11000_PL_Unit_Dist_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_Time_TypeTableAdapter sp_AS_11000_PL_Unit_Time_TypeTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLServiceStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11078WorkDetailItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Service_StatusTableAdapter sp_AS_11000_PL_Service_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter sp_AS_11078_Work_Detail_ItemTableAdapter;
        private DevExpress.XtraEditors.CheckEdit ceIsCurrentInterval;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btnAddLinkedRecord;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;


    }
}

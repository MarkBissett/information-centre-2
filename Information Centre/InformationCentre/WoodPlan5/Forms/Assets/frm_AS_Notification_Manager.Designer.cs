﻿namespace WoodPlan5
{
    partial class frm_AS_Notification_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Notification_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.notificationGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11126NotificationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.notificationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNotificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotificationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriorityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateToRemind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActiveKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMOTDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysUntilMOTDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HyperLinkEdit = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NotesMemoExEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.TextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp_AS_11126_Notification_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11126_Notification_ItemTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiRefreshForm = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.bbiExportExcel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportNewExcel = new DevExpress.XtraBars.BarButtonItem();
            this.colRegistrationPlate = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11126NotificationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HyperLinkEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoExEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.Manager = null;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1362, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 740);
            this.barDockControlBottom.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 714);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1362, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 714);
            // 
            // bbiSave
            // 
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.LargeGlyph")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.Manager = null;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiCancel,
            this.bbiRefreshForm,
            this.bbiExportExcel,
            this.barSubItem1,
            this.bbiExportNewExcel});
            this.barManager1.MaxItemId = 35;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("save_16x16.png", "images/save/save_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/save/save_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "save_16x16.png");
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1675, 521, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1362, 714);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1362, 714);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // notificationGridControl
            // 
            this.notificationGridControl.DataSource = this.spAS11126NotificationItemBindingSource;
            this.notificationGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Append.ImageIndex = 0;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Edit.ImageIndex = 1;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.EndEdit.ImageIndex = 4;
            this.notificationGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Remove.ImageIndex = 2;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "";
            this.notificationGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, false, "", "add")});
            this.notificationGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.notificationGridControl_EmbeddedNavigator_ButtonClick);
            this.notificationGridControl.Location = new System.Drawing.Point(0, 26);
            this.notificationGridControl.MainView = this.notificationGridView;
            this.notificationGridControl.Name = "notificationGridControl";
            this.notificationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.NotesMemoExEdit,
            this.HyperLinkEdit,
            this.TextEdit,
            this.repositoryItemTextEdit1});
            this.notificationGridControl.Size = new System.Drawing.Size(1362, 714);
            this.notificationGridControl.TabIndex = 5;
            this.notificationGridControl.Tag = "Notification Manager";
            this.notificationGridControl.UseEmbeddedNavigator = true;
            this.notificationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.notificationGridView});
            // 
            // spAS11126NotificationItemBindingSource
            // 
            this.spAS11126NotificationItemBindingSource.DataMember = "sp_AS_11126_Notification_Item";
            this.spAS11126NotificationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // notificationGridView
            // 
            this.notificationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNotificationID,
            this.colNotificationTypeID,
            this.colNotificationType,
            this.colLinkedToRecordID,
            this.colLinkedToRecordType,
            this.colEquipmentReference,
            this.colPriorityID,
            this.colPriority,
            this.colRegistrationPlate,
            this.colDateToRemind,
            this.colDateCreated,
            this.colKeeperTypeID,
            this.colKeeperType,
            this.colKeeperID,
            this.colFullName,
            this.colLastKeeper,
            this.colActiveKeeper,
            this.colRegistrationDate,
            this.colMOTDueDate,
            this.colDaysUntilMOTDueDate,
            this.colMessage,
            this.colStatusID,
            this.colStatus,
            this.colMode,
            this.colRecordID});
            this.notificationGridView.GridControl = this.notificationGridControl;
            this.notificationGridView.Name = "notificationGridView";
            this.notificationGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.notificationGridView.OptionsFind.AlwaysVisible = true;
            this.notificationGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.notificationGridView.OptionsLayout.StoreAppearance = true;
            this.notificationGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.notificationGridView.OptionsSelection.MultiSelect = true;
            this.notificationGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.notificationGridView.OptionsView.ColumnAutoWidth = false;
            this.notificationGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.notificationGridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.notificationGridView_CustomDrawCell);
            this.notificationGridView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.notificationGridView_RowStyle);
            this.notificationGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.notificationGridView_PopupMenuShowing);
            this.notificationGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.NotificationGridView_SelectionChanged);
            this.notificationGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.notificationGridView_CustomDrawEmptyForeground);
            this.notificationGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.notificationGridView_CustomFilterDialog);
            this.notificationGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.notificationGridView_MouseUp);
            this.notificationGridView.DoubleClick += new System.EventHandler(this.notificationGridView_DoubleClick);
            // 
            // colNotificationID
            // 
            this.colNotificationID.FieldName = "NotificationID";
            this.colNotificationID.Name = "colNotificationID";
            this.colNotificationID.OptionsColumn.AllowEdit = false;
            this.colNotificationID.OptionsColumn.AllowFocus = false;
            this.colNotificationID.OptionsColumn.ReadOnly = true;
            this.colNotificationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotificationID.Width = 89;
            // 
            // colNotificationTypeID
            // 
            this.colNotificationTypeID.FieldName = "NotificationTypeID";
            this.colNotificationTypeID.Name = "colNotificationTypeID";
            this.colNotificationTypeID.OptionsColumn.AllowEdit = false;
            this.colNotificationTypeID.OptionsColumn.AllowFocus = false;
            this.colNotificationTypeID.OptionsColumn.ReadOnly = true;
            this.colNotificationTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotificationTypeID.Width = 116;
            // 
            // colNotificationType
            // 
            this.colNotificationType.FieldName = "NotificationType";
            this.colNotificationType.Name = "colNotificationType";
            this.colNotificationType.OptionsColumn.AllowEdit = false;
            this.colNotificationType.OptionsColumn.AllowFocus = false;
            this.colNotificationType.OptionsColumn.ReadOnly = true;
            this.colNotificationType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotificationType.Visible = true;
            this.colNotificationType.VisibleIndex = 0;
            this.colNotificationType.Width = 160;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToRecordID.Width = 117;
            // 
            // colLinkedToRecordType
            // 
            this.colLinkedToRecordType.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType.Name = "colLinkedToRecordType";
            this.colLinkedToRecordType.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToRecordType.Width = 130;
            // 
            // colEquipmentReference
            // 
            this.colEquipmentReference.FieldName = "EquipmentReference";
            this.colEquipmentReference.Name = "colEquipmentReference";
            this.colEquipmentReference.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference.Visible = true;
            this.colEquipmentReference.VisibleIndex = 1;
            this.colEquipmentReference.Width = 124;
            // 
            // colPriorityID
            // 
            this.colPriorityID.FieldName = "PriorityID";
            this.colPriorityID.Name = "colPriorityID";
            this.colPriorityID.OptionsColumn.AllowEdit = false;
            this.colPriorityID.OptionsColumn.AllowFocus = false;
            this.colPriorityID.OptionsColumn.ReadOnly = true;
            this.colPriorityID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPriority
            // 
            this.colPriority.FieldName = "Priority";
            this.colPriority.Name = "colPriority";
            this.colPriority.OptionsColumn.AllowEdit = false;
            this.colPriority.OptionsColumn.AllowFocus = false;
            this.colPriority.OptionsColumn.ReadOnly = true;
            this.colPriority.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPriority.Visible = true;
            this.colPriority.VisibleIndex = 2;
            // 
            // colDateToRemind
            // 
            this.colDateToRemind.FieldName = "DateToRemind";
            this.colDateToRemind.Name = "colDateToRemind";
            this.colDateToRemind.OptionsColumn.AllowEdit = false;
            this.colDateToRemind.OptionsColumn.AllowFocus = false;
            this.colDateToRemind.OptionsColumn.ReadOnly = true;
            this.colDateToRemind.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateToRemind.Visible = true;
            this.colDateToRemind.VisibleIndex = 3;
            this.colDateToRemind.Width = 97;
            // 
            // colDateCreated
            // 
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 4;
            this.colDateCreated.Width = 86;
            // 
            // colKeeperTypeID
            // 
            this.colKeeperTypeID.FieldName = "KeeperTypeID";
            this.colKeeperTypeID.Name = "colKeeperTypeID";
            this.colKeeperTypeID.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperTypeID.Width = 96;
            // 
            // colKeeperType
            // 
            this.colKeeperType.FieldName = "KeeperType";
            this.colKeeperType.Name = "colKeeperType";
            this.colKeeperType.OptionsColumn.AllowEdit = false;
            this.colKeeperType.OptionsColumn.AllowFocus = false;
            this.colKeeperType.OptionsColumn.ReadOnly = true;
            this.colKeeperType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperType.Visible = true;
            this.colKeeperType.VisibleIndex = 6;
            this.colKeeperType.Width = 157;
            // 
            // colKeeperID
            // 
            this.colKeeperID.FieldName = "KeeperID";
            this.colKeeperID.Name = "colKeeperID";
            this.colKeeperID.OptionsColumn.AllowEdit = false;
            this.colKeeperID.OptionsColumn.AllowFocus = false;
            this.colKeeperID.OptionsColumn.ReadOnly = true;
            this.colKeeperID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colFullName
            // 
            this.colFullName.FieldName = "FullName";
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.AllowEdit = false;
            this.colFullName.OptionsColumn.AllowFocus = false;
            this.colFullName.OptionsColumn.ReadOnly = true;
            this.colFullName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 7;
            this.colFullName.Width = 182;
            // 
            // colLastKeeper
            // 
            this.colLastKeeper.FieldName = "LastKeeper";
            this.colLastKeeper.Name = "colLastKeeper";
            this.colLastKeeper.OptionsColumn.AllowEdit = false;
            this.colLastKeeper.OptionsColumn.AllowFocus = false;
            this.colLastKeeper.OptionsColumn.ReadOnly = true;
            this.colLastKeeper.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastKeeper.Visible = true;
            this.colLastKeeper.VisibleIndex = 13;
            this.colLastKeeper.Width = 213;
            // 
            // colActiveKeeper
            // 
            this.colActiveKeeper.FieldName = "ActiveKeeper";
            this.colActiveKeeper.Name = "colActiveKeeper";
            this.colActiveKeeper.OptionsColumn.AllowEdit = false;
            this.colActiveKeeper.OptionsColumn.AllowFocus = false;
            this.colActiveKeeper.OptionsColumn.ReadOnly = true;
            this.colActiveKeeper.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActiveKeeper.Visible = true;
            this.colActiveKeeper.VisibleIndex = 12;
            this.colActiveKeeper.Width = 204;
            // 
            // colRegistrationDate
            // 
            this.colRegistrationDate.FieldName = "RegistrationDate";
            this.colRegistrationDate.Name = "colRegistrationDate";
            this.colRegistrationDate.OptionsColumn.AllowEdit = false;
            this.colRegistrationDate.OptionsColumn.AllowFocus = false;
            this.colRegistrationDate.OptionsColumn.ReadOnly = true;
            this.colRegistrationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistrationDate.Visible = true;
            this.colRegistrationDate.VisibleIndex = 9;
            this.colRegistrationDate.Width = 85;
            // 
            // colMOTDueDate
            // 
            this.colMOTDueDate.FieldName = "MOTDueDate";
            this.colMOTDueDate.Name = "colMOTDueDate";
            this.colMOTDueDate.OptionsColumn.AllowEdit = false;
            this.colMOTDueDate.OptionsColumn.AllowFocus = false;
            this.colMOTDueDate.OptionsColumn.ReadOnly = true;
            this.colMOTDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMOTDueDate.Visible = true;
            this.colMOTDueDate.VisibleIndex = 10;
            this.colMOTDueDate.Width = 85;
            // 
            // colDaysUntilMOTDueDate
            // 
            this.colDaysUntilMOTDueDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDaysUntilMOTDueDate.FieldName = "DaysUntilMOTDueDate";
            this.colDaysUntilMOTDueDate.Name = "colDaysUntilMOTDueDate";
            this.colDaysUntilMOTDueDate.OptionsColumn.AllowEdit = false;
            this.colDaysUntilMOTDueDate.OptionsColumn.AllowFocus = false;
            this.colDaysUntilMOTDueDate.OptionsColumn.ReadOnly = true;
            this.colDaysUntilMOTDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDaysUntilMOTDueDate.Visible = true;
            this.colDaysUntilMOTDueDate.VisibleIndex = 11;
            this.colDaysUntilMOTDueDate.Width = 97;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "######0 Days";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colMessage
            // 
            this.colMessage.ColumnEdit = this.HyperLinkEdit;
            this.colMessage.FieldName = "Message";
            this.colMessage.Name = "colMessage";
            this.colMessage.OptionsColumn.ReadOnly = true;
            this.colMessage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMessage.Visible = true;
            this.colMessage.VisibleIndex = 14;
            this.colMessage.Width = 797;
            // 
            // HyperLinkEdit
            // 
            this.HyperLinkEdit.AutoHeight = false;
            this.HyperLinkEdit.Name = "HyperLinkEdit";
            this.HyperLinkEdit.SingleClick = true;
            this.HyperLinkEdit.Click += new System.EventHandler(this.HyperLinkEdit_Click);
            // 
            // colStatusID
            // 
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 5;
            this.colStatus.Width = 156;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // NotesMemoExEdit
            // 
            this.NotesMemoExEdit.AutoHeight = false;
            this.NotesMemoExEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NotesMemoExEdit.Name = "NotesMemoExEdit";
            this.NotesMemoExEdit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.NotesMemoExEdit.ShowIcon = false;
            // 
            // TextEdit
            // 
            this.TextEdit.AutoHeight = false;
            this.TextEdit.Name = "TextEdit";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // bbiCancel
            // 
            this.bbiCancel.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiCancel.Caption = "Cancel and Close";
            this.bbiCancel.DropDownEnabled = false;
            this.bbiCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCancel.Glyph")));
            this.bbiCancel.Id = 30;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11126_Notification_ItemTableAdapter
            // 
            this.sp_AS_11126_Notification_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 2";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(1430, 165);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshForm, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1, true)});
            this.bar2.Text = "Custom 2";
            // 
            // bbiRefreshForm
            // 
            this.bbiRefreshForm.Caption = "Refresh notification Records";
            this.bbiRefreshForm.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefreshForm.Glyph")));
            this.bbiRefreshForm.Id = 31;
            this.bbiRefreshForm.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiRefreshForm.LargeGlyph")));
            this.bbiRefreshForm.Name = "bbiRefreshForm";
            this.bbiRefreshForm.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshForm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshForm_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Export";
            this.barSubItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem1.Glyph")));
            this.barSubItem1.Id = 33;
            this.barSubItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barSubItem1.LargeGlyph")));
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportNewExcel, true)});
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bbiExportExcel
            // 
            this.bbiExportExcel.Caption = "Export To Excel (2003)";
            this.bbiExportExcel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExportExcel.Glyph")));
            this.bbiExportExcel.Id = 32;
            this.bbiExportExcel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiExportExcel.LargeGlyph")));
            this.bbiExportExcel.Name = "bbiExportExcel";
            this.bbiExportExcel.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiExportExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportExcel_ItemClick);
            // 
            // bbiExportNewExcel
            // 
            this.bbiExportNewExcel.Caption = "Export To Excel";
            this.bbiExportNewExcel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExportNewExcel.Glyph")));
            this.bbiExportNewExcel.Id = 34;
            this.bbiExportNewExcel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiExportNewExcel.LargeGlyph")));
            this.bbiExportNewExcel.Name = "bbiExportNewExcel";
            this.bbiExportNewExcel.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiExportNewExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportNewExcel_ItemClick);
            // 
            // colRegistrationPlate
            // 
            this.colRegistrationPlate.FieldName = "RegistrationPlate";
            this.colRegistrationPlate.Name = "colRegistrationPlate";
            this.colRegistrationPlate.OptionsColumn.AllowEdit = false;
            this.colRegistrationPlate.OptionsColumn.AllowFocus = false;
            this.colRegistrationPlate.OptionsColumn.ReadOnly = true;
            this.colRegistrationPlate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistrationPlate.Visible = true;
            this.colRegistrationPlate.VisibleIndex = 8;
            this.colRegistrationPlate.Width = 93;
            // 
            // frm_AS_Notification_Manager
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1362, 740);
            this.Controls.Add(this.notificationGridControl);
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Notification_Manager";
            this.Text = "Notification Manager";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.frm_AS_Notification_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Notification_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            this.Controls.SetChildIndex(this.notificationGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11126NotificationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HyperLinkEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NotesMemoExEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl notificationGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView notificationGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit NotesMemoExEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit HyperLinkEdit;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit TextEdit;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource spAS11126NotificationItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationID;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colPriorityID;
        private DevExpress.XtraGrid.Columns.GridColumn colPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colDateToRemind;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperID;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11126_Notification_ItemTableAdapter sp_AS_11126_Notification_ItemTableAdapter;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshForm;
        private DevExpress.XtraBars.BarButtonItem bbiExportExcel;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem bbiExportNewExcel;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMOTDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysUntilMOTDueDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationPlate;
    }
}

﻿namespace WoodPlan5
{
    partial class frm_AS_Rental_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Rental_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.spAS11011EquipmentCategoryListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.equipmentDataLayoutControl = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.dataNavigator2 = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11108RentalDetailsItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.memIntervalNotes = new DevExpress.XtraEditors.MemoEdit();
            this.equipmentChildTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.roadTaxTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.roadTaxGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11096RoadTaxItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.roadTaxGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRoadTaxID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaxExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTaxPeriodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTaxPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.keeperTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.keeperGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11044KeeperAllocationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.keeperGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKeeperAllocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueKeeperNotes = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.transactionTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.transactionGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11041TransactionItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.transactionsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransactionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedOnExchequer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetTransactionPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceOnExchequer = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.purposeTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.purposeGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11084PurposeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.purposeGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentPurposeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurpose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurposeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.fuelCardTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.fuelCardGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11089FuelCardItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fuelCardGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFuelCardID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCardNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationMark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeper1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAllocationID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCardStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCardStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit19 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.trackerTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.trackerGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11023TrackerListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.trackerGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTrackerInformationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrackerVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrackerReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMake1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOdometerMetres = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOdometerMiles = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMPG = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatestStartJourney = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatestEndJourney = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedKPH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedMPH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlaceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostSect = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGeofenceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrict = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDriverEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.billingTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.billingGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11050DepreciationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.billingGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepreciationID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNarrative1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceSheetCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfitLossCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepreciationAmount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPreviousValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowEdit1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.depreciationTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.depreciationGridControl = new DevExpress.XtraGrid.GridControl();
            this.depreciationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepreciationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNarrative = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceSheetCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfitLossCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepreciationAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEditDep = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPreviousValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowEdit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.coverTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.coverGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11075CoverItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.coverGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCoverID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEquipmentReference16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoverType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoverTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolicy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnnualCoverCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRenewalDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExcess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.serviceIntervalTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.serviceIntervalGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11054ServiceDataItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.serviceIntervalGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colServiceDataID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceIntervalScheduleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceUnitsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistanceFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeUnitsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarrantyPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarrantyEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWarrantyEndDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceDataNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceDueDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlertWithinXDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlertWithinXTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIntervalScheduleNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsCurrentInterval = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.workDetailTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.workDetailGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11078WorkDetailItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.workDetailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkDetailID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentMileage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkCompletionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceIntervalID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.incidentTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.incidentGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11081IncidentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.incidentGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateHappened = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRepairDueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRepairDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeverityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeverity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWitness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAtFaultID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAtFault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFollowUpDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNotes4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.incidentNotesMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colMode3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.speedingTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.speedingGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11105SpeedingItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.speedingGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSpeedingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedMPH1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravelTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlaceName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrict1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReported = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceReported = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMode17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.equipmentDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11102RentalItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.lueEquipmentCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.lueMakeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11014MakeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueModelID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11017ModelListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLEquipmentOwnershipStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueSupplierID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11020SupplierListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtUniqueReference = new DevExpress.XtraEditors.TextEdit();
            this.memRentalNotes = new DevExpress.XtraEditors.MemoEdit();
            this.txtModelSpecifications = new DevExpress.XtraEditors.TextEdit();
            this.lueRentalCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLRentalCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueRentalStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLRentalStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueRentalSpecificationID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLRentalSpecificationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnRate = new DevExpress.XtraEditors.SpinEdit();
            this.txtCustomerReference = new DevExpress.XtraEditors.TextEdit();
            this.deStartDate = new DevExpress.XtraEditors.DateEdit();
            this.deActualEndDate = new DevExpress.XtraEditors.DateEdit();
            this.deEndDate = new DevExpress.XtraEditors.DateEdit();
            this.ceSetAsMain = new DevExpress.XtraEditors.CheckEdit();
            this.spinEditEngineSize = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditEmissions = new DevExpress.XtraEditors.SpinEdit();
            this.dateEditRegistrationDate = new DevExpress.XtraEditors.DateEdit();
            this.lueFuelTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLFuelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spinEditListPrice = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.linkedChildDataGroup = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.rentalEquipControlGroup = new DevExpress.XtraLayout.TabbedControlGroup();
            this.rentalLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnershipStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMakeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForModelID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForModelSpecifications = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentCategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUniqueReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSupplierID = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.itemForEngineSize = new DevExpress.XtraLayout.LayoutControlItem();
            this.itemForEmissions = new DevExpress.XtraLayout.LayoutControlItem();
            this.itemForRegistrationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFuelTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForListPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.rentalIntervalControlGroup = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup4 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRentalStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRentalCategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSetAsMain = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRentalSpecificationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForActualEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.spAS11066DepreciationSettingsItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLEquipmentAvailabilityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spAS11000FleetManagerDummyScreenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnPopUp1Ok = new DevExpress.XtraEditors.SimpleButton();
            this.sp_AS_11011_Equipment_Category_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11011_Equipment_Category_ListTableAdapter();
            this.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter();
            this.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter();
            this.sp_AS_11014_Make_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter();
            this.sp_AS_11017_Model_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter();
            this.sp_AS_11020_Supplier_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter();
            this.sp_AS_11023_Tracker_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11023_Tracker_ListTableAdapter();
            this.spAS11000DuplicateMakeModelSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter();
            this.spAS11000DuplicateSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_Duplicate_SearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter();
            this.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11066_Depreciation_Settings_ItemTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp_AS_11000_FleetManagerDummyScreenTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_FleetManagerDummyScreenTableAdapter();
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter();
            this.sp_AS_11041_Transaction_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter();
            this.sp_AS_11050_Depreciation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11050_Depreciation_ItemTableAdapter();
            this.sp_AS_11075_Cover_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11075_Cover_ItemTableAdapter();
            this.sp_AS_11078_Work_Detail_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter();
            this.sp_AS_11081_Incident_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter();
            this.sp_AS_11084_Purpose_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11084_Purpose_ItemTableAdapter();
            this.sp_AS_11089_Fuel_Card_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11089_Fuel_Card_ItemTableAdapter();
            this.sp_AS_11105_Speeding_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11105_Speeding_ItemTableAdapter();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp_AS_11044_Keeper_Allocation_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ListTableAdapter();
            this.sp_AS_11096_Road_Tax_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11096_Road_Tax_ItemTableAdapter();
            this.sp_AS_11102_Rental_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11102_Rental_ItemTableAdapter();
            this.sp_AS_11108_Rental_Details_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11108_Rental_Details_ItemTableAdapter();
            this.sp_AS_11000_PL_Rental_CategoryTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_CategoryTableAdapter();
            this.sp_AS_11000_PL_Rental_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_StatusTableAdapter();
            this.sp_AS_11000_PL_Rental_SpecificationTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_SpecificationTableAdapter();
            this.sp_AS_11000_PL_Incident_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_StatusTableAdapter();
            this.sp_AS_11000_PL_Incident_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_TypeTableAdapter();
            this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_Repair_DueTableAdapter();
            this.dataSet_AS_DataEntry1 = new WoodPlan5.DataSet_AS_DataEntry();
            this.spAS11000PLSeverityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_PL_SeverityTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_SeverityTableAdapter();
            this.spAS11000PLLegalActionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_PL_Legal_ActionTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Legal_ActionTableAdapter();
            this.spAS11000PLKeeperAtFaultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_At_FaultTableAdapter();
            this.sp_AS_11054_Service_Data_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Data_ItemTableAdapter();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem5 = new DevExpress.XtraBars.BarStaticItem();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControl2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.barStaticItem6 = new DevExpress.XtraBars.BarStaticItem();
            this.bar6 = new DevExpress.XtraBars.Bar();
            this.barStaticItem7 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem8 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem9 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem10 = new DevExpress.XtraBars.BarStaticItem();
            this.bar7 = new DevExpress.XtraBars.Bar();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.bar8 = new DevExpress.XtraBars.Bar();
            this.barEditItem3 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControl3 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.barStaticItem11 = new DevExpress.XtraBars.BarStaticItem();
            this.bar9 = new DevExpress.XtraBars.Bar();
            this.barStaticItem12 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem13 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem14 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem15 = new DevExpress.XtraBars.BarStaticItem();
            this.bar10 = new DevExpress.XtraBars.Bar();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.bar11 = new DevExpress.XtraBars.Bar();
            this.barEditItem4 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControl4 = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.barStaticItem16 = new DevExpress.XtraBars.BarStaticItem();
            this.sp_AS_11000_PL_FuelTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_FuelTableAdapter();
            this.bar12 = new DevExpress.XtraBars.Bar();
            this.barStaticItem17 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem18 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem19 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem20 = new DevExpress.XtraBars.BarStaticItem();
            this.bar13 = new DevExpress.XtraBars.Bar();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem21 = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11011EquipmentCategoryListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataLayoutControl)).BeginInit();
            this.equipmentDataLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11108RentalDetailsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memIntervalNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentChildTabControl)).BeginInit();
            this.equipmentChildTabControl.SuspendLayout();
            this.roadTaxTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11096RoadTaxItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit4)).BeginInit();
            this.keeperTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperNotes)).BeginInit();
            this.transactionTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transactionGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOnExchequer)).BeginInit();
            this.purposeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11084PurposeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).BeginInit();
            this.fuelCardTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11089FuelCardItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit19)).BeginInit();
            this.trackerTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11023TrackerListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            this.billingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.billingGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11050DepreciationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.depreciationTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEditDep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            this.coverTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.coverGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11075CoverItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coverGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit1)).BeginInit();
            this.serviceIntervalTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serviceIntervalGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceDataItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceIntervalGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            this.workDetailTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).BeginInit();
            this.incidentTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentNotesMemoEdit)).BeginInit();
            this.speedingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11105SpeedingItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceReported)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11102RentalItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEquipmentCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMakeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11014MakeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueModelID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentOwnershipStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUniqueReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memRentalNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModelSpecifications.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalSpecificationID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalSpecificationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActualEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActualEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSetAsMain.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEngineSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEmissions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditRegistrationDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditRegistrationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueFuelTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLFuelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditListPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkedChildDataGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentalEquipControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentalLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnershipStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMakeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModelID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModelSpecifications)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUniqueReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemForEngineSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemForEmissions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemForRegistrationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFuelTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForListPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentalIntervalControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSetAsMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalSpecificationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11066DepreciationSettingsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentAvailabilityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000FleetManagerDummyScreenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateMakeModelSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSeverityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLLegalActionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperAtFaultBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).BeginInit();
            this.popupContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl3)).BeginInit();
            this.popupContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl4)).BeginInit();
            this.popupContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 740);
            this.barDockControlBottom.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 740);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1362, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 740);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // spAS11011EquipmentCategoryListBindingSource
            // 
            this.spAS11011EquipmentCategoryListBindingSource.DataMember = "sp_AS_11011_Equipment_Category_List";
            this.spAS11011EquipmentCategoryListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // equipmentDataLayoutControl
            // 
            this.equipmentDataLayoutControl.Controls.Add(this.dataNavigator2);
            this.equipmentDataLayoutControl.Controls.Add(this.memIntervalNotes);
            this.equipmentDataLayoutControl.Controls.Add(this.equipmentChildTabControl);
            this.equipmentDataLayoutControl.Controls.Add(this.equipmentDataNavigator);
            this.equipmentDataLayoutControl.Controls.Add(this.txtEquipmentReference);
            this.equipmentDataLayoutControl.Controls.Add(this.lueEquipmentCategoryID);
            this.equipmentDataLayoutControl.Controls.Add(this.lueMakeID);
            this.equipmentDataLayoutControl.Controls.Add(this.lueModelID);
            this.equipmentDataLayoutControl.Controls.Add(this.lueStatusID);
            this.equipmentDataLayoutControl.Controls.Add(this.lueSupplierID);
            this.equipmentDataLayoutControl.Controls.Add(this.txtUniqueReference);
            this.equipmentDataLayoutControl.Controls.Add(this.memRentalNotes);
            this.equipmentDataLayoutControl.Controls.Add(this.txtModelSpecifications);
            this.equipmentDataLayoutControl.Controls.Add(this.lueRentalCategoryID);
            this.equipmentDataLayoutControl.Controls.Add(this.lueRentalStatusID);
            this.equipmentDataLayoutControl.Controls.Add(this.lueRentalSpecificationID);
            this.equipmentDataLayoutControl.Controls.Add(this.spnRate);
            this.equipmentDataLayoutControl.Controls.Add(this.txtCustomerReference);
            this.equipmentDataLayoutControl.Controls.Add(this.deStartDate);
            this.equipmentDataLayoutControl.Controls.Add(this.deActualEndDate);
            this.equipmentDataLayoutControl.Controls.Add(this.deEndDate);
            this.equipmentDataLayoutControl.Controls.Add(this.ceSetAsMain);
            this.equipmentDataLayoutControl.Controls.Add(this.spinEditEngineSize);
            this.equipmentDataLayoutControl.Controls.Add(this.spinEditEmissions);
            this.equipmentDataLayoutControl.Controls.Add(this.dateEditRegistrationDate);
            this.equipmentDataLayoutControl.Controls.Add(this.lueFuelTypeID);
            this.equipmentDataLayoutControl.Controls.Add(this.spinEditListPrice);
            this.equipmentDataLayoutControl.DataSource = this.spAS11102RentalItemBindingSource;
            this.equipmentDataLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.equipmentDataLayoutControl.Location = new System.Drawing.Point(0, 0);
            this.equipmentDataLayoutControl.Name = "equipmentDataLayoutControl";
            this.equipmentDataLayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(0, 41, 572, 672);
            this.equipmentDataLayoutControl.Root = this.layoutControlGroup1;
            this.equipmentDataLayoutControl.Size = new System.Drawing.Size(1362, 740);
            this.equipmentDataLayoutControl.TabIndex = 4;
            this.equipmentDataLayoutControl.Text = "dataLayoutControl1";
            // 
            // dataNavigator2
            // 
            this.dataNavigator2.Buttons.Append.Visible = false;
            this.dataNavigator2.Buttons.CancelEdit.Visible = false;
            this.dataNavigator2.Buttons.EndEdit.Visible = false;
            this.dataNavigator2.Buttons.Remove.Visible = false;
            this.dataNavigator2.DataSource = this.spAS11108RentalDetailsItemBindingSource;
            this.dataNavigator2.Location = new System.Drawing.Point(630, 381);
            this.dataNavigator2.Name = "dataNavigator2";
            this.dataNavigator2.Size = new System.Drawing.Size(235, 19);
            this.dataNavigator2.StyleController = this.equipmentDataLayoutControl;
            this.dataNavigator2.TabIndex = 121;
            this.dataNavigator2.Text = "dataNavigator2";
            this.dataNavigator2.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator2.TextStringFormat = "Rental Interval {0} of {1}";
            // 
            // spAS11108RentalDetailsItemBindingSource
            // 
            this.spAS11108RentalDetailsItemBindingSource.DataMember = "sp_AS_11108_Rental_Details_Item";
            this.spAS11108RentalDetailsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // memIntervalNotes
            // 
            this.memIntervalNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "Notes", true));
            this.memIntervalNotes.Location = new System.Drawing.Point(642, 440);
            this.memIntervalNotes.MenuManager = this.barManager1;
            this.memIntervalNotes.Name = "memIntervalNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memIntervalNotes, true);
            this.memIntervalNotes.Size = new System.Drawing.Size(667, 163);
            this.scSpellChecker.SetSpellCheckerOptions(this.memIntervalNotes, optionsSpelling1);
            this.memIntervalNotes.StyleController = this.equipmentDataLayoutControl;
            this.memIntervalNotes.TabIndex = 154;
            this.memIntervalNotes.Tag = "Interval Notes";
            // 
            // equipmentChildTabControl
            // 
            this.equipmentChildTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.equipmentChildTabControl.Location = new System.Drawing.Point(26, 664);
            this.equipmentChildTabControl.Name = "equipmentChildTabControl";
            this.equipmentChildTabControl.SelectedTabPage = this.roadTaxTabPage;
            this.equipmentChildTabControl.Size = new System.Drawing.Size(1295, 226);
            this.equipmentChildTabControl.TabIndex = 6;
            this.equipmentChildTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.keeperTabPage,
            this.transactionTabPage,
            this.purposeTabPage,
            this.fuelCardTabPage,
            this.trackerTabPage,
            this.roadTaxTabPage,
            this.billingTabPage,
            this.depreciationTabPage,
            this.coverTabPage,
            this.serviceIntervalTabPage,
            this.workDetailTabPage,
            this.incidentTabPage,
            this.speedingTabPage});
            // 
            // roadTaxTabPage
            // 
            this.roadTaxTabPage.Controls.Add(this.roadTaxGridControl);
            this.roadTaxTabPage.Name = "roadTaxTabPage";
            this.roadTaxTabPage.Size = new System.Drawing.Size(1290, 200);
            this.roadTaxTabPage.Text = "Road Tax";
            // 
            // roadTaxGridControl
            // 
            this.roadTaxGridControl.DataSource = this.spAS11096RoadTaxItemBindingSource;
            this.roadTaxGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.roadTaxGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.roadTaxGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.roadTaxGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.roadTaxGridControl.Location = new System.Drawing.Point(0, 0);
            this.roadTaxGridControl.MainView = this.roadTaxGridView;
            this.roadTaxGridControl.MenuManager = this.barManager1;
            this.roadTaxGridControl.Name = "roadTaxGridControl";
            this.roadTaxGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.moneyTextEdit4});
            this.roadTaxGridControl.Size = new System.Drawing.Size(1290, 200);
            this.roadTaxGridControl.TabIndex = 1;
            this.roadTaxGridControl.UseEmbeddedNavigator = true;
            this.roadTaxGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.roadTaxGridView});
            // 
            // spAS11096RoadTaxItemBindingSource
            // 
            this.spAS11096RoadTaxItemBindingSource.DataMember = "sp_AS_11096_Road_Tax_Item";
            this.spAS11096RoadTaxItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("country_16x16.png", "images/miscellaneous/country_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/country_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "country_16x16.png");
            this.imageCollection1.InsertGalleryImage("insert_16x16.png", "images/actions/insert_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/insert_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "insert_16x16.png");
            // 
            // roadTaxGridView
            // 
            this.roadTaxGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRoadTaxID,
            this.colEquipmentReference12,
            this.colEquipmentID14,
            this.colTaxExpiryDate,
            this.colAmount,
            this.colTaxPeriodID,
            this.colTaxPeriod,
            this.colMode13,
            this.colRecordID13});
            this.roadTaxGridView.GridControl = this.roadTaxGridControl;
            this.roadTaxGridView.Name = "roadTaxGridView";
            this.roadTaxGridView.OptionsCustomization.AllowFilter = false;
            this.roadTaxGridView.OptionsCustomization.AllowGroup = false;
            this.roadTaxGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.roadTaxGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.roadTaxGridView.OptionsLayout.StoreAppearance = true;
            this.roadTaxGridView.OptionsSelection.MultiSelect = true;
            this.roadTaxGridView.OptionsView.ColumnAutoWidth = false;
            this.roadTaxGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.roadTaxGridView.OptionsView.ShowGroupPanel = false;
            this.roadTaxGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.roadTaxGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.roadTaxGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.roadTaxGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.roadTaxGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.roadTaxGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.roadTaxGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colRoadTaxID
            // 
            this.colRoadTaxID.FieldName = "RoadTaxID";
            this.colRoadTaxID.Name = "colRoadTaxID";
            this.colRoadTaxID.OptionsColumn.AllowEdit = false;
            this.colRoadTaxID.OptionsColumn.AllowFocus = false;
            this.colRoadTaxID.OptionsColumn.ReadOnly = true;
            this.colRoadTaxID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRoadTaxID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRoadTaxID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadTaxID.Width = 157;
            // 
            // colEquipmentReference12
            // 
            this.colEquipmentReference12.FieldName = "EquipmentReference";
            this.colEquipmentReference12.Name = "colEquipmentReference12";
            this.colEquipmentReference12.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference12.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference12.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference12.Visible = true;
            this.colEquipmentReference12.VisibleIndex = 0;
            this.colEquipmentReference12.Width = 187;
            // 
            // colEquipmentID14
            // 
            this.colEquipmentID14.FieldName = "EquipmentID";
            this.colEquipmentID14.Name = "colEquipmentID14";
            this.colEquipmentID14.OptionsColumn.AllowEdit = false;
            this.colEquipmentID14.OptionsColumn.AllowFocus = false;
            this.colEquipmentID14.OptionsColumn.ReadOnly = true;
            this.colEquipmentID14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID14.Width = 154;
            // 
            // colTaxExpiryDate
            // 
            this.colTaxExpiryDate.FieldName = "TaxExpiryDate";
            this.colTaxExpiryDate.Name = "colTaxExpiryDate";
            this.colTaxExpiryDate.OptionsColumn.AllowEdit = false;
            this.colTaxExpiryDate.OptionsColumn.AllowFocus = false;
            this.colTaxExpiryDate.OptionsColumn.ReadOnly = true;
            this.colTaxExpiryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaxExpiryDate.Visible = true;
            this.colTaxExpiryDate.VisibleIndex = 1;
            this.colTaxExpiryDate.Width = 203;
            // 
            // colAmount
            // 
            this.colAmount.ColumnEdit = this.moneyTextEdit4;
            this.colAmount.FieldName = "Amount";
            this.colAmount.Name = "colAmount";
            this.colAmount.OptionsColumn.AllowEdit = false;
            this.colAmount.OptionsColumn.AllowFocus = false;
            this.colAmount.OptionsColumn.ReadOnly = true;
            this.colAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAmount.Visible = true;
            this.colAmount.VisibleIndex = 2;
            this.colAmount.Width = 222;
            // 
            // moneyTextEdit4
            // 
            this.moneyTextEdit4.AutoHeight = false;
            this.moneyTextEdit4.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit4.Name = "moneyTextEdit4";
            // 
            // colTaxPeriodID
            // 
            this.colTaxPeriodID.FieldName = "TaxPeriodID";
            this.colTaxPeriodID.Name = "colTaxPeriodID";
            this.colTaxPeriodID.OptionsColumn.AllowEdit = false;
            this.colTaxPeriodID.OptionsColumn.AllowFocus = false;
            this.colTaxPeriodID.OptionsColumn.ReadOnly = true;
            this.colTaxPeriodID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTaxPeriodID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTaxPeriodID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaxPeriodID.Width = 77;
            // 
            // colTaxPeriod
            // 
            this.colTaxPeriod.FieldName = "TaxPeriod";
            this.colTaxPeriod.Name = "colTaxPeriod";
            this.colTaxPeriod.OptionsColumn.AllowEdit = false;
            this.colTaxPeriod.OptionsColumn.AllowFocus = false;
            this.colTaxPeriod.OptionsColumn.ReadOnly = true;
            this.colTaxPeriod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTaxPeriod.Visible = true;
            this.colTaxPeriod.VisibleIndex = 3;
            this.colTaxPeriod.Width = 367;
            // 
            // colMode13
            // 
            this.colMode13.FieldName = "Mode";
            this.colMode13.Name = "colMode13";
            this.colMode13.OptionsColumn.AllowEdit = false;
            this.colMode13.OptionsColumn.AllowFocus = false;
            this.colMode13.OptionsColumn.ReadOnly = true;
            this.colMode13.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode13.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID13
            // 
            this.colRecordID13.FieldName = "RecordID";
            this.colRecordID13.Name = "colRecordID13";
            this.colRecordID13.OptionsColumn.AllowEdit = false;
            this.colRecordID13.OptionsColumn.AllowFocus = false;
            this.colRecordID13.OptionsColumn.ReadOnly = true;
            this.colRecordID13.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID13.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // keeperTabPage
            // 
            this.keeperTabPage.AutoScroll = true;
            this.keeperTabPage.Controls.Add(this.keeperGridControl);
            this.keeperTabPage.Name = "keeperTabPage";
            this.keeperTabPage.Size = new System.Drawing.Size(1290, 200);
            this.keeperTabPage.Text = "Keeper Allocations";
            // 
            // keeperGridControl
            // 
            this.keeperGridControl.DataSource = this.spAS11044KeeperAllocationItemBindingSource;
            this.keeperGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.keeperGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.keeperGridControl.Location = new System.Drawing.Point(0, 0);
            this.keeperGridControl.MainView = this.keeperGridView;
            this.keeperGridControl.MenuManager = this.barManager1;
            this.keeperGridControl.Name = "keeperGridControl";
            this.keeperGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lueKeeperNotes});
            this.keeperGridControl.Size = new System.Drawing.Size(1290, 200);
            this.keeperGridControl.TabIndex = 2;
            this.keeperGridControl.UseEmbeddedNavigator = true;
            this.keeperGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.keeperGridView});
            // 
            // spAS11044KeeperAllocationItemBindingSource
            // 
            this.spAS11044KeeperAllocationItemBindingSource.DataMember = "sp_AS_11044_Keeper_Allocation_Item";
            this.spAS11044KeeperAllocationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // keeperGridView
            // 
            this.keeperGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKeeperAllocationID,
            this.colEquipmentReference9,
            this.colKeeperTypeID,
            this.colKeeperType,
            this.colKeeperID,
            this.colKeeper,
            this.colEquipmentID11,
            this.colAllocationStatusID,
            this.colAllocationStatus,
            this.colAllocationDate,
            this.colAllocationEndDate,
            this.colNotes1,
            this.colMode7,
            this.colRecordID7});
            this.keeperGridView.GridControl = this.keeperGridControl;
            this.keeperGridView.Name = "keeperGridView";
            this.keeperGridView.OptionsCustomization.AllowGroup = false;
            this.keeperGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.keeperGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.keeperGridView.OptionsLayout.StoreAppearance = true;
            this.keeperGridView.OptionsSelection.MultiSelect = true;
            this.keeperGridView.OptionsView.ColumnAutoWidth = false;
            this.keeperGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.keeperGridView.OptionsView.ShowGroupPanel = false;
            this.keeperGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.keeperGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.keeperGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.keeperGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.keeperGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.keeperGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.keeperGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colKeeperAllocationID
            // 
            this.colKeeperAllocationID.FieldName = "KeeperAllocationID";
            this.colKeeperAllocationID.Name = "colKeeperAllocationID";
            this.colKeeperAllocationID.OptionsColumn.AllowEdit = false;
            this.colKeeperAllocationID.OptionsColumn.AllowFocus = false;
            this.colKeeperAllocationID.OptionsColumn.ReadOnly = true;
            this.colKeeperAllocationID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAllocationID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAllocationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAllocationID.Width = 119;
            // 
            // colEquipmentReference9
            // 
            this.colEquipmentReference9.FieldName = "EquipmentReference";
            this.colEquipmentReference9.Name = "colEquipmentReference9";
            this.colEquipmentReference9.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference9.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference9.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference9.Visible = true;
            this.colEquipmentReference9.VisibleIndex = 0;
            this.colEquipmentReference9.Width = 125;
            // 
            // colKeeperTypeID
            // 
            this.colKeeperTypeID.FieldName = "KeeperTypeID";
            this.colKeeperTypeID.Name = "colKeeperTypeID";
            this.colKeeperTypeID.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperTypeID.Width = 97;
            // 
            // colKeeperType
            // 
            this.colKeeperType.FieldName = "KeeperType";
            this.colKeeperType.Name = "colKeeperType";
            this.colKeeperType.OptionsColumn.AllowEdit = false;
            this.colKeeperType.OptionsColumn.AllowFocus = false;
            this.colKeeperType.OptionsColumn.ReadOnly = true;
            this.colKeeperType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperType.Visible = true;
            this.colKeeperType.VisibleIndex = 1;
            this.colKeeperType.Width = 137;
            // 
            // colKeeperID
            // 
            this.colKeeperID.FieldName = "KeeperID";
            this.colKeeperID.Name = "colKeeperID";
            this.colKeeperID.OptionsColumn.AllowEdit = false;
            this.colKeeperID.OptionsColumn.AllowFocus = false;
            this.colKeeperID.OptionsColumn.ReadOnly = true;
            this.colKeeperID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colKeeper
            // 
            this.colKeeper.FieldName = "Keeper";
            this.colKeeper.Name = "colKeeper";
            this.colKeeper.OptionsColumn.AllowEdit = false;
            this.colKeeper.OptionsColumn.AllowFocus = false;
            this.colKeeper.OptionsColumn.ReadOnly = true;
            this.colKeeper.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeper.Visible = true;
            this.colKeeper.VisibleIndex = 2;
            this.colKeeper.Width = 269;
            // 
            // colEquipmentID11
            // 
            this.colEquipmentID11.FieldName = "EquipmentID";
            this.colEquipmentID11.Name = "colEquipmentID11";
            this.colEquipmentID11.OptionsColumn.AllowEdit = false;
            this.colEquipmentID11.OptionsColumn.AllowFocus = false;
            this.colEquipmentID11.OptionsColumn.ReadOnly = true;
            this.colEquipmentID11.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID11.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID11.Width = 86;
            // 
            // colAllocationStatusID
            // 
            this.colAllocationStatusID.FieldName = "AllocationStatusID";
            this.colAllocationStatusID.Name = "colAllocationStatusID";
            this.colAllocationStatusID.OptionsColumn.AllowEdit = false;
            this.colAllocationStatusID.OptionsColumn.AllowFocus = false;
            this.colAllocationStatusID.OptionsColumn.ReadOnly = true;
            this.colAllocationStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colAllocationStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colAllocationStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationStatusID.Width = 116;
            // 
            // colAllocationStatus
            // 
            this.colAllocationStatus.FieldName = "AllocationStatus";
            this.colAllocationStatus.Name = "colAllocationStatus";
            this.colAllocationStatus.OptionsColumn.AllowEdit = false;
            this.colAllocationStatus.OptionsColumn.AllowFocus = false;
            this.colAllocationStatus.OptionsColumn.ReadOnly = true;
            this.colAllocationStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationStatus.Visible = true;
            this.colAllocationStatus.VisibleIndex = 3;
            this.colAllocationStatus.Width = 156;
            // 
            // colAllocationDate
            // 
            this.colAllocationDate.FieldName = "AllocationDate";
            this.colAllocationDate.Name = "colAllocationDate";
            this.colAllocationDate.OptionsColumn.AllowEdit = false;
            this.colAllocationDate.OptionsColumn.AllowFocus = false;
            this.colAllocationDate.OptionsColumn.ReadOnly = true;
            this.colAllocationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationDate.Visible = true;
            this.colAllocationDate.VisibleIndex = 4;
            this.colAllocationDate.Width = 144;
            // 
            // colAllocationEndDate
            // 
            this.colAllocationEndDate.FieldName = "AllocationEndDate";
            this.colAllocationEndDate.Name = "colAllocationEndDate";
            this.colAllocationEndDate.OptionsColumn.AllowEdit = false;
            this.colAllocationEndDate.OptionsColumn.AllowFocus = false;
            this.colAllocationEndDate.OptionsColumn.ReadOnly = true;
            this.colAllocationEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationEndDate.Visible = true;
            this.colAllocationEndDate.VisibleIndex = 5;
            this.colAllocationEndDate.Width = 151;
            // 
            // colNotes1
            // 
            this.colNotes1.FieldName = "Notes";
            this.colNotes1.Name = "colNotes1";
            this.colNotes1.OptionsColumn.AllowEdit = false;
            this.colNotes1.OptionsColumn.AllowFocus = false;
            this.colNotes1.OptionsColumn.ReadOnly = true;
            this.colNotes1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes1.Visible = true;
            this.colNotes1.VisibleIndex = 6;
            this.colNotes1.Width = 580;
            // 
            // colMode7
            // 
            this.colMode7.FieldName = "Mode";
            this.colMode7.Name = "colMode7";
            this.colMode7.OptionsColumn.AllowEdit = false;
            this.colMode7.OptionsColumn.AllowFocus = false;
            this.colMode7.OptionsColumn.ReadOnly = true;
            this.colMode7.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode7.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID7
            // 
            this.colRecordID7.FieldName = "RecordID";
            this.colRecordID7.Name = "colRecordID7";
            this.colRecordID7.OptionsColumn.AllowEdit = false;
            this.colRecordID7.OptionsColumn.AllowFocus = false;
            this.colRecordID7.OptionsColumn.ReadOnly = true;
            this.colRecordID7.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID7.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // lueKeeperNotes
            // 
            this.lueKeeperNotes.Name = "lueKeeperNotes";
            // 
            // transactionTabPage
            // 
            this.transactionTabPage.AutoScroll = true;
            this.transactionTabPage.Controls.Add(this.transactionGridControl);
            this.transactionTabPage.Name = "transactionTabPage";
            this.transactionTabPage.Size = new System.Drawing.Size(1290, 200);
            this.transactionTabPage.Text = "Transactions";
            // 
            // transactionGridControl
            // 
            this.transactionGridControl.DataSource = this.spAS11041TransactionItemBindingSource;
            this.transactionGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.transactionGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.transactionGridControl.Location = new System.Drawing.Point(0, 0);
            this.transactionGridControl.MainView = this.transactionsGridView;
            this.transactionGridControl.MenuManager = this.barManager1;
            this.transactionGridControl.Name = "transactionGridControl";
            this.transactionGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceOnExchequer,
            this.moneyTextEdit});
            this.transactionGridControl.Size = new System.Drawing.Size(1290, 200);
            this.transactionGridControl.TabIndex = 3;
            this.transactionGridControl.UseEmbeddedNavigator = true;
            this.transactionGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.transactionsGridView});
            // 
            // spAS11041TransactionItemBindingSource
            // 
            this.spAS11041TransactionItemBindingSource.DataMember = "sp_AS_11041_Transaction_Item";
            this.spAS11041TransactionItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // transactionsGridView
            // 
            this.transactionsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransactionID1,
            this.colEquipmentReference10,
            this.colEquipmentID12,
            this.colTransactionTypeID,
            this.colTransactionType,
            this.colTransactionDate,
            this.colTransactionOrderNumber,
            this.colPurchaseInvoice,
            this.colDescription1,
            this.colCompletedOnExchequer,
            this.colNetTransactionPrice,
            this.colVAT,
            this.colMode8,
            this.colRecordID8});
            this.transactionsGridView.GridControl = this.transactionGridControl;
            this.transactionsGridView.Name = "transactionsGridView";
            this.transactionsGridView.OptionsCustomization.AllowGroup = false;
            this.transactionsGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.transactionsGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.transactionsGridView.OptionsLayout.StoreAppearance = true;
            this.transactionsGridView.OptionsSelection.MultiSelect = true;
            this.transactionsGridView.OptionsView.ColumnAutoWidth = false;
            this.transactionsGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.transactionsGridView.OptionsView.ShowGroupPanel = false;
            this.transactionsGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.transactionsGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.transactionsGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.transactionsGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.transactionsGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.transactionsGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.transactionsGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colTransactionID1
            // 
            this.colTransactionID1.FieldName = "TransactionID";
            this.colTransactionID1.Name = "colTransactionID1";
            this.colTransactionID1.OptionsColumn.AllowEdit = false;
            this.colTransactionID1.OptionsColumn.AllowFocus = false;
            this.colTransactionID1.OptionsColumn.ReadOnly = true;
            this.colTransactionID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionID1.Width = 92;
            // 
            // colEquipmentReference10
            // 
            this.colEquipmentReference10.FieldName = "EquipmentReference";
            this.colEquipmentReference10.Name = "colEquipmentReference10";
            this.colEquipmentReference10.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference10.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference10.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference10.Visible = true;
            this.colEquipmentReference10.VisibleIndex = 0;
            this.colEquipmentReference10.Width = 149;
            // 
            // colEquipmentID12
            // 
            this.colEquipmentID12.FieldName = "EquipmentID";
            this.colEquipmentID12.Name = "colEquipmentID12";
            this.colEquipmentID12.OptionsColumn.AllowEdit = false;
            this.colEquipmentID12.OptionsColumn.AllowFocus = false;
            this.colEquipmentID12.OptionsColumn.ReadOnly = true;
            this.colEquipmentID12.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID12.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID12.Width = 86;
            // 
            // colTransactionTypeID
            // 
            this.colTransactionTypeID.FieldName = "TransactionTypeID";
            this.colTransactionTypeID.Name = "colTransactionTypeID";
            this.colTransactionTypeID.OptionsColumn.AllowEdit = false;
            this.colTransactionTypeID.OptionsColumn.AllowFocus = false;
            this.colTransactionTypeID.OptionsColumn.ReadOnly = true;
            this.colTransactionTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionTypeID.Width = 119;
            // 
            // colTransactionType
            // 
            this.colTransactionType.FieldName = "TransactionType";
            this.colTransactionType.Name = "colTransactionType";
            this.colTransactionType.OptionsColumn.AllowEdit = false;
            this.colTransactionType.OptionsColumn.AllowFocus = false;
            this.colTransactionType.OptionsColumn.ReadOnly = true;
            this.colTransactionType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionType.Visible = true;
            this.colTransactionType.VisibleIndex = 1;
            this.colTransactionType.Width = 145;
            // 
            // colTransactionDate
            // 
            this.colTransactionDate.FieldName = "TransactionDate";
            this.colTransactionDate.Name = "colTransactionDate";
            this.colTransactionDate.OptionsColumn.AllowEdit = false;
            this.colTransactionDate.OptionsColumn.AllowFocus = false;
            this.colTransactionDate.OptionsColumn.ReadOnly = true;
            this.colTransactionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionDate.Visible = true;
            this.colTransactionDate.VisibleIndex = 2;
            this.colTransactionDate.Width = 119;
            // 
            // colTransactionOrderNumber
            // 
            this.colTransactionOrderNumber.FieldName = "TransactionOrderNumber";
            this.colTransactionOrderNumber.Name = "colTransactionOrderNumber";
            this.colTransactionOrderNumber.OptionsColumn.AllowEdit = false;
            this.colTransactionOrderNumber.OptionsColumn.AllowFocus = false;
            this.colTransactionOrderNumber.OptionsColumn.ReadOnly = true;
            this.colTransactionOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionOrderNumber.Visible = true;
            this.colTransactionOrderNumber.VisibleIndex = 3;
            this.colTransactionOrderNumber.Width = 180;
            // 
            // colPurchaseInvoice
            // 
            this.colPurchaseInvoice.FieldName = "PurchaseInvoice";
            this.colPurchaseInvoice.Name = "colPurchaseInvoice";
            this.colPurchaseInvoice.OptionsColumn.AllowEdit = false;
            this.colPurchaseInvoice.OptionsColumn.AllowFocus = false;
            this.colPurchaseInvoice.OptionsColumn.ReadOnly = true;
            this.colPurchaseInvoice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPurchaseInvoice.Visible = true;
            this.colPurchaseInvoice.VisibleIndex = 4;
            this.colPurchaseInvoice.Width = 128;
            // 
            // colDescription1
            // 
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 5;
            this.colDescription1.Width = 428;
            // 
            // colCompletedOnExchequer
            // 
            this.colCompletedOnExchequer.FieldName = "CompletedOnExchequer";
            this.colCompletedOnExchequer.Name = "colCompletedOnExchequer";
            this.colCompletedOnExchequer.OptionsColumn.AllowEdit = false;
            this.colCompletedOnExchequer.OptionsColumn.AllowFocus = false;
            this.colCompletedOnExchequer.OptionsColumn.ReadOnly = true;
            this.colCompletedOnExchequer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompletedOnExchequer.Width = 144;
            // 
            // colNetTransactionPrice
            // 
            this.colNetTransactionPrice.ColumnEdit = this.moneyTextEdit;
            this.colNetTransactionPrice.FieldName = "NetTransactionPrice";
            this.colNetTransactionPrice.Name = "colNetTransactionPrice";
            this.colNetTransactionPrice.OptionsColumn.AllowEdit = false;
            this.colNetTransactionPrice.OptionsColumn.AllowFocus = false;
            this.colNetTransactionPrice.OptionsColumn.ReadOnly = true;
            this.colNetTransactionPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNetTransactionPrice.Visible = true;
            this.colNetTransactionPrice.VisibleIndex = 6;
            this.colNetTransactionPrice.Width = 124;
            // 
            // moneyTextEdit
            // 
            this.moneyTextEdit.AutoHeight = false;
            this.moneyTextEdit.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit.Name = "moneyTextEdit";
            // 
            // colVAT
            // 
            this.colVAT.ColumnEdit = this.moneyTextEdit;
            this.colVAT.FieldName = "VAT";
            this.colVAT.Name = "colVAT";
            this.colVAT.OptionsColumn.AllowEdit = false;
            this.colVAT.OptionsColumn.AllowFocus = false;
            this.colVAT.OptionsColumn.ReadOnly = true;
            this.colVAT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 7;
            // 
            // colMode8
            // 
            this.colMode8.FieldName = "Mode";
            this.colMode8.Name = "colMode8";
            this.colMode8.OptionsColumn.AllowEdit = false;
            this.colMode8.OptionsColumn.AllowFocus = false;
            this.colMode8.OptionsColumn.ReadOnly = true;
            this.colMode8.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode8.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID8
            // 
            this.colRecordID8.FieldName = "RecordID";
            this.colRecordID8.Name = "colRecordID8";
            this.colRecordID8.OptionsColumn.AllowEdit = false;
            this.colRecordID8.OptionsColumn.AllowFocus = false;
            this.colRecordID8.OptionsColumn.ReadOnly = true;
            this.colRecordID8.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID8.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ceOnExchequer
            // 
            this.ceOnExchequer.AutoHeight = false;
            this.ceOnExchequer.Caption = "Check";
            this.ceOnExchequer.Name = "ceOnExchequer";
            // 
            // purposeTabPage
            // 
            this.purposeTabPage.Controls.Add(this.purposeGridControl);
            this.purposeTabPage.Name = "purposeTabPage";
            this.purposeTabPage.Size = new System.Drawing.Size(1290, 200);
            this.purposeTabPage.Text = "Asset Purpose";
            // 
            // purposeGridControl
            // 
            this.purposeGridControl.DataSource = this.spAS11084PurposeItemBindingSource;
            this.purposeGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.purposeGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.purposeGridControl.Location = new System.Drawing.Point(0, 0);
            this.purposeGridControl.MainView = this.purposeGridView;
            this.purposeGridControl.MenuManager = this.barManager1;
            this.purposeGridControl.Name = "purposeGridControl";
            this.purposeGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit16,
            this.repositoryItemMemoExEdit17});
            this.purposeGridControl.Size = new System.Drawing.Size(1290, 200);
            this.purposeGridControl.TabIndex = 3;
            this.purposeGridControl.UseEmbeddedNavigator = true;
            this.purposeGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.purposeGridView});
            // 
            // spAS11084PurposeItemBindingSource
            // 
            this.spAS11084PurposeItemBindingSource.DataMember = "sp_AS_11084_Purpose_Item";
            this.spAS11084PurposeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // purposeGridView
            // 
            this.purposeGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentPurposeID,
            this.colEquipmentID15,
            this.colEquipmentReference13,
            this.colPurpose,
            this.colPurposeID,
            this.colMode14,
            this.colRecordID14});
            this.purposeGridView.GridControl = this.purposeGridControl;
            this.purposeGridView.Name = "purposeGridView";
            this.purposeGridView.OptionsCustomization.AllowFilter = false;
            this.purposeGridView.OptionsCustomization.AllowGroup = false;
            this.purposeGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.purposeGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.purposeGridView.OptionsLayout.StoreAppearance = true;
            this.purposeGridView.OptionsSelection.MultiSelect = true;
            this.purposeGridView.OptionsView.ColumnAutoWidth = false;
            this.purposeGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.purposeGridView.OptionsView.ShowGroupPanel = false;
            this.purposeGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.purposeGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.purposeGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.purposeGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.purposeGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.purposeGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.purposeGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentPurposeID
            // 
            this.colEquipmentPurposeID.FieldName = "EquipmentPurposeID";
            this.colEquipmentPurposeID.Name = "colEquipmentPurposeID";
            this.colEquipmentPurposeID.OptionsColumn.AllowEdit = false;
            this.colEquipmentPurposeID.OptionsColumn.AllowFocus = false;
            this.colEquipmentPurposeID.OptionsColumn.ReadOnly = true;
            this.colEquipmentPurposeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentPurposeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentPurposeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentPurposeID.Width = 118;
            // 
            // colEquipmentID15
            // 
            this.colEquipmentID15.FieldName = "EquipmentID";
            this.colEquipmentID15.Name = "colEquipmentID15";
            this.colEquipmentID15.OptionsColumn.AllowEdit = false;
            this.colEquipmentID15.OptionsColumn.AllowFocus = false;
            this.colEquipmentID15.OptionsColumn.ReadOnly = true;
            this.colEquipmentID15.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID15.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID15.Width = 76;
            // 
            // colEquipmentReference13
            // 
            this.colEquipmentReference13.FieldName = "EquipmentReference";
            this.colEquipmentReference13.Name = "colEquipmentReference13";
            this.colEquipmentReference13.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference13.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference13.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference13.Visible = true;
            this.colEquipmentReference13.VisibleIndex = 0;
            this.colEquipmentReference13.Width = 137;
            // 
            // colPurpose
            // 
            this.colPurpose.FieldName = "Purpose";
            this.colPurpose.Name = "colPurpose";
            this.colPurpose.OptionsColumn.AllowEdit = false;
            this.colPurpose.OptionsColumn.AllowFocus = false;
            this.colPurpose.OptionsColumn.ReadOnly = true;
            this.colPurpose.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPurpose.Visible = true;
            this.colPurpose.VisibleIndex = 1;
            this.colPurpose.Width = 401;
            // 
            // colPurposeID
            // 
            this.colPurposeID.FieldName = "PurposeID";
            this.colPurposeID.Name = "colPurposeID";
            this.colPurposeID.OptionsColumn.AllowEdit = false;
            this.colPurposeID.OptionsColumn.AllowFocus = false;
            this.colPurposeID.OptionsColumn.ReadOnly = true;
            this.colPurposeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colPurposeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colPurposeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode14
            // 
            this.colMode14.FieldName = "Mode";
            this.colMode14.Name = "colMode14";
            this.colMode14.OptionsColumn.AllowEdit = false;
            this.colMode14.OptionsColumn.AllowFocus = false;
            this.colMode14.OptionsColumn.ReadOnly = true;
            this.colMode14.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode14.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID14
            // 
            this.colRecordID14.FieldName = "RecordID";
            this.colRecordID14.Name = "colRecordID14";
            this.colRecordID14.OptionsColumn.AllowEdit = false;
            this.colRecordID14.OptionsColumn.AllowFocus = false;
            this.colRecordID14.OptionsColumn.ReadOnly = true;
            this.colRecordID14.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID14.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit16
            // 
            this.repositoryItemMemoExEdit16.AutoHeight = false;
            this.repositoryItemMemoExEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit16.Name = "repositoryItemMemoExEdit16";
            this.repositoryItemMemoExEdit16.ReadOnly = true;
            this.repositoryItemMemoExEdit16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit16.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit17
            // 
            this.repositoryItemMemoExEdit17.AutoHeight = false;
            this.repositoryItemMemoExEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit17.Name = "repositoryItemMemoExEdit17";
            this.repositoryItemMemoExEdit17.ReadOnly = true;
            this.repositoryItemMemoExEdit17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit17.ShowIcon = false;
            // 
            // fuelCardTabPage
            // 
            this.fuelCardTabPage.Controls.Add(this.fuelCardGridControl);
            this.fuelCardTabPage.Name = "fuelCardTabPage";
            this.fuelCardTabPage.Size = new System.Drawing.Size(1290, 200);
            this.fuelCardTabPage.Text = "Fuel Card";
            // 
            // fuelCardGridControl
            // 
            this.fuelCardGridControl.DataSource = this.spAS11089FuelCardItemBindingSource;
            this.fuelCardGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.fuelCardGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.fuelCardGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.fuelCardGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.fuelCardGridControl.Location = new System.Drawing.Point(0, 0);
            this.fuelCardGridControl.MainView = this.fuelCardGridView;
            this.fuelCardGridControl.MenuManager = this.barManager1;
            this.fuelCardGridControl.Name = "fuelCardGridControl";
            this.fuelCardGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit18,
            this.repositoryItemMemoExEdit19});
            this.fuelCardGridControl.Size = new System.Drawing.Size(1290, 200);
            this.fuelCardGridControl.TabIndex = 3;
            this.fuelCardGridControl.UseEmbeddedNavigator = true;
            this.fuelCardGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.fuelCardGridView});
            // 
            // spAS11089FuelCardItemBindingSource
            // 
            this.spAS11089FuelCardItemBindingSource.DataMember = "sp_AS_11089_Fuel_Card_Item";
            this.spAS11089FuelCardItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // fuelCardGridView
            // 
            this.fuelCardGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFuelCardID,
            this.colCardNumber,
            this.colRegistrationMark,
            this.colEquipmentReference14,
            this.colEquipmentID16,
            this.colKeeper1,
            this.colKeeperAllocationID1,
            this.colExpiryDate,
            this.colSupplierReference2,
            this.colSupplierID2,
            this.colCardStatus,
            this.colCardStatusID,
            this.colNotes5,
            this.colMode15,
            this.colRecordID15});
            this.fuelCardGridView.GridControl = this.fuelCardGridControl;
            this.fuelCardGridView.Name = "fuelCardGridView";
            this.fuelCardGridView.OptionsCustomization.AllowFilter = false;
            this.fuelCardGridView.OptionsCustomization.AllowGroup = false;
            this.fuelCardGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.fuelCardGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.fuelCardGridView.OptionsLayout.StoreAppearance = true;
            this.fuelCardGridView.OptionsSelection.MultiSelect = true;
            this.fuelCardGridView.OptionsView.ColumnAutoWidth = false;
            this.fuelCardGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.fuelCardGridView.OptionsView.ShowGroupPanel = false;
            this.fuelCardGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.fuelCardGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.fuelCardGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.fuelCardGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.fuelCardGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.fuelCardGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.fuelCardGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colFuelCardID
            // 
            this.colFuelCardID.FieldName = "FuelCardID";
            this.colFuelCardID.Name = "colFuelCardID";
            this.colFuelCardID.OptionsColumn.AllowEdit = false;
            this.colFuelCardID.OptionsColumn.AllowFocus = false;
            this.colFuelCardID.OptionsColumn.ReadOnly = true;
            this.colFuelCardID.OptionsColumn.ShowInCustomizationForm = false;
            this.colFuelCardID.OptionsColumn.ShowInExpressionEditor = false;
            this.colFuelCardID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCardNumber
            // 
            this.colCardNumber.FieldName = "CardNumber";
            this.colCardNumber.Name = "colCardNumber";
            this.colCardNumber.OptionsColumn.AllowEdit = false;
            this.colCardNumber.OptionsColumn.AllowFocus = false;
            this.colCardNumber.OptionsColumn.ReadOnly = true;
            this.colCardNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCardNumber.Visible = true;
            this.colCardNumber.VisibleIndex = 1;
            this.colCardNumber.Width = 273;
            // 
            // colRegistrationMark
            // 
            this.colRegistrationMark.FieldName = "Registration Mark";
            this.colRegistrationMark.Name = "colRegistrationMark";
            this.colRegistrationMark.OptionsColumn.AllowEdit = false;
            this.colRegistrationMark.OptionsColumn.AllowFocus = false;
            this.colRegistrationMark.OptionsColumn.ReadOnly = true;
            this.colRegistrationMark.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistrationMark.Visible = true;
            this.colRegistrationMark.VisibleIndex = 2;
            this.colRegistrationMark.Width = 143;
            // 
            // colEquipmentReference14
            // 
            this.colEquipmentReference14.FieldName = "EquipmentReference";
            this.colEquipmentReference14.Name = "colEquipmentReference14";
            this.colEquipmentReference14.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference14.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference14.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference14.Visible = true;
            this.colEquipmentReference14.VisibleIndex = 0;
            this.colEquipmentReference14.Width = 123;
            // 
            // colEquipmentID16
            // 
            this.colEquipmentID16.FieldName = "EquipmentID";
            this.colEquipmentID16.Name = "colEquipmentID16";
            this.colEquipmentID16.OptionsColumn.AllowEdit = false;
            this.colEquipmentID16.OptionsColumn.AllowFocus = false;
            this.colEquipmentID16.OptionsColumn.ReadOnly = true;
            this.colEquipmentID16.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID16.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID16.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID16.Width = 76;
            // 
            // colKeeper1
            // 
            this.colKeeper1.FieldName = "Keeper";
            this.colKeeper1.Name = "colKeeper1";
            this.colKeeper1.OptionsColumn.AllowEdit = false;
            this.colKeeper1.OptionsColumn.AllowFocus = false;
            this.colKeeper1.OptionsColumn.ReadOnly = true;
            this.colKeeper1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeper1.Visible = true;
            this.colKeeper1.VisibleIndex = 3;
            this.colKeeper1.Width = 259;
            // 
            // colKeeperAllocationID1
            // 
            this.colKeeperAllocationID1.FieldName = "KeeperAllocationID";
            this.colKeeperAllocationID1.Name = "colKeeperAllocationID1";
            this.colKeeperAllocationID1.OptionsColumn.AllowEdit = false;
            this.colKeeperAllocationID1.OptionsColumn.AllowFocus = false;
            this.colKeeperAllocationID1.OptionsColumn.ReadOnly = true;
            this.colKeeperAllocationID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAllocationID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAllocationID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAllocationID1.Width = 109;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.OptionsColumn.AllowEdit = false;
            this.colExpiryDate.OptionsColumn.AllowFocus = false;
            this.colExpiryDate.OptionsColumn.ReadOnly = true;
            this.colExpiryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 4;
            this.colExpiryDate.Width = 138;
            // 
            // colSupplierReference2
            // 
            this.colSupplierReference2.FieldName = "SupplierReference";
            this.colSupplierReference2.Name = "colSupplierReference2";
            this.colSupplierReference2.OptionsColumn.AllowEdit = false;
            this.colSupplierReference2.OptionsColumn.AllowFocus = false;
            this.colSupplierReference2.OptionsColumn.ReadOnly = true;
            this.colSupplierReference2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference2.Visible = true;
            this.colSupplierReference2.VisibleIndex = 5;
            this.colSupplierReference2.Width = 169;
            // 
            // colSupplierID2
            // 
            this.colSupplierID2.FieldName = "SupplierID";
            this.colSupplierID2.Name = "colSupplierID2";
            this.colSupplierID2.OptionsColumn.AllowEdit = false;
            this.colSupplierID2.OptionsColumn.AllowFocus = false;
            this.colSupplierID2.OptionsColumn.ReadOnly = true;
            this.colSupplierID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colSupplierID2.OptionsColumn.ShowInExpressionEditor = false;
            this.colSupplierID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCardStatus
            // 
            this.colCardStatus.FieldName = "CardStatus";
            this.colCardStatus.Name = "colCardStatus";
            this.colCardStatus.OptionsColumn.AllowEdit = false;
            this.colCardStatus.OptionsColumn.AllowFocus = false;
            this.colCardStatus.OptionsColumn.ReadOnly = true;
            this.colCardStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCardStatus.Visible = true;
            this.colCardStatus.VisibleIndex = 6;
            this.colCardStatus.Width = 136;
            // 
            // colCardStatusID
            // 
            this.colCardStatusID.FieldName = "CardStatusID";
            this.colCardStatusID.Name = "colCardStatusID";
            this.colCardStatusID.OptionsColumn.AllowEdit = false;
            this.colCardStatusID.OptionsColumn.AllowFocus = false;
            this.colCardStatusID.OptionsColumn.ReadOnly = true;
            this.colCardStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colCardStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colCardStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCardStatusID.Width = 83;
            // 
            // colNotes5
            // 
            this.colNotes5.FieldName = "Notes";
            this.colNotes5.Name = "colNotes5";
            this.colNotes5.OptionsColumn.AllowEdit = false;
            this.colNotes5.OptionsColumn.AllowFocus = false;
            this.colNotes5.OptionsColumn.ReadOnly = true;
            this.colNotes5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes5.Visible = true;
            this.colNotes5.VisibleIndex = 7;
            this.colNotes5.Width = 739;
            // 
            // colMode15
            // 
            this.colMode15.FieldName = "Mode";
            this.colMode15.Name = "colMode15";
            this.colMode15.OptionsColumn.AllowEdit = false;
            this.colMode15.OptionsColumn.AllowFocus = false;
            this.colMode15.OptionsColumn.ReadOnly = true;
            this.colMode15.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode15.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID15
            // 
            this.colRecordID15.FieldName = "RecordID";
            this.colRecordID15.Name = "colRecordID15";
            this.colRecordID15.OptionsColumn.AllowEdit = false;
            this.colRecordID15.OptionsColumn.AllowFocus = false;
            this.colRecordID15.OptionsColumn.ReadOnly = true;
            this.colRecordID15.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID15.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit18
            // 
            this.repositoryItemMemoExEdit18.AutoHeight = false;
            this.repositoryItemMemoExEdit18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit18.Name = "repositoryItemMemoExEdit18";
            this.repositoryItemMemoExEdit18.ReadOnly = true;
            this.repositoryItemMemoExEdit18.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit18.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit19
            // 
            this.repositoryItemMemoExEdit19.AutoHeight = false;
            this.repositoryItemMemoExEdit19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit19.Name = "repositoryItemMemoExEdit19";
            this.repositoryItemMemoExEdit19.ReadOnly = true;
            this.repositoryItemMemoExEdit19.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit19.ShowIcon = false;
            // 
            // trackerTabPage
            // 
            this.trackerTabPage.Controls.Add(this.trackerGridControl);
            this.trackerTabPage.Name = "trackerTabPage";
            this.trackerTabPage.Size = new System.Drawing.Size(1290, 200);
            this.trackerTabPage.Text = "Verilocation";
            // 
            // trackerGridControl
            // 
            this.trackerGridControl.DataSource = this.spAS11023TrackerListBindingSource;
            this.trackerGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.trackerGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.trackerGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.trackerGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.trackerGridControl.Location = new System.Drawing.Point(0, 0);
            this.trackerGridControl.MainView = this.trackerGridView;
            this.trackerGridControl.MenuManager = this.barManager1;
            this.trackerGridControl.Name = "trackerGridControl";
            this.trackerGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemMemoExEdit10});
            this.trackerGridControl.Size = new System.Drawing.Size(1290, 200);
            this.trackerGridControl.TabIndex = 1;
            this.trackerGridControl.UseEmbeddedNavigator = true;
            this.trackerGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.trackerGridView});
            // 
            // spAS11023TrackerListBindingSource
            // 
            this.spAS11023TrackerListBindingSource.DataMember = "sp_AS_11023_Tracker_List";
            this.spAS11023TrackerListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // trackerGridView
            // 
            this.trackerGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTrackerInformationID,
            this.colEquipmentReference6,
            this.colRegistration,
            this.colTrackerVID,
            this.colTrackerReference,
            this.colEquipmentID8,
            this.colMake1,
            this.colModel1,
            this.colOdometerMetres,
            this.colOdometerMiles,
            this.colReason,
            this.colMPG,
            this.colLatitude,
            this.colLatestStartJourney,
            this.colLatestEndJourney,
            this.colLongitude,
            this.colSpeedKPH,
            this.colSpeedMPH,
            this.colAverageSpeed,
            this.colPlaceName,
            this.colRoadName,
            this.colPostSect,
            this.colGeofenceName,
            this.colDistrict,
            this.colLastUpdate,
            this.colDriverName,
            this.colDriverMobile,
            this.colDriverEmail,
            this.colMode4,
            this.colRecordID4});
            this.trackerGridView.GridControl = this.trackerGridControl;
            this.trackerGridView.Name = "trackerGridView";
            this.trackerGridView.OptionsCustomization.AllowFilter = false;
            this.trackerGridView.OptionsCustomization.AllowGroup = false;
            this.trackerGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.trackerGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.trackerGridView.OptionsLayout.StoreAppearance = true;
            this.trackerGridView.OptionsSelection.MultiSelect = true;
            this.trackerGridView.OptionsView.ColumnAutoWidth = false;
            this.trackerGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.trackerGridView.OptionsView.ShowGroupPanel = false;
            this.trackerGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.trackerGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.trackerGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.trackerGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.trackerGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.trackerGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.trackerGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colTrackerInformationID
            // 
            this.colTrackerInformationID.FieldName = "TrackerInformationID";
            this.colTrackerInformationID.Name = "colTrackerInformationID";
            this.colTrackerInformationID.OptionsColumn.AllowEdit = false;
            this.colTrackerInformationID.OptionsColumn.AllowFocus = false;
            this.colTrackerInformationID.OptionsColumn.ReadOnly = true;
            this.colTrackerInformationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTrackerInformationID.Width = 121;
            // 
            // colEquipmentReference6
            // 
            this.colEquipmentReference6.FieldName = "EquipmentReference";
            this.colEquipmentReference6.Name = "colEquipmentReference6";
            this.colEquipmentReference6.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference6.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference6.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference6.Visible = true;
            this.colEquipmentReference6.VisibleIndex = 0;
            this.colEquipmentReference6.Width = 115;
            // 
            // colRegistration
            // 
            this.colRegistration.FieldName = "Registration";
            this.colRegistration.Name = "colRegistration";
            this.colRegistration.OptionsColumn.AllowEdit = false;
            this.colRegistration.OptionsColumn.AllowFocus = false;
            this.colRegistration.OptionsColumn.ReadOnly = true;
            this.colRegistration.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistration.Visible = true;
            this.colRegistration.VisibleIndex = 1;
            // 
            // colTrackerVID
            // 
            this.colTrackerVID.FieldName = "TrackerVID";
            this.colTrackerVID.Name = "colTrackerVID";
            this.colTrackerVID.OptionsColumn.AllowEdit = false;
            this.colTrackerVID.OptionsColumn.AllowFocus = false;
            this.colTrackerVID.OptionsColumn.ReadOnly = true;
            this.colTrackerVID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colTrackerReference
            // 
            this.colTrackerReference.FieldName = "TrackerReference";
            this.colTrackerReference.Name = "colTrackerReference";
            this.colTrackerReference.OptionsColumn.AllowEdit = false;
            this.colTrackerReference.OptionsColumn.AllowFocus = false;
            this.colTrackerReference.OptionsColumn.ReadOnly = true;
            this.colTrackerReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTrackerReference.Width = 162;
            // 
            // colEquipmentID8
            // 
            this.colEquipmentID8.FieldName = "EquipmentID";
            this.colEquipmentID8.Name = "colEquipmentID8";
            this.colEquipmentID8.OptionsColumn.AllowEdit = false;
            this.colEquipmentID8.OptionsColumn.AllowFocus = false;
            this.colEquipmentID8.OptionsColumn.ReadOnly = true;
            this.colEquipmentID8.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID8.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID8.Width = 76;
            // 
            // colMake1
            // 
            this.colMake1.FieldName = "Make";
            this.colMake1.Name = "colMake1";
            this.colMake1.OptionsColumn.AllowEdit = false;
            this.colMake1.OptionsColumn.AllowFocus = false;
            this.colMake1.OptionsColumn.ReadOnly = true;
            this.colMake1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colModel1
            // 
            this.colModel1.FieldName = "Model";
            this.colModel1.Name = "colModel1";
            this.colModel1.OptionsColumn.AllowEdit = false;
            this.colModel1.OptionsColumn.AllowFocus = false;
            this.colModel1.OptionsColumn.ReadOnly = true;
            this.colModel1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colOdometerMetres
            // 
            this.colOdometerMetres.FieldName = "OdometerMetres";
            this.colOdometerMetres.Name = "colOdometerMetres";
            this.colOdometerMetres.OptionsColumn.AllowEdit = false;
            this.colOdometerMetres.OptionsColumn.AllowFocus = false;
            this.colOdometerMetres.OptionsColumn.ReadOnly = true;
            this.colOdometerMetres.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOdometerMetres.Width = 96;
            // 
            // colOdometerMiles
            // 
            this.colOdometerMiles.FieldName = "OdometerMiles";
            this.colOdometerMiles.Name = "colOdometerMiles";
            this.colOdometerMiles.OptionsColumn.AllowEdit = false;
            this.colOdometerMiles.OptionsColumn.AllowFocus = false;
            this.colOdometerMiles.OptionsColumn.ReadOnly = true;
            this.colOdometerMiles.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOdometerMiles.Visible = true;
            this.colOdometerMiles.VisibleIndex = 2;
            this.colOdometerMiles.Width = 86;
            // 
            // colReason
            // 
            this.colReason.FieldName = "Reason";
            this.colReason.Name = "colReason";
            this.colReason.OptionsColumn.AllowEdit = false;
            this.colReason.OptionsColumn.AllowFocus = false;
            this.colReason.OptionsColumn.ReadOnly = true;
            this.colReason.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReason.Visible = true;
            this.colReason.VisibleIndex = 3;
            this.colReason.Width = 147;
            // 
            // colMPG
            // 
            this.colMPG.FieldName = "MPG";
            this.colMPG.Name = "colMPG";
            this.colMPG.OptionsColumn.AllowEdit = false;
            this.colMPG.OptionsColumn.AllowFocus = false;
            this.colMPG.OptionsColumn.ReadOnly = true;
            this.colMPG.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMPG.Visible = true;
            this.colMPG.VisibleIndex = 4;
            // 
            // colLatitude
            // 
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            this.colLatitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colLatestStartJourney
            // 
            this.colLatestStartJourney.FieldName = "LatestStartJourney";
            this.colLatestStartJourney.Name = "colLatestStartJourney";
            this.colLatestStartJourney.OptionsColumn.AllowEdit = false;
            this.colLatestStartJourney.OptionsColumn.AllowFocus = false;
            this.colLatestStartJourney.OptionsColumn.ShowInCustomizationForm = false;
            this.colLatestStartJourney.OptionsColumn.ShowInExpressionEditor = false;
            this.colLatestStartJourney.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatestStartJourney.Width = 166;
            // 
            // colLatestEndJourney
            // 
            this.colLatestEndJourney.FieldName = "LatestEndJourney";
            this.colLatestEndJourney.Name = "colLatestEndJourney";
            this.colLatestEndJourney.OptionsColumn.AllowEdit = false;
            this.colLatestEndJourney.OptionsColumn.AllowFocus = false;
            this.colLatestEndJourney.OptionsColumn.ShowInCustomizationForm = false;
            this.colLatestEndJourney.OptionsColumn.ShowInExpressionEditor = false;
            this.colLatestEndJourney.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatestEndJourney.Width = 160;
            // 
            // colLongitude
            // 
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            this.colLongitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSpeedKPH
            // 
            this.colSpeedKPH.FieldName = "SpeedKPH";
            this.colSpeedKPH.Name = "colSpeedKPH";
            this.colSpeedKPH.OptionsColumn.AllowEdit = false;
            this.colSpeedKPH.OptionsColumn.AllowFocus = false;
            this.colSpeedKPH.OptionsColumn.ReadOnly = true;
            this.colSpeedKPH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSpeedMPH
            // 
            this.colSpeedMPH.FieldName = "SpeedMPH";
            this.colSpeedMPH.Name = "colSpeedMPH";
            this.colSpeedMPH.OptionsColumn.AllowEdit = false;
            this.colSpeedMPH.OptionsColumn.AllowFocus = false;
            this.colSpeedMPH.OptionsColumn.ReadOnly = true;
            this.colSpeedMPH.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeedMPH.Visible = true;
            this.colSpeedMPH.VisibleIndex = 5;
            // 
            // colAverageSpeed
            // 
            this.colAverageSpeed.FieldName = "AverageSpeed";
            this.colAverageSpeed.Name = "colAverageSpeed";
            this.colAverageSpeed.OptionsColumn.AllowEdit = false;
            this.colAverageSpeed.OptionsColumn.AllowFocus = false;
            this.colAverageSpeed.OptionsColumn.ReadOnly = true;
            this.colAverageSpeed.OptionsColumn.ShowInCustomizationForm = false;
            this.colAverageSpeed.OptionsColumn.ShowInExpressionEditor = false;
            this.colAverageSpeed.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAverageSpeed.Width = 86;
            // 
            // colPlaceName
            // 
            this.colPlaceName.FieldName = "PlaceName";
            this.colPlaceName.Name = "colPlaceName";
            this.colPlaceName.OptionsColumn.AllowEdit = false;
            this.colPlaceName.OptionsColumn.AllowFocus = false;
            this.colPlaceName.OptionsColumn.ReadOnly = true;
            this.colPlaceName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlaceName.Visible = true;
            this.colPlaceName.VisibleIndex = 6;
            this.colPlaceName.Width = 241;
            // 
            // colRoadName
            // 
            this.colRoadName.FieldName = "RoadName";
            this.colRoadName.Name = "colRoadName";
            this.colRoadName.OptionsColumn.AllowEdit = false;
            this.colRoadName.OptionsColumn.AllowFocus = false;
            this.colRoadName.OptionsColumn.ReadOnly = true;
            this.colRoadName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadName.Visible = true;
            this.colRoadName.VisibleIndex = 7;
            this.colRoadName.Width = 234;
            // 
            // colPostSect
            // 
            this.colPostSect.FieldName = "PostSect";
            this.colPostSect.Name = "colPostSect";
            this.colPostSect.OptionsColumn.AllowEdit = false;
            this.colPostSect.OptionsColumn.AllowFocus = false;
            this.colPostSect.OptionsColumn.ReadOnly = true;
            this.colPostSect.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPostSect.Visible = true;
            this.colPostSect.VisibleIndex = 8;
            // 
            // colGeofenceName
            // 
            this.colGeofenceName.FieldName = "GeofenceName";
            this.colGeofenceName.Name = "colGeofenceName";
            this.colGeofenceName.OptionsColumn.AllowEdit = false;
            this.colGeofenceName.OptionsColumn.AllowFocus = false;
            this.colGeofenceName.OptionsColumn.ReadOnly = true;
            this.colGeofenceName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGeofenceName.Width = 88;
            // 
            // colDistrict
            // 
            this.colDistrict.FieldName = "District";
            this.colDistrict.Name = "colDistrict";
            this.colDistrict.OptionsColumn.AllowEdit = false;
            this.colDistrict.OptionsColumn.AllowFocus = false;
            this.colDistrict.OptionsColumn.ReadOnly = true;
            this.colDistrict.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistrict.Visible = true;
            this.colDistrict.VisibleIndex = 9;
            this.colDistrict.Width = 250;
            // 
            // colLastUpdate
            // 
            this.colLastUpdate.FieldName = "LastUpdate";
            this.colLastUpdate.Name = "colLastUpdate";
            this.colLastUpdate.OptionsColumn.AllowEdit = false;
            this.colLastUpdate.OptionsColumn.AllowFocus = false;
            this.colLastUpdate.OptionsColumn.ReadOnly = true;
            this.colLastUpdate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colDriverName
            // 
            this.colDriverName.FieldName = "DriverName";
            this.colDriverName.Name = "colDriverName";
            this.colDriverName.OptionsColumn.AllowEdit = false;
            this.colDriverName.OptionsColumn.AllowFocus = false;
            this.colDriverName.OptionsColumn.ReadOnly = true;
            this.colDriverName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colDriverMobile
            // 
            this.colDriverMobile.FieldName = "DriverMobile";
            this.colDriverMobile.Name = "colDriverMobile";
            this.colDriverMobile.OptionsColumn.AllowEdit = false;
            this.colDriverMobile.OptionsColumn.AllowFocus = false;
            this.colDriverMobile.OptionsColumn.ReadOnly = true;
            this.colDriverMobile.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colDriverEmail
            // 
            this.colDriverEmail.FieldName = "DriverEmail";
            this.colDriverEmail.Name = "colDriverEmail";
            this.colDriverEmail.OptionsColumn.AllowEdit = false;
            this.colDriverEmail.OptionsColumn.AllowFocus = false;
            this.colDriverEmail.OptionsColumn.ReadOnly = true;
            this.colDriverEmail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode4
            // 
            this.colMode4.FieldName = "Mode";
            this.colMode4.Name = "colMode4";
            this.colMode4.OptionsColumn.AllowEdit = false;
            this.colMode4.OptionsColumn.AllowFocus = false;
            this.colMode4.OptionsColumn.ReadOnly = true;
            this.colMode4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID4
            // 
            this.colRecordID4.FieldName = "RecordID";
            this.colRecordID4.Name = "colRecordID4";
            this.colRecordID4.OptionsColumn.AllowEdit = false;
            this.colRecordID4.OptionsColumn.AllowFocus = false;
            this.colRecordID4.OptionsColumn.ReadOnly = true;
            this.colRecordID4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ReadOnly = true;
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ReadOnly = true;
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // billingTabPage
            // 
            this.billingTabPage.AutoScroll = true;
            this.billingTabPage.Controls.Add(this.billingGridControl);
            this.billingTabPage.Name = "billingTabPage";
            this.billingTabPage.Size = new System.Drawing.Size(1290, 200);
            this.billingTabPage.Text = "Billing Details";
            // 
            // billingGridControl
            // 
            this.billingGridControl.DataSource = this.spAS11050DepreciationItemBindingSource;
            this.billingGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billingGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.billingGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.billingGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, -1, true, false, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, -1, true, false, "", "delete")});
            this.billingGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.billingGridControl.Location = new System.Drawing.Point(0, 0);
            this.billingGridControl.MainView = this.billingGridView;
            this.billingGridControl.MenuManager = this.barManager1;
            this.billingGridControl.Name = "billingGridControl";
            this.billingGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.moneyTextEdit3});
            this.billingGridControl.Size = new System.Drawing.Size(1290, 200);
            this.billingGridControl.TabIndex = 4;
            this.billingGridControl.UseEmbeddedNavigator = true;
            this.billingGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.billingGridView});
            // 
            // spAS11050DepreciationItemBindingSource
            // 
            this.spAS11050DepreciationItemBindingSource.DataMember = "sp_AS_11050_Depreciation_Item";
            this.spAS11050DepreciationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // billingGridView
            // 
            this.billingGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepreciationID1,
            this.colEquipmentID1,
            this.colEquipmentReference,
            this.colNarrative1,
            this.colBalanceSheetCode1,
            this.colProfitLossCode1,
            this.colBillingCentreCodeID1,
            this.colCompanyCode1,
            this.colDepartmentCode1,
            this.colCostCentreCode1,
            this.colDepreciationAmount1,
            this.colPreviousValue1,
            this.colCurrentValue1,
            this.colPeriodNumber1,
            this.colPeriodStartDate1,
            this.colPeriodEndDate1,
            this.colRemarks1,
            this.colAllowEdit1,
            this.colMode9,
            this.colRecordID9});
            this.billingGridView.GridControl = this.billingGridControl;
            this.billingGridView.Name = "billingGridView";
            this.billingGridView.OptionsCustomization.AllowGroup = false;
            this.billingGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.billingGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.billingGridView.OptionsLayout.StoreAppearance = true;
            this.billingGridView.OptionsSelection.MultiSelect = true;
            this.billingGridView.OptionsView.ColumnAutoWidth = false;
            this.billingGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.billingGridView.OptionsView.ShowGroupPanel = false;
            this.billingGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.billingGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.billingGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.billingGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.billingGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.billingGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.billingGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colDepreciationID1
            // 
            this.colDepreciationID1.FieldName = "DepreciationID";
            this.colDepreciationID1.Name = "colDepreciationID1";
            this.colDepreciationID1.OptionsColumn.AllowEdit = false;
            this.colDepreciationID1.OptionsColumn.AllowFocus = false;
            this.colDepreciationID1.OptionsColumn.ReadOnly = true;
            this.colDepreciationID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colDepreciationID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colDepreciationID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID1
            // 
            this.colEquipmentID1.FieldName = "EquipmentID";
            this.colEquipmentID1.Name = "colEquipmentID1";
            this.colEquipmentID1.OptionsColumn.AllowEdit = false;
            this.colEquipmentID1.OptionsColumn.AllowFocus = false;
            this.colEquipmentID1.OptionsColumn.ReadOnly = true;
            this.colEquipmentID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID1.Width = 86;
            // 
            // colEquipmentReference
            // 
            this.colEquipmentReference.FieldName = "EquipmentReference";
            this.colEquipmentReference.Name = "colEquipmentReference";
            this.colEquipmentReference.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference.Visible = true;
            this.colEquipmentReference.VisibleIndex = 0;
            this.colEquipmentReference.Width = 201;
            // 
            // colNarrative1
            // 
            this.colNarrative1.FieldName = "Narrative";
            this.colNarrative1.Name = "colNarrative1";
            this.colNarrative1.OptionsColumn.AllowEdit = false;
            this.colNarrative1.OptionsColumn.AllowFocus = false;
            this.colNarrative1.OptionsColumn.ReadOnly = true;
            this.colNarrative1.OptionsColumn.ShowInCustomizationForm = false;
            this.colNarrative1.OptionsColumn.ShowInExpressionEditor = false;
            this.colNarrative1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colBalanceSheetCode1
            // 
            this.colBalanceSheetCode1.FieldName = "BalanceSheetCode";
            this.colBalanceSheetCode1.Name = "colBalanceSheetCode1";
            this.colBalanceSheetCode1.OptionsColumn.AllowEdit = false;
            this.colBalanceSheetCode1.OptionsColumn.AllowFocus = false;
            this.colBalanceSheetCode1.OptionsColumn.ReadOnly = true;
            this.colBalanceSheetCode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colBalanceSheetCode1.OptionsColumn.ShowInExpressionEditor = false;
            this.colBalanceSheetCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBalanceSheetCode1.Width = 118;
            // 
            // colProfitLossCode1
            // 
            this.colProfitLossCode1.FieldName = "ProfitLossCode";
            this.colProfitLossCode1.Name = "colProfitLossCode1";
            this.colProfitLossCode1.OptionsColumn.AllowEdit = false;
            this.colProfitLossCode1.OptionsColumn.AllowFocus = false;
            this.colProfitLossCode1.OptionsColumn.ReadOnly = true;
            this.colProfitLossCode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colProfitLossCode1.OptionsColumn.ShowInExpressionEditor = false;
            this.colProfitLossCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProfitLossCode1.Width = 100;
            // 
            // colBillingCentreCodeID1
            // 
            this.colBillingCentreCodeID1.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID1.Name = "colBillingCentreCodeID1";
            this.colBillingCentreCodeID1.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID1.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID1.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colBillingCentreCodeID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colBillingCentreCodeID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBillingCentreCodeID1.Width = 126;
            // 
            // colCompanyCode1
            // 
            this.colCompanyCode1.FieldName = "CompanyCode";
            this.colCompanyCode1.Name = "colCompanyCode1";
            this.colCompanyCode1.OptionsColumn.AllowEdit = false;
            this.colCompanyCode1.OptionsColumn.AllowFocus = false;
            this.colCompanyCode1.OptionsColumn.ReadOnly = true;
            this.colCompanyCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyCode1.Visible = true;
            this.colCompanyCode1.VisibleIndex = 1;
            this.colCompanyCode1.Width = 121;
            // 
            // colDepartmentCode1
            // 
            this.colDepartmentCode1.FieldName = "DepartmentCode";
            this.colDepartmentCode1.Name = "colDepartmentCode1";
            this.colDepartmentCode1.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode1.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode1.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentCode1.Visible = true;
            this.colDepartmentCode1.VisibleIndex = 2;
            this.colDepartmentCode1.Width = 180;
            // 
            // colCostCentreCode1
            // 
            this.colCostCentreCode1.FieldName = "CostCentreCode";
            this.colCostCentreCode1.Name = "colCostCentreCode1";
            this.colCostCentreCode1.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode1.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode1.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreCode1.Visible = true;
            this.colCostCentreCode1.VisibleIndex = 3;
            this.colCostCentreCode1.Width = 130;
            // 
            // colDepreciationAmount1
            // 
            this.colDepreciationAmount1.ColumnEdit = this.moneyTextEdit3;
            this.colDepreciationAmount1.FieldName = "DepreciationAmount";
            this.colDepreciationAmount1.Name = "colDepreciationAmount1";
            this.colDepreciationAmount1.OptionsColumn.AllowEdit = false;
            this.colDepreciationAmount1.OptionsColumn.AllowFocus = false;
            this.colDepreciationAmount1.OptionsColumn.ReadOnly = true;
            this.colDepreciationAmount1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepreciationAmount1.Visible = true;
            this.colDepreciationAmount1.VisibleIndex = 4;
            this.colDepreciationAmount1.Width = 122;
            // 
            // moneyTextEdit3
            // 
            this.moneyTextEdit3.AutoHeight = false;
            this.moneyTextEdit3.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit3.Name = "moneyTextEdit3";
            // 
            // colPreviousValue1
            // 
            this.colPreviousValue1.FieldName = "PreviousValue";
            this.colPreviousValue1.Name = "colPreviousValue1";
            this.colPreviousValue1.OptionsColumn.AllowEdit = false;
            this.colPreviousValue1.OptionsColumn.AllowFocus = false;
            this.colPreviousValue1.OptionsColumn.ReadOnly = true;
            this.colPreviousValue1.OptionsColumn.ShowInCustomizationForm = false;
            this.colPreviousValue1.OptionsColumn.ShowInExpressionEditor = false;
            this.colPreviousValue1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPreviousValue1.Width = 92;
            // 
            // colCurrentValue1
            // 
            this.colCurrentValue1.FieldName = "CurrentValue";
            this.colCurrentValue1.Name = "colCurrentValue1";
            this.colCurrentValue1.OptionsColumn.AllowEdit = false;
            this.colCurrentValue1.OptionsColumn.AllowFocus = false;
            this.colCurrentValue1.OptionsColumn.ReadOnly = true;
            this.colCurrentValue1.OptionsColumn.ShowInCustomizationForm = false;
            this.colCurrentValue1.OptionsColumn.ShowInExpressionEditor = false;
            this.colCurrentValue1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentValue1.Width = 88;
            // 
            // colPeriodNumber1
            // 
            this.colPeriodNumber1.FieldName = "PeriodNumber";
            this.colPeriodNumber1.Name = "colPeriodNumber1";
            this.colPeriodNumber1.OptionsColumn.AllowEdit = false;
            this.colPeriodNumber1.OptionsColumn.AllowFocus = false;
            this.colPeriodNumber1.OptionsColumn.ReadOnly = true;
            this.colPeriodNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodNumber1.Visible = true;
            this.colPeriodNumber1.VisibleIndex = 5;
            this.colPeriodNumber1.Width = 92;
            // 
            // colPeriodStartDate1
            // 
            this.colPeriodStartDate1.FieldName = "PeriodStartDate";
            this.colPeriodStartDate1.Name = "colPeriodStartDate1";
            this.colPeriodStartDate1.OptionsColumn.AllowEdit = false;
            this.colPeriodStartDate1.OptionsColumn.AllowFocus = false;
            this.colPeriodStartDate1.OptionsColumn.ReadOnly = true;
            this.colPeriodStartDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodStartDate1.Visible = true;
            this.colPeriodStartDate1.VisibleIndex = 6;
            this.colPeriodStartDate1.Width = 105;
            // 
            // colPeriodEndDate1
            // 
            this.colPeriodEndDate1.FieldName = "PeriodEndDate";
            this.colPeriodEndDate1.Name = "colPeriodEndDate1";
            this.colPeriodEndDate1.OptionsColumn.AllowEdit = false;
            this.colPeriodEndDate1.OptionsColumn.AllowFocus = false;
            this.colPeriodEndDate1.OptionsColumn.ReadOnly = true;
            this.colPeriodEndDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodEndDate1.Visible = true;
            this.colPeriodEndDate1.VisibleIndex = 7;
            this.colPeriodEndDate1.Width = 99;
            // 
            // colRemarks1
            // 
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.AllowEdit = false;
            this.colRemarks1.OptionsColumn.AllowFocus = false;
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.OptionsColumn.ShowInCustomizationForm = false;
            this.colRemarks1.OptionsColumn.ShowInExpressionEditor = false;
            this.colRemarks1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colAllowEdit1
            // 
            this.colAllowEdit1.FieldName = "AllowEdit";
            this.colAllowEdit1.Name = "colAllowEdit1";
            this.colAllowEdit1.OptionsColumn.AllowEdit = false;
            this.colAllowEdit1.OptionsColumn.AllowFocus = false;
            this.colAllowEdit1.OptionsColumn.ReadOnly = true;
            this.colAllowEdit1.OptionsColumn.ShowInCustomizationForm = false;
            this.colAllowEdit1.OptionsColumn.ShowInExpressionEditor = false;
            this.colAllowEdit1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode9
            // 
            this.colMode9.FieldName = "Mode";
            this.colMode9.Name = "colMode9";
            this.colMode9.OptionsColumn.AllowEdit = false;
            this.colMode9.OptionsColumn.AllowFocus = false;
            this.colMode9.OptionsColumn.ReadOnly = true;
            this.colMode9.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode9.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID9
            // 
            this.colRecordID9.FieldName = "RecordID";
            this.colRecordID9.Name = "colRecordID9";
            this.colRecordID9.OptionsColumn.AllowEdit = false;
            this.colRecordID9.OptionsColumn.AllowFocus = false;
            this.colRecordID9.OptionsColumn.ReadOnly = true;
            this.colRecordID9.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID9.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // depreciationTabPage
            // 
            this.depreciationTabPage.Controls.Add(this.depreciationGridControl);
            this.depreciationTabPage.Name = "depreciationTabPage";
            this.depreciationTabPage.Size = new System.Drawing.Size(1290, 200);
            this.depreciationTabPage.Text = "Depreciation Data";
            // 
            // depreciationGridControl
            // 
            this.depreciationGridControl.DataSource = this.spAS11050DepreciationItemBindingSource;
            this.depreciationGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.depreciationGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.depreciationGridControl.Location = new System.Drawing.Point(0, 0);
            this.depreciationGridControl.MainView = this.depreciationGridView;
            this.depreciationGridControl.MenuManager = this.barManager1;
            this.depreciationGridControl.Name = "depreciationGridControl";
            this.depreciationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.moneyTextEditDep});
            this.depreciationGridControl.Size = new System.Drawing.Size(1290, 200);
            this.depreciationGridControl.TabIndex = 3;
            this.depreciationGridControl.UseEmbeddedNavigator = true;
            this.depreciationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.depreciationGridView});
            // 
            // depreciationGridView
            // 
            this.depreciationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepreciationID,
            this.colEquipmentID,
            this.colEquipmentReference1,
            this.colNarrative,
            this.colBalanceSheetCode,
            this.colProfitLossCode,
            this.colBillingCentreCodeID,
            this.colCompanyCode,
            this.colDepartmentCode,
            this.colCostCentreCode,
            this.colDepreciationAmount,
            this.colPreviousValue,
            this.colCurrentValue,
            this.colPeriodNumber,
            this.colPeriodStartDate,
            this.colPeriodEndDate,
            this.colRemarks,
            this.colAllowEdit,
            this.colMode,
            this.colRecordID});
            this.depreciationGridView.GridControl = this.depreciationGridControl;
            this.depreciationGridView.Name = "depreciationGridView";
            this.depreciationGridView.OptionsCustomization.AllowGroup = false;
            this.depreciationGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.depreciationGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.depreciationGridView.OptionsLayout.StoreAppearance = true;
            this.depreciationGridView.OptionsSelection.MultiSelect = true;
            this.depreciationGridView.OptionsView.ColumnAutoWidth = false;
            this.depreciationGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.depreciationGridView.OptionsView.ShowGroupPanel = false;
            this.depreciationGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.depreciationGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.depreciationGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.depreciationGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.depreciationGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.depreciationGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.depreciationGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colDepreciationID
            // 
            this.colDepreciationID.FieldName = "DepreciationID";
            this.colDepreciationID.Name = "colDepreciationID";
            this.colDepreciationID.OptionsColumn.AllowEdit = false;
            this.colDepreciationID.OptionsColumn.AllowFocus = false;
            this.colDepreciationID.OptionsColumn.ReadOnly = true;
            this.colDepreciationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID
            // 
            this.colEquipmentID.FieldName = "EquipmentID";
            this.colEquipmentID.Name = "colEquipmentID";
            this.colEquipmentID.OptionsColumn.AllowEdit = false;
            this.colEquipmentID.OptionsColumn.AllowFocus = false;
            this.colEquipmentID.OptionsColumn.ReadOnly = true;
            this.colEquipmentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentReference1
            // 
            this.colEquipmentReference1.FieldName = "EquipmentReference";
            this.colEquipmentReference1.Name = "colEquipmentReference1";
            this.colEquipmentReference1.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference1.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference1.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference1.Visible = true;
            this.colEquipmentReference1.VisibleIndex = 0;
            this.colEquipmentReference1.Width = 125;
            // 
            // colNarrative
            // 
            this.colNarrative.FieldName = "Narrative";
            this.colNarrative.Name = "colNarrative";
            this.colNarrative.OptionsColumn.AllowEdit = false;
            this.colNarrative.OptionsColumn.AllowFocus = false;
            this.colNarrative.OptionsColumn.ReadOnly = true;
            this.colNarrative.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNarrative.Visible = true;
            this.colNarrative.VisibleIndex = 1;
            this.colNarrative.Width = 132;
            // 
            // colBalanceSheetCode
            // 
            this.colBalanceSheetCode.FieldName = "BalanceSheetCode";
            this.colBalanceSheetCode.Name = "colBalanceSheetCode";
            this.colBalanceSheetCode.OptionsColumn.AllowEdit = false;
            this.colBalanceSheetCode.OptionsColumn.AllowFocus = false;
            this.colBalanceSheetCode.OptionsColumn.ReadOnly = true;
            this.colBalanceSheetCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBalanceSheetCode.Visible = true;
            this.colBalanceSheetCode.VisibleIndex = 2;
            this.colBalanceSheetCode.Width = 159;
            // 
            // colProfitLossCode
            // 
            this.colProfitLossCode.FieldName = "ProfitLossCode";
            this.colProfitLossCode.Name = "colProfitLossCode";
            this.colProfitLossCode.OptionsColumn.AllowEdit = false;
            this.colProfitLossCode.OptionsColumn.AllowFocus = false;
            this.colProfitLossCode.OptionsColumn.ReadOnly = true;
            this.colProfitLossCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProfitLossCode.Visible = true;
            this.colProfitLossCode.VisibleIndex = 3;
            this.colProfitLossCode.Width = 113;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBillingCentreCodeID.Width = 126;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyCode.Visible = true;
            this.colCompanyCode.VisibleIndex = 4;
            this.colCompanyCode.Width = 95;
            // 
            // colDepartmentCode
            // 
            this.colDepartmentCode.FieldName = "DepartmentCode";
            this.colDepartmentCode.Name = "colDepartmentCode";
            this.colDepartmentCode.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentCode.Visible = true;
            this.colDepartmentCode.VisibleIndex = 5;
            this.colDepartmentCode.Width = 107;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreCode.Visible = true;
            this.colCostCentreCode.VisibleIndex = 6;
            this.colCostCentreCode.Width = 108;
            // 
            // colDepreciationAmount
            // 
            this.colDepreciationAmount.ColumnEdit = this.moneyTextEditDep;
            this.colDepreciationAmount.FieldName = "DepreciationAmount";
            this.colDepreciationAmount.Name = "colDepreciationAmount";
            this.colDepreciationAmount.OptionsColumn.AllowEdit = false;
            this.colDepreciationAmount.OptionsColumn.AllowFocus = false;
            this.colDepreciationAmount.OptionsColumn.ReadOnly = true;
            this.colDepreciationAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepreciationAmount.Visible = true;
            this.colDepreciationAmount.VisibleIndex = 7;
            this.colDepreciationAmount.Width = 122;
            // 
            // moneyTextEditDep
            // 
            this.moneyTextEditDep.AutoHeight = false;
            this.moneyTextEditDep.DisplayFormat.FormatString = "c2";
            this.moneyTextEditDep.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEditDep.Name = "moneyTextEditDep";
            // 
            // colPreviousValue
            // 
            this.colPreviousValue.ColumnEdit = this.moneyTextEditDep;
            this.colPreviousValue.FieldName = "PreviousValue";
            this.colPreviousValue.Name = "colPreviousValue";
            this.colPreviousValue.OptionsColumn.AllowEdit = false;
            this.colPreviousValue.OptionsColumn.AllowFocus = false;
            this.colPreviousValue.OptionsColumn.ReadOnly = true;
            this.colPreviousValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPreviousValue.Visible = true;
            this.colPreviousValue.VisibleIndex = 8;
            this.colPreviousValue.Width = 92;
            // 
            // colCurrentValue
            // 
            this.colCurrentValue.ColumnEdit = this.moneyTextEditDep;
            this.colCurrentValue.FieldName = "CurrentValue";
            this.colCurrentValue.Name = "colCurrentValue";
            this.colCurrentValue.OptionsColumn.AllowEdit = false;
            this.colCurrentValue.OptionsColumn.AllowFocus = false;
            this.colCurrentValue.OptionsColumn.ReadOnly = true;
            this.colCurrentValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentValue.Visible = true;
            this.colCurrentValue.VisibleIndex = 9;
            this.colCurrentValue.Width = 88;
            // 
            // colPeriodNumber
            // 
            this.colPeriodNumber.FieldName = "PeriodNumber";
            this.colPeriodNumber.Name = "colPeriodNumber";
            this.colPeriodNumber.OptionsColumn.AllowEdit = false;
            this.colPeriodNumber.OptionsColumn.AllowFocus = false;
            this.colPeriodNumber.OptionsColumn.ReadOnly = true;
            this.colPeriodNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodNumber.Visible = true;
            this.colPeriodNumber.VisibleIndex = 10;
            this.colPeriodNumber.Width = 92;
            // 
            // colPeriodStartDate
            // 
            this.colPeriodStartDate.FieldName = "PeriodStartDate";
            this.colPeriodStartDate.Name = "colPeriodStartDate";
            this.colPeriodStartDate.OptionsColumn.AllowEdit = false;
            this.colPeriodStartDate.OptionsColumn.AllowFocus = false;
            this.colPeriodStartDate.OptionsColumn.ReadOnly = true;
            this.colPeriodStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodStartDate.Visible = true;
            this.colPeriodStartDate.VisibleIndex = 11;
            this.colPeriodStartDate.Width = 105;
            // 
            // colPeriodEndDate
            // 
            this.colPeriodEndDate.FieldName = "PeriodEndDate";
            this.colPeriodEndDate.Name = "colPeriodEndDate";
            this.colPeriodEndDate.OptionsColumn.AllowEdit = false;
            this.colPeriodEndDate.OptionsColumn.AllowFocus = false;
            this.colPeriodEndDate.OptionsColumn.ReadOnly = true;
            this.colPeriodEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodEndDate.Visible = true;
            this.colPeriodEndDate.VisibleIndex = 12;
            this.colPeriodEndDate.Width = 99;
            // 
            // colRemarks
            // 
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 13;
            this.colRemarks.Width = 595;
            // 
            // colAllowEdit
            // 
            this.colAllowEdit.FieldName = "AllowEdit";
            this.colAllowEdit.Name = "colAllowEdit";
            this.colAllowEdit.OptionsColumn.AllowEdit = false;
            this.colAllowEdit.OptionsColumn.AllowFocus = false;
            this.colAllowEdit.OptionsColumn.ReadOnly = true;
            this.colAllowEdit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // coverTabPage
            // 
            this.coverTabPage.Controls.Add(this.coverGridControl);
            this.coverTabPage.Name = "coverTabPage";
            this.coverTabPage.Size = new System.Drawing.Size(1290, 200);
            this.coverTabPage.Text = "Policy and Insurance Cover Details";
            // 
            // coverGridControl
            // 
            this.coverGridControl.DataSource = this.spAS11075CoverItemBindingSource;
            this.coverGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.coverGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.coverGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.coverGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.coverGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.coverGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.coverGridControl.Location = new System.Drawing.Point(0, 0);
            this.coverGridControl.MainView = this.coverGridView;
            this.coverGridControl.MenuManager = this.barManager1;
            this.coverGridControl.Name = "coverGridControl";
            this.coverGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit9,
            this.moneyTextEdit1});
            this.coverGridControl.Size = new System.Drawing.Size(1290, 200);
            this.coverGridControl.TabIndex = 1;
            this.coverGridControl.UseEmbeddedNavigator = true;
            this.coverGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.coverGridView});
            // 
            // spAS11075CoverItemBindingSource
            // 
            this.spAS11075CoverItemBindingSource.DataMember = "sp_AS_11075_Cover_Item";
            this.spAS11075CoverItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // coverGridView
            // 
            this.coverGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCoverID,
            this.colEquipmentReference16,
            this.colEquipmentID3,
            this.colCoverType,
            this.colCoverTypeID,
            this.colSupplierID1,
            this.colSupplierReference1,
            this.colPolicy,
            this.colAnnualCoverCost,
            this.colRenewalDate,
            this.colNotes3,
            this.colExcess,
            this.colMode11,
            this.colRecordID11});
            this.coverGridView.GridControl = this.coverGridControl;
            this.coverGridView.Name = "coverGridView";
            this.coverGridView.OptionsCustomization.AllowFilter = false;
            this.coverGridView.OptionsCustomization.AllowGroup = false;
            this.coverGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.coverGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.coverGridView.OptionsLayout.StoreAppearance = true;
            this.coverGridView.OptionsSelection.MultiSelect = true;
            this.coverGridView.OptionsView.ColumnAutoWidth = false;
            this.coverGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.coverGridView.OptionsView.ShowGroupPanel = false;
            this.coverGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.coverGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.coverGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.coverGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.coverGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.coverGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.coverGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colCoverID
            // 
            this.colCoverID.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colCoverID.FieldName = "CoverID";
            this.colCoverID.Name = "colCoverID";
            this.colCoverID.OptionsColumn.AllowEdit = false;
            this.colCoverID.OptionsColumn.AllowFocus = false;
            this.colCoverID.OptionsColumn.ReadOnly = true;
            this.colCoverID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ReadOnly = true;
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // colEquipmentReference16
            // 
            this.colEquipmentReference16.FieldName = "EquipmentReference";
            this.colEquipmentReference16.Name = "colEquipmentReference16";
            this.colEquipmentReference16.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference16.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference16.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference16.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference16.Visible = true;
            this.colEquipmentReference16.VisibleIndex = 0;
            this.colEquipmentReference16.Width = 148;
            // 
            // colEquipmentID3
            // 
            this.colEquipmentID3.FieldName = "EquipmentID";
            this.colEquipmentID3.Name = "colEquipmentID3";
            this.colEquipmentID3.OptionsColumn.AllowEdit = false;
            this.colEquipmentID3.OptionsColumn.AllowFocus = false;
            this.colEquipmentID3.OptionsColumn.ReadOnly = true;
            this.colEquipmentID3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID3.Width = 76;
            // 
            // colCoverType
            // 
            this.colCoverType.FieldName = "CoverType";
            this.colCoverType.Name = "colCoverType";
            this.colCoverType.OptionsColumn.AllowEdit = false;
            this.colCoverType.OptionsColumn.AllowFocus = false;
            this.colCoverType.OptionsColumn.ReadOnly = true;
            this.colCoverType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCoverType.Visible = true;
            this.colCoverType.VisibleIndex = 1;
            // 
            // colCoverTypeID
            // 
            this.colCoverTypeID.FieldName = "CoverTypeID";
            this.colCoverTypeID.Name = "colCoverTypeID";
            this.colCoverTypeID.OptionsColumn.AllowEdit = false;
            this.colCoverTypeID.OptionsColumn.AllowFocus = false;
            this.colCoverTypeID.OptionsColumn.ReadOnly = true;
            this.colCoverTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCoverTypeID.Width = 82;
            // 
            // colSupplierID1
            // 
            this.colSupplierID1.FieldName = "SupplierID";
            this.colSupplierID1.Name = "colSupplierID1";
            this.colSupplierID1.OptionsColumn.AllowEdit = false;
            this.colSupplierID1.OptionsColumn.AllowFocus = false;
            this.colSupplierID1.OptionsColumn.ReadOnly = true;
            this.colSupplierID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSupplierReference1
            // 
            this.colSupplierReference1.FieldName = "SupplierReference";
            this.colSupplierReference1.Name = "colSupplierReference1";
            this.colSupplierReference1.OptionsColumn.AllowEdit = false;
            this.colSupplierReference1.OptionsColumn.AllowFocus = false;
            this.colSupplierReference1.OptionsColumn.ReadOnly = true;
            this.colSupplierReference1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference1.Visible = true;
            this.colSupplierReference1.VisibleIndex = 2;
            this.colSupplierReference1.Width = 335;
            // 
            // colPolicy
            // 
            this.colPolicy.FieldName = "Policy";
            this.colPolicy.Name = "colPolicy";
            this.colPolicy.OptionsColumn.AllowEdit = false;
            this.colPolicy.OptionsColumn.AllowFocus = false;
            this.colPolicy.OptionsColumn.ReadOnly = true;
            this.colPolicy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPolicy.Visible = true;
            this.colPolicy.VisibleIndex = 3;
            this.colPolicy.Width = 328;
            // 
            // colAnnualCoverCost
            // 
            this.colAnnualCoverCost.ColumnEdit = this.moneyTextEdit1;
            this.colAnnualCoverCost.FieldName = "AnnualCoverCost";
            this.colAnnualCoverCost.Name = "colAnnualCoverCost";
            this.colAnnualCoverCost.OptionsColumn.AllowEdit = false;
            this.colAnnualCoverCost.OptionsColumn.AllowFocus = false;
            this.colAnnualCoverCost.OptionsColumn.ReadOnly = true;
            this.colAnnualCoverCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAnnualCoverCost.Visible = true;
            this.colAnnualCoverCost.VisibleIndex = 4;
            this.colAnnualCoverCost.Width = 200;
            // 
            // moneyTextEdit1
            // 
            this.moneyTextEdit1.AutoHeight = false;
            this.moneyTextEdit1.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit1.Name = "moneyTextEdit1";
            // 
            // colRenewalDate
            // 
            this.colRenewalDate.FieldName = "RenewalDate";
            this.colRenewalDate.Name = "colRenewalDate";
            this.colRenewalDate.OptionsColumn.AllowEdit = false;
            this.colRenewalDate.OptionsColumn.AllowFocus = false;
            this.colRenewalDate.OptionsColumn.ReadOnly = true;
            this.colRenewalDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRenewalDate.Visible = true;
            this.colRenewalDate.VisibleIndex = 5;
            this.colRenewalDate.Width = 178;
            // 
            // colNotes3
            // 
            this.colNotes3.FieldName = "Notes";
            this.colNotes3.Name = "colNotes3";
            this.colNotes3.OptionsColumn.AllowEdit = false;
            this.colNotes3.OptionsColumn.AllowFocus = false;
            this.colNotes3.OptionsColumn.ReadOnly = true;
            this.colNotes3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes3.Visible = true;
            this.colNotes3.VisibleIndex = 7;
            this.colNotes3.Width = 679;
            // 
            // colExcess
            // 
            this.colExcess.ColumnEdit = this.moneyTextEdit1;
            this.colExcess.FieldName = "Excess";
            this.colExcess.Name = "colExcess";
            this.colExcess.OptionsColumn.AllowEdit = false;
            this.colExcess.OptionsColumn.AllowFocus = false;
            this.colExcess.OptionsColumn.ReadOnly = true;
            this.colExcess.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExcess.Visible = true;
            this.colExcess.VisibleIndex = 6;
            this.colExcess.Width = 112;
            // 
            // colMode11
            // 
            this.colMode11.FieldName = "Mode";
            this.colMode11.Name = "colMode11";
            this.colMode11.OptionsColumn.AllowEdit = false;
            this.colMode11.OptionsColumn.AllowFocus = false;
            this.colMode11.OptionsColumn.ReadOnly = true;
            this.colMode11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID11
            // 
            this.colRecordID11.FieldName = "RecordID";
            this.colRecordID11.Name = "colRecordID11";
            this.colRecordID11.OptionsColumn.AllowEdit = false;
            this.colRecordID11.OptionsColumn.AllowFocus = false;
            this.colRecordID11.OptionsColumn.ReadOnly = true;
            this.colRecordID11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // serviceIntervalTabPage
            // 
            this.serviceIntervalTabPage.Controls.Add(this.serviceIntervalGridControl);
            this.serviceIntervalTabPage.Name = "serviceIntervalTabPage";
            this.serviceIntervalTabPage.Size = new System.Drawing.Size(1290, 200);
            this.serviceIntervalTabPage.Text = "Service Interval Information";
            // 
            // serviceIntervalGridControl
            // 
            this.serviceIntervalGridControl.DataSource = this.spAS11054ServiceDataItemBindingSource;
            this.serviceIntervalGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.serviceIntervalGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.serviceIntervalGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.serviceIntervalGridControl.Location = new System.Drawing.Point(0, 0);
            this.serviceIntervalGridControl.MainView = this.serviceIntervalGridView;
            this.serviceIntervalGridControl.MenuManager = this.barManager1;
            this.serviceIntervalGridControl.Name = "serviceIntervalGridControl";
            this.serviceIntervalGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit11,
            this.repositoryItemMemoExEdit12});
            this.serviceIntervalGridControl.Size = new System.Drawing.Size(1290, 200);
            this.serviceIntervalGridControl.TabIndex = 1;
            this.serviceIntervalGridControl.UseEmbeddedNavigator = true;
            this.serviceIntervalGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.serviceIntervalGridView});
            // 
            // spAS11054ServiceDataItemBindingSource
            // 
            this.spAS11054ServiceDataItemBindingSource.DataMember = "sp_AS_11054_Service_Data_Item";
            this.spAS11054ServiceDataItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // serviceIntervalGridView
            // 
            this.serviceIntervalGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colServiceDataID,
            this.colServiceIntervalScheduleID,
            this.colEquipmentID4,
            this.colEquipmentReference2,
            this.colServiceTypeID,
            this.colServiceType1,
            this.colDistanceUnitsID,
            this.colDistanceUnits,
            this.colDistanceFrequency,
            this.colTimeUnitsID,
            this.colTimeUnits,
            this.colTimeFrequency,
            this.colWarrantyPeriod,
            this.colWarrantyEndDate,
            this.colWarrantyEndDistance,
            this.colServiceDataNotes,
            this.colServiceStatus,
            this.colServiceDueDistance,
            this.colServiceDueDate,
            this.colAlertWithinXDistance,
            this.colAlertWithinXTime,
            this.colWorkType1,
            this.colIntervalScheduleNotes,
            this.colIsCurrentInterval,
            this.colMode1,
            this.colRecordID1});
            this.serviceIntervalGridView.GridControl = this.serviceIntervalGridControl;
            this.serviceIntervalGridView.Name = "serviceIntervalGridView";
            this.serviceIntervalGridView.OptionsCustomization.AllowFilter = false;
            this.serviceIntervalGridView.OptionsCustomization.AllowGroup = false;
            this.serviceIntervalGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.serviceIntervalGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.serviceIntervalGridView.OptionsLayout.StoreAppearance = true;
            this.serviceIntervalGridView.OptionsSelection.MultiSelect = true;
            this.serviceIntervalGridView.OptionsView.ColumnAutoWidth = false;
            this.serviceIntervalGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.serviceIntervalGridView.OptionsView.ShowGroupPanel = false;
            this.serviceIntervalGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.serviceIntervalGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.serviceIntervalGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.serviceIntervalGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.serviceIntervalGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.serviceIntervalGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.serviceIntervalGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colServiceDataID
            // 
            this.colServiceDataID.FieldName = "ServiceDataID";
            this.colServiceDataID.Name = "colServiceDataID";
            this.colServiceDataID.OptionsColumn.AllowEdit = false;
            this.colServiceDataID.OptionsColumn.AllowFocus = false;
            this.colServiceDataID.OptionsColumn.ReadOnly = true;
            this.colServiceDataID.Width = 85;
            // 
            // colServiceIntervalScheduleID
            // 
            this.colServiceIntervalScheduleID.FieldName = "ServiceIntervalScheduleID";
            this.colServiceIntervalScheduleID.Name = "colServiceIntervalScheduleID";
            this.colServiceIntervalScheduleID.OptionsColumn.AllowEdit = false;
            this.colServiceIntervalScheduleID.OptionsColumn.AllowFocus = false;
            this.colServiceIntervalScheduleID.OptionsColumn.ReadOnly = true;
            this.colServiceIntervalScheduleID.Width = 146;
            // 
            // colEquipmentID4
            // 
            this.colEquipmentID4.FieldName = "EquipmentID";
            this.colEquipmentID4.Name = "colEquipmentID4";
            this.colEquipmentID4.OptionsColumn.AllowEdit = false;
            this.colEquipmentID4.OptionsColumn.AllowFocus = false;
            this.colEquipmentID4.OptionsColumn.ReadOnly = true;
            // 
            // colEquipmentReference2
            // 
            this.colEquipmentReference2.FieldName = "EquipmentReference";
            this.colEquipmentReference2.Name = "colEquipmentReference2";
            this.colEquipmentReference2.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference2.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference2.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference2.Visible = true;
            this.colEquipmentReference2.VisibleIndex = 0;
            this.colEquipmentReference2.Width = 113;
            // 
            // colServiceTypeID
            // 
            this.colServiceTypeID.FieldName = "ServiceTypeID";
            this.colServiceTypeID.Name = "colServiceTypeID";
            this.colServiceTypeID.OptionsColumn.AllowEdit = false;
            this.colServiceTypeID.OptionsColumn.AllowFocus = false;
            this.colServiceTypeID.OptionsColumn.ReadOnly = true;
            this.colServiceTypeID.Width = 86;
            // 
            // colServiceType1
            // 
            this.colServiceType1.FieldName = "ServiceType";
            this.colServiceType1.Name = "colServiceType1";
            this.colServiceType1.OptionsColumn.AllowEdit = false;
            this.colServiceType1.OptionsColumn.AllowFocus = false;
            this.colServiceType1.OptionsColumn.ReadOnly = true;
            this.colServiceType1.Visible = true;
            this.colServiceType1.VisibleIndex = 1;
            // 
            // colDistanceUnitsID
            // 
            this.colDistanceUnitsID.FieldName = "DistanceUnitsID";
            this.colDistanceUnitsID.Name = "colDistanceUnitsID";
            this.colDistanceUnitsID.OptionsColumn.AllowEdit = false;
            this.colDistanceUnitsID.OptionsColumn.AllowFocus = false;
            this.colDistanceUnitsID.OptionsColumn.ReadOnly = true;
            this.colDistanceUnitsID.Width = 92;
            // 
            // colDistanceUnits
            // 
            this.colDistanceUnits.FieldName = "DistanceUnits";
            this.colDistanceUnits.Name = "colDistanceUnits";
            this.colDistanceUnits.OptionsColumn.AllowEdit = false;
            this.colDistanceUnits.OptionsColumn.AllowFocus = false;
            this.colDistanceUnits.OptionsColumn.ReadOnly = true;
            this.colDistanceUnits.Visible = true;
            this.colDistanceUnits.VisibleIndex = 2;
            this.colDistanceUnits.Width = 78;
            // 
            // colDistanceFrequency
            // 
            this.colDistanceFrequency.FieldName = "DistanceFrequency";
            this.colDistanceFrequency.Name = "colDistanceFrequency";
            this.colDistanceFrequency.OptionsColumn.AllowEdit = false;
            this.colDistanceFrequency.OptionsColumn.AllowFocus = false;
            this.colDistanceFrequency.OptionsColumn.ReadOnly = true;
            this.colDistanceFrequency.Visible = true;
            this.colDistanceFrequency.VisibleIndex = 3;
            this.colDistanceFrequency.Width = 105;
            // 
            // colTimeUnitsID
            // 
            this.colTimeUnitsID.FieldName = "TimeUnitsID";
            this.colTimeUnitsID.Name = "colTimeUnitsID";
            this.colTimeUnitsID.OptionsColumn.AllowEdit = false;
            this.colTimeUnitsID.OptionsColumn.AllowFocus = false;
            this.colTimeUnitsID.OptionsColumn.ReadOnly = true;
            // 
            // colTimeUnits
            // 
            this.colTimeUnits.FieldName = "TimeUnits";
            this.colTimeUnits.Name = "colTimeUnits";
            this.colTimeUnits.OptionsColumn.AllowEdit = false;
            this.colTimeUnits.OptionsColumn.AllowFocus = false;
            this.colTimeUnits.OptionsColumn.ReadOnly = true;
            this.colTimeUnits.Visible = true;
            this.colTimeUnits.VisibleIndex = 4;
            // 
            // colTimeFrequency
            // 
            this.colTimeFrequency.FieldName = "TimeFrequency";
            this.colTimeFrequency.Name = "colTimeFrequency";
            this.colTimeFrequency.OptionsColumn.AllowEdit = false;
            this.colTimeFrequency.OptionsColumn.AllowFocus = false;
            this.colTimeFrequency.OptionsColumn.ReadOnly = true;
            this.colTimeFrequency.Visible = true;
            this.colTimeFrequency.VisibleIndex = 5;
            this.colTimeFrequency.Width = 86;
            // 
            // colWarrantyPeriod
            // 
            this.colWarrantyPeriod.FieldName = "WarrantyPeriod";
            this.colWarrantyPeriod.Name = "colWarrantyPeriod";
            this.colWarrantyPeriod.OptionsColumn.AllowEdit = false;
            this.colWarrantyPeriod.OptionsColumn.AllowFocus = false;
            this.colWarrantyPeriod.OptionsColumn.ReadOnly = true;
            this.colWarrantyPeriod.Visible = true;
            this.colWarrantyPeriod.VisibleIndex = 6;
            this.colWarrantyPeriod.Width = 89;
            // 
            // colWarrantyEndDate
            // 
            this.colWarrantyEndDate.FieldName = "WarrantyEndDate";
            this.colWarrantyEndDate.Name = "colWarrantyEndDate";
            this.colWarrantyEndDate.OptionsColumn.AllowEdit = false;
            this.colWarrantyEndDate.OptionsColumn.AllowFocus = false;
            this.colWarrantyEndDate.OptionsColumn.ReadOnly = true;
            this.colWarrantyEndDate.Visible = true;
            this.colWarrantyEndDate.VisibleIndex = 7;
            this.colWarrantyEndDate.Width = 103;
            // 
            // colWarrantyEndDistance
            // 
            this.colWarrantyEndDistance.FieldName = "WarrantyEndDistance";
            this.colWarrantyEndDistance.Name = "colWarrantyEndDistance";
            this.colWarrantyEndDistance.OptionsColumn.AllowEdit = false;
            this.colWarrantyEndDistance.OptionsColumn.AllowFocus = false;
            this.colWarrantyEndDistance.OptionsColumn.ReadOnly = true;
            this.colWarrantyEndDistance.Visible = true;
            this.colWarrantyEndDistance.VisibleIndex = 8;
            this.colWarrantyEndDistance.Width = 121;
            // 
            // colServiceDataNotes
            // 
            this.colServiceDataNotes.FieldName = "ServiceDataNotes";
            this.colServiceDataNotes.Name = "colServiceDataNotes";
            this.colServiceDataNotes.OptionsColumn.AllowEdit = false;
            this.colServiceDataNotes.OptionsColumn.AllowFocus = false;
            this.colServiceDataNotes.OptionsColumn.ReadOnly = true;
            this.colServiceDataNotes.Visible = true;
            this.colServiceDataNotes.VisibleIndex = 17;
            this.colServiceDataNotes.Width = 102;
            // 
            // colServiceStatus
            // 
            this.colServiceStatus.FieldName = "ServiceStatus";
            this.colServiceStatus.Name = "colServiceStatus";
            this.colServiceStatus.OptionsColumn.AllowEdit = false;
            this.colServiceStatus.OptionsColumn.AllowFocus = false;
            this.colServiceStatus.OptionsColumn.ReadOnly = true;
            this.colServiceStatus.Visible = true;
            this.colServiceStatus.VisibleIndex = 9;
            this.colServiceStatus.Width = 79;
            // 
            // colServiceDueDistance
            // 
            this.colServiceDueDistance.FieldName = "ServiceDueDistance";
            this.colServiceDueDistance.Name = "colServiceDueDistance";
            this.colServiceDueDistance.OptionsColumn.AllowEdit = false;
            this.colServiceDueDistance.OptionsColumn.AllowFocus = false;
            this.colServiceDueDistance.OptionsColumn.ReadOnly = true;
            this.colServiceDueDistance.Visible = true;
            this.colServiceDueDistance.VisibleIndex = 10;
            this.colServiceDueDistance.Width = 111;
            // 
            // colServiceDueDate
            // 
            this.colServiceDueDate.FieldName = "ServiceDueDate";
            this.colServiceDueDate.Name = "colServiceDueDate";
            this.colServiceDueDate.OptionsColumn.AllowEdit = false;
            this.colServiceDueDate.OptionsColumn.AllowFocus = false;
            this.colServiceDueDate.OptionsColumn.ReadOnly = true;
            this.colServiceDueDate.Visible = true;
            this.colServiceDueDate.VisibleIndex = 11;
            this.colServiceDueDate.Width = 93;
            // 
            // colAlertWithinXDistance
            // 
            this.colAlertWithinXDistance.FieldName = "AlertWithinXDistance";
            this.colAlertWithinXDistance.Name = "colAlertWithinXDistance";
            this.colAlertWithinXDistance.OptionsColumn.AllowEdit = false;
            this.colAlertWithinXDistance.OptionsColumn.AllowFocus = false;
            this.colAlertWithinXDistance.OptionsColumn.ReadOnly = true;
            this.colAlertWithinXDistance.Visible = true;
            this.colAlertWithinXDistance.VisibleIndex = 12;
            this.colAlertWithinXDistance.Width = 116;
            // 
            // colAlertWithinXTime
            // 
            this.colAlertWithinXTime.FieldName = "AlertWithinXTime";
            this.colAlertWithinXTime.Name = "colAlertWithinXTime";
            this.colAlertWithinXTime.OptionsColumn.AllowEdit = false;
            this.colAlertWithinXTime.OptionsColumn.AllowFocus = false;
            this.colAlertWithinXTime.OptionsColumn.ReadOnly = true;
            this.colAlertWithinXTime.Visible = true;
            this.colAlertWithinXTime.VisibleIndex = 13;
            this.colAlertWithinXTime.Width = 97;
            // 
            // colWorkType1
            // 
            this.colWorkType1.FieldName = "WorkType";
            this.colWorkType1.Name = "colWorkType1";
            this.colWorkType1.OptionsColumn.AllowEdit = false;
            this.colWorkType1.OptionsColumn.AllowFocus = false;
            this.colWorkType1.OptionsColumn.ReadOnly = true;
            this.colWorkType1.Visible = true;
            this.colWorkType1.VisibleIndex = 14;
            // 
            // colIntervalScheduleNotes
            // 
            this.colIntervalScheduleNotes.FieldName = "IntervalScheduleNotes";
            this.colIntervalScheduleNotes.Name = "colIntervalScheduleNotes";
            this.colIntervalScheduleNotes.OptionsColumn.AllowEdit = false;
            this.colIntervalScheduleNotes.OptionsColumn.AllowFocus = false;
            this.colIntervalScheduleNotes.OptionsColumn.ReadOnly = true;
            this.colIntervalScheduleNotes.Visible = true;
            this.colIntervalScheduleNotes.VisibleIndex = 15;
            this.colIntervalScheduleNotes.Width = 125;
            // 
            // colIsCurrentInterval
            // 
            this.colIsCurrentInterval.FieldName = "IsCurrentInterval";
            this.colIsCurrentInterval.Name = "colIsCurrentInterval";
            this.colIsCurrentInterval.OptionsColumn.AllowEdit = false;
            this.colIsCurrentInterval.OptionsColumn.AllowFocus = false;
            this.colIsCurrentInterval.OptionsColumn.ReadOnly = true;
            this.colIsCurrentInterval.Visible = true;
            this.colIsCurrentInterval.VisibleIndex = 16;
            this.colIsCurrentInterval.Width = 100;
            // 
            // colMode1
            // 
            this.colMode1.FieldName = "Mode";
            this.colMode1.Name = "colMode1";
            this.colMode1.OptionsColumn.AllowEdit = false;
            this.colMode1.OptionsColumn.AllowFocus = false;
            this.colMode1.OptionsColumn.ReadOnly = true;
            // 
            // colRecordID1
            // 
            this.colRecordID1.FieldName = "RecordID";
            this.colRecordID1.Name = "colRecordID1";
            this.colRecordID1.OptionsColumn.AllowEdit = false;
            this.colRecordID1.OptionsColumn.AllowFocus = false;
            this.colRecordID1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ReadOnly = true;
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ReadOnly = true;
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // workDetailTabPage
            // 
            this.workDetailTabPage.Controls.Add(this.workDetailGridControl);
            this.workDetailTabPage.Name = "workDetailTabPage";
            this.workDetailTabPage.Size = new System.Drawing.Size(1290, 200);
            this.workDetailTabPage.Text = "Work Order Details";
            // 
            // workDetailGridControl
            // 
            this.workDetailGridControl.DataSource = this.spAS11078WorkDetailItemBindingSource;
            this.workDetailGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.workDetailGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.workDetailGridControl.Location = new System.Drawing.Point(0, 0);
            this.workDetailGridControl.MainView = this.workDetailGridView;
            this.workDetailGridControl.MenuManager = this.barManager1;
            this.workDetailGridControl.Name = "workDetailGridControl";
            this.workDetailGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit13,
            this.repositoryItemMemoExEdit14});
            this.workDetailGridControl.Size = new System.Drawing.Size(1290, 200);
            this.workDetailGridControl.TabIndex = 1;
            this.workDetailGridControl.UseEmbeddedNavigator = true;
            this.workDetailGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.workDetailGridView});
            // 
            // spAS11078WorkDetailItemBindingSource
            // 
            this.spAS11078WorkDetailItemBindingSource.DataMember = "sp_AS_11078_Work_Detail_Item";
            this.spAS11078WorkDetailItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // workDetailGridView
            // 
            this.workDetailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkDetailID,
            this.colEquipmentID2,
            this.colEquipmentReference11,
            this.colWorkType,
            this.colWorkTypeID,
            this.colDateRaised,
            this.colSupplierID,
            this.colSupplierReference,
            this.colStatusID,
            this.colStatus,
            this.colDescription2,
            this.colTransactionID,
            this.colOrderNumber,
            this.colCurrentMileage,
            this.colWorkCompletionDate,
            this.colServiceIntervalID,
            this.colServiceType,
            this.colNotes2,
            this.colMode10,
            this.colRecordID10});
            this.workDetailGridView.GridControl = this.workDetailGridControl;
            this.workDetailGridView.Name = "workDetailGridView";
            this.workDetailGridView.OptionsCustomization.AllowFilter = false;
            this.workDetailGridView.OptionsCustomization.AllowGroup = false;
            this.workDetailGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.workDetailGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.workDetailGridView.OptionsLayout.StoreAppearance = true;
            this.workDetailGridView.OptionsSelection.MultiSelect = true;
            this.workDetailGridView.OptionsView.ColumnAutoWidth = false;
            this.workDetailGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.workDetailGridView.OptionsView.ShowGroupPanel = false;
            this.workDetailGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.workDetailGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.workDetailGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.workDetailGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.workDetailGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.workDetailGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.workDetailGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colWorkDetailID
            // 
            this.colWorkDetailID.FieldName = "WorkDetailID";
            this.colWorkDetailID.Name = "colWorkDetailID";
            this.colWorkDetailID.OptionsColumn.AllowEdit = false;
            this.colWorkDetailID.OptionsColumn.AllowFocus = false;
            this.colWorkDetailID.OptionsColumn.ReadOnly = true;
            this.colWorkDetailID.OptionsColumn.ShowInCustomizationForm = false;
            this.colWorkDetailID.OptionsColumn.ShowInExpressionEditor = false;
            this.colWorkDetailID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkDetailID.Width = 81;
            // 
            // colEquipmentID2
            // 
            this.colEquipmentID2.FieldName = "EquipmentID";
            this.colEquipmentID2.Name = "colEquipmentID2";
            this.colEquipmentID2.OptionsColumn.AllowEdit = false;
            this.colEquipmentID2.OptionsColumn.AllowFocus = false;
            this.colEquipmentID2.OptionsColumn.ReadOnly = true;
            this.colEquipmentID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID2.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID2.Width = 76;
            // 
            // colEquipmentReference11
            // 
            this.colEquipmentReference11.FieldName = "EquipmentReference";
            this.colEquipmentReference11.Name = "colEquipmentReference11";
            this.colEquipmentReference11.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference11.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference11.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference11.Visible = true;
            this.colEquipmentReference11.VisibleIndex = 0;
            this.colEquipmentReference11.Width = 115;
            // 
            // colWorkType
            // 
            this.colWorkType.FieldName = "WorkType";
            this.colWorkType.Name = "colWorkType";
            this.colWorkType.OptionsColumn.AllowEdit = false;
            this.colWorkType.OptionsColumn.AllowFocus = false;
            this.colWorkType.OptionsColumn.ReadOnly = true;
            this.colWorkType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkType.Visible = true;
            this.colWorkType.VisibleIndex = 1;
            this.colWorkType.Width = 160;
            // 
            // colWorkTypeID
            // 
            this.colWorkTypeID.FieldName = "WorkTypeID";
            this.colWorkTypeID.Name = "colWorkTypeID";
            this.colWorkTypeID.OptionsColumn.AllowEdit = false;
            this.colWorkTypeID.OptionsColumn.AllowFocus = false;
            this.colWorkTypeID.OptionsColumn.ReadOnly = true;
            this.colWorkTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colWorkTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colWorkTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkTypeID.Width = 78;
            // 
            // colDateRaised
            // 
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 2;
            this.colDateRaised.Width = 135;
            // 
            // colSupplierID
            // 
            this.colSupplierID.FieldName = "SupplierID";
            this.colSupplierID.Name = "colSupplierID";
            this.colSupplierID.OptionsColumn.AllowEdit = false;
            this.colSupplierID.OptionsColumn.AllowFocus = false;
            this.colSupplierID.OptionsColumn.ReadOnly = true;
            this.colSupplierID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSupplierID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSupplierID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSupplierReference
            // 
            this.colSupplierReference.FieldName = "SupplierReference";
            this.colSupplierReference.Name = "colSupplierReference";
            this.colSupplierReference.OptionsColumn.AllowEdit = false;
            this.colSupplierReference.OptionsColumn.AllowFocus = false;
            this.colSupplierReference.OptionsColumn.ReadOnly = true;
            this.colSupplierReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference.Visible = true;
            this.colSupplierReference.VisibleIndex = 3;
            this.colSupplierReference.Width = 164;
            // 
            // colStatusID
            // 
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 4;
            this.colStatus.Width = 152;
            // 
            // colDescription2
            // 
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 5;
            this.colDescription2.Width = 212;
            // 
            // colTransactionID
            // 
            this.colTransactionID.FieldName = "TransactionID";
            this.colTransactionID.Name = "colTransactionID";
            this.colTransactionID.OptionsColumn.AllowEdit = false;
            this.colTransactionID.OptionsColumn.AllowFocus = false;
            this.colTransactionID.OptionsColumn.ReadOnly = true;
            this.colTransactionID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionID.Width = 82;
            // 
            // colOrderNumber
            // 
            this.colOrderNumber.FieldName = "OrderNumber";
            this.colOrderNumber.Name = "colOrderNumber";
            this.colOrderNumber.OptionsColumn.AllowEdit = false;
            this.colOrderNumber.OptionsColumn.AllowFocus = false;
            this.colOrderNumber.OptionsColumn.ReadOnly = true;
            this.colOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOrderNumber.Visible = true;
            this.colOrderNumber.VisibleIndex = 6;
            this.colOrderNumber.Width = 157;
            // 
            // colCurrentMileage
            // 
            this.colCurrentMileage.FieldName = "CurrentMileage";
            this.colCurrentMileage.Name = "colCurrentMileage";
            this.colCurrentMileage.OptionsColumn.AllowEdit = false;
            this.colCurrentMileage.OptionsColumn.AllowFocus = false;
            this.colCurrentMileage.OptionsColumn.ReadOnly = true;
            this.colCurrentMileage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentMileage.Visible = true;
            this.colCurrentMileage.VisibleIndex = 7;
            this.colCurrentMileage.Width = 151;
            // 
            // colWorkCompletionDate
            // 
            this.colWorkCompletionDate.FieldName = "WorkCompletionDate";
            this.colWorkCompletionDate.Name = "colWorkCompletionDate";
            this.colWorkCompletionDate.OptionsColumn.AllowEdit = false;
            this.colWorkCompletionDate.OptionsColumn.AllowFocus = false;
            this.colWorkCompletionDate.OptionsColumn.ReadOnly = true;
            this.colWorkCompletionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkCompletionDate.Visible = true;
            this.colWorkCompletionDate.VisibleIndex = 8;
            this.colWorkCompletionDate.Width = 183;
            // 
            // colServiceIntervalID
            // 
            this.colServiceIntervalID.FieldName = "ServiceIntervalID";
            this.colServiceIntervalID.Name = "colServiceIntervalID";
            this.colServiceIntervalID.OptionsColumn.AllowEdit = false;
            this.colServiceIntervalID.OptionsColumn.AllowFocus = false;
            this.colServiceIntervalID.OptionsColumn.ReadOnly = true;
            this.colServiceIntervalID.OptionsColumn.ShowInCustomizationForm = false;
            this.colServiceIntervalID.OptionsColumn.ShowInExpressionEditor = false;
            this.colServiceIntervalID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceIntervalID.Width = 102;
            // 
            // colServiceType
            // 
            this.colServiceType.FieldName = "ServiceType";
            this.colServiceType.Name = "colServiceType";
            this.colServiceType.OptionsColumn.AllowEdit = false;
            this.colServiceType.OptionsColumn.AllowFocus = false;
            this.colServiceType.OptionsColumn.ReadOnly = true;
            this.colServiceType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceType.Visible = true;
            this.colServiceType.VisibleIndex = 9;
            this.colServiceType.Width = 127;
            // 
            // colNotes2
            // 
            this.colNotes2.FieldName = "Notes";
            this.colNotes2.Name = "colNotes2";
            this.colNotes2.OptionsColumn.AllowEdit = false;
            this.colNotes2.OptionsColumn.AllowFocus = false;
            this.colNotes2.OptionsColumn.ReadOnly = true;
            this.colNotes2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes2.Visible = true;
            this.colNotes2.VisibleIndex = 10;
            this.colNotes2.Width = 312;
            // 
            // colMode10
            // 
            this.colMode10.FieldName = "Mode";
            this.colMode10.Name = "colMode10";
            this.colMode10.OptionsColumn.AllowEdit = false;
            this.colMode10.OptionsColumn.AllowFocus = false;
            this.colMode10.OptionsColumn.ReadOnly = true;
            this.colMode10.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode10.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID10
            // 
            this.colRecordID10.FieldName = "RecordID";
            this.colRecordID10.Name = "colRecordID10";
            this.colRecordID10.OptionsColumn.AllowEdit = false;
            this.colRecordID10.OptionsColumn.AllowFocus = false;
            this.colRecordID10.OptionsColumn.ReadOnly = true;
            this.colRecordID10.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID10.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit13
            // 
            this.repositoryItemMemoExEdit13.AutoHeight = false;
            this.repositoryItemMemoExEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit13.Name = "repositoryItemMemoExEdit13";
            this.repositoryItemMemoExEdit13.ReadOnly = true;
            this.repositoryItemMemoExEdit13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit13.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit14
            // 
            this.repositoryItemMemoExEdit14.AutoHeight = false;
            this.repositoryItemMemoExEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit14.Name = "repositoryItemMemoExEdit14";
            this.repositoryItemMemoExEdit14.ReadOnly = true;
            this.repositoryItemMemoExEdit14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit14.ShowIcon = false;
            // 
            // incidentTabPage
            // 
            this.incidentTabPage.Controls.Add(this.incidentGridControl);
            this.incidentTabPage.Name = "incidentTabPage";
            this.incidentTabPage.Size = new System.Drawing.Size(1290, 200);
            this.incidentTabPage.Text = "Incident Details";
            // 
            // incidentGridControl
            // 
            this.incidentGridControl.DataSource = this.spAS11081IncidentItemBindingSource;
            this.incidentGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.incidentGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.incidentGridControl.Location = new System.Drawing.Point(0, 0);
            this.incidentGridControl.MainView = this.incidentGridView;
            this.incidentGridControl.MenuManager = this.barManager1;
            this.incidentGridControl.Name = "incidentGridControl";
            this.incidentGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.incidentNotesMemoEdit,
            this.moneyTextEdit5});
            this.incidentGridControl.Size = new System.Drawing.Size(1290, 200);
            this.incidentGridControl.TabIndex = 2;
            this.incidentGridControl.UseEmbeddedNavigator = true;
            this.incidentGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.incidentGridView});
            // 
            // spAS11081IncidentItemBindingSource
            // 
            this.spAS11081IncidentItemBindingSource.DataMember = "sp_AS_11081_Incident_Item";
            this.spAS11081IncidentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // incidentGridView
            // 
            this.incidentGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIncidentID,
            this.colEquipmentID7,
            this.colEquipmentReference5,
            this.colIncidentStatusID,
            this.colIncidentStatus,
            this.colIncidentTypeID,
            this.colIncidentType,
            this.colIncidentReference,
            this.colDateHappened,
            this.colLocation,
            this.colRepairDueID,
            this.colRepairDue,
            this.colSeverityID,
            this.colSeverity,
            this.colWitness,
            this.colKeeperAtFaultID,
            this.colKeeperAtFault,
            this.colLegalActionID,
            this.colLegalAction,
            this.colFollowUpDate,
            this.colCost,
            this.colNotes4,
            this.colMode3,
            this.colRecordID3});
            this.incidentGridView.GridControl = this.incidentGridControl;
            this.incidentGridView.Name = "incidentGridView";
            this.incidentGridView.OptionsCustomization.AllowFilter = false;
            this.incidentGridView.OptionsCustomization.AllowGroup = false;
            this.incidentGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.incidentGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.incidentGridView.OptionsLayout.StoreAppearance = true;
            this.incidentGridView.OptionsSelection.MultiSelect = true;
            this.incidentGridView.OptionsView.ColumnAutoWidth = false;
            this.incidentGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.incidentGridView.OptionsView.ShowGroupPanel = false;
            this.incidentGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.incidentGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.incidentGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.incidentGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.incidentGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.incidentGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.incidentGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colIncidentID
            // 
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            this.colIncidentID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID7
            // 
            this.colEquipmentID7.FieldName = "EquipmentID";
            this.colEquipmentID7.Name = "colEquipmentID7";
            this.colEquipmentID7.OptionsColumn.AllowEdit = false;
            this.colEquipmentID7.OptionsColumn.AllowFocus = false;
            this.colEquipmentID7.OptionsColumn.ReadOnly = true;
            this.colEquipmentID7.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID7.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID7.Width = 76;
            // 
            // colEquipmentReference5
            // 
            this.colEquipmentReference5.FieldName = "EquipmentReference";
            this.colEquipmentReference5.Name = "colEquipmentReference5";
            this.colEquipmentReference5.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference5.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference5.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference5.Visible = true;
            this.colEquipmentReference5.VisibleIndex = 0;
            this.colEquipmentReference5.Width = 115;
            // 
            // colIncidentStatusID
            // 
            this.colIncidentStatusID.FieldName = "IncidentStatusID";
            this.colIncidentStatusID.Name = "colIncidentStatusID";
            this.colIncidentStatusID.OptionsColumn.AllowEdit = false;
            this.colIncidentStatusID.OptionsColumn.AllowFocus = false;
            this.colIncidentStatusID.OptionsColumn.ReadOnly = true;
            this.colIncidentStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentStatusID.Width = 99;
            // 
            // colIncidentStatus
            // 
            this.colIncidentStatus.FieldName = "IncidentStatus";
            this.colIncidentStatus.Name = "colIncidentStatus";
            this.colIncidentStatus.OptionsColumn.AllowEdit = false;
            this.colIncidentStatus.OptionsColumn.AllowFocus = false;
            this.colIncidentStatus.OptionsColumn.ReadOnly = true;
            this.colIncidentStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentStatus.Visible = true;
            this.colIncidentStatus.VisibleIndex = 2;
            this.colIncidentStatus.Width = 96;
            // 
            // colIncidentTypeID
            // 
            this.colIncidentTypeID.FieldName = "IncidentTypeID";
            this.colIncidentTypeID.Name = "colIncidentTypeID";
            this.colIncidentTypeID.OptionsColumn.AllowEdit = false;
            this.colIncidentTypeID.OptionsColumn.AllowFocus = false;
            this.colIncidentTypeID.OptionsColumn.ReadOnly = true;
            this.colIncidentTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentTypeID.Width = 92;
            // 
            // colIncidentType
            // 
            this.colIncidentType.FieldName = "IncidentType";
            this.colIncidentType.Name = "colIncidentType";
            this.colIncidentType.OptionsColumn.AllowEdit = false;
            this.colIncidentType.OptionsColumn.AllowFocus = false;
            this.colIncidentType.OptionsColumn.ReadOnly = true;
            this.colIncidentType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentType.Visible = true;
            this.colIncidentType.VisibleIndex = 3;
            this.colIncidentType.Width = 97;
            // 
            // colIncidentReference
            // 
            this.colIncidentReference.FieldName = "IncidentReference";
            this.colIncidentReference.Name = "colIncidentReference";
            this.colIncidentReference.OptionsColumn.AllowEdit = false;
            this.colIncidentReference.OptionsColumn.AllowFocus = false;
            this.colIncidentReference.OptionsColumn.ReadOnly = true;
            this.colIncidentReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentReference.Visible = true;
            this.colIncidentReference.VisibleIndex = 1;
            this.colIncidentReference.Width = 104;
            // 
            // colDateHappened
            // 
            this.colDateHappened.FieldName = "DateHappened";
            this.colDateHappened.Name = "colDateHappened";
            this.colDateHappened.OptionsColumn.AllowEdit = false;
            this.colDateHappened.OptionsColumn.AllowFocus = false;
            this.colDateHappened.OptionsColumn.ReadOnly = true;
            this.colDateHappened.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateHappened.Visible = true;
            this.colDateHappened.VisibleIndex = 4;
            this.colDateHappened.Width = 87;
            // 
            // colLocation
            // 
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 5;
            this.colLocation.Width = 233;
            // 
            // colRepairDueID
            // 
            this.colRepairDueID.FieldName = "RepairDueID";
            this.colRepairDueID.Name = "colRepairDueID";
            this.colRepairDueID.OptionsColumn.AllowEdit = false;
            this.colRepairDueID.OptionsColumn.AllowFocus = false;
            this.colRepairDueID.OptionsColumn.ReadOnly = true;
            this.colRepairDueID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRepairDueID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRepairDueID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRepairDueID.Width = 79;
            // 
            // colRepairDue
            // 
            this.colRepairDue.FieldName = "RepairDue";
            this.colRepairDue.Name = "colRepairDue";
            this.colRepairDue.OptionsColumn.AllowEdit = false;
            this.colRepairDue.OptionsColumn.AllowFocus = false;
            this.colRepairDue.OptionsColumn.ReadOnly = true;
            this.colRepairDue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRepairDue.Visible = true;
            this.colRepairDue.VisibleIndex = 6;
            this.colRepairDue.Width = 91;
            // 
            // colSeverityID
            // 
            this.colSeverityID.FieldName = "SeverityID";
            this.colSeverityID.Name = "colSeverityID";
            this.colSeverityID.OptionsColumn.AllowEdit = false;
            this.colSeverityID.OptionsColumn.AllowFocus = false;
            this.colSeverityID.OptionsColumn.ReadOnly = true;
            this.colSeverityID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSeverityID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSeverityID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSeverity
            // 
            this.colSeverity.FieldName = "Severity";
            this.colSeverity.Name = "colSeverity";
            this.colSeverity.OptionsColumn.AllowEdit = false;
            this.colSeverity.OptionsColumn.AllowFocus = false;
            this.colSeverity.OptionsColumn.ReadOnly = true;
            this.colSeverity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSeverity.Visible = true;
            this.colSeverity.VisibleIndex = 7;
            this.colSeverity.Width = 92;
            // 
            // colWitness
            // 
            this.colWitness.FieldName = "Witness";
            this.colWitness.Name = "colWitness";
            this.colWitness.OptionsColumn.AllowEdit = false;
            this.colWitness.OptionsColumn.AllowFocus = false;
            this.colWitness.OptionsColumn.ReadOnly = true;
            this.colWitness.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWitness.Visible = true;
            this.colWitness.VisibleIndex = 8;
            this.colWitness.Width = 248;
            // 
            // colKeeperAtFaultID
            // 
            this.colKeeperAtFaultID.FieldName = "KeeperAtFaultID";
            this.colKeeperAtFaultID.Name = "colKeeperAtFaultID";
            this.colKeeperAtFaultID.OptionsColumn.AllowEdit = false;
            this.colKeeperAtFaultID.OptionsColumn.AllowFocus = false;
            this.colKeeperAtFaultID.OptionsColumn.ReadOnly = true;
            this.colKeeperAtFaultID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAtFaultID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAtFaultID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAtFaultID.Width = 101;
            // 
            // colKeeperAtFault
            // 
            this.colKeeperAtFault.FieldName = "KeeperAtFault";
            this.colKeeperAtFault.Name = "colKeeperAtFault";
            this.colKeeperAtFault.OptionsColumn.AllowEdit = false;
            this.colKeeperAtFault.OptionsColumn.AllowFocus = false;
            this.colKeeperAtFault.OptionsColumn.ReadOnly = true;
            this.colKeeperAtFault.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAtFault.Visible = true;
            this.colKeeperAtFault.VisibleIndex = 9;
            this.colKeeperAtFault.Width = 93;
            // 
            // colLegalActionID
            // 
            this.colLegalActionID.FieldName = "LegalActionID";
            this.colLegalActionID.Name = "colLegalActionID";
            this.colLegalActionID.OptionsColumn.AllowEdit = false;
            this.colLegalActionID.OptionsColumn.AllowFocus = false;
            this.colLegalActionID.OptionsColumn.ReadOnly = true;
            this.colLegalActionID.OptionsColumn.ShowInCustomizationForm = false;
            this.colLegalActionID.OptionsColumn.ShowInExpressionEditor = false;
            this.colLegalActionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLegalActionID.Width = 84;
            // 
            // colLegalAction
            // 
            this.colLegalAction.FieldName = "LegalAction";
            this.colLegalAction.Name = "colLegalAction";
            this.colLegalAction.OptionsColumn.AllowEdit = false;
            this.colLegalAction.OptionsColumn.AllowFocus = false;
            this.colLegalAction.OptionsColumn.ReadOnly = true;
            this.colLegalAction.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLegalAction.Visible = true;
            this.colLegalAction.VisibleIndex = 10;
            this.colLegalAction.Width = 119;
            // 
            // colFollowUpDate
            // 
            this.colFollowUpDate.FieldName = "FollowUpDate";
            this.colFollowUpDate.Name = "colFollowUpDate";
            this.colFollowUpDate.OptionsColumn.AllowEdit = false;
            this.colFollowUpDate.OptionsColumn.AllowFocus = false;
            this.colFollowUpDate.OptionsColumn.ReadOnly = true;
            this.colFollowUpDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFollowUpDate.Visible = true;
            this.colFollowUpDate.VisibleIndex = 11;
            this.colFollowUpDate.Width = 99;
            // 
            // colCost
            // 
            this.colCost.ColumnEdit = this.moneyTextEdit5;
            this.colCost.FieldName = "Cost";
            this.colCost.Name = "colCost";
            this.colCost.OptionsColumn.AllowEdit = false;
            this.colCost.OptionsColumn.AllowFocus = false;
            this.colCost.OptionsColumn.ReadOnly = true;
            this.colCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCost.Visible = true;
            this.colCost.VisibleIndex = 12;
            this.colCost.Width = 84;
            // 
            // moneyTextEdit5
            // 
            this.moneyTextEdit5.AutoHeight = false;
            this.moneyTextEdit5.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit5.Name = "moneyTextEdit5";
            // 
            // colNotes4
            // 
            this.colNotes4.ColumnEdit = this.incidentNotesMemoEdit;
            this.colNotes4.FieldName = "Notes";
            this.colNotes4.Name = "colNotes4";
            this.colNotes4.OptionsColumn.AllowEdit = false;
            this.colNotes4.OptionsColumn.AllowFocus = false;
            this.colNotes4.OptionsColumn.ReadOnly = true;
            this.colNotes4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes4.Visible = true;
            this.colNotes4.VisibleIndex = 13;
            this.colNotes4.Width = 472;
            // 
            // incidentNotesMemoEdit
            // 
            this.incidentNotesMemoEdit.Name = "incidentNotesMemoEdit";
            // 
            // colMode3
            // 
            this.colMode3.FieldName = "Mode";
            this.colMode3.Name = "colMode3";
            this.colMode3.OptionsColumn.AllowEdit = false;
            this.colMode3.OptionsColumn.AllowFocus = false;
            this.colMode3.OptionsColumn.ReadOnly = true;
            this.colMode3.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode3.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID3
            // 
            this.colRecordID3.FieldName = "RecordID";
            this.colRecordID3.Name = "colRecordID3";
            this.colRecordID3.OptionsColumn.AllowEdit = false;
            this.colRecordID3.OptionsColumn.AllowFocus = false;
            this.colRecordID3.OptionsColumn.ReadOnly = true;
            this.colRecordID3.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID3.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // speedingTabPage
            // 
            this.speedingTabPage.Controls.Add(this.speedingGridControl);
            this.speedingTabPage.Name = "speedingTabPage";
            this.speedingTabPage.Size = new System.Drawing.Size(1290, 200);
            this.speedingTabPage.Text = "Speeding Data";
            // 
            // speedingGridControl
            // 
            this.speedingGridControl.DataSource = this.spAS11105SpeedingItemBindingSource;
            this.speedingGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.speedingGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.speedingGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Add To Incident", "insert"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, false, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, false, "Delete Selected Record(s)", "delete")});
            this.speedingGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.speedingGridControl.Location = new System.Drawing.Point(0, 0);
            this.speedingGridControl.MainView = this.speedingGridView;
            this.speedingGridControl.MenuManager = this.barManager1;
            this.speedingGridControl.Name = "speedingGridControl";
            this.speedingGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceReported});
            this.speedingGridControl.Size = new System.Drawing.Size(1290, 200);
            this.speedingGridControl.TabIndex = 2;
            this.speedingGridControl.UseEmbeddedNavigator = true;
            this.speedingGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.speedingGridView});
            // 
            // spAS11105SpeedingItemBindingSource
            // 
            this.spAS11105SpeedingItemBindingSource.DataMember = "sp_AS_11105_Speeding_Item";
            this.spAS11105SpeedingItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // speedingGridView
            // 
            this.speedingGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSpeedingID,
            this.colEquipmentReference17,
            this.colVID,
            this.colEquipmentID17,
            this.colSpeed,
            this.colSpeedMPH1,
            this.colTravelTime,
            this.colRoadTypeID,
            this.colRoadType,
            this.colPlaceName1,
            this.colDistrict1,
            this.colReported,
            this.colMode17,
            this.colRecordID17});
            this.speedingGridView.GridControl = this.speedingGridControl;
            this.speedingGridView.Name = "speedingGridView";
            this.speedingGridView.OptionsCustomization.AllowFilter = false;
            this.speedingGridView.OptionsCustomization.AllowGroup = false;
            this.speedingGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.speedingGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.speedingGridView.OptionsLayout.StoreAppearance = true;
            this.speedingGridView.OptionsSelection.MultiSelect = true;
            this.speedingGridView.OptionsView.ColumnAutoWidth = false;
            this.speedingGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.speedingGridView.OptionsView.ShowGroupPanel = false;
            this.speedingGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.speedingGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.speedingGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.speedingGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.speedingGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.speedingGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.speedingGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colSpeedingID
            // 
            this.colSpeedingID.FieldName = "SpeedingID";
            this.colSpeedingID.Image = ((System.Drawing.Image)(resources.GetObject("colSpeedingID.Image")));
            this.colSpeedingID.Name = "colSpeedingID";
            this.colSpeedingID.OptionsColumn.AllowEdit = false;
            this.colSpeedingID.OptionsColumn.AllowFocus = false;
            this.colSpeedingID.OptionsColumn.ReadOnly = true;
            this.colSpeedingID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSpeedingID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSpeedingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentReference17
            // 
            this.colEquipmentReference17.FieldName = "EquipmentReference";
            this.colEquipmentReference17.Name = "colEquipmentReference17";
            this.colEquipmentReference17.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference17.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference17.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference17.Visible = true;
            this.colEquipmentReference17.VisibleIndex = 0;
            this.colEquipmentReference17.Width = 138;
            // 
            // colVID
            // 
            this.colVID.FieldName = "VID";
            this.colVID.Name = "colVID";
            this.colVID.OptionsColumn.AllowEdit = false;
            this.colVID.OptionsColumn.AllowFocus = false;
            this.colVID.OptionsColumn.ReadOnly = true;
            this.colVID.OptionsColumn.ShowInCustomizationForm = false;
            this.colVID.OptionsColumn.ShowInExpressionEditor = false;
            this.colVID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID17
            // 
            this.colEquipmentID17.FieldName = "EquipmentID";
            this.colEquipmentID17.Name = "colEquipmentID17";
            this.colEquipmentID17.OptionsColumn.AllowEdit = false;
            this.colEquipmentID17.OptionsColumn.AllowFocus = false;
            this.colEquipmentID17.OptionsColumn.ReadOnly = true;
            this.colEquipmentID17.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID17.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID17.Width = 76;
            // 
            // colSpeed
            // 
            this.colSpeed.FieldName = "Speed";
            this.colSpeed.Name = "colSpeed";
            this.colSpeed.OptionsColumn.AllowEdit = false;
            this.colSpeed.OptionsColumn.AllowFocus = false;
            this.colSpeed.OptionsColumn.ReadOnly = true;
            this.colSpeed.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSpeedMPH1
            // 
            this.colSpeedMPH1.FieldName = "SpeedMPH";
            this.colSpeedMPH1.Name = "colSpeedMPH1";
            this.colSpeedMPH1.OptionsColumn.AllowEdit = false;
            this.colSpeedMPH1.OptionsColumn.AllowFocus = false;
            this.colSpeedMPH1.OptionsColumn.ReadOnly = true;
            this.colSpeedMPH1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpeedMPH1.Visible = true;
            this.colSpeedMPH1.VisibleIndex = 1;
            this.colSpeedMPH1.Width = 87;
            // 
            // colTravelTime
            // 
            this.colTravelTime.FieldName = "TravelTime";
            this.colTravelTime.Name = "colTravelTime";
            this.colTravelTime.OptionsColumn.AllowEdit = false;
            this.colTravelTime.OptionsColumn.AllowFocus = false;
            this.colTravelTime.OptionsColumn.ReadOnly = true;
            this.colTravelTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTravelTime.Visible = true;
            this.colTravelTime.VisibleIndex = 2;
            // 
            // colRoadTypeID
            // 
            this.colRoadTypeID.FieldName = "RoadTypeID";
            this.colRoadTypeID.Name = "colRoadTypeID";
            this.colRoadTypeID.OptionsColumn.AllowEdit = false;
            this.colRoadTypeID.OptionsColumn.AllowFocus = false;
            this.colRoadTypeID.OptionsColumn.ReadOnly = true;
            this.colRoadTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRoadTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRoadTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadTypeID.Width = 78;
            // 
            // colRoadType
            // 
            this.colRoadType.FieldName = "RoadType";
            this.colRoadType.Name = "colRoadType";
            this.colRoadType.OptionsColumn.AllowEdit = false;
            this.colRoadType.OptionsColumn.AllowFocus = false;
            this.colRoadType.OptionsColumn.ReadOnly = true;
            this.colRoadType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRoadType.Visible = true;
            this.colRoadType.VisibleIndex = 3;
            this.colRoadType.Width = 311;
            // 
            // colPlaceName1
            // 
            this.colPlaceName1.FieldName = "PlaceName";
            this.colPlaceName1.Name = "colPlaceName1";
            this.colPlaceName1.OptionsColumn.AllowEdit = false;
            this.colPlaceName1.OptionsColumn.AllowFocus = false;
            this.colPlaceName1.OptionsColumn.ReadOnly = true;
            this.colPlaceName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlaceName1.Visible = true;
            this.colPlaceName1.VisibleIndex = 4;
            this.colPlaceName1.Width = 338;
            // 
            // colDistrict1
            // 
            this.colDistrict1.FieldName = "District";
            this.colDistrict1.Name = "colDistrict1";
            this.colDistrict1.OptionsColumn.AllowEdit = false;
            this.colDistrict1.OptionsColumn.AllowFocus = false;
            this.colDistrict1.OptionsColumn.ReadOnly = true;
            this.colDistrict1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDistrict1.Visible = true;
            this.colDistrict1.VisibleIndex = 5;
            this.colDistrict1.Width = 358;
            // 
            // colReported
            // 
            this.colReported.ColumnEdit = this.ceReported;
            this.colReported.FieldName = "Reported";
            this.colReported.Name = "colReported";
            this.colReported.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReported.Visible = true;
            this.colReported.VisibleIndex = 6;
            this.colReported.Width = 64;
            // 
            // ceReported
            // 
            this.ceReported.AutoHeight = false;
            this.ceReported.Caption = "Check";
            this.ceReported.Name = "ceReported";
            // 
            // colMode17
            // 
            this.colMode17.FieldName = "Mode";
            this.colMode17.Name = "colMode17";
            this.colMode17.OptionsColumn.AllowEdit = false;
            this.colMode17.OptionsColumn.AllowFocus = false;
            this.colMode17.OptionsColumn.ReadOnly = true;
            this.colMode17.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode17.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID17
            // 
            this.colRecordID17.FieldName = "RecordID";
            this.colRecordID17.Name = "colRecordID17";
            this.colRecordID17.OptionsColumn.AllowEdit = false;
            this.colRecordID17.OptionsColumn.AllowFocus = false;
            this.colRecordID17.OptionsColumn.ReadOnly = true;
            this.colRecordID17.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID17.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // equipmentDataNavigator
            // 
            this.equipmentDataNavigator.Buttons.Append.Visible = false;
            this.equipmentDataNavigator.Buttons.CancelEdit.Visible = false;
            this.equipmentDataNavigator.Buttons.EndEdit.Visible = false;
            this.equipmentDataNavigator.Buttons.Remove.Visible = false;
            this.equipmentDataNavigator.DataSource = this.spAS11102RentalItemBindingSource;
            this.equipmentDataNavigator.Location = new System.Drawing.Point(50, 12);
            this.equipmentDataNavigator.Name = "equipmentDataNavigator";
            this.equipmentDataNavigator.Size = new System.Drawing.Size(211, 19);
            this.equipmentDataNavigator.StyleController = this.equipmentDataLayoutControl;
            this.equipmentDataNavigator.TabIndex = 120;
            this.equipmentDataNavigator.Text = "equipmentDataNavigator";
            this.equipmentDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.equipmentDataNavigator.TextStringFormat = "Rental Equipment {0} of {1}";
            // 
            // spAS11102RentalItemBindingSource
            // 
            this.spAS11102RentalItemBindingSource.DataMember = "sp_AS_11102_Rental_Item";
            this.spAS11102RentalItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            this.spAS11102RentalItemBindingSource.CurrentChanged += new System.EventHandler(this.spAS11002RentalEquipmentItemBindingSource_CurrentChanged);
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(164, 68);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(391, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling2);
            this.txtEquipmentReference.StyleController = this.equipmentDataLayoutControl;
            this.txtEquipmentReference.TabIndex = 136;
            // 
            // lueEquipmentCategoryID
            // 
            this.lueEquipmentCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "EquipmentCategoryID", true));
            this.lueEquipmentCategoryID.Location = new System.Drawing.Point(164, 92);
            this.lueEquipmentCategoryID.MenuManager = this.barManager1;
            this.lueEquipmentCategoryID.Name = "lueEquipmentCategoryID";
            this.lueEquipmentCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueEquipmentCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Category", 63, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ProfitLossCode", "Profit Loss Code", 88, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BalanceSheetCode", "Balance Sheet Code", 106, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisposalsGL", "Disposals GL", 69, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueEquipmentCategoryID.Properties.DataSource = this.spAS11011EquipmentCategoryListBindingSource;
            this.lueEquipmentCategoryID.Properties.DisplayMember = "Description";
            this.lueEquipmentCategoryID.Properties.NullText = "";
            this.lueEquipmentCategoryID.Properties.NullValuePrompt = "-- Please Select Category --";
            this.lueEquipmentCategoryID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueEquipmentCategoryID.Properties.ValueMember = "EquipmentCategory";
            this.lueEquipmentCategoryID.Size = new System.Drawing.Size(391, 20);
            this.lueEquipmentCategoryID.StyleController = this.equipmentDataLayoutControl;
            this.lueEquipmentCategoryID.TabIndex = 137;
            this.lueEquipmentCategoryID.Tag = "Equipment Category";
            this.lueEquipmentCategoryID.EditValueChanged += new System.EventHandler(this.lueEquipmentCategoryID_EditValueChanged);
            this.lueEquipmentCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lueEquipment_Category_Validating);
            // 
            // lueMakeID
            // 
            this.lueMakeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "MakeID", true));
            this.lueMakeID.Location = new System.Drawing.Point(164, 116);
            this.lueMakeID.MenuManager = this.barManager1;
            this.lueMakeID.Name = "lueMakeID";
            this.lueMakeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueMakeID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Add Make", "Add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueMakeID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Refresh Make", "Refresh", null, true)});
            this.lueMakeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Make", 63, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueMakeID.Properties.DataSource = this.spAS11014MakeListBindingSource;
            this.lueMakeID.Properties.DisplayMember = "Description";
            this.lueMakeID.Properties.NullText = "";
            this.lueMakeID.Properties.NullValuePrompt = "-- Please Select Make --";
            this.lueMakeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueMakeID.Properties.ValueMember = "Make";
            this.lueMakeID.Size = new System.Drawing.Size(391, 22);
            this.lueMakeID.StyleController = this.equipmentDataLayoutControl;
            this.lueMakeID.TabIndex = 138;
            this.lueMakeID.Tag = "Make";
            this.lueMakeID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.makeLookUpEdit_ButtonPressed);
            this.lueMakeID.EditValueChanged += new System.EventHandler(this.lueMake_EditValueChanged);
            this.lueMakeID.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.lueMakeID_EditValueChanging);
            this.lueMakeID.Validating += new System.ComponentModel.CancelEventHandler(this.lueMake_Validating);
            // 
            // spAS11014MakeListBindingSource
            // 
            this.spAS11014MakeListBindingSource.DataMember = "sp_AS_11014_Make_List";
            this.spAS11014MakeListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueModelID
            // 
            this.lueModelID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "ModelID", true));
            this.lueModelID.Location = new System.Drawing.Point(699, 116);
            this.lueModelID.MenuManager = this.barManager1;
            this.lueModelID.Name = "lueModelID";
            this.lueModelID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueModelID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Add Model", "Add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueModelID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Refresh Model", "Refresh", null, true)});
            this.lueModelID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Model", 63, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueModelID.Properties.DataSource = this.spAS11017ModelListBindingSource;
            this.lueModelID.Properties.DisplayMember = "Description";
            this.lueModelID.Properties.NullText = "";
            this.lueModelID.Properties.NullValuePrompt = "-- Please Select Model --";
            this.lueModelID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueModelID.Properties.ValueMember = "Model";
            this.lueModelID.Size = new System.Drawing.Size(622, 22);
            this.lueModelID.StyleController = this.equipmentDataLayoutControl;
            this.lueModelID.TabIndex = 139;
            this.lueModelID.Tag = "Model";
            this.lueModelID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.modelLookUpEdit_ButtonPressed);
            this.lueModelID.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.lueModelID_EditValueChanging);
            this.lueModelID.Validating += new System.ComponentModel.CancelEventHandler(this.lueModel_Validating);
            // 
            // spAS11017ModelListBindingSource
            // 
            this.spAS11017ModelListBindingSource.DataMember = "sp_AS_11017_Model_List";
            this.spAS11017ModelListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueStatusID
            // 
            this.lueStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "OwnershipStatusID", true));
            this.lueStatusID.Location = new System.Drawing.Point(699, 92);
            this.lueStatusID.MenuManager = this.barManager1;
            this.lueStatusID.Name = "lueStatusID";
            this.lueStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueStatusID.Properties.DataSource = this.spAS11000PLEquipmentOwnershipStatusBindingSource;
            this.lueStatusID.Properties.DisplayMember = "Value";
            this.lueStatusID.Properties.NullText = "";
            this.lueStatusID.Properties.NullValuePrompt = "-- Please Select Type Of Rental --";
            this.lueStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueStatusID.Properties.ValueMember = "PickListID";
            this.lueStatusID.Size = new System.Drawing.Size(622, 20);
            this.lueStatusID.StyleController = this.equipmentDataLayoutControl;
            this.lueStatusID.TabIndex = 140;
            this.lueStatusID.Tag = "Rental Type";
            this.lueStatusID.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.lueStatusID_EditValueChanging);
            this.lueStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.lueOwnership_Status_Validating);
            // 
            // spAS11000PLEquipmentOwnershipStatusBindingSource
            // 
            this.spAS11000PLEquipmentOwnershipStatusBindingSource.DataMember = "sp_AS_11000_PL_Equipment_Ownership_Status";
            this.spAS11000PLEquipmentOwnershipStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueSupplierID
            // 
            this.lueSupplierID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "SupplierID", true));
            this.lueSupplierID.Location = new System.Drawing.Point(164, 142);
            this.lueSupplierID.MenuManager = this.barManager1;
            this.lueSupplierID.Name = "lueSupplierID";
            this.lueSupplierID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueSupplierID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Add Supplier", "Add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueSupplierID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Refresh Suppliers", "Refresh", null, true)});
            this.lueSupplierID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierType", "Supplier Type", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierReference", "Supplier Reference", 101, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSupplierID.Properties.DataSource = this.spAS11020SupplierListBindingSource;
            this.lueSupplierID.Properties.DisplayMember = "Name";
            this.lueSupplierID.Properties.NullText = "";
            this.lueSupplierID.Properties.NullValuePrompt = "-- Please Select Supplier --";
            this.lueSupplierID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSupplierID.Properties.ValueMember = "SupplierID";
            this.lueSupplierID.Size = new System.Drawing.Size(391, 22);
            this.lueSupplierID.StyleController = this.equipmentDataLayoutControl;
            this.lueSupplierID.TabIndex = 141;
            this.lueSupplierID.Tag = "Supplier";
            this.lueSupplierID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.supplierLookUpEdit_ButtonPressed);
            this.lueSupplierID.EditValueChanged += new System.EventHandler(this.lueSupplier_EditValueChanged);
            this.lueSupplierID.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.lueSupplierID_EditValueChanging);
            this.lueSupplierID.Validating += new System.ComponentModel.CancelEventHandler(this.lueSupplier_Validating);
            // 
            // spAS11020SupplierListBindingSource
            // 
            this.spAS11020SupplierListBindingSource.DataMember = "sp_AS_11020_Supplier_List";
            this.spAS11020SupplierListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // txtUniqueReference
            // 
            this.txtUniqueReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "UniqueReference", true));
            this.txtUniqueReference.Location = new System.Drawing.Point(699, 68);
            this.txtUniqueReference.MenuManager = this.barManager1;
            this.txtUniqueReference.Name = "txtUniqueReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtUniqueReference, true);
            this.txtUniqueReference.Size = new System.Drawing.Size(622, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtUniqueReference, optionsSpelling3);
            this.txtUniqueReference.StyleController = this.equipmentDataLayoutControl;
            this.txtUniqueReference.TabIndex = 142;
            this.txtUniqueReference.Tag = "Unique Reference";
            this.txtUniqueReference.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.txtUniqueReference_EditValueChanging);
            this.txtUniqueReference.Validating += new System.ComponentModel.CancelEventHandler(this.txtUniqueReference_Validating);
            // 
            // memRentalNotes
            // 
            this.memRentalNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "Notes", true));
            this.memRentalNotes.Location = new System.Drawing.Point(571, 204);
            this.memRentalNotes.MenuManager = this.barManager1;
            this.memRentalNotes.Name = "memRentalNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memRentalNotes, true);
            this.memRentalNotes.Size = new System.Drawing.Size(738, 116);
            this.scSpellChecker.SetSpellCheckerOptions(this.memRentalNotes, optionsSpelling4);
            this.memRentalNotes.StyleController = this.equipmentDataLayoutControl;
            this.memRentalNotes.TabIndex = 143;
            this.memRentalNotes.Tag = "Rental Equipment Notes";
            // 
            // txtModelSpecifications
            // 
            this.txtModelSpecifications.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "ModelSpecifications", true));
            this.txtModelSpecifications.Location = new System.Drawing.Point(699, 142);
            this.txtModelSpecifications.MenuManager = this.barManager1;
            this.txtModelSpecifications.Name = "txtModelSpecifications";
            this.txtModelSpecifications.Properties.NullValuePrompt = "None";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtModelSpecifications, true);
            this.txtModelSpecifications.Size = new System.Drawing.Size(622, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtModelSpecifications, optionsSpelling5);
            this.txtModelSpecifications.StyleController = this.equipmentDataLayoutControl;
            this.txtModelSpecifications.TabIndex = 144;
            this.txtModelSpecifications.Tag = "Model Specification";
            this.txtModelSpecifications.Validating += new System.ComponentModel.CancelEventHandler(this.txtModelSpecifications_Validating);
            // 
            // lueRentalCategoryID
            // 
            this.lueRentalCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "RentalCategoryID", true));
            this.lueRentalCategoryID.Location = new System.Drawing.Point(164, 499);
            this.lueRentalCategoryID.MenuManager = this.barManager1;
            this.lueRentalCategoryID.Name = "lueRentalCategoryID";
            this.lueRentalCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueRentalCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Rental Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueRentalCategoryID.Properties.DataSource = this.spAS11000PLRentalCategoryBindingSource;
            this.lueRentalCategoryID.Properties.DisplayMember = "Value";
            this.lueRentalCategoryID.Properties.NullText = "-- Select Category --";
            this.lueRentalCategoryID.Properties.ValueMember = "PickListID";
            this.lueRentalCategoryID.Size = new System.Drawing.Size(462, 20);
            this.lueRentalCategoryID.StyleController = this.equipmentDataLayoutControl;
            this.lueRentalCategoryID.TabIndex = 145;
            this.lueRentalCategoryID.Tag = "Rental Category";
            this.lueRentalCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lueRentalCategoryID_Validating);
            // 
            // spAS11000PLRentalCategoryBindingSource
            // 
            this.spAS11000PLRentalCategoryBindingSource.DataMember = "sp_AS_11000_PL_Rental_Category";
            this.spAS11000PLRentalCategoryBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueRentalStatusID
            // 
            this.lueRentalStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "RentalStatusID", true));
            this.lueRentalStatusID.Location = new System.Drawing.Point(164, 523);
            this.lueRentalStatusID.MenuManager = this.barManager1;
            this.lueRentalStatusID.Name = "lueRentalStatusID";
            this.lueRentalStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueRentalStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueRentalStatusID.Properties.DataSource = this.spAS11000PLRentalStatusBindingSource;
            this.lueRentalStatusID.Properties.DisplayMember = "Value";
            this.lueRentalStatusID.Properties.NullText = "-- Select Status --";
            this.lueRentalStatusID.Properties.ValueMember = "PickListID";
            this.lueRentalStatusID.Size = new System.Drawing.Size(462, 20);
            this.lueRentalStatusID.StyleController = this.equipmentDataLayoutControl;
            this.lueRentalStatusID.TabIndex = 146;
            this.lueRentalStatusID.Tag = "Rental Status";
            this.lueRentalStatusID.EditValueChanged += new System.EventHandler(this.lueRentalStatusID_EditValueChanged);
            this.lueRentalStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.lueRentalStatusID_Validating);
            // 
            // spAS11000PLRentalStatusBindingSource
            // 
            this.spAS11000PLRentalStatusBindingSource.DataMember = "sp_AS_11000_PL_Rental_Status";
            this.spAS11000PLRentalStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueRentalSpecificationID
            // 
            this.lueRentalSpecificationID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "RentalSpecificationID", true));
            this.lueRentalSpecificationID.Location = new System.Drawing.Point(164, 547);
            this.lueRentalSpecificationID.MenuManager = this.barManager1;
            this.lueRentalSpecificationID.Name = "lueRentalSpecificationID";
            this.lueRentalSpecificationID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueRentalSpecificationID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Specification", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueRentalSpecificationID.Properties.DataSource = this.spAS11000PLRentalSpecificationBindingSource;
            this.lueRentalSpecificationID.Properties.DisplayMember = "Value";
            this.lueRentalSpecificationID.Properties.NullText = "-- Select Specification --";
            this.lueRentalSpecificationID.Properties.ValueMember = "PickListID";
            this.lueRentalSpecificationID.Size = new System.Drawing.Size(462, 20);
            this.lueRentalSpecificationID.StyleController = this.equipmentDataLayoutControl;
            this.lueRentalSpecificationID.TabIndex = 147;
            this.lueRentalSpecificationID.Tag = "Rental  Specification";
            this.lueRentalSpecificationID.Validating += new System.ComponentModel.CancelEventHandler(this.lueRentalSpecificationID_Validating);
            // 
            // spAS11000PLRentalSpecificationBindingSource
            // 
            this.spAS11000PLRentalSpecificationBindingSource.DataMember = "sp_AS_11000_PL_Rental_Specification";
            this.spAS11000PLRentalSpecificationBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnRate
            // 
            this.spnRate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "Rate", true));
            this.spnRate.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnRate.Location = new System.Drawing.Point(164, 595);
            this.spnRate.MenuManager = this.barManager1;
            this.spnRate.Name = "spnRate";
            this.spnRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnRate.Properties.DisplayFormat.FormatString = "c2";
            this.spnRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnRate.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnRate.Size = new System.Drawing.Size(462, 20);
            this.spnRate.StyleController = this.equipmentDataLayoutControl;
            this.spnRate.TabIndex = 148;
            this.spnRate.Tag = "Daily Rate";
            this.spnRate.Validating += new System.ComponentModel.CancelEventHandler(this.spnRate_Validating);
            // 
            // txtCustomerReference
            // 
            this.txtCustomerReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "CustomerReference", true));
            this.txtCustomerReference.Location = new System.Drawing.Point(164, 571);
            this.txtCustomerReference.MenuManager = this.barManager1;
            this.txtCustomerReference.Name = "txtCustomerReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtCustomerReference, true);
            this.txtCustomerReference.Size = new System.Drawing.Size(462, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtCustomerReference, optionsSpelling6);
            this.txtCustomerReference.StyleController = this.equipmentDataLayoutControl;
            this.txtCustomerReference.TabIndex = 149;
            this.txtCustomerReference.Tag = "Customer Order Reference";
            this.txtCustomerReference.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustomerReference_Validating);
            // 
            // deStartDate
            // 
            this.deStartDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "StartDate", true));
            this.deStartDate.EditValue = null;
            this.deStartDate.Location = new System.Drawing.Point(164, 404);
            this.deStartDate.MenuManager = this.barManager1;
            this.deStartDate.Name = "deStartDate";
            this.deStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartDate.Size = new System.Drawing.Size(462, 20);
            this.deStartDate.StyleController = this.equipmentDataLayoutControl;
            this.deStartDate.TabIndex = 150;
            this.deStartDate.Tag = "Start Date";
            this.deStartDate.Validating += new System.ComponentModel.CancelEventHandler(this.deStartDate_Validating);
            // 
            // deActualEndDate
            // 
            this.deActualEndDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "ActualEndDate", true));
            this.deActualEndDate.EditValue = null;
            this.deActualEndDate.Location = new System.Drawing.Point(164, 452);
            this.deActualEndDate.MenuManager = this.barManager1;
            this.deActualEndDate.Name = "deActualEndDate";
            this.deActualEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deActualEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deActualEndDate.Size = new System.Drawing.Size(462, 20);
            this.deActualEndDate.StyleController = this.equipmentDataLayoutControl;
            this.deActualEndDate.TabIndex = 151;
            this.deActualEndDate.Tag = "Actual End Date";
            this.deActualEndDate.Validating += new System.ComponentModel.CancelEventHandler(this.deActualEndDate_Validating);
            // 
            // deEndDate
            // 
            this.deEndDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "EndDate", true));
            this.deEndDate.EditValue = null;
            this.deEndDate.Location = new System.Drawing.Point(164, 428);
            this.deEndDate.MenuManager = this.barManager1;
            this.deEndDate.Name = "deEndDate";
            this.deEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndDate.Size = new System.Drawing.Size(462, 20);
            this.deEndDate.StyleController = this.equipmentDataLayoutControl;
            this.deEndDate.TabIndex = 152;
            this.deEndDate.Tag = "End Date";
            this.deEndDate.EditValueChanged += new System.EventHandler(this.deEndDate_EditValueChanged);
            this.deEndDate.Validating += new System.ComponentModel.CancelEventHandler(this.deEndDate_Validating);
            // 
            // ceSetAsMain
            // 
            this.ceSetAsMain.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "SetAsMain", true));
            this.ceSetAsMain.Location = new System.Drawing.Point(24, 476);
            this.ceSetAsMain.MenuManager = this.barManager1;
            this.ceSetAsMain.Name = "ceSetAsMain";
            this.ceSetAsMain.Properties.Caption = "Set As Main :";
            this.ceSetAsMain.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ceSetAsMain.Size = new System.Drawing.Size(602, 19);
            this.ceSetAsMain.StyleController = this.equipmentDataLayoutControl;
            this.ceSetAsMain.TabIndex = 153;
            this.ceSetAsMain.Tag = "Set As Main";
            // 
            // spinEditEngineSize
            // 
            this.spinEditEngineSize.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "EngineSize", true));
            this.spinEditEngineSize.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditEngineSize.Location = new System.Drawing.Point(176, 200);
            this.spinEditEngineSize.Name = "spinEditEngineSize";
            this.spinEditEngineSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditEngineSize.Properties.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.spinEditEngineSize.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spinEditEngineSize.Size = new System.Drawing.Size(367, 20);
            this.spinEditEngineSize.StyleController = this.equipmentDataLayoutControl;
            this.spinEditEngineSize.TabIndex = 44;
            // 
            // spinEditEmissions
            // 
            this.spinEditEmissions.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "Emissions", true));
            this.spinEditEmissions.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditEmissions.Location = new System.Drawing.Point(176, 248);
            this.spinEditEmissions.Name = "spinEditEmissions";
            this.spinEditEmissions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditEmissions.Properties.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.spinEditEmissions.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spinEditEmissions.Size = new System.Drawing.Size(367, 20);
            this.spinEditEmissions.StyleController = this.equipmentDataLayoutControl;
            this.spinEditEmissions.TabIndex = 48;
            // 
            // dateEditRegistrationDate
            // 
            this.dateEditRegistrationDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "RegistrationDate", true));
            this.dateEditRegistrationDate.EditValue = null;
            this.dateEditRegistrationDate.Location = new System.Drawing.Point(176, 296);
            this.dateEditRegistrationDate.Name = "dateEditRegistrationDate";
            this.dateEditRegistrationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditRegistrationDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditRegistrationDate.Size = new System.Drawing.Size(367, 20);
            this.dateEditRegistrationDate.StyleController = this.equipmentDataLayoutControl;
            this.dateEditRegistrationDate.TabIndex = 56;
            // 
            // lueFuelTypeID
            // 
            this.lueFuelTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "FuelTypeID", true));
            this.lueFuelTypeID.Location = new System.Drawing.Point(176, 224);
            this.lueFuelTypeID.Name = "lueFuelTypeID";
            this.lueFuelTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueFuelTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Rental Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueFuelTypeID.Properties.DataSource = this.spAS11000PLFuelBindingSource;
            this.lueFuelTypeID.Properties.DisplayMember = "Value";
            this.lueFuelTypeID.Properties.NullText = "-- Select Fuel Type --";
            this.lueFuelTypeID.Properties.ValueMember = "PickListID";
            this.lueFuelTypeID.Size = new System.Drawing.Size(367, 20);
            this.lueFuelTypeID.StyleController = this.equipmentDataLayoutControl;
            this.lueFuelTypeID.TabIndex = 145;
            this.lueFuelTypeID.Tag = "Fuel Type";
            // 
            // spAS11000PLFuelBindingSource
            // 
            this.spAS11000PLFuelBindingSource.DataMember = "sp_AS_11000_PL_Fuel";
            this.spAS11000PLFuelBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spinEditListPrice
            // 
            this.spinEditListPrice.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11102RentalItemBindingSource, "ListPrice", true));
            this.spinEditListPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditListPrice.Location = new System.Drawing.Point(176, 272);
            this.spinEditListPrice.Name = "spinEditListPrice";
            this.spinEditListPrice.Properties.DisplayFormat.FormatString = "c2";
            this.spinEditListPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spinEditListPrice.Properties.MaxValue = new decimal(new int[] {
            -1530494976,
            232830,
            0,
            0});
            this.spinEditListPrice.Properties.NullText = "0.00";
            this.spinEditListPrice.Properties.NullValuePrompt = "Please Enter Net Price";
            this.spinEditListPrice.Size = new System.Drawing.Size(367, 20);
            this.spinEditListPrice.StyleController = this.equipmentDataLayoutControl;
            this.spinEditListPrice.TabIndex = 130;
            this.spinEditListPrice.Tag = "Net Price";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem9,
            this.linkedChildDataGroup,
            this.layoutControlItem55,
            this.emptySpaceItem7,
            this.layoutControlGroup2,
            this.layoutControlGroup5,
            this.rentalEquipControlGroup,
            this.rentalIntervalControlGroup,
            this.emptySpaceItem13});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1345, 926);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 894);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1325, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // linkedChildDataGroup
            // 
            this.linkedChildDataGroup.CustomizationFormText = "linkedChildDataGroup";
            this.linkedChildDataGroup.Location = new System.Drawing.Point(0, 619);
            this.linkedChildDataGroup.Name = "linkedChildDataGroup";
            this.linkedChildDataGroup.SelectedTabPage = this.layoutControlGroup4;
            this.linkedChildDataGroup.SelectedTabPageIndex = 0;
            this.linkedChildDataGroup.Size = new System.Drawing.Size(1325, 275);
            this.linkedChildDataGroup.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Linked Child Data";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem27,
            this.emptySpaceItem2});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1301, 230);
            this.layoutControlGroup4.Text = "Linked Child Data";
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.equipmentChildTabControl;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(2, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(1299, 230);
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(10, 230);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(1, 230);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(2, 230);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.Control = this.equipmentDataNavigator;
            this.layoutControlItem55.CustomizationFormText = "layoutControlItem55";
            this.layoutControlItem55.Location = new System.Drawing.Point(38, 0);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(215, 23);
            this.layoutControlItem55.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem55.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(38, 23);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 904);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1325, 1);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.AllowDrawBackground = false;
            this.layoutControlGroup5.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 905);
            this.layoutControlGroup5.Name = "autoGeneratedGroup1";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1325, 1);
            // 
            // rentalEquipControlGroup
            // 
            this.rentalEquipControlGroup.CustomizationFormText = "tabbedControlGroup2";
            this.rentalEquipControlGroup.Location = new System.Drawing.Point(0, 23);
            this.rentalEquipControlGroup.Name = "rentalEquipControlGroup";
            this.rentalEquipControlGroup.SelectedTabPage = this.rentalLayoutControlGroup;
            this.rentalEquipControlGroup.SelectedTabPageIndex = 0;
            this.rentalEquipControlGroup.Size = new System.Drawing.Size(1325, 313);
            this.rentalEquipControlGroup.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.rentalLayoutControlGroup});
            // 
            // rentalLayoutControlGroup
            // 
            this.rentalLayoutControlGroup.CustomizationFormText = "Rental Equipment";
            this.rentalLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEquipmentReference,
            this.ItemForOwnershipStatusID,
            this.ItemForMakeID,
            this.ItemForModelID,
            this.ItemForModelSpecifications,
            this.ItemForEquipmentCategoryID,
            this.ItemForUniqueReference,
            this.ItemForSupplierID,
            this.tabbedControlGroup1,
            this.layoutControlGroup8});
            this.rentalLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.rentalLayoutControlGroup.Name = "rentalLayoutControlGroup";
            this.rentalLayoutControlGroup.Size = new System.Drawing.Size(1301, 268);
            this.rentalLayoutControlGroup.Text = "Rental Equipment";
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.txtEquipmentReference;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference :";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(535, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference :";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForOwnershipStatusID
            // 
            this.ItemForOwnershipStatusID.Control = this.lueStatusID;
            this.ItemForOwnershipStatusID.CustomizationFormText = "Status :";
            this.ItemForOwnershipStatusID.Location = new System.Drawing.Point(535, 24);
            this.ItemForOwnershipStatusID.Name = "ItemForOwnershipStatusID";
            this.ItemForOwnershipStatusID.Size = new System.Drawing.Size(766, 24);
            this.ItemForOwnershipStatusID.Text = "Rental Type :";
            this.ItemForOwnershipStatusID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForMakeID
            // 
            this.ItemForMakeID.Control = this.lueMakeID;
            this.ItemForMakeID.CustomizationFormText = "Make :";
            this.ItemForMakeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForMakeID.Name = "ItemForMakeID";
            this.ItemForMakeID.Size = new System.Drawing.Size(535, 26);
            this.ItemForMakeID.Text = "Make :";
            this.ItemForMakeID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForModelID
            // 
            this.ItemForModelID.Control = this.lueModelID;
            this.ItemForModelID.CustomizationFormText = "Model :";
            this.ItemForModelID.Location = new System.Drawing.Point(535, 48);
            this.ItemForModelID.Name = "ItemForModelID";
            this.ItemForModelID.Size = new System.Drawing.Size(766, 26);
            this.ItemForModelID.Text = "Model :";
            this.ItemForModelID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForModelSpecifications
            // 
            this.ItemForModelSpecifications.Control = this.txtModelSpecifications;
            this.ItemForModelSpecifications.CustomizationFormText = "Model Specifications :";
            this.ItemForModelSpecifications.Location = new System.Drawing.Point(535, 74);
            this.ItemForModelSpecifications.Name = "ItemForModelSpecifications";
            this.ItemForModelSpecifications.Size = new System.Drawing.Size(766, 26);
            this.ItemForModelSpecifications.Text = "Model Specifications :";
            this.ItemForModelSpecifications.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForEquipmentCategoryID
            // 
            this.ItemForEquipmentCategoryID.Control = this.lueEquipmentCategoryID;
            this.ItemForEquipmentCategoryID.CustomizationFormText = "Equipment Category :";
            this.ItemForEquipmentCategoryID.Location = new System.Drawing.Point(0, 24);
            this.ItemForEquipmentCategoryID.Name = "ItemForEquipmentCategoryID";
            this.ItemForEquipmentCategoryID.Size = new System.Drawing.Size(535, 24);
            this.ItemForEquipmentCategoryID.Text = "Equipment Category :";
            this.ItemForEquipmentCategoryID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForUniqueReference
            // 
            this.ItemForUniqueReference.Control = this.txtUniqueReference;
            this.ItemForUniqueReference.CustomizationFormText = "Unique Reference :";
            this.ItemForUniqueReference.Location = new System.Drawing.Point(535, 0);
            this.ItemForUniqueReference.Name = "ItemForUniqueReference";
            this.ItemForUniqueReference.Size = new System.Drawing.Size(766, 24);
            this.ItemForUniqueReference.Text = "Unique Reference :";
            this.ItemForUniqueReference.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForSupplierID
            // 
            this.ItemForSupplierID.Control = this.lueSupplierID;
            this.ItemForSupplierID.CustomizationFormText = "Supplier :";
            this.ItemForSupplierID.Location = new System.Drawing.Point(0, 74);
            this.ItemForSupplierID.Name = "ItemForSupplierID";
            this.ItemForSupplierID.Size = new System.Drawing.Size(535, 26);
            this.ItemForSupplierID.Text = "Supplier :";
            this.ItemForSupplierID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(535, 100);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(766, 168);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImage")));
            this.layoutControlGroup3.CustomizationFormText = "Notes";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(742, 120);
            this.layoutControlGroup3.Text = "Rental Equipment Notes";
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.memRentalNotes;
            this.ItemForNotes.CustomizationFormText = "Notes";
            this.ItemForNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForNotes.MaxSize = new System.Drawing.Size(0, 120);
            this.ItemForNotes.MinSize = new System.Drawing.Size(154, 120);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(742, 120);
            this.ItemForNotes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNotes.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.itemForEngineSize,
            this.itemForEmissions,
            this.itemForRegistrationDate,
            this.ItemForFuelTypeID,
            this.ItemForListPrice});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 100);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(535, 168);
            this.layoutControlGroup8.Text = "Rental Vehicle details ( for P11D)";
            // 
            // itemForEngineSize
            // 
            this.itemForEngineSize.Control = this.spinEditEngineSize;
            this.itemForEngineSize.CustomizationFormText = "Engine Size:";
            this.itemForEngineSize.Location = new System.Drawing.Point(0, 0);
            this.itemForEngineSize.Name = "itemForEngineSize";
            this.itemForEngineSize.Size = new System.Drawing.Size(511, 24);
            this.itemForEngineSize.Text = "Engine Size:";
            this.itemForEngineSize.TextSize = new System.Drawing.Size(137, 13);
            // 
            // itemForEmissions
            // 
            this.itemForEmissions.Control = this.spinEditEmissions;
            this.itemForEmissions.CustomizationFormText = "Emissions:";
            this.itemForEmissions.Location = new System.Drawing.Point(0, 48);
            this.itemForEmissions.Name = "itemForEmissions";
            this.itemForEmissions.Size = new System.Drawing.Size(511, 24);
            this.itemForEmissions.Text = "Emissions:";
            this.itemForEmissions.TextSize = new System.Drawing.Size(137, 13);
            // 
            // itemForRegistrationDate
            // 
            this.itemForRegistrationDate.Control = this.dateEditRegistrationDate;
            this.itemForRegistrationDate.CustomizationFormText = "Registration Date:";
            this.itemForRegistrationDate.Location = new System.Drawing.Point(0, 96);
            this.itemForRegistrationDate.Name = "itemForRegistrationDate";
            this.itemForRegistrationDate.Size = new System.Drawing.Size(511, 28);
            this.itemForRegistrationDate.Text = "Registration Date:";
            this.itemForRegistrationDate.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForFuelTypeID
            // 
            this.ItemForFuelTypeID.Control = this.lueFuelTypeID;
            this.ItemForFuelTypeID.CustomizationFormText = "Fuel Type :";
            this.ItemForFuelTypeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForFuelTypeID.Name = "ItemForFuelTypeID";
            this.ItemForFuelTypeID.Size = new System.Drawing.Size(511, 24);
            this.ItemForFuelTypeID.Text = "Fuel Type:";
            this.ItemForFuelTypeID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForListPrice
            // 
            this.ItemForListPrice.Control = this.spinEditListPrice;
            this.ItemForListPrice.CustomizationFormText = "List Price :";
            this.ItemForListPrice.Location = new System.Drawing.Point(0, 72);
            this.ItemForListPrice.Name = "ItemForListPrice";
            this.ItemForListPrice.Size = new System.Drawing.Size(511, 24);
            this.ItemForListPrice.Text = "List Price :";
            this.ItemForListPrice.TextSize = new System.Drawing.Size(137, 13);
            // 
            // rentalIntervalControlGroup
            // 
            this.rentalIntervalControlGroup.CustomizationFormText = "tabbedControlGroup3";
            this.rentalIntervalControlGroup.Location = new System.Drawing.Point(0, 336);
            this.rentalIntervalControlGroup.Name = "rentalIntervalControlGroup";
            this.rentalIntervalControlGroup.SelectedTabPage = this.layoutControlGroup6;
            this.rentalIntervalControlGroup.SelectedTabPageIndex = 0;
            this.rentalIntervalControlGroup.Size = new System.Drawing.Size(1325, 283);
            this.rentalIntervalControlGroup.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Rental Interval";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup4,
            this.ItemForCustomerReference,
            this.ItemForRate,
            this.ItemForRentalStatusID,
            this.ItemForRentalCategoryID,
            this.ItemForSetAsMain,
            this.ItemForStartDate,
            this.ItemForEndDate,
            this.ItemForRentalSpecificationID,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.ItemForActualEndDate});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1301, 238);
            this.layoutControlGroup6.Text = "Rental Interval";
            // 
            // tabbedControlGroup4
            // 
            this.tabbedControlGroup4.CustomizationFormText = "tabbedControlGroup4";
            this.tabbedControlGroup4.Location = new System.Drawing.Point(606, 23);
            this.tabbedControlGroup4.Name = "tabbedControlGroup4";
            this.tabbedControlGroup4.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup4.SelectedTabPageIndex = 0;
            this.tabbedControlGroup4.Size = new System.Drawing.Size(695, 215);
            this.tabbedControlGroup4.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup7.CaptionImage")));
            this.layoutControlGroup7.CustomizationFormText = "Interval Notes";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(671, 167);
            this.layoutControlGroup7.Text = "Interval Notes";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.memIntervalNotes;
            this.layoutControlItem1.CustomizationFormText = "Notes:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(671, 167);
            this.layoutControlItem1.Text = "Notes:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForCustomerReference
            // 
            this.ItemForCustomerReference.Control = this.txtCustomerReference;
            this.ItemForCustomerReference.CustomizationFormText = "Customer Order Reference :";
            this.ItemForCustomerReference.Location = new System.Drawing.Point(0, 190);
            this.ItemForCustomerReference.Name = "ItemForCustomerReference";
            this.ItemForCustomerReference.Size = new System.Drawing.Size(606, 24);
            this.ItemForCustomerReference.Text = "Customer Order Reference :";
            this.ItemForCustomerReference.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForRate
            // 
            this.ItemForRate.Control = this.spnRate;
            this.ItemForRate.CustomizationFormText = "Monthly Rate :";
            this.ItemForRate.Location = new System.Drawing.Point(0, 214);
            this.ItemForRate.Name = "ItemForRate";
            this.ItemForRate.Size = new System.Drawing.Size(606, 24);
            this.ItemForRate.Text = "Monthly Rate :";
            this.ItemForRate.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForRentalStatusID
            // 
            this.ItemForRentalStatusID.Control = this.lueRentalStatusID;
            this.ItemForRentalStatusID.CustomizationFormText = "Rental Status :";
            this.ItemForRentalStatusID.Location = new System.Drawing.Point(0, 142);
            this.ItemForRentalStatusID.Name = "ItemForRentalStatusID";
            this.ItemForRentalStatusID.Size = new System.Drawing.Size(606, 24);
            this.ItemForRentalStatusID.Text = "Rental Status :";
            this.ItemForRentalStatusID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForRentalCategoryID
            // 
            this.ItemForRentalCategoryID.Control = this.lueRentalCategoryID;
            this.ItemForRentalCategoryID.CustomizationFormText = "Rental Category :";
            this.ItemForRentalCategoryID.Location = new System.Drawing.Point(0, 118);
            this.ItemForRentalCategoryID.Name = "ItemForRentalCategoryID";
            this.ItemForRentalCategoryID.Size = new System.Drawing.Size(606, 24);
            this.ItemForRentalCategoryID.Text = "Rental Category :";
            this.ItemForRentalCategoryID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForSetAsMain
            // 
            this.ItemForSetAsMain.Control = this.ceSetAsMain;
            this.ItemForSetAsMain.CustomizationFormText = "Set As Main";
            this.ItemForSetAsMain.Location = new System.Drawing.Point(0, 95);
            this.ItemForSetAsMain.Name = "ItemForSetAsMain";
            this.ItemForSetAsMain.Size = new System.Drawing.Size(606, 23);
            this.ItemForSetAsMain.Text = "Set As Main";
            this.ItemForSetAsMain.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForSetAsMain.TextVisible = false;
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.deStartDate;
            this.ItemForStartDate.CustomizationFormText = "Start Date :";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 23);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(606, 24);
            this.ItemForStartDate.Text = "Start Date :";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.deEndDate;
            this.ItemForEndDate.CustomizationFormText = "End Date :";
            this.ItemForEndDate.Location = new System.Drawing.Point(0, 47);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(606, 24);
            this.ItemForEndDate.Text = "End Date :";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForRentalSpecificationID
            // 
            this.ItemForRentalSpecificationID.Control = this.lueRentalSpecificationID;
            this.ItemForRentalSpecificationID.CustomizationFormText = "Rental Specification :";
            this.ItemForRentalSpecificationID.Location = new System.Drawing.Point(0, 166);
            this.ItemForRentalSpecificationID.Name = "ItemForRentalSpecificationID";
            this.ItemForRentalSpecificationID.Size = new System.Drawing.Size(606, 24);
            this.ItemForRentalSpecificationID.Text = "Rental Specification :";
            this.ItemForRentalSpecificationID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.dataNavigator2;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(606, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(239, 23);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(606, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(845, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(456, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForActualEndDate
            // 
            this.ItemForActualEndDate.Control = this.deActualEndDate;
            this.ItemForActualEndDate.CustomizationFormText = "Actual End Date :";
            this.ItemForActualEndDate.Location = new System.Drawing.Point(0, 71);
            this.ItemForActualEndDate.Name = "ItemForActualEndDate";
            this.ItemForActualEndDate.Size = new System.Drawing.Size(606, 24);
            this.ItemForActualEndDate.Text = "Actual End Date :";
            this.ItemForActualEndDate.TextSize = new System.Drawing.Size(137, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(253, 0);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(1072, 23);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // spAS11066DepreciationSettingsItemBindingSource
            // 
            this.spAS11066DepreciationSettingsItemBindingSource.DataMember = "sp_AS_11066_Depreciation_Settings_Item";
            this.spAS11066DepreciationSettingsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLEquipmentAvailabilityBindingSource
            // 
            this.spAS11000PLEquipmentAvailabilityBindingSource.DataMember = "sp_AS_11000_PL_Equipment_Availability";
            this.spAS11000PLEquipmentAvailabilityBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.gridControl1);
            this.popupContainerControl1.Controls.Add(this.btnPopUp1Ok);
            this.popupContainerControl1.Location = new System.Drawing.Point(283, 12);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(351, 302);
            this.popupContainerControl1.TabIndex = 5;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.spAS11000FleetManagerDummyScreenBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(0, 3);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(344, 269);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spAS11000FleetManagerDummyScreenBindingSource
            // 
            this.spAS11000FleetManagerDummyScreenBindingSource.DataMember = "sp_AS_11000_FleetManagerDummyScreen";
            this.spAS11000FleetManagerDummyScreenBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colID, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Linked Data";
            this.colDescription.FieldName = "Description";
            this.colDescription.MinWidth = 50;
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 490;
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            // 
            // btnPopUp1Ok
            // 
            this.btnPopUp1Ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPopUp1Ok.Location = new System.Drawing.Point(4, 276);
            this.btnPopUp1Ok.Name = "btnPopUp1Ok";
            this.btnPopUp1Ok.Size = new System.Drawing.Size(75, 23);
            this.btnPopUp1Ok.TabIndex = 0;
            this.btnPopUp1Ok.Text = "Ok";
            this.btnPopUp1Ok.Click += new System.EventHandler(this.btnPopUp1Ok_Click);
            // 
            // sp_AS_11011_Equipment_Category_ListTableAdapter
            // 
            this.sp_AS_11011_Equipment_Category_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Equipment_AvailabilityTableAdapter
            // 
            this.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11014_Make_ListTableAdapter
            // 
            this.sp_AS_11014_Make_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11017_Model_ListTableAdapter
            // 
            this.sp_AS_11017_Model_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11020_Supplier_ListTableAdapter
            // 
            this.sp_AS_11020_Supplier_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11023_Tracker_ListTableAdapter
            // 
            this.sp_AS_11023_Tracker_ListTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11000DuplicateMakeModelSearchBindingSource
            // 
            this.spAS11000DuplicateMakeModelSearchBindingSource.DataMember = "sp_AS_11000_Duplicate_Make_Model_Search";
            this.spAS11000DuplicateMakeModelSearchBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11000DuplicateSearchBindingSource
            // 
            this.spAS11000DuplicateSearchBindingSource.DataMember = "sp_AS_11000_Duplicate_Search";
            this.spAS11000DuplicateSearchBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_Duplicate_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11066_Depreciation_Settings_ItemTableAdapter
            // 
            this.sp_AS_11066_Depreciation_Settings_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Filter";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItem1)});
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.Text = "Filter";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Linked Data :";
            this.barEditItem1.Edit = this.repositoryItemPopupContainerEdit1;
            this.barEditItem1.EditValue = "No Linked Data Filter";
            this.barEditItem1.EditWidth = 353;
            this.barEditItem1.Id = 35;
            this.barEditItem1.Name = "barEditItem1";
            this.barEditItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControl1;
            this.repositoryItemPopupContainerEdit1.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit1_QueryResultValue);
            // 
            // sp_AS_11000_FleetManagerDummyScreenTableAdapter
            // 
            this.sp_AS_11000_FleetManagerDummyScreenTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keeper_Allocation_ItemTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11041_Transaction_ItemTableAdapter
            // 
            this.sp_AS_11041_Transaction_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11050_Depreciation_ItemTableAdapter
            // 
            this.sp_AS_11050_Depreciation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11075_Cover_ItemTableAdapter
            // 
            this.sp_AS_11075_Cover_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11078_Work_Detail_ItemTableAdapter
            // 
            this.sp_AS_11078_Work_Detail_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11081_Incident_ItemTableAdapter
            // 
            this.sp_AS_11081_Incident_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11084_Purpose_ItemTableAdapter
            // 
            this.sp_AS_11084_Purpose_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11089_Fuel_Card_ItemTableAdapter
            // 
            this.sp_AS_11089_Fuel_Card_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11105_Speeding_ItemTableAdapter
            // 
            this.sp_AS_11105_Speeding_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_AS_11044_Keeper_Allocation_ListTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11096_Road_Tax_ItemTableAdapter
            // 
            this.sp_AS_11096_Road_Tax_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11102_Rental_ItemTableAdapter
            // 
            this.sp_AS_11102_Rental_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11108_Rental_Details_ItemTableAdapter
            // 
            this.sp_AS_11108_Rental_Details_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Rental_CategoryTableAdapter
            // 
            this.sp_AS_11000_PL_Rental_CategoryTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Rental_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Rental_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Rental_SpecificationTableAdapter
            // 
            this.sp_AS_11000_PL_Rental_SpecificationTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_Repair_DueTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AS_DataEntry1
            // 
            this.dataSet_AS_DataEntry1.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spAS11000PLSeverityBindingSource
            // 
            this.spAS11000PLSeverityBindingSource.DataMember = "sp_AS_11000_PL_Severity";
            this.spAS11000PLSeverityBindingSource.DataSource = this.dataSet_AS_DataEntry1;
            // 
            // sp_AS_11000_PL_SeverityTableAdapter
            // 
            this.sp_AS_11000_PL_SeverityTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11000PLLegalActionBindingSource
            // 
            this.spAS11000PLLegalActionBindingSource.DataMember = "sp_AS_11000_PL_Legal_Action";
            this.spAS11000PLLegalActionBindingSource.DataSource = this.dataSet_AS_DataEntry1;
            // 
            // sp_AS_11000_PL_Legal_ActionTableAdapter
            // 
            this.sp_AS_11000_PL_Legal_ActionTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11000PLKeeperAtFaultBindingSource
            // 
            this.spAS11000PLKeeperAtFaultBindingSource.DataMember = "sp_AS_11000_PL_Keeper_At_Fault";
            this.spAS11000PLKeeperAtFaultBindingSource.DataSource = this.dataSet_AS_DataEntry1;
            // 
            // sp_AS_11000_PL_Keeper_At_FaultTableAdapter
            // 
            this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11054_Service_Data_ItemTableAdapter
            // 
            this.sp_AS_11054_Service_Data_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status Bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem2, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem4),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem5, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Form Mode: Editing";
            this.barStaticItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem2.Glyph")));
            this.barStaticItem2.Id = 29;
            this.barStaticItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem2.LargeGlyph")));
            this.barStaticItem2.Name = "barStaticItem2";
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItem2.SuperTip = superToolTip4;
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "Records Loaded: 1";
            this.barStaticItem3.Id = 31;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "Changes Pending";
            this.barStaticItem4.Id = 32;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem4.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItem5
            // 
            this.barStaticItem5.Caption = "Information:";
            this.barStaticItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem5.Glyph")));
            this.barStaticItem5.Id = 33;
            this.barStaticItem5.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem5.LargeGlyph")));
            this.barStaticItem5.Name = "barStaticItem5";
            this.barStaticItem5.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar4
            // 
            this.bar4.BarName = "bar1";
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar4.Text = "Screen Functions";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Save";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 27;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Text = "Save Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.barButtonItem1.SuperTip = superToolTip5;
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Cancel";
            this.barButtonItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.Glyph")));
            this.barButtonItem2.Id = 28;
            this.barButtonItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.LargeGlyph")));
            this.barButtonItem2.Name = "barButtonItem2";
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Text = "Cancel Button - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.barButtonItem2.SuperTip = superToolTip6;
            // 
            // bar5
            // 
            this.bar5.BarName = "Filter";
            this.bar5.DockCol = 1;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItem2)});
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.Text = "Filter";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "Linked Data :";
            this.barEditItem2.Edit = this.repositoryItemPopupContainerEdit2;
            this.barEditItem2.EditValue = "No Linked Data Filter";
            this.barEditItem2.EditWidth = 353;
            this.barEditItem2.Id = 35;
            this.barEditItem2.Name = "barEditItem2";
            this.barEditItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            this.repositoryItemPopupContainerEdit2.PopupControl = this.popupContainerControl2;
            this.repositoryItemPopupContainerEdit2.ShowPopupCloseButton = false;
            // 
            // popupContainerControl2
            // 
            this.popupContainerControl2.Controls.Add(this.gridControl2);
            this.popupContainerControl2.Controls.Add(this.simpleButton1);
            this.popupContainerControl2.Location = new System.Drawing.Point(283, 12);
            this.popupContainerControl2.Name = "popupContainerControl2";
            this.popupContainerControl2.Size = new System.Drawing.Size(351, 302);
            this.popupContainerControl2.TabIndex = 5;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.Location = new System.Drawing.Point(0, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(344, 269);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Linked Data";
            this.gridColumn1.FieldName = "Description";
            this.gridColumn1.MinWidth = 50;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 490;
            // 
            // gridColumn2
            // 
            this.gridColumn2.FieldName = "ID";
            this.gridColumn2.Name = "gridColumn2";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton1.Location = new System.Drawing.Point(4, 276);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Ok";
            // 
            // barStaticItem6
            // 
            this.barStaticItem6.Id = -1;
            this.barStaticItem6.Name = "barStaticItem6";
            this.barStaticItem6.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar6
            // 
            this.bar6.BarName = "Status Bar";
            this.bar6.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar6.DockCol = 0;
            this.bar6.DockRow = 0;
            this.bar6.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar6.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem7, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem8),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem9),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem10, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar6.OptionsBar.AllowQuickCustomization = false;
            this.bar6.OptionsBar.DrawDragBorder = false;
            this.bar6.OptionsBar.UseWholeRow = true;
            this.bar6.Text = "Status bar";
            // 
            // barStaticItem7
            // 
            this.barStaticItem7.Caption = "Form Mode: Editing";
            this.barStaticItem7.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem7.Glyph")));
            this.barStaticItem7.Id = 29;
            this.barStaticItem7.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem7.LargeGlyph")));
            this.barStaticItem7.Name = "barStaticItem7";
            toolTipTitleItem7.Text = "Form Mode - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.barStaticItem7.SuperTip = superToolTip7;
            this.barStaticItem7.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem8
            // 
            this.barStaticItem8.Caption = "Records Loaded: 1";
            this.barStaticItem8.Id = 31;
            this.barStaticItem8.Name = "barStaticItem8";
            this.barStaticItem8.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem9
            // 
            this.barStaticItem9.Caption = "Changes Pending";
            this.barStaticItem9.Id = 32;
            this.barStaticItem9.Name = "barStaticItem9";
            this.barStaticItem9.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem9.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItem10
            // 
            this.barStaticItem10.Caption = "Information:";
            this.barStaticItem10.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem10.Glyph")));
            this.barStaticItem10.Id = 33;
            this.barStaticItem10.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem10.LargeGlyph")));
            this.barStaticItem10.Name = "barStaticItem10";
            this.barStaticItem10.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar7
            // 
            this.bar7.BarName = "bar1";
            this.bar7.DockCol = 0;
            this.bar7.DockRow = 0;
            this.bar7.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar7.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar7.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4)});
            this.bar7.Text = "Screen Functions";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Save";
            this.barButtonItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.Glyph")));
            this.barButtonItem3.Id = 27;
            this.barButtonItem3.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.LargeGlyph")));
            this.barButtonItem3.Name = "barButtonItem3";
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Text = "Save Button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.barButtonItem3.SuperTip = superToolTip8;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Cancel";
            this.barButtonItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.Glyph")));
            this.barButtonItem4.Id = 28;
            this.barButtonItem4.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.LargeGlyph")));
            this.barButtonItem4.Name = "barButtonItem4";
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.Text = "Cancel Button - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.barButtonItem4.SuperTip = superToolTip9;
            // 
            // bar8
            // 
            this.bar8.BarName = "Filter";
            this.bar8.DockCol = 1;
            this.bar8.DockRow = 0;
            this.bar8.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar8.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItem3)});
            this.bar8.OptionsBar.DrawDragBorder = false;
            this.bar8.Text = "Filter";
            // 
            // barEditItem3
            // 
            this.barEditItem3.Caption = "Linked Data :";
            this.barEditItem3.Edit = this.repositoryItemPopupContainerEdit3;
            this.barEditItem3.EditValue = "No Linked Data Filter";
            this.barEditItem3.EditWidth = 353;
            this.barEditItem3.Id = 35;
            this.barEditItem3.Name = "barEditItem3";
            this.barEditItem3.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit3
            // 
            this.repositoryItemPopupContainerEdit3.AutoHeight = false;
            this.repositoryItemPopupContainerEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit3.Name = "repositoryItemPopupContainerEdit3";
            this.repositoryItemPopupContainerEdit3.PopupControl = this.popupContainerControl3;
            this.repositoryItemPopupContainerEdit3.ShowPopupCloseButton = false;
            // 
            // popupContainerControl3
            // 
            this.popupContainerControl3.Controls.Add(this.gridControl3);
            this.popupContainerControl3.Controls.Add(this.simpleButton2);
            this.popupContainerControl3.Location = new System.Drawing.Point(283, 12);
            this.popupContainerControl3.Name = "popupContainerControl3";
            this.popupContainerControl3.Size = new System.Drawing.Size(351, 302);
            this.popupContainerControl3.TabIndex = 5;
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.Location = new System.Drawing.Point(0, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(344, 269);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn4});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Linked Data";
            this.gridColumn3.FieldName = "Description";
            this.gridColumn3.MinWidth = 50;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 490;
            // 
            // gridColumn4
            // 
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton2.Location = new System.Drawing.Point(4, 276);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 0;
            this.simpleButton2.Text = "Ok";
            // 
            // barStaticItem11
            // 
            this.barStaticItem11.Id = -1;
            this.barStaticItem11.Name = "barStaticItem11";
            this.barStaticItem11.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar9
            // 
            this.bar9.BarName = "Status Bar";
            this.bar9.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar9.DockCol = 0;
            this.bar9.DockRow = 0;
            this.bar9.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar9.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem12, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem13),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem14),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem15, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar9.OptionsBar.AllowQuickCustomization = false;
            this.bar9.OptionsBar.DrawDragBorder = false;
            this.bar9.OptionsBar.UseWholeRow = true;
            this.bar9.Text = "Status bar";
            // 
            // barStaticItem12
            // 
            this.barStaticItem12.Caption = "Form Mode: Editing";
            this.barStaticItem12.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem12.Glyph")));
            this.barStaticItem12.Id = 29;
            this.barStaticItem12.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem12.LargeGlyph")));
            this.barStaticItem12.Name = "barStaticItem12";
            toolTipTitleItem10.Text = "Form Mode - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.barStaticItem12.SuperTip = superToolTip10;
            this.barStaticItem12.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem13
            // 
            this.barStaticItem13.Caption = "Records Loaded: 1";
            this.barStaticItem13.Id = 31;
            this.barStaticItem13.Name = "barStaticItem13";
            this.barStaticItem13.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem14
            // 
            this.barStaticItem14.Caption = "Changes Pending";
            this.barStaticItem14.Id = 32;
            this.barStaticItem14.Name = "barStaticItem14";
            this.barStaticItem14.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem14.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItem15
            // 
            this.barStaticItem15.Caption = "Information:";
            this.barStaticItem15.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem15.Glyph")));
            this.barStaticItem15.Id = 33;
            this.barStaticItem15.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem15.LargeGlyph")));
            this.barStaticItem15.Name = "barStaticItem15";
            this.barStaticItem15.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar10
            // 
            this.bar10.BarName = "bar1";
            this.bar10.DockCol = 0;
            this.bar10.DockRow = 0;
            this.bar10.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar10.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar10.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6)});
            this.bar10.Text = "Screen Functions";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Save";
            this.barButtonItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.Glyph")));
            this.barButtonItem5.Id = 27;
            this.barButtonItem5.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.LargeGlyph")));
            this.barButtonItem5.Name = "barButtonItem5";
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.Text = "Save Button - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.barButtonItem5.SuperTip = superToolTip11;
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Cancel";
            this.barButtonItem6.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.Glyph")));
            this.barButtonItem6.Id = 28;
            this.barButtonItem6.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.LargeGlyph")));
            this.barButtonItem6.Name = "barButtonItem6";
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.Text = "Cancel Button - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.barButtonItem6.SuperTip = superToolTip12;
            // 
            // bar11
            // 
            this.bar11.BarName = "Filter";
            this.bar11.DockCol = 1;
            this.bar11.DockRow = 0;
            this.bar11.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar11.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItem4)});
            this.bar11.OptionsBar.DrawDragBorder = false;
            this.bar11.Text = "Filter";
            // 
            // barEditItem4
            // 
            this.barEditItem4.Caption = "Linked Data :";
            this.barEditItem4.Edit = this.repositoryItemPopupContainerEdit4;
            this.barEditItem4.EditValue = "No Linked Data Filter";
            this.barEditItem4.EditWidth = 353;
            this.barEditItem4.Id = 35;
            this.barEditItem4.Name = "barEditItem4";
            this.barEditItem4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEdit4
            // 
            this.repositoryItemPopupContainerEdit4.AutoHeight = false;
            this.repositoryItemPopupContainerEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit4.Name = "repositoryItemPopupContainerEdit4";
            this.repositoryItemPopupContainerEdit4.PopupControl = this.popupContainerControl4;
            this.repositoryItemPopupContainerEdit4.ShowPopupCloseButton = false;
            // 
            // popupContainerControl4
            // 
            this.popupContainerControl4.Controls.Add(this.gridControl4);
            this.popupContainerControl4.Controls.Add(this.simpleButton3);
            this.popupContainerControl4.Location = new System.Drawing.Point(283, 12);
            this.popupContainerControl4.Name = "popupContainerControl4";
            this.popupContainerControl4.Size = new System.Drawing.Size(351, 302);
            this.popupContainerControl4.TabIndex = 5;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.Location = new System.Drawing.Point(0, 3);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(344, 269);
            this.gridControl4.TabIndex = 1;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Linked Data";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.MinWidth = 50;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 490;
            // 
            // gridColumn6
            // 
            this.gridColumn6.FieldName = "ID";
            this.gridColumn6.Name = "gridColumn6";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.simpleButton3.Location = new System.Drawing.Point(4, 276);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 0;
            this.simpleButton3.Text = "Ok";
            // 
            // barStaticItem16
            // 
            this.barStaticItem16.Id = -1;
            this.barStaticItem16.Name = "barStaticItem16";
            this.barStaticItem16.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // sp_AS_11000_PL_FuelTableAdapter
            // 
            this.sp_AS_11000_PL_FuelTableAdapter.ClearBeforeFill = true;
            // 
            // bar12
            // 
            this.bar12.BarName = "Status Bar";
            this.bar12.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar12.DockCol = 0;
            this.bar12.DockRow = 0;
            this.bar12.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar12.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem17, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem18),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem19),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItem20, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar12.OptionsBar.AllowQuickCustomization = false;
            this.bar12.OptionsBar.DrawDragBorder = false;
            this.bar12.OptionsBar.UseWholeRow = true;
            this.bar12.Text = "Status bar";
            // 
            // barStaticItem17
            // 
            this.barStaticItem17.Caption = "Form Mode: Editing";
            this.barStaticItem17.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem17.Glyph")));
            this.barStaticItem17.Id = 29;
            this.barStaticItem17.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem17.LargeGlyph")));
            this.barStaticItem17.Name = "barStaticItem17";
            toolTipTitleItem13.Text = "Form Mode - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.barStaticItem17.SuperTip = superToolTip13;
            this.barStaticItem17.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem18
            // 
            this.barStaticItem18.Caption = "Records Loaded: 1";
            this.barStaticItem18.Id = 31;
            this.barStaticItem18.Name = "barStaticItem18";
            this.barStaticItem18.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem19
            // 
            this.barStaticItem19.Caption = "Changes Pending";
            this.barStaticItem19.Id = 32;
            this.barStaticItem19.Name = "barStaticItem19";
            this.barStaticItem19.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem19.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItem20
            // 
            this.barStaticItem20.Caption = "Information:";
            this.barStaticItem20.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem20.Glyph")));
            this.barStaticItem20.Id = 33;
            this.barStaticItem20.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem20.LargeGlyph")));
            this.barStaticItem20.Name = "barStaticItem20";
            this.barStaticItem20.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar13
            // 
            this.bar13.BarName = "bar1";
            this.bar13.DockCol = 0;
            this.bar13.DockRow = 0;
            this.bar13.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar13.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar13.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8)});
            this.bar13.Text = "Screen Functions";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Save";
            this.barButtonItem7.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem7.Glyph")));
            this.barButtonItem7.Id = 27;
            this.barButtonItem7.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem7.LargeGlyph")));
            this.barButtonItem7.Name = "barButtonItem7";
            toolTipTitleItem14.Appearance.Options.UseImage = true;
            toolTipTitleItem14.Text = "Save Button - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.barButtonItem7.SuperTip = superToolTip14;
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Cancel";
            this.barButtonItem8.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.Glyph")));
            this.barButtonItem8.Id = 28;
            this.barButtonItem8.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.LargeGlyph")));
            this.barButtonItem8.Name = "barButtonItem8";
            toolTipTitleItem15.Appearance.Options.UseImage = true;
            toolTipTitleItem15.Text = "Cancel Button - Information";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.barButtonItem8.SuperTip = superToolTip15;
            // 
            // barStaticItem21
            // 
            this.barStaticItem21.Id = -1;
            this.barStaticItem21.Name = "barStaticItem21";
            this.barStaticItem21.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // frm_AS_Rental_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1362, 740);
            this.Controls.Add(this.equipmentDataLayoutControl);
            this.Controls.Add(this.popupContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Rental_Edit";
            this.Text = " Rental Edit";
            this.Activated += new System.EventHandler(this.frm_AS_Rental_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Rental_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Rental_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.popupContainerControl1, 0);
            this.Controls.SetChildIndex(this.equipmentDataLayoutControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11011EquipmentCategoryListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentDataLayoutControl)).EndInit();
            this.equipmentDataLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spAS11108RentalDetailsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memIntervalNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentChildTabControl)).EndInit();
            this.equipmentChildTabControl.ResumeLayout(false);
            this.roadTaxTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11096RoadTaxItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roadTaxGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit4)).EndInit();
            this.keeperTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperNotes)).EndInit();
            this.transactionTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.transactionGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOnExchequer)).EndInit();
            this.purposeTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11084PurposeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).EndInit();
            this.fuelCardTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11089FuelCardItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCardGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit19)).EndInit();
            this.trackerTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11023TrackerListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackerGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            this.billingTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.billingGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11050DepreciationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.depreciationTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEditDep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            this.coverTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.coverGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11075CoverItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coverGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit1)).EndInit();
            this.serviceIntervalTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.serviceIntervalGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceDataItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceIntervalGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            this.workDetailTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).EndInit();
            this.incidentTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentNotesMemoEdit)).EndInit();
            this.speedingTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11105SpeedingItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speedingGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceReported)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11102RentalItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEquipmentCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMakeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11014MakeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueModelID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentOwnershipStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUniqueReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memRentalNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModelSpecifications.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalSpecificationID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalSpecificationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActualEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActualEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSetAsMain.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEngineSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEmissions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditRegistrationDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditRegistrationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueFuelTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLFuelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditListPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkedChildDataGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentalEquipControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentalLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnershipStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMakeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModelID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModelSpecifications)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUniqueReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemForEngineSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemForEmissions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemForRegistrationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFuelTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForListPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rentalIntervalControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSetAsMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalSpecificationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11066DepreciationSettingsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentAvailabilityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000FleetManagerDummyScreenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateMakeModelSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSeverityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLLegalActionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperAtFaultBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).EndInit();
            this.popupContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl3)).EndInit();
            this.popupContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl4)).EndInit();
            this.popupContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl equipmentDataLayoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DataNavigator equipmentDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private System.Windows.Forms.BindingSource spAS11023TrackerListBindingSource;
        private System.Windows.Forms.BindingSource spAS11011EquipmentCategoryListBindingSource;
        private System.Windows.Forms.BindingSource spAS11014MakeListBindingSource;
        private System.Windows.Forms.BindingSource spAS11017ModelListBindingSource;
        private System.Windows.Forms.BindingSource spAS11020SupplierListBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLEquipmentOwnershipStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLEquipmentAvailabilityBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11011_Equipment_Category_ListTableAdapter sp_AS_11011_Equipment_Category_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter sp_AS_11000_PL_Equipment_AvailabilityTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter sp_AS_11014_Make_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter sp_AS_11017_Model_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter sp_AS_11020_Supplier_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11023_Tracker_ListTableAdapter sp_AS_11023_Tracker_ListTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000DuplicateMakeModelSearchBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000DuplicateSearchBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter sp_AS_11000_Duplicate_SearchTableAdapter;
        private System.Windows.Forms.BindingSource spAS11066DepreciationSettingsItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11066_Depreciation_Settings_ItemTableAdapter sp_AS_11066_Depreciation_Settings_ItemTableAdapter;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnPopUp1Ok;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource spAS11000FleetManagerDummyScreenBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_FleetManagerDummyScreenTableAdapter sp_AS_11000_FleetManagerDummyScreenTableAdapter;
        private DevExpress.XtraTab.XtraTabControl equipmentChildTabControl;
        private DevExpress.XtraTab.XtraTabPage roadTaxTabPage;
        private DevExpress.XtraGrid.GridControl roadTaxGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView roadTaxGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadTaxID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference12;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID14;
        private DevExpress.XtraGrid.Columns.GridColumn colTaxExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAmount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colTaxPeriodID;
        private DevExpress.XtraGrid.Columns.GridColumn colTaxPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colMode13;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID13;
        private DevExpress.XtraTab.XtraTabPage trackerTabPage;
        private DevExpress.XtraGrid.GridControl trackerGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView trackerGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colTrackerInformationID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference6;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistration;
        private DevExpress.XtraGrid.Columns.GridColumn colTrackerVID;
        private DevExpress.XtraGrid.Columns.GridColumn colTrackerReference;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID8;
        private DevExpress.XtraGrid.Columns.GridColumn colMake1;
        private DevExpress.XtraGrid.Columns.GridColumn colModel1;
        private DevExpress.XtraGrid.Columns.GridColumn colOdometerMetres;
        private DevExpress.XtraGrid.Columns.GridColumn colOdometerMiles;
        private DevExpress.XtraGrid.Columns.GridColumn colReason;
        private DevExpress.XtraGrid.Columns.GridColumn colMPG;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLatestStartJourney;
        private DevExpress.XtraGrid.Columns.GridColumn colLatestEndJourney;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedKPH;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedMPH;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageSpeed;
        private DevExpress.XtraGrid.Columns.GridColumn colPlaceName;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadName;
        private DevExpress.XtraGrid.Columns.GridColumn colPostSect;
        private DevExpress.XtraGrid.Columns.GridColumn colGeofenceName;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrict;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdate;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverName;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colDriverEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colMode4;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private DevExpress.XtraTab.XtraTabPage keeperTabPage;
        private DevExpress.XtraGrid.GridControl keeperGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView keeperGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAllocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference9;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID11;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode7;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit lueKeeperNotes;
        private DevExpress.XtraTab.XtraTabPage transactionTabPage;
        private DevExpress.XtraGrid.GridControl transactionGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView transactionsGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference10;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID12;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionType;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedOnExchequer;
        private DevExpress.XtraGrid.Columns.GridColumn colNetTransactionPrice;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colMode8;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceOnExchequer;
        private DevExpress.XtraTab.XtraTabPage billingTabPage;
        private DevExpress.XtraGrid.GridControl billingGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView billingGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colNarrative1;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceSheetCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colProfitLossCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationAmount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colPreviousValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode9;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraTab.XtraTabPage depreciationTabPage;
        private DevExpress.XtraGrid.GridControl depreciationGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView depreciationGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colNarrative;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceSheetCode;
        private DevExpress.XtraGrid.Columns.GridColumn colProfitLossCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationAmount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEditDep;
        private DevExpress.XtraGrid.Columns.GridColumn colPreviousValue;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraTab.XtraTabPage coverTabPage;
        private DevExpress.XtraGrid.GridControl coverGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView coverGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colCoverID;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference16;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID3;
        private DevExpress.XtraGrid.Columns.GridColumn colCoverType;
        private DevExpress.XtraGrid.Columns.GridColumn colCoverTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colPolicy;
        private DevExpress.XtraGrid.Columns.GridColumn colAnnualCoverCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRenewalDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes3;
        private DevExpress.XtraGrid.Columns.GridColumn colExcess;
        private DevExpress.XtraGrid.Columns.GridColumn colMode11;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID11;
        private DevExpress.XtraTab.XtraTabPage serviceIntervalTabPage;
        private DevExpress.XtraGrid.GridControl serviceIntervalGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView serviceIntervalGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraTab.XtraTabPage workDetailTabPage;
        private DevExpress.XtraGrid.GridControl workDetailGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView workDetailGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkDetailID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID2;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference11;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentMileage;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkCompletionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceIntervalID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceType;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes2;
        private DevExpress.XtraGrid.Columns.GridColumn colMode10;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID10;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit13;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit14;
        private DevExpress.XtraTab.XtraTabPage incidentTabPage;
        private DevExpress.XtraGrid.GridControl incidentGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView incidentGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID7;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference5;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentType;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colDateHappened;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colRepairDueID;
        private DevExpress.XtraGrid.Columns.GridColumn colRepairDue;
        private DevExpress.XtraGrid.Columns.GridColumn colSeverityID;
        private DevExpress.XtraGrid.Columns.GridColumn colSeverity;
        private DevExpress.XtraGrid.Columns.GridColumn colWitness;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAtFaultID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAtFault;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalAction;
        private DevExpress.XtraGrid.Columns.GridColumn colFollowUpDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit incidentNotesMemoEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colMode3;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID3;
        private DevExpress.XtraTab.XtraTabPage purposeTabPage;
        private DevExpress.XtraGrid.GridControl purposeGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView purposeGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentPurposeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID15;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference13;
        private DevExpress.XtraGrid.Columns.GridColumn colPurpose;
        private DevExpress.XtraGrid.Columns.GridColumn colPurposeID;
        private DevExpress.XtraGrid.Columns.GridColumn colMode14;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit16;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit17;
        private DevExpress.XtraTab.XtraTabPage fuelCardTabPage;
        private DevExpress.XtraGrid.GridControl fuelCardGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView fuelCardGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelCardID;
        private DevExpress.XtraGrid.Columns.GridColumn colCardNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationMark;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference14;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID16;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeper1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAllocationID1;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference2;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID2;
        private DevExpress.XtraGrid.Columns.GridColumn colCardStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCardStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes5;
        private DevExpress.XtraGrid.Columns.GridColumn colMode15;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID15;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit18;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit19;
        private DevExpress.XtraTab.XtraTabPage speedingTabPage;
        private DevExpress.XtraGrid.GridControl speedingGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView speedingGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedingID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference17;
        private DevExpress.XtraGrid.Columns.GridColumn colVID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID17;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeed;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedMPH1;
        private DevExpress.XtraGrid.Columns.GridColumn colTravelTime;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadType;
        private DevExpress.XtraGrid.Columns.GridColumn colPlaceName1;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrict1;
        private DevExpress.XtraGrid.Columns.GridColumn colReported;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceReported;
        private DevExpress.XtraGrid.Columns.GridColumn colMode17;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID17;
        private System.Windows.Forms.BindingSource spAS11044KeeperAllocationItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11041TransactionItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter sp_AS_11044_Keeper_Allocation_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter sp_AS_11041_Transaction_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11050DepreciationItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11050_Depreciation_ItemTableAdapter sp_AS_11050_Depreciation_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11075CoverItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11078WorkDetailItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11081IncidentItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11084PurposeItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11089FuelCardItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11105SpeedingItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11075_Cover_ItemTableAdapter sp_AS_11075_Cover_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter sp_AS_11078_Work_Detail_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter sp_AS_11081_Incident_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11084_Purpose_ItemTableAdapter sp_AS_11084_Purpose_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11089_Fuel_Card_ItemTableAdapter sp_AS_11089_Fuel_Card_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11105_Speeding_ItemTableAdapter sp_AS_11105_Speeding_ItemTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.TabbedControlGroup linkedChildDataGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DataSet_AT dataSet_AT;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ListTableAdapter sp_AS_11044_Keeper_Allocation_ListTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource spAS11096RoadTaxItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11096_Road_Tax_ItemTableAdapter sp_AS_11096_Road_Tax_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private System.Windows.Forms.BindingSource spAS11102RentalItemBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueEquipmentCategoryID;
        private DevExpress.XtraEditors.LookUpEdit lueMakeID;
        private DevExpress.XtraEditors.LookUpEdit lueModelID;
        private DevExpress.XtraEditors.LookUpEdit lueStatusID;
        private DevExpress.XtraEditors.LookUpEdit lueSupplierID;
        private DevExpress.XtraEditors.TextEdit txtUniqueReference;
        private DevExpress.XtraEditors.MemoEdit memRentalNotes;
        private DevExpress.XtraEditors.TextEdit txtModelSpecifications;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11102_Rental_ItemTableAdapter sp_AS_11102_Rental_ItemTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueRentalCategoryID;
        private System.Windows.Forms.BindingSource spAS11108RentalDetailsItemBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueRentalStatusID;
        private DevExpress.XtraEditors.LookUpEdit lueRentalSpecificationID;
        private DevExpress.XtraEditors.SpinEdit spnRate;
        private DevExpress.XtraEditors.TextEdit txtCustomerReference;
        private DevExpress.XtraEditors.DateEdit deStartDate;
        private DevExpress.XtraEditors.DateEdit deActualEndDate;
        private DevExpress.XtraEditors.DateEdit deEndDate;
        private DevExpress.XtraEditors.CheckEdit ceSetAsMain;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.TabbedControlGroup rentalEquipControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup rentalLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUniqueReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSupplierID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnershipStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMakeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForModelID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentCategoryID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForModelSpecifications;
        private DevExpress.XtraLayout.TabbedControlGroup rentalIntervalControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRentalSpecificationID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRentalStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRentalCategoryID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSetAsMain;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11108_Rental_Details_ItemTableAdapter sp_AS_11108_Rental_Details_ItemTableAdapter;
        private DevExpress.XtraEditors.MemoEdit memIntervalNotes;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private System.Windows.Forms.BindingSource spAS11000PLRentalCategoryBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLRentalStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLRentalSpecificationBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_CategoryTableAdapter sp_AS_11000_PL_Rental_CategoryTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_StatusTableAdapter sp_AS_11000_PL_Rental_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_SpecificationTableAdapter sp_AS_11000_PL_Rental_SpecificationTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_StatusTableAdapter sp_AS_11000_PL_Incident_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_TypeTableAdapter sp_AS_11000_PL_Incident_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_Repair_DueTableAdapter sp_AS_11000_PL_Incident_Repair_DueTableAdapter;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry1;
        private System.Windows.Forms.BindingSource spAS11000PLSeverityBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_SeverityTableAdapter sp_AS_11000_PL_SeverityTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLLegalActionBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Legal_ActionTableAdapter sp_AS_11000_PL_Legal_ActionTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLKeeperAtFaultBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_At_FaultTableAdapter sp_AS_11000_PL_Keeper_At_FaultTableAdapter;
        private System.Windows.Forms.BindingSource spAS11054ServiceDataItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDataID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceIntervalScheduleID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID4;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference2;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceType1;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceUnitsID;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colDistanceFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeUnitsID;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colWarrantyPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colWarrantyEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWarrantyEndDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDataNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDueDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAlertWithinXDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colAlertWithinXTime;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType1;
        private DevExpress.XtraGrid.Columns.GridColumn colIntervalScheduleNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colIsCurrentInterval;
        private DevExpress.XtraGrid.Columns.GridColumn colMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID1;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Data_ItemTableAdapter sp_AS_11054_Service_Data_ItemTableAdapter;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraEditors.SpinEdit spinEditEngineSize;
        private DevExpress.XtraLayout.LayoutControlItem itemForEngineSize;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem5;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem6;
        private DevExpress.XtraEditors.SpinEdit spinEditEmissions;
        private DevExpress.XtraLayout.LayoutControlItem itemForEmissions;
        private DevExpress.XtraBars.Bar bar6;
        private DevExpress.XtraBars.BarStaticItem barStaticItem7;
        private DevExpress.XtraBars.BarStaticItem barStaticItem8;
        private DevExpress.XtraBars.BarStaticItem barStaticItem9;
        private DevExpress.XtraBars.BarStaticItem barStaticItem10;
        private DevExpress.XtraBars.Bar bar7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.Bar bar8;
        private DevExpress.XtraBars.BarEditItem barEditItem3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl3;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem11;
        private DevExpress.XtraEditors.DateEdit dateEditRegistrationDate;
        private DevExpress.XtraLayout.LayoutControlItem itemForRegistrationDate;
        private DevExpress.XtraBars.Bar bar9;
        private DevExpress.XtraBars.BarStaticItem barStaticItem12;
        private DevExpress.XtraBars.BarStaticItem barStaticItem13;
        private DevExpress.XtraBars.BarStaticItem barStaticItem14;
        private DevExpress.XtraBars.BarStaticItem barStaticItem15;
        private DevExpress.XtraBars.Bar bar10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.Bar bar11;
        private DevExpress.XtraBars.BarEditItem barEditItem4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit4;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl4;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem16;
        private DevExpress.XtraEditors.LookUpEdit lueFuelTypeID;
        private System.Windows.Forms.BindingSource spAS11000PLFuelBindingSource;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFuelTypeID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_FuelTableAdapter sp_AS_11000_PL_FuelTableAdapter;
        private DevExpress.XtraEditors.SpinEdit spinEditListPrice;
        private DevExpress.XtraLayout.LayoutControlItem ItemForListPrice;
        private DevExpress.XtraBars.Bar bar12;
        private DevExpress.XtraBars.BarStaticItem barStaticItem17;
        private DevExpress.XtraBars.BarStaticItem barStaticItem18;
        private DevExpress.XtraBars.BarStaticItem barStaticItem19;
        private DevExpress.XtraBars.BarStaticItem barStaticItem20;
        private DevExpress.XtraBars.Bar bar13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarStaticItem barStaticItem21;
    }
}

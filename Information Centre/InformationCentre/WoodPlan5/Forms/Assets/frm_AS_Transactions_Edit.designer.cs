﻿namespace WoodPlan5
{
    partial class frm_AS_Transactions_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Transactions_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11041TransactionItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.lueTransactionTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLTransactionTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deTransactionDate = new DevExpress.XtraEditors.DateEdit();
            this.txtTransactionOrderNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtPurchaseInvoice = new DevExpress.XtraEditors.TextEdit();
            this.memDescription = new DevExpress.XtraEditors.MemoEdit();
            this.ceCompletedOnExchequer = new DevExpress.XtraEditors.CheckEdit();
            this.spnNetTransactionPrice = new DevExpress.XtraEditors.SpinEdit();
            this.spnVAT = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransactionTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransactionOrderNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCompletedOnExchequer = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransactionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPurchaseInvoice = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNetTransactionPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVAT = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11041_Transaction_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter();
            this.sp_AS_11000_PL_Transaction_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Transaction_TypeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTransactionTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLTransactionTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTransactionDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTransactionDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionOrderNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPurchaseInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceCompletedOnExchequer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnNetTransactionPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransactionTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransactionOrderNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletedOnExchequer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransactionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPurchaseInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNetTransactionPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(871, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 528);
            this.barDockControlBottom.Size = new System.Drawing.Size(871, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 502);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(871, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 502);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(871, 502);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(129, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(263, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11041TransactionItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(141, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // spAS11041TransactionItemBindingSource
            // 
            this.spAS11041TransactionItemBindingSource.DataMember = "sp_AS_11041_Transaction_Item";
            this.spAS11041TransactionItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEquipmentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueTransactionTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deTransactionDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtTransactionOrderNumber);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtPurchaseInvoice);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memDescription);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceCompletedOnExchequer);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnNetTransactionPrice);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnVAT);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11041TransactionItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(743, 232, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(871, 502);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11041TransactionItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(125, 35);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(734, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling1);
            this.txtEquipmentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEquipmentReference.TabIndex = 123;
            // 
            // lueTransactionTypeID
            // 
            this.lueTransactionTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11041TransactionItemBindingSource, "TransactionTypeID", true));
            this.lueTransactionTypeID.Location = new System.Drawing.Point(125, 59);
            this.lueTransactionTypeID.MenuManager = this.barManager1;
            this.lueTransactionTypeID.Name = "lueTransactionTypeID";
            this.lueTransactionTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTransactionTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Transaction Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueTransactionTypeID.Properties.DataSource = this.spAS11000PLTransactionTypeBindingSource;
            this.lueTransactionTypeID.Properties.DisplayMember = "Value";
            this.lueTransactionTypeID.Properties.NullText = "";
            this.lueTransactionTypeID.Properties.ValueMember = "PickListID";
            this.lueTransactionTypeID.Size = new System.Drawing.Size(307, 20);
            this.lueTransactionTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueTransactionTypeID.TabIndex = 124;
            this.lueTransactionTypeID.Tag = "Transaction Type";
            this.lueTransactionTypeID.EditValueChanged += new System.EventHandler(this.lueTransactionTypeID_EditValueChanged);
            this.lueTransactionTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lueTransactionTypeID_Validating);
            // 
            // spAS11000PLTransactionTypeBindingSource
            // 
            this.spAS11000PLTransactionTypeBindingSource.DataMember = "sp_AS_11000_PL_Transaction_Type";
            this.spAS11000PLTransactionTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deTransactionDate
            // 
            this.deTransactionDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11041TransactionItemBindingSource, "TransactionDate", true));
            this.deTransactionDate.EditValue = null;
            this.deTransactionDate.Location = new System.Drawing.Point(549, 59);
            this.deTransactionDate.MenuManager = this.barManager1;
            this.deTransactionDate.Name = "deTransactionDate";
            this.deTransactionDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deTransactionDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deTransactionDate.Size = new System.Drawing.Size(310, 20);
            this.deTransactionDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deTransactionDate.TabIndex = 125;
            this.deTransactionDate.Tag = "Transaction Date";
            this.deTransactionDate.Validating += new System.ComponentModel.CancelEventHandler(this.deTransactionDate_Validating);
            // 
            // txtTransactionOrderNumber
            // 
            this.txtTransactionOrderNumber.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11041TransactionItemBindingSource, "TransactionOrderNumber", true));
            this.txtTransactionOrderNumber.Location = new System.Drawing.Point(125, 83);
            this.txtTransactionOrderNumber.MenuManager = this.barManager1;
            this.txtTransactionOrderNumber.Name = "txtTransactionOrderNumber";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtTransactionOrderNumber, true);
            this.txtTransactionOrderNumber.Size = new System.Drawing.Size(307, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtTransactionOrderNumber, optionsSpelling2);
            this.txtTransactionOrderNumber.StyleController = this.editFormDataLayoutControlGroup;
            this.txtTransactionOrderNumber.TabIndex = 126;
            this.txtTransactionOrderNumber.Tag = "Order Number";
            this.txtTransactionOrderNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtTransactionOrderNumber_Validating);
            // 
            // txtPurchaseInvoice
            // 
            this.txtPurchaseInvoice.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11041TransactionItemBindingSource, "PurchaseInvoice", true));
            this.txtPurchaseInvoice.Location = new System.Drawing.Point(549, 83);
            this.txtPurchaseInvoice.MenuManager = this.barManager1;
            this.txtPurchaseInvoice.Name = "txtPurchaseInvoice";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtPurchaseInvoice, true);
            this.txtPurchaseInvoice.Size = new System.Drawing.Size(310, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtPurchaseInvoice, optionsSpelling3);
            this.txtPurchaseInvoice.StyleController = this.editFormDataLayoutControlGroup;
            this.txtPurchaseInvoice.TabIndex = 127;
            this.txtPurchaseInvoice.Tag = "Purchase Invoice";
            // 
            // memDescription
            // 
            this.memDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11041TransactionItemBindingSource, "Description", true));
            this.memDescription.Location = new System.Drawing.Point(24, 167);
            this.memDescription.MenuManager = this.barManager1;
            this.memDescription.Name = "memDescription";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memDescription, true);
            this.memDescription.Size = new System.Drawing.Size(823, 288);
            this.scSpellChecker.SetSpellCheckerOptions(this.memDescription, optionsSpelling4);
            this.memDescription.StyleController = this.editFormDataLayoutControlGroup;
            this.memDescription.TabIndex = 128;
            // 
            // ceCompletedOnExchequer
            // 
            this.ceCompletedOnExchequer.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11041TransactionItemBindingSource, "CompletedOnExchequer", true));
            this.ceCompletedOnExchequer.Location = new System.Drawing.Point(12, 471);
            this.ceCompletedOnExchequer.MenuManager = this.barManager1;
            this.ceCompletedOnExchequer.Name = "ceCompletedOnExchequer";
            this.ceCompletedOnExchequer.Properties.Caption = "Completed On Exchequer";
            this.ceCompletedOnExchequer.Size = new System.Drawing.Size(847, 19);
            this.ceCompletedOnExchequer.StyleController = this.editFormDataLayoutControlGroup;
            this.ceCompletedOnExchequer.TabIndex = 129;
            this.ceCompletedOnExchequer.Tag = "Completed On Exchequer";
            // 
            // spnNetTransactionPrice
            // 
            this.spnNetTransactionPrice.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11041TransactionItemBindingSource, "NetTransactionPrice", true));
            this.spnNetTransactionPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnNetTransactionPrice.Location = new System.Drawing.Point(125, 107);
            this.spnNetTransactionPrice.MenuManager = this.barManager1;
            this.spnNetTransactionPrice.Name = "spnNetTransactionPrice";
            this.spnNetTransactionPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnNetTransactionPrice.Properties.DisplayFormat.FormatString = "c2";
            this.spnNetTransactionPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnNetTransactionPrice.Properties.MaxValue = new decimal(new int[] {
            -1530494976,
            232830,
            0,
            0});
            this.spnNetTransactionPrice.Properties.NullText = "0.00";
            this.spnNetTransactionPrice.Properties.NullValuePrompt = "Please Enter Net Price";
            this.spnNetTransactionPrice.Size = new System.Drawing.Size(307, 20);
            this.spnNetTransactionPrice.StyleController = this.editFormDataLayoutControlGroup;
            this.spnNetTransactionPrice.TabIndex = 130;
            this.spnNetTransactionPrice.Tag = "Net Price";
            this.spnNetTransactionPrice.EditValueChanged += new System.EventHandler(this.spnNetTransactionPrice_EditValueChanged);
            this.spnNetTransactionPrice.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditControl_Validating);
            // 
            // spnVAT
            // 
            this.spnVAT.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11041TransactionItemBindingSource, "VAT", true));
            this.spnVAT.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnVAT.Location = new System.Drawing.Point(549, 107);
            this.spnVAT.MenuManager = this.barManager1;
            this.spnVAT.Name = "spnVAT";
            this.spnVAT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnVAT.Properties.DisplayFormat.FormatString = "c2";
            this.spnVAT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnVAT.Properties.MaxValue = new decimal(new int[] {
            -1530494976,
            232830,
            0,
            0});
            this.spnVAT.Properties.NullText = "0.00";
            this.spnVAT.Properties.NullValuePrompt = "Please Enter VAT";
            this.spnVAT.Size = new System.Drawing.Size(310, 20);
            this.spnVAT.StyleController = this.editFormDataLayoutControlGroup;
            this.spnVAT.TabIndex = 131;
            this.spnVAT.Tag = "VAT";
            this.spnVAT.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditControl_Validating);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEquipmentReference,
            this.ItemForTransactionTypeID,
            this.ItemForTransactionOrderNumber,
            this.ItemForCompletedOnExchequer,
            this.ItemForTransactionDate,
            this.ItemForPurchaseInvoice,
            this.tabbedControlGroup1,
            this.ItemForNetTransactionPrice,
            this.ItemForVAT});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(851, 459);
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.txtEquipmentReference;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference :";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(851, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference :";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForTransactionTypeID
            // 
            this.ItemForTransactionTypeID.Control = this.lueTransactionTypeID;
            this.ItemForTransactionTypeID.CustomizationFormText = "Transaction Type :";
            this.ItemForTransactionTypeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForTransactionTypeID.Name = "ItemForTransactionTypeID";
            this.ItemForTransactionTypeID.Size = new System.Drawing.Size(424, 24);
            this.ItemForTransactionTypeID.Text = "Transaction Type :";
            this.ItemForTransactionTypeID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForTransactionOrderNumber
            // 
            this.ItemForTransactionOrderNumber.Control = this.txtTransactionOrderNumber;
            this.ItemForTransactionOrderNumber.CustomizationFormText = "Order Number :";
            this.ItemForTransactionOrderNumber.Location = new System.Drawing.Point(0, 48);
            this.ItemForTransactionOrderNumber.Name = "ItemForTransactionOrderNumber";
            this.ItemForTransactionOrderNumber.Size = new System.Drawing.Size(424, 24);
            this.ItemForTransactionOrderNumber.Text = "Order Number :";
            this.ItemForTransactionOrderNumber.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForCompletedOnExchequer
            // 
            this.ItemForCompletedOnExchequer.Control = this.ceCompletedOnExchequer;
            this.ItemForCompletedOnExchequer.CustomizationFormText = "Completed On Exchequer";
            this.ItemForCompletedOnExchequer.Location = new System.Drawing.Point(0, 436);
            this.ItemForCompletedOnExchequer.Name = "ItemForCompletedOnExchequer";
            this.ItemForCompletedOnExchequer.Size = new System.Drawing.Size(851, 23);
            this.ItemForCompletedOnExchequer.Text = "Completed On Exchequer";
            this.ItemForCompletedOnExchequer.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForCompletedOnExchequer.TextVisible = false;
            // 
            // ItemForTransactionDate
            // 
            this.ItemForTransactionDate.Control = this.deTransactionDate;
            this.ItemForTransactionDate.CustomizationFormText = "Transaction Date :";
            this.ItemForTransactionDate.Location = new System.Drawing.Point(424, 24);
            this.ItemForTransactionDate.Name = "ItemForTransactionDate";
            this.ItemForTransactionDate.Size = new System.Drawing.Size(427, 24);
            this.ItemForTransactionDate.Text = "Transaction Date :";
            this.ItemForTransactionDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForPurchaseInvoice
            // 
            this.ItemForPurchaseInvoice.Control = this.txtPurchaseInvoice;
            this.ItemForPurchaseInvoice.CustomizationFormText = "Purchase Invoice :";
            this.ItemForPurchaseInvoice.Location = new System.Drawing.Point(424, 48);
            this.ItemForPurchaseInvoice.Name = "ItemForPurchaseInvoice";
            this.ItemForPurchaseInvoice.Size = new System.Drawing.Size(427, 24);
            this.ItemForPurchaseInvoice.Tag = "Purchase Invoice";
            this.ItemForPurchaseInvoice.Text = "Purchase Invoice :";
            this.ItemForPurchaseInvoice.TextSize = new System.Drawing.Size(110, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 96);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(851, 340);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup2.CaptionImage")));
            this.layoutControlGroup2.CustomizationFormText = "Notes";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDescription});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(827, 292);
            this.layoutControlGroup2.Text = "Notes";
            // 
            // ItemForDescription
            // 
            this.ItemForDescription.Control = this.memDescription;
            this.ItemForDescription.CustomizationFormText = "Description";
            this.ItemForDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForDescription.Name = "ItemForDescription";
            this.ItemForDescription.Size = new System.Drawing.Size(827, 292);
            this.ItemForDescription.Text = "Description";
            this.ItemForDescription.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForDescription.TextVisible = false;
            // 
            // ItemForNetTransactionPrice
            // 
            this.ItemForNetTransactionPrice.Control = this.spnNetTransactionPrice;
            this.ItemForNetTransactionPrice.CustomizationFormText = "Net Price :";
            this.ItemForNetTransactionPrice.Location = new System.Drawing.Point(0, 72);
            this.ItemForNetTransactionPrice.Name = "ItemForNetTransactionPrice";
            this.ItemForNetTransactionPrice.Size = new System.Drawing.Size(424, 24);
            this.ItemForNetTransactionPrice.Text = "Net Price :";
            this.ItemForNetTransactionPrice.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForVAT
            // 
            this.ItemForVAT.Control = this.spnVAT;
            this.ItemForVAT.CustomizationFormText = "VAT :";
            this.ItemForVAT.Location = new System.Drawing.Point(424, 72);
            this.ItemForVAT.Name = "ItemForVAT";
            this.ItemForVAT.Size = new System.Drawing.Size(427, 24);
            this.ItemForVAT.Text = "VAT :";
            this.ItemForVAT.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(392, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(459, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(129, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11041_Transaction_ItemTableAdapter
            // 
            this.sp_AS_11041_Transaction_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Transaction_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Transaction_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Transactions_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(871, 558);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Transactions_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Transactions";
            this.Activated += new System.EventHandler(this.frm_AS_Transactions_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Transactions_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Transactions_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTransactionTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLTransactionTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTransactionDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTransactionDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionOrderNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPurchaseInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceCompletedOnExchequer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnNetTransactionPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransactionTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransactionOrderNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompletedOnExchequer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransactionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPurchaseInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNetTransactionPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource spAS11041TransactionItemBindingSource;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private DevExpress.XtraEditors.LookUpEdit lueTransactionTypeID;
        private DevExpress.XtraEditors.DateEdit deTransactionDate;
        private DevExpress.XtraEditors.TextEdit txtTransactionOrderNumber;
        private DevExpress.XtraEditors.TextEdit txtPurchaseInvoice;
        private DevExpress.XtraEditors.MemoEdit memDescription;
        private DevExpress.XtraEditors.CheckEdit ceCompletedOnExchequer;
        private DevExpress.XtraEditors.SpinEdit spnNetTransactionPrice;
        private DevExpress.XtraEditors.SpinEdit spnVAT;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransactionTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransactionDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransactionOrderNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPurchaseInvoice;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCompletedOnExchequer;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNetTransactionPrice;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVAT;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter sp_AS_11041_Transaction_ItemTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource spAS11000PLTransactionTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Transaction_TypeTableAdapter sp_AS_11000_PL_Transaction_TypeTableAdapter;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescription;


    }
}

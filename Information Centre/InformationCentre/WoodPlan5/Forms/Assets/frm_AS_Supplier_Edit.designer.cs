﻿namespace WoodPlan5
{
    partial class frm_AS_Supplier_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Supplier_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11020SupplierListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.lueSupplierTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLSupplierTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtSupplierAccountNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtSupplierReference = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.txtAddress1 = new DevExpress.XtraEditors.TextEdit();
            this.txtAddress2 = new DevExpress.XtraEditors.TextEdit();
            this.txtAddress3 = new DevExpress.XtraEditors.TextEdit();
            this.txtCity = new DevExpress.XtraEditors.TextEdit();
            this.txtPostcode = new DevExpress.XtraEditors.TextEdit();
            this.txtCounty = new DevExpress.XtraEditors.TextEdit();
            this.txtCountry = new DevExpress.XtraEditors.TextEdit();
            this.txtMainContact = new DevExpress.XtraEditors.TextEdit();
            this.txtPhone = new DevExpress.XtraEditors.TextEdit();
            this.txtMobile = new DevExpress.XtraEditors.TextEdit();
            this.txtFax = new DevExpress.XtraEditors.TextEdit();
            this.txtEmailAddress = new DevExpress.XtraEditors.TextEdit();
            this.memNotes = new DevExpress.XtraEditors.MemoEdit();
            this.txtWebsite = new DevExpress.XtraEditors.TextEdit();
            this.VehicleSupplierCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.PlantSupplierCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GadgetSupplierCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.HardwareSupplierCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SoftwareSupplierCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.OfficeSupplierCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.RentalSupplierCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSupplierTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEmailAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPhone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMainContact = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAddress3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCounty = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCountry = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddress1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddress2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCity = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMobile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWebsite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFax = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccountNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSupplierReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForVehicleSupplier = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGadgetSupplier = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSoftwareSupplier = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRentalSupplier = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHardwareSupplier = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOfficeSupplier = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPlantSupplier = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11020_Supplier_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter();
            this.sp_AS_11000_Duplicate_SearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter();
            this.sp_AS_11000_PL_Supplier_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Supplier_TypeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSupplierTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSupplierAccountNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSupplierReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCounty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMainContact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VehicleSupplierCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantSupplierCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GadgetSupplierCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HardwareSupplierCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoftwareSupplierCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OfficeSupplierCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RentalSupplierCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMainContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebsite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVehicleSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGadgetSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoftwareSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHardwareSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOfficeSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlantSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1059, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 699);
            this.barDockControlBottom.Size = new System.Drawing.Size(1059, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 673);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1059, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 673);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.layoutControlGroup2,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1059, 673);
            this.editFormLayoutControlGroup.Text = "Root";
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(287, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(263, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11020SupplierListBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(299, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // spAS11020SupplierListBindingSource
            // 
            this.spAS11020SupplierListBindingSource.DataMember = "sp_AS_11020_Supplier_List";
            this.spAS11020SupplierListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueSupplierTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtSupplierAccountNumber);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtSupplierReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtName);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtAddress1);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtAddress2);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtAddress3);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtCity);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtPostcode);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtCounty);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtCountry);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtMainContact);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtPhone);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtMobile);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtFax);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEmailAddress);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memNotes);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtWebsite);
            this.editFormDataLayoutControlGroup.Controls.Add(this.VehicleSupplierCheckEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.PlantSupplierCheckEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.GadgetSupplierCheckEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.HardwareSupplierCheckEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.SoftwareSupplierCheckEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.OfficeSupplierCheckEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.RentalSupplierCheckEdit);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11020SupplierListBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1825, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1059, 673);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // lueSupplierTypeID
            // 
            this.lueSupplierTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "SupplierTypeID", true));
            this.lueSupplierTypeID.Location = new System.Drawing.Point(113, 35);
            this.lueSupplierTypeID.MenuManager = this.barManager1;
            this.lueSupplierTypeID.Name = "lueSupplierTypeID";
            this.lueSupplierTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSupplierTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Supplier Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSupplierTypeID.Properties.DataSource = this.spAS11000PLSupplierTypeBindingSource;
            this.lueSupplierTypeID.Properties.DisplayMember = "Value";
            this.lueSupplierTypeID.Properties.NullText = "";
            this.lueSupplierTypeID.Properties.NullValuePrompt = "-- Please Select Supplier Type --";
            this.lueSupplierTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSupplierTypeID.Properties.ValueMember = "PickListID";
            this.lueSupplierTypeID.Size = new System.Drawing.Size(182, 20);
            this.lueSupplierTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueSupplierTypeID.TabIndex = 123;
            this.lueSupplierTypeID.Tag = "Supplier Type";
            this.lueSupplierTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lueSupplierTypeID_Validating);
            // 
            // spAS11000PLSupplierTypeBindingSource
            // 
            this.spAS11000PLSupplierTypeBindingSource.DataMember = "sp_AS_11000_PL_Supplier_Type";
            this.spAS11000PLSupplierTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // txtSupplierAccountNumber
            // 
            this.txtSupplierAccountNumber.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "AccountNumber", true));
            this.txtSupplierAccountNumber.Location = new System.Drawing.Point(400, 35);
            this.txtSupplierAccountNumber.MenuManager = this.barManager1;
            this.txtSupplierAccountNumber.Name = "txtSupplierAccountNumber";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtSupplierAccountNumber, true);
            this.txtSupplierAccountNumber.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtSupplierAccountNumber, optionsSpelling1);
            this.txtSupplierAccountNumber.StyleController = this.editFormDataLayoutControlGroup;
            this.txtSupplierAccountNumber.TabIndex = 124;
            this.txtSupplierAccountNumber.Tag = "Account Number";
            this.txtSupplierAccountNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtSupplierReference
            // 
            this.txtSupplierReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "SupplierReference", true));
            this.txtSupplierReference.Location = new System.Drawing.Point(747, 35);
            this.txtSupplierReference.MenuManager = this.barManager1;
            this.txtSupplierReference.Name = "txtSupplierReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtSupplierReference, true);
            this.txtSupplierReference.Size = new System.Drawing.Size(300, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtSupplierReference, optionsSpelling2);
            this.txtSupplierReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtSupplierReference.TabIndex = 125;
            this.txtSupplierReference.Tag = "allowEmpty";
            this.txtSupplierReference.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtName
            // 
            this.txtName.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Name", true));
            this.txtName.Location = new System.Drawing.Point(113, 59);
            this.txtName.MenuManager = this.barManager1;
            this.txtName.Name = "txtName";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtName, true);
            this.txtName.Size = new System.Drawing.Size(934, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtName, optionsSpelling3);
            this.txtName.StyleController = this.editFormDataLayoutControlGroup;
            this.txtName.TabIndex = 126;
            this.txtName.Tag = "Supplier Name";
            this.txtName.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtAddress1
            // 
            this.txtAddress1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Address1", true));
            this.txtAddress1.Location = new System.Drawing.Point(137, 145);
            this.txtAddress1.MenuManager = this.barManager1;
            this.txtAddress1.Name = "txtAddress1";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtAddress1, true);
            this.txtAddress1.Size = new System.Drawing.Size(886, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtAddress1, optionsSpelling4);
            this.txtAddress1.StyleController = this.editFormDataLayoutControlGroup;
            this.txtAddress1.TabIndex = 127;
            this.txtAddress1.Tag = "Address1";
            this.txtAddress1.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtAddress2
            // 
            this.txtAddress2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Address2", true));
            this.txtAddress2.Location = new System.Drawing.Point(137, 169);
            this.txtAddress2.MenuManager = this.barManager1;
            this.txtAddress2.Name = "txtAddress2";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtAddress2, true);
            this.txtAddress2.Size = new System.Drawing.Size(886, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtAddress2, optionsSpelling5);
            this.txtAddress2.StyleController = this.editFormDataLayoutControlGroup;
            this.txtAddress2.TabIndex = 128;
            this.txtAddress2.Tag = "allowEmpty";
            this.txtAddress2.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtAddress3
            // 
            this.txtAddress3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Address3", true));
            this.txtAddress3.Location = new System.Drawing.Point(137, 193);
            this.txtAddress3.MenuManager = this.barManager1;
            this.txtAddress3.Name = "txtAddress3";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtAddress3, true);
            this.txtAddress3.Size = new System.Drawing.Size(886, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtAddress3, optionsSpelling6);
            this.txtAddress3.StyleController = this.editFormDataLayoutControlGroup;
            this.txtAddress3.TabIndex = 129;
            this.txtAddress3.Tag = "allowEmpty";
            this.txtAddress3.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtCity
            // 
            this.txtCity.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "City", true));
            this.txtCity.Location = new System.Drawing.Point(137, 217);
            this.txtCity.MenuManager = this.barManager1;
            this.txtCity.Name = "txtCity";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtCity, true);
            this.txtCity.Size = new System.Drawing.Size(886, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtCity, optionsSpelling7);
            this.txtCity.StyleController = this.editFormDataLayoutControlGroup;
            this.txtCity.TabIndex = 130;
            this.txtCity.Tag = "City";
            this.txtCity.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtPostcode
            // 
            this.txtPostcode.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Postcode", true));
            this.txtPostcode.Location = new System.Drawing.Point(137, 241);
            this.txtPostcode.MenuManager = this.barManager1;
            this.txtPostcode.Name = "txtPostcode";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtPostcode, true);
            this.txtPostcode.Size = new System.Drawing.Size(166, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtPostcode, optionsSpelling8);
            this.txtPostcode.StyleController = this.editFormDataLayoutControlGroup;
            this.txtPostcode.TabIndex = 131;
            this.txtPostcode.Tag = "Postcode";
            this.txtPostcode.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtCounty
            // 
            this.txtCounty.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "County", true));
            this.txtCounty.Location = new System.Drawing.Point(408, 241);
            this.txtCounty.MenuManager = this.barManager1;
            this.txtCounty.Name = "txtCounty";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtCounty, true);
            this.txtCounty.Size = new System.Drawing.Size(241, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtCounty, optionsSpelling9);
            this.txtCounty.StyleController = this.editFormDataLayoutControlGroup;
            this.txtCounty.TabIndex = 132;
            this.txtCounty.Tag = "County";
            this.txtCounty.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtCountry
            // 
            this.txtCountry.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Country", true));
            this.txtCountry.Location = new System.Drawing.Point(754, 241);
            this.txtCountry.MenuManager = this.barManager1;
            this.txtCountry.Name = "txtCountry";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtCountry, true);
            this.txtCountry.Size = new System.Drawing.Size(269, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtCountry, optionsSpelling10);
            this.txtCountry.StyleController = this.editFormDataLayoutControlGroup;
            this.txtCountry.TabIndex = 133;
            this.txtCountry.Tag = "allowEmpty";
            this.txtCountry.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtMainContact
            // 
            this.txtMainContact.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "MainContact", true));
            this.txtMainContact.Location = new System.Drawing.Point(125, 277);
            this.txtMainContact.MenuManager = this.barManager1;
            this.txtMainContact.Name = "txtMainContact";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtMainContact, true);
            this.txtMainContact.Size = new System.Drawing.Size(910, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtMainContact, optionsSpelling11);
            this.txtMainContact.StyleController = this.editFormDataLayoutControlGroup;
            this.txtMainContact.TabIndex = 134;
            this.txtMainContact.Tag = "Main Contact";
            this.txtMainContact.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtPhone
            // 
            this.txtPhone.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Phone", true));
            this.txtPhone.Location = new System.Drawing.Point(125, 301);
            this.txtPhone.MenuManager = this.barManager1;
            this.txtPhone.Name = "txtPhone";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtPhone, true);
            this.txtPhone.Size = new System.Drawing.Size(175, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtPhone, optionsSpelling12);
            this.txtPhone.StyleController = this.editFormDataLayoutControlGroup;
            this.txtPhone.TabIndex = 135;
            this.txtPhone.Tag = "Phone";
            this.txtPhone.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtMobile
            // 
            this.txtMobile.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Mobile", true));
            this.txtMobile.Location = new System.Drawing.Point(405, 301);
            this.txtMobile.MenuManager = this.barManager1;
            this.txtMobile.Name = "txtMobile";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtMobile, true);
            this.txtMobile.Size = new System.Drawing.Size(245, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtMobile, optionsSpelling13);
            this.txtMobile.StyleController = this.editFormDataLayoutControlGroup;
            this.txtMobile.TabIndex = 136;
            this.txtMobile.Tag = "allowEmpty";
            this.txtMobile.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtFax
            // 
            this.txtFax.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Fax", true));
            this.txtFax.Location = new System.Drawing.Point(755, 301);
            this.txtFax.MenuManager = this.barManager1;
            this.txtFax.Name = "txtFax";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtFax, true);
            this.txtFax.Size = new System.Drawing.Size(280, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtFax, optionsSpelling14);
            this.txtFax.StyleController = this.editFormDataLayoutControlGroup;
            this.txtFax.TabIndex = 137;
            this.txtFax.Tag = "allowEmpty";
            this.txtFax.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // txtEmailAddress
            // 
            this.txtEmailAddress.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "EmailAddress", true));
            this.txtEmailAddress.Location = new System.Drawing.Point(125, 325);
            this.txtEmailAddress.MenuManager = this.barManager1;
            this.txtEmailAddress.Name = "txtEmailAddress";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEmailAddress, true);
            this.txtEmailAddress.Size = new System.Drawing.Size(175, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEmailAddress, optionsSpelling15);
            this.txtEmailAddress.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEmailAddress.TabIndex = 138;
            this.txtEmailAddress.Tag = "allowEmpty";
            this.txtEmailAddress.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // memNotes
            // 
            this.memNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Notes", true));
            this.memNotes.Location = new System.Drawing.Point(24, 532);
            this.memNotes.MenuManager = this.barManager1;
            this.memNotes.Name = "memNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memNotes, true);
            this.memNotes.Size = new System.Drawing.Size(1011, 117);
            this.scSpellChecker.SetSpellCheckerOptions(this.memNotes, optionsSpelling16);
            this.memNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.memNotes.TabIndex = 139;
            this.memNotes.Tag = "Notes";
            this.memNotes.UseOptimizedRendering = true;
            // 
            // txtWebsite
            // 
            this.txtWebsite.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "Website", true));
            this.txtWebsite.Location = new System.Drawing.Point(405, 325);
            this.txtWebsite.MenuManager = this.barManager1;
            this.txtWebsite.Name = "txtWebsite";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtWebsite, true);
            this.txtWebsite.Size = new System.Drawing.Size(630, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtWebsite, optionsSpelling17);
            this.txtWebsite.StyleController = this.editFormDataLayoutControlGroup;
            this.txtWebsite.TabIndex = 140;
            this.txtWebsite.Tag = "allowEmpty";
            this.txtWebsite.Validating += new System.ComponentModel.CancelEventHandler(this.txtControl_Validating);
            // 
            // VehicleSupplierCheckEdit
            // 
            this.VehicleSupplierCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "VehicleSupplier", true));
            this.VehicleSupplierCheckEdit.Location = new System.Drawing.Point(125, 392);
            this.VehicleSupplierCheckEdit.MenuManager = this.barManager1;
            this.VehicleSupplierCheckEdit.Name = "VehicleSupplierCheckEdit";
            this.VehicleSupplierCheckEdit.Properties.Caption = "(Tick To Appear In Vehicle Supplier List)";
            this.VehicleSupplierCheckEdit.Size = new System.Drawing.Size(402, 19);
            this.VehicleSupplierCheckEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.VehicleSupplierCheckEdit.TabIndex = 141;
            // 
            // PlantSupplierCheckEdit
            // 
            this.PlantSupplierCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "PlantSupplier", true));
            this.PlantSupplierCheckEdit.Location = new System.Drawing.Point(632, 392);
            this.PlantSupplierCheckEdit.MenuManager = this.barManager1;
            this.PlantSupplierCheckEdit.Name = "PlantSupplierCheckEdit";
            this.PlantSupplierCheckEdit.Properties.Caption = "(Tick To Appear In Plant Supplier List)";
            this.PlantSupplierCheckEdit.Size = new System.Drawing.Size(403, 19);
            this.PlantSupplierCheckEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.PlantSupplierCheckEdit.TabIndex = 142;
            // 
            // GadgetSupplierCheckEdit
            // 
            this.GadgetSupplierCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "GadgetSupplier", true));
            this.GadgetSupplierCheckEdit.Location = new System.Drawing.Point(125, 415);
            this.GadgetSupplierCheckEdit.MenuManager = this.barManager1;
            this.GadgetSupplierCheckEdit.Name = "GadgetSupplierCheckEdit";
            this.GadgetSupplierCheckEdit.Properties.Caption = "(Tick To Appear In Gadget Supplier List)";
            this.GadgetSupplierCheckEdit.Size = new System.Drawing.Size(402, 19);
            this.GadgetSupplierCheckEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.GadgetSupplierCheckEdit.TabIndex = 143;
            // 
            // HardwareSupplierCheckEdit
            // 
            this.HardwareSupplierCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "HardwareSupplier", true));
            this.HardwareSupplierCheckEdit.Location = new System.Drawing.Point(632, 438);
            this.HardwareSupplierCheckEdit.MenuManager = this.barManager1;
            this.HardwareSupplierCheckEdit.Name = "HardwareSupplierCheckEdit";
            this.HardwareSupplierCheckEdit.Properties.Caption = "(Tick To Appear In Hardware Supplier List)";
            this.HardwareSupplierCheckEdit.Size = new System.Drawing.Size(403, 19);
            this.HardwareSupplierCheckEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.HardwareSupplierCheckEdit.TabIndex = 144;
            // 
            // SoftwareSupplierCheckEdit
            // 
            this.SoftwareSupplierCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "SoftwareSupplier", true));
            this.SoftwareSupplierCheckEdit.Location = new System.Drawing.Point(125, 438);
            this.SoftwareSupplierCheckEdit.MenuManager = this.barManager1;
            this.SoftwareSupplierCheckEdit.Name = "SoftwareSupplierCheckEdit";
            this.SoftwareSupplierCheckEdit.Properties.Caption = "(Tick To Appear In Software Supplier List)";
            this.SoftwareSupplierCheckEdit.Size = new System.Drawing.Size(402, 19);
            this.SoftwareSupplierCheckEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.SoftwareSupplierCheckEdit.TabIndex = 145;
            // 
            // OfficeSupplierCheckEdit
            // 
            this.OfficeSupplierCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "OfficeSupplier", true));
            this.OfficeSupplierCheckEdit.Location = new System.Drawing.Point(632, 415);
            this.OfficeSupplierCheckEdit.MenuManager = this.barManager1;
            this.OfficeSupplierCheckEdit.Name = "OfficeSupplierCheckEdit";
            this.OfficeSupplierCheckEdit.Properties.Caption = "(Tick To Appear In Office Supplier List)";
            this.OfficeSupplierCheckEdit.Size = new System.Drawing.Size(403, 19);
            this.OfficeSupplierCheckEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.OfficeSupplierCheckEdit.TabIndex = 146;
            // 
            // RentalSupplierCheckEdit
            // 
            this.RentalSupplierCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11020SupplierListBindingSource, "RentalSupplier", true));
            this.RentalSupplierCheckEdit.Location = new System.Drawing.Point(125, 461);
            this.RentalSupplierCheckEdit.MenuManager = this.barManager1;
            this.RentalSupplierCheckEdit.Name = "RentalSupplierCheckEdit";
            this.RentalSupplierCheckEdit.Properties.Caption = "(Tick To Appear In Office Rental List)";
            this.RentalSupplierCheckEdit.Size = new System.Drawing.Size(910, 19);
            this.RentalSupplierCheckEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.RentalSupplierCheckEdit.TabIndex = 147;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSupplierTypeID,
            this.ItemForName,
            this.layoutControlGroup4,
            this.ItemForAccountNumber,
            this.ItemForSupplierReference,
            this.layoutControlGroup6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1039, 461);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // ItemForSupplierTypeID
            // 
            this.ItemForSupplierTypeID.Control = this.lueSupplierTypeID;
            this.ItemForSupplierTypeID.CustomizationFormText = "Supplier Type :";
            this.ItemForSupplierTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForSupplierTypeID.Name = "ItemForSupplierTypeID";
            this.ItemForSupplierTypeID.Size = new System.Drawing.Size(287, 24);
            this.ItemForSupplierTypeID.Text = "Supplier Type :";
            this.ItemForSupplierTypeID.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForName
            // 
            this.ItemForName.Control = this.txtName;
            this.ItemForName.CustomizationFormText = "Name :";
            this.ItemForName.Location = new System.Drawing.Point(0, 24);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(1039, 24);
            this.ItemForName.Text = "Name :";
            this.ItemForName.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Contact Details";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmailAddress,
            this.ItemForPhone,
            this.ItemForMainContact,
            this.layoutControlGroup5,
            this.ItemForMobile,
            this.ItemForWebsite,
            this.ItemForFax});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1039, 278);
            this.layoutControlGroup4.Text = "Contact Details";
            // 
            // ItemForEmailAddress
            // 
            this.ItemForEmailAddress.Control = this.txtEmailAddress;
            this.ItemForEmailAddress.CustomizationFormText = "Email Address :";
            this.ItemForEmailAddress.Location = new System.Drawing.Point(0, 211);
            this.ItemForEmailAddress.Name = "ItemForEmailAddress";
            this.ItemForEmailAddress.Size = new System.Drawing.Size(280, 24);
            this.ItemForEmailAddress.Text = "Email Address :";
            this.ItemForEmailAddress.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForPhone
            // 
            this.ItemForPhone.Control = this.txtPhone;
            this.ItemForPhone.CustomizationFormText = "Phone :";
            this.ItemForPhone.Location = new System.Drawing.Point(0, 187);
            this.ItemForPhone.Name = "ItemForPhone";
            this.ItemForPhone.Size = new System.Drawing.Size(280, 24);
            this.ItemForPhone.Text = "Phone :";
            this.ItemForPhone.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForMainContact
            // 
            this.ItemForMainContact.Control = this.txtMainContact;
            this.ItemForMainContact.CustomizationFormText = "Main Contact :";
            this.ItemForMainContact.Location = new System.Drawing.Point(0, 163);
            this.ItemForMainContact.Name = "ItemForMainContact";
            this.ItemForMainContact.Size = new System.Drawing.Size(1015, 24);
            this.ItemForMainContact.Text = "Main Contact :";
            this.ItemForMainContact.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Address Details";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAddress3,
            this.ItemForPostcode,
            this.ItemForCounty,
            this.ItemForCountry,
            this.ItemForAddress1,
            this.ItemForAddress2,
            this.ItemForCity});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1015, 163);
            this.layoutControlGroup5.Text = "Address Details";
            // 
            // ItemForAddress3
            // 
            this.ItemForAddress3.Control = this.txtAddress3;
            this.ItemForAddress3.CustomizationFormText = "Address3 :";
            this.ItemForAddress3.Location = new System.Drawing.Point(0, 48);
            this.ItemForAddress3.Name = "ItemForAddress3";
            this.ItemForAddress3.Size = new System.Drawing.Size(991, 24);
            this.ItemForAddress3.Text = "Address3 :";
            this.ItemForAddress3.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForPostcode
            // 
            this.ItemForPostcode.Control = this.txtPostcode;
            this.ItemForPostcode.CustomizationFormText = "Postcode :";
            this.ItemForPostcode.Location = new System.Drawing.Point(0, 96);
            this.ItemForPostcode.Name = "ItemForPostcode";
            this.ItemForPostcode.Size = new System.Drawing.Size(271, 24);
            this.ItemForPostcode.Text = "Postcode :";
            this.ItemForPostcode.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForCounty
            // 
            this.ItemForCounty.Control = this.txtCounty;
            this.ItemForCounty.CustomizationFormText = "County :";
            this.ItemForCounty.Location = new System.Drawing.Point(271, 96);
            this.ItemForCounty.Name = "ItemForCounty";
            this.ItemForCounty.Size = new System.Drawing.Size(346, 24);
            this.ItemForCounty.Text = "County :";
            this.ItemForCounty.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForCountry
            // 
            this.ItemForCountry.Control = this.txtCountry;
            this.ItemForCountry.CustomizationFormText = "Country :";
            this.ItemForCountry.Location = new System.Drawing.Point(617, 96);
            this.ItemForCountry.Name = "ItemForCountry";
            this.ItemForCountry.Size = new System.Drawing.Size(374, 24);
            this.ItemForCountry.Text = "Country :";
            this.ItemForCountry.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForAddress1
            // 
            this.ItemForAddress1.Control = this.txtAddress1;
            this.ItemForAddress1.CustomizationFormText = "Address1 :";
            this.ItemForAddress1.Location = new System.Drawing.Point(0, 0);
            this.ItemForAddress1.Name = "ItemForAddress1";
            this.ItemForAddress1.Size = new System.Drawing.Size(991, 24);
            this.ItemForAddress1.Text = "Address1 :";
            this.ItemForAddress1.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForAddress2
            // 
            this.ItemForAddress2.Control = this.txtAddress2;
            this.ItemForAddress2.CustomizationFormText = "Address2 :";
            this.ItemForAddress2.Location = new System.Drawing.Point(0, 24);
            this.ItemForAddress2.Name = "ItemForAddress2";
            this.ItemForAddress2.Size = new System.Drawing.Size(991, 24);
            this.ItemForAddress2.Text = "Address2 :";
            this.ItemForAddress2.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForCity
            // 
            this.ItemForCity.Control = this.txtCity;
            this.ItemForCity.CustomizationFormText = "City :";
            this.ItemForCity.Location = new System.Drawing.Point(0, 72);
            this.ItemForCity.Name = "ItemForCity";
            this.ItemForCity.Size = new System.Drawing.Size(991, 24);
            this.ItemForCity.Text = "City :";
            this.ItemForCity.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForMobile
            // 
            this.ItemForMobile.Control = this.txtMobile;
            this.ItemForMobile.CustomizationFormText = "Mobile :";
            this.ItemForMobile.Location = new System.Drawing.Point(280, 187);
            this.ItemForMobile.Name = "ItemForMobile";
            this.ItemForMobile.Size = new System.Drawing.Size(350, 24);
            this.ItemForMobile.Text = "Mobile :";
            this.ItemForMobile.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForWebsite
            // 
            this.ItemForWebsite.Control = this.txtWebsite;
            this.ItemForWebsite.CustomizationFormText = "Website :";
            this.ItemForWebsite.Location = new System.Drawing.Point(280, 211);
            this.ItemForWebsite.Name = "ItemForWebsite";
            this.ItemForWebsite.Size = new System.Drawing.Size(735, 24);
            this.ItemForWebsite.Text = "Website :";
            this.ItemForWebsite.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForFax
            // 
            this.ItemForFax.Control = this.txtFax;
            this.ItemForFax.CustomizationFormText = "Fax :";
            this.ItemForFax.Location = new System.Drawing.Point(630, 187);
            this.ItemForFax.Name = "ItemForFax";
            this.ItemForFax.Size = new System.Drawing.Size(385, 24);
            this.ItemForFax.Text = "Fax :";
            this.ItemForFax.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForAccountNumber
            // 
            this.ItemForAccountNumber.Control = this.txtSupplierAccountNumber;
            this.ItemForAccountNumber.CustomizationFormText = "Account Number :";
            this.ItemForAccountNumber.Location = new System.Drawing.Point(287, 0);
            this.ItemForAccountNumber.Name = "ItemForAccountNumber";
            this.ItemForAccountNumber.Size = new System.Drawing.Size(347, 24);
            this.ItemForAccountNumber.Text = "Account Number :";
            this.ItemForAccountNumber.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForSupplierReference
            // 
            this.ItemForSupplierReference.Control = this.txtSupplierReference;
            this.ItemForSupplierReference.CustomizationFormText = "Supplier Reference :";
            this.ItemForSupplierReference.Location = new System.Drawing.Point(634, 0);
            this.ItemForSupplierReference.Name = "ItemForSupplierReference";
            this.ItemForSupplierReference.Size = new System.Drawing.Size(405, 24);
            this.ItemForSupplierReference.Text = "Supplier Reference :";
            this.ItemForSupplierReference.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Supplier Preference";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForVehicleSupplier,
            this.ItemForGadgetSupplier,
            this.ItemForSoftwareSupplier,
            this.ItemForRentalSupplier,
            this.ItemForHardwareSupplier,
            this.ItemForOfficeSupplier,
            this.ItemForPlantSupplier});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 326);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1039, 135);
            this.layoutControlGroup6.Text = "Supplier Preference";
            // 
            // ItemForVehicleSupplier
            // 
            this.ItemForVehicleSupplier.Control = this.VehicleSupplierCheckEdit;
            this.ItemForVehicleSupplier.CustomizationFormText = "Vehicle Supplier :";
            this.ItemForVehicleSupplier.Location = new System.Drawing.Point(0, 0);
            this.ItemForVehicleSupplier.Name = "ItemForVehicleSupplier";
            this.ItemForVehicleSupplier.Size = new System.Drawing.Size(507, 23);
            this.ItemForVehicleSupplier.Text = "Vehicle Supplier :";
            this.ItemForVehicleSupplier.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForGadgetSupplier
            // 
            this.ItemForGadgetSupplier.Control = this.GadgetSupplierCheckEdit;
            this.ItemForGadgetSupplier.CustomizationFormText = "Gadget Supplier :";
            this.ItemForGadgetSupplier.Location = new System.Drawing.Point(0, 23);
            this.ItemForGadgetSupplier.Name = "ItemForGadgetSupplier";
            this.ItemForGadgetSupplier.Size = new System.Drawing.Size(507, 23);
            this.ItemForGadgetSupplier.Text = "Gadget Supplier :";
            this.ItemForGadgetSupplier.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForSoftwareSupplier
            // 
            this.ItemForSoftwareSupplier.Control = this.SoftwareSupplierCheckEdit;
            this.ItemForSoftwareSupplier.CustomizationFormText = "Software Supplier :";
            this.ItemForSoftwareSupplier.Location = new System.Drawing.Point(0, 46);
            this.ItemForSoftwareSupplier.Name = "ItemForSoftwareSupplier";
            this.ItemForSoftwareSupplier.Size = new System.Drawing.Size(507, 23);
            this.ItemForSoftwareSupplier.Text = "Software Supplier :";
            this.ItemForSoftwareSupplier.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForRentalSupplier
            // 
            this.ItemForRentalSupplier.Control = this.RentalSupplierCheckEdit;
            this.ItemForRentalSupplier.CustomizationFormText = "Rental Supplier :";
            this.ItemForRentalSupplier.Location = new System.Drawing.Point(0, 69);
            this.ItemForRentalSupplier.Name = "ItemForRentalSupplier";
            this.ItemForRentalSupplier.Size = new System.Drawing.Size(1015, 23);
            this.ItemForRentalSupplier.Text = "Rental Supplier :";
            this.ItemForRentalSupplier.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForHardwareSupplier
            // 
            this.ItemForHardwareSupplier.Control = this.HardwareSupplierCheckEdit;
            this.ItemForHardwareSupplier.CustomizationFormText = "Hardware Supplier :";
            this.ItemForHardwareSupplier.Location = new System.Drawing.Point(507, 46);
            this.ItemForHardwareSupplier.Name = "ItemForHardwareSupplier";
            this.ItemForHardwareSupplier.Size = new System.Drawing.Size(508, 23);
            this.ItemForHardwareSupplier.Text = "Hardware Supplier :";
            this.ItemForHardwareSupplier.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForOfficeSupplier
            // 
            this.ItemForOfficeSupplier.Control = this.OfficeSupplierCheckEdit;
            this.ItemForOfficeSupplier.CustomizationFormText = "Office Supplier :";
            this.ItemForOfficeSupplier.Location = new System.Drawing.Point(507, 23);
            this.ItemForOfficeSupplier.Name = "ItemForOfficeSupplier";
            this.ItemForOfficeSupplier.Size = new System.Drawing.Size(508, 23);
            this.ItemForOfficeSupplier.Text = "Office Supplier :";
            this.ItemForOfficeSupplier.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForPlantSupplier
            // 
            this.ItemForPlantSupplier.Control = this.PlantSupplierCheckEdit;
            this.ItemForPlantSupplier.CustomizationFormText = "Plant Supplier :";
            this.ItemForPlantSupplier.Location = new System.Drawing.Point(507, 0);
            this.ItemForPlantSupplier.Name = "ItemForPlantSupplier";
            this.ItemForPlantSupplier.Size = new System.Drawing.Size(508, 23);
            this.ItemForPlantSupplier.Text = "Plant Supplier :";
            this.ItemForPlantSupplier.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 484);
            this.layoutControlGroup2.Name = "autoGeneratedGroup1";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1039, 169);
            this.layoutControlGroup2.Text = "autoGeneratedGroup1";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1039, 169);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImage")));
            this.layoutControlGroup3.CustomizationFormText = "Notes";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1015, 121);
            this.layoutControlGroup3.Text = "Notes";
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.memNotes;
            this.ItemForNotes.CustomizationFormText = "Notes";
            this.ItemForNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(1015, 121);
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNotes.TextToControlDistance = 0;
            this.ItemForNotes.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(287, 23);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(550, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(489, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11020_Supplier_ListTableAdapter
            // 
            this.sp_AS_11020_Supplier_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_Duplicate_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Supplier_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Supplier_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Supplier_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1059, 729);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1065, 586);
            this.Name = "frm_AS_Supplier_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Suppliers";
            this.Activated += new System.EventHandler(this.frm_AS_Supplier_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Supplier_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Supplier_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplierTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSupplierTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSupplierAccountNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSupplierReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCounty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountry.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMainContact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VehicleSupplierCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlantSupplierCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GadgetSupplierCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HardwareSupplierCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoftwareSupplierCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OfficeSupplierCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RentalSupplierCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMainContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCounty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWebsite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplierReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVehicleSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGadgetSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoftwareSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHardwareSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOfficeSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlantSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource spAS11020SupplierListBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueSupplierTypeID;
        private DevExpress.XtraEditors.TextEdit txtSupplierAccountNumber;
        private DevExpress.XtraEditors.TextEdit txtSupplierReference;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.TextEdit txtAddress1;
        private DevExpress.XtraEditors.TextEdit txtAddress2;
        private DevExpress.XtraEditors.TextEdit txtAddress3;
        private DevExpress.XtraEditors.TextEdit txtCity;
        private DevExpress.XtraEditors.TextEdit txtPostcode;
        private DevExpress.XtraEditors.TextEdit txtCounty;
        private DevExpress.XtraEditors.TextEdit txtCountry;
        private DevExpress.XtraEditors.TextEdit txtMainContact;
        private DevExpress.XtraEditors.TextEdit txtPhone;
        private DevExpress.XtraEditors.TextEdit txtMobile;
        private DevExpress.XtraEditors.TextEdit txtFax;
        private DevExpress.XtraEditors.TextEdit txtEmailAddress;
        private DevExpress.XtraEditors.MemoEdit memNotes;
        private DevExpress.XtraEditors.TextEdit txtWebsite;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSupplierTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccountNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSupplierReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddress1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddress2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddress3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCity;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCounty;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCountry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMainContact;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPhone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMobile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFax;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmailAddress;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWebsite;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter sp_AS_11020_Supplier_ListTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter sp_AS_11000_Duplicate_SearchTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLSupplierTypeBindingSource;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Supplier_TypeTableAdapter sp_AS_11000_PL_Supplier_TypeTableAdapter;
        private DevExpress.XtraEditors.CheckEdit VehicleSupplierCheckEdit;
        private DevExpress.XtraEditors.CheckEdit PlantSupplierCheckEdit;
        private DevExpress.XtraEditors.CheckEdit GadgetSupplierCheckEdit;
        private DevExpress.XtraEditors.CheckEdit HardwareSupplierCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SoftwareSupplierCheckEdit;
        private DevExpress.XtraEditors.CheckEdit OfficeSupplierCheckEdit;
        private DevExpress.XtraEditors.CheckEdit RentalSupplierCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVehicleSupplier;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGadgetSupplier;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSoftwareSupplier;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRentalSupplier;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHardwareSupplier;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOfficeSupplier;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlantSupplier;


    }
}

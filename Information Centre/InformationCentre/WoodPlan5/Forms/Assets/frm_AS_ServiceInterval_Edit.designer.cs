﻿namespace WoodPlan5
{
    partial class frm_AS_ServiceInterval_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_ServiceInterval_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForServiceStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.lueServiceStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11163ServiceScheduleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLServiceStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtServiceDataReference = new DevExpress.XtraEditors.TextEdit();
            this.spnServiceDueDistance = new DevExpress.XtraEditors.SpinEdit();
            this.deServiceDueDate = new DevExpress.XtraEditors.DateEdit();
            this.lueAlertDistanceUnitsID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLUnitDistTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnAlertWithinXDistance = new DevExpress.XtraEditors.SpinEdit();
            this.lueAlertTimeUnitsID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLUnitTimeTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnAlertWithinXTime = new DevExpress.XtraEditors.SpinEdit();
            this.lueWorkDetailID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11078WorkDetailItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.memIntervalScheduleNotes = new DevExpress.XtraEditors.MemoEdit();
            this.IsCurrentIntervalCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ItemForServiceDueDistance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForServiceDueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAlertDistanceUnitsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAlertWithinXDistance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAlertTimeUnitsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAlertWithinXTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkDetailID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForIntervalScheduleNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsCurrentInterval = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp_AS_11163_Service_ScheduleTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11163_Service_ScheduleTableAdapter();
            this.sp_AS_11000_PL_Service_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Service_StatusTableAdapter();
            this.sp_AS_11000_PL_Unit_Dist_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_Dist_TypeTableAdapter();
            this.sp_AS_11000_PL_Unit_Time_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_Time_TypeTableAdapter();
            this.sp_AS_11078_Work_Detail_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueServiceStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11163ServiceScheduleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLServiceStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDataReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnServiceDueDistance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAlertDistanceUnitsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitDistTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAlertWithinXDistance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAlertTimeUnitsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitTimeTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAlertWithinXTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWorkDetailID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memIntervalScheduleNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsCurrentIntervalCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertDistanceUnitsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertWithinXDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertTimeUnitsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertWithinXTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkDetailID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIntervalScheduleNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsCurrentInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(797, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 454);
            this.barDockControlBottom.Size = new System.Drawing.Size(797, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 428);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(797, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 428);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "editFormLayoutControlGroup";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(797, 428);
            this.editFormLayoutControlGroup.Text = "editFormLayoutControlGroup";
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForServiceStatusID,
            this.ItemForServiceDueDistance,
            this.ItemForServiceDueDate,
            this.ItemForAlertDistanceUnitsID,
            this.ItemForAlertWithinXDistance,
            this.ItemForAlertTimeUnitsID,
            this.ItemForAlertWithinXTime,
            this.ItemForWorkDetailID,
            this.layoutControlGroup2,
            this.layoutControlItem2,
            this.ItemForIsCurrentInterval});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(777, 408);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // ItemForServiceStatusID
            // 
            this.ItemForServiceStatusID.Control = this.lueServiceStatusID;
            this.ItemForServiceStatusID.CustomizationFormText = "Service Status :";
            this.ItemForServiceStatusID.Location = new System.Drawing.Point(0, 47);
            this.ItemForServiceStatusID.Name = "ItemForServiceStatusID";
            this.ItemForServiceStatusID.Size = new System.Drawing.Size(777, 24);
            this.ItemForServiceStatusID.Text = "Service Status :";
            this.ItemForServiceStatusID.TextSize = new System.Drawing.Size(117, 13);
            // 
            // lueServiceStatusID
            // 
            this.lueServiceStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "ServiceStatusID", true));
            this.lueServiceStatusID.Location = new System.Drawing.Point(132, 59);
            this.lueServiceStatusID.MenuManager = this.barManager1;
            this.lueServiceStatusID.Name = "lueServiceStatusID";
            this.lueServiceStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueServiceStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Service Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueServiceStatusID.Properties.DataSource = this.spAS11000PLServiceStatusBindingSource;
            this.lueServiceStatusID.Properties.DisplayMember = "Value";
            this.lueServiceStatusID.Properties.NullText = "";
            this.lueServiceStatusID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueServiceStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueServiceStatusID.Properties.ValueMember = "PickListID";
            this.lueServiceStatusID.Size = new System.Drawing.Size(653, 20);
            this.lueServiceStatusID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueServiceStatusID.TabIndex = 124;
            this.lueServiceStatusID.EditValueChanged += new System.EventHandler(this.lueServiceStatusID_EditValueChanged);
            this.lueServiceStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11163ServiceScheduleBindingSource
            // 
            this.spAS11163ServiceScheduleBindingSource.DataMember = "sp_AS_11163_Service_Schedule";
            this.spAS11163ServiceScheduleBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLServiceStatusBindingSource
            // 
            this.spAS11000PLServiceStatusBindingSource.DataMember = "sp_AS_11000_PL_Service_Status";
            this.spAS11000PLServiceStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtServiceDataReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueServiceStatusID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnServiceDueDistance);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deServiceDueDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueAlertDistanceUnitsID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnAlertWithinXDistance);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueAlertTimeUnitsID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnAlertWithinXTime);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueWorkDetailID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memIntervalScheduleNotes);
            this.editFormDataLayoutControlGroup.Controls.Add(this.IsCurrentIntervalCheckEdit);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11163ServiceScheduleBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1935, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(797, 428);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtServiceDataReference
            // 
            this.txtServiceDataReference.Location = new System.Drawing.Point(132, 12);
            this.txtServiceDataReference.MenuManager = this.barManager1;
            this.txtServiceDataReference.Name = "txtServiceDataReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtServiceDataReference, true);
            this.txtServiceDataReference.Size = new System.Drawing.Size(653, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtServiceDataReference, optionsSpelling1);
            this.txtServiceDataReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtServiceDataReference.TabIndex = 134;
            // 
            // spnServiceDueDistance
            // 
            this.spnServiceDueDistance.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "ServiceDueDistance", true));
            this.spnServiceDueDistance.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnServiceDueDistance.Location = new System.Drawing.Point(132, 83);
            this.spnServiceDueDistance.MenuManager = this.barManager1;
            this.spnServiceDueDistance.Name = "spnServiceDueDistance";
            this.spnServiceDueDistance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnServiceDueDistance.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnServiceDueDistance.Size = new System.Drawing.Size(653, 20);
            this.spnServiceDueDistance.StyleController = this.editFormDataLayoutControlGroup;
            this.spnServiceDueDistance.TabIndex = 125;
            this.spnServiceDueDistance.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // deServiceDueDate
            // 
            this.deServiceDueDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "ServiceDueDate", true));
            this.deServiceDueDate.EditValue = null;
            this.deServiceDueDate.Location = new System.Drawing.Point(132, 107);
            this.deServiceDueDate.MenuManager = this.barManager1;
            this.deServiceDueDate.Name = "deServiceDueDate";
            this.deServiceDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deServiceDueDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deServiceDueDate.Size = new System.Drawing.Size(653, 20);
            this.deServiceDueDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deServiceDueDate.TabIndex = 126;
            this.deServiceDueDate.Validating += new System.ComponentModel.CancelEventHandler(this.deServiceDueDate_Validating);
            // 
            // lueAlertDistanceUnitsID
            // 
            this.lueAlertDistanceUnitsID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "AlertDistanceUnitsID", true));
            this.lueAlertDistanceUnitsID.Location = new System.Drawing.Point(132, 131);
            this.lueAlertDistanceUnitsID.MenuManager = this.barManager1;
            this.lueAlertDistanceUnitsID.Name = "lueAlertDistanceUnitsID";
            this.lueAlertDistanceUnitsID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAlertDistanceUnitsID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Distance Units", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAlertDistanceUnitsID.Properties.DataSource = this.spAS11000PLUnitDistTypeBindingSource;
            this.lueAlertDistanceUnitsID.Properties.DisplayMember = "Value";
            this.lueAlertDistanceUnitsID.Properties.NullText = "";
            this.lueAlertDistanceUnitsID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueAlertDistanceUnitsID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueAlertDistanceUnitsID.Properties.ValueMember = "PickListID";
            this.lueAlertDistanceUnitsID.Size = new System.Drawing.Size(653, 20);
            this.lueAlertDistanceUnitsID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueAlertDistanceUnitsID.TabIndex = 127;
            this.lueAlertDistanceUnitsID.EditValueChanged += new System.EventHandler(this.lueAlertDistanceUnitsID_EditValueChanged);
            this.lueAlertDistanceUnitsID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLUnitDistTypeBindingSource
            // 
            this.spAS11000PLUnitDistTypeBindingSource.DataMember = "sp_AS_11000_PL_Unit_Dist_Type";
            this.spAS11000PLUnitDistTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnAlertWithinXDistance
            // 
            this.spnAlertWithinXDistance.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "AlertWithinXDistance", true));
            this.spnAlertWithinXDistance.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnAlertWithinXDistance.Location = new System.Drawing.Point(132, 155);
            this.spnAlertWithinXDistance.MenuManager = this.barManager1;
            this.spnAlertWithinXDistance.Name = "spnAlertWithinXDistance";
            this.spnAlertWithinXDistance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnAlertWithinXDistance.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnAlertWithinXDistance.Size = new System.Drawing.Size(653, 20);
            this.spnAlertWithinXDistance.StyleController = this.editFormDataLayoutControlGroup;
            this.spnAlertWithinXDistance.TabIndex = 128;
            this.spnAlertWithinXDistance.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // lueAlertTimeUnitsID
            // 
            this.lueAlertTimeUnitsID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "AlertTimeUnitsID", true));
            this.lueAlertTimeUnitsID.Location = new System.Drawing.Point(132, 179);
            this.lueAlertTimeUnitsID.MenuManager = this.barManager1;
            this.lueAlertTimeUnitsID.Name = "lueAlertTimeUnitsID";
            this.lueAlertTimeUnitsID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAlertTimeUnitsID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Time Units", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAlertTimeUnitsID.Properties.DataSource = this.spAS11000PLUnitTimeTypeBindingSource;
            this.lueAlertTimeUnitsID.Properties.DisplayMember = "Value";
            this.lueAlertTimeUnitsID.Properties.NullText = "";
            this.lueAlertTimeUnitsID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueAlertTimeUnitsID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueAlertTimeUnitsID.Properties.ValueMember = "PickListID";
            this.lueAlertTimeUnitsID.Size = new System.Drawing.Size(653, 20);
            this.lueAlertTimeUnitsID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueAlertTimeUnitsID.TabIndex = 129;
            this.lueAlertTimeUnitsID.EditValueChanged += new System.EventHandler(this.lueAlertDistanceUnitsID_EditValueChanged);
            this.lueAlertTimeUnitsID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLUnitTimeTypeBindingSource
            // 
            this.spAS11000PLUnitTimeTypeBindingSource.DataMember = "sp_AS_11000_PL_Unit_Time_Type";
            this.spAS11000PLUnitTimeTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnAlertWithinXTime
            // 
            this.spnAlertWithinXTime.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "AlertWithinXTime", true));
            this.spnAlertWithinXTime.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnAlertWithinXTime.Location = new System.Drawing.Point(132, 203);
            this.spnAlertWithinXTime.MenuManager = this.barManager1;
            this.spnAlertWithinXTime.Name = "spnAlertWithinXTime";
            this.spnAlertWithinXTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnAlertWithinXTime.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnAlertWithinXTime.Size = new System.Drawing.Size(653, 20);
            this.spnAlertWithinXTime.StyleController = this.editFormDataLayoutControlGroup;
            this.spnAlertWithinXTime.TabIndex = 130;
            this.spnAlertWithinXTime.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // lueWorkDetailID
            // 
            this.lueWorkDetailID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "WorkDetailID", true));
            this.lueWorkDetailID.Location = new System.Drawing.Point(132, 227);
            this.lueWorkDetailID.MenuManager = this.barManager1;
            this.lueWorkDetailID.Name = "lueWorkDetailID";
            this.lueWorkDetailID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueWorkDetailID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Add Work Order", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueWorkDetailID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Refresh Work Orders", "refresh", null, true)});
            this.lueWorkDetailID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EquipmentReference", "Equipment Reference", 113, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WorkType", "Work Type", 62, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DateRaised", "Date Raised", 68, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierReference", "Supplier Reference", 101, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Status", "Status", 41, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OrderNumber", "Order Number", 78, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WorkCompletionDate", "Work Completion Date", 117, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ServiceType", "Service Type", 72, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueWorkDetailID.Properties.DataSource = this.spAS11078WorkDetailItemBindingSource;
            this.lueWorkDetailID.Properties.DisplayMember = "WorkType";
            this.lueWorkDetailID.Properties.NullText = "";
            this.lueWorkDetailID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueWorkDetailID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueWorkDetailID.Properties.ValueMember = "WorkDetailID";
            this.lueWorkDetailID.Size = new System.Drawing.Size(653, 22);
            this.lueWorkDetailID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueWorkDetailID.TabIndex = 131;
            this.lueWorkDetailID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lueWorkDetailID_ButtonPressed);
            this.lueWorkDetailID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11078WorkDetailItemBindingSource
            // 
            this.spAS11078WorkDetailItemBindingSource.DataMember = "sp_AS_11078_Work_Detail_Item";
            this.spAS11078WorkDetailItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // memIntervalScheduleNotes
            // 
            this.memIntervalScheduleNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "IntervalScheduleNotes", true));
            this.memIntervalScheduleNotes.Location = new System.Drawing.Point(24, 284);
            this.memIntervalScheduleNotes.MenuManager = this.barManager1;
            this.memIntervalScheduleNotes.Name = "memIntervalScheduleNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memIntervalScheduleNotes, true);
            this.memIntervalScheduleNotes.Size = new System.Drawing.Size(749, 120);
            this.scSpellChecker.SetSpellCheckerOptions(this.memIntervalScheduleNotes, optionsSpelling2);
            this.memIntervalScheduleNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.memIntervalScheduleNotes.TabIndex = 132;
            this.memIntervalScheduleNotes.UseOptimizedRendering = true;
            // 
            // IsCurrentIntervalCheckEdit
            // 
            this.IsCurrentIntervalCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11163ServiceScheduleBindingSource, "IsCurrentInterval", true));
            this.IsCurrentIntervalCheckEdit.Location = new System.Drawing.Point(132, 36);
            this.IsCurrentIntervalCheckEdit.MenuManager = this.barManager1;
            this.IsCurrentIntervalCheckEdit.Name = "IsCurrentIntervalCheckEdit";
            this.IsCurrentIntervalCheckEdit.Properties.Caption = "";
            this.IsCurrentIntervalCheckEdit.Size = new System.Drawing.Size(653, 19);
            this.IsCurrentIntervalCheckEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.IsCurrentIntervalCheckEdit.TabIndex = 133;
            // 
            // ItemForServiceDueDistance
            // 
            this.ItemForServiceDueDistance.Control = this.spnServiceDueDistance;
            this.ItemForServiceDueDistance.CustomizationFormText = "Service Due Distance :";
            this.ItemForServiceDueDistance.Location = new System.Drawing.Point(0, 71);
            this.ItemForServiceDueDistance.Name = "ItemForServiceDueDistance";
            this.ItemForServiceDueDistance.Size = new System.Drawing.Size(777, 24);
            this.ItemForServiceDueDistance.Text = "Service Due Distance :";
            this.ItemForServiceDueDistance.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForServiceDueDate
            // 
            this.ItemForServiceDueDate.Control = this.deServiceDueDate;
            this.ItemForServiceDueDate.CustomizationFormText = "Service Due Date :";
            this.ItemForServiceDueDate.Location = new System.Drawing.Point(0, 95);
            this.ItemForServiceDueDate.Name = "ItemForServiceDueDate";
            this.ItemForServiceDueDate.Size = new System.Drawing.Size(777, 24);
            this.ItemForServiceDueDate.Text = "Service Due Date :";
            this.ItemForServiceDueDate.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForAlertDistanceUnitsID
            // 
            this.ItemForAlertDistanceUnitsID.Control = this.lueAlertDistanceUnitsID;
            this.ItemForAlertDistanceUnitsID.CustomizationFormText = "Alert Distance Units :";
            this.ItemForAlertDistanceUnitsID.Location = new System.Drawing.Point(0, 119);
            this.ItemForAlertDistanceUnitsID.Name = "ItemForAlertDistanceUnitsID";
            this.ItemForAlertDistanceUnitsID.Size = new System.Drawing.Size(777, 24);
            this.ItemForAlertDistanceUnitsID.Text = "Alert Distance Units :";
            this.ItemForAlertDistanceUnitsID.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForAlertWithinXDistance
            // 
            this.ItemForAlertWithinXDistance.Control = this.spnAlertWithinXDistance;
            this.ItemForAlertWithinXDistance.CustomizationFormText = "Alert Within XDistance :";
            this.ItemForAlertWithinXDistance.Location = new System.Drawing.Point(0, 143);
            this.ItemForAlertWithinXDistance.Name = "ItemForAlertWithinXDistance";
            this.ItemForAlertWithinXDistance.Size = new System.Drawing.Size(777, 24);
            this.ItemForAlertWithinXDistance.Text = "Alert Within XDistance :";
            this.ItemForAlertWithinXDistance.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForAlertTimeUnitsID
            // 
            this.ItemForAlertTimeUnitsID.Control = this.lueAlertTimeUnitsID;
            this.ItemForAlertTimeUnitsID.CustomizationFormText = "Alert Time Units :";
            this.ItemForAlertTimeUnitsID.Location = new System.Drawing.Point(0, 167);
            this.ItemForAlertTimeUnitsID.Name = "ItemForAlertTimeUnitsID";
            this.ItemForAlertTimeUnitsID.Size = new System.Drawing.Size(777, 24);
            this.ItemForAlertTimeUnitsID.Text = "Alert Time Units :";
            this.ItemForAlertTimeUnitsID.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForAlertWithinXTime
            // 
            this.ItemForAlertWithinXTime.Control = this.spnAlertWithinXTime;
            this.ItemForAlertWithinXTime.CustomizationFormText = "Alert Within XTime :";
            this.ItemForAlertWithinXTime.Location = new System.Drawing.Point(0, 191);
            this.ItemForAlertWithinXTime.Name = "ItemForAlertWithinXTime";
            this.ItemForAlertWithinXTime.Size = new System.Drawing.Size(777, 24);
            this.ItemForAlertWithinXTime.Text = "Alert Within XTime :";
            this.ItemForAlertWithinXTime.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForWorkDetailID
            // 
            this.ItemForWorkDetailID.Control = this.lueWorkDetailID;
            this.ItemForWorkDetailID.CustomizationFormText = "Work Detail :";
            this.ItemForWorkDetailID.Location = new System.Drawing.Point(0, 215);
            this.ItemForWorkDetailID.Name = "ItemForWorkDetailID";
            this.ItemForWorkDetailID.Size = new System.Drawing.Size(777, 26);
            this.ItemForWorkDetailID.Text = "Work Detail :";
            this.ItemForWorkDetailID.TextSize = new System.Drawing.Size(117, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Service Interval Notes";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIntervalScheduleNotes});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 241);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(777, 167);
            this.layoutControlGroup2.Text = "Service Interval Notes";
            // 
            // ItemForIntervalScheduleNotes
            // 
            this.ItemForIntervalScheduleNotes.Control = this.memIntervalScheduleNotes;
            this.ItemForIntervalScheduleNotes.CustomizationFormText = "Interval Schedule Notes";
            this.ItemForIntervalScheduleNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForIntervalScheduleNotes.Name = "ItemForIntervalScheduleNotes";
            this.ItemForIntervalScheduleNotes.Size = new System.Drawing.Size(753, 124);
            this.ItemForIntervalScheduleNotes.Text = "Interval Schedule Notes";
            this.ItemForIntervalScheduleNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForIntervalScheduleNotes.TextToControlDistance = 0;
            this.ItemForIntervalScheduleNotes.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtServiceDataReference;
            this.layoutControlItem2.CustomizationFormText = "Sevice Data Reference :";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(777, 24);
            this.layoutControlItem2.Text = "Sevice Data Reference :";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForIsCurrentInterval
            // 
            this.ItemForIsCurrentInterval.Control = this.IsCurrentIntervalCheckEdit;
            this.ItemForIsCurrentInterval.CustomizationFormText = "Is Current Interval :";
            this.ItemForIsCurrentInterval.Location = new System.Drawing.Point(0, 24);
            this.ItemForIsCurrentInterval.Name = "ItemForIsCurrentInterval";
            this.ItemForIsCurrentInterval.Size = new System.Drawing.Size(777, 23);
            this.ItemForIsCurrentInterval.Text = "Is Current Interval :";
            this.ItemForIsCurrentInterval.TextSize = new System.Drawing.Size(117, 13);
            // 
            // sp_AS_11163_Service_ScheduleTableAdapter
            // 
            this.sp_AS_11163_Service_ScheduleTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Service_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Service_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Unit_Dist_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Unit_Dist_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Unit_Time_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Unit_Time_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11078_Work_Detail_ItemTableAdapter
            // 
            this.sp_AS_11078_Work_Detail_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_ServiceInterval_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(797, 484);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_ServiceInterval_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Service Interval";
            this.Activated += new System.EventHandler(this.frm_AS_ServiceInterval_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_ServiceInterval_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_ServiceInterval_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueServiceStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11163ServiceScheduleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLServiceStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtServiceDataReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnServiceDueDistance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deServiceDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAlertDistanceUnitsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitDistTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAlertWithinXDistance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAlertTimeUnitsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitTimeTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnAlertWithinXTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWorkDetailID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memIntervalScheduleNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsCurrentIntervalCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertDistanceUnitsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertWithinXDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertTimeUnitsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAlertWithinXTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkDetailID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIntervalScheduleNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsCurrentInterval)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.TextEdit txtServiceDataReference;
        private DevExpress.XtraEditors.LookUpEdit lueServiceStatusID;
        private System.Windows.Forms.BindingSource spAS11163ServiceScheduleBindingSource;
        private DevExpress.XtraEditors.SpinEdit spnServiceDueDistance;
        private DevExpress.XtraEditors.DateEdit deServiceDueDate;
        private DevExpress.XtraEditors.LookUpEdit lueAlertDistanceUnitsID;
        private DevExpress.XtraEditors.SpinEdit spnAlertWithinXDistance;
        private DevExpress.XtraEditors.LookUpEdit lueAlertTimeUnitsID;
        private DevExpress.XtraEditors.SpinEdit spnAlertWithinXTime;
        private DevExpress.XtraEditors.LookUpEdit lueWorkDetailID;
        private DevExpress.XtraEditors.MemoEdit memIntervalScheduleNotes;
        private DevExpress.XtraEditors.CheckEdit IsCurrentIntervalCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceDueDistance;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceDueDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAlertDistanceUnitsID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAlertWithinXDistance;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAlertTimeUnitsID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAlertWithinXTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkDetailID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsCurrentInterval;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIntervalScheduleNotes;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11163_Service_ScheduleTableAdapter sp_AS_11163_Service_ScheduleTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLServiceStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLUnitDistTypeBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLUnitTimeTypeBindingSource;
        private System.Windows.Forms.BindingSource spAS11078WorkDetailItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Service_StatusTableAdapter sp_AS_11000_PL_Service_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_Dist_TypeTableAdapter sp_AS_11000_PL_Unit_Dist_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_Time_TypeTableAdapter sp_AS_11000_PL_Unit_Time_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter sp_AS_11078_Work_Detail_ItemTableAdapter;


    }
}

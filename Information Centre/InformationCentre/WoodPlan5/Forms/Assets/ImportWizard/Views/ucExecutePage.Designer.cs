﻿namespace WoodPlan5.Forms.Assets.ImportWizard
{
    partial class ucExecutePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.spAS11017ModelListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.sp_AS_11044_Keeper_Allocation_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11002_Equipment_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11002_Equipment_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.sp_AS_11005_Vehicle_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter();
            this.sp_AS_11008_Plant_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11008_Plant_ItemTableAdapter();
            this.sp_AS_11029_Office_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11029_Office_ItemTableAdapter();
            this.sp_AS_11041_Transaction_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter();
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter();
            this.sp_AS_11075_Cover_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11075_Cover_ItemTableAdapter();
            this.sp_AS_11081_Incident_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter();
            this.sp_AS_11084_Purpose_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11084_Purpose_ItemTableAdapter();
            this.sp_AS_11089_Fuel_Card_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11089_Fuel_Card_ItemTableAdapter();
            this.sp_AS_11096_Road_Tax_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11096_Road_Tax_ItemTableAdapter();
            this.sp_AS_11126_Notification_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11126_Notification_ItemTableAdapter();
            this.sp_AS_11005_Vehicle_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11029OfficeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11041TransactionItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11075CoverItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11084PurposeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11089FuelCardItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11081IncidentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11126NotificationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11096RoadTaxItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11008_Plant_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_Duplicate_SearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_Duplicate_SearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter();
            this.spAS11093InitialBillingItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11093_Initial_Billing_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11093_Initial_Billing_ItemTableAdapter();
            this.sp_AS_11014_Make_ListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11014_Make_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter();
            this.sp_AS_11017_Model_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter();
            this.sp_AS_11078_Work_Detail_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11078_Work_Detail_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter();
            this.listBoxControl1 = new DevExpress.XtraEditors.ListBoxControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lbKeeperName = new DevExpress.XtraEditors.LabelControl();
            this.lblKeeper = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lblTitle = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lblChange1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lblChange2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lblText1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lblText2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lblinitialBillingName = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lblInitialBilling = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblAdditionalList = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lblTransactionType = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lblTransactionAmount = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp_AS_11166_P11D_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11166_P11D_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11166_P11D_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11044_Keeper_Allocation_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11002_Equipment_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11005_Vehicle_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11029OfficeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11075CoverItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11084PurposeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11089FuelCardItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11126NotificationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11096RoadTaxItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11008_Plant_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11000_Duplicate_SearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11093InitialBillingItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11014_Make_ListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11078_Work_Detail_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChange1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChange2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblinitialBillingName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInitialBilling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAdditionalList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTransactionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTransactionAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11166_P11D_ItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // spAS11017ModelListBindingSource
            // 
            this.spAS11017ModelListBindingSource.DataMember = "sp_AS_11017_Model_List";
            this.spAS11017ModelListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_AS_11044_Keeper_Allocation_ItemBindingSource
            // 
            this.sp_AS_11044_Keeper_Allocation_ItemBindingSource.DataMember = "sp_AS_11044_Keeper_Allocation_Item";
            this.sp_AS_11044_Keeper_Allocation_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11002_Equipment_ItemBindingSource
            // 
            this.sp_AS_11002_Equipment_ItemBindingSource.DataMember = "sp_AS_11002_Equipment_Item";
            this.sp_AS_11002_Equipment_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11002_Equipment_ItemTableAdapter
            // 
            this.sp_AS_11002_Equipment_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = this.sp_AS_11002_Equipment_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = this.sp_AS_11005_Vehicle_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = this.sp_AS_11008_Plant_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = this.sp_AS_11029_Office_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = this.sp_AS_11041_Transaction_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = this.sp_AS_11075_Cover_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = this.sp_AS_11081_Incident_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = this.sp_AS_11084_Purpose_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = this.sp_AS_11089_Fuel_Card_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = this.sp_AS_11096_Road_Tax_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = this.sp_AS_11126_Notification_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_AS_11005_Vehicle_ItemTableAdapter
            // 
            this.sp_AS_11005_Vehicle_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11008_Plant_ItemTableAdapter
            // 
            this.sp_AS_11008_Plant_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11029_Office_ItemTableAdapter
            // 
            this.sp_AS_11029_Office_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11041_Transaction_ItemTableAdapter
            // 
            this.sp_AS_11041_Transaction_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keeper_Allocation_ItemTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11075_Cover_ItemTableAdapter
            // 
            this.sp_AS_11075_Cover_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11081_Incident_ItemTableAdapter
            // 
            this.sp_AS_11081_Incident_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11084_Purpose_ItemTableAdapter
            // 
            this.sp_AS_11084_Purpose_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11089_Fuel_Card_ItemTableAdapter
            // 
            this.sp_AS_11089_Fuel_Card_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11096_Road_Tax_ItemTableAdapter
            // 
            this.sp_AS_11096_Road_Tax_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11126_Notification_ItemTableAdapter
            // 
            this.sp_AS_11126_Notification_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11005_Vehicle_ItemBindingSource
            // 
            this.sp_AS_11005_Vehicle_ItemBindingSource.DataMember = "sp_AS_11005_Vehicle_Item";
            this.sp_AS_11005_Vehicle_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11029OfficeItemBindingSource
            // 
            this.spAS11029OfficeItemBindingSource.DataMember = "sp_AS_11029_Office_Item";
            this.spAS11029OfficeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11041TransactionItemBindingSource
            // 
            this.spAS11041TransactionItemBindingSource.DataMember = "sp_AS_11041_Transaction_Item";
            this.spAS11041TransactionItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11075CoverItemBindingSource
            // 
            this.spAS11075CoverItemBindingSource.DataMember = "sp_AS_11075_Cover_Item";
            this.spAS11075CoverItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11084PurposeItemBindingSource
            // 
            this.spAS11084PurposeItemBindingSource.DataMember = "sp_AS_11084_Purpose_Item";
            this.spAS11084PurposeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11089FuelCardItemBindingSource
            // 
            this.spAS11089FuelCardItemBindingSource.DataMember = "sp_AS_11089_Fuel_Card_Item";
            this.spAS11089FuelCardItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11081IncidentItemBindingSource
            // 
            this.spAS11081IncidentItemBindingSource.DataMember = "sp_AS_11081_Incident_Item";
            this.spAS11081IncidentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11126NotificationItemBindingSource
            // 
            this.spAS11126NotificationItemBindingSource.DataMember = "sp_AS_11126_Notification_Item";
            this.spAS11126NotificationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11096RoadTaxItemBindingSource
            // 
            this.spAS11096RoadTaxItemBindingSource.DataMember = "sp_AS_11096_Road_Tax_Item";
            this.spAS11096RoadTaxItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11008_Plant_ItemBindingSource
            // 
            this.sp_AS_11008_Plant_ItemBindingSource.DataMember = "sp_AS_11008_Plant_Item";
            this.sp_AS_11008_Plant_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_Duplicate_SearchBindingSource
            // 
            this.sp_AS_11000_Duplicate_SearchBindingSource.DataMember = "sp_AS_11000_Duplicate_Search";
            this.sp_AS_11000_Duplicate_SearchBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_Duplicate_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11093InitialBillingItemBindingSource
            // 
            this.spAS11093InitialBillingItemBindingSource.DataMember = "sp_AS_11093_Initial_Billing_Item";
            this.spAS11093InitialBillingItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11093_Initial_Billing_ItemTableAdapter
            // 
            this.sp_AS_11093_Initial_Billing_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11014_Make_ListBindingSource
            // 
            this.sp_AS_11014_Make_ListBindingSource.DataMember = "sp_AS_11014_Make_List";
            this.sp_AS_11014_Make_ListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11014_Make_ListTableAdapter
            // 
            this.sp_AS_11014_Make_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11017_Model_ListTableAdapter
            // 
            this.sp_AS_11017_Model_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11078_Work_Detail_ItemBindingSource
            // 
            this.sp_AS_11078_Work_Detail_ItemBindingSource.DataMember = "sp_AS_11078_Work_Detail_Item";
            this.sp_AS_11078_Work_Detail_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11078_Work_Detail_ItemTableAdapter
            // 
            this.sp_AS_11078_Work_Detail_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // listBoxControl1
            // 
            this.listBoxControl1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxControl1.Appearance.Options.UseFont = true;
            this.listBoxControl1.Location = new System.Drawing.Point(371, 197);
            this.listBoxControl1.Name = "listBoxControl1";
            this.listBoxControl1.Size = new System.Drawing.Size(882, 301);
            this.listBoxControl1.StyleController = this.layoutControl1;
            this.listBoxControl1.TabIndex = 4;
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.lbKeeperName);
            this.layoutControl1.Controls.Add(this.lblKeeper);
            this.layoutControl1.Controls.Add(this.listBoxControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(0, 300, 450, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1296, 946);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lbKeeperName
            // 
            this.lbKeeperName.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lbKeeperName.Location = new System.Drawing.Point(371, 137);
            this.lbKeeperName.Name = "lbKeeperName";
            this.lbKeeperName.Size = new System.Drawing.Size(882, 16);
            this.lbKeeperName.StyleController = this.layoutControl1;
            this.lbKeeperName.TabIndex = 7;
            // 
            // lblKeeper
            // 
            this.lblKeeper.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblKeeper.Location = new System.Drawing.Point(53, 137);
            this.lblKeeper.Name = "lblKeeper";
            this.lblKeeper.Size = new System.Drawing.Size(314, 16);
            this.lblKeeper.StyleController = this.layoutControl1;
            this.lblKeeper.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lblTitle,
            this.emptySpaceItem1,
            this.simpleLabelItem2,
            this.lblChange1,
            this.emptySpaceItem2,
            this.lblChange2,
            this.lblText1,
            this.lblText2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.lblinitialBillingName,
            this.lblInitialBilling,
            this.layoutControlItem1,
            this.lblAdditionalList,
            this.lblTransactionType,
            this.lblTransactionAmount,
            this.emptySpaceItem5,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(40, 40, 0, 40);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1296, 946);
            this.layoutControlGroup1.TextVisible = false;
            this.layoutControlGroup1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;
            // 
            // lblTitle
            // 
            this.lblTitle.AllowHotTrack = false;
            this.lblTitle.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.AppearanceItemCaption.Options.UseFont = true;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(1214, 34);
            this.lblTitle.Text = "Commiting new records:";
            this.lblTitle.TextSize = new System.Drawing.Size(235, 30);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 94);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(10, 405);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 405);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(10, 405);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.CustomizationFormText = "Click Install to begin installation";
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 891);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 0, 0, 0);
            this.simpleLabelItem2.Size = new System.Drawing.Size(1214, 13);
            this.simpleLabelItem2.Text = "Click Next Save Record(s)";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(235, 13);
            // 
            // lblChange1
            // 
            this.lblChange1.AllowHotTrack = false;
            this.lblChange1.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChange1.AppearanceItemCaption.Options.UseFont = true;
            this.lblChange1.CustomizationFormText = "Commit Changes:";
            this.lblChange1.Location = new System.Drawing.Point(328, 94);
            this.lblChange1.Name = "lblChange1";
            this.lblChange1.Size = new System.Drawing.Size(886, 20);
            this.lblChange1.Text = " ";
            this.lblChange1.TextSize = new System.Drawing.Size(235, 16);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 499);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1214, 324);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lblChange2
            // 
            this.lblChange2.AllowHotTrack = false;
            this.lblChange2.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChange2.AppearanceItemCaption.Options.UseFont = true;
            this.lblChange2.CustomizationFormText = "change 2";
            this.lblChange2.Location = new System.Drawing.Point(328, 114);
            this.lblChange2.Name = "lblChange2";
            this.lblChange2.Size = new System.Drawing.Size(886, 20);
            this.lblChange2.Text = " ";
            this.lblChange2.TextSize = new System.Drawing.Size(235, 16);
            // 
            // lblText1
            // 
            this.lblText1.AllowHotTrack = false;
            this.lblText1.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText1.AppearanceItemCaption.Options.UseFont = true;
            this.lblText1.CustomizationFormText = "text1";
            this.lblText1.Location = new System.Drawing.Point(10, 94);
            this.lblText1.Name = "lblText1";
            this.lblText1.Size = new System.Drawing.Size(318, 20);
            this.lblText1.Text = " ";
            this.lblText1.TextSize = new System.Drawing.Size(235, 16);
            // 
            // lblText2
            // 
            this.lblText2.AllowHotTrack = false;
            this.lblText2.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText2.AppearanceItemCaption.Options.UseFont = true;
            this.lblText2.CustomizationFormText = "text2";
            this.lblText2.Location = new System.Drawing.Point(10, 114);
            this.lblText2.Name = "lblText2";
            this.lblText2.Size = new System.Drawing.Size(318, 20);
            this.lblText2.Text = " ";
            this.lblText2.TextSize = new System.Drawing.Size(235, 16);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 34);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1214, 60);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(10, 214);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(318, 285);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lblinitialBillingName
            // 
            this.lblinitialBillingName.AllowHotTrack = false;
            this.lblinitialBillingName.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblinitialBillingName.AppearanceItemCaption.Options.UseFont = true;
            this.lblinitialBillingName.CustomizationFormText = "change 2";
            this.lblinitialBillingName.Location = new System.Drawing.Point(328, 154);
            this.lblinitialBillingName.Name = "lblinitialBillingName";
            this.lblinitialBillingName.Size = new System.Drawing.Size(886, 20);
            this.lblinitialBillingName.Text = " ";
            this.lblinitialBillingName.TextSize = new System.Drawing.Size(235, 16);
            // 
            // lblInitialBilling
            // 
            this.lblInitialBilling.AllowHotTrack = false;
            this.lblInitialBilling.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInitialBilling.AppearanceItemCaption.Options.UseFont = true;
            this.lblInitialBilling.CustomizationFormText = "change 2";
            this.lblInitialBilling.Location = new System.Drawing.Point(10, 154);
            this.lblInitialBilling.Name = "lblInitialBilling";
            this.lblInitialBilling.Size = new System.Drawing.Size(318, 20);
            this.lblInitialBilling.Text = " ";
            this.lblInitialBilling.TextSize = new System.Drawing.Size(235, 16);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.listBoxControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(328, 194);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(886, 305);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // lblAdditionalList
            // 
            this.lblAdditionalList.AllowHotTrack = false;
            this.lblAdditionalList.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdditionalList.AppearanceItemCaption.Options.UseFont = true;
            this.lblAdditionalList.CustomizationFormText = "change 2";
            this.lblAdditionalList.Location = new System.Drawing.Point(10, 194);
            this.lblAdditionalList.Name = "lblAdditionalList";
            this.lblAdditionalList.Size = new System.Drawing.Size(318, 20);
            this.lblAdditionalList.Text = " ";
            this.lblAdditionalList.TextSize = new System.Drawing.Size(235, 16);
            // 
            // lblTransactionType
            // 
            this.lblTransactionType.AllowHotTrack = false;
            this.lblTransactionType.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransactionType.AppearanceItemCaption.Options.UseFont = true;
            this.lblTransactionType.CustomizationFormText = "change 2";
            this.lblTransactionType.Location = new System.Drawing.Point(10, 174);
            this.lblTransactionType.Name = "lblTransactionType";
            this.lblTransactionType.Size = new System.Drawing.Size(318, 20);
            this.lblTransactionType.Text = " ";
            this.lblTransactionType.TextSize = new System.Drawing.Size(235, 16);
            // 
            // lblTransactionAmount
            // 
            this.lblTransactionAmount.AllowHotTrack = false;
            this.lblTransactionAmount.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransactionAmount.AppearanceItemCaption.Options.UseFont = true;
            this.lblTransactionAmount.CustomizationFormText = "change 2";
            this.lblTransactionAmount.Location = new System.Drawing.Point(328, 174);
            this.lblTransactionAmount.Name = "lblTransactionAmount";
            this.lblTransactionAmount.Size = new System.Drawing.Size(886, 20);
            this.lblTransactionAmount.Text = " ";
            this.lblTransactionAmount.TextSize = new System.Drawing.Size(235, 16);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 823);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1214, 68);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.lblKeeper;
            this.layoutControlItem2.Location = new System.Drawing.Point(10, 134);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(318, 20);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.lbKeeperName;
            this.layoutControlItem3.Location = new System.Drawing.Point(328, 134);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(886, 20);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // sp_AS_11166_P11D_ItemBindingSource
            // 
            this.sp_AS_11166_P11D_ItemBindingSource.DataMember = "sp_AS_11166_P11D_Item";
            this.sp_AS_11166_P11D_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11166_P11D_ItemTableAdapter
            // 
            this.sp_AS_11166_P11D_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // ucExecutePage
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Office 2013";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ucExecutePage";
            this.Size = new System.Drawing.Size(1296, 946);
            this.Enter += new System.EventHandler(this.ucExecutePage_Enter);
            this.Validated += new System.EventHandler(this.ucExecutePage_Validated);
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11044_Keeper_Allocation_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11002_Equipment_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11005_Vehicle_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11029OfficeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11075CoverItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11084PurposeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11089FuelCardItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11126NotificationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11096RoadTaxItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11008_Plant_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11000_Duplicate_SearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11093InitialBillingItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11014_Make_ListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11078_Work_Detail_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChange1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChange2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblinitialBillingName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInitialBilling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAdditionalList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTransactionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTransactionAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11166_P11D_ItemBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private System.Windows.Forms.BindingSource sp_AS_11002_Equipment_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter sp_AS_11002_Equipment_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter sp_AS_11005_Vehicle_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11005_Vehicle_ItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11126NotificationItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11029_Office_ItemTableAdapter sp_AS_11029_Office_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter sp_AS_11041_Transaction_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11075_Cover_ItemTableAdapter sp_AS_11075_Cover_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter sp_AS_11081_Incident_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11084_Purpose_ItemTableAdapter sp_AS_11084_Purpose_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11089_Fuel_Card_ItemTableAdapter sp_AS_11089_Fuel_Card_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11126_Notification_ItemTableAdapter sp_AS_11126_Notification_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11029OfficeItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11041TransactionItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11075CoverItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11084PurposeItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11089FuelCardItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11081IncidentItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11096_Road_Tax_ItemTableAdapter sp_AS_11096_Road_Tax_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11096RoadTaxItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11008_Plant_ItemTableAdapter sp_AS_11008_Plant_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11008_Plant_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter sp_AS_11044_Keeper_Allocation_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11044_Keeper_Allocation_ItemBindingSource;
        private System.Windows.Forms.BindingSource sp_AS_11000_Duplicate_SearchBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter sp_AS_11000_Duplicate_SearchTableAdapter;
        private System.Windows.Forms.BindingSource spAS11093InitialBillingItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11093_Initial_Billing_ItemTableAdapter sp_AS_11093_Initial_Billing_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11014_Make_ListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter sp_AS_11014_Make_ListTableAdapter;
        private System.Windows.Forms.BindingSource spAS11017ModelListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter sp_AS_11017_Model_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11078_Work_Detail_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter sp_AS_11078_Work_Detail_ItemTableAdapter;
        private DevExpress.XtraEditors.ListBoxControl listBoxControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.LabelControl lbKeeperName;
        private DevExpress.XtraEditors.LabelControl lblKeeper;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem lblTitle;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraLayout.SimpleLabelItem lblChange1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.SimpleLabelItem lblChange2;
        private DevExpress.XtraLayout.SimpleLabelItem lblText1;
        private DevExpress.XtraLayout.SimpleLabelItem lblText2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.SimpleLabelItem lblinitialBillingName;
        private DevExpress.XtraLayout.SimpleLabelItem lblInitialBilling;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SimpleLabelItem lblAdditionalList;
        private DevExpress.XtraLayout.SimpleLabelItem lblTransactionType;
        private DevExpress.XtraLayout.SimpleLabelItem lblTransactionAmount;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource sp_AS_11166_P11D_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11166_P11D_ItemTableAdapter sp_AS_11166_P11D_ItemTableAdapter;
    }
}

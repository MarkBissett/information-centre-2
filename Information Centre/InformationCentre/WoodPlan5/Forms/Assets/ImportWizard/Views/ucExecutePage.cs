﻿using System;
using System.ComponentModel;
using System.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using System.Windows.Forms;

namespace WoodPlan5.Forms.Assets.ImportWizard
{
    public partial class ucExecutePage : Views.BaseWizardPage
    {
        public ucExecutePage()
        {
            InitializeComponent();
        }

        #region Declaration

        public static bool blnExecuteValid = false;

        int intEquipmentID = 0;
        int intEquipmentCategoryID = 0;
        public static DataRowView drEquipmentNewRowView;
        public static DataRowView drEquipmentChildNewRowView;
        public static DataRowView drTransactionNewRowView;
        public static DataRowView drKeeperNewRowView;
        public static DataRowView drNotificationDetailsNewRowView;
        //public static DataRowView drInitialBillingNewRowView;
        public static DataRowView drCoverNewRowView;
        public static DataRowView drFuelCoverNewRowView;
        public static DataRowView drServiceDataNewRowView;
        public static DataRowView drAssetPurposeNewRowView;
        public static DataRowView drWorkOrderNewRowView;
        public static DataRowView drIncidentNewRowView;
        public static DataRowView drRoadTaxNewRowView;
        public static string strKeeper = "";
        public static string strKeeperType = "";
        public static string strInitialBillingName = "";
        public static string strTransctionType = "";
        public static string strTransctionAmount = "";
        public static string strCoverType = "";
        public static string strNotificationType = "";
        public static string strWorkOrderType = "";
        public static string strIncidentType = "";
        public static string strRoadTaxPeriod = "";
        public static string strAssetPurpose = "";
        public static string strOfficeCategory = "";
        public static int intInitialBillingCode = 0;
        int intMake = 0;
        int intModel = 0;
        public static bool blnPageValid = false;
        public static bool blnOkToFireEvents = false;
        public static BaseObjects.GlobalSettings gsSettings = new BaseObjects.GlobalSettings();
        public static BaseObjects.GlobalSettings GlobalSettings
        {
            get
            {
                return gsSettings;
            }
            set
            {
                gsSettings = value;
            }
        }

        #endregion
        
        #region Event
          
        private void ucExecutePage_Enter(object sender, EventArgs e)
        {
            if (blnOkToFireEvents)
            {
                loadConfirmationPage();
                populateListControl();
                checkPageValid();
            }
        }

        private void checkPageValid()
        {
            DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow drEquipRow = (DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)drEquipmentNewRowView.Row;
            intEquipmentCategoryID = drEquipRow.EquipmentCategory;
            intMake = drEquipRow.Make;
            intModel = drEquipRow.Model;
            string strManufacturerID = drEquipRow.ManufacturerID;
            string searchMode = "";
            string searchValue = "";
            bool validManufacturerID = false;
            bool validVehicleRegistration = false;
            if (intEquipmentCategoryID != 7)
            {
                searchMode = "manufacturer_id";
                searchValue = strManufacturerID;
                validManufacturerID = checkDuplicate(searchValue, searchMode, intEquipmentCategoryID.ToString());

                if (intEquipmentCategoryID == 1)
                {
                    searchMode = "vehicle_plate";
                    searchValue = ((DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)drEquipmentChildNewRowView.Row).RegistrationPlate; 
                    validVehicleRegistration = checkDuplicate(searchValue, searchMode, intEquipmentCategoryID.ToString());
                }
                else
                {
                    validVehicleRegistration = true;
                }
            }
            //else
            //{
            //    searchMode = "rental";
            //    searchValue = vehicleRegPlate;
            //    validManufacturerID = checkDuplicate(searchValue, searchMode, intEquipmentCategoryID.ToString());
            //}
            if (validManufacturerID && validVehicleRegistration && ucStep3Page.blnKeeperValid && ucStep3Page.blnInitialBillingValid && ucStep3Page.blnTransactionValid)
            {
                blnExecuteValid = true;
            }
            else
            {
                XtraMessageBox.Show("Duplicate Manufacturer Identity already exists or required details have not been completed.", "Error in Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);              
                blnExecuteValid = false;
            }

            blnPageValid = blnExecuteValid;
        }

        private void populateListControl()
        {
            if (ucStep3Page.blnFuelCardValid)
            {
                listBoxControl1.Items.Add(string.Format("Fuel Card Number : {0}",Convert.ToString(drFuelCoverNewRowView["CardNumber"])));
            }

            if (ucStep3Page.blnCoverValid)
            {
                listBoxControl1.Items.Add(string.Format("Cover Type : {0}  Policy : {1}", strCoverType,Convert.ToString(drCoverNewRowView["Policy"])));
            }

            if (ucStep3Page.blnNotificationValid)
            {
                listBoxControl1.Items.Add(string.Format("Notification Type : {0} Date to Remind : {1}", strNotificationType, Convert.ToDateTime(drNotificationDetailsNewRowView["DateToRemind"]).ToString("ddd dd MMM yyyy")));
            }

            if (ucStep3Page.blnIncidentValid)
            {
                listBoxControl1.Items.Add(string.Format("Incident Type : {0} Incident Reference : {1}", strIncidentType, Convert.ToString(drIncidentNewRowView["IncidentReference"])));
            }

            if (ucStep3Page.blnRoadTaxValid)
            {
                listBoxControl1.Items.Add(string.Format("Road Tax Period : {0} Expiry Date : {1}", strRoadTaxPeriod, Convert.ToDateTime(drRoadTaxNewRowView["TaxExpiryDate"]).ToString("ddd dd MMM yyyy"))); 
            }

            if (ucStep3Page.blnAssetPurposeValid)
            {
                listBoxControl1.Items.Add(string.Format("Asset Purpose : {0}", strAssetPurpose));
            }

            if (ucStep3Page.blnWorkOrderValid)
            {
                listBoxControl1.Items.Add(string.Format("Work Order Type : {0} Start Date : {1}", strWorkOrderType, Convert.ToDateTime(drWorkOrderNewRowView["DateRaised"]).ToString("ddd dd MMM yyyy")));
            }

            if (listBoxControl1.ItemCount == 0)
            {
                listBoxControl1.Items.Add("None");
            }
        }

        #endregion

        #region Methods


        private void loadConfirmationPage()
        {

            resetLabels();
            Application.DoEvents();
            switch (Convert.ToInt32(drEquipmentNewRowView["EquipmentCategory"]))
            {
                case 1:
                    lblTitle.Text = "Commiting new vehicle record:";
                    lblText1.Text = "VIN Number :";
                    lblText2.Text = "Registration Plate :";

                    lblChange2.Text = Convert.ToString(drEquipmentChildNewRowView["RegistrationPlate"]);
                    break;
                case 2:
                    lblTitle.Text = "Commiting new plant & machinery record:";
                    lblText1.Text = "Manufacturer ID :";
                    lblText2.Text = "Plant Description :";
                    lblChange2.Text = Convert.ToString(drEquipmentChildNewRowView["ShortDescription"]);
                    break;
                case 6:
                    lblTitle.Text = "Commiting new plant & machinery record:";
                    lblText1.Text = "Manufacturer ID :";
                    lblText2.Text = "Office Category :";
                    lblChange2.Text = strOfficeCategory;
                    break;
                default:
                    
                    lblTitle.Text = "Commiting equipment record:";
                    lblText1.Text = "Manufacturer ID :";
                    lblText2.Text = "Description :";
                    break;
            }

            lblAdditionalList.Text = "Optional Linked Data :";
            lblChange1.Text = Convert.ToString(drEquipmentNewRowView["ManufacturerID"]);
            lblKeeper.Text = "Allocated Keeper :";
            lbKeeperName.Text = string.Format("{0} ({1})", strKeeper, strKeeperType);
            lblInitialBilling.Text = "Initial Billing Department :";
            lblinitialBillingName.Text = strInitialBillingName;
            lblTransactionType.Text = strTransctionType;
            lblTransactionAmount.Text = strTransctionAmount;
            listBoxControl1.Items.Clear();
        }

        private void save()
        {
            try
            {
                this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item.ImportRow((DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)drEquipmentNewRowView.Row);
                this.sp_AS_11002_Equipment_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                intEquipmentID = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item[0].EquipmentID;
                switch (Convert.ToInt32(drEquipmentNewRowView["EquipmentCategory"]))
                {
                    case 1:
                        DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow drNewVehicleRow = (DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)drEquipmentChildNewRowView.Row;
                        drNewVehicleRow.EquipmentID = intEquipmentID;
                        this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.ImportRow(drNewVehicleRow);
                        this.sp_AS_11005_Vehicle_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        break;
                    case 2:
                try
                        {
                                DataSet_AS_DataEntry.sp_AS_11008_Plant_ItemRow drNewPlantRow = (DataSet_AS_DataEntry.sp_AS_11008_Plant_ItemRow)drEquipmentChildNewRowView.Row;
                                drNewPlantRow.EquipmentID = intEquipmentID;
                                this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item.ImportRow(drNewPlantRow);
                                this.sp_AS_11008_Plant_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        catch (Exception)//ex)
                        {
                            //MessageBox.Show(ex.Message);
                        }
                        break;
                    case 6:
                        try
                        {
                            DataSet_AS_DataEntry.sp_AS_11029_Office_ItemRow drNewOfficeRow = (DataSet_AS_DataEntry.sp_AS_11029_Office_ItemRow)drEquipmentChildNewRowView.Row;
                            drNewOfficeRow.EquipmentID = intEquipmentID;
                            this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item.ImportRow(drNewOfficeRow);
                            this.sp_AS_11029_Office_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                        }
                        catch (Exception)//ex)
                        {
                            //MessageBox.Show(ex.Message);
                        }
                        break;
                    default:
                        break;
                }
                try
                {
                    if (ucStep3Page.blnInitialBillingValid)
                    {
                        this.sp_AS_11093_Initial_Billing_ItemTableAdapter.Insert("add", "", 0, intEquipmentID, intInitialBillingCode);
                    }
                }
                catch (Exception)//ex)
                {
                    //MessageBox.Show(ex.Message);
                }

                int intCurrentKeeperAllocationID = 0;
                if (ucStep3Page.blnKeeperValid)
                {
                    ((DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)drKeeperNewRowView.Row).EquipmentID = intEquipmentID;
                    this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item.ImportRow((DataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_ItemRow)drKeeperNewRowView.Row);
                    this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                    intCurrentKeeperAllocationID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID;
                }
                string strMake = "Unknown";
                string strModel = "Unknown";
                if (ucStep3Page.blnTransactionValid)
                {

                    this.sp_AS_11002_Equipment_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, intEquipmentID.ToString(), "view");
                    DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow drEquip = (DataSet_AS_DataEntry.sp_AS_11002_Equipment_ItemRow)dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item.Rows[0];

                    this.sp_AS_11014_Make_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11014_Make_List, intMake.ToString(), "view");
                    DataRow drMake = this.dataSet_AS_DataEntry.sp_AS_11014_Make_List.Rows[0];

                    this.sp_AS_11017_Model_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11017_Model_List, intModel.ToString(), "view");
                    DataRow drModel = this.dataSet_AS_DataEntry.sp_AS_11017_Model_List.Rows[0];

                    string gcRef = drEquip.EquipmentReference;
                    strMake = ((DataSet_AS_DataEntry.sp_AS_11014_Make_ListRow)(drMake)).Description;
                    strModel = ((DataSet_AS_DataEntry.sp_AS_11017_Model_ListRow)(drModel)).Description;
                    string tempMake = string.Format("GC Reference : {0}\r\nMake : {1}", gcRef, strMake);
                    string tempModel = string.Format("\r\nModel : {0}", strModel);


                    string tempRegistration = "";
                    string tempManufacturer = string.Format("\r\nManufacturer ID : {0}", drEquip.ManufacturerID);
                    switch (intEquipmentCategoryID)
                    {
                        case 1:
                            this.sp_AS_11005_Vehicle_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item, intEquipmentID.ToString(), "view");
                            DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow drVehicle = (DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0];
                            tempRegistration = string.Format("\r\nRegistration : {0}", drVehicle.RegistrationPlate);
                            tempManufacturer = string.Format("\r\nVIN : {0}", drEquip.ManufacturerID);
                            break;
                        default:
                            break;
                    }

                    string strTransactionDescription = tempMake + tempModel + tempManufacturer + tempRegistration;
                    DataSet_AS_DataEntry.sp_AS_11041_Transaction_ItemRow drNewTransactionRow = (DataSet_AS_DataEntry.sp_AS_11041_Transaction_ItemRow)drTransactionNewRowView.Row;
                    drNewTransactionRow.EquipmentID = intEquipmentID;
                    drNewTransactionRow.Description = strTransactionDescription;
                    this.dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item.ImportRow(drNewTransactionRow);                    
                    sp_AS_11041_Transaction_ItemTableAdapter.Connection.ConnectionString = GlobalSettings.ConnectionString;
                    this.sp_AS_11041_Transaction_ItemTableAdapter.Update(dataSet_AS_DataEntry);

                    Application.DoEvents();
                }

                // This call no longer required as the P11D creation is handled within the Keeper Allocation Store Proc (sp_AS_11045)
                //AddP11D(intEquipmentID, strMake, strModel, intCurrentKeeperAllocationID);
                //

                if (ucStep3Page.blnFuelCardValid)
                {
                    DataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_ItemRow drNewFuelCardRow = (DataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_ItemRow)drFuelCoverNewRowView.Row;
                    drNewFuelCardRow.EquipmentID = intEquipmentID;
                    drNewFuelCardRow.KeeperAllocationID = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID; 
                    this.dataSet_AS_DataEntry.sp_AS_11089_Fuel_Card_Item.ImportRow(drNewFuelCardRow);
                    this.sp_AS_11089_Fuel_Card_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                }

                if (ucStep3Page.blnCoverValid)
                {
                    ((DataSet_AS_DataEntry.sp_AS_11075_Cover_ItemRow)drCoverNewRowView.Row).EquipmentID = intEquipmentID;
                    this.dataSet_AS_DataEntry.sp_AS_11075_Cover_Item.ImportRow((DataSet_AS_DataEntry.sp_AS_11075_Cover_ItemRow)drCoverNewRowView.Row);
                    this.sp_AS_11075_Cover_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                }

                if (ucStep3Page.blnNotificationValid)
                {
                    ((DataSet_AS_DataEntry.sp_AS_11126_Notification_ItemRow)drNotificationDetailsNewRowView.Row).LinkedToRecordType = 11;
                    ((DataSet_AS_DataEntry.sp_AS_11126_Notification_ItemRow)drNotificationDetailsNewRowView.Row).LinkedToRecordID = intEquipmentID;
                    this.dataSet_AS_DataEntry.sp_AS_11126_Notification_Item.ImportRow((DataSet_AS_DataEntry.sp_AS_11126_Notification_ItemRow)drNotificationDetailsNewRowView.Row);
                    this.sp_AS_11126_Notification_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                }

                if (ucStep3Page.blnIncidentValid)
                {
                    ((DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)drIncidentNewRowView.Row).EquipmentID = intEquipmentID;
                    this.dataSet_AS_DataEntry.sp_AS_11081_Incident_Item.ImportRow((DataSet_AS_DataEntry.sp_AS_11081_Incident_ItemRow)drIncidentNewRowView.Row);
                    this.sp_AS_11081_Incident_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                }

                if (ucStep3Page.blnRoadTaxValid)
                {
                    ((DataSet_AS_DataEntry.sp_AS_11096_Road_Tax_ItemRow)drRoadTaxNewRowView.Row).EquipmentID = intEquipmentID;
                    this.dataSet_AS_DataEntry.sp_AS_11096_Road_Tax_Item.ImportRow((DataSet_AS_DataEntry.sp_AS_11096_Road_Tax_ItemRow)drRoadTaxNewRowView.Row);
                    this.sp_AS_11096_Road_Tax_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                }

                if (ucStep3Page.blnAssetPurposeValid)
                {
                    ((DataSet_AS_DataEntry.sp_AS_11084_Purpose_ItemRow)drAssetPurposeNewRowView.Row).EquipmentID = intEquipmentID;
                    this.dataSet_AS_DataEntry.sp_AS_11084_Purpose_Item.ImportRow((DataSet_AS_DataEntry.sp_AS_11084_Purpose_ItemRow)drAssetPurposeNewRowView.Row);
                    this.sp_AS_11084_Purpose_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                }

                if (ucStep3Page.blnWorkOrderValid)
                {
                    ((DataSet_AS_DataEntry.sp_AS_11078_Work_Detail_ItemRow)drWorkOrderNewRowView.Row).EquipmentID = intEquipmentID;
                    ((DataSet_AS_DataEntry.sp_AS_11078_Work_Detail_ItemRow)drWorkOrderNewRowView.Row).TransactionID = this.dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item[0].TransactionID; 
                    this.dataSet_AS_DataEntry.sp_AS_11078_Work_Detail_Item.ImportRow((DataSet_AS_DataEntry.sp_AS_11078_Work_Detail_ItemRow)drWorkOrderNewRowView.Row);
                    this.sp_AS_11078_Work_Detail_ItemTableAdapter.Update(dataSet_AS_DataEntry);
                }
            }
            catch (Exception ex)
            {
                var dialogType = typeof(Form).Assembly.GetType("System.Windows.Forms.PropertyGridInternal.GridErrorDlg");
                var dialog = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());
                dialog.Text = "Error Saving Changes";
                dialogType.GetProperty("Details").SetValue(dialog, ex.Message, null);
                dialogType.GetProperty("Message").SetValue(dialog, String.Format("An error occurred while saving the record changes [{0}]!\n\nTry saving again - if the problem persists, contact Technical Support.", ex.Message), null);
                var msgResult = dialog.ShowDialog();
            }

        }

        /*** redundant method as the P11D set up now included within the KeeperAllocation Store proc (sp_AS_11045_Keeper_Allocation_Update)
        private void AddP11D(int intEquipmentID, string sMake, string sModel, int intCurrentKeeperAllocationID)
        {
            sp_AS_11044_Keeper_Allocation_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item, intCurrentKeeperAllocationID.ToString(), "");

                    //int intRowEquipID = 0;
                    //int intRowP11DID = -1;
                    //string strRowManufacturerID = "";
                    //int intRowKeeperAllocationID = 0;
                    //System.DateTime deRowTransactionDate = getCurrentDate();
                    //int intRowKeeperType = 0;
                    //int intRowKeeperID = 0;
                    //string strRowKeeperFullName = "";
                    Nullable<DateTime> deRowAllocationDate = null;
                    Nullable<DateTime> deRowAllocationEndDate = null;
                    //int intRowAllocationStatus = 0;
                    //Decimal dcRowListPrice = 0;
                    //Decimal dcRowAdditionalCost = 0;
                    int intRowEmissions = 0;
                    int intRowEngineSize = 0;
                    string strRowFuelType = "None";
                    string strRowRegistrationPlate = "None";
                    Nullable<DateTime> deRowRegistrationDate = null;
                    //string strRowMake = "None";
                    //string strRowModel = "None";
                    //string strRowNotes = "New Keeper Record";

                    DataRow drNewP11Row = this.dataSet_AS_DataEntry.sp_AS_11166_P11D_Item.NewRow();
                    drNewP11Row["Mode"] = "add";
                    drNewP11Row["RecordID"] = "-1";
                    drNewP11Row["EquipmentID"] = intEquipmentID;
                    drNewP11Row["P11DID"] = -1;
                    drNewP11Row["ManufacturerID"] = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item[0].ManufacturerID;
                    drNewP11Row["KeeperAllocationID"] = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperAllocationID;
                    drNewP11Row["TransactionDate"] = this.dataSet_AS_DataEntry.sp_AS_11041_Transaction_Item[0].TransactionDate;
                    drNewP11Row["KeeperTypeID"] = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperTypeID;
                    drNewP11Row["KeeperID"] = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].KeeperID;
                    drNewP11Row["KeeperFullName"] = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].Keeper;

                    //object value = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].AllocationDate;
                    //if (value != DBNull.Value)
                    //{
                    //    deRowAllocationDate = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].AllocationDate;
                    //}
                    //if (deRowAllocationDate.HasValue)
                    //{
                    //    drNewP11Row["AllocationDate"] = deRowAllocationDate;
                    //}

                    try
                    {
                        if (!DBNull.Value.Equals(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].AllocationDate))
                        {
                            drNewP11Row["AllocationDate"] = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].AllocationDate;
                        }
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        if (!DBNull.Value.Equals(this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].AllocationEndDate))
                        {
                            drNewP11Row["AllocationEndDate"] = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].AllocationEndDate;
                        }
                    }
                    catch (Exception)
                    {
                        
                    }
                    drNewP11Row["AllocationStatus"] = this.dataSet_AS_DataEntry.sp_AS_11044_Keeper_Allocation_Item[0].AllocationStatusID;
                    drNewP11Row["ListPrice"] = 0;
                    drNewP11Row["AdditionalCost"] = 0;

                    if (this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows.Count > 0)
                    {
                        intRowEmissions = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0])).Emissions;
                        intRowEngineSize = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0])).EngineSize;
                        strRowFuelType = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0])).FuelType;
                        strRowRegistrationPlate = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0])).RegistrationPlate;
                        try
                        {
                            deRowRegistrationDate = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11005_Vehicle_ItemRow)(this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows[0])).RegistrationDate;
                        }
                        catch (StrongTypingException ex)
                        { }
                    }

                    drNewP11Row["Emissions"] = intRowEmissions;
                    drNewP11Row["EngineSize"] = intRowEngineSize;
                    drNewP11Row["FuelType"] = strRowFuelType;
                    drNewP11Row["RegistrationPlate"] = strRowRegistrationPlate;
                    if (deRowRegistrationDate.HasValue)
                    {
                        drNewP11Row["RegistrationDate"] = deRowRegistrationDate;
                    }
                    drNewP11Row["Make"] = sMake;
                    drNewP11Row["Model"] = sModel;
                    drNewP11Row["Notes"] = "New Keeper Record";
                    drNewP11Row["SelectedForEmail"] = true;
                    this.dataSet_AS_DataEntry.sp_AS_11166_P11D_Item.Rows.Add(drNewP11Row);
                    this.sp_AS_11166_P11D_ItemTableAdapter.Update(dataSet_AS_DataEntry);
        }
        ***/


        internal static bool checkValid()
        {
            return blnPageValid;
        }

        private bool checkDuplicate(string searchValue, string searchMode, string equipmentType)
        {
            bool dup = true;

            sp_AS_11000_Duplicate_SearchTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search, searchValue, Convert.ToInt32(equipmentType), searchMode);

            switch (this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search.Count)
            {
                case 0:
                    dup = true;
                    break;
                case 1:
                    dup = false;
                    break;
                default:
                    dup = true;
                    break;
            }
            return dup;
        }

        private void resetLabels()
        {
            lblTitle.Text = "Commiting new record:";
            lblText1.Text = "";
            lblText2.Text = "";
            lblChange1.Text = "";
            lblChange2.Text = "";
        }

        #endregion


        private void ucExecutePage_Validated(object sender, EventArgs e)
        {
            checkPageValid();

            if (blnPageValid)
            {
                save();
            }
            else
            {
                MessageBox.Show("This page has inconsistant data and will not be saved");
            }
        }
    }
}
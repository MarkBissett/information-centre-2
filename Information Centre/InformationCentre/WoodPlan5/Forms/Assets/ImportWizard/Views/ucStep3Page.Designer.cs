﻿namespace WoodPlan5.Forms.Assets.ImportWizard
{
    partial class ucStep3Page
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucStep3Page));
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tiInitialBilling = new DevExpress.XtraEditors.TileItem();
            this.tiKeeperDetails = new DevExpress.XtraEditors.TileItem();
            this.tiTransactionDetails = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tiIncident = new DevExpress.XtraEditors.TileItem();
            this.tiFuelCard = new DevExpress.XtraEditors.TileItem();
            this.tiWorkOrderDetails = new DevExpress.XtraEditors.TileItem();
            this.tiNotificationDetails = new DevExpress.XtraEditors.TileItem();
            this.tiCoverDetails = new DevExpress.XtraEditors.TileItem();
            this.tiRoadTax = new DevExpress.XtraEditors.TileItem();
            this.tiAssetPurpose = new DevExpress.XtraEditors.TileItem();
            this.tiServiceData = new DevExpress.XtraEditors.TileItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tileItem1 = new DevExpress.XtraEditors.TileItem();
            this.tileItem3 = new DevExpress.XtraEditors.TileItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.tileControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1543, 340, 450, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1000, 650);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // tileControl1
            // 
            this.tileControl1.AppearanceItem.Normal.BackColor = System.Drawing.Color.AliceBlue;
            this.tileControl1.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileControl1.DragSize = new System.Drawing.Size(0, 0);
            this.tileControl1.Groups.Add(this.tileGroup1);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.ItemSize = 150;
            this.tileControl1.Location = new System.Drawing.Point(42, 60);
            this.tileControl1.MaxId = 37;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Size = new System.Drawing.Size(916, 525);
            this.tileControl1.TabIndex = 4;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup1
            // 
            this.tileGroup1.Items.Add(this.tiInitialBilling);
            this.tileGroup1.Items.Add(this.tiKeeperDetails);
            this.tileGroup1.Items.Add(this.tiTransactionDetails);
            this.tileGroup1.Name = "tileGroup1";
            this.tileGroup1.Tag = "Mandatory";
            this.tileGroup1.Text = "Mandatory";
            // 
            // tiInitialBilling
            // 
            this.tiInitialBilling.AppearanceItem.Normal.BackColor = System.Drawing.Color.Red;
            this.tiInitialBilling.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiInitialBilling.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tiInitialBilling.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiInitialBilling.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiInitialBilling.AppearanceItem.Normal.Options.UseFont = true;
            this.tiInitialBilling.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiInitialBilling.BackgroundImage")));
            tileItemElement1.Text = "Initial Billing                                      (Mandatory)";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiInitialBilling.Elements.Add(tileItemElement1);
            this.tiInitialBilling.Id = 5;
            this.tiInitialBilling.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tiInitialBilling.Name = "tiInitialBilling";
            this.tiInitialBilling.Tag = "InitialBilling";
            this.tiInitialBilling.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // tiKeeperDetails
            // 
            this.tiKeeperDetails.AppearanceItem.Normal.BackColor = System.Drawing.Color.Red;
            this.tiKeeperDetails.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiKeeperDetails.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tiKeeperDetails.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiKeeperDetails.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiKeeperDetails.AppearanceItem.Normal.Options.UseFont = true;
            this.tiKeeperDetails.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiKeeperDetails.BackgroundImage")));
            tileItemElement2.Text = "Keeper Details                                      (Mandatory)";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiKeeperDetails.Elements.Add(tileItemElement2);
            this.tiKeeperDetails.Id = 6;
            this.tiKeeperDetails.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tiKeeperDetails.Name = "tiKeeperDetails";
            this.tiKeeperDetails.Tag = "KeeperDetails";
            this.tiKeeperDetails.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // tiTransactionDetails
            // 
            this.tiTransactionDetails.AppearanceItem.Normal.BackColor = System.Drawing.Color.Red;
            this.tiTransactionDetails.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiTransactionDetails.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tiTransactionDetails.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiTransactionDetails.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiTransactionDetails.AppearanceItem.Normal.Options.UseFont = true;
            this.tiTransactionDetails.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiTransactionDetails.BackgroundImage")));
            tileItemElement3.Appearance.Normal.BackColor = System.Drawing.Color.Transparent;
            tileItemElement3.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            tileItemElement3.Appearance.Normal.Options.UseBackColor = true;
            tileItemElement3.Appearance.Normal.Options.UseFont = true;
            tileItemElement3.Text = "Transaction Details                                      (Mandatory)";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiTransactionDetails.Elements.Add(tileItemElement3);
            this.tiTransactionDetails.Id = 7;
            this.tiTransactionDetails.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tiTransactionDetails.Name = "tiTransactionDetails";
            this.tiTransactionDetails.Tag = "TransactionDetails";
            this.tiTransactionDetails.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.tiIncident);
            this.tileGroup3.Items.Add(this.tiFuelCard);
            this.tileGroup3.Items.Add(this.tiWorkOrderDetails);
            this.tileGroup3.Items.Add(this.tiNotificationDetails);
            this.tileGroup3.Items.Add(this.tiCoverDetails);
            this.tileGroup3.Items.Add(this.tiRoadTax);
            this.tileGroup3.Items.Add(this.tiAssetPurpose);
            this.tileGroup3.Items.Add(this.tiServiceData);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tiIncident
            // 
            this.tiIncident.AppearanceItem.Normal.BackColor = System.Drawing.Color.Indigo;
            this.tiIncident.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiIncident.AppearanceItem.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tiIncident.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiIncident.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiIncident.AppearanceItem.Normal.Options.UseFont = true;
            this.tiIncident.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiIncident.BackgroundImage")));
            tileItemElement4.Text = "Incident Details";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiIncident.Elements.Add(tileItemElement4);
            this.tiIncident.Id = 17;
            this.tiIncident.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tiIncident.Name = "tiIncident";
            this.tiIncident.Tag = "IncidentDetails";
            this.tiIncident.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // tiFuelCard
            // 
            this.tiFuelCard.AppearanceItem.Normal.BackColor = System.Drawing.Color.Indigo;
            this.tiFuelCard.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiFuelCard.AppearanceItem.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tiFuelCard.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiFuelCard.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiFuelCard.AppearanceItem.Normal.Options.UseFont = true;
            this.tiFuelCard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiFuelCard.BackgroundImage")));
            tileItemElement5.Text = "Fuel Card";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiFuelCard.Elements.Add(tileItemElement5);
            this.tiFuelCard.Id = 18;
            this.tiFuelCard.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tiFuelCard.Name = "tiFuelCard";
            this.tiFuelCard.Tag = "FuelCard";
            this.tiFuelCard.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // tiWorkOrderDetails
            // 
            this.tiWorkOrderDetails.AppearanceItem.Normal.BackColor = System.Drawing.Color.Indigo;
            this.tiWorkOrderDetails.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiWorkOrderDetails.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tiWorkOrderDetails.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiWorkOrderDetails.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiWorkOrderDetails.AppearanceItem.Normal.Options.UseFont = true;
            this.tiWorkOrderDetails.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiWorkOrderDetails.BackgroundImage")));
            tileItemElement6.Text = "Work Order Details";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiWorkOrderDetails.Elements.Add(tileItemElement6);
            this.tiWorkOrderDetails.Id = 15;
            this.tiWorkOrderDetails.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tiWorkOrderDetails.Name = "tiWorkOrderDetails";
            this.tiWorkOrderDetails.Tag = "WorkOrderDetail";
            this.tiWorkOrderDetails.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // tiNotificationDetails
            // 
            this.tiNotificationDetails.AppearanceItem.Normal.BackColor = System.Drawing.Color.Indigo;
            this.tiNotificationDetails.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiNotificationDetails.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tiNotificationDetails.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiNotificationDetails.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiNotificationDetails.AppearanceItem.Normal.Options.UseFont = true;
            this.tiNotificationDetails.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiNotificationDetails.BackgroundImage")));
            tileItemElement7.Text = "Notification Details";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiNotificationDetails.Elements.Add(tileItemElement7);
            this.tiNotificationDetails.Id = 12;
            this.tiNotificationDetails.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tiNotificationDetails.Name = "tiNotificationDetails";
            this.tiNotificationDetails.Tag = "NotificationDetails";
            this.tiNotificationDetails.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // tiCoverDetails
            // 
            this.tiCoverDetails.AppearanceItem.Normal.BackColor = System.Drawing.Color.Indigo;
            this.tiCoverDetails.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiCoverDetails.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tiCoverDetails.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiCoverDetails.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiCoverDetails.AppearanceItem.Normal.Options.UseFont = true;
            this.tiCoverDetails.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiCoverDetails.BackgroundImage")));
            tileItemElement8.Text = "Policy & Insurance Cover Details";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiCoverDetails.Elements.Add(tileItemElement8);
            this.tiCoverDetails.Id = 13;
            this.tiCoverDetails.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tiCoverDetails.Name = "tiCoverDetails";
            this.tiCoverDetails.Tag = "CoverDetails";
            this.tiCoverDetails.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // tiRoadTax
            // 
            this.tiRoadTax.AppearanceItem.Normal.BackColor = System.Drawing.Color.Indigo;
            this.tiRoadTax.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiRoadTax.AppearanceItem.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tiRoadTax.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiRoadTax.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiRoadTax.AppearanceItem.Normal.Options.UseFont = true;
            this.tiRoadTax.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiRoadTax.BackgroundImage")));
            tileItemElement9.Text = "Road Tax";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiRoadTax.Elements.Add(tileItemElement9);
            this.tiRoadTax.Id = 22;
            this.tiRoadTax.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tiRoadTax.Name = "tiRoadTax";
            this.tiRoadTax.Tag = "RoadTax";
            this.tiRoadTax.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // tiAssetPurpose
            // 
            this.tiAssetPurpose.AppearanceItem.Normal.BackColor = System.Drawing.Color.Indigo;
            this.tiAssetPurpose.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiAssetPurpose.AppearanceItem.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tiAssetPurpose.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiAssetPurpose.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiAssetPurpose.AppearanceItem.Normal.Options.UseFont = true;
            this.tiAssetPurpose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiAssetPurpose.BackgroundImage")));
            tileItemElement10.Text = "Asset Purpose";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiAssetPurpose.Elements.Add(tileItemElement10);
            this.tiAssetPurpose.Id = 19;
            this.tiAssetPurpose.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tiAssetPurpose.Name = "tiAssetPurpose";
            this.tiAssetPurpose.Tag = "AssetPurpose";
            this.tiAssetPurpose.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // tiServiceData
            // 
            this.tiServiceData.AppearanceItem.Normal.BackColor = System.Drawing.Color.Indigo;
            this.tiServiceData.AppearanceItem.Normal.BorderColor = System.Drawing.Color.Black;
            this.tiServiceData.AppearanceItem.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tiServiceData.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tiServiceData.AppearanceItem.Normal.Options.UseBorderColor = true;
            this.tiServiceData.AppearanceItem.Normal.Options.UseFont = true;
            this.tiServiceData.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tiServiceData.BackgroundImage")));
            tileItemElement11.Text = "Service Data ";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tiServiceData.Elements.Add(tileItemElement11);
            this.tiServiceData.Id = 14;
            this.tiServiceData.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tiServiceData.Name = "tiServiceData";
            this.tiServiceData.Tag = "ServiceData";
            this.tiServiceData.Visible = false;
            this.tiServiceData.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tile_ItemClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem2,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(40, 40, 0, 40);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1000, 650);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem1.CustomizationFormText = "Installation progress:";
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(896, 34);
            this.simpleLabelItem1.Text = "Additional Details:";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(174, 30);
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.CustomizationFormText = "Click Install to begin installation";
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 597);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 0, 0, 0);
            this.simpleLabelItem2.Size = new System.Drawing.Size(920, 13);
            this.simpleLabelItem2.Text = "Click Next to continue";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(174, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.tileControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(920, 529);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 587);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(920, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tileItem1
            // 
            this.tileItem1.AppearanceItem.Normal.BackColor = System.Drawing.Color.CornflowerBlue;
            this.tileItem1.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tileItem1.BackgroundImage")));
            tileItemElement12.Appearance.Normal.BackColor = System.Drawing.Color.Transparent;
            tileItemElement12.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            tileItemElement12.Appearance.Normal.Options.UseBackColor = true;
            tileItemElement12.Appearance.Normal.Options.UseFont = true;
            tileItemElement12.Text = "Transaction Details";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tileItem1.Elements.Add(tileItemElement12);
            this.tileItem1.Id = 7;
            this.tileItem1.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItem1.Name = "tileItem1";
            // 
            // tileItem3
            // 
            this.tileItem3.AppearanceItem.Normal.BackColor = System.Drawing.Color.Indigo;
            this.tileItem3.AppearanceItem.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tileItem3.AppearanceItem.Normal.Options.UseBackColor = true;
            this.tileItem3.AppearanceItem.Normal.Options.UseFont = true;
            tileItemElement13.Text = "Fuel Card";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomRight;
            this.tileItem3.Elements.Add(tileItemElement13);
            this.tileItem3.Id = 18;
            this.tileItem3.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem3.Name = "tileItem3";
            this.tileItem3.Tag = "FuelCard";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(920, 58);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // ucStep3Page
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Office 2013";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ucStep3Page";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileItem tiInitialBilling;
        private DevExpress.XtraEditors.TileItem tiKeeperDetails;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem tiTransactionDetails;
        private DevExpress.XtraEditors.TileItem tileItem1;
        private DevExpress.XtraEditors.TileItem tiNotificationDetails;
        private DevExpress.XtraEditors.TileItem tiCoverDetails;
        private DevExpress.XtraEditors.TileItem tiServiceData;
        private DevExpress.XtraEditors.TileItem tiWorkOrderDetails;
        private DevExpress.XtraEditors.TileItem tiIncident;
        private DevExpress.XtraEditors.TileItem tiAssetPurpose;
        private DevExpress.XtraEditors.TileItem tiFuelCard;
        private DevExpress.XtraEditors.TileItem tiRoadTax;
        private DevExpress.XtraEditors.TileItem tileItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
    }
}

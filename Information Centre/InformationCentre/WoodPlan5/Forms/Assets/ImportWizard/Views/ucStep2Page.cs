﻿using DevExpress.XtraDataLayout;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using System;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Linq;

namespace WoodPlan5.Forms.Assets.ImportWizard
{
    public partial class ucStep2Page : Views.BaseWizardPage
    {
        public ucStep2Page()
        {
            InitializeComponent();
        }
        #region Declaration

        public static bool blnStep2Valid = false;
        public static bool blnPageValid = false;
        public enum SentenceCase { Upper, Lower, Title, AsIs }
        bool bool_FormLoading = true;
        internal static int intAddNewRecordCounter = 0;

        int intDefaultFuelType = 0;
        int intDefaultTowbarType = 0;
        const int intDefaultTotalTyreNumber = 5;
        const int intDefaultEngineSize = 0;
        const int intDefaultEmissions = 0;
        const int intDefaultServiceDueMileage = 0;
        const int intDefaultCurrentMileage = 0;
        public static string strUserName = "";
        int intCount = 0;

        #endregion

        #region Methods

        #region ValidateMethod

        private bool validateDateEdit(DateEdit dateEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
            
            if (dateEdit.Visible == false)
                return valid;

            string ErrorText = "Please Enter a Valid Date.";
            if (dateEdit.Tag != null)
            {
                ErrorText = string.Format("Please Enter a valid {0}.", dateEdit.Tag.ToString());
            }
            //Check if date is after 


            return valid;
        }

        private bool validateLookupEdit(LookUpEdit lookupEdit, EquipmentType equipmentType)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
        
            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(ucStep1Page.passedEquipType))
                {
                    dxErrorProvider.SetError(lookupEdit, "");
                    return valid;
                }
            }

            string ErrorText = "Please Select Value.";

            if (lookupEdit.Tag == null ? false : true)
            {
                ErrorText = "Please Select " + lookupEdit.Tag + ".";
            }
            if ((lookupEdit.EditValue == null) || ucStep1Page.strFormMode.ToLower() != "blockedit" && string.IsNullOrEmpty(lookupEdit.EditValue.ToString()))
            {
                dxErrorProvider.SetError(lookupEdit, ErrorText);
                blnPageValid = !dxErrorProvider.HasErrors;
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }
            else
            {
                dxErrorProvider.SetError(lookupEdit, "");
                blnPageValid = !dxErrorProvider.HasErrors;
                return valid;
            }
        }

        private bool validateMemEdit(MemoEdit txtBox, SentenceCase SentenceCase, EquipmentType equipmentType)
        {
            if (bool_FormLoading)
                return true;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(ucStep1Page.passedEquipType))
                {
                    dxErrorProvider.SetError(txtBox, "");
                    blnPageValid = !dxErrorProvider.HasErrors;
                    return true;
                }
            }
            bool valid = false;
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.Text = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.Text = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            string originalText = txtBox.Text;
            //check empty field
            if (originalText == "")
            {
                dxErrorProvider.SetError(txtBox, "Please enter a valid text.");
                blnPageValid = !dxErrorProvider.HasErrors;
                return valid;
            }
            else
            {
                valid = true;
            }

            if (valid)
            {
                dxErrorProvider.SetError(txtBox, "");
                blnPageValid = !dxErrorProvider.HasErrors;
            }
            return valid;
        }

        private bool validateTextBox(TextEdit txtBox, SentenceCase SentenceCase, EquipmentType equipmentType)
        {
            if (bool_FormLoading)
                return true;
            
            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(ucStep1Page.passedEquipType))
                {
                    dxErrorProvider.SetError(txtBox, "");
                    blnPageValid = !dxErrorProvider.HasErrors;
                    return true;
                }
            }
            bool valid = false;
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.Text = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.Text = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            if (equipmentType == EquipmentType.Plant && txtBox.Text == "N/A")
            {
                dxErrorProvider.SetError(txtBox, "");
                return true;
            }
            string originalText = txtBox.Text;
            string parsedText = "";

            switch (txtBox.Name)
            {
                case "txtServiceTag":
                case "txtShortDescription":
                    parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9 ]", "");
                    break;
                default:
                    parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9]", "");
                    break;
            }

            //check non alphanumeric characters
            if (originalText != parsedText || (txtBox.Text == "" && txtBox.Name != "txtPlantRegistrationPlate"))
            {
                dxErrorProvider.SetError(txtBox, "Please enter a valid text, avoid entering non alphanumeric characters, invalid characters have been removed");
                blnPageValid = !dxErrorProvider.HasErrors;
                //txtBox.Focus();
                return valid;
            }

            //check registration plate length
            if ((txtBox.Name == "txtRegistration_Plate" || txtBox.Name == "txtPlantRegistrationPlate") && parsedText.Length > 7)
            {
                dxErrorProvider.SetError(txtBox, "Supplied registration exceeds 7 characters");
                blnPageValid = !dxErrorProvider.HasErrors;
                return valid;
            }


            //check duplication last
            if (txtBox.Name == "txtRegistrationPlate" || txtBox.Name == "txtPlantRegistrationPlate" || txtBox.Name == "txtManufacturerID" || txtBox.Name == "txtIMEI")
            {
                string searchMode, searchValue;

                switch (txtBox.Name)
                {
                    case "txtRegistrationPlate":
                        searchMode = "vehicle_plate";
                        searchValue = txtBox.Text;

                        valid = checkDuplicate(searchValue, searchMode, EquipmentType.Vehicle);
                        if (!valid)
                        {
                            dxErrorProvider.SetError(txtBox, "Duplicate Registration Plate already exists");
                            blnPageValid = !dxErrorProvider.HasErrors;
                        }
                        break;
                    case "txtPlantRegistrationPlate":
                        searchMode = "plant_plate";
                        searchValue = txtBox.Text;

                        valid = checkDuplicate(searchValue, searchMode, EquipmentType.Plant);
                        if (!valid)
                        {
                            dxErrorProvider.SetError(txtBox, "Duplicate Registration Plate already exists");
                        }
                        break;
                    case "txtIMEI":
                        searchMode = "imei";
                        searchValue = txtBox.Text;

                        valid = checkDuplicate(searchValue, searchMode, EquipmentType.Gadget);
                        if (!valid)
                        {
                            dxErrorProvider.SetError(txtBox, "Duplicate IMEI already exists");
                            blnPageValid = !dxErrorProvider.HasErrors;
                        }
                        break;
                    case "txtManufacturerID":
                        if (Convert.ToInt32(ucStep1Page.passedEquipType) > 0)
                        {
                            searchMode = "manufacturer_id";
                            searchValue = txtBox.Text;

                            //valid = checkDuplicate(searchValue, searchMode,(EquipmentType)Convert.ToInt32(lueEquipmentCategory.EditValue));
                            valid = checkDuplicate(searchValue, searchMode, EquipmentType.None);
                            if (!valid)
                            {
                                dxErrorProvider.SetError(txtBox, "Duplicate Manufacturer Identity already exists");
                                blnPageValid = !dxErrorProvider.HasErrors;
                            }
                        }
                        else
                        {
                            valid = false;
                            dxErrorProvider.SetError(txtBox, "Cannot validate this field before a Equipment Category value is selected.");
                            blnPageValid = !dxErrorProvider.HasErrors;
                        }
                        break;
                }
            }
            else
            {
                valid = true;
            }
            if (valid)
            {
                dxErrorProvider.SetError(txtBox, "");
                blnPageValid = !dxErrorProvider.HasErrors;
            }
            return valid;
        }

        private bool validateSpinEdit(SpinEdit spnEdit, EquipmentType equipmentType)
        {
            bool valid = true;

            if (bool_FormLoading)
                return valid;
        
            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(ucStep1Page.passedEquipType))
                {
                    dxErrorProvider.SetError(spnEdit, "");
                    blnPageValid = !dxErrorProvider.HasErrors;
                    return valid;
                }
            }

            if (spnEdit.EditValue == null || spnEdit.Properties.GetDisplayText(spnEdit.EditValue) == "")
            {
                dxErrorProvider.SetError(spnEdit, string.Format("Please enter a valid {0} value", spnEdit.Tag));
                blnPageValid = !dxErrorProvider.HasErrors;
                //spnEdit.Focus();
                return valid = false;  // Show stop icon as field is invalid //
            }
            else
            {
                dxErrorProvider.SetError(spnEdit, "");
                blnPageValid = !dxErrorProvider.HasErrors;
                return valid;
            }

        }

        #endregion


        internal void testData()
        {

            Random random = new Random();

            if (strUserName == "tafadzwa.mandengu" || strUserName == "william.self")
            {
                txtRegistrationPlate.EditValue = string.Format("LM{0}", random.Next(1000));
                txtLogBookNumber.EditValue = random.Next(100000000).ToString();
                lueVehicleCategoryID.EditValue = 68;
            }
        }

        private void setDefaultValues()
        {
            if (bool_FormLoading)
                return;

            var result = (from t in dataSet_AS_DataEntry.sp_AS_11000_PL_Fuel
                        where t.Value == "None"
                        select t.PickListID).Take(1);
  
            foreach (int num in result)
                {
                    intDefaultFuelType = num;
                }

            var result1 = (from t in dataSet_AS_DataEntry.sp_AS_11000_PL_Vehicle_Towbar_Type
                          where t.Value == "None"
                          select t.PickListID).Take(1);

            foreach (int num in result1)
            {
                intDefaultTowbarType = num;
            }
        }
        
        private void EndEdit()
        {
            spAS11005VehicleItemBindingSource.EndEdit();
            spAS11008PlantItemBindingSource.EndEdit();
            //spAS11026GadgetItemBindingSource.EndEdit();
            spAS11029OfficeItemBindingSource.EndEdit();
            //spAS11032HardwareItemBindingSource.EndEdit();
            //spAS11035SoftwareItemBindingSource.EndEdit();
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += string.Format("--> {0}  {1}\n", item.Text, dxErrorProvider.GetError(c));
                }
            }
            if (strErrorMessages != "") strErrorMessages = string.Format("The following errors are present...\n\n{0}\n", strErrorMessages);
            return strErrorMessages;
        }

        private void PopulatePickList()
        {
            sp_AS_11000_PL_FuelTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Fuel, 5); //Fuel
            sp_AS_11000_PL_Plant_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Plant_Category, 12); //Purpose
            sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Vehicle_Towbar_Type, 10); //Tow Bar Type
            sp_AS_11000_PL_Vehicle_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Vehicle_Category, 33); //Vehicle Category
            sp_AS_11000_PL_Office_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Office_Category, 27); //Office Category
        }

        private void postOpen()
        {
            bool_FormLoading = false;
        }

        internal static bool checkValid()
        {
            return blnPageValid;
        }

        private bool checkDuplicate(string searchValue, string searchMode, EquipmentType equipmentType)
        {
            bool dup = true;

            switch (ucStep1Page.strFormMode.ToLower())
            {
                case "add":
                    spAS11000DuplicateSearchTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search, searchValue, Convert.ToInt32(ucStep1Page.passedEquipType), searchMode);

                    switch (spAS11000DuplicateSearchBindingSource.Count)
                    {
                        case 0:
                            dup = true;
                            break;
                        case 1:
                            dup = false;
                            break;
                        default:
                            dup = true;
                            break;
                    }
                    break;
                case "edit":
                    string whereClause = "";

                    switch (searchMode.ToLower())
                    {

                        case "vehicle_plate":
                            whereClause = string.Format("[RegistrationPlate] ='{0}'", searchValue);
                            break;
                        case "imei":
                            whereClause = string.Format("[IMEI] ='{0}'", searchValue);
                            break;
                        case "manufacturer_id":
                            whereClause = string.Format("[ManufacturerID] ='{0}'", searchValue);
                            break;
                        default:
                            break;
                    }
                    switch (equipmentType)
                    {
                        case EquipmentType.None://Equipment
                            //switch (Convert.ToInt32(ucStep1Page.passedEquipType))
                            //{
                            //    case 1://Vehicles
                            //        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Vehicle);
                            //        break;
                            //    case 2://Plant
                            //        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Plant);
                            //        break;
                            //    case 3://Gadget
                            //        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Gadget);
                            //        break;
                            //    case 4://Hardware
                            //        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Hardware);
                            //        break;
                            //    case 5://Software
                            //        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Software);
                            //        break;
                            //    case 6://Office
                            //        dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Office);
                            //        break;
                            //    default:
                            //        break;
                            //}

                            break;
                        case EquipmentType.Vehicle:
                            dup = returnDuplicateStatus(whereClause, this.spAS11005VehicleItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item, EquipmentType.None);
                            break;
                        case EquipmentType.Plant:
                            dup = returnDuplicateStatus(whereClause, this.spAS11008PlantItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item, EquipmentType.None);
                            break;
                        case EquipmentType.Gadget:
                            dup = returnDuplicateStatus(whereClause, this.sp_AS_11026_Gadget_ItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item, EquipmentType.None);
                            break;
                        case EquipmentType.Hardware:
                            dup = returnDuplicateStatus(whereClause, this.sp_AS_11032_Hardware_ItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item, EquipmentType.None);
                            break;
                        case EquipmentType.Software:
                            dup = returnDuplicateStatus(whereClause, this.sp_AS_11035_Software_ItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item, EquipmentType.None);
                            break;
                        case EquipmentType.Office:
                            dup = returnDuplicateStatus(whereClause, this.spAS11029OfficeItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item, EquipmentType.None);
                            break;
                    }
                    break;
                default:

                    break;
            }

            return dup;

        }

        private bool returnDuplicateStatus(string whereClause, BindingSource bs, DataTable dt, EquipmentType equipmentType)
        {
            if (dt.Select(whereClause).GetLength(0) > 1)
            {
                bs.ResetCurrentItem();
                return false;
            }
            else
            {
                return true;
            }
        }

        public void ChildVisibility(int IDs, int typesOfEquipment)
        {
            string strChildRecordIDs = IDs.ToString();
            HideAllChildPages("", "");
            
        switch (typesOfEquipment)
        {
            case 1://vehicles
                vehicleLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                //navigate to record
                if (ucStep1Page.strFormMode.ToLower() != "add")
                {
                    spAS11005VehicleItemBindingSource.Position = spAS11005VehicleItemBindingSource.Find("EquipmentID", strChildRecordIDs);
                }
                if (ucStep1Page.strFormMode.ToLower() == "add")
                {
                    BeginAddNewChild(typesOfEquipment);
                }
                break;
            case 2://plant
                plantLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                //navigate to record
                if (ucStep1Page.strFormMode.ToLower() != "add")
                {
                    spAS11008PlantItemBindingSource.Position = spAS11008PlantItemBindingSource.Find("EquipmentID", strChildRecordIDs);
                }
                if (ucStep1Page.strFormMode.ToLower() == "add")
                {
                    BeginAddNewChild(typesOfEquipment);
                }
                break;
            //case 3://gadget
            //    gadgetLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            //    //navigate to record
            //    if (ucStep1Page.strFormMode.ToLower() != "add")
            //    {
            //        spAS11026GadgetItemBindingSource.Position = spAS11026GadgetItemBindingSource.Find("EquipmentID", strChildRecordIDs);
            //    }
            //    if (ucStep1Page.strFormMode.ToLower() == "add")
            //    {
            //        BeginAddNewChild(typesOfEquipment);
            //    }
            //    break;
            //case 4://hardware
            //    hardwareLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            //    //navigate to record
            //    if (ucStep1Page.strFormMode.ToLower() != "add")
            //    {
            //        spAS11032HardwareItemBindingSource.Position = spAS11032HardwareItemBindingSource.Find("EquipmentID", strChildRecordIDs);
            //    }

            //    if (ucStep1Page.strFormMode.ToLower() == "add")
            //    {
            //        BeginAddNewChild(typesOfEquipment);
            //    }
            //    break;
            //case 5://software
            //    softwareLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            //    //navigate to record
            //    if (ucStep1Page.strFormMode.ToLower() != "add")
            //    {
            //        spAS11035SoftwareItemBindingSource.Position = spAS11035SoftwareItemBindingSource.Find("EquipmentID", strChildRecordIDs);
            //    }

            //    if (ucStep1Page.strFormMode.ToLower() == "add")
            //    {
            //        BeginAddNewChild(typesOfEquipment);
            //    }
            //    break;
            case 6://office
                officeLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                //navigate to record
                if (ucStep1Page.strFormMode.ToLower() != "add")
                {
                    spAS11029OfficeItemBindingSource.Position = spAS11029OfficeItemBindingSource.Find("EquipmentID", strChildRecordIDs);
                }

                if (ucStep1Page.strFormMode.ToLower() == "add")
                {
                    BeginAddNewChild(typesOfEquipment);
                }
                break;
            default:
                if (ucStep1Page.strFormMode.ToLower() == "add")
                {
                    HideAllChildPages("Please Select Equipment Type", "Awaiting Equipment section completion...");
                }
                break;
    }
    
        }

         private void HideAllChildPages(string HeaderText, string BackgroundText)
        {
            if (HeaderText == "")
                HeaderText = "No Details";
            if (BackgroundText == "")
                BackgroundText = "No Details Available";

            NoDetailsLayoutControlGroup.Text = HeaderText;
            NoDetailsLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            vehicleLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            plantLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            //gadgetLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            //hardwareLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            //softwareLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            officeLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }

         private DateTime getCurrentDate()
         {
             DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetDate = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
             // GetDate.ChangeConnectionString(strConnectionString);
             GetDate.sp_AS_11138_Get_Server_Date();
             DateTime d = DateTime.Parse(GetDate.sp_AS_11138_Get_Server_Date().ToString());
             return d;
         }

         private void RejectChildChanges()
         {
             this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.RejectChanges();
             this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item.RejectChanges();
             //this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item.RejectChanges();
             this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item.RejectChanges();
             //this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item.RejectChanges();
             //this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item.RejectChanges();
         }

         private void BeginAddNewChild(int EquipmentType)
         {
             RejectChildChanges();
             DataRow drNewRow;
             switch (EquipmentType)
             {
                 case 1://Vehicles
                     drNewRow = this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.NewRow();
                     drNewRow["Mode"] = "add";
                     drNewRow["EquipmentID"] = "-1";
                     drNewRow["TotalTyreNumber"] = intDefaultTotalTyreNumber;
                     drNewRow["EngineSize"] = intDefaultEngineSize;
                     drNewRow["Emissions"] = intDefaultEmissions;
                     drNewRow["ServiceDueMileage"] = intDefaultServiceDueMileage;
                     drNewRow["CurrentMileage"] = intDefaultCurrentMileage;
                     drNewRow["Colour"] = "White";
                     if (intDefaultFuelType != 0)
                     {
                         drNewRow["FuelTypeID"] = intDefaultFuelType;
                     }
                     if (intDefaultTowbarType != 0)
                     {
                         drNewRow["TowbarTypeID"] = intDefaultTowbarType;
                     }
                     drNewRow["RegistrationDate"] = getCurrentDate();

                     if (spAS11005VehicleItemBindingSource.Find("EquipmentID", "-1") < 0)
                     {
                         this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item.Rows.Add(drNewRow);
                     }
                     break;
                 case 2://Plant
                     drNewRow = this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item.NewRow();
                     drNewRow["Mode"] = "add";
                     drNewRow["EngineSize"] = 0;
                     if (intDefaultFuelType != 0)
                     {
                         drNewRow["FuelTypeID"] = intDefaultFuelType;
                     }
                     drNewRow["EquipmentID"] = "-1";
                     drNewRow["LOLERDate"] = getCurrentDate().AddMonths(6);
                     drNewRow["PUWERDate"] = getCurrentDate().AddMonths(6);
                     if (spAS11008PlantItemBindingSource.Find("EquipmentID", "-1") < 0)
                     {
                         this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item.Rows.Add(drNewRow);
                     }
                     break;
                 //case 3://Gadget
                 //    drNewRow = this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item.NewRow();
                 //    drNewRow["Mode"] = "add";
                 //    drNewRow["EquipmentID"] = "-1";
                 //    if (spAS11026GadgetItemBindingSource.Find("EquipmentID", "-1") < 0)
                 //    {
                 //        this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item.Rows.Add(drNewRow);
                 //    }
                 //    break;
                 //case 4://Hardware
                 //    drNewRow = this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item.NewRow();
                 //    drNewRow["Mode"] = "add";
                 //    drNewRow["EquipmentID"] = "-1";
                 //    if (spAS11032HardwareItemBindingSource.Find("EquipmentID", "-1") < 0)
                 //    {
                 //        this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item.Rows.Add(drNewRow);
                 //    }
                 //    break;
                 //case 5://Software
                 //    drNewRow = this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item.NewRow();
                 //    drNewRow["Mode"] = "add";
                 //    drNewRow["EquipmentID"] = "-1";
                 //    if (spAS11035SoftwareItemBindingSource.Find("EquipmentID", "-1") < 0)
                 //    {
                 //        this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item.Rows.Add(drNewRow);
                 //    }
                 //    break;
                 case 6://Office
                     drNewRow = this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item.NewRow();
                     drNewRow["Mode"] = "add";
                     drNewRow["EquipmentID"] = "-1";
                     drNewRow["Quantity"] = "1";
                     if (spAS11029OfficeItemBindingSource.Find("EquipmentID", "-1") < 0)
                     {
                         this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item.Rows.Add(drNewRow);
                     }
                     break;
             }

             NoDetailsLayoutControlGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
         }

        #endregion

        #region Event


         private void ucStep2Page_Load(object sender, EventArgs e)
         {
             PopulatePickList();
             postOpen();
             setDefaultValues();
         }

         #region EventValidators
        
         private void cpeColour_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
         {
             if (bool_FormLoading)
                 return;

             e.NewValue = Regex.Replace(e.NewValue.ToString().Remove(0, 7), "]", "");
         }

         private void lueVehicleCategoryID_EditValueChanged(object sender, EventArgs e)
         {
             if (bool_FormLoading)
                 return;

            calculateMOTDueDate();
         }

        private void deRegistrationDate_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;

            calculateMOTDueDate();
        }

        private void memDescription_Validating(object sender, CancelEventArgs e)
         {
             if (validateMemEdit((MemoEdit)sender, SentenceCase.AsIs, EquipmentType.Office))
             {
                 e.Cancel = false;// Remove stop icon as field is valid //
             }
             else
             {
                 e.Cancel = true;// Show stop icon as field is invalid //
             }
         }

         private void lookUpEditVehicle_Validating(object sender, CancelEventArgs e)
         {
             if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Vehicle))
             {
                 e.Cancel = false;// Remove stop icon as field is valid //
             }
             else
             {
                 e.Cancel = true;// Show stop icon as field is invalid //
             }
         }

         private void lookUpEditPlant_Validating(object sender, CancelEventArgs e)
         {
             if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Plant))
             {
                 e.Cancel = false;// Remove stop icon as field is valid //
             }
             else
             {
                 e.Cancel = true;// Show stop icon as field is invalid //
             }
         }

         private void lookUpEditOffice_Validating(object sender, CancelEventArgs e)
         {
             if (validateLookupEdit((LookUpEdit)sender, EquipmentType.Office))
             {
                 e.Cancel = false;// Remove stop icon as field is valid //
             }
             else
             {
                 e.Cancel = true;// Show stop icon as field is invalid //
             }
         }
        
         private void dateEdit_Validating(object sender, CancelEventArgs e)
         {
             if (validateDateEdit((DateEdit)sender))
             {
                 e.Cancel = false;// Remove stop icon as field is valid //
             }
             else
             {
                 e.Cancel = true;// Show stop icon as field is invalid //
             }
         }

         private void spinEditVehicle_Validating(object sender, CancelEventArgs e)
         {
             if (validateSpinEdit((SpinEdit)sender, EquipmentType.Vehicle))
             {
                 e.Cancel = false;// Remove stop icon as field is valid //
             }
             else
             {
                 e.Cancel = true;// Show stop icon as field is invalid //
             }
         }

         private void spinEditPlant_Validating(object sender, CancelEventArgs e)
         {
             if (validateSpinEdit((SpinEdit)sender, EquipmentType.Plant))
             {
                 e.Cancel = false;// Remove stop icon as field is valid //
             }
             else
             {
                 e.Cancel = true;// Show stop icon as field is invalid //
             }
         }

         private void spinEditOffice_Validating(object sender, CancelEventArgs e)
         {
             if (validateSpinEdit((SpinEdit)sender, EquipmentType.Office))
             {
                 e.Cancel = false;// Remove stop icon as field is valid //
             }
             else
             {
                 e.Cancel = true;// Show stop icon as field is invalid //
             }
         }

         private void textEditVehicle_Validating(object sender, CancelEventArgs e)
         {
             if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.Vehicle))
             {
                 e.Cancel = false;
             }
             else
             {
                 e.Cancel = true;
             }
         }

         private void textEditPlant_Validating(object sender, CancelEventArgs e)
         {
             if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.Plant))
             {
                 e.Cancel = false;
             }
             else
             {
                 e.Cancel = true;
             }

         }

         private void txtPlantRegistrationPlate_Validating(object sender, CancelEventArgs e)
         {
             if (validateTextBox((TextEdit)sender, SentenceCase.Upper, EquipmentType.Plant))
             {
                 e.Cancel = false;
             }
             else
             {
                 e.Cancel = true;
             }
         }
        #endregion

        private void calculateMOTDueDate()
        {
            if (lueVehicleCategoryID.Properties.GetDisplayText(lueVehicleCategoryID.EditValue) == "Car")
            {
                //deMOTDueDate.EditValue = getCurrentDate().AddYears(3);
                deMOTDueDate.EditValue = deRegistrationDate.DateTime.AddYears(3);
            }
            else if (lueVehicleCategoryID.Properties.GetDisplayText(lueVehicleCategoryID.EditValue) == "Commercial")
            {
                //deMOTDueDate.EditValue = getCurrentDate().AddYears(1);
                deMOTDueDate.EditValue = deRegistrationDate.DateTime.AddYears(1);
            }
        }


         private void ucStep2Page_Enter(object sender, EventArgs e)
         {
             switch ((int)ucStep1Page.passedEquipType)
             {
                 case 1:
                     lblPageTitle.Text = "Adding Vehicle:";
                     break;
                 case 2:
                     lblPageTitle.Text = "Adding Plant and Machinery:";
                     break;
                 case 3:
                     lblPageTitle.Text = "Adding Gadget:";
                     break;
                 case 4:
                     lblPageTitle.Text = "Adding Hardware:";
                     break;
                 case 5:
                     lblPageTitle.Text = "Adding Software:";
                     break;
                 case 6:
                     lblPageTitle.Text = "Adding Office Equipment:";
                     break;
             }
             if (intAddNewRecordCounter == 0)
             {
                 ChildVisibility(0, (ucStep1Page.passedEquipType == EquipmentType.None ? 1 : (int)ucStep1Page.passedEquipType));
                 intAddNewRecordCounter++;
             }

             if (intCount == 0)
             {
                 testData();
                 intCount++;
             }
             ValidateChildren();
            
         }

         private void ucStep2Page_Validated(object sender, EventArgs e)
         {
             if (blnPageValid)
             {
                 EndEdit();
                 switch (ucStep1Page.passedEquipType)
                 {
                     case EquipmentType.Vehicle:
                         ucExecutePage.drEquipmentChildNewRowView = (DataRowView)spAS11005VehicleItemBindingSource.Current;                         
                         break;
                     case EquipmentType.Plant:
                         ucExecutePage.drEquipmentChildNewRowView = (DataRowView)spAS11008PlantItemBindingSource.Current;
                         break;
                     case EquipmentType.Office:
                         ucExecutePage.drEquipmentChildNewRowView = (DataRowView)spAS11029OfficeItemBindingSource.Current;
                         ucExecutePage.strOfficeCategory = lueOfficeCategoryID.Properties.GetDisplayText(lueOfficeCategoryID.EditValue);
                         break;
                 }
             }
         }

         private void ucStep2Page_Validating(object sender, CancelEventArgs e)
         {
             ValidateChildren();
         }


        #endregion

    }
}
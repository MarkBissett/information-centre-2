﻿using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using System;
using System.Data;
using DevExpress.XtraEditors;
using System.Globalization;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace WoodPlan5.Forms.Assets.ImportWizard
{

    public enum EquipmentType { None, Vehicle = 1, Plant = 2, Gadget = 3, Hardware = 4, Software = 5, Office = 6 }

    public partial class ucStep1Page : Views.BaseWizardPage
    {
        public ucStep1Page()
        {
            InitializeComponent();
        }

        #region Declaration

        public static bool blnStep1Valid = false;
        public string strConnectionString = "";
        public static bool blnPageValid = false;
        public enum SentenceCase { Upper, Lower, Title, AsIs }
        public static EquipmentType passedEquipType;
        public int intExchequerCategoryMVG = 0;
        bool bool_FormLoading = true;
        public static string strFormMode = "add";
        public string strRecordIDs = "";
        public static string strUserName = ""; 
        int intCount = 0;
        #endregion
        
        #region Events

        #region EventValidators
        
        private void lueDepreciationSetting_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
            {
                return;
            }
            if (lueDepreciationSetting.EditValue != null && lueDepreciationSetting.EditValue.ToString() != "")
            {
                //change linked values
                DataRow[] rows = this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Select("DepreciationSettingID  = " + lueDepreciationSetting.EditValue.ToString());

                if (rows.Length > 0)
                //if (this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Count > 0)
                {
                    //DataRow row = this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Rows[0];
                    DataRow row = rows[0];
                    if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).MaximumOccurrence).ToString() != "")
                    {
                        spnMaximumOccurrence.EditValue = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).MaximumOccurrence;
                    }
                    else
                    {
                        spnMaximumOccurrence.EditValue = 48;
                    }
                    if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).DayOfMonthApplyDepreciation).ToString() != "")
                    {
                        spnDayOfMonthApplyDepreciation.EditValue = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).DayOfMonthApplyDepreciation;
                    }
                    else
                    {
                        spnDayOfMonthApplyDepreciation.EditValue = 1;
                    }

                    if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).OccursUnitDescriptorID).ToString() != "")
                    {
                        lueOccursUnitDescriptor.EditValue = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).OccursUnitDescriptorID;
                    }
                    else
                    {
                        lueOccursUnitDescriptor.EditValue = 64;
                    }

                }
                else
                {
                    spnMaximumOccurrence.EditValue = 48;
                    spnDayOfMonthApplyDepreciation.EditValue = 1;
                    lueOccursUnitDescriptor.EditValue = 64;
                }

            }


        }
        
        private void lookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender, EquipmentType.None))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }


        private void textEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper, passedEquipType))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }

        }

        #endregion

        private void ucStep1Page_Load(object sender, System.EventArgs e)
        {
            addNewEquipmentRow();

            //strConnectionString = frm_Import_Wizard.
            spnMaximumOccurrence.Properties.ReadOnly = true;
            spnDayOfMonthApplyDepreciation.Properties.ReadOnly = true;
            lueOccursUnitDescriptor.Properties.ReadOnly = true;
            PopulateEquipmentPickList();
            lueEquipmentCategory.EditValue = (int)EquipmentType.Vehicle; ;//default add vehicle
            postOpen();
        }
        
        private void lueEquipmentCategory_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading && strFormMode.ToLower() != "add")
                return;

            if (lueEquipmentCategory.EditValue != null)
            {
                if (lueEquipmentCategory.EditValue.ToString() != "")
                {
                    //RefreshChildBindingSource();
                    //RejectChildChanges();
                    ClearErrors(this.Controls);
                    RefreshMake();

                    manufacturerIDDescription(lueEquipmentCategory.EditValue.ToString());

                    this.lueMake.EditValue = null;
                   
                }
            }
            ucStep2Page.intAddNewRecordCounter = 0;
            ValidateChildren();
        }

        private void lueMake_EditValueChanged(object sender, EventArgs e)
        {
            if (lueMake.EditValue != null)
            {
                if (lueMake.EditValue.ToString() != "")
                {
                    RefreshModel();
                    try
                    {
                        switch (strFormMode.ToLower())
                        {
                            case "add":

                                // ChildVisibility(0, (int)passedEquipType);

                                //CategoryEditValueChangedCount+=1;
                                this.lueModel.EditValue = null;
                                break;

                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                }
            }
            else
            {
                this.lueModel.EditValue = null;
            }

            ValidateChildren();
        }
        
        private void lueSupplier_EditValueChanged(object sender, EventArgs e)
        {
            RefreshSupplier();
        }

        private void ucStep1Page_Validated(object sender, EventArgs e)
        {
            if (blnPageValid)
            {
                spAS11002EquipmentItemBindingSource.EndEdit();
                ucExecutePage.drEquipmentNewRowView = (DataRowView)spAS11002EquipmentItemBindingSource.Current;
            }
        }

        private void ucStep1Page_Enter(object sender, EventArgs e)
        {

            if (intCount == 0)
            {
                testData();
                intCount++;
            }
            ValidateChildren();
        }

        #endregion

        #region Methods

        #region ValidateMethod

        private bool validateDateEdit(DateEdit dateEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
            string ErrorText = "Please Enter a Valid Date.";
            if (dateEdit.Tag != null)
            {
                ErrorText = "Please Enter a valid " + dateEdit.Tag.ToString() + ".";
            }
            //Check if date is after 


            return valid;
        }

        private bool validateLookupEdit(LookUpEdit lookupEdit, EquipmentType equipmentType)
        {

            bool valid = true;
            if (bool_FormLoading)
                return valid;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(lueEquipmentCategory.EditValue))
                {
                    dxErrorProviderEquipment.SetError(lookupEdit, "");
                    return valid;
                }
            }

            string ErrorText = "Please Select Value.";

            if (lookupEdit.Tag == null ? false : true)
            {
                ErrorText = "Please Select " + lookupEdit.Tag + ".";
            }
            if ((lookupEdit.EditValue == null) || ucStep1Page.strFormMode.ToLower() != "blockedit" && string.IsNullOrEmpty(lookupEdit.EditValue.ToString()))
            {
                dxErrorProviderEquipment.SetError(lookupEdit, ErrorText);
                blnPageValid = !dxErrorProviderEquipment.HasErrors;
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }
            else
            {
                dxErrorProviderEquipment.SetError(lookupEdit, "");
                blnPageValid = !dxErrorProviderEquipment.HasErrors;
                return valid;
            }
        }

        private bool validateMemEdit(MemoEdit txtBox, SentenceCase SentenceCase, EquipmentType equipmentType)
        {
            if (bool_FormLoading)
                return true;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(lueEquipmentCategory.EditValue))
                {
                    dxErrorProviderEquipment.SetError(txtBox, "");
                    blnPageValid = !dxErrorProviderEquipment.HasErrors;
                    return true;
                }
            }
            bool valid = false;
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.Text = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.Text = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            string originalText = txtBox.Text;
            //check empty field
            if (originalText == "")
            {
                dxErrorProviderEquipment.SetError(txtBox, "Please enter a valid text.");
                blnPageValid = !dxErrorProviderEquipment.HasErrors;
                return valid;
            }
            else
            {
                valid = true;
            }

            if (valid)
            {
                dxErrorProviderEquipment.SetError(txtBox, "");
                blnPageValid = !dxErrorProviderEquipment.HasErrors;
            }
            return valid;
        }

        private bool validateTextBox(TextEdit txtBox, SentenceCase SentenceCase, EquipmentType equipmentType)
        {
            if (bool_FormLoading)
                return true;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(lueEquipmentCategory.EditValue))
                {
                    dxErrorProviderEquipment.SetError(txtBox, "");
                    blnPageValid = !dxErrorProviderEquipment.HasErrors;
                    return true;
                }
            }
            bool valid = false;
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.Text = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.Text = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            string originalText = txtBox.Text;
            string parsedText = "";
            //if (lueEquipmentCategory.EditValue.ToString() == "6")
            //{
            //    parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9 ]", "");
            //    txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
            //}
            //else
            //{
            //    parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9]", "");
            //}

            switch (txtBox.Name)
            {
                case "txtServiceTag":
                case "txtShortDescription":
                    parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9 ]", "");
                    break;
                default:
                    parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9]", "");
                    break;
            }

            //check non alphanumeric characters
            if (originalText != parsedText || txtBox.Text == "")
            {
                dxErrorProviderEquipment.SetError(txtBox, "Please enter a valid text, avoid entering non alphanumeric characters, invalid characters have been removed");
                blnPageValid = !dxErrorProviderEquipment.HasErrors;
                //txtBox.Focus();
                return valid;
            }

            //check registration plate length
            if (txtBox.Name == "txtRegistration_Plate" && parsedText.Length > 7)
            {
                dxErrorProviderEquipment.SetError(txtBox, "Supplied registration exceeds 7 characters");
                blnPageValid = !dxErrorProviderEquipment.HasErrors;
                //  txtBox.Focus();
                return valid;
            }


            //check duplication last
            if (txtBox.Name == "txtRegistration_Plate" || txtBox.Name == "txtManufacturerID" || txtBox.Name == "txtIMEI")
            {
                string searchMode, searchValue;

                switch (txtBox.Name)
                {
                    case "txtRegistration_Plate":
                        searchMode = "vehicle_plate";
                        searchValue = txtBox.Text;

                        valid = checkDuplicate(searchValue, searchMode, EquipmentType.Vehicle);
                        if (!valid)
                        {
                            dxErrorProviderEquipment.SetError(txtBox, "Duplicate Registration Plate already exists");
                            blnPageValid = !dxErrorProviderEquipment.HasErrors;
                        }
                        break;
                    case "txtIMEI":
                        searchMode = "imei";
                        searchValue = txtBox.Text;

                        valid = checkDuplicate(searchValue, searchMode, EquipmentType.Gadget);
                        if (!valid)
                        {
                            dxErrorProviderEquipment.SetError(txtBox, "Duplicate IMEI already exists");
                            blnPageValid = !dxErrorProviderEquipment.HasErrors;
                        }
                        break;
                    case "txtManufacturerID":
                        if (lueEquipmentCategory.EditValue != null)
                        {
                            searchMode = "manufacturer_id";
                            searchValue = txtBox.Text;

                            //valid = checkDuplicate(searchValue, searchMode,(EquipmentType)Convert.ToInt32(lueEquipmentCategory.EditValue));
                            valid = checkDuplicate(searchValue, searchMode, EquipmentType.None);
                            if (!valid)
                            {
                                dxErrorProviderEquipment.SetError(txtBox, "Duplicate Manufacturer Identity already exists");
                                blnPageValid = !dxErrorProviderEquipment.HasErrors;
                            }
                        }
                        else
                        {
                            valid = false;
                            dxErrorProviderEquipment.SetError(txtBox, "Cannot validate this field before a Equipment Category value is selected.");
                            blnPageValid = !dxErrorProviderEquipment.HasErrors;
                        }
                        break;
                }
            }
            else
            {
                valid = true;
            }
            if (valid)
            {
                dxErrorProviderEquipment.SetError(txtBox, "");
                blnPageValid = !dxErrorProviderEquipment.HasErrors;
            }
            return valid;
        }

        private bool validateSpinEdit(SpinEdit spnEdit, EquipmentType equipmentType)
        {
            bool valid = true;

            if (bool_FormLoading)
                return valid;

            if (equipmentType != EquipmentType.None)
            {
                if ((int)equipmentType != Convert.ToInt32(lueEquipmentCategory.EditValue))
                {
                    dxErrorProviderEquipment.SetError(spnEdit, "");
                    blnPageValid = !dxErrorProviderEquipment.HasErrors;
                    return valid;
                }
            }


            if (spnEdit.EditValue == null || spnEdit.EditValue.ToString() == "")
            {
                dxErrorProviderEquipment.SetError(spnEdit, string.Format("Please enter a valid {0} value", spnEdit.Tag));
                blnPageValid = !dxErrorProviderEquipment.HasErrors;
                //spnEdit.Focus();
                return valid = false;  // Show stop icon as field is invalid //
            }
            else
            {
                dxErrorProviderEquipment.SetError(spnEdit, "");
                blnPageValid = !dxErrorProviderEquipment.HasErrors;
                return valid;
            }

        }

        #endregion

        internal void manufacturerIDDescription(string manufacturerIDEquipType)
        {
            switch (manufacturerIDEquipType)
            {
                case "1":
                    ifManufacturerID.Text = "VIN Number ";
                    passedEquipType = EquipmentType.Vehicle;
                    break;
                case "2":
                    this.ifManufacturerID.Text = "Manufacturer ID ";
                    passedEquipType = EquipmentType.Plant;
                    break;
                //case "3":
                //case "4":
                //case "5":
                //    this.ifManufacturerID.Text = "ICT Tag ";
                //    break;
                case "6":
                    this.ifManufacturerID.Text = "Unique Item Description ";
                    passedEquipType = EquipmentType.Office;
                    break;
                default:
                    this.ifManufacturerID.Text = "Manufacturer ID ";
                    break;
            }
        }
                
        internal void RefreshModel()
        {
            sp_AS_11017_Model_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11017_Model_List, "", "view"); //Model
            if (lueMake.EditValue == null ? false : true)
            {
                this.spAS11017ModelListBindingSource.Filter = "Make = " + lueMake.EditValue;

                if (this.spAS11017ModelListBindingSource.Count == 0)
                {
                    lueModel.Properties.NullValuePrompt = "--No Models Available For Selected Make, Please Add--";
                    lueModel.Properties.AllowFocused = false;
                }
                else
                {
                    lueModel.Properties.NullValuePrompt = "--Please Select a Model--";
                    lueModel.Properties.AllowFocused = true;
                }
            }
            else
            {
                this.spAS11017ModelListBindingSource.Filter = "Make = 0";
                lueModel.Properties.NullValuePrompt = "--No Models Available, Please Select Make Before Proceeding--";
            }


        }

        internal void RefreshMake()
        {
            sp_AS_11014_Make_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11014_Make_List, "", "view"); //Make
            if (lueEquipmentCategory.EditValue == null ? false : true)
            {
                this.spAS11014MakeListBindingSource.Filter = "EquipmentCategory = " + lueEquipmentCategory.EditValue;

                if (this.spAS11014MakeListBindingSource.Count == 0)
                {
                    lueMake.Properties.NullValuePrompt = "--No Makes Available For Selected Category, Please Add--";
                    lueMake.Properties.AllowFocused = false;
                }
                else
                {
                    lueMake.Properties.NullValuePrompt = "--Please Select a Make--";
                    lueMake.Properties.AllowFocused = true;
                }
            }
            else
            {
                this.spAS11014MakeListBindingSource.Filter = "EquipmentCategory = 0";
                lueMake.Properties.NullValuePrompt = "--No Makes Available, Please Select Category Before Proceeding--";
            }

        }

        private void RefreshSupplier()
        {
            sp_AS_11020_Supplier_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11020_Supplier_List, "", "view"); //Supplier
            if (this.spAS11020SupplierListBindingSource.Count == 0)
            {
                lueSupplier.Properties.NullValuePrompt = "--No Suppliers Available, Please Add Suppliers--";
                lueSupplier.Properties.AllowFocused = false;
            }
            else
            {
                lueSupplier.Properties.NullValuePrompt = "--Please Select a Supplier--";
                lueSupplier.Properties.AllowFocused = true;
                //Filter suppliers based on caller
                string strSupplierFilter = "";
                switch (passedEquipType)
                {
                    case EquipmentType.Vehicle:
                        strSupplierFilter = "[VehicleSupplier] = True";
                        break;
                    case EquipmentType.Plant:
                        strSupplierFilter = "[PlantSupplier] = True";
                        break;
                    case EquipmentType.Office:
                        strSupplierFilter = "[OfficeSupplier] = True";
                        break;
                    default:
                        strSupplierFilter = "";
                        break;
                }
                spAS11020SupplierListBindingSource.Filter = strSupplierFilter;
            }
        }

        private void ClearErrors(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is LookUpEdit)
                    dxErrorProviderEquipment.SetError(item2, "");
                if (item2 is TextEdit)
                    dxErrorProviderEquipment.SetError(item2, "");
                if (item2 is DateEdit)
                    dxErrorProviderEquipment.SetError(item2, "");
                if (item2 is ColorPickEdit)
                    dxErrorProviderEquipment.SetError(item2, "");
                if (item2 is SpinEdit)
                    dxErrorProviderEquipment.SetError(item2, "");
                if (item2 is ContainerControl)
                    this.ClearErrors(item.Controls);
                if (item2 is DevExpress.XtraDataLayout.DataLayoutControl)
                    this.ClearErrors(item.Controls);
            }
        }

        private void postOpen()
        {
            bool_FormLoading = false;
        }

        private void PopulateEquipmentPickList()
        {
            sp_AS_11011_Equipment_Category_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11011_Equipment_Category_List, "", "view"); //Equipment Category

            sp_AS_11000_PL_Exchequer_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_Category, 54); //Exchequer Category

            sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Equipment_GC_Marked, 3); //GC Marked
            sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Equipment_Ownership_Status, 1); //Ownership Status
            sp_AS_11000_PL_Equipment_AvailabilityTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Equipment_Availability, 2); //Availability

            sp_AS_11014_Make_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11014_Make_List, "", "view"); //Make

            sp_AS_11017_Model_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11017_Model_List, "", "view"); //Model

            sp_AS_11020_Supplier_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11020_Supplier_List, "", "view"); //Supplier

            sp_AS_11000_PL_Unit_DescriptorTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Unit_Descriptor, 25); //Unit descriptor

            sp_AS_11066_Depreciation_Settings_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item, "", ""); //Depreciation setiing

            //Filter types based on caller
            const string strCategoryFilter = "[EquipmentCategory] = '1' OR [EquipmentCategory] = '2' OR [EquipmentCategory] = '3' OR [EquipmentCategory] = '6'";           
            spAS11011EquipmentCategoryListBindingSource.Filter = strCategoryFilter;
            //Filter suppliers based on caller
            const string strSupplierFilter = "[VehicleSupplier] = True OR [PlantSupplier] = True OR [GadgetSupplier] = True OR [OfficeSupplier] = True";
            spAS11020SupplierListBindingSource.Filter = strSupplierFilter;

        }
        

        internal void testData()
        {
            int intExchequerCategory;

            if (intExchequerCategoryMVG != 0)
                intExchequerCategory = intExchequerCategoryMVG;
            else
                intExchequerCategory = 0;

            Random random = new Random();
            
            if (strUserName == "tafadzwa.mandengu" || strUserName == "william.self")
            {
                lueSupplier.EditValue = 1;
                lueGCMarked.EditValue = 8;
                lueAvailability.EditValue = 6;
                lueOwnershipStatus.EditValue = 1;
                txtManufacturerID.EditValue = string.Format("Test{0}", random.Next(154448));
                lueMake.EditValue = 1;
                lueModel.EditValue = 6;
                txtExchequerID.EditValue = "266";
                lueExchequerCategoryID.EditValue = intExchequerCategory;
            }
        }

        private void addNewEquipmentRow()
        {
            try
            {
                int intExchequerCategory = 0;

                if (intExchequerCategoryMVG != 0)
                {
                    intExchequerCategory = intExchequerCategoryMVG;
                }
                else
                {
                    sp_AS_11000_PL_Exchequer_CategoryTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_Category, 54); //Exchequer Category
                    DataRow[] drExCats = this.dataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_Category.Select("[value] like 'none'");
                    if (drExCats.Length > 0)
                    {
                        foreach (DataRow drExCat in drExCats)
                        {
                            intExchequerCategory = ((DataSet_AS_DataEntry.sp_AS_11000_PL_Exchequer_CategoryRow)(drExCat)).PickListID;
                        }
                    }
                }

                DataRow drNewRow;
                drNewRow = this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item.NewRow();
                drNewRow["Mode"] = "add";
                drNewRow["RecordID"] = "-1";
                if ((int)passedEquipType == 0)
                {
                    drNewRow["EquipmentCategory"] = "1";
                }
                else
                {
                    drNewRow["EquipmentCategory"] = (int)passedEquipType;
                }
                drNewRow["EquipmentReference"] = "";
                drNewRow["ManufacturerID"] = DBNull.Value;
                drNewRow["Make"] = DBNull.Value;
                drNewRow["Model"] = DBNull.Value;
                drNewRow["ExchequerID"] = "";
                drNewRow["ExchequerCategoryID"] = intExchequerCategory;
                drNewRow["Archive"] = false;
                sp_AS_11066_Depreciation_Settings_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item, "", "");
                string depFilter = "";
                switch ((int)passedEquipType)
                {
                    case 1:
                    case 2:
                    case 4:
                    case 6:
                        depFilter = "SettingName = 'Default Setting'";
                        break;
                    case 3:
                        depFilter = "SettingName = 'Gadget Setting'";
                        break;
                    case 5:
                        depFilter = "SettingName = 'No Depreciation Required'";
                        break;
                    default:
                        depFilter = "SettingName = 'Default Setting'";
                        break;

                }

                DataRow[] rows = dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Select(depFilter);
                if (rows.Length > 0)
                //if (this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Count > 0)
                {
                    //DataRow row = this.dataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_Item.Rows[0];
                    DataRow row = rows[0];
                    if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).MaximumOccurrence).ToString() != "")
                    {
                        drNewRow["MaximumOccurrence"] = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).MaximumOccurrence;
                    }
                    else
                    {
                        drNewRow["MaximumOccurrence"] = 48;
                    }
                    if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).DayOfMonthApplyDepreciation).ToString() != "")
                    {
                        drNewRow["DayOfMonthApplyDepreciation"] = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).DayOfMonthApplyDepreciation;
                    }
                    else
                    {
                        drNewRow["DayOfMonthApplyDepreciation"] = 1;
                    }

                    if ((((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).OccursUnitDescriptorID).ToString() != "")
                    {
                        drNewRow["OccursUnitDescriptor"] = ((DataSet_AS_DataEntry.sp_AS_11066_Depreciation_Settings_ItemRow)(row)).OccursUnitDescriptorID;
                    }
                    else
                    {
                        drNewRow["OccursUnitDescriptor"] = 64;
                    }

                }
                else
                {
                    drNewRow["MaximumOccurrence"] = 48;
                    drNewRow["DayOfMonthApplyDepreciation"] = 1;
                    drNewRow["OccursUnitDescriptor"] = 64;
                }
                //test data

                this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item.Rows.Add(drNewRow);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private bool checkDuplicate(string searchValue, string searchMode, EquipmentType equipmentType)
        {
            bool dup = true;

            switch (strFormMode.ToLower())
            {
                case "add":
                    sp_AS_11000_Duplicate_SearchTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11000_Duplicate_Search, searchValue, Convert.ToInt32(lueEquipmentCategory.EditValue), searchMode);

                    switch (spAS11000DuplicateSearchBindingSource.Count)
                    {
                        case 0:
                            dup = true;
                            break;
                        case 1:
                            dup = false;
                            break;
                        default:
                            dup = true;
                            break;
                    }
                    break;
                case "edit":
                    string whereClause = "";

                    switch (searchMode.ToLower())
                    {

                        case "vehicle_plate":
                            whereClause = string.Format("[RegistrationPlate] ='{0}'", searchValue);
                            break;
                        case "imei":
                            whereClause = string.Format("[IMEI] ='{0}'", searchValue);
                            break;
                        case "manufacturer_id":
                            whereClause = string.Format("[ManufacturerID] ='{0}'", searchValue);
                            break;
                        default:
                            break;
                    }
                    switch (equipmentType)
                    {
                        case EquipmentType.None://Equipment
                            switch (Convert.ToInt32(lueEquipmentCategory.EditValue))
                            {
                                case 1://Vehicles
                                    dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Vehicle);
                                    break;
                                case 2://Plant
                                    dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Plant);
                                    break;
                                case 3://Gadget
                                    dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Gadget);
                                    break;
                                case 4://Hardware
                                    dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Hardware);
                                    break;
                                case 5://Software
                                    dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Software);
                                    break;
                                case 6://Office
                                    dup = returnDuplicateStatus(whereClause, this.spAS11002EquipmentItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11002_Equipment_Item, EquipmentType.Office);
                                    break;
                                default:
                                    break;
                            }

                            break;
                        case EquipmentType.Vehicle:
                            dup = returnDuplicateStatus(whereClause, this.sp_AS_11005_Vehicle_ItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11005_Vehicle_Item, EquipmentType.None);
                            break;
                        case EquipmentType.Plant:
                            dup = returnDuplicateStatus(whereClause, this.sp_AS_11008_Plant_ItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11008_Plant_Item, EquipmentType.None);
                            break;
                        case EquipmentType.Gadget:
                            dup = returnDuplicateStatus(whereClause, this.sp_AS_11026_Gadget_ItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11026_Gadget_Item, EquipmentType.None);
                            break;
                        case EquipmentType.Hardware:
                            dup = returnDuplicateStatus(whereClause, this.sp_AS_11032_Hardware_ItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11032_Hardware_Item, EquipmentType.None);
                            break;
                        case EquipmentType.Software:
                            dup = returnDuplicateStatus(whereClause, this.sp_AS_11035_Software_ItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11035_Software_Item, EquipmentType.None);
                            break;
                        case EquipmentType.Office:
                            dup = returnDuplicateStatus(whereClause, this.sp_AS_11029_Office_ItemBindingSource, this.dataSet_AS_DataEntry.sp_AS_11029_Office_Item, EquipmentType.None);
                            break;
                    }
                    break;
                default:

                    break;
            }
            
            return dup;

        }

        private bool returnDuplicateStatus(string whereClause, BindingSource bs, DataTable dt, EquipmentType equipmentType)
        {

            if (dt.Select(whereClause).GetLength(0) > 1)
            {
                bs.ResetCurrentItem();
                return false;
            }
            else
            {
                return true;
            }
        }

        internal static bool checkValid()
        {
            return blnPageValid;
        }

        private void ucStep1Page_Validating(object sender, CancelEventArgs e)
        {
            Validate();
            ValidateChildren();
        }

        #region Validation Methods
        
        #endregion

        #endregion


    }
}
﻿namespace WoodPlan5.Forms.Assets.ImportWizard
{
    partial class ucStep1Page
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucStep1Page));
            this.sp_AS_11066_Depreciation_Settings_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.spAS11002EquipmentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11011EquipmentCategoryListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11014MakeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11017ModelListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11020SupplierListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLEquipmentGCMarkedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLEquipmentOwnershipStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLEquipmentAvailabilityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLUnitDescriptorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLExchequerCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11002_Equipment_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter();
            this.sp_AS_11011_Equipment_Category_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11011_Equipment_Category_ListTableAdapter();
            this.sp_AS_11014_Make_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter();
            this.sp_AS_11017_Model_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter();
            this.sp_AS_11020_Supplier_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter();
            this.sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter();
            this.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter();
            this.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter();
            this.sp_AS_11000_PL_Unit_DescriptorTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_DescriptorTableAdapter();
            this.sp_AS_11000_PL_Exchequer_CategoryTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Exchequer_CategoryTableAdapter();
            this.spAS11000DuplicateSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_Duplicate_SearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter();
            this.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11066_Depreciation_Settings_ItemTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.sp_AS_11005_Vehicle_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11005_Vehicle_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter();
            this.sp_AS_11008_Plant_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11008_Plant_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11008_Plant_ItemTableAdapter();
            this.sp_AS_11029_Office_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11029_Office_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11029_Office_ItemTableAdapter();
            this.sp_AS_11026_Gadget_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11026_Gadget_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11026_Gadget_ItemTableAdapter();
            this.sp_AS_11032_Hardware_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11032_Hardware_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11032_Hardware_ItemTableAdapter();
            this.sp_AS_11035_Software_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11035_Software_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11035_Software_ItemTableAdapter();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.lueDepreciationSetting = new DevExpress.XtraEditors.LookUpEdit();
            this.lueEquipmentCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.txtManufacturerID = new DevExpress.XtraEditors.TextEdit();
            this.lueMake = new DevExpress.XtraEditors.LookUpEdit();
            this.lueModel = new DevExpress.XtraEditors.LookUpEdit();
            this.lueSupplier = new DevExpress.XtraEditors.LookUpEdit();
            this.lueGCMarked = new DevExpress.XtraEditors.LookUpEdit();
            this.lueOwnershipStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.lueAvailability = new DevExpress.XtraEditors.LookUpEdit();
            this.deDeliveryDate = new DevExpress.XtraEditors.DateEdit();
            this.deDepreciationStartDate = new DevExpress.XtraEditors.DateEdit();
            this.txtExchequerID = new DevExpress.XtraEditors.TextEdit();
            this.txtSageReference = new DevExpress.XtraEditors.TextEdit();
            this.lueOccursUnitDescriptor = new DevExpress.XtraEditors.LookUpEdit();
            this.spnMaximumOccurrence = new DevExpress.XtraEditors.SpinEdit();
            this.spnDayOfMonthApplyDepreciation = new DevExpress.XtraEditors.SpinEdit();
            this.lueExchequerCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.memNotes = new DevExpress.XtraEditors.MemoEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ifManufacturerID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMake = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSupplier = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForMaximumOccurrence = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOccursUnitDescriptor = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDayOfMonthApplyDepreciation = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForExchequerCategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDepreciationStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExchequerID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSageReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForOwnershipStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGCMarked = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAvailability = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeliveryDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEquipmentCategory = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxErrorProviderEquipment = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11066_Depreciation_Settings_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11002EquipmentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11011EquipmentCategoryListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11014MakeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentGCMarkedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentOwnershipStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentAvailabilityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitDescriptorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExchequerCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11005_Vehicle_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11008_Plant_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11029_Office_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11026_Gadget_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11032_Hardware_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11035_Software_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepreciationSetting.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEquipmentCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManufacturerID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMake.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueGCMarked.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOwnershipStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAvailability.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDeliveryDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDeliveryDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDepreciationStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDepreciationStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchequerID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSageReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOccursUnitDescriptor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnMaximumOccurrence.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnDayOfMonthApplyDepreciation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueExchequerCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ifManufacturerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaximumOccurrence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOccursUnitDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDayOfMonthApplyDepreciation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExchequerCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepreciationStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExchequerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSageReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnershipStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCMarked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAvailability)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeliveryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProviderEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            this.SuspendLayout();
            // 
            // sp_AS_11066_Depreciation_Settings_ItemBindingSource
            // 
            this.sp_AS_11066_Depreciation_Settings_ItemBindingSource.DataMember = "sp_AS_11066_Depreciation_Settings_Item";
            this.sp_AS_11066_Depreciation_Settings_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spAS11002EquipmentItemBindingSource
            // 
            this.spAS11002EquipmentItemBindingSource.DataMember = "sp_AS_11002_Equipment_Item";
            this.spAS11002EquipmentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11011EquipmentCategoryListBindingSource
            // 
            this.spAS11011EquipmentCategoryListBindingSource.DataMember = "sp_AS_11011_Equipment_Category_List";
            this.spAS11011EquipmentCategoryListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11014MakeListBindingSource
            // 
            this.spAS11014MakeListBindingSource.DataMember = "sp_AS_11014_Make_List";
            this.spAS11014MakeListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11017ModelListBindingSource
            // 
            this.spAS11017ModelListBindingSource.DataMember = "sp_AS_11017_Model_List";
            this.spAS11017ModelListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11020SupplierListBindingSource
            // 
            this.spAS11020SupplierListBindingSource.DataMember = "sp_AS_11020_Supplier_List";
            this.spAS11020SupplierListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLEquipmentGCMarkedBindingSource
            // 
            this.spAS11000PLEquipmentGCMarkedBindingSource.DataMember = "sp_AS_11000_PL_Equipment_GC_Marked";
            this.spAS11000PLEquipmentGCMarkedBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLEquipmentOwnershipStatusBindingSource
            // 
            this.spAS11000PLEquipmentOwnershipStatusBindingSource.DataMember = "sp_AS_11000_PL_Equipment_Ownership_Status";
            this.spAS11000PLEquipmentOwnershipStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLEquipmentAvailabilityBindingSource
            // 
            this.spAS11000PLEquipmentAvailabilityBindingSource.DataMember = "sp_AS_11000_PL_Equipment_Availability";
            this.spAS11000PLEquipmentAvailabilityBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLUnitDescriptorBindingSource
            // 
            this.spAS11000PLUnitDescriptorBindingSource.DataMember = "sp_AS_11000_PL_Unit_Descriptor";
            this.spAS11000PLUnitDescriptorBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLExchequerCategoryBindingSource
            // 
            this.spAS11000PLExchequerCategoryBindingSource.DataMember = "sp_AS_11000_PL_Exchequer_Category";
            this.spAS11000PLExchequerCategoryBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11002_Equipment_ItemTableAdapter
            // 
            this.sp_AS_11002_Equipment_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11011_Equipment_Category_ListTableAdapter
            // 
            this.sp_AS_11011_Equipment_Category_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11014_Make_ListTableAdapter
            // 
            this.sp_AS_11014_Make_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11017_Model_ListTableAdapter
            // 
            this.sp_AS_11017_Model_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11020_Supplier_ListTableAdapter
            // 
            this.sp_AS_11020_Supplier_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter
            // 
            this.sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Equipment_AvailabilityTableAdapter
            // 
            this.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Unit_DescriptorTableAdapter
            // 
            this.sp_AS_11000_PL_Unit_DescriptorTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Exchequer_CategoryTableAdapter
            // 
            this.sp_AS_11000_PL_Exchequer_CategoryTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11000DuplicateSearchBindingSource
            // 
            this.spAS11000DuplicateSearchBindingSource.DataMember = "sp_AS_11000_Duplicate_Search";
            this.spAS11000DuplicateSearchBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_Duplicate_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11066_Depreciation_Settings_ItemTableAdapter
            // 
            this.sp_AS_11066_Depreciation_Settings_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = this.sp_AS_11002_Equipment_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = this.sp_AS_11011_Equipment_Category_ListTableAdapter;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = this.sp_AS_11014_Make_ListTableAdapter;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = this.sp_AS_11017_Model_ListTableAdapter;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = this.sp_AS_11020_Supplier_ListTableAdapter;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = this.sp_AS_11066_Depreciation_Settings_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_AS_11005_Vehicle_ItemBindingSource
            // 
            this.sp_AS_11005_Vehicle_ItemBindingSource.DataMember = "sp_AS_11005_Vehicle_Item";
            this.sp_AS_11005_Vehicle_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11005_Vehicle_ItemTableAdapter
            // 
            this.sp_AS_11005_Vehicle_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11008_Plant_ItemBindingSource
            // 
            this.sp_AS_11008_Plant_ItemBindingSource.DataMember = "sp_AS_11008_Plant_Item";
            this.sp_AS_11008_Plant_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11008_Plant_ItemTableAdapter
            // 
            this.sp_AS_11008_Plant_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11029_Office_ItemBindingSource
            // 
            this.sp_AS_11029_Office_ItemBindingSource.DataMember = "sp_AS_11029_Office_Item";
            this.sp_AS_11029_Office_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11029_Office_ItemTableAdapter
            // 
            this.sp_AS_11029_Office_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11026_Gadget_ItemBindingSource
            // 
            this.sp_AS_11026_Gadget_ItemBindingSource.DataMember = "sp_AS_11026_Gadget_Item";
            this.sp_AS_11026_Gadget_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11026_Gadget_ItemTableAdapter
            // 
            this.sp_AS_11026_Gadget_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11032_Hardware_ItemBindingSource
            // 
            this.sp_AS_11032_Hardware_ItemBindingSource.DataMember = "sp_AS_11032_Hardware_Item";
            this.sp_AS_11032_Hardware_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11032_Hardware_ItemTableAdapter
            // 
            this.sp_AS_11032_Hardware_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11035_Software_ItemBindingSource
            // 
            this.sp_AS_11035_Software_ItemBindingSource.DataMember = "sp_AS_11035_Software_Item";
            this.sp_AS_11035_Software_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11035_Software_ItemTableAdapter
            // 
            this.sp_AS_11035_Software_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.dataLayoutControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(-1469, 300, 450, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1259, 909);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.lueDepreciationSetting);
            this.dataLayoutControl1.Controls.Add(this.lueEquipmentCategory);
            this.dataLayoutControl1.Controls.Add(this.txtManufacturerID);
            this.dataLayoutControl1.Controls.Add(this.lueMake);
            this.dataLayoutControl1.Controls.Add(this.lueModel);
            this.dataLayoutControl1.Controls.Add(this.lueSupplier);
            this.dataLayoutControl1.Controls.Add(this.lueGCMarked);
            this.dataLayoutControl1.Controls.Add(this.lueOwnershipStatus);
            this.dataLayoutControl1.Controls.Add(this.lueAvailability);
            this.dataLayoutControl1.Controls.Add(this.deDeliveryDate);
            this.dataLayoutControl1.Controls.Add(this.deDepreciationStartDate);
            this.dataLayoutControl1.Controls.Add(this.txtExchequerID);
            this.dataLayoutControl1.Controls.Add(this.txtSageReference);
            this.dataLayoutControl1.Controls.Add(this.lueOccursUnitDescriptor);
            this.dataLayoutControl1.Controls.Add(this.spnMaximumOccurrence);
            this.dataLayoutControl1.Controls.Add(this.spnDayOfMonthApplyDepreciation);
            this.dataLayoutControl1.Controls.Add(this.lueExchequerCategoryID);
            this.dataLayoutControl1.Controls.Add(this.memNotes);
            this.dataLayoutControl1.DataSource = this.spAS11002EquipmentItemBindingSource;
            this.dataLayoutControl1.Location = new System.Drawing.Point(54, 48);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2120, 390, 250, 350);
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1151, 794);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // lueDepreciationSetting
            // 
            this.lueDepreciationSetting.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp_AS_11066_Depreciation_Settings_ItemBindingSource, "DepreciationSettingID", true));
            this.lueDepreciationSetting.Location = new System.Drawing.Point(187, 249);
            this.lueDepreciationSetting.Name = "lueDepreciationSetting";
            this.lueDepreciationSetting.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDepreciationSetting.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SettingName", "Setting Name", 74, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OccurrenceFrequency", "Occurrence Frequency", 119, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OccursUnitDescriptor", "Occurs Unit Descriptor", 117, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MaximumOccurrence", "Maximum Occurrence", 112, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DayOfMonthApplyDepreciation", "Day Of Month Apply Depreciation", 170, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LastChanged", "Last Changed", 76, DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueDepreciationSetting.Properties.DataSource = this.sp_AS_11066_Depreciation_Settings_ItemBindingSource;
            this.lueDepreciationSetting.Properties.DisplayMember = "SettingName";
            this.lueDepreciationSetting.Properties.NullText = "";
            this.lueDepreciationSetting.Properties.NullValuePrompt = "-- Please Select --";
            this.lueDepreciationSetting.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueDepreciationSetting.Properties.ValueMember = "DepreciationSettingID";
            this.lueDepreciationSetting.Size = new System.Drawing.Size(401, 20);
            this.lueDepreciationSetting.StyleController = this.dataLayoutControl1;
            this.lueDepreciationSetting.TabIndex = 21;
            this.lueDepreciationSetting.EditValueChanged += new System.EventHandler(this.lueDepreciationSetting_EditValueChanged);
            // 
            // lueEquipmentCategory
            // 
            this.lueEquipmentCategory.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "EquipmentCategory", true));
            this.lueEquipmentCategory.Location = new System.Drawing.Point(187, 43);
            this.lueEquipmentCategory.Name = "lueEquipmentCategory";
            this.lueEquipmentCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueEquipmentCategory.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Equipment Category", 63, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ProfitLossCode", "Profit Loss Code", 88, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BalanceSheetCode", "Balance Sheet Code", 106, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DisposalsGL", "Disposals GL", 69, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueEquipmentCategory.Properties.DataSource = this.spAS11011EquipmentCategoryListBindingSource;
            this.lueEquipmentCategory.Properties.DisplayMember = "Description";
            this.lueEquipmentCategory.Properties.NullText = "";
            this.lueEquipmentCategory.Properties.NullValuePrompt = "-- Please Select --";
            this.lueEquipmentCategory.Properties.ValueMember = "EquipmentCategory";
            this.lueEquipmentCategory.Size = new System.Drawing.Size(940, 20);
            this.lueEquipmentCategory.StyleController = this.dataLayoutControl1;
            this.lueEquipmentCategory.TabIndex = 4;
            this.lueEquipmentCategory.Tag = "Equipment Category";
            this.lueEquipmentCategory.EditValueChanged += new System.EventHandler(this.lueEquipmentCategory_EditValueChanged);
            this.lueEquipmentCategory.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // txtManufacturerID
            // 
            this.txtManufacturerID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "ManufacturerID", true));
            this.txtManufacturerID.Location = new System.Drawing.Point(187, 110);
            this.txtManufacturerID.Name = "txtManufacturerID";
            this.txtManufacturerID.Size = new System.Drawing.Size(401, 20);
            this.txtManufacturerID.StyleController = this.dataLayoutControl1;
            this.txtManufacturerID.TabIndex = 5;
            this.txtManufacturerID.Tag = "Manufacturer ID";
            this.txtManufacturerID.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit_Validating);
            // 
            // lueMake
            // 
            this.lueMake.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Make", true));
            this.lueMake.Location = new System.Drawing.Point(187, 134);
            this.lueMake.Name = "lueMake";
            this.lueMake.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueMake.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Make", 63, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CategoryDescription", "Equipment Category", 111, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueMake.Properties.DataSource = this.spAS11014MakeListBindingSource;
            this.lueMake.Properties.DisplayMember = "Description";
            this.lueMake.Properties.NullText = "";
            this.lueMake.Properties.NullValuePrompt = "-- Please Select --";
            this.lueMake.Properties.ValueMember = "Make";
            this.lueMake.Size = new System.Drawing.Size(401, 20);
            this.lueMake.StyleController = this.dataLayoutControl1;
            this.lueMake.TabIndex = 6;
            this.lueMake.Tag = "Make";
            this.lueMake.EditValueChanged += new System.EventHandler(this.lueMake_EditValueChanged);
            this.lueMake.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // lueModel
            // 
            this.lueModel.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Model", true));
            this.lueModel.Location = new System.Drawing.Point(187, 158);
            this.lueModel.Name = "lueModel";
            this.lueModel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueModel.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Make_Description", "Make Description", 94, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Model", 63, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueModel.Properties.DataSource = this.spAS11017ModelListBindingSource;
            this.lueModel.Properties.DisplayMember = "Description";
            this.lueModel.Properties.NullText = "";
            this.lueModel.Properties.NullValuePrompt = "-- Please Select --";
            this.lueModel.Properties.ValueMember = "Model";
            this.lueModel.Size = new System.Drawing.Size(401, 20);
            this.lueModel.StyleController = this.dataLayoutControl1;
            this.lueModel.TabIndex = 7;
            this.lueModel.Tag = "Model";
            this.lueModel.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // lueSupplier
            // 
            this.lueSupplier.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Supplier", true));
            this.lueSupplier.Location = new System.Drawing.Point(187, 182);
            this.lueSupplier.Name = "lueSupplier";
            this.lueSupplier.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSupplier.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierType", "Supplier Type", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierReference", "Supplier Reference", 101, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSupplier.Properties.DataSource = this.spAS11020SupplierListBindingSource;
            this.lueSupplier.Properties.DisplayMember = "SupplierReference";
            this.lueSupplier.Properties.NullText = "";
            this.lueSupplier.Properties.NullValuePrompt = "-- Please Select --";
            this.lueSupplier.Properties.ValueMember = "SupplierID";
            this.lueSupplier.Size = new System.Drawing.Size(401, 20);
            this.lueSupplier.StyleController = this.dataLayoutControl1;
            this.lueSupplier.TabIndex = 8;
            this.lueSupplier.Tag = "Supplier";
            this.lueSupplier.EditValueChanged += new System.EventHandler(this.lueSupplier_EditValueChanged);
            this.lueSupplier.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // lueGCMarked
            // 
            this.lueGCMarked.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "GCMarked", true));
            this.lueGCMarked.Location = new System.Drawing.Point(779, 110);
            this.lueGCMarked.Name = "lueGCMarked";
            this.lueGCMarked.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueGCMarked.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "GC Marked", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueGCMarked.Properties.DataSource = this.spAS11000PLEquipmentGCMarkedBindingSource;
            this.lueGCMarked.Properties.DisplayMember = "Value";
            this.lueGCMarked.Properties.NullText = "";
            this.lueGCMarked.Properties.NullValuePrompt = "-- Please Select --";
            this.lueGCMarked.Properties.ValueMember = "PickListID";
            this.lueGCMarked.Size = new System.Drawing.Size(348, 20);
            this.lueGCMarked.StyleController = this.dataLayoutControl1;
            this.lueGCMarked.TabIndex = 9;
            this.lueGCMarked.Tag = "GC Mark ";
            this.lueGCMarked.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // lueOwnershipStatus
            // 
            this.lueOwnershipStatus.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "OwnershipStatus", true));
            this.lueOwnershipStatus.Location = new System.Drawing.Point(779, 134);
            this.lueOwnershipStatus.Name = "lueOwnershipStatus";
            this.lueOwnershipStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueOwnershipStatus.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Ownership Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueOwnershipStatus.Properties.DataSource = this.spAS11000PLEquipmentOwnershipStatusBindingSource;
            this.lueOwnershipStatus.Properties.DisplayMember = "Value";
            this.lueOwnershipStatus.Properties.NullText = "";
            this.lueOwnershipStatus.Properties.NullValuePrompt = "-- Please Select --";
            this.lueOwnershipStatus.Properties.ValueMember = "PickListID";
            this.lueOwnershipStatus.Size = new System.Drawing.Size(348, 20);
            this.lueOwnershipStatus.StyleController = this.dataLayoutControl1;
            this.lueOwnershipStatus.TabIndex = 10;
            this.lueOwnershipStatus.Tag = "Ownership Status";
            this.lueOwnershipStatus.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // lueAvailability
            // 
            this.lueAvailability.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Availability", true));
            this.lueAvailability.Location = new System.Drawing.Point(779, 158);
            this.lueAvailability.Name = "lueAvailability";
            this.lueAvailability.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAvailability.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Availability", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAvailability.Properties.DataSource = this.spAS11000PLEquipmentAvailabilityBindingSource;
            this.lueAvailability.Properties.DisplayMember = "Value";
            this.lueAvailability.Properties.NullText = "";
            this.lueAvailability.Properties.NullValuePrompt = "-- Please Select --";
            this.lueAvailability.Properties.ValueMember = "PickListID";
            this.lueAvailability.Size = new System.Drawing.Size(348, 20);
            this.lueAvailability.StyleController = this.dataLayoutControl1;
            this.lueAvailability.TabIndex = 11;
            this.lueAvailability.Tag = "Availability";
            this.lueAvailability.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // deDeliveryDate
            // 
            this.deDeliveryDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "DeliveryDate", true));
            this.deDeliveryDate.EditValue = null;
            this.deDeliveryDate.Location = new System.Drawing.Point(779, 182);
            this.deDeliveryDate.Name = "deDeliveryDate";
            this.deDeliveryDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDeliveryDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDeliveryDate.Size = new System.Drawing.Size(348, 20);
            this.deDeliveryDate.StyleController = this.dataLayoutControl1;
            this.deDeliveryDate.TabIndex = 12;
            this.deDeliveryDate.Tag = "Delivery Date";
            // 
            // deDepreciationStartDate
            // 
            this.deDepreciationStartDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "DepreciationStartDate", true));
            this.deDepreciationStartDate.EditValue = null;
            this.deDepreciationStartDate.Location = new System.Drawing.Point(779, 273);
            this.deDepreciationStartDate.Name = "deDepreciationStartDate";
            this.deDepreciationStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDepreciationStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDepreciationStartDate.Size = new System.Drawing.Size(348, 20);
            this.deDepreciationStartDate.StyleController = this.dataLayoutControl1;
            this.deDepreciationStartDate.TabIndex = 13;
            this.deDepreciationStartDate.Tag = "Depreciation Start Date";
            // 
            // txtExchequerID
            // 
            this.txtExchequerID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "ExchequerID", true));
            this.txtExchequerID.Location = new System.Drawing.Point(779, 297);
            this.txtExchequerID.Name = "txtExchequerID";
            this.txtExchequerID.Size = new System.Drawing.Size(348, 20);
            this.txtExchequerID.StyleController = this.dataLayoutControl1;
            this.txtExchequerID.TabIndex = 14;
            this.txtExchequerID.Tag = "Exchequer ID";
            // 
            // txtSageReference
            // 
            this.txtSageReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "SageReference", true));
            this.txtSageReference.Location = new System.Drawing.Point(779, 321);
            this.txtSageReference.Name = "txtSageReference";
            this.txtSageReference.Size = new System.Drawing.Size(348, 20);
            this.txtSageReference.StyleController = this.dataLayoutControl1;
            this.txtSageReference.TabIndex = 15;
            this.txtSageReference.Tag = "Sage Reference";
            // 
            // lueOccursUnitDescriptor
            // 
            this.lueOccursUnitDescriptor.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "OccursUnitDescriptor", true));
            this.lueOccursUnitDescriptor.Location = new System.Drawing.Point(187, 273);
            this.lueOccursUnitDescriptor.Name = "lueOccursUnitDescriptor";
            this.lueOccursUnitDescriptor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueOccursUnitDescriptor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Occurs Unit Descriptor", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueOccursUnitDescriptor.Properties.DataSource = this.spAS11000PLUnitDescriptorBindingSource;
            this.lueOccursUnitDescriptor.Properties.DisplayMember = "Value";
            this.lueOccursUnitDescriptor.Properties.NullText = "";
            this.lueOccursUnitDescriptor.Properties.NullValuePrompt = "-- Please Select --";
            this.lueOccursUnitDescriptor.Properties.ValueMember = "PickListID";
            this.lueOccursUnitDescriptor.Size = new System.Drawing.Size(401, 20);
            this.lueOccursUnitDescriptor.StyleController = this.dataLayoutControl1;
            this.lueOccursUnitDescriptor.TabIndex = 16;
            this.lueOccursUnitDescriptor.Tag = "Occurs Unit Descriptor";
            this.lueOccursUnitDescriptor.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spnMaximumOccurrence
            // 
            this.spnMaximumOccurrence.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "MaximumOccurrence", true));
            this.spnMaximumOccurrence.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnMaximumOccurrence.Location = new System.Drawing.Point(187, 297);
            this.spnMaximumOccurrence.Name = "spnMaximumOccurrence";
            this.spnMaximumOccurrence.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnMaximumOccurrence.Size = new System.Drawing.Size(401, 20);
            this.spnMaximumOccurrence.StyleController = this.dataLayoutControl1;
            this.spnMaximumOccurrence.TabIndex = 17;
            this.spnMaximumOccurrence.Tag = "Maximum Occurrence";
            // 
            // spnDayOfMonthApplyDepreciation
            // 
            this.spnDayOfMonthApplyDepreciation.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "DayOfMonthApplyDepreciation", true));
            this.spnDayOfMonthApplyDepreciation.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnDayOfMonthApplyDepreciation.Location = new System.Drawing.Point(187, 321);
            this.spnDayOfMonthApplyDepreciation.Name = "spnDayOfMonthApplyDepreciation";
            this.spnDayOfMonthApplyDepreciation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnDayOfMonthApplyDepreciation.Properties.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.spnDayOfMonthApplyDepreciation.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnDayOfMonthApplyDepreciation.Size = new System.Drawing.Size(401, 20);
            this.spnDayOfMonthApplyDepreciation.StyleController = this.dataLayoutControl1;
            this.spnDayOfMonthApplyDepreciation.TabIndex = 18;
            this.spnDayOfMonthApplyDepreciation.Tag = "Day Of Month Apply Depreciation";
            // 
            // lueExchequerCategoryID
            // 
            this.lueExchequerCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "ExchequerCategoryID", true));
            this.lueExchequerCategoryID.Location = new System.Drawing.Point(779, 249);
            this.lueExchequerCategoryID.Name = "lueExchequerCategoryID";
            this.lueExchequerCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueExchequerCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Exchequer Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueExchequerCategoryID.Properties.DataSource = this.spAS11000PLExchequerCategoryBindingSource;
            this.lueExchequerCategoryID.Properties.DisplayMember = "Value";
            this.lueExchequerCategoryID.Properties.NullText = "";
            this.lueExchequerCategoryID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueExchequerCategoryID.Properties.ValueMember = "PickListID";
            this.lueExchequerCategoryID.Size = new System.Drawing.Size(348, 20);
            this.lueExchequerCategoryID.StyleController = this.dataLayoutControl1;
            this.lueExchequerCategoryID.TabIndex = 19;
            this.lueExchequerCategoryID.Tag = "Exchequer Category";
            this.lueExchequerCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // memNotes
            // 
            this.memNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11002EquipmentItemBindingSource, "Notes", true));
            this.memNotes.Location = new System.Drawing.Point(24, 395);
            this.memNotes.Name = "memNotes";
            this.memNotes.Size = new System.Drawing.Size(1103, 375);
            this.memNotes.StyleController = this.dataLayoutControl1;
            this.memNotes.TabIndex = 20;
            this.memNotes.Tag = "Notes";
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1151, 794);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlGroup8});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1131, 774);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 345);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1131, 429);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup7.CaptionImage")));
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1107, 379);
            this.layoutControlGroup7.Text = "Remarks";
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.memNotes;
            this.ItemForNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(1107, 379);
            this.ItemForNotes.StartNewLine = true;
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNotes.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImage")));
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ifManufacturerID,
            this.ItemForMake,
            this.ItemForModel,
            this.ItemForSupplier});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 67);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(592, 139);
            this.layoutControlGroup3.Text = "Manufacturer Details";
            // 
            // ifManufacturerID
            // 
            this.ifManufacturerID.Control = this.txtManufacturerID;
            this.ifManufacturerID.Location = new System.Drawing.Point(0, 0);
            this.ifManufacturerID.Name = "ifManufacturerID";
            this.ifManufacturerID.Size = new System.Drawing.Size(568, 24);
            this.ifManufacturerID.Text = "Manufacturer ID";
            this.ifManufacturerID.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForMake
            // 
            this.ItemForMake.Control = this.lueMake;
            this.ItemForMake.Location = new System.Drawing.Point(0, 24);
            this.ItemForMake.Name = "ItemForMake";
            this.ItemForMake.Size = new System.Drawing.Size(568, 24);
            this.ItemForMake.Text = "Make";
            this.ItemForMake.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForModel
            // 
            this.ItemForModel.Control = this.lueModel;
            this.ItemForModel.Location = new System.Drawing.Point(0, 48);
            this.ItemForModel.Name = "ItemForModel";
            this.ItemForModel.Size = new System.Drawing.Size(568, 24);
            this.ItemForModel.Text = "Model";
            this.ItemForModel.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForSupplier
            // 
            this.ItemForSupplier.Control = this.lueSupplier;
            this.ItemForSupplier.Location = new System.Drawing.Point(0, 72);
            this.ItemForSupplier.Name = "ItemForSupplier";
            this.ItemForSupplier.Size = new System.Drawing.Size(568, 24);
            this.ItemForSupplier.Text = "Supplier";
            this.ItemForSupplier.TextSize = new System.Drawing.Size(160, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForMaximumOccurrence,
            this.ItemForOccursUnitDescriptor,
            this.ItemForDayOfMonthApplyDepreciation,
            this.layoutControlItem2});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 206);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(592, 139);
            this.layoutControlGroup4.Text = "Depreciation Details";
            // 
            // ItemForMaximumOccurrence
            // 
            this.ItemForMaximumOccurrence.Control = this.spnMaximumOccurrence;
            this.ItemForMaximumOccurrence.Location = new System.Drawing.Point(0, 48);
            this.ItemForMaximumOccurrence.Name = "ItemForMaximumOccurrence";
            this.ItemForMaximumOccurrence.Size = new System.Drawing.Size(568, 24);
            this.ItemForMaximumOccurrence.Text = "Maximum Occurrence";
            this.ItemForMaximumOccurrence.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForOccursUnitDescriptor
            // 
            this.ItemForOccursUnitDescriptor.Control = this.lueOccursUnitDescriptor;
            this.ItemForOccursUnitDescriptor.Location = new System.Drawing.Point(0, 24);
            this.ItemForOccursUnitDescriptor.Name = "ItemForOccursUnitDescriptor";
            this.ItemForOccursUnitDescriptor.Size = new System.Drawing.Size(568, 24);
            this.ItemForOccursUnitDescriptor.Text = "Occurs Unit Descriptor";
            this.ItemForOccursUnitDescriptor.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForDayOfMonthApplyDepreciation
            // 
            this.ItemForDayOfMonthApplyDepreciation.Control = this.spnDayOfMonthApplyDepreciation;
            this.ItemForDayOfMonthApplyDepreciation.Location = new System.Drawing.Point(0, 72);
            this.ItemForDayOfMonthApplyDepreciation.Name = "ItemForDayOfMonthApplyDepreciation";
            this.ItemForDayOfMonthApplyDepreciation.Size = new System.Drawing.Size(568, 24);
            this.ItemForDayOfMonthApplyDepreciation.Text = "Day Of Month Apply Depreciation";
            this.ItemForDayOfMonthApplyDepreciation.TextSize = new System.Drawing.Size(160, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lueDepreciationSetting;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(568, 24);
            this.layoutControlItem2.Text = "Depreciation Setting";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(160, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForExchequerCategoryID,
            this.ItemForDepreciationStartDate,
            this.ItemForExchequerID,
            this.ItemForSageReference});
            this.layoutControlGroup5.Location = new System.Drawing.Point(592, 206);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(539, 139);
            this.layoutControlGroup5.Text = "Finance Details";
            // 
            // ItemForExchequerCategoryID
            // 
            this.ItemForExchequerCategoryID.Control = this.lueExchequerCategoryID;
            this.ItemForExchequerCategoryID.Location = new System.Drawing.Point(0, 0);
            this.ItemForExchequerCategoryID.Name = "ItemForExchequerCategoryID";
            this.ItemForExchequerCategoryID.Size = new System.Drawing.Size(515, 24);
            this.ItemForExchequerCategoryID.Text = "Exchequer Category";
            this.ItemForExchequerCategoryID.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForDepreciationStartDate
            // 
            this.ItemForDepreciationStartDate.Control = this.deDepreciationStartDate;
            this.ItemForDepreciationStartDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForDepreciationStartDate.Name = "ItemForDepreciationStartDate";
            this.ItemForDepreciationStartDate.Size = new System.Drawing.Size(515, 24);
            this.ItemForDepreciationStartDate.Text = "Depreciation Start Date";
            this.ItemForDepreciationStartDate.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForExchequerID
            // 
            this.ItemForExchequerID.Control = this.txtExchequerID;
            this.ItemForExchequerID.Location = new System.Drawing.Point(0, 48);
            this.ItemForExchequerID.Name = "ItemForExchequerID";
            this.ItemForExchequerID.Size = new System.Drawing.Size(515, 24);
            this.ItemForExchequerID.Text = "Exchequer";
            this.ItemForExchequerID.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForSageReference
            // 
            this.ItemForSageReference.Control = this.txtSageReference;
            this.ItemForSageReference.Location = new System.Drawing.Point(0, 72);
            this.ItemForSageReference.Name = "ItemForSageReference";
            this.ItemForSageReference.Size = new System.Drawing.Size(515, 24);
            this.ItemForSageReference.Text = "Sage Reference";
            this.ItemForSageReference.TextSize = new System.Drawing.Size(160, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForOwnershipStatus,
            this.ItemForGCMarked,
            this.ItemForAvailability,
            this.ItemForDeliveryDate});
            this.layoutControlGroup6.Location = new System.Drawing.Point(592, 67);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(539, 139);
            this.layoutControlGroup6.Text = "Ground Control Details";
            // 
            // ItemForOwnershipStatus
            // 
            this.ItemForOwnershipStatus.Control = this.lueOwnershipStatus;
            this.ItemForOwnershipStatus.Location = new System.Drawing.Point(0, 24);
            this.ItemForOwnershipStatus.Name = "ItemForOwnershipStatus";
            this.ItemForOwnershipStatus.Size = new System.Drawing.Size(515, 24);
            this.ItemForOwnershipStatus.Text = "Ownership Status";
            this.ItemForOwnershipStatus.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForGCMarked
            // 
            this.ItemForGCMarked.Control = this.lueGCMarked;
            this.ItemForGCMarked.Location = new System.Drawing.Point(0, 0);
            this.ItemForGCMarked.Name = "ItemForGCMarked";
            this.ItemForGCMarked.Size = new System.Drawing.Size(515, 24);
            this.ItemForGCMarked.Text = "GC Marked";
            this.ItemForGCMarked.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForAvailability
            // 
            this.ItemForAvailability.Control = this.lueAvailability;
            this.ItemForAvailability.Location = new System.Drawing.Point(0, 48);
            this.ItemForAvailability.Name = "ItemForAvailability";
            this.ItemForAvailability.Size = new System.Drawing.Size(515, 24);
            this.ItemForAvailability.Text = "Availability";
            this.ItemForAvailability.TextSize = new System.Drawing.Size(160, 13);
            // 
            // ItemForDeliveryDate
            // 
            this.ItemForDeliveryDate.Control = this.deDeliveryDate;
            this.ItemForDeliveryDate.Location = new System.Drawing.Point(0, 72);
            this.ItemForDeliveryDate.Name = "ItemForDeliveryDate";
            this.ItemForDeliveryDate.Size = new System.Drawing.Size(515, 24);
            this.ItemForDeliveryDate.Text = "Delivery Date";
            this.ItemForDeliveryDate.TextSize = new System.Drawing.Size(160, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEquipmentCategory});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1131, 67);
            this.layoutControlGroup8.Text = "Equipment Selection";
            // 
            // ItemForEquipmentCategory
            // 
            this.ItemForEquipmentCategory.Control = this.lueEquipmentCategory;
            this.ItemForEquipmentCategory.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentCategory.Name = "ItemForEquipmentCategory";
            this.ItemForEquipmentCategory.Size = new System.Drawing.Size(1107, 24);
            this.ItemForEquipmentCategory.Text = "Equipment Category";
            this.ItemForEquipmentCategory.TextSize = new System.Drawing.Size(160, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem2,
            this.layoutControlGroup9});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(40, 40, 0, 40);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1259, 909);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem1.CustomizationFormText = "Options:";
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(1155, 34);
            this.simpleLabelItem1.Text = "Adding Equipment:";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(185, 30);
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.CustomizationFormText = "Select installation folder to continue";
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 856);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 0, 0, 0);
            this.simpleLabelItem2.Size = new System.Drawing.Size(1179, 13);
            this.simpleLabelItem2.Text = "Select next to continue";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(185, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataLayoutControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1155, 798);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // dxErrorProviderEquipment
            // 
            this.dxErrorProviderEquipment.ContainerControl = this;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.simpleLabelItem1});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1179, 856);
            this.layoutControlGroup9.TextVisible = false;
            // 
            // ucStep1Page
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Office 2013";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ucStep1Page";
            this.Size = new System.Drawing.Size(1259, 909);
            this.validPage = true;
            this.Load += new System.EventHandler(this.ucStep1Page_Load);
            this.Enter += new System.EventHandler(this.ucStep1Page_Enter);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.ucStep1Page_Validating);
            this.Validated += new System.EventHandler(this.ucStep1Page_Validated);
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11066_Depreciation_Settings_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11002EquipmentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11011EquipmentCategoryListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11014MakeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11020SupplierListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentGCMarkedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentOwnershipStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLEquipmentAvailabilityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLUnitDescriptorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLExchequerCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11005_Vehicle_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11008_Plant_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11029_Office_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11026_Gadget_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11032_Hardware_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11035_Software_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueDepreciationSetting.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueEquipmentCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtManufacturerID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMake.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSupplier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueGCMarked.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOwnershipStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAvailability.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDeliveryDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDeliveryDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDepreciationStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDepreciationStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExchequerID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSageReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOccursUnitDescriptor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnMaximumOccurrence.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnDayOfMonthApplyDepreciation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueExchequerCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ifManufacturerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMaximumOccurrence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOccursUnitDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDayOfMonthApplyDepreciation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExchequerCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepreciationStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExchequerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSageReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnershipStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCMarked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAvailability)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeliveryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProviderEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.LookUpEdit lueEquipmentCategory;
        private System.Windows.Forms.BindingSource spAS11002EquipmentItemBindingSource;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraEditors.TextEdit txtManufacturerID;
        private DevExpress.XtraEditors.LookUpEdit lueMake;
        private DevExpress.XtraEditors.LookUpEdit lueModel;
        private DevExpress.XtraEditors.LookUpEdit lueSupplier;
        private DevExpress.XtraEditors.LookUpEdit lueGCMarked;
        private DevExpress.XtraEditors.LookUpEdit lueOwnershipStatus;
        private DevExpress.XtraEditors.LookUpEdit lueAvailability;
        private DevExpress.XtraEditors.DateEdit deDeliveryDate;
        private DevExpress.XtraEditors.DateEdit deDepreciationStartDate;
        private DevExpress.XtraEditors.TextEdit txtExchequerID;
        private DevExpress.XtraEditors.TextEdit txtSageReference;
        private DevExpress.XtraEditors.LookUpEdit lueOccursUnitDescriptor;
        private DevExpress.XtraEditors.SpinEdit spnMaximumOccurrence;
        private DevExpress.XtraEditors.SpinEdit spnDayOfMonthApplyDepreciation;
        private DevExpress.XtraEditors.LookUpEdit lueExchequerCategoryID;
        private DevExpress.XtraEditors.MemoEdit memNotes;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGCMarked;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnershipStatus;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAvailability;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeliveryDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDepreciationStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExchequerID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSageReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOccursUnitDescriptor;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMaximumOccurrence;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDayOfMonthApplyDepreciation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExchequerCategoryID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter sp_AS_11002_Equipment_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11011EquipmentCategoryListBindingSource;
        private System.Windows.Forms.BindingSource spAS11014MakeListBindingSource;
        private System.Windows.Forms.BindingSource spAS11017ModelListBindingSource;
        private System.Windows.Forms.BindingSource spAS11020SupplierListBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLEquipmentGCMarkedBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLEquipmentOwnershipStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLEquipmentAvailabilityBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLUnitDescriptorBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLExchequerCategoryBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11011_Equipment_Category_ListTableAdapter sp_AS_11011_Equipment_Category_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter sp_AS_11014_Make_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter sp_AS_11017_Model_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11020_Supplier_ListTableAdapter sp_AS_11020_Supplier_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter sp_AS_11000_PL_Equipment_GC_MarkedTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter sp_AS_11000_PL_Equipment_Ownership_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Equipment_AvailabilityTableAdapter sp_AS_11000_PL_Equipment_AvailabilityTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Unit_DescriptorTableAdapter sp_AS_11000_PL_Unit_DescriptorTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Exchequer_CategoryTableAdapter sp_AS_11000_PL_Exchequer_CategoryTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000DuplicateSearchBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter sp_AS_11000_Duplicate_SearchTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11066_Depreciation_Settings_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11066_Depreciation_Settings_ItemTableAdapter sp_AS_11066_Depreciation_Settings_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.LookUpEdit lueDepreciationSetting;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProviderEquipment;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMake;
        private DevExpress.XtraLayout.LayoutControlItem ItemForModel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSupplier;
        private DevExpress.XtraLayout.LayoutControlItem ifManufacturerID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentCategory;
        private System.Windows.Forms.BindingSource sp_AS_11005_Vehicle_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter sp_AS_11005_Vehicle_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11008_Plant_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11008_Plant_ItemTableAdapter sp_AS_11008_Plant_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11029_Office_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11029_Office_ItemTableAdapter sp_AS_11029_Office_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11026_Gadget_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11026_Gadget_ItemTableAdapter sp_AS_11026_Gadget_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11032_Hardware_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11032_Hardware_ItemTableAdapter sp_AS_11032_Hardware_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11035_Software_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11035_Software_ItemTableAdapter sp_AS_11035_Software_ItemTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
    }
}

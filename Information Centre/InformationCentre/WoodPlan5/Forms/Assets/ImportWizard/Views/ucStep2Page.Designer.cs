﻿namespace WoodPlan5.Forms.Assets.ImportWizard
{
    partial class ucStep2Page
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.spAS11029OfficeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.spAS11000PLOfficeCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11008PlantItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLFuelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLPlantCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11005VehicleItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLVehicleCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000PLVehicleTowbarTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11008_Plant_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11008_Plant_ItemTableAdapter();
            this.sp_AS_11029_Office_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11029_Office_ItemTableAdapter();
            this.sp_AS_11005_Vehicle_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.spAS11000DuplicateSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11000DuplicateSearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter();
            this.sp_AS_11035_Software_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11035_Software_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11035_Software_ItemTableAdapter();
            this.sp_AS_11032_Hardware_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11032_Hardware_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11032_Hardware_ItemTableAdapter();
            this.sp_AS_11026_Gadget_ItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11026_Gadget_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11026_Gadget_ItemTableAdapter();
            this.sp_AS_11000_PL_Vehicle_CategoryTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Vehicle_CategoryTableAdapter();
            this.sp_AS_11000_PL_FuelTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_FuelTableAdapter();
            this.sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter();
            this.sp_AS_11000_PL_Plant_CategoryTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Plant_CategoryTableAdapter();
            this.sp_AS_11000_PL_Office_CategoryTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Office_CategoryTableAdapter();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtPlantRegistrationPlate = new DevExpress.XtraEditors.TextEdit();
            this.spnQuantity = new DevExpress.XtraEditors.SpinEdit();
            this.lueOfficeCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.spnEngineSize = new DevExpress.XtraEditors.SpinEdit();
            this.dePUWERDate = new DevExpress.XtraEditors.DateEdit();
            this.deLOLERDate = new DevExpress.XtraEditors.DateEdit();
            this.luePlantFuelTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.luePlantCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.txtShortDescription = new DevExpress.XtraEditors.TextEdit();
            this.lueVehicleCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.txtRegistrationPlate = new DevExpress.XtraEditors.TextEdit();
            this.txtLogBookNumber = new DevExpress.XtraEditors.TextEdit();
            this.txtModelSpecifications = new DevExpress.XtraEditors.TextEdit();
            this.spnVehicleEngineSize = new DevExpress.XtraEditors.SpinEdit();
            this.lueVehicleFuelTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spnEmissions = new DevExpress.XtraEditors.SpinEdit();
            this.spnTotalTyreNumber = new DevExpress.XtraEditors.SpinEdit();
            this.lueTowbarTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.cpeColour = new DevExpress.XtraEditors.ColorEdit();
            this.deRegistrationDate = new DevExpress.XtraEditors.DateEdit();
            this.deMOTDueDate = new DevExpress.XtraEditors.DateEdit();
            this.spnCurrentMileage = new DevExpress.XtraEditors.SpinEdit();
            this.spnServiceDueMileage = new DevExpress.XtraEditors.SpinEdit();
            this.txtRadioCode = new DevExpress.XtraEditors.TextEdit();
            this.txtKeyCode = new DevExpress.XtraEditors.TextEdit();
            this.txtElectronicCode = new DevExpress.XtraEditors.TextEdit();
            this.memDescription = new DevExpress.XtraEditors.MemoEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.plantLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.vehicleLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRegistrationPlate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLogBookNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRadioCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKeyCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForElectronicCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForMOTDueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCurrentMileage = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForServiceDueMileage = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForModelSpecifications = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalTyreNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForColour = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegistrationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTowbarTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEmissions = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEngineSize = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFuelTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.officeLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.NoDetailsLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblPageTitle = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVehicleCategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.spAS11029OfficeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLOfficeCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11008PlantItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLFuelBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLPlantCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11005VehicleItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLVehicleCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLVehicleTowbarTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11035_Software_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11032_Hardware_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11026_Gadget_ItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlantRegistrationPlate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOfficeCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnEngineSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dePUWERDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dePUWERDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLOLERDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLOLERDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePlantFuelTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePlantCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShortDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueVehicleCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationPlate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogBookNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModelSpecifications.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnVehicleEngineSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueVehicleFuelTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnEmissions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnTotalTyreNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTowbarTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpeColour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRegistrationDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRegistrationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deMOTDueDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deMOTDueDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnCurrentMileage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnServiceDueMileage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRadioCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeyCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtElectronicCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegistrationPlate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLogBookNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRadioCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeyCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForElectronicCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMOTDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrentMileage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueMileage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModelSpecifications)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalTyreNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForColour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegistrationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTowbarTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmissions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEngineSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFuelTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoDetailsLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVehicleCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // spAS11029OfficeItemBindingSource
            // 
            this.spAS11029OfficeItemBindingSource.DataMember = "sp_AS_11029_Office_Item";
            this.spAS11029OfficeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spAS11000PLOfficeCategoryBindingSource
            // 
            this.spAS11000PLOfficeCategoryBindingSource.DataMember = "sp_AS_11000_PL_Office_Category";
            this.spAS11000PLOfficeCategoryBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11008PlantItemBindingSource
            // 
            this.spAS11008PlantItemBindingSource.DataMember = "sp_AS_11008_Plant_Item";
            this.spAS11008PlantItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLFuelBindingSource
            // 
            this.spAS11000PLFuelBindingSource.DataMember = "sp_AS_11000_PL_Fuel";
            this.spAS11000PLFuelBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLPlantCategoryBindingSource
            // 
            this.spAS11000PLPlantCategoryBindingSource.DataMember = "sp_AS_11000_PL_Plant_Category";
            this.spAS11000PLPlantCategoryBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11005VehicleItemBindingSource
            // 
            this.spAS11005VehicleItemBindingSource.DataMember = "sp_AS_11005_Vehicle_Item";
            this.spAS11005VehicleItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLVehicleCategoryBindingSource
            // 
            this.spAS11000PLVehicleCategoryBindingSource.DataMember = "sp_AS_11000_PL_Vehicle_Category";
            this.spAS11000PLVehicleCategoryBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000PLVehicleTowbarTypeBindingSource
            // 
            this.spAS11000PLVehicleTowbarTypeBindingSource.DataMember = "sp_AS_11000_PL_Vehicle_Towbar_Type";
            this.spAS11000PLVehicleTowbarTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11008_Plant_ItemTableAdapter
            // 
            this.sp_AS_11008_Plant_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11029_Office_ItemTableAdapter
            // 
            this.sp_AS_11029_Office_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11005_Vehicle_ItemTableAdapter
            // 
            this.sp_AS_11005_Vehicle_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = this.sp_AS_11005_Vehicle_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = this.sp_AS_11008_Plant_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = this.sp_AS_11029_Office_ItemTableAdapter;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup1.SelectedTabPageIndex = 1;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1049, 692);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1025, 646);
            this.layoutControlGroup6.Text = "Vehicle Detail";
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1025, 646);
            this.layoutControlGroup5.Text = "Plant and Machinery Detail";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1025, 598);
            this.layoutControlGroup4.Text = "Item Description";
            // 
            // spAS11000DuplicateSearchBindingSource
            // 
            this.spAS11000DuplicateSearchBindingSource.DataMember = "sp_AS_11000_Duplicate_Search";
            this.spAS11000DuplicateSearchBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11000DuplicateSearchTableAdapter
            // 
            this.spAS11000DuplicateSearchTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11035_Software_ItemBindingSource
            // 
            this.sp_AS_11035_Software_ItemBindingSource.DataMember = "sp_AS_11035_Software_Item";
            this.sp_AS_11035_Software_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11035_Software_ItemTableAdapter
            // 
            this.sp_AS_11035_Software_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11032_Hardware_ItemBindingSource
            // 
            this.sp_AS_11032_Hardware_ItemBindingSource.DataMember = "sp_AS_11032_Hardware_Item";
            this.sp_AS_11032_Hardware_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11032_Hardware_ItemTableAdapter
            // 
            this.sp_AS_11032_Hardware_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11026_Gadget_ItemBindingSource
            // 
            this.sp_AS_11026_Gadget_ItemBindingSource.DataMember = "sp_AS_11026_Gadget_Item";
            this.sp_AS_11026_Gadget_ItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11026_Gadget_ItemTableAdapter
            // 
            this.sp_AS_11026_Gadget_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Vehicle_CategoryTableAdapter
            // 
            this.sp_AS_11000_PL_Vehicle_CategoryTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_FuelTableAdapter
            // 
            this.sp_AS_11000_PL_FuelTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Plant_CategoryTableAdapter
            // 
            this.sp_AS_11000_PL_Plant_CategoryTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Office_CategoryTableAdapter
            // 
            this.sp_AS_11000_PL_Office_CategoryTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.dataLayoutControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(-1469, 300, 450, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1255, 905);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.txtPlantRegistrationPlate);
            this.dataLayoutControl1.Controls.Add(this.spnQuantity);
            this.dataLayoutControl1.Controls.Add(this.lueOfficeCategoryID);
            this.dataLayoutControl1.Controls.Add(this.spnEngineSize);
            this.dataLayoutControl1.Controls.Add(this.dePUWERDate);
            this.dataLayoutControl1.Controls.Add(this.deLOLERDate);
            this.dataLayoutControl1.Controls.Add(this.luePlantFuelTypeID);
            this.dataLayoutControl1.Controls.Add(this.luePlantCategoryID);
            this.dataLayoutControl1.Controls.Add(this.txtShortDescription);
            this.dataLayoutControl1.Controls.Add(this.lueVehicleCategoryID);
            this.dataLayoutControl1.Controls.Add(this.txtRegistrationPlate);
            this.dataLayoutControl1.Controls.Add(this.txtLogBookNumber);
            this.dataLayoutControl1.Controls.Add(this.txtModelSpecifications);
            this.dataLayoutControl1.Controls.Add(this.spnVehicleEngineSize);
            this.dataLayoutControl1.Controls.Add(this.lueVehicleFuelTypeID);
            this.dataLayoutControl1.Controls.Add(this.spnEmissions);
            this.dataLayoutControl1.Controls.Add(this.spnTotalTyreNumber);
            this.dataLayoutControl1.Controls.Add(this.lueTowbarTypeID);
            this.dataLayoutControl1.Controls.Add(this.cpeColour);
            this.dataLayoutControl1.Controls.Add(this.deRegistrationDate);
            this.dataLayoutControl1.Controls.Add(this.deMOTDueDate);
            this.dataLayoutControl1.Controls.Add(this.spnCurrentMileage);
            this.dataLayoutControl1.Controls.Add(this.spnServiceDueMileage);
            this.dataLayoutControl1.Controls.Add(this.txtRadioCode);
            this.dataLayoutControl1.Controls.Add(this.txtKeyCode);
            this.dataLayoutControl1.Controls.Add(this.txtElectronicCode);
            this.dataLayoutControl1.Controls.Add(this.memDescription);
            this.dataLayoutControl1.DataSource = this.spAS11005VehicleItemBindingSource;
            this.dataLayoutControl1.Location = new System.Drawing.Point(54, 48);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2200, 501, 250, 350);
            this.dataLayoutControl1.Root = this.Root;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1147, 790);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // txtPlantRegistrationPlate
            // 
            this.txtPlantRegistrationPlate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "RegistrationPlate", true));
            this.txtPlantRegistrationPlate.Location = new System.Drawing.Point(123, 71);
            this.txtPlantRegistrationPlate.Name = "txtPlantRegistrationPlate";
            this.txtPlantRegistrationPlate.Size = new System.Drawing.Size(1000, 20);
            this.txtPlantRegistrationPlate.StyleController = this.dataLayoutControl1;
            this.txtPlantRegistrationPlate.TabIndex = 32;
            this.txtPlantRegistrationPlate.Validating += new System.ComponentModel.CancelEventHandler(this.txtPlantRegistrationPlate_Validating);
            // 
            // spnQuantity
            // 
            this.spnQuantity.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11029OfficeItemBindingSource, "Quantity", true));
            this.spnQuantity.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnQuantity.Location = new System.Drawing.Point(123, 71);
            this.spnQuantity.Name = "spnQuantity";
            this.spnQuantity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnQuantity.Size = new System.Drawing.Size(1000, 20);
            this.spnQuantity.StyleController = this.dataLayoutControl1;
            this.spnQuantity.TabIndex = 29;
            this.spnQuantity.Tag = "Quantity";
            this.spnQuantity.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditOffice_Validating);
            // 
            // lueOfficeCategoryID
            // 
            this.lueOfficeCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11029OfficeItemBindingSource, "OfficeCategoryID", true));
            this.lueOfficeCategoryID.Location = new System.Drawing.Point(123, 47);
            this.lueOfficeCategoryID.Name = "lueOfficeCategoryID";
            this.lueOfficeCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueOfficeCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Office Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueOfficeCategoryID.Properties.DataSource = this.spAS11000PLOfficeCategoryBindingSource;
            this.lueOfficeCategoryID.Properties.DisplayMember = "Value";
            this.lueOfficeCategoryID.Properties.NullText = "";
            this.lueOfficeCategoryID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueOfficeCategoryID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueOfficeCategoryID.Properties.ValueMember = "PickListID";
            this.lueOfficeCategoryID.Size = new System.Drawing.Size(1000, 20);
            this.lueOfficeCategoryID.StyleController = this.dataLayoutControl1;
            this.lueOfficeCategoryID.TabIndex = 28;
            this.lueOfficeCategoryID.Tag = "Office Category";
            this.lueOfficeCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEditOffice_Validating);
            // 
            // spnEngineSize
            // 
            this.spnEngineSize.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "EngineSize", true));
            this.spnEngineSize.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnEngineSize.Location = new System.Drawing.Point(123, 143);
            this.spnEngineSize.Name = "spnEngineSize";
            this.spnEngineSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnEngineSize.Size = new System.Drawing.Size(1000, 20);
            this.spnEngineSize.StyleController = this.dataLayoutControl1;
            this.spnEngineSize.TabIndex = 27;
            this.spnEngineSize.Tag = "Engine Size";
            this.spnEngineSize.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditPlant_Validating);
            // 
            // dePUWERDate
            // 
            this.dePUWERDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "PUWERDate", true));
            this.dePUWERDate.EditValue = null;
            this.dePUWERDate.Location = new System.Drawing.Point(123, 191);
            this.dePUWERDate.Name = "dePUWERDate";
            this.dePUWERDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dePUWERDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dePUWERDate.Size = new System.Drawing.Size(1000, 20);
            this.dePUWERDate.StyleController = this.dataLayoutControl1;
            this.dePUWERDate.TabIndex = 26;
            this.dePUWERDate.Tag = "PUWER Date";
            // 
            // deLOLERDate
            // 
            this.deLOLERDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "LOLERDate", true));
            this.deLOLERDate.EditValue = null;
            this.deLOLERDate.Location = new System.Drawing.Point(123, 167);
            this.deLOLERDate.Name = "deLOLERDate";
            this.deLOLERDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deLOLERDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deLOLERDate.Size = new System.Drawing.Size(1000, 20);
            this.deLOLERDate.StyleController = this.dataLayoutControl1;
            this.deLOLERDate.TabIndex = 25;
            this.deLOLERDate.Tag = "LOLER Date";
            // 
            // luePlantFuelTypeID
            // 
            this.luePlantFuelTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "FuelTypeID", true));
            this.luePlantFuelTypeID.Location = new System.Drawing.Point(123, 119);
            this.luePlantFuelTypeID.Name = "luePlantFuelTypeID";
            this.luePlantFuelTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePlantFuelTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Fuel Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.luePlantFuelTypeID.Properties.DataSource = this.spAS11000PLFuelBindingSource;
            this.luePlantFuelTypeID.Properties.DisplayMember = "Value";
            this.luePlantFuelTypeID.Properties.NullText = "";
            this.luePlantFuelTypeID.Properties.NullValuePrompt = "-- Please Select --";
            this.luePlantFuelTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.luePlantFuelTypeID.Properties.ValueMember = "PickListID";
            this.luePlantFuelTypeID.Size = new System.Drawing.Size(1000, 20);
            this.luePlantFuelTypeID.StyleController = this.dataLayoutControl1;
            this.luePlantFuelTypeID.TabIndex = 24;
            this.luePlantFuelTypeID.Tag = "Fuel Type";
            this.luePlantFuelTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEditPlant_Validating);
            // 
            // luePlantCategoryID
            // 
            this.luePlantCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "PlantCategoryID", true));
            this.luePlantCategoryID.Location = new System.Drawing.Point(123, 95);
            this.luePlantCategoryID.Name = "luePlantCategoryID";
            this.luePlantCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePlantCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Plant Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.luePlantCategoryID.Properties.DataSource = this.spAS11000PLPlantCategoryBindingSource;
            this.luePlantCategoryID.Properties.DisplayMember = "Value";
            this.luePlantCategoryID.Properties.NullText = "";
            this.luePlantCategoryID.Properties.NullValuePrompt = "-- Please Select --";
            this.luePlantCategoryID.Properties.NullValuePromptShowForEmptyValue = true;
            this.luePlantCategoryID.Properties.ValueMember = "PickListID";
            this.luePlantCategoryID.Size = new System.Drawing.Size(1000, 20);
            this.luePlantCategoryID.StyleController = this.dataLayoutControl1;
            this.luePlantCategoryID.TabIndex = 23;
            this.luePlantCategoryID.Tag = "Plant Category";
            this.luePlantCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEditPlant_Validating);
            // 
            // txtShortDescription
            // 
            this.txtShortDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11008PlantItemBindingSource, "ShortDescription", true));
            this.txtShortDescription.Location = new System.Drawing.Point(123, 47);
            this.txtShortDescription.Name = "txtShortDescription";
            this.txtShortDescription.Size = new System.Drawing.Size(1000, 20);
            this.txtShortDescription.StyleController = this.dataLayoutControl1;
            this.txtShortDescription.TabIndex = 22;
            this.txtShortDescription.Tag = "Short Description";
            this.txtShortDescription.Validating += new System.ComponentModel.CancelEventHandler(this.textEditPlant_Validating);
            // 
            // lueVehicleCategoryID
            // 
            this.lueVehicleCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "VehicleCategoryID", true));
            this.lueVehicleCategoryID.Location = new System.Drawing.Point(135, 102);
            this.lueVehicleCategoryID.Name = "lueVehicleCategoryID";
            this.lueVehicleCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueVehicleCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Vehicle Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueVehicleCategoryID.Properties.DataSource = this.spAS11000PLVehicleCategoryBindingSource;
            this.lueVehicleCategoryID.Properties.DisplayMember = "Value";
            this.lueVehicleCategoryID.Properties.NullText = "";
            this.lueVehicleCategoryID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueVehicleCategoryID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueVehicleCategoryID.Properties.ValueMember = "PickListID";
            this.lueVehicleCategoryID.Size = new System.Drawing.Size(976, 20);
            this.lueVehicleCategoryID.StyleController = this.dataLayoutControl1;
            this.lueVehicleCategoryID.TabIndex = 4;
            this.lueVehicleCategoryID.Tag = "Vehicle Category";
            this.lueVehicleCategoryID.EditValueChanged += new System.EventHandler(this.lueVehicleCategoryID_EditValueChanged);
            this.lueVehicleCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEditVehicle_Validating);
            // 
            // txtRegistrationPlate
            // 
            this.txtRegistrationPlate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "RegistrationPlate", true));
            this.txtRegistrationPlate.Location = new System.Drawing.Point(135, 78);
            this.txtRegistrationPlate.Name = "txtRegistrationPlate";
            this.txtRegistrationPlate.Size = new System.Drawing.Size(976, 20);
            this.txtRegistrationPlate.StyleController = this.dataLayoutControl1;
            this.txtRegistrationPlate.TabIndex = 5;
            this.txtRegistrationPlate.Tag = "Registration Plate";
            this.txtRegistrationPlate.Validating += new System.ComponentModel.CancelEventHandler(this.textEditVehicle_Validating);
            // 
            // txtLogBookNumber
            // 
            this.txtLogBookNumber.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "LogBookNumber", true));
            this.txtLogBookNumber.Location = new System.Drawing.Point(135, 126);
            this.txtLogBookNumber.Name = "txtLogBookNumber";
            this.txtLogBookNumber.Size = new System.Drawing.Size(976, 20);
            this.txtLogBookNumber.StyleController = this.dataLayoutControl1;
            this.txtLogBookNumber.TabIndex = 6;
            this.txtLogBookNumber.Tag = "Log Book Number";
            this.txtLogBookNumber.Validating += new System.ComponentModel.CancelEventHandler(this.textEditVehicle_Validating);
            // 
            // txtModelSpecifications
            // 
            this.txtModelSpecifications.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "ModelSpecifications", true));
            this.txtModelSpecifications.EditValue = "None";
            this.txtModelSpecifications.Location = new System.Drawing.Point(135, 308);
            this.txtModelSpecifications.Name = "txtModelSpecifications";
            this.txtModelSpecifications.Size = new System.Drawing.Size(428, 20);
            this.txtModelSpecifications.StyleController = this.dataLayoutControl1;
            this.txtModelSpecifications.TabIndex = 7;
            this.txtModelSpecifications.Tag = "Model Specifications";
            // 
            // spnVehicleEngineSize
            // 
            this.spnVehicleEngineSize.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "EngineSize", true));
            this.spnVehicleEngineSize.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnVehicleEngineSize.Location = new System.Drawing.Point(135, 193);
            this.spnVehicleEngineSize.Name = "spnVehicleEngineSize";
            this.spnVehicleEngineSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnVehicleEngineSize.Size = new System.Drawing.Size(976, 20);
            this.spnVehicleEngineSize.StyleController = this.dataLayoutControl1;
            this.spnVehicleEngineSize.TabIndex = 8;
            this.spnVehicleEngineSize.Tag = "Engine Size";
            this.spnVehicleEngineSize.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditVehicle_Validating);
            // 
            // lueVehicleFuelTypeID
            // 
            this.lueVehicleFuelTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "FuelTypeID", true));
            this.lueVehicleFuelTypeID.Location = new System.Drawing.Point(135, 217);
            this.lueVehicleFuelTypeID.Name = "lueVehicleFuelTypeID";
            this.lueVehicleFuelTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueVehicleFuelTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Fuel Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueVehicleFuelTypeID.Properties.DataSource = this.spAS11000PLFuelBindingSource;
            this.lueVehicleFuelTypeID.Properties.DisplayMember = "Value";
            this.lueVehicleFuelTypeID.Properties.NullText = "";
            this.lueVehicleFuelTypeID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueVehicleFuelTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueVehicleFuelTypeID.Properties.ValueMember = "PickListID";
            this.lueVehicleFuelTypeID.Size = new System.Drawing.Size(976, 20);
            this.lueVehicleFuelTypeID.StyleController = this.dataLayoutControl1;
            this.lueVehicleFuelTypeID.TabIndex = 9;
            this.lueVehicleFuelTypeID.Tag = "Fuel Type";
            this.lueVehicleFuelTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEditVehicle_Validating);
            // 
            // spnEmissions
            // 
            this.spnEmissions.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "Emissions", true));
            this.spnEmissions.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnEmissions.Location = new System.Drawing.Point(135, 241);
            this.spnEmissions.Name = "spnEmissions";
            this.spnEmissions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnEmissions.Size = new System.Drawing.Size(976, 20);
            this.spnEmissions.StyleController = this.dataLayoutControl1;
            this.spnEmissions.TabIndex = 10;
            this.spnEmissions.Tag = "Emissions";
            this.spnEmissions.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditVehicle_Validating);
            // 
            // spnTotalTyreNumber
            // 
            this.spnTotalTyreNumber.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "TotalTyreNumber", true));
            this.spnTotalTyreNumber.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnTotalTyreNumber.Location = new System.Drawing.Point(135, 332);
            this.spnTotalTyreNumber.Name = "spnTotalTyreNumber";
            this.spnTotalTyreNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnTotalTyreNumber.Size = new System.Drawing.Size(428, 20);
            this.spnTotalTyreNumber.StyleController = this.dataLayoutControl1;
            this.spnTotalTyreNumber.TabIndex = 11;
            this.spnTotalTyreNumber.Tag = "Total Tyre Number";
            this.spnTotalTyreNumber.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditVehicle_Validating);
            // 
            // lueTowbarTypeID
            // 
            this.lueTowbarTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "TowbarTypeID", true));
            this.lueTowbarTypeID.Location = new System.Drawing.Point(135, 356);
            this.lueTowbarTypeID.Name = "lueTowbarTypeID";
            this.lueTowbarTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTowbarTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Towbar Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueTowbarTypeID.Properties.DataSource = this.spAS11000PLVehicleTowbarTypeBindingSource;
            this.lueTowbarTypeID.Properties.DisplayMember = "Value";
            this.lueTowbarTypeID.Properties.NullText = "";
            this.lueTowbarTypeID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueTowbarTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueTowbarTypeID.Properties.ValueMember = "PickListID";
            this.lueTowbarTypeID.Size = new System.Drawing.Size(428, 20);
            this.lueTowbarTypeID.StyleController = this.dataLayoutControl1;
            this.lueTowbarTypeID.TabIndex = 12;
            this.lueTowbarTypeID.Tag = "Towbar Type";
            this.lueTowbarTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEditVehicle_Validating);
            // 
            // cpeColour
            // 
            this.cpeColour.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "Colour", true));
            this.cpeColour.EditValue = System.Drawing.Color.Empty;
            this.cpeColour.Location = new System.Drawing.Point(666, 308);
            this.cpeColour.Name = "cpeColour";
            this.cpeColour.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cpeColour.Properties.ShowColorDialog = false;
            this.cpeColour.Properties.ShowCustomColors = false;
            this.cpeColour.Properties.ShowSystemColors = false;
            this.cpeColour.Size = new System.Drawing.Size(445, 20);
            this.cpeColour.StyleController = this.dataLayoutControl1;
            this.cpeColour.TabIndex = 13;
            this.cpeColour.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.cpeColour_EditValueChanging);
            // 
            // deRegistrationDate
            // 
            this.deRegistrationDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "RegistrationDate", true));
            this.deRegistrationDate.EditValue = null;
            this.deRegistrationDate.Location = new System.Drawing.Point(666, 332);
            this.deRegistrationDate.Name = "deRegistrationDate";
            this.deRegistrationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deRegistrationDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deRegistrationDate.Size = new System.Drawing.Size(445, 20);
            this.deRegistrationDate.StyleController = this.dataLayoutControl1;
            this.deRegistrationDate.TabIndex = 14;
            this.deRegistrationDate.Tag = "Registration Date";
            this.deRegistrationDate.EditValueChanged += new System.EventHandler(this.deRegistrationDate_EditValueChanged);
            this.deRegistrationDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // deMOTDueDate
            // 
            this.deMOTDueDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "MOTDueDate", true));
            this.deMOTDueDate.EditValue = null;
            this.deMOTDueDate.Location = new System.Drawing.Point(135, 471);
            this.deMOTDueDate.Name = "deMOTDueDate";
            this.deMOTDueDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deMOTDueDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deMOTDueDate.Size = new System.Drawing.Size(415, 20);
            this.deMOTDueDate.StyleController = this.dataLayoutControl1;
            this.deMOTDueDate.TabIndex = 15;
            this.deMOTDueDate.Tag = "MOT Due Date";
            this.deMOTDueDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // spnCurrentMileage
            // 
            this.spnCurrentMileage.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "CurrentMileage", true));
            this.spnCurrentMileage.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnCurrentMileage.Location = new System.Drawing.Point(135, 447);
            this.spnCurrentMileage.Name = "spnCurrentMileage";
            this.spnCurrentMileage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnCurrentMileage.Size = new System.Drawing.Size(415, 20);
            this.spnCurrentMileage.StyleController = this.dataLayoutControl1;
            this.spnCurrentMileage.TabIndex = 16;
            this.spnCurrentMileage.Tag = "Current Mileage";
            this.spnCurrentMileage.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditVehicle_Validating);
            // 
            // spnServiceDueMileage
            // 
            this.spnServiceDueMileage.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "ServiceDueMileage", true));
            this.spnServiceDueMileage.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnServiceDueMileage.Location = new System.Drawing.Point(135, 423);
            this.spnServiceDueMileage.Name = "spnServiceDueMileage";
            this.spnServiceDueMileage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnServiceDueMileage.Size = new System.Drawing.Size(415, 20);
            this.spnServiceDueMileage.StyleController = this.dataLayoutControl1;
            this.spnServiceDueMileage.TabIndex = 17;
            this.spnServiceDueMileage.Tag = "Service Due Mileage";
            this.spnServiceDueMileage.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditVehicle_Validating);
            // 
            // txtRadioCode
            // 
            this.txtRadioCode.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "RadioCode", true));
            this.txtRadioCode.Location = new System.Drawing.Point(677, 471);
            this.txtRadioCode.Name = "txtRadioCode";
            this.txtRadioCode.Size = new System.Drawing.Size(434, 20);
            this.txtRadioCode.StyleController = this.dataLayoutControl1;
            this.txtRadioCode.TabIndex = 18;
            this.txtRadioCode.Tag = "Radio Code";
            // 
            // txtKeyCode
            // 
            this.txtKeyCode.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "KeyCode", true));
            this.txtKeyCode.Location = new System.Drawing.Point(677, 447);
            this.txtKeyCode.Name = "txtKeyCode";
            this.txtKeyCode.Size = new System.Drawing.Size(434, 20);
            this.txtKeyCode.StyleController = this.dataLayoutControl1;
            this.txtKeyCode.TabIndex = 19;
            this.txtKeyCode.Tag = "Key Code";
            // 
            // txtElectronicCode
            // 
            this.txtElectronicCode.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11005VehicleItemBindingSource, "ElectronicCode", true));
            this.txtElectronicCode.Location = new System.Drawing.Point(677, 423);
            this.txtElectronicCode.Name = "txtElectronicCode";
            this.txtElectronicCode.Size = new System.Drawing.Size(434, 20);
            this.txtElectronicCode.StyleController = this.dataLayoutControl1;
            this.txtElectronicCode.TabIndex = 20;
            this.txtElectronicCode.Tag = "Electronic Code";
            // 
            // memDescription
            // 
            this.memDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11029OfficeItemBindingSource, "Description", true));
            this.memDescription.Location = new System.Drawing.Point(36, 126);
            this.memDescription.Name = "memDescription";
            this.memDescription.Size = new System.Drawing.Size(1075, 365);
            this.memDescription.StyleController = this.dataLayoutControl1;
            this.memDescription.TabIndex = 31;
            this.memDescription.Tag = "Item Description";
            this.memDescription.Validating += new System.ComponentModel.CancelEventHandler(this.memDescription_Validating);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1147, 790);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1127, 770);
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.OptionsCustomization.AllowDrag = DevExpress.XtraLayout.ItemDragDropMode.Disable;
            this.tabbedControlGroup2.OptionsCustomization.AllowDrop = DevExpress.XtraLayout.ItemDragDropMode.Disable;
            this.tabbedControlGroup2.SelectedTabPage = this.vehicleLayoutControlGroup;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(1127, 507);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.vehicleLayoutControlGroup,
            this.plantLayoutControlGroup,
            this.officeLayoutControlGroup,
            this.NoDetailsLayoutControlGroup});
            // 
            // plantLayoutControlGroup
            // 
            this.plantLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlItem13});
            this.plantLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.plantLayoutControlGroup.Name = "plantLayoutControlGroup";
            this.plantLayoutControlGroup.Size = new System.Drawing.Size(1103, 460);
            this.plantLayoutControlGroup.Text = "Plant and Machinery Detail";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtShortDescription;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1103, 24);
            this.layoutControlItem3.Text = "Short Description ";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.luePlantFuelTypeID;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1103, 24);
            this.layoutControlItem5.Text = "Fuel Type ";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.deLOLERDate;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(1103, 24);
            this.layoutControlItem6.Text = "LOLERDate ";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.luePlantCategoryID;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1103, 24);
            this.layoutControlItem4.Text = "Plant Category ";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.spnEngineSize;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(1103, 24);
            this.layoutControlItem8.Text = "Engine Size ";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.dePUWERDate;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(1103, 316);
            this.layoutControlItem7.Text = "PUWERDate ";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtPlantRegistrationPlate;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(1103, 24);
            this.layoutControlItem13.Text = "Registration Plate:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(96, 13);
            // 
            // vehicleLayoutControlGroup
            // 
            this.vehicleLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup13,
            this.layoutControlGroup12,
            this.layoutControlGroup11,
            this.layoutControlGroup10});
            this.vehicleLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.vehicleLayoutControlGroup.Name = "vehicleLayoutControlGroup";
            this.vehicleLayoutControlGroup.Size = new System.Drawing.Size(1103, 460);
            this.vehicleLayoutControlGroup.Text = "Vehicle Detail";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRegistrationPlate,
            this.layoutControlItem12,
            this.ItemForLogBookNumber});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1103, 115);
            this.layoutControlGroup9.Text = "Registration Details";
            // 
            // ItemForRegistrationPlate
            // 
            this.ItemForRegistrationPlate.Control = this.txtRegistrationPlate;
            this.ItemForRegistrationPlate.Location = new System.Drawing.Point(0, 0);
            this.ItemForRegistrationPlate.Name = "ItemForRegistrationPlate";
            this.ItemForRegistrationPlate.Size = new System.Drawing.Size(1079, 24);
            this.ItemForRegistrationPlate.Text = "Registration Plate";
            this.ItemForRegistrationPlate.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.lueVehicleCategoryID;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(1079, 24);
            this.layoutControlItem12.Text = "Vehicle Category ";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForLogBookNumber
            // 
            this.ItemForLogBookNumber.Control = this.txtLogBookNumber;
            this.ItemForLogBookNumber.Location = new System.Drawing.Point(0, 48);
            this.ItemForLogBookNumber.Name = "ItemForLogBookNumber";
            this.ItemForLogBookNumber.Size = new System.Drawing.Size(1079, 24);
            this.ItemForLogBookNumber.Text = "Log Book Number";
            this.ItemForLogBookNumber.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRadioCode,
            this.ItemForKeyCode,
            this.ItemForElectronicCode});
            this.layoutControlGroup13.Location = new System.Drawing.Point(542, 345);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(561, 115);
            this.layoutControlGroup13.Text = "Vehicle Codes";
            // 
            // ItemForRadioCode
            // 
            this.ItemForRadioCode.Control = this.txtRadioCode;
            this.ItemForRadioCode.Location = new System.Drawing.Point(0, 48);
            this.ItemForRadioCode.Name = "ItemForRadioCode";
            this.ItemForRadioCode.Size = new System.Drawing.Size(537, 24);
            this.ItemForRadioCode.Text = "Radio Code";
            this.ItemForRadioCode.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForKeyCode
            // 
            this.ItemForKeyCode.Control = this.txtKeyCode;
            this.ItemForKeyCode.Location = new System.Drawing.Point(0, 24);
            this.ItemForKeyCode.Name = "ItemForKeyCode";
            this.ItemForKeyCode.Size = new System.Drawing.Size(537, 24);
            this.ItemForKeyCode.Text = "Key Code";
            this.ItemForKeyCode.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForElectronicCode
            // 
            this.ItemForElectronicCode.Control = this.txtElectronicCode;
            this.ItemForElectronicCode.Location = new System.Drawing.Point(0, 0);
            this.ItemForElectronicCode.Name = "ItemForElectronicCode";
            this.ItemForElectronicCode.Size = new System.Drawing.Size(537, 24);
            this.ItemForElectronicCode.Text = "Electronic Code";
            this.ItemForElectronicCode.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForMOTDueDate,
            this.ItemForCurrentMileage,
            this.ItemForServiceDueMileage});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 345);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(542, 115);
            this.layoutControlGroup12.Text = "Maintenance Details";
            // 
            // ItemForMOTDueDate
            // 
            this.ItemForMOTDueDate.Control = this.deMOTDueDate;
            this.ItemForMOTDueDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForMOTDueDate.Name = "ItemForMOTDueDate";
            this.ItemForMOTDueDate.Size = new System.Drawing.Size(518, 24);
            this.ItemForMOTDueDate.Text = "MOT Due Date";
            this.ItemForMOTDueDate.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForCurrentMileage
            // 
            this.ItemForCurrentMileage.Control = this.spnCurrentMileage;
            this.ItemForCurrentMileage.Location = new System.Drawing.Point(0, 24);
            this.ItemForCurrentMileage.Name = "ItemForCurrentMileage";
            this.ItemForCurrentMileage.Size = new System.Drawing.Size(518, 24);
            this.ItemForCurrentMileage.Text = "Current Mileage";
            this.ItemForCurrentMileage.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForServiceDueMileage
            // 
            this.ItemForServiceDueMileage.Control = this.spnServiceDueMileage;
            this.ItemForServiceDueMileage.Location = new System.Drawing.Point(0, 0);
            this.ItemForServiceDueMileage.Name = "ItemForServiceDueMileage";
            this.ItemForServiceDueMileage.Size = new System.Drawing.Size(518, 24);
            this.ItemForServiceDueMileage.Text = "Service Due Mileage";
            this.ItemForServiceDueMileage.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForModelSpecifications,
            this.ItemForTotalTyreNumber,
            this.ItemForColour,
            this.ItemForRegistrationDate,
            this.ItemForTowbarTypeID});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 230);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(1103, 115);
            this.layoutControlGroup11.Text = "Vehicle Specification";
            // 
            // ItemForModelSpecifications
            // 
            this.ItemForModelSpecifications.Control = this.txtModelSpecifications;
            this.ItemForModelSpecifications.Location = new System.Drawing.Point(0, 0);
            this.ItemForModelSpecifications.Name = "ItemForModelSpecifications";
            this.ItemForModelSpecifications.Size = new System.Drawing.Size(531, 24);
            this.ItemForModelSpecifications.Text = "Model Specifications";
            this.ItemForModelSpecifications.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForTotalTyreNumber
            // 
            this.ItemForTotalTyreNumber.Control = this.spnTotalTyreNumber;
            this.ItemForTotalTyreNumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForTotalTyreNumber.Name = "ItemForTotalTyreNumber";
            this.ItemForTotalTyreNumber.Size = new System.Drawing.Size(531, 24);
            this.ItemForTotalTyreNumber.Text = "Total Tyre Number";
            this.ItemForTotalTyreNumber.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForColour
            // 
            this.ItemForColour.Control = this.cpeColour;
            this.ItemForColour.Location = new System.Drawing.Point(531, 0);
            this.ItemForColour.Name = "ItemForColour";
            this.ItemForColour.Size = new System.Drawing.Size(548, 24);
            this.ItemForColour.Text = "Colour";
            this.ItemForColour.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForRegistrationDate
            // 
            this.ItemForRegistrationDate.Control = this.deRegistrationDate;
            this.ItemForRegistrationDate.Location = new System.Drawing.Point(531, 24);
            this.ItemForRegistrationDate.Name = "ItemForRegistrationDate";
            this.ItemForRegistrationDate.Size = new System.Drawing.Size(548, 48);
            this.ItemForRegistrationDate.Text = "Registration Date";
            this.ItemForRegistrationDate.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForTowbarTypeID
            // 
            this.ItemForTowbarTypeID.Control = this.lueTowbarTypeID;
            this.ItemForTowbarTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForTowbarTypeID.Name = "ItemForTowbarTypeID";
            this.ItemForTowbarTypeID.Size = new System.Drawing.Size(531, 24);
            this.ItemForTowbarTypeID.Text = "Towbar Type ";
            this.ItemForTowbarTypeID.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmissions,
            this.ItemForEngineSize,
            this.ItemForFuelTypeID});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 115);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(1103, 115);
            this.layoutControlGroup10.Text = "Perfomance Details";
            // 
            // ItemForEmissions
            // 
            this.ItemForEmissions.Control = this.spnEmissions;
            this.ItemForEmissions.Location = new System.Drawing.Point(0, 48);
            this.ItemForEmissions.Name = "ItemForEmissions";
            this.ItemForEmissions.Size = new System.Drawing.Size(1079, 24);
            this.ItemForEmissions.Text = "Emissions";
            this.ItemForEmissions.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForEngineSize
            // 
            this.ItemForEngineSize.Control = this.spnVehicleEngineSize;
            this.ItemForEngineSize.Location = new System.Drawing.Point(0, 0);
            this.ItemForEngineSize.Name = "ItemForEngineSize";
            this.ItemForEngineSize.Size = new System.Drawing.Size(1079, 24);
            this.ItemForEngineSize.Text = "Engine Size";
            this.ItemForEngineSize.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForFuelTypeID
            // 
            this.ItemForFuelTypeID.Control = this.lueVehicleFuelTypeID;
            this.ItemForFuelTypeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForFuelTypeID.Name = "ItemForFuelTypeID";
            this.ItemForFuelTypeID.Size = new System.Drawing.Size(1079, 24);
            this.ItemForFuelTypeID.Text = "Fuel Type ";
            this.ItemForFuelTypeID.TextSize = new System.Drawing.Size(96, 13);
            // 
            // officeLayoutControlGroup
            // 
            this.officeLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlGroup14});
            this.officeLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.officeLayoutControlGroup.Name = "officeLayoutControlGroup";
            this.officeLayoutControlGroup.Size = new System.Drawing.Size(1103, 460);
            this.officeLayoutControlGroup.Text = "Office Equipment Detail";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.lueOfficeCategoryID;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(1103, 24);
            this.layoutControlItem9.Text = "Office Category ";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.spnQuantity;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(1103, 24);
            this.layoutControlItem10.Text = "Quantity ";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14});
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 48);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(1103, 412);
            this.layoutControlGroup14.Text = "Item Description";
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.memDescription;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(1079, 369);
            this.layoutControlItem14.Text = "Description:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // NoDetailsLayoutControlGroup
            // 
            this.NoDetailsLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.NoDetailsLayoutControlGroup.Name = "NoDetailsLayoutControlGroup";
            this.NoDetailsLayoutControlGroup.Size = new System.Drawing.Size(1103, 460);
            this.NoDetailsLayoutControlGroup.Text = "No Details";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 507);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1127, 263);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(40, 40, 0, 40);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1255, 905);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.CustomizationFormText = "Click Install to begin installation";
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 852);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 0, 0, 0);
            this.simpleLabelItem2.Size = new System.Drawing.Size(1175, 13);
            this.simpleLabelItem2.Text = "Click Next to continue";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(148, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.lblPageTitle});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1175, 852);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataLayoutControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1151, 794);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // lblPageTitle
            // 
            this.lblPageTitle.AllowHotTrack = false;
            this.lblPageTitle.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblPageTitle.AppearanceItemCaption.Options.UseFont = true;
            this.lblPageTitle.CustomizationFormText = "Installation progress:";
            this.lblPageTitle.Location = new System.Drawing.Point(0, 0);
            this.lblPageTitle.Name = "lblPageTitle";
            this.lblPageTitle.Size = new System.Drawing.Size(1151, 34);
            this.lblPageTitle.Text = "Adding Vehicle:";
            this.lblPageTitle.TextSize = new System.Drawing.Size(148, 30);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lueOfficeCategoryID;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1025, 24);
            this.layoutControlItem2.Text = "Office Category ";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForVehicleCategoryID
            // 
            this.ItemForVehicleCategoryID.Control = this.lueVehicleCategoryID;
            this.ItemForVehicleCategoryID.Location = new System.Drawing.Point(0, 0);
            this.ItemForVehicleCategoryID.Name = "ItemForVehicleCategoryID";
            this.ItemForVehicleCategoryID.Size = new System.Drawing.Size(520, 24);
            this.ItemForVehicleCategoryID.Text = "Vehicle Category ";
            this.ItemForVehicleCategoryID.TextSize = new System.Drawing.Size(96, 13);
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // ucStep2Page
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Office 2013";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ucStep2Page";
            this.Size = new System.Drawing.Size(1255, 905);
            this.Load += new System.EventHandler(this.ucStep2Page_Load);
            this.Enter += new System.EventHandler(this.ucStep2Page_Enter);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.ucStep2Page_Validating);
            this.Validated += new System.EventHandler(this.ucStep2Page_Validated);
            ((System.ComponentModel.ISupportInitialize)(this.spAS11029OfficeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLOfficeCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11008PlantItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLFuelBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLPlantCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11005VehicleItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLVehicleCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLVehicleTowbarTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11035_Software_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11032_Hardware_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_AS_11026_Gadget_ItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPlantRegistrationPlate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueOfficeCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnEngineSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dePUWERDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dePUWERDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLOLERDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLOLERDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePlantFuelTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePlantCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShortDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueVehicleCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRegistrationPlate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogBookNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModelSpecifications.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnVehicleEngineSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueVehicleFuelTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnEmissions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnTotalTyreNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTowbarTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpeColour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRegistrationDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deRegistrationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deMOTDueDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deMOTDueDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnCurrentMileage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnServiceDueMileage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRadioCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKeyCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtElectronicCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegistrationPlate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLogBookNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRadioCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeyCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForElectronicCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMOTDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCurrentMileage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForServiceDueMileage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModelSpecifications)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalTyreNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForColour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegistrationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTowbarTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmissions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEngineSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFuelTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoDetailsLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVehicleCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem lblPageTitle;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.LookUpEdit lueVehicleCategoryID;
        private System.Windows.Forms.BindingSource spAS11005VehicleItemBindingSource;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraEditors.TextEdit txtRegistrationPlate;
        private DevExpress.XtraEditors.TextEdit txtLogBookNumber;
        private DevExpress.XtraEditors.TextEdit txtModelSpecifications;
        private DevExpress.XtraEditors.SpinEdit spnVehicleEngineSize;
        private DevExpress.XtraEditors.LookUpEdit lueVehicleFuelTypeID;
        private DevExpress.XtraEditors.SpinEdit spnEmissions;
        private DevExpress.XtraEditors.SpinEdit spnTotalTyreNumber;
        private DevExpress.XtraEditors.LookUpEdit lueTowbarTypeID;
        private DevExpress.XtraEditors.ColorEdit cpeColour;
        private DevExpress.XtraEditors.DateEdit deRegistrationDate;
        private DevExpress.XtraEditors.DateEdit deMOTDueDate;
        private DevExpress.XtraEditors.SpinEdit spnCurrentMileage;
        private DevExpress.XtraEditors.SpinEdit spnServiceDueMileage;
        private DevExpress.XtraEditors.TextEdit txtRadioCode;
        private DevExpress.XtraEditors.TextEdit txtKeyCode;
        private DevExpress.XtraEditors.TextEdit txtElectronicCode;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource spAS11008PlantItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11008_Plant_ItemTableAdapter sp_AS_11008_Plant_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11029OfficeItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11029_Office_ItemTableAdapter sp_AS_11029_Office_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter sp_AS_11005_Vehicle_ItemTableAdapter;
        private DevExpress.XtraEditors.SpinEdit spnEngineSize;
        private DevExpress.XtraEditors.DateEdit dePUWERDate;
        private DevExpress.XtraEditors.DateEdit deLOLERDate;
        private DevExpress.XtraEditors.LookUpEdit luePlantFuelTypeID;
        private DevExpress.XtraEditors.LookUpEdit luePlantCategoryID;
        private DevExpress.XtraEditors.TextEdit txtShortDescription;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraEditors.SpinEdit spnQuantity;
        private DevExpress.XtraEditors.LookUpEdit lueOfficeCategoryID;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.MemoEdit memDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVehicleCategoryID;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup vehicleLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem ItemForElectronicCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKeyCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRadioCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForServiceDueMileage;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCurrentMileage;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMOTDueDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegistrationPlate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLogBookNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmissions;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEngineSize;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFuelTypeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForColour;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTowbarTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalTyreNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegistrationDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForModelSpecifications;
        private DevExpress.XtraLayout.LayoutControlGroup plantLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup officeLayoutControlGroup;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlGroup NoDetailsLayoutControlGroup;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private System.Windows.Forms.BindingSource spAS11000DuplicateSearchBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter spAS11000DuplicateSearchTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11035_Software_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11035_Software_ItemTableAdapter sp_AS_11035_Software_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11032_Hardware_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11032_Hardware_ItemTableAdapter sp_AS_11032_Hardware_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp_AS_11026_Gadget_ItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11026_Gadget_ItemTableAdapter sp_AS_11026_Gadget_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLVehicleCategoryBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLFuelBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLVehicleTowbarTypeBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Vehicle_CategoryTableAdapter sp_AS_11000_PL_Vehicle_CategoryTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_FuelTableAdapter sp_AS_11000_PL_FuelTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter sp_AS_11000_PL_Vehicle_Towbar_TypeTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLPlantCategoryBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Plant_CategoryTableAdapter sp_AS_11000_PL_Plant_CategoryTableAdapter;
        private System.Windows.Forms.BindingSource spAS11000PLOfficeCategoryBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Office_CategoryTableAdapter sp_AS_11000_PL_Office_CategoryTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.TextEdit txtPlantRegistrationPlate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
    }
}

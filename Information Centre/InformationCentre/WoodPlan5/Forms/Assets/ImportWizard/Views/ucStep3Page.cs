﻿using DevExpress.XtraEditors;
using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using BaseObjects;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraSplashScreen;

using System.Drawing;
using System.Text;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace WoodPlan5.Forms.Assets.ImportWizard
{
    public partial class ucStep3Page : Views.BaseWizardPage
    {
        public ucStep3Page()
        {
            InitializeComponent();
        }

        #region Declaration

        int intCount = 0;
        public static string strUserName = "";

        public static bool blnStep3Valid = false;
        public enum FormMode { add, edit, view, delete, blockadd, blockedit }; 
        private string strGCRef = "";
        private FormMode mode = FormMode.add;
        public static bool blnTransactionValid = false;
        public static bool blnKeeperValid = false;
        public static bool blnInitialBillingValid = false;
        public static bool blnNotificationValid = false;
        public static bool blnCoverValid = false;
        public static bool blnIncidentValid = false;
        public static bool blnFuelCardValid = false;
        public static bool blnServiceDataValid = false;
        public static bool blnAssetPurposeValid = false;
        public static bool blnWorkOrderValid = false;
        public static bool blnRoadTaxValid = false;

        #endregion
            
        #region Methods


        internal static bool checkValid()
        {
           // checkManuFactureID();
            if (blnKeeperValid && blnInitialBillingValid && blnTransactionValid)
            {
                blnStep3Valid = true;
            }
            else
            {
                blnStep3Valid = false;
            }
            return blnStep3Valid;
        }

        internal static void TransactionValid(bool blnValidationStatus)
        {
            blnTransactionValid = blnValidationStatus;            
        }

        internal static void KeeperValid(bool blnValidationStatus)
        {
            blnKeeperValid = blnValidationStatus;
        }

        internal static void InitialBillingValid(bool blnValidationStatus)
        {
            blnInitialBillingValid = blnValidationStatus;
        }

        internal static void NotificationDetailsValid(bool blnValidationStatus)
        {
            blnNotificationValid = blnValidationStatus;
        }

        internal static void CoverDetailsValid(bool blnValidationStatus)
        {
            blnCoverValid = blnValidationStatus;
        }

        internal static void IncidentDetailsValid(bool blnValidationStatus)
        {
            blnIncidentValid = blnValidationStatus;
        }

        internal static void FuelCardValid(bool blnValidationStatus)
        {
            blnFuelCardValid = blnValidationStatus;
        }

        internal static void ServiceDataValid(bool blnValidationStatus)
        {
            blnServiceDataValid = blnValidationStatus;
        }

        internal static void AssetPurposeValid(bool blnValidationStatus)
        {
            blnAssetPurposeValid = blnValidationStatus;
        }

        internal static void WorkOrderDetailsValid(bool blnValidationStatus)
        {
            blnWorkOrderValid = blnValidationStatus;
        }

        internal static void RoadTaxDetailsValid(bool blnValidationStatus)
        {
            blnRoadTaxValid = blnValidationStatus;
        }

        private void OpenLinkedForm(string strFormTag)
        {
            
            if (mode != FormMode.add)
            {
                XtraMessageBox.Show(string.Format("{0} is restricted.",mode), "Restricted Function.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strGCRef = "";
            const string strFrmCaller = "ucStep3Page";
            frm_Import_Wizard frmParent = (frm_Import_Wizard)this.ParentForm;
            switch (strFormTag)
            {
            	case "TransactionDetails":

                            #region Transaction

                            using (frm_AS_Transactions_Edit tChildForm = new frm_AS_Transactions_Edit() { GlobalSettings = frmParent.GlobalSettings, hasPurchaseRecord = false, hasPriceListRecord = false, strEquipmentIDs = "-1", strRecordIDs = "", strFormMode = (mode.ToString()).ToLower(), formMode = (frm_AS_Transactions_Edit.FormMode)mode, strCaller = strFrmCaller, strGCReference = "", strTransactionDescription = "", intRecordCount = 1, FormPermissions = frmParent.FormPermissions })
                            {
                                TransactionValid(false);
                                tChildForm.Size = new Size(877, 590);
                                tChildForm.StartPosition = FormStartPosition.CenterScreen;
                                tChildForm.ShowDialog();
                            }
                            if (blnTransactionValid)
                            {
                                tiTransactionDetails.Checked = true;
                                tiTransactionDetails.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiTransactionDetails.Checked = false;
                                tiTransactionDetails.AppearanceItem.Normal.BackColor = Color.Red;
                            }
                            #endregion

                    	break;
                case "KeeperDetails":

                            #region Keepers

                        using (frm_AS_Keeper_Edit kChildForm = new frm_AS_Keeper_Edit() { GlobalSettings = frmParent.GlobalSettings, strRecordIDs = "", strEquipmentIDs = "-1" })
                        {
                            ;
                            kChildForm.formMode = (frm_AS_Keeper_Edit.FormMode)mode;
                            kChildForm.strFormMode = (mode.ToString()).ToLower();
                            kChildForm.strCaller = strFrmCaller;
                            kChildForm.strGCReference = strGCRef;
                            kChildForm.intRecordCount = 1;
                            kChildForm.FormPermissions = frmParent.FormPermissions;
                            KeeperValid(false);
                            kChildForm.Size = new Size(877, 590);
                            kChildForm.StartPosition = FormStartPosition.CenterScreen;
                            kChildForm.ShowDialog();
                        }
                            if (blnKeeperValid)
                            {
                                tiKeeperDetails.Checked = true;
                                tiKeeperDetails.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiKeeperDetails.Checked = false;
                                tiKeeperDetails.AppearanceItem.Normal.BackColor = Color.Red;
                            }
                            #endregion

                            break;
                case "InitialBilling":

                            #region Initial Billing

                            using (frm_AS_Billings_Edit bChildForm = new frm_AS_Billings_Edit() { GlobalSettings = frmParent.GlobalSettings, strRecordIDs = "", strEquipmentIDs = "-1", formMode = (frm_AS_Billings_Edit.FormMode)mode, strFormMode = (mode.ToString()).ToLower(), strCaller = strFrmCaller, strGCReference = strGCRef, intRecordCount = 1, FormPermissions = frmParent.FormPermissions })
                            {
                                InitialBillingValid(false);
                                bChildForm.Size = new Size(877, 590);
                                bChildForm.StartPosition = FormStartPosition.CenterScreen;
                                bChildForm.ShowDialog();
                            }
                            
                            if (blnInitialBillingValid)
                            {
                                tiInitialBilling.Checked = true;
                                tiInitialBilling.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiInitialBilling.Checked = false;
                                tiInitialBilling.AppearanceItem.Normal.BackColor = Color.Red;
                            }
                            #endregion

                            break;
                case "NotificationDetails":

                            #region Notifications

                            using (frm_AS_Notification_Edit nChildForm = new frm_AS_Notification_Edit() { GlobalSettings = frmParent.GlobalSettings, strRecordIDs = "", strEquipmentIDs = "-1" })
                            {
                                ;
                                nChildForm.formMode = (frm_AS_Notification_Edit.FormMode)mode;
                                nChildForm.strFormMode = (mode.ToString()).ToLower();
                                nChildForm.strCaller = strFrmCaller;
                                nChildForm.strGCReference = strGCRef;
                                nChildForm.intRecordCount = 1;
                                nChildForm.FormPermissions = frmParent.FormPermissions;
                                NotificationDetailsValid(false);
                                nChildForm.Size = new Size(877, 590);
                                nChildForm.StartPosition = FormStartPosition.CenterScreen;
                                nChildForm.ShowDialog();
                            }
                            if (blnNotificationValid)
                            {
                                tiNotificationDetails.Checked = true;
                                tiNotificationDetails.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiNotificationDetails.Checked = false;
                                tiNotificationDetails.AppearanceItem.Normal.BackColor = Color.Indigo;
                            }
                            #endregion

                            break;
                case "ServiceData":

                            #region Service Data

                            using (frm_AS_Service_Data_Edit sIChildForm = new frm_AS_Service_Data_Edit() { GlobalSettings = frmParent.GlobalSettings, strRecordIDs = "", strEquipmentIDs = "-1" })
                            {
                                ;
                                sIChildForm.formMode = (frm_AS_Service_Data_Edit.FormMode)mode;
                                sIChildForm.strFormMode = (mode.ToString()).ToLower();
                                sIChildForm.strCaller = strFrmCaller;
                                sIChildForm.strGCReference = strGCRef;
                                sIChildForm.intRecordCount = 1;
                                sIChildForm.FormPermissions = frmParent.FormPermissions;
                                ServiceDataValid(false);
                                sIChildForm.Size = new Size(877, 590);
                                sIChildForm.StartPosition = FormStartPosition.CenterScreen;
                                sIChildForm.ShowDialog();
                            }
                            if (blnServiceDataValid)
                            {
                                tiServiceData.Checked = true;
                                tiServiceData.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiServiceData.Checked = false;
                                tiServiceData.AppearanceItem.Normal.BackColor = Color.Indigo;
                            }
                            #endregion

                            break;
                case "CoverDetails":

                            #region Cover

                            using (frm_AS_Cover_Edit cChildForm = new frm_AS_Cover_Edit() { GlobalSettings = frmParent.GlobalSettings, strRecordIDs = "", strEquipmentIDs = "-1" })
                            {
                                ;
                                cChildForm.formMode = (frm_AS_Cover_Edit.FormMode)mode;
                                cChildForm.strFormMode = (mode.ToString()).ToLower();
                                cChildForm.strCaller = strFrmCaller;
                                cChildForm.strGCReference = strGCRef;
                                cChildForm.intRecordCount = 1;
                                cChildForm.FormPermissions = frmParent.FormPermissions;
                                CoverDetailsValid(false);
                                cChildForm.Size = new Size(877, 590);
                                cChildForm.StartPosition = FormStartPosition.CenterScreen;
                                cChildForm.ShowDialog();
                            }
                            if (blnCoverValid)
                            {
                                tiCoverDetails.Checked = true;
                                tiCoverDetails.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiCoverDetails.Checked = false;
                                tiCoverDetails.AppearanceItem.Normal.BackColor = Color.Indigo;
                            }
                            #endregion

                            break;
                case "WorkOrderDetail":

                            #region Work Detail

                            using (frm_AS_WorkDetail_Edit wChildForm = new frm_AS_WorkDetail_Edit() { GlobalSettings = frmParent.GlobalSettings, strRecordIDs = "", strEquipmentIDs = "-1" })
                            {
                                
                                wChildForm.formMode = (frm_AS_WorkDetail_Edit.FormMode)mode;
                                wChildForm.strFormMode = (mode.ToString()).ToLower();
                                wChildForm.strCaller = strFrmCaller;
                                wChildForm.strGCReference = strGCRef;
                                wChildForm.intRecordCount = 1;
                                wChildForm.FormPermissions = frmParent.FormPermissions;
                                WorkOrderDetailsValid(false);
                                wChildForm.Size = new Size(877, 590);
                                wChildForm.StartPosition = FormStartPosition.CenterScreen;
                                wChildForm.ShowDialog();
                            }
                            if (blnWorkOrderValid)
                            {
                                tiWorkOrderDetails.Checked = true;
                                tiWorkOrderDetails.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiWorkOrderDetails.Checked = false;
                                tiWorkOrderDetails.AppearanceItem.Normal.BackColor = Color.Indigo;
                            }
                            #endregion

                            break;
                case "AssetPurpose":

                            #region Asset Purpose

                            using (frm_AS_Purpose_Edit pChildForm = new frm_AS_Purpose_Edit() { GlobalSettings = frmParent.GlobalSettings, strRecordIDs = "", strEquipmentIDs = "-1" })
                            {
                                ;
                                pChildForm.formMode = (frm_AS_Purpose_Edit.FormMode)mode;
                                pChildForm.strFormMode = (mode.ToString()).ToLower();
                                pChildForm.strCaller = strFrmCaller;
                                pChildForm.strGCReference = strGCRef;
                                pChildForm.intRecordCount = 1;
                                pChildForm.FormPermissions = frmParent.FormPermissions;
                                AssetPurposeValid(false);
                                pChildForm.Size = new Size(877, 590);
                                pChildForm.StartPosition = FormStartPosition.CenterScreen;
                                pChildForm.ShowDialog();
                            }
                            if (blnAssetPurposeValid)
                            {
                                tiAssetPurpose.Checked = true;
                                tiAssetPurpose.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiAssetPurpose.Checked = false;
                                tiAssetPurpose.AppearanceItem.Normal.BackColor = Color.Indigo;
                            }
                        #endregion

                            break;
                case "FuelCard":

                            #region Fuel Card

                            int intKeeperAllocationID = 0;
                            if (blnKeeperValid)
                            {
                                if (ucExecutePage.drKeeperNewRowView != null)
                                {
                                    DataRowView currentRow = ucExecutePage.drKeeperNewRowView;
                                    intKeeperAllocationID = Convert.ToInt32(currentRow["KeeperAllocationID"]);
                                }
                            }
                            else
                            {
                                XtraMessageBox.Show("Please Complete the Keeper Details before attempting to complete this form.", "Dependent Form", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                            using (frm_AS_FuelCard_Edit fcChildForm = new frm_AS_FuelCard_Edit() { GlobalSettings = frmParent.GlobalSettings, strRecordIDs = "", strEquipmentIDs = "-1" })
                            {
                                ;
                                fcChildForm.formMode = (frm_AS_FuelCard_Edit.FormMode)mode;
                                fcChildForm.strFormMode = (mode.ToString()).ToLower();
                                fcChildForm.strCaller = strFrmCaller;
                                fcChildForm.strGCReference = strGCRef;
                                fcChildForm.intDefaultKeeperAllocation = intKeeperAllocationID;
                                fcChildForm.intRecordCount = 1;
                                fcChildForm.FormPermissions = frmParent.FormPermissions;
                                FuelCardValid(false);
                                fcChildForm.Size = new Size(877, 590);
                                fcChildForm.StartPosition = FormStartPosition.CenterScreen;
                                fcChildForm.ShowDialog();
                            }
                            if (blnFuelCardValid)
                            {
                                tiFuelCard.Checked = true;
                                tiFuelCard.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiFuelCard.Checked = false;
                                tiFuelCard.AppearanceItem.Normal.BackColor = Color.Indigo;
                            }
                            #endregion

                            break;
                case "RoadTax":

                            #region Road Tax

                            using (frm_AS_Road_Tax_Edit rtChildForm = new frm_AS_Road_Tax_Edit() { GlobalSettings = frmParent.GlobalSettings, strRecordIDs = "", strEquipmentIDs = "-1" })
                            {
                                ;
                                rtChildForm.formMode = (frm_AS_Road_Tax_Edit.FormMode)mode;
                                rtChildForm.strFormMode = (mode.ToString()).ToLower();
                                rtChildForm.strCaller = strFrmCaller;
                                rtChildForm.strGCReference = strGCRef;
                                rtChildForm.intRecordCount = 1;
                                rtChildForm.FormPermissions = frmParent.FormPermissions;
                                RoadTaxDetailsValid(false);
                                rtChildForm.Size = new Size(877, 590);
                                rtChildForm.StartPosition = FormStartPosition.CenterScreen;
                                rtChildForm.ShowDialog();
                            }
                            if (blnRoadTaxValid)
                            {
                                tiRoadTax.Checked = true;
                                tiRoadTax.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiRoadTax.Checked = false;
                                tiRoadTax.AppearanceItem.Normal.BackColor = Color.Indigo;
                            }
                            #endregion

                            break;
                case "IncidentDetails":

                            #region Incident Details

                            using (frm_AS_Incident_Edit iChildForm = new frm_AS_Incident_Edit() { GlobalSettings = frmParent.GlobalSettings, strRecordIDs = "", strEquipmentIDs = "-1" })
                            {
                                ;
                                iChildForm.formMode = (frm_AS_Incident_Edit.FormMode)mode;
                                iChildForm.strFormMode = (mode.ToString()).ToLower();
                                iChildForm.strCaller = strFrmCaller;
                                iChildForm.strGCReference = strGCRef;
                                iChildForm.intRecordCount = 1;
                                iChildForm.FormPermissions = frmParent.FormPermissions;
                                IncidentDetailsValid(false);
                                iChildForm.Size = new Size(877, 590);
                                iChildForm.StartPosition = FormStartPosition.CenterScreen;
                                iChildForm.ShowDialog();
                            }
                            if (blnIncidentValid)
                            {
                                tiIncident.Checked = true;
                                tiIncident.AppearanceItem.Normal.BackColor = Color.LimeGreen;
                            }
                            else
                            {
                                tiIncident.Checked = false;
                                tiIncident.AppearanceItem.Normal.BackColor = Color.Indigo;
                            }
                            #endregion

                            break;               
                default:
                        XtraMessageBox.Show("Tile link not yet available please contact administrator.","Restricted Function.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        break;
            }
        }
        
        #endregion
        
        #region Events


        private void tile_ItemClick(object sender, DevExpress.XtraEditors.TileItemEventArgs e)
        {
            OpenLinkedForm(((TileItem)sender).Tag.ToString());
        }

        #endregion

    }
}
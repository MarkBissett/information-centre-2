﻿namespace WoodPlan5.Forms.Assets.ImportWizard.ViewModels
{
    class Step1PageViewModel : IWizardPageViewModel
    {
        public bool IsComplete
        {
            get { return !string.IsNullOrEmpty(Path) && System.IO.Directory.Exists(Path); }
        }
        public string Path
        {
            get;
            set;
        }
        public bool CanReturn { get { return true; } }

        public bool pageIsValid
        {
            get
            {
                return ucStep1Page.blnStep1Valid;
            }
        }

        
    }
}
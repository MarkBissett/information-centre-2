﻿using System.Windows.Forms;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using System;

namespace WoodPlan5.Forms.Assets.ImportWizard.ViewModels
{
    class WizardViewModel : IWizardViewModel
    {
        WindowsUIView view;
        IWizardPageViewModel[] pages;
        Form mainForm;
        int index = 0;
        public WizardViewModel(IWizardPageViewModel[] pages, WindowsUIView view, Form mainForm)
        {
            this.pages = pages;
            this.view = view;
            this.mainForm = mainForm;
        }
        public bool CanNext()
        {
            return (index >= 0) && (index < pages.Length - 1);// && CurrentPage.IsComplete;
        }
        public void Next()
        { 
            CheckPageValid();
            switch (index)
            {
                case 1:
                    if (WoodPlan5.Forms.Assets.ImportWizard.ucStep1Page.blnStep1Valid )
                    {
                        ActivatePage(++index);
                    }   
                    break;
                case 2:
                    if (WoodPlan5.Forms.Assets.ImportWizard.ucStep2Page.blnStep2Valid)
                    {
                        ActivatePage(++index);
                    } 
                    break;
                case 3:
                    if (WoodPlan5.Forms.Assets.ImportWizard.ucStep3Page.blnStep3Valid)
                    {
                        ucExecutePage.blnOkToFireEvents = true;
                        ActivatePage(++index);
                    }
                    else
                    {
                        ucExecutePage.blnOkToFireEvents = false;
                    }
                    break;
                case 4:
                    if (WoodPlan5.Forms.Assets.ImportWizard.ucExecutePage.blnExecuteValid)
                    {
                        ucExecutePage.blnOkToFireEvents = false;
                        ActivatePage(++index);
                    }
                    break;
                default:
                    if (CurrentPage.pageIsValid)
                    {
                        ActivatePage(++index);
                    } 
                    break;
            }
             
        }
        public bool CanPrev()
        {
            return index > 0 && index < pages.Length && CurrentPage.CanReturn;
        }
        public void Prev()
        {
            ActivatePage(--index);
        }
        public IWizardPageViewModel CurrentPage
        {
            get { return pages[index]; }
        }
        public void PageCompleted()
        {
            CheckPageValid();
            if (CanNext())
                Next();
        }

        private void CheckPageValid()
        {
            string strMessage = "This page is not valid";
            switch (index)
            {
                case 1:
                    ucStep1Page.blnStep1Valid = ucStep1Page.checkValid();//(DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "check Validation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes ? true : false);
                    if (!ucStep1Page.blnStep1Valid)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Please complete required fields then click Next to proceed", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                case 2:
                    ucStep2Page.blnStep2Valid = ucStep2Page.checkValid();
                    if (!ucStep2Page.blnStep2Valid)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Please complete required fields then click Next to proceed", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                case 3:
                    ucStep3Page.blnStep3Valid = ucStep3Page.checkValid(); //(DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "check Validation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes ? true : false);                   
                     if (!ucStep3Page.blnStep3Valid)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Please complete required tiles (red background) then click Next to proceed", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                case 4:
                    ucExecutePage.blnExecuteValid = ucExecutePage.checkValid();
                        //(DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "check Validation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes ? true : false);
                    if (!ucExecutePage.blnExecuteValid)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show(" Please complete required fields\n or Duplicate Manufacturer Identity already exists", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
            }
        }
         
        void ActivatePage(int index)
        {
            PageGroup pageGroup = view.ContentContainers["pageGroup"] as PageGroup;
            view.ActivateDocument(pageGroup.Items[index]);
        }
        public bool CanClose()
        {
            return index >= 0 && index < pages.Length - 1;
        }
        public void Close(bool isClosing)
        {
            if (isClosing)
            {
                Flyout flyout = view.ContentContainers["closeFlyout"] as Flyout;
                flyout.Action = new FlyoutAction()
                {
                    Caption = "Equipment Import Wizard",
                    Description = "Are you sure you want to exit the Equipment Import Wizard?"
                };
                view.FlyoutHidden += view_FlyoutHidden;
                view.ActivateContainer(flyout);
            }
            else Close();
        }
        void view_FlyoutHidden(object sender, FlyoutResultEventArgs e)
        {
            view.FlyoutHidden -= view_FlyoutHidden;
            if (e.Result == System.Windows.Forms.DialogResult.Yes)
                Close();
        }
        void Close()
        {
            mainForm.BeginInvoke(new System.Action(mainForm.Close));
        }

    }
}
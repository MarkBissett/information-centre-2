﻿namespace WoodPlan5.Forms.Assets.ImportWizard.ViewModels
{
    class FinishPageViewModel : IWizardPageViewModel
    {
        public bool IsComplete { get { return true; } }
        public bool CanReturn { get { return false; } }
        public bool pageIsValid
        {
            get
            {
                return true;
            }
        }

        //public bool blnStep2Valid
        //{
        //    get;
        //    set;
        //}
    }
      
}
﻿namespace WoodPlan5.Forms.Assets.ImportWizard.ViewModels
{
    class ExecutePageViewModel : IWizardPageViewModel
    {
        public bool IsComplete
        {
            get;
            set;
        }
        public bool CanReturn { get { return !IsComplete; } }
        
        public bool pageIsValid
        {
            get
            {
                return ucExecutePage.blnExecuteValid;
            }
        }        
    }
}
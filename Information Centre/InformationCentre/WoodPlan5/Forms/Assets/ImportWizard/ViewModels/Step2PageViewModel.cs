﻿namespace WoodPlan5.Forms.Assets.ImportWizard.ViewModels
{
    class Step2PageViewModel : IWizardPageViewModel
    {
        public bool IsComplete
        {
            get;
            set;
        }
        public bool CanReturn { get { return !IsComplete; } }

        public bool pageIsValid
        {
            get
            {
                return blnStep2Valid;
            }
        }

        public bool blnStep2Valid
        {
            get;
            set;
        }
    }
}
﻿namespace WoodPlan5.Forms.Assets.ImportWizard.ViewModels
{
    class Step3PageViewModel : IWizardPageViewModel
    {
        public bool IsComplete
        {
            get;
            set;
        }
        public bool CanReturn { get { return !IsComplete; } }

        public bool pageIsValid
        {
            get
            {
                return blnStep3Valid;
            }
        }

        public bool blnStep3Valid
        {
            get;
            set;
        }
    }
}
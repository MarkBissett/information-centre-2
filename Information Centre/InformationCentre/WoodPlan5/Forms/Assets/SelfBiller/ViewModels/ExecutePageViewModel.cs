﻿namespace WoodPlan5.Forms.Assets.SelfBiller.ViewModels
{
    class ExecutePageViewModel : IWizardPageViewModel
    {
        public bool IsComplete
        {
            get;
            set;
        }
        public bool CanReturn { get { return !IsComplete; } }

        public bool pageIsValid
        {
            get { return true; }
        }
    }
}
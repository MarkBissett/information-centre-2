﻿namespace WoodPlan5.Forms.Assets.SelfBiller.ViewModels
{
    class StartPageViewModel : IWizardPageViewModel
    {
        public bool IsComplete { get { return true; } }
        public bool CanReturn { get { return false; } }


        public bool pageIsValid
        {
            get { return true; }
        }
    }
}
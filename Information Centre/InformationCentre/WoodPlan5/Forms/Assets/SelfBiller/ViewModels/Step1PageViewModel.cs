﻿namespace WoodPlan5.Forms.Assets.SelfBiller.ViewModels
{
    class Step1PageViewModel : IWizardPageViewModel
    {
         public bool IsComplete
        {
            get
            {
                return blnValidEntry;
            }
        }
        public bool blnValidEntry
        {
            get;
            set;
        }

        public bool CanReturn { get { return true; } }

        public bool pageIsValid
        {
            get { return true; }
        }
    }
}
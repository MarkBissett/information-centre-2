﻿using System.Windows.Forms;
using DevExpress.XtraBars.Docking2010.Views.WindowsUI;


namespace WoodPlan5.Forms.Assets.SelfBiller.ViewModels
{
    class WizardViewModel : IWizardViewModel
    {
        WindowsUIView view;
        IWizardPageViewModel[] pages;
        BaseObjects.frmBase mainForm;
  
        int index = 0;
        public WizardViewModel(IWizardPageViewModel[] pages, WindowsUIView view, BaseObjects.frmBase mainForm)
        {
            this.pages = pages;
            this.view = view;
            this.mainForm = mainForm;
            //strSavedPath = strPrevPath;
        }

        string strSavedPath = "";

        public string strPrevSavedPath
        {
            get { return strSavedPath; }
            set
            {
                strSavedPath = value;
            }
        }

        public bool CanNext()
        {
            return (index >= 0) && (index < pages.Length - 1) && CurrentPage.IsComplete;
        }
        public void Next()
        {
            if (view.Caption == "Step 1 of 3 Self Biller Wizard" && strSavedPath !="")
            {
                ((ViewModels.Step2PageViewModel)pages[2]).Path = strSavedPath;
            }
            ActivatePage(++index);
        }
        public bool CanPrev()
        {
            return index > 0 && index < pages.Length && CurrentPage.CanReturn;
        }

        public void Prev()
        {
           ActivatePage(--index);
           if (view.Caption == "Step 2 of 3 Self Biller Wizard")
           {
               ((ViewModels.Step1PageViewModel)pages[1]).blnValidEntry = false;
           }
        }

        public IWizardPageViewModel CurrentPage
        {
            get {
                    return pages[index]; 
                }
        }
        public void PageCompleted()
        {
            if (CanNext())
                Next();
        }
        void ActivatePage(int index)
        {
            PageGroup pageGroup = view.ContentContainers["pageGroup"] as PageGroup;
            view.ActivateDocument(pageGroup.Items[index]);
        }
        public bool CanClose()
        {
            return index >= 0 && index < pages.Length - 1;
        }
        public void Close(bool isClosing)
        {
            if (isClosing)
            {
                Flyout flyout = view.ContentContainers["closeFlyout"] as Flyout;
                flyout.Action = new FlyoutAction()
                {
                    Caption = "Self Biller Wizard",
                    Description = "Are you sure you want to exit the wizard?"
                };
                view.FlyoutHidden += view_FlyoutHidden;
                view.ActivateContainer(flyout);
            }
            else Close();
        }
        void view_FlyoutHidden(object sender, FlyoutResultEventArgs e)
        {
            view.FlyoutHidden -= view_FlyoutHidden;
            if (e.Result == System.Windows.Forms.DialogResult.Yes)
                Close();
        }
        void Close()
        {
            mainForm.BeginInvoke(new System.Action(mainForm.Close));
        }

    }
}
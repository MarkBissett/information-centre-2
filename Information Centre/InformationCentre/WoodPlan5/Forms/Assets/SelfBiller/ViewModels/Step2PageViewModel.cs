﻿namespace WoodPlan5.Forms.Assets.SelfBiller.ViewModels
{
    class Step2PageViewModel : IWizardPageViewModel
    {
        public bool IsComplete
        {
            get { return !string.IsNullOrEmpty(Path) && System.IO.Directory.Exists(Path);  }
        }
        public string Path
        {
            get;
            set;
        }

        public bool CanReturn { get { return true; } }

        public bool pageIsValid
        {
            get { return true; }
        }
    }
}
﻿namespace WoodPlan5.Forms.Assets.SelfBiller.Views
{
    partial class ucStep2Page
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucStep2Page));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtExportFolder = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lblAction = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lblProceed = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.spAS11011EquipmentCategoryListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.spAS11014MakeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11017ModelListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11014_Make_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter();
            this.sp_AS_11011_Equipment_Category_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11011_Equipment_Category_ListTableAdapter();
            this.sp_AS_11017_Model_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.spAS11000DuplicateMakeModelSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtExportFolder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProceed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11011EquipmentCategoryListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11014MakeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateMakeModelSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.txtExportFolder);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1862, 604, 450, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(860, 355);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtExportFolder
            // 
            this.txtExportFolder.Location = new System.Drawing.Point(120, 142);
            this.txtExportFolder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtExportFolder.Name = "txtExportFolder";
            this.txtExportFolder.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.txtExportFolder.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExportFolder.Properties.Appearance.Options.UseFont = true;
            this.txtExportFolder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtExportFolder.Size = new System.Drawing.Size(620, 28);
            this.txtExportFolder.StyleController = this.layoutControl1;
            this.txtExportFolder.TabIndex = 9;
            this.txtExportFolder.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtExportFolder_ButtonClick);
            this.txtExportFolder.EditValueChanged += new System.EventHandler(this.txtExportFolder_EditValueChanged);
            this.txtExportFolder.VisibleChanged += new System.EventHandler(this.txtExportFolder_VisibleChanged);
            this.txtExportFolder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExportFolder_KeyPress);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lblAction,
            this.lblProceed,
            this.emptySpaceItem4,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(40, 40, 0, 40);
            this.layoutControlGroup1.Size = new System.Drawing.Size(860, 355);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lblAction
            // 
            this.lblAction.AllowHotTrack = false;
            this.lblAction.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblAction.AppearanceItemCaption.Options.UseFont = true;
            this.lblAction.CustomizationFormText = "Options:";
            this.lblAction.Location = new System.Drawing.Point(0, 0);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(780, 34);
            this.lblAction.Text = "Output :";
            this.lblAction.TextSize = new System.Drawing.Size(152, 30);
            // 
            // lblProceed
            // 
            this.lblProceed.AllowHotTrack = false;
            this.lblProceed.CustomizationFormText = "Select export folder to continue";
            this.lblProceed.Location = new System.Drawing.Point(0, 302);
            this.lblProceed.Name = "lblProceed";
            this.lblProceed.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 0, 0, 0);
            this.lblProceed.Size = new System.Drawing.Size(780, 13);
            this.lblProceed.Text = "Select export folder to continue";
            this.lblProceed.TextSize = new System.Drawing.Size(152, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 250);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(780, 52);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtExportFolder;
            this.layoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.layoutControlItem1.CustomizationFormText = "Export Folder :";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 80, 80, 80);
            this.layoutControlItem1.Size = new System.Drawing.Size(780, 216);
            this.layoutControlItem1.Text = "Export Folder:";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(152, 25);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("info_16x16.png", "images/support/info_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/info_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "info_16x16.png");
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "Ok_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "refresh_16x16.png");
            this.imageCollection1.InsertGalleryImage("forward_16x16.png", "images/navigation/forward_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/forward_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "forward_16x16.png");
            this.imageCollection1.InsertGalleryImage("next_16x16.png", "images/arrows/next_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/arrows/next_16x16.png"), 7);
            this.imageCollection1.Images.SetKeyName(7, "next_16x16.png");
            // 
            // spAS11011EquipmentCategoryListBindingSource
            // 
            this.spAS11011EquipmentCategoryListBindingSource.DataMember = "sp_AS_11011_Equipment_Category_List";
            this.spAS11011EquipmentCategoryListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spAS11014MakeListBindingSource
            // 
            this.spAS11014MakeListBindingSource.DataMember = "sp_AS_11014_Make_List";
            this.spAS11014MakeListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11017ModelListBindingSource
            // 
            this.spAS11017ModelListBindingSource.DataMember = "sp_AS_11017_Model_List";
            this.spAS11017ModelListBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11014_Make_ListTableAdapter
            // 
            this.sp_AS_11014_Make_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11011_Equipment_Category_ListTableAdapter
            // 
            this.sp_AS_11011_Equipment_Category_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11017_Model_ListTableAdapter
            // 
            this.sp_AS_11017_Model_ListTableAdapter.ClearBeforeFill = true;
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // spAS11000DuplicateMakeModelSearchBindingSource
            // 
            this.spAS11000DuplicateMakeModelSearchBindingSource.DataMember = "sp_AS_11000_Duplicate_Make_Model_Search";
            this.spAS11000DuplicateMakeModelSearchBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.layoutControlItem2.CustomizationFormText = "Installation Folder:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 80, 80, 80);
            this.layoutControlItem2.Size = new System.Drawing.Size(780, 216);
            this.layoutControlItem2.Text = "Export Folder:";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(152, 25);
            this.layoutControlItem2.TrimClientAreaToControl = false;
            // 
            // ucStep2Page
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Office 2013";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ucStep2Page";
            this.Size = new System.Drawing.Size(860, 355);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtExportFolder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProceed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11011EquipmentCategoryListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11014MakeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11017ModelListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateMakeModelSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem lblAction;
        private DevExpress.XtraLayout.SimpleLabelItem lblProceed;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource spAS11014MakeListBindingSource;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private System.Windows.Forms.BindingSource spAS11011EquipmentCategoryListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11014_Make_ListTableAdapter sp_AS_11014_Make_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11011_Equipment_Category_ListTableAdapter sp_AS_11011_Equipment_Category_ListTableAdapter;
        private System.Windows.Forms.BindingSource spAS11017ModelListBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11017_Model_ListTableAdapter sp_AS_11017_Model_ListTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private System.Windows.Forms.BindingSource spAS11000DuplicateMakeModelSearchBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter sp_AS_11000_Duplicate_Make_Model_SearchTableAdapter;
        internal DevExpress.XtraEditors.ButtonEdit txtExportFolder;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}

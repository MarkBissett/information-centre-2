﻿using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Data;
using System;

namespace WoodPlan5.Forms.Assets.SelfBiller.Views
{
    public partial class ucStep2Page : Views.BaseWizardPage
    {
        public ucStep2Page()
        {
            InitializeComponent();
        }

        #region Declaration

        public static bool blnHasSavedPath;   

        #endregion

        #region Events

        private void txtExportFolder_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }


        private void txtExportFolder_VisibleChanged(object sender, EventArgs e)
        {
            try
                {
                    if (blnHasSavedPath && folderBrowserDialog1.SelectedPath == "")
                    {                
                        folderBrowserDialog1.SelectedPath = ucExecutePage.Importfolder;
                        txtExportFolder.Text = ucExecutePage.Importfolder;                
                    }
                }
                catch (Exception)
                {
                    //MessageBox.Show(ex.Message, "Export Path Error");
                }
        }

        private void txtExportFolder_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtExportFolder.EditValue = folderBrowserDialog1.SelectedPath;
            }
        }


        private void txtExportFolder_EditValueChanged(object sender, EventArgs e)
        {
            ((ViewModels.Step2PageViewModel)PageViewModel).Path = txtExportFolder.EditValue as string;
            ucExecutePage.Importfolder = txtExportFolder.EditValue as string;
            WizardViewModel.PageCompleted();
        }


        #endregion
              
        
    }
}
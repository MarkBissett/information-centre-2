﻿namespace WoodPlan5.Forms.Assets.SelfBiller.Views
{
    partial class ucExecutePage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bgWorker = new System.ComponentModel.BackgroundWorker();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.spreadsheetControl = new DevExpress.XtraSpreadsheet.SpreadsheetControl();
            this.lblProgressText = new DevExpress.XtraEditors.LabelControl();
            this.sbInvoicesCheckGridControl = new DevExpress.XtraGrid.GridControl();
            this.sbInvoicesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_SB_DataEntry = new WoodPlan5.DataSet_SB_DataEntry();
            this.sbInvoicesCheckGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSubContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnalysisCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClient1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDirectEmployed1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVAT1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExchequerPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscrepancy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sbInvoicesGridControl = new DevExpress.XtraGrid.GridControl();
            this.sbInvoicesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnalysisCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyItemTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDirectEmployed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCommit = new DevExpress.XtraEditors.SimpleButton();
            this.progressEdit = new DevExpress.XtraEditors.ProgressBarControl();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblConfirm = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lblChange1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sbInvoicesTableAdapter = new WoodPlan5.DataSet_SB_DataEntryTableAdapters.sbInvoicesTableAdapter();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sbInvoicesCheckGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sbInvoicesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_SB_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sbInvoicesCheckGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sbInvoicesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sbInvoicesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyItemTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChange1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // bgWorker
            // 
            this.bgWorker.WorkerReportsProgress = true;
            this.bgWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.bgWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorker_ProgressChanged);
            this.bgWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.spreadsheetControl);
            this.layoutControl1.Controls.Add(this.lblProgressText);
            this.layoutControl1.Controls.Add(this.sbInvoicesCheckGridControl);
            this.layoutControl1.Controls.Add(this.sbInvoicesGridControl);
            this.layoutControl1.Controls.Add(this.btnCommit);
            this.layoutControl1.Controls.Add(this.progressEdit);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem3});
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2282, 108, 450, 804);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(759, 354);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // spreadsheetControl
            // 
            this.spreadsheetControl.Location = new System.Drawing.Point(224, 170);
            this.spreadsheetControl.Name = "spreadsheetControl";
            this.spreadsheetControl.Size = new System.Drawing.Size(153, 101);
            this.spreadsheetControl.TabIndex = 2;
            this.spreadsheetControl.Text = "spreadsheetControl1";
            // 
            // lblProgressText
            // 
            this.lblProgressText.Location = new System.Drawing.Point(126, 152);
            this.lblProgressText.Name = "lblProgressText";
            this.lblProgressText.Size = new System.Drawing.Size(28, 13);
            this.lblProgressText.StyleController = this.layoutControl1;
            this.lblProgressText.TabIndex = 11;
            // 
            // sbInvoicesCheckGridControl
            // 
            this.sbInvoicesCheckGridControl.DataSource = this.sbInvoicesBindingSource;
            this.sbInvoicesCheckGridControl.Location = new System.Drawing.Point(42, 170);
            this.sbInvoicesCheckGridControl.MainView = this.sbInvoicesCheckGridView;
            this.sbInvoicesCheckGridControl.Name = "sbInvoicesCheckGridControl";
            this.sbInvoicesCheckGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.moneyTextEdit});
            this.sbInvoicesCheckGridControl.Size = new System.Drawing.Size(675, 74);
            this.sbInvoicesCheckGridControl.TabIndex = 10;
            this.sbInvoicesCheckGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.sbInvoicesCheckGridView});
            // 
            // sbInvoicesBindingSource
            // 
            this.sbInvoicesBindingSource.DataMember = "sbInvoices";
            this.sbInvoicesBindingSource.DataSource = this.dataSet_SB_DataEntry;
            // 
            // dataSet_SB_DataEntry
            // 
            this.dataSet_SB_DataEntry.DataSetName = "DataSet_SB_DataEntry";
            this.dataSet_SB_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sbInvoicesCheckGridView
            // 
            this.sbInvoicesCheckGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSubContractorID1,
            this.colEmployeeCode1,
            this.colInvoiceDate1,
            this.colInvoiceID1,
            this.colJobCode1,
            this.colAnalysisCode1,
            this.colCostCentre1,
            this.colDepartment1,
            this.colPaid1,
            this.colName1,
            this.colClient1,
            this.colDirectEmployed1,
            this.colVAT1,
            this.colExchequerPaid,
            this.colAccessPaid,
            this.colDiscrepancy});
            this.sbInvoicesCheckGridView.GridControl = this.sbInvoicesCheckGridControl;
            this.sbInvoicesCheckGridView.Name = "sbInvoicesCheckGridView";
            // 
            // colSubContractorID1
            // 
            this.colSubContractorID1.FieldName = "SubContractorID";
            this.colSubContractorID1.MinWidth = 200;
            this.colSubContractorID1.Name = "colSubContractorID1";
            this.colSubContractorID1.Visible = true;
            this.colSubContractorID1.VisibleIndex = 0;
            this.colSubContractorID1.Width = 200;
            // 
            // colEmployeeCode1
            // 
            this.colEmployeeCode1.FieldName = "EmployeeCode";
            this.colEmployeeCode1.MinWidth = 200;
            this.colEmployeeCode1.Name = "colEmployeeCode1";
            this.colEmployeeCode1.Visible = true;
            this.colEmployeeCode1.VisibleIndex = 1;
            this.colEmployeeCode1.Width = 200;
            // 
            // colInvoiceDate1
            // 
            this.colInvoiceDate1.FieldName = "InvoiceDate";
            this.colInvoiceDate1.MinWidth = 200;
            this.colInvoiceDate1.Name = "colInvoiceDate1";
            this.colInvoiceDate1.Visible = true;
            this.colInvoiceDate1.VisibleIndex = 2;
            this.colInvoiceDate1.Width = 200;
            // 
            // colInvoiceID1
            // 
            this.colInvoiceID1.FieldName = "InvoiceID";
            this.colInvoiceID1.MinWidth = 200;
            this.colInvoiceID1.Name = "colInvoiceID1";
            this.colInvoiceID1.Visible = true;
            this.colInvoiceID1.VisibleIndex = 3;
            this.colInvoiceID1.Width = 200;
            // 
            // colJobCode1
            // 
            this.colJobCode1.FieldName = "JobCode";
            this.colJobCode1.MinWidth = 200;
            this.colJobCode1.Name = "colJobCode1";
            this.colJobCode1.Visible = true;
            this.colJobCode1.VisibleIndex = 4;
            this.colJobCode1.Width = 200;
            // 
            // colAnalysisCode1
            // 
            this.colAnalysisCode1.FieldName = "AnalysisCode";
            this.colAnalysisCode1.MinWidth = 200;
            this.colAnalysisCode1.Name = "colAnalysisCode1";
            this.colAnalysisCode1.Visible = true;
            this.colAnalysisCode1.VisibleIndex = 5;
            this.colAnalysisCode1.Width = 200;
            // 
            // colCostCentre1
            // 
            this.colCostCentre1.FieldName = "Cost Centre";
            this.colCostCentre1.MinWidth = 200;
            this.colCostCentre1.Name = "colCostCentre1";
            this.colCostCentre1.Visible = true;
            this.colCostCentre1.VisibleIndex = 6;
            this.colCostCentre1.Width = 200;
            // 
            // colDepartment1
            // 
            this.colDepartment1.FieldName = "Department";
            this.colDepartment1.MinWidth = 200;
            this.colDepartment1.Name = "colDepartment1";
            this.colDepartment1.Visible = true;
            this.colDepartment1.VisibleIndex = 7;
            this.colDepartment1.Width = 200;
            // 
            // colPaid1
            // 
            this.colPaid1.ColumnEdit = this.moneyTextEdit;
            this.colPaid1.FieldName = "Paid";
            this.colPaid1.MinWidth = 200;
            this.colPaid1.Name = "colPaid1";
            this.colPaid1.Visible = true;
            this.colPaid1.VisibleIndex = 8;
            this.colPaid1.Width = 200;
            // 
            // moneyTextEdit
            // 
            this.moneyTextEdit.AutoHeight = false;
            this.moneyTextEdit.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit.Name = "moneyTextEdit";
            // 
            // colName1
            // 
            this.colName1.FieldName = "Name";
            this.colName1.MinWidth = 200;
            this.colName1.Name = "colName1";
            this.colName1.Visible = true;
            this.colName1.VisibleIndex = 9;
            this.colName1.Width = 200;
            // 
            // colClient1
            // 
            this.colClient1.FieldName = "Client";
            this.colClient1.MinWidth = 200;
            this.colClient1.Name = "colClient1";
            this.colClient1.Visible = true;
            this.colClient1.VisibleIndex = 10;
            this.colClient1.Width = 200;
            // 
            // colDirectEmployed1
            // 
            this.colDirectEmployed1.FieldName = "DirectEmployed";
            this.colDirectEmployed1.MinWidth = 200;
            this.colDirectEmployed1.Name = "colDirectEmployed1";
            this.colDirectEmployed1.Visible = true;
            this.colDirectEmployed1.VisibleIndex = 11;
            this.colDirectEmployed1.Width = 200;
            // 
            // colVAT1
            // 
            this.colVAT1.FieldName = "VAT";
            this.colVAT1.MinWidth = 200;
            this.colVAT1.Name = "colVAT1";
            this.colVAT1.Visible = true;
            this.colVAT1.VisibleIndex = 12;
            this.colVAT1.Width = 200;
            // 
            // colExchequerPaid
            // 
            this.colExchequerPaid.ColumnEdit = this.moneyTextEdit;
            this.colExchequerPaid.FieldName = "ExchequerPaid";
            this.colExchequerPaid.MinWidth = 200;
            this.colExchequerPaid.Name = "colExchequerPaid";
            this.colExchequerPaid.Visible = true;
            this.colExchequerPaid.VisibleIndex = 13;
            this.colExchequerPaid.Width = 200;
            // 
            // colAccessPaid
            // 
            this.colAccessPaid.ColumnEdit = this.moneyTextEdit;
            this.colAccessPaid.FieldName = "AccessPaid";
            this.colAccessPaid.MinWidth = 200;
            this.colAccessPaid.Name = "colAccessPaid";
            this.colAccessPaid.Visible = true;
            this.colAccessPaid.VisibleIndex = 14;
            this.colAccessPaid.Width = 200;
            // 
            // colDiscrepancy
            // 
            this.colDiscrepancy.ColumnEdit = this.moneyTextEdit;
            this.colDiscrepancy.FieldName = "Discrepancy";
            this.colDiscrepancy.MinWidth = 200;
            this.colDiscrepancy.Name = "colDiscrepancy";
            this.colDiscrepancy.Visible = true;
            this.colDiscrepancy.VisibleIndex = 15;
            this.colDiscrepancy.Width = 200;
            // 
            // sbInvoicesGridControl
            // 
            this.sbInvoicesGridControl.DataSource = this.sbInvoicesBindingSource;
            this.sbInvoicesGridControl.Location = new System.Drawing.Point(42, 170);
            this.sbInvoicesGridControl.MainView = this.sbInvoicesGridView;
            this.sbInvoicesGridControl.Name = "sbInvoicesGridControl";
            this.sbInvoicesGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.moneyItemTextEdit});
            this.sbInvoicesGridControl.Size = new System.Drawing.Size(335, 101);
            this.sbInvoicesGridControl.TabIndex = 10;
            this.sbInvoicesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.sbInvoicesGridView});
            // 
            // sbInvoicesGridView
            // 
            this.sbInvoicesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSubContractorID,
            this.colEmployeeCode,
            this.colInvoiceDate,
            this.colInvoiceID,
            this.colJobCode,
            this.colAnalysisCode,
            this.colCostCentre,
            this.colDepartment,
            this.colPaid,
            this.colName,
            this.colClient,
            this.colDirectEmployed,
            this.colVAT});
            this.sbInvoicesGridView.GridControl = this.sbInvoicesGridControl;
            this.sbInvoicesGridView.Name = "sbInvoicesGridView";
            this.sbInvoicesGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.MinWidth = 200;
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            this.colSubContractorID.Visible = true;
            this.colSubContractorID.VisibleIndex = 0;
            this.colSubContractorID.Width = 200;
            // 
            // colEmployeeCode
            // 
            this.colEmployeeCode.FieldName = "EmployeeCode";
            this.colEmployeeCode.MinWidth = 200;
            this.colEmployeeCode.Name = "colEmployeeCode";
            this.colEmployeeCode.OptionsColumn.ReadOnly = true;
            this.colEmployeeCode.Visible = true;
            this.colEmployeeCode.VisibleIndex = 1;
            this.colEmployeeCode.Width = 200;
            // 
            // colInvoiceDate
            // 
            this.colInvoiceDate.FieldName = "InvoiceDate";
            this.colInvoiceDate.MinWidth = 200;
            this.colInvoiceDate.Name = "colInvoiceDate";
            this.colInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colInvoiceDate.Visible = true;
            this.colInvoiceDate.VisibleIndex = 2;
            this.colInvoiceDate.Width = 200;
            // 
            // colInvoiceID
            // 
            this.colInvoiceID.FieldName = "InvoiceID";
            this.colInvoiceID.MinWidth = 200;
            this.colInvoiceID.Name = "colInvoiceID";
            this.colInvoiceID.OptionsColumn.ReadOnly = true;
            this.colInvoiceID.Visible = true;
            this.colInvoiceID.VisibleIndex = 3;
            this.colInvoiceID.Width = 200;
            // 
            // colJobCode
            // 
            this.colJobCode.FieldName = "JobCode";
            this.colJobCode.MinWidth = 200;
            this.colJobCode.Name = "colJobCode";
            this.colJobCode.OptionsColumn.ReadOnly = true;
            this.colJobCode.Visible = true;
            this.colJobCode.VisibleIndex = 4;
            this.colJobCode.Width = 200;
            // 
            // colAnalysisCode
            // 
            this.colAnalysisCode.FieldName = "AnalysisCode";
            this.colAnalysisCode.MinWidth = 200;
            this.colAnalysisCode.Name = "colAnalysisCode";
            this.colAnalysisCode.OptionsColumn.ReadOnly = true;
            this.colAnalysisCode.Visible = true;
            this.colAnalysisCode.VisibleIndex = 5;
            this.colAnalysisCode.Width = 200;
            // 
            // colCostCentre
            // 
            this.colCostCentre.FieldName = "Cost Centre";
            this.colCostCentre.MinWidth = 200;
            this.colCostCentre.Name = "colCostCentre";
            this.colCostCentre.OptionsColumn.ReadOnly = true;
            this.colCostCentre.Visible = true;
            this.colCostCentre.VisibleIndex = 6;
            this.colCostCentre.Width = 200;
            // 
            // colDepartment
            // 
            this.colDepartment.FieldName = "Department";
            this.colDepartment.MinWidth = 200;
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.OptionsColumn.ReadOnly = true;
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 7;
            this.colDepartment.Width = 200;
            // 
            // colPaid
            // 
            this.colPaid.ColumnEdit = this.moneyItemTextEdit;
            this.colPaid.DisplayFormat.FormatString = "c2";
            this.colPaid.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colPaid.FieldName = "Paid";
            this.colPaid.MinWidth = 200;
            this.colPaid.Name = "colPaid";
            this.colPaid.OptionsColumn.ReadOnly = true;
            this.colPaid.Visible = true;
            this.colPaid.VisibleIndex = 8;
            this.colPaid.Width = 200;
            // 
            // moneyItemTextEdit
            // 
            this.moneyItemTextEdit.AutoHeight = false;
            this.moneyItemTextEdit.DisplayFormat.FormatString = "c2";
            this.moneyItemTextEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyItemTextEdit.Name = "moneyItemTextEdit";
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.MinWidth = 200;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 9;
            this.colName.Width = 200;
            // 
            // colClient
            // 
            this.colClient.FieldName = "Client";
            this.colClient.MinWidth = 200;
            this.colClient.Name = "colClient";
            this.colClient.OptionsColumn.ReadOnly = true;
            this.colClient.Visible = true;
            this.colClient.VisibleIndex = 10;
            this.colClient.Width = 200;
            // 
            // colDirectEmployed
            // 
            this.colDirectEmployed.FieldName = "DirectEmployed";
            this.colDirectEmployed.MinWidth = 200;
            this.colDirectEmployed.Name = "colDirectEmployed";
            this.colDirectEmployed.OptionsColumn.ReadOnly = true;
            this.colDirectEmployed.Visible = true;
            this.colDirectEmployed.VisibleIndex = 11;
            this.colDirectEmployed.Width = 200;
            // 
            // colVAT
            // 
            this.colVAT.DisplayFormat.FormatString = "c2";
            this.colVAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colVAT.FieldName = "VAT";
            this.colVAT.MinWidth = 200;
            this.colVAT.Name = "colVAT";
            this.colVAT.OptionsColumn.ReadOnly = true;
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 12;
            this.colVAT.Width = 200;
            // 
            // btnCommit
            // 
            this.btnCommit.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.btnCommit.Appearance.Options.UseFont = true;
            this.btnCommit.AutoWidthInLayoutControl = true;
            this.btnCommit.Location = new System.Drawing.Point(527, 275);
            this.btnCommit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCommit.MinimumSize = new System.Drawing.Size(112, 0);
            this.btnCommit.Name = "btnCommit";
            this.btnCommit.Size = new System.Drawing.Size(112, 26);
            this.btnCommit.StyleController = this.layoutControl1;
            this.btnCommit.TabIndex = 9;
            this.btnCommit.Text = "Create Report";
            this.btnCommit.Click += new System.EventHandler(this.startButton_Click);
            // 
            // progressEdit
            // 
            this.progressEdit.EditValue = null;
            this.progressEdit.Location = new System.Drawing.Point(120, 114);
            this.progressEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressEdit.MinimumSize = new System.Drawing.Size(0, 26);
            this.progressEdit.Name = "progressEdit";
            this.progressEdit.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressEdit.Size = new System.Drawing.Size(519, 26);
            this.progressEdit.StyleController = this.layoutControl1;
            this.progressEdit.TabIndex = 8;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.sbInvoicesCheckGridControl;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(679, 78);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.spreadsheetControl;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(339, 105);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.sbInvoicesGridControl;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(339, 105);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.lblConfirm,
            this.lblChange1,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(40, 40, 0, 40);
            this.layoutControlGroup1.Size = new System.Drawing.Size(759, 354);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.progressEdit;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.layoutControlItem2.CustomizationFormText = "Progress bar :";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(1, 75);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 80, 80, 80);
            this.layoutControlItem2.Size = new System.Drawing.Size(679, 116);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Progress bar :";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            this.layoutControlItem2.TrimClientAreaToControl = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnCommit;
            this.layoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.FillControlToClientArea = false;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 275);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 80, 0, 0);
            this.layoutControlItem1.Size = new System.Drawing.Size(679, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.SupportHorzAlignment;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // lblConfirm
            // 
            this.lblConfirm.AllowHotTrack = false;
            this.lblConfirm.CustomizationFormText = "Click Save to confirm changes";
            this.lblConfirm.Location = new System.Drawing.Point(0, 301);
            this.lblConfirm.Name = "lblConfirm";
            this.lblConfirm.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 0, 0, 0);
            this.lblConfirm.Size = new System.Drawing.Size(679, 13);
            this.lblConfirm.Text = "Click Create Report to proceed";
            this.lblConfirm.TextSize = new System.Drawing.Size(179, 13);
            // 
            // lblChange1
            // 
            this.lblChange1.AllowHotTrack = false;
            this.lblChange1.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblChange1.AppearanceItemCaption.Options.UseFont = true;
            this.lblChange1.CustomizationFormText = "Commit Changes:";
            this.lblChange1.Location = new System.Drawing.Point(0, 0);
            this.lblChange1.Name = "lblChange1";
            this.lblChange1.Size = new System.Drawing.Size(679, 34);
            this.lblChange1.Text = "  Report progress :";
            this.lblChange1.TextSize = new System.Drawing.Size(179, 30);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(679, 108);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 150);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(84, 17);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(116, 150);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(563, 17);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lblProgressText;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(84, 150);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(32, 17);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // sbInvoicesTableAdapter
            // 
            this.sbInvoicesTableAdapter.ClearBeforeFill = true;
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem1.CustomizationFormText = "Installation progress:";
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(827, 34);
            this.simpleLabelItem1.Text = "Report progress:";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(161, 30);
            // 
            // ucExecutePage
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Office 2013";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ucExecutePage";
            this.Size = new System.Drawing.Size(759, 354);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sbInvoicesCheckGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sbInvoicesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_SB_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sbInvoicesCheckGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sbInvoicesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sbInvoicesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyItemTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChange1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker bgWorker;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.ProgressBarControl progressEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.SimpleLabelItem lblChange1;
        private DevExpress.XtraEditors.SimpleButton btnCommit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SimpleLabelItem lblConfirm;
        private DataSet_SB_DataEntry dataSet_SB_DataEntry;
        private System.Windows.Forms.BindingSource sbInvoicesBindingSource;
        private DataSet_SB_DataEntryTableAdapters.sbInvoicesTableAdapter sbInvoicesTableAdapter;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraGrid.GridControl sbInvoicesCheckGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView sbInvoicesCheckGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceID1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colAnalysisCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment1;
        private DevExpress.XtraGrid.Columns.GridColumn colPaid1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClient1;
        private DevExpress.XtraGrid.Columns.GridColumn colDirectEmployed1;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT1;
        private DevExpress.XtraGrid.Columns.GridColumn colExchequerPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscrepancy;
        private DevExpress.XtraGrid.GridControl sbInvoicesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView sbInvoicesGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeCode;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colAnalysisCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colPaid;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyItemTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colClient;
        private DevExpress.XtraGrid.Columns.GridColumn colDirectEmployed;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.LabelControl lblProgressText;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraSpreadsheet.SpreadsheetControl spreadsheetControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}

﻿namespace WoodPlan5.Forms.Assets.SelfBiller.Views
{
    partial class ucStep1Page
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucStep1Page));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnNext = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.chkCBGroupName = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.spGroupBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_SB_DataEntry = new WoodPlan5.DataSet_SB_DataEntry();
            this.rgSector = new DevExpress.XtraEditors.RadioGroup();
            this.txtInvoice2 = new DevExpress.XtraEditors.TextEdit();
            this.txtInvoice1 = new DevExpress.XtraEditors.TextEdit();
            this.rgEmploymentStatus = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem1 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleLabelItem3 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.sp_GroupTableAdapter = new WoodPlan5.DataSet_SB_DataEntryTableAdapters.sp_GroupTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCBGroupName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spGroupBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_SB_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgSector.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoice2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoice1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgEmploymentStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.btnNext);
            this.layoutControl1.Controls.Add(this.chkCBGroupName);
            this.layoutControl1.Controls.Add(this.rgSector);
            this.layoutControl1.Controls.Add(this.txtInvoice2);
            this.layoutControl1.Controls.Add(this.txtInvoice1);
            this.layoutControl1.Controls.Add(this.rgEmploymentStatus);
            this.layoutControl1.Controls.Add(this.labelControl2);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2610, 444, 450, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(860, 355);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnNext
            // 
            this.btnNext.ImageIndex = 6;
            this.btnNext.ImageList = this.imageCollection1;
            this.btnNext.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnNext.Location = new System.Drawing.Point(729, 278);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(89, 22);
            this.btnNext.StyleController = this.layoutControl1;
            this.btnNext.TabIndex = 17;
            this.btnNext.Text = "Next";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("info_16x16.png", "images/support/info_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/info_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "info_16x16.png");
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "Ok_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "refresh_16x16.png");
            this.imageCollection1.InsertGalleryImage("next_16x16.png", "images/arrows/next_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/arrows/next_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "next_16x16.png");
            // 
            // chkCBGroupName
            // 
            this.chkCBGroupName.EditValue = "";
            this.chkCBGroupName.Location = new System.Drawing.Point(151, 198);
            this.chkCBGroupName.Name = "chkCBGroupName";
            this.chkCBGroupName.Properties.AllowMultiSelect = true;
            this.chkCBGroupName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chkCBGroupName.Properties.DataSource = this.spGroupBindingSource;
            this.chkCBGroupName.Properties.DisplayMember = "groupName";
            this.chkCBGroupName.Properties.NullValuePrompt = "-- Please Select Group Name --";
            this.chkCBGroupName.Properties.NullValuePromptShowForEmptyValue = true;
            this.chkCBGroupName.Properties.ValueMember = "groupID";
            this.chkCBGroupName.Size = new System.Drawing.Size(667, 20);
            this.chkCBGroupName.StyleController = this.layoutControl1;
            this.chkCBGroupName.TabIndex = 16;
            this.chkCBGroupName.Validated += new System.EventHandler(this.chkCBGroupName_Validated);
            // 
            // spGroupBindingSource
            // 
            this.spGroupBindingSource.DataMember = "sp_Group";
            this.spGroupBindingSource.DataSource = this.dataSet_SB_DataEntry;
            // 
            // dataSet_SB_DataEntry
            // 
            this.dataSet_SB_DataEntry.DataSetName = "DataSet_SB_DataEntry";
            this.dataSet_SB_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rgSector
            // 
            this.rgSector.EditValue = 0;
            this.rgSector.Location = new System.Drawing.Point(151, 160);
            this.rgSector.Name = "rgSector";
            this.rgSector.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "GC"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Public Sector"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Arb")});
            this.rgSector.Size = new System.Drawing.Size(667, 34);
            this.rgSector.StyleController = this.layoutControl1;
            this.rgSector.TabIndex = 12;
            this.rgSector.SelectedIndexChanged += new System.EventHandler(this.rgSector_SelectedIndexChanged);
            // 
            // txtInvoice2
            // 
            this.txtInvoice2.EditValue = "";
            this.txtInvoice2.Location = new System.Drawing.Point(151, 136);
            this.txtInvoice2.Name = "txtInvoice2";
            this.txtInvoice2.Properties.NullValuePrompt = "SBWKYY e.g. (SB51214) or -1";
            this.txtInvoice2.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtInvoice2.Size = new System.Drawing.Size(667, 20);
            this.txtInvoice2.StyleController = this.layoutControl1;
            this.txtInvoice2.TabIndex = 11;
            this.txtInvoice2.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
            // 
            // txtInvoice1
            // 
            this.txtInvoice1.EditValue = "";
            this.txtInvoice1.Location = new System.Drawing.Point(151, 112);
            this.txtInvoice1.Name = "txtInvoice1";
            this.txtInvoice1.Properties.NullValuePrompt = "SBWKYY e.g. (SB51214)";
            this.txtInvoice1.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtInvoice1.Size = new System.Drawing.Size(667, 20);
            this.txtInvoice1.StyleController = this.layoutControl1;
            this.txtInvoice1.TabIndex = 10;
            this.txtInvoice1.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
            // 
            // rgEmploymentStatus
            // 
            this.rgEmploymentStatus.EditValue = 0;
            this.rgEmploymentStatus.Location = new System.Drawing.Point(151, 75);
            this.rgEmploymentStatus.Name = "rgEmploymentStatus";
            this.rgEmploymentStatus.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Not Directly Employed (Subcontractor)"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Directly Employed")});
            this.rgEmploymentStatus.Size = new System.Drawing.Size(667, 33);
            this.rgEmploymentStatus.StyleController = this.layoutControl1;
            this.rgEmploymentStatus.TabIndex = 9;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(42, 36);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Padding = new System.Windows.Forms.Padding(30, 10, 10, 10);
            this.labelControl2.Size = new System.Drawing.Size(776, 35);
            this.labelControl2.StyleController = this.layoutControl1;
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "Please complete required information before you proceed.";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem1,
            this.simpleLabelItem2,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(40, 40, 0, 40);
            this.layoutControlGroup1.Size = new System.Drawing.Size(860, 355);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // simpleLabelItem1
            // 
            this.simpleLabelItem1.AllowHotTrack = false;
            this.simpleLabelItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem1.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem1.CustomizationFormText = "Options:";
            this.simpleLabelItem1.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem1.Name = "simpleLabelItem1";
            this.simpleLabelItem1.Size = new System.Drawing.Size(780, 34);
            this.simpleLabelItem1.Text = "Options:";
            this.simpleLabelItem1.TextSize = new System.Drawing.Size(106, 30);
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.CustomizationFormText = "Select installation folder to continue";
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 302);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 0, 0, 0);
            this.simpleLabelItem2.Size = new System.Drawing.Size(780, 13);
            this.simpleLabelItem2.Text = "Click Next to proceed.";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(106, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 220);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(780, 56);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl2;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(780, 39);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.rgEmploymentStatus;
            this.layoutControlItem2.CustomizationFormText = "Employment Status :";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 73);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(780, 37);
            this.layoutControlItem2.Text = "Employment Status :";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtInvoice1;
            this.layoutControlItem3.CustomizationFormText = "Invoice ID 1 :";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 110);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(780, 24);
            this.layoutControlItem3.Text = "Invoice ID 1 :";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtInvoice2;
            this.layoutControlItem4.CustomizationFormText = "Invoice ID 2 :";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 134);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(780, 24);
            this.layoutControlItem4.Text = "Invoice ID 2 :";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.rgSector;
            this.layoutControlItem6.CustomizationFormText = "Sector :";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 158);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(780, 38);
            this.layoutControlItem6.Text = "Sector :";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.chkCBGroupName;
            this.layoutControlItem7.CustomizationFormText = "Group Name :";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 196);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(780, 24);
            this.layoutControlItem7.Text = "Group Name :";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnNext;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(687, 276);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(93, 26);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 276);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(687, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleLabelItem3
            // 
            this.simpleLabelItem3.AllowHotTrack = false;
            this.simpleLabelItem3.CustomizationFormText = "Select installation folder to continue";
            this.simpleLabelItem3.Location = new System.Drawing.Point(0, 302);
            this.simpleLabelItem3.Name = "simpleLabelItem2";
            this.simpleLabelItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(80, 0, 0, 0);
            this.simpleLabelItem3.Size = new System.Drawing.Size(780, 13);
            this.simpleLabelItem3.Text = "Select Make/Model from the lists to enable edit options. Click Next to continue";
            this.simpleLabelItem3.TextSize = new System.Drawing.Size(372, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.CustomizationFormText = "Employment Status :";
            this.layoutControlItem5.Location = new System.Drawing.Point(257, 93);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(918, 31);
            this.layoutControlItem5.Text = "Employment Status :";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(99, 13);
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // sp_GroupTableAdapter
            // 
            this.sp_GroupTableAdapter.ClearBeforeFill = true;
            // 
            // ucStep1Page
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Appearance.Options.UseBackColor = true;
            this.Appearance.Options.UseForeColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.SkinName = "Office 2013";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ucStep1Page";
            this.Size = new System.Drawing.Size(860, 355);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.ucStep1Page_Validating);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCBGroupName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spGroupBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_SB_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgSector.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoice2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoice1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgEmploymentStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.CheckedComboBoxEdit chkCBGroupName;
        private DevExpress.XtraEditors.RadioGroup rgSector;
        private DevExpress.XtraEditors.TextEdit txtInvoice2;
        private DevExpress.XtraEditors.TextEdit txtInvoice1;
        private DevExpress.XtraEditors.RadioGroup rgEmploymentStatus;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private System.Windows.Forms.BindingSource spGroupBindingSource;
        private DataSet_SB_DataEntry dataSet_SB_DataEntry;
        internal DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DataSet_SB_DataEntryTableAdapters.sp_GroupTableAdapter sp_GroupTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnNext;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    }
}

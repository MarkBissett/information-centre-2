﻿using DevExpress.XtraBars.Docking2010.Views.WindowsUI;
using DevExpress.XtraEditors;
using WoodPlan5.Forms.Assets;

namespace WoodPlan5.Forms.Assets.SelfBiller.Views
{
    public partial class BaseWizardPage : XtraUserControl, ISupportNavigation
    {
        public BaseWizardPage()
        {
            InitializeComponent();
        }
        protected IWizardPageViewModel PageViewModel
        {
            get { return WizardViewModel.CurrentPage; }
        }
        protected IWizardViewModel WizardViewModel
        {
            get { return wizardViewModel; }
        }
        #region ISupportNavigation Members
        IWizardViewModel wizardViewModel;
        void ISupportNavigation.OnNavigatedTo(INavigationArgs args)
        {
            wizardViewModel = args.Parameter as IWizardViewModel;
        }
        void ISupportNavigation.OnNavigatedFrom(INavigationArgs args)
        {
        }
        #endregion

        public bool validPage
        {
            get;
            set;
        }
        

        private void BaseWizardPage_Load(object sender, System.EventArgs e)
        {

        }
    }
}
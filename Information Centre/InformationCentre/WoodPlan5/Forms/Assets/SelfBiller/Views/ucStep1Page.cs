﻿using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors;
using System;
using System.Text.RegularExpressions;

namespace WoodPlan5.Forms.Assets.SelfBiller.Views
{
    public partial class ucStep1Page : Views.BaseWizardPage
    {
        public ucStep1Page()
        {
            InitializeComponent();
            sp_GroupTableAdapter.Fill(dataSet_SB_DataEntry.sp_Group, "", "");
            PublicSectorFilter();
        }

        #region Declaration

        internal static bool blnStep1PageValid = true;
        internal static bool blnInvoice1Valid = false;
        internal static bool blnInvoice2Valid = false;


        #endregion

        #region Events

        private void chkCBGroupName_Validated(object sender, EventArgs e)
        {
            if (chkCBGroupName.EditValue == null ? false : true)
            {
                ucExecutePage.strGroupID = Convert.ToString(chkCBGroupName.EditValue);
            }
            else
            {
                ucExecutePage.strGroupID = "0";
            }
        }

        private void rgSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            PublicSectorFilter();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            CheckValid();
            if (blnStep1PageValid)
            {
                ((ViewModels.Step1PageViewModel)PageViewModel).blnValidEntry = true;
                updateValues();

                //if (ucExecutePage.Importfolder != "")
                //{
                //    ((ViewModels.Step2PageViewModel)PageViewModel).blnValidPath = true;
                //}
                //else
                //{
                //    ((ViewModels.Step2PageViewModel)PageViewModel).blnValidPath = false;
                //}
                WizardViewModel.PageCompleted();

            }
            else
            {
                ((ViewModels.Step1PageViewModel)PageViewModel).blnValidEntry = false;
            }
        }

        private void textBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void ucStep1Page_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.ValidateChildren();

            blnStep1PageValid = !dxErrorProvider.HasErrors;
           
        }

        #endregion

        #region Methods

        private void updateValues()
        {
            ucExecutePage.intEmploymentStatus = Convert.ToInt32(rgEmploymentStatus.EditValue);
            ucExecutePage.intSector = Convert.ToInt32(rgSector.EditValue);
            ucExecutePage.invoiceID1 = txtInvoice1.EditValue.ToString();
            string inv2 = txtInvoice2.EditValue.ToString();

            if (inv2 == "")
                inv2 = "-1";
            ucExecutePage.invoiceID2 = inv2;
        }

        internal static void CheckValid()
        {
            if (blnInvoice1Valid && blnInvoice2Valid)
            {
                blnStep1PageValid = true;
            }
            else
            {
                blnStep1PageValid = false;
            }
        }
        
        private bool validateTextBox(TextEdit txtBox)
        {

            bool valid = false;

            txtBox.Text = txtBox.Text.ToUpper();
            string originalText = txtBox.Text;

            string parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9]-", "");
            if (txtBox.Name == "txtInvoice1")
            {
                if (Regex.IsMatch(originalText, "SB\\d\\d\\d\\d") && txtBox.Text.Length == 6)
                {
                    valid = true;
                }
                else
                {
                    dxErrorProvider.SetError(txtBox, "Please enter a valid text e.g. SB1214");
                }
                blnInvoice1Valid = valid;

                if (valid && txtInvoice2.Text == "-1")
                {
                    blnStep1PageValid = true;
                }
            }
            else
            {
                if (txtBox.Text == "-1" || txtBox.Text == "")
                {
                    if (txtBox.Text == "")
                    {
                        txtBox.Text = "-1";
                    }
                    valid = true;
                }
                else
                {
                    if (Regex.IsMatch(originalText, "SB\\d\\d\\d\\d") && txtBox.Text.Length == 6)
                    {
                        valid = true;
                    }
                    else
                    {
                        dxErrorProvider.SetError(txtBox, "Please enter a valid text or enter '-1'");
                    }
                }

                blnInvoice2Valid = valid;
            }

            if (txtBox.Name == "txtInvoice1" && blnInvoice1Valid && txtInvoice2.Text == "")
            {
                txtInvoice2.Focus();
            }

            if (valid)
            {
                dxErrorProvider.SetError(txtBox, "");
            }
            return valid;
        }
        
        private void PublicSectorFilter()
        {
            if (rgSector.EditValue == null ? true : false)
            {
                spGroupBindingSource.Filter = "";
            }
            else
            {
                switch (Convert.ToInt32(rgSector.EditValue))
                {
                    case 0://non public sector (GC)
                        spGroupBindingSource.Filter = "groupName <> 'Public Sector' and groupName <> 'Arb' ";
                        break;
                    case 1:// public sector
                        spGroupBindingSource.Filter = "groupName = 'Public Sector'";
                        break;
                    case 2:// Arb
                        spGroupBindingSource.Filter = "groupName = 'Arb'";
                        break;
                }
                chkCBGroupName.CheckAll();
            }
        }
        
        #endregion

     
    }
}
﻿using DevExpress.Spreadsheet;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using System.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;


namespace WoodPlan5.Forms.Assets.SelfBiller.Views
{
    public partial class ucExecutePage : Views.BaseWizardPage
    {
        public ucExecutePage()
        {
            InitializeComponent();
        }


        #region Declaration

        public static int intEmploymentStatus;
        public static int intSector;
        public static int intGroupID;
        public static string strGroupID;
        public static string invoiceID1;
        public static string invoiceID2;
        public static string Importfolder;
        int iCount = 0;
        string strFilePath = "";
        string strFileName = "";

        #endregion


        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i <= 10; i++)
            {
                iCount = i;
                switch (i)
                {
                    case 2:
                        try
                        {
                            sbInvoicesTableAdapter.Fill(dataSet_SB_DataEntry.sbInvoices, intEmploymentStatus, invoiceID1, invoiceID2, strGroupID);
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message);
                        }
                        break;
                    case 7://Write results
                        strFileName = @"\\" + "SB" + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss");
                        strFilePath = Importfolder + strFileName + ".Xlsx";
                        sbInvoicesGridControl.DataSource = dataSet_SB_DataEntry.sbInvoices;
                        sbInvoicesCheckGridControl.DataSource = dataSet_SB_DataEntry.sbInvoices;
                        
                        DevExpress.Spreadsheet.Workbook destWorkBook = new DevExpress.Spreadsheet.Workbook();
                        destWorkBook.CreateNewDocument();

                        destWorkBook.Worksheets.Add().Name = "Exchequer Export";   
                        System.Data.DataTable dt1 =  getDT(sbInvoicesGridControl);
                        destWorkBook.Worksheets["Exchequer Export"].Import(dt1, true, 0, 0);

                        destWorkBook.Worksheets.Add().Name = "Quick Check Page";
                        System.Data.DataTable dt2 = getDT(sbInvoicesCheckGridControl);
                        destWorkBook.Worksheets["Quick Check Page"].Import(dt2, true, 0, 0);
                        
                        int intX = 0;
                        foreach (DevExpress.Spreadsheet.Worksheet sheet in destWorkBook.Worksheets)
                        {
                            sheet.FreezeRows(0);
                            switch (intX)
                            {
                                case 1:
                                    FormatSheetCurrency(sheet, sheet.Range["I:I"]);                             
                                    FormatSheetRownHeaderApperance(sheet);
                                    
                                    break;
                                case 2://Quick Check Page
                                    FormatSheetCurrency(sheet, sheet.Range["I:I"]);
                                    FormatSheetCurrency(sheet, sheet.Range["N:P"]);       
                                    FormatSheetRownHeaderApperance(sheet);
                                    break;

                                default:
                                    break;
                            }
                            intX++;
                        }
                        
                        destWorkBook.Worksheets.RemoveAt(0);
                        destWorkBook.Worksheets.ActiveWorksheet = destWorkBook.Worksheets["Quick Check Page"];
                        destWorkBook.SaveDocument(strFilePath);
                        destWorkBook.Dispose();
           
                        break;
                                           
                    case 9:
                        System.Threading.Thread.Sleep(2000);
                        break;
                }
                System.Threading.Thread.Sleep(100);
                ((BackgroundWorker)sender).ReportProgress(i);
            }


        }

        private void FormatSheetRownHeaderApperance(DevExpress.Spreadsheet.Worksheet sheet)
        {
            int lastCol = sheet.GetUsedRange().RightColumnIndex;
            int lastRow = sheet.GetUsedRange().BottomRowIndex;
            // Alternating rowcolor //
            for (int ii = 1; ii <= lastRow; ii++)
            {
                if (ii % 2 == 0) sheet.Range.FromLTRB(0, ii, lastCol, ii).FillColor = DXColor.FromArgb(197, 217, 241);
            }
            DevExpress.Spreadsheet.Range header = sheet.Range.FromLTRB(0, 0, lastCol, 0);
            header.AutoFitRows();
            header.FillColor = DXColor.FromArgb(83, 141, 213);
            sheet.GetUsedRange().AutoFitColumns();
        }

        private void FormatSheetCurrency(DevExpress.Spreadsheet.Worksheet sheet, DevExpress.Spreadsheet.Range range)
        {
            DevExpress.Spreadsheet.Range colI = range;
            Formatting colIFormatting = colI.BeginUpdateFormatting();
            colIFormatting.NumberFormat = "£#,##0.00";
            colI.EndUpdateFormatting(colIFormatting);

            //int lastCol = sheet.GetUsedRange().RightColumnIndex;
            //int lastRow = sheet.GetUsedRange().BottomRowIndex;


     
        }

        private System.Data.DataTable getDT(GridControl gridControl)
        {
            GridView view = (GridView)gridControl.MainView;
            System.Data.DataTable dt = new System.Data.DataTable();
            foreach (GridColumn column in view.VisibleColumns)
            {
                dt.Columns.Add(column.FieldName, column.ColumnType);
            }
            for (int i = 0; i < view.DataRowCount; i++)
            {
                DataRow row = dt.NewRow();
                foreach (GridColumn column in view.VisibleColumns)
                {
                    row[column.FieldName] = view.GetRowCellValue(i, column);
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressEdit.PerformStep();
            switch (iCount)
            {
                case 1:
                    lblProgressText.Text = "Connecting....";
                    break;
                case 2:
                    lblProgressText.Text = "Running Query....";
                    break;
                case 7:
                    lblProgressText.Text = "Preparing Export....";
                    break;
                case 8:
                    lblProgressText.Text = "Exporting Data....";
                    break;
                case 9:
                    lblProgressText.Text = "Cleaning Up....";
                    break;

            }
        }


        void startButton_Click(object sender, EventArgs e)
        {
            if (!bgWorker.IsBusy)
            {
                btnCommit.Enabled = false;
                bgWorker.RunWorkerAsync();
            }

        }

        //private void RenameExcelSheetsName(string path)
        //{
        //    object oMissing = System.Reflection.Missing.Value;
        //    Microsoft.Office.Interop.Excel.ApplicationClass xl = new Microsoft.Office.Interop.Excel.ApplicationClass();
        //    Microsoft.Office.Interop.Excel.Workbook xlBook;
        //    Microsoft.Office.Interop.Excel.Worksheet xlSheet1;
        //    Microsoft.Office.Interop.Excel.Worksheet xlSheet2;
        //    string laPath = path;//excel file path
        //    xlBook = (Microsoft.Office.Interop.Excel.Workbook)xl.Workbooks.Open(laPath, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);

        //    switch (xlBook.Worksheets.Count)
        //    {
        //        case 1:
        //                xlSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlBook.Worksheets.get_Item(1);//get worksheet number
        //                xlSheet1.Name ="Exchequer Export";
        //                xlBook.Save();
        //                xl.Application.Workbooks.Close();
        //            break;
        //        case 2:
        //                xlSheet1 = (Microsoft.Office.Interop.Excel.Worksheet)xlBook.Worksheets.get_Item(1);//get worksheet number
        //                xlSheet2 = (Microsoft.Office.Interop.Excel.Worksheet)xlBook.Worksheets.get_Item(2);//get worksheet number
        //                xlSheet1.Name = "Exchequer Export";
        //                xlSheet2.Name = "Quick Check Page";
        //                xlBook.Save();
        //                xl.Application.Workbooks.Close();
        //            break;
        //    }
           
        //}
        

        void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled)
                ((ViewModels.ExecutePageViewModel)PageViewModel).IsComplete = true;
            WizardViewModel.PageCompleted();
        }

        public void FormatXlsxFilesDevExpress(string destXlsxFileName, string sourceXlsxFile)
        {
            DevExpress.Spreadsheet.Workbook destWorkBook = new DevExpress.Spreadsheet.Workbook();
            destWorkBook.CreateNewDocument();

            int i = 0;
            //{
                DevExpress.Spreadsheet.Workbook sourceWorkBook = new DevExpress.Spreadsheet.Workbook();

                sourceWorkBook.LoadDocument(sourceXlsxFile);
                foreach (DevExpress.Spreadsheet.Worksheet sheet in sourceWorkBook.Worksheets)
                {
                    i++;
                    DevExpress.Spreadsheet.Worksheet temp = destWorkBook.Worksheets.Add();
                    temp.CopyFrom(sheet);
                    int lastCol = temp.GetUsedRange().RightColumnIndex;
                    int lastRow = temp.GetUsedRange().BottomRowIndex;

                    DevExpress.Spreadsheet.Range header = temp.Range.FromLTRB(0, 0, lastCol, 0);
                    header.AutoFitRows();

                    // Alternating rowcolor //
                    for (int ii = 1; ii <= lastRow; ii++)
                    {
                        if (ii % 2 == 0) temp.Range.FromLTRB(0, ii, lastCol, ii).FillColor = DXColor.FromArgb(38, 124, 250);
                    }
                    temp.GetUsedRange().AutoFitColumns();

                    switch (i)
                    {
                        case 1:
                            DevExpress.Spreadsheet.Range colI  = temp.Range["I:I"];
                            Formatting colIFormatting = colI.BeginUpdateFormatting();
                            colIFormatting.NumberFormat = "£#,##0.00";
                            colI.EndUpdateFormatting(colIFormatting);
                            temp.Name = "Exchequer Export";
                            break;
                        case 2:
                            DevExpress.Spreadsheet.Range columnI  = temp.Range["I:I"];
                            Formatting columnIFormatting = columnI.BeginUpdateFormatting();
                            columnIFormatting.NumberFormat = "£#,##0.00";
                            columnI.EndUpdateFormatting(columnIFormatting);
                            temp.Name = "Quick Check Page";
                            break;
                        
                        default:
                            break;
                    }
                }
            sourceWorkBook.Dispose();
            destWorkBook.Worksheets.RemoveAt(0);
            destWorkBook.Worksheets.ActiveWorksheet = destWorkBook.Worksheets["Exchequer Export"];
            destWorkBook.SaveDocument(destXlsxFileName);
            destWorkBook.Dispose();
        }




    }
}
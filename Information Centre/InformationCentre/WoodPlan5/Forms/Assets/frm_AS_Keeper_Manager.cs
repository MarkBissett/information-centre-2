using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using System.Collections.Specialized;
using DevExpress.XtraSplashScreen;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace WoodPlan5
{
    public partial class frm_AS_Keeper_Manager : BaseObjects.frmBase
    {

        private void GetDefaultLayoutFileDirectoryFrom(out bool shouldReturn)
        {
            shouldReturn = false;
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);

                strFromAddress = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetFromAddress"));
                strFromAddressDisplay = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetFromAddressDisplay"));
                strAssetSubjectLine = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetReminderSubjectLine"));
                strAppBase = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetEmailHTMLPath"));
                strAssetEmailHTMLLayoutFile = string.Format(@"{0}\{1}", strAppBase, Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetEmailHTMLLayoutFile")));
                strSMTPMailServerAddress = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerAddress"));
                strSMTPMailServerUsername = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerUsername"));
                strSMTPMailServerPassword = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerPassword"));
                strSMTPMailServerPort = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerPort"));
                autoSendDate = Convert.ToDateTime(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetAutoSendDate"));
                intReturnedMonthLimit = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetReturnedMonthLimit"));
                autoSendEmail = (Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetAutoSendEmail")) == 1 ? true : false);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Location of the Mail Merge File Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Mail Merge Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                shouldReturn = true;
                return;
            }
        }

        private void frm_AS_Keeper_Manager_Load(object sender, EventArgs e)
        {
            if (fProgress != null)
                fProgress.UpdateProgress(20); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 1113;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            LoadConnectionStrings(this.GlobalSettings.ConnectionString);
            LoadAdapters();
            // Get Form Permissions //            
            ProcessPermissionsForForm();
            if (fProgress != null)
                fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
                fProgress.UpdateProgress(10); // Update Progress Bar //

            
            if (fProgress != null)
                fProgress.UpdateProgress(10); // Update Progress Bar //


            if (strPassedInRecordDs != "")  // Opened in drill-down mode //
            {
                //popupContainerEdit1.Text = "Custom Filter";
                Load_Data();  // Load records //
            }

            // Get default layout file directory from System_Settings table //
            bool shouldReturn;
            GetDefaultLayoutFileDirectoryFrom(out shouldReturn);
            if (shouldReturn)
                return;
            if (fProgress != null)
                fProgress.UpdateProgress(10); // Update Progress Bar //
            emptyEditor = new RepositoryItem();
            loadSettings("load");
            //set minimum date for date picker
            this.repositoryItemDateEdit1.MinValue = getCurrentDate().AddDays(0);
           
            
        }

        private void loadSettings(string mode)
        {
            switch (mode)
            {
                case "load":
                    bbiAutoSend.EditValue = autoSendEmail;
                    bbiNextEmailDate.EditValue = autoSendDate;
                    bbiNextEmailDate.Enabled = autoSendEmail;
                    break;
                case "updateSendEmail":
                    autoSendEmail = Convert.ToBoolean(bbiAutoSend.EditValue);
                    break;
                case "updateSendDate":
                    autoSendDate = Convert.ToDateTime(bbiNextEmailDate.EditValue);
                    break;
            }
        }
        
        #region Instance Variables...

        string strAppBase = string.Empty;
        private int intReturnedMonthLimit = -1;
        DataTable dt ;//= new DataTable();
        private bool bool_FormLoading = true;
        private Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private DateTime autoSendDate;
        private bool autoSendEmail;
        private string strSMTPMailServerAddress = string.Empty;
        private string strSMTPMailServerUsername = string.Empty;
        private string strSMTPMailServerPassword = string.Empty;
        private string strSMTPMailServerPort = string.Empty;

        private string strAssetSubjectLine = string.Empty;
        private string strFromAddress = string.Empty;
        private string strFromAddressDisplay = string.Empty;
        private string strAssetEmailHTMLLayoutFile = string.Empty;
        private Settings set = Settings.Default;
        private string strConnectionString = string.Empty;
        private bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public string strPassedInRecordDs = string.Empty;  // Used to hold IDs when form opened from another form in drill-down mode //
        public string strRecordIDs = string.Empty;
        public string strRecordsToLoad = string.Empty;
        public int numOfSelectedRows = 0;

        int i_int_FocusedGrid = 0;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        
        
        // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateEquipment; 
        public RefreshGridState RefreshGridViewStateVehicle;  
        public RefreshGridState RefreshGridViewStatePlant;  
        public RefreshGridState RefreshGridViewStateGadget;
        public RefreshGridState RefreshGridViewStateHardware;
        public RefreshGridState RefreshGridViewStateSoftware;
        public RefreshGridState RefreshGridViewStateOffice;
        public RefreshGridState RefreshGridViewStateKeeper;
        public RefreshGridState RefreshGridViewStateTransactions;
        public RefreshGridState RefreshGridViewStateBilling;
        public RefreshGridState RefreshGridViewStateDepreciation;
        public RefreshGridState RefreshGridViewStateCover;
        public RefreshGridState RefreshGridViewStateServiceInterval;
        public RefreshGridState RefreshGridViewStateWork;
        public RefreshGridState RefreshGridViewStateIncident;
        public RefreshGridState RefreshGridViewStatePurpose;
        public RefreshGridState RefreshGridViewStateFuelCard;

        //public RefreshGridState RefreshGridViewStateReminder;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        private string i_str_AddedRecordIDs1 = string.Empty;  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs2 = string.Empty;  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs3 = string.Empty;  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        private string strDefaultPath = string.Empty;  // Holds the first part of the path, retrieved from the System_Settings table //

        private enum FormType { child, parent };
        private enum GridType {child, parent };
        public enum FormMode { add, edit, view, delete, blockadd, blockedit };
        private enum EquipmentType { None, Vehicle = 1, Plant = 2, Gadget = 3, Hardware = 4, Software = 5, Office = 6}

        private enum SendEmailType {none,selected,all}
        #endregion
       
        #region Constructor and compulsory implementation

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        //if (sfpPermissions.blCreate)
                        //{
                        //    iBool_AllowAdd = true;
                        //}
                        //if (sfpPermissions.blUpdate)
                        //{
                        //    iBool_AllowEdit = true;
                        //}
                        //if (sfpPermissions.blDelete)
                        //{
                        //    iBool_AllowDelete = true;
                        //}
                        break;
                    
                }
            }
        }

        public frm_AS_Keeper_Manager()
        {
            InitializeComponent();
        }

        #endregion

        #region Form_Events

        private void commonGridView_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                if (i_int_FocusedGrid != 0)
                {
                    //navigate to keeper details
                }
            }
        }

       private void LoadAdapters()
        {
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            sp_AS_11111_Keeper_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11111_Keeper_List, null, null);
            sp_AS_11112_Keeper_Email_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_Item, null, null);
                       
            RefreshAdapters();
            RefreshBindingSources();
        }

       private void RefreshBindingSources()
       {
           sp_AS_11000_PL_Email_TypeTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11000_PL_Email_Type,45);
           DataRow[] dr = dataSet_AS_DataEntry.sp_AS_11000_PL_Email_Type.Select("[Value] = 'Suppression Applied'");
           int intEmailType =((DataSet_AS_DataEntry.sp_AS_11000_PL_Email_TypeRow)dr[0]).PickListID;
           string activeUnTickedFilter = "[SelectedForMail] = 0 and [Emailable] = 1 ";
           bsAS11112KeeperActiveUnticked.Filter = activeUnTickedFilter;
           string activeFilter = "[SelectedForMail] = 1 and [Emailable] = 1";
           bsAS11112KeeperActiveValid.Filter = activeFilter;
           string activeInvalidFilter = "[Emailable] = 0";
           bsAS11112KeeperActiveInvalid.Filter = activeInvalidFilter;
           
       }

       private void updateKeeperList(string strMode)
       {
           int intKeeperType;
           int intKeeperID;
           toggleAll(false);
           //add new keepers if any
           DataRow[] drKeeperList = this.dataSet_AS_DataEntry.sp_AS_11111_Keeper_List.Select();
           int progressTotal = drKeeperList.Length;
           int intCounter = 0;
           float increament = ((float)(1) / progressTotal) * 100;
           float progressPercentage = increament;
           frmProgress fProgress = new frmProgress(0);
           switch (strMode)
           {
               case "add":
                   fProgress.UpdateCaption("Searching for new Keepers...");
                   break;
               case "edit":
                   fProgress.UpdateCaption("Updating Keeper details...");
                   break;
               default:
                   fProgress.UpdateCaption("Updating Keeper details...");
                   break;
           }
           fProgress.Show();

           if (progressTotal < 1)
           {
               fProgress.UpdateProgress(100); // Update Progress Bar //
               fProgress.Close();
               fProgress = null;
           }
           if (strMode == "edit")
           {
               foreach (DataRow rowKeeper in drKeeperList)
               {
                   intKeeperType = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11111_Keeper_ListRow)(rowKeeper)).KeeperTypeID;
                   intKeeperID = ((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11111_Keeper_ListRow)(rowKeeper)).EmployeeNumber;
                   sp_AS_11112_Keeper_Email_ItemTableAdapter.Insert(strMode, "", true, 0, 0, "", "", intKeeperType, intKeeperID, "", "", getCurrentDate(), getCurrentDate(), false, "", false, 0, true);

                   intCounter++;
                   float f = (((float)(intCounter) / (float)(progressTotal)) * 100);
                   if (f > progressPercentage)
                   {
                       if (fProgress != null)
                           fProgress.UpdateProgress(increament); // Update Progress Bar //
                       progressPercentage++;
                   }
               }
           }
           else
           {
               sp_AS_11112_Keeper_Email_ItemTableAdapter.Insert(strMode, "", true, 0, 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), false, "", false, 0, true);
               fProgress.UpdateProgress(100); // Update Progress Bar //
               fProgress.Close();
               fProgress = null;
           }

           fixEmailValidation();

           if (fProgress != null)
           {
               fProgress.UpdateProgress(10); // Update Progress Bar //
               fProgress.Close();
               fProgress = null;
           }
            
           sp_AS_11112_Keeper_Email_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_Item, null, null);
           toggleAll(true);
       }

       private void fixEmailValidation()
       {
            string strSendEmailTo = string.Empty;
            int strRecordSelected = 0;
            bool boolSelectedForMail = false;
            bool boolIsCurrentKeeper = false;
           
            DataRow[] drTickedEmail = this.dataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_Item.Select("[SelectedForMail] = 1");
         
            foreach (DataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_ItemRow dataRowEmailList in drTickedEmail)
            {
                strRecordSelected = dataRowEmailList.EmailTrackerID;
                strSendEmailTo = (dataRowEmailList.EmailAddress ?? "");
                boolSelectedForMail = dataRowEmailList.SelectedForMail;
                boolIsCurrentKeeper = dataRowEmailList.IsCurrentKeeper;
                if (boolSelectedForMail && !IsValidEmail(strSendEmailTo))
                {
                    boolSelectedForMail = false;
                    this.sp_AS_11112_Keeper_Email_ItemTableAdapter.Update("emailfix", strRecordSelected.ToString(), boolSelectedForMail, strRecordSelected, 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), true, "", boolIsCurrentKeeper, 0, false);
                }
            }
       }
                      
        public static bool IsValidEmail(string email)
        {
            Regex rx = new Regex(@"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$");
            return rx.IsMatch(email);
        }

       private void toggleAll(bool status)
       {
           bbiReset.Enabled = status;
           bbiFindNewKeeper.Enabled = status;
           bbiSendMail.Enabled = status;
       }
        
       private void RefreshAdapters()
       {
           sp_AS_11112_Keeper_Email_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_Item, "", "");
       }

        private void LoadConnectionStrings(string ConnString)
        {
            strConnectionString = ConnString;
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;            
            //sp_AS_11001_Equipment_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;           
        }

        private void frm_AS_Keeper_Manager_Activated(object sender, EventArgs e)
        {
            frmActivated();
        }


        #endregion

        #region Form_Functions

        public void frmActivated()
        {
            if (UpdateRefreshStatus > 0 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
            {
                Load_Data();
            }
        }

        private void customFilterDraw(GridView view, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(view, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        private void emptyForeGroundMessage(GridView view, string message, CustomDrawEventArgs e)
        {
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString(message, e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }
 
        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInRecordDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                //LoadLastSavedUserScreenSettings();
            }

            bool_FormLoading = false;

        }
  
        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0)
                UpdateRefreshStatus = 0;
            LoadAdapters();
        
                    
        }


        private string[] splitToRows(string strAssetList)
        {
            string[] parts;
            char[] delimiters = new char[] { '#' };
            parts = strAssetList.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            return parts;
        }

        private string[] splitToColumnValues(string strAssetList)
        {
            string[] parts;
            char[] delimiters = new char[] { ';',':' };
            parts = strAssetList.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            return parts;
        }

        private string[] splitStrRecords(string newIDs)
        {
            string[] parts;
            char[] delimiters = new char[] { ';',',' };
            if (newIDs == "")
            {
                parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            else 
            {
                parts = newIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }             
            
            return parts;
        }

        #endregion
        
   
        private string stringRecords(int[] IDs)
        {
            strRecordIDs = "";
            foreach (int rec in IDs)
            {
                strRecordIDs += Convert.ToString(rec) + ',';
            }
            return strRecordIDs;
        }

        private void gridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void commonView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            // This event replace the default functionality code of the FilterEditor (Column editor type is bound to column's RepositoryItem from grid - this uses a combo box instead to present the available values). //
            // Date Fields are ignored [default DatePicker used]. //
            GridView view = (GridView)sender;
            customFilterDraw(view, e);
        }

        private void commonGridView_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            string GridName = ((GridView)sender).Name;
            switch (GridName)
            {
                case "keeperGridView":
                    i_int_FocusedGrid = 0;
                    break;

                case "activeEmailGridView":
                    i_int_FocusedGrid = 1;
                    break;

                case "activeInvalidGridView":
                    i_int_FocusedGrid = 2;
                    break;

                case "activeUntickedEmailGridView":
                    i_int_FocusedGrid = 3;
                    break;
            }
        }

        private DateTime getCurrentDate()
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetDate = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetDate.ChangeConnectionString(strConnectionString);
            GetDate.sp_AS_11138_Get_Server_Date();
            DateTime d = DateTime.Parse(GetDate.sp_AS_11138_Get_Server_Date().ToString());
            return d;
        }

        private void ceSelected_EditValueChanging(object sender, ChangingEventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;

            bool checkStatus = ce.Checked;
            //Convert.ToBoolean(currentRow["SelectedForMail"])
            e.Cancel = false;
            string strRecordSelected = "";
            DataRowView currentRow = null;
            switch (i_int_FocusedGrid)
            {
            	case 1:
                    currentRow = (DataRowView)bsAS11112KeeperActiveValid.Current;
            		break;
                case 2:
                    currentRow = (DataRowView)bsAS11112KeeperActiveInvalid.Current;
                    break;
                case 3:
                    currentRow = (DataRowView)bsAS11112KeeperActiveUnticked.Current;
                    break;
            }
             
            if (currentRow != null)
            {
                strRecordSelected = currentRow["EmailTrackerID"].ToString();
            }

            this.sp_AS_11112_Keeper_Email_ItemTableAdapter.Update("selected", strRecordSelected, !checkStatus, Convert.ToInt32(strRecordSelected), 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), false, "", false, 0,true);

            RefreshAdapters();
        }

        private void bbiSendMail_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            sendEmailList(SendEmailType.all,"", "");
        }

        private void sendEmailList(SendEmailType sendEmailType, string strKeeperTypeID, string strEmployeeNumber)
        {
            bool errorOccured = false;
            mailTabControl.SelectedTabPage = activeKeeperTabPage;
            string strSendEmailTo = string.Empty;
            string strSendFullName = string.Empty;
            int strRecordSelected = 0;
            string strBody = string.Empty;
            bool boolSelectedForMail = false;
            bool boolIsCurrentKeeper = false;
           
            try
            {

                string strAssetList = "No equipment found.";

                //Fill dataset
                string filter = string.Empty;
                if (sendEmailType == SendEmailType.selected)
                {
                    filter = string.Format("[SelectedForMail] = 1 and EmployeeNumber = {0} and KeeperTypeID = {1}", strEmployeeNumber, strKeeperTypeID);
                }
                else
                {
                    filter = string.Format("[SelectedForMail] = 1");
                }
                DataRow[] drTickedEmail = this.dataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_Item.Select(filter);
                //Loop through all records to send
                int progressTotal = drTickedEmail.Length;
                int intCounter = 0;
                float increament = ((float)(1) / progressTotal) * 100;
                float progressPercentage = increament;
                frmProgress fProgress = new frmProgress(0);
                fProgress.UpdateCaption("Sending Emails...");
                fProgress.Show();

                if (progressTotal < 1)
                {
                    fProgress.UpdateProgress(100); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }

                foreach (DataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_ItemRow dataRowEmailList in drTickedEmail)
                {

                    boolSelectedForMail = dataRowEmailList.SelectedForMail;
                    boolIsCurrentKeeper = dataRowEmailList.IsCurrentKeeper;
                    if (boolSelectedForMail)
                    {
                        switch (sendEmailType)
                        {
                            case SendEmailType.selected://email selected
                            case SendEmailType.all:
                                strSendFullName = dataRowEmailList.FullName;
                                strSendEmailTo = dataRowEmailList.EmailAddress;
                                strRecordSelected = dataRowEmailList.EmailTrackerID;
                                int intKeeperID = dataRowEmailList.EmployeeNumber;
                                int intKeeperType = dataRowEmailList.KeeperTypeID;
                                int intActiveCounter = 0;
                                int intMissingCounter = 0;
                                int intReturnedCounter = 0;
                                // = -1;//number of months to go back for return status

                                strAssetList = "";
                                this.sp_AS_11111_Retrieve_Keeper_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11111_Retrieve_Keeper_List, intKeeperType, intKeeperID);
                                dt = new DataTable();
                                createDatatable();

                                DataRow[] drAssetList = this.dataSet_AS_DataEntry.sp_AS_11111_Retrieve_Keeper_List.Select();
                                foreach (DataSet_AS_DataEntry.sp_AS_11111_Retrieve_Keeper_ListRow dataRowAssetList in drAssetList)
                                {
                                    string allocationStatus = dataRowAssetList.AllocationStatus.ToLower();
                                    DateTime allocationEndDate = getCurrentDate().AddMonths(intReturnedMonthLimit);
                                    if (allocationStatus == "returned")
                                    {
                                        allocationEndDate = dataRowAssetList.AllocationEndDate;
                                        if (allocationEndDate > getCurrentDate().AddMonths(intReturnedMonthLimit))
                                        {
                                            string[] strAssetRows = splitToRows(dataRowAssetList.AssetDetailedDescription);
                                            foreach (string strReturnedRow in strAssetRows)
                                            {
                                                string[] strAssetColumnsNValue = splitToColumnValues(strReturnedRow);
                                                addRow(strAssetColumnsNValue);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string[] strAssetRows = splitToRows(dataRowAssetList.AssetDetailedDescription);
                                        foreach (string strReturnedRow in strAssetRows)
                                        {
                                            string[] strAssetColumnsNValue = splitToColumnValues(strReturnedRow);
                                            addRow(strAssetColumnsNValue);
                                        }

                                    }
                                    switch (allocationStatus)
                                    {
                                        case "active":
                                            intActiveCounter++;
                                            break;
                                        case "returned":
                                            if (allocationEndDate > getCurrentDate().AddMonths(intReturnedMonthLimit))
                                            {
                                                intReturnedCounter++;
                                            }
                                            break;
                                        case "missing":
                                            intMissingCounter++;
                                            break;
                                    }
                                }

                                strAssetList += "<br/>" + ConvertDataTableToHTML(dt);
                                string strActive = "";
                                string strReturned = "";
                                string strMissing = "";
                                string strSummary = "<br/>Allocation Summary: ";

                                if (intActiveCounter > 0)
                                {
                                    strActive = string.Format("<br/>{0} record(s) reflecting the Active Allocation status", intActiveCounter);
                                    strSummary += strActive;
                                }
                                if (intReturnedCounter > 0)
                                {
                                    strReturned = string.Format("<br/>{0} record(s) reflecting the Returned Allocation status", intReturnedCounter);
                                    strSummary += strReturned;
                                }
                                if (intMissingCounter > 0)
                                {
                                    strMissing = string.Format("<br/>{0} record(s) reflecting the Missing Allocation status", intMissingCounter);
                                    strSummary += strMissing;
                                }

                                //send email
                                //strBody = "";
                                //strBody = File.ReadAllText(strAssetEmailHTMLLayoutFile);
                                //strBody = strBody.Replace("Summary", strSummary);
                                //strBody = strBody.Replace("FullName", strSendFullName);
                                //strBody = strBody.Replace("AssetList", strAssetList);

                                sendEmail(strRecordSelected, strSendEmailTo, strSendFullName, strSummary, strAssetList);
                                break;
                        }
                    }
                    intCounter++;
                    float f = (((float)(intCounter) / (float)(progressTotal)) * 100);
                    if (f > progressPercentage)
                    {
                        if (fProgress != null)
                            fProgress.UpdateProgress(increament); // Update Progress Bar //
                        progressPercentage++;
                    }

                }

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(10); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("An error occurred trying to send email file. Please contact administrator.", "Error Sending Email", MessageBoxButtons.OK, MessageBoxIcon.Error);

                this.sp_AS_11112_Keeper_Email_ItemTableAdapter.Update("smtpmsg", strRecordSelected.ToString(), boolSelectedForMail, strRecordSelected, 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), true, ex.Message, boolIsCurrentKeeper, 0, true);
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                errorOccured = true;
                return;
            }
            finally
            {
                if (!errorOccured)
                {
                    XtraMessageBox.Show("Emails have been successfully sent.", "Emails Successfully Sent", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        
        private void addRow(string[] strRow)
        {    
            DataRow dtNewRow = dt.NewRow();
            dtNewRow["GC Reference"] = strRow[1];//GC Reference
            if (strRow[2] == " Unique Reference ")
            {
                dtNewRow[1] = "";//strRow[3];//VIN    
                dtNewRow["Unique Reference"] = strRow[3];//ManufacturerID Unique Reference 
                dtNewRow[3] = strRow[5];//Category
                dtNewRow[4] = strRow[3];//Registration Plate
            }
            else
            {
                if (strRow[2] == " VIN ")
                {
                    dtNewRow[1] = strRow[3];//VIN    
                    dtNewRow["Unique Reference"] = "";//ManufacturerID    Unique Reference
                    dtNewRow[3] = " Vehicle ";//Category   
                    dtNewRow[4] = strRow[5];//Registration Plate
                }
                else
                {
                    dtNewRow[1] = "";//VIN 
                    dtNewRow["Unique Reference"] = strRow[3];//ManufacturerID

                    dtNewRow[3] = strRow[5];//Category
                    dtNewRow[4] = "";//Registration Plate
                }
            }
            dtNewRow[5] = strRow[7];//Make
            dtNewRow[6] = strRow[9];//Model
            dtNewRow[7] = strRow[11];//Status

            dt.Rows.Add(dtNewRow);
        }
        
        public string ConvertDataTableToHTML(DataTable dt)
        {
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html >");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");
            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<table border=1 style=background-color:lightgreen;border:1px dotted black;width:80%;border-collapse:collapse;>");

            strHTMLBuilder.Append("<tr style=background-color:orange;color:white;>");
            foreach (DataColumn myColumn in dt.Columns)
            {
                strHTMLBuilder.Append("<th style=padding:3px;>");
                strHTMLBuilder.Append(myColumn.ColumnName);
                strHTMLBuilder.Append("</th>");

            }
            strHTMLBuilder.Append("</tr>");


            foreach (DataRow myRow in dt.Rows)
            {

                strHTMLBuilder.Append("<tr >");
                foreach (DataColumn myColumn in dt.Columns)
                {
                    strHTMLBuilder.Append("<td nowrap=nowrap>");
                    strHTMLBuilder.Append(myRow[myColumn.ColumnName].ToString());
                    strHTMLBuilder.Append("</td>");

                }
                strHTMLBuilder.Append("</tr>");
            }

            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</body>");
            strHTMLBuilder.Append("</html>");

            string Htmltext = strHTMLBuilder.ToString();

            return Htmltext;
        }

        private void createDatatable()
        {
            dt.Columns.Add("GC Reference");
            dt.Columns.Add("VIN");
            dt.Columns.Add("Unique Reference");
            dt.Columns.Add("Category");
            dt.Columns.Add("Registration Plate");
            dt.Columns.Add("Make");
            dt.Columns.Add("Model");
            dt.Columns.Add("Status");

        }


        private void bbiSendSelected_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            GridView view = (GridView)activeEmailGridControl.MainView;
            view.PostEditor();
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedRecords = "";
            if (intCount > 0)
            {
                switch (XtraMessageBox.Show(String.Format("You are about to email {0} selected individual(s) on the Active Keeper (Valid Emails) - Tab Page. Do you want to proceed ?", intCount), "Mailing Selected Individuals", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.No:
                        // cancel operation //
                        XtraMessageBox.Show("The operation has been cancelled successfully.", "Emailing Cancelled", MessageBoxButtons.OK);
                        break;
                    case DialogResult.Yes:
                        // email selected //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            DataRow dr = view.GetDataRow(intRowHandle);
                            string strEmployeeNumber1 = ((DataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_ItemRow)(dr)).EmployeeNumber.ToString();
                            string strKeeperTypeID1 = ((DataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_ItemRow)(dr)).KeeperTypeID.ToString();
                            sendEmailList(SendEmailType.selected, strKeeperTypeID1, strEmployeeNumber1);                                          
                        }
                        break;  
                }
            }

           
        }

        private void sendEmail(int strRecordSelected, string strSendEmailTo, string strSendFullName, string strSummary, string strAssetList)
        {
            try
            {
                // Create a MailMessage object
                MailMessage mm = new MailMessage();
                // Define the sender and recipient
                mm.From = new MailAddress(strFromAddress, strFromAddressDisplay);
                mm.To.Add(new MailAddress(strSendEmailTo, strSendFullName));
                //mm.To.Add(new MailAddress("tafadzwa.mandengu@ground-control.co.uk", strSendFullName));
                mm.Bcc.Add(new MailAddress("tafadzwa.mandengu@ground-control.co.uk", "Taf"));

                // Define the subject and body
                mm.Subject = strAssetSubjectLine;

               // Stream htmlStream = new StreamReader();
                //Stream imageStream1 = null;
                //Stream imageStream2 = null;
                try
                {




                    //try
                    //{
                    //    Stream htmlStream = null;
                    //    var assembly = Assembly.GetExecutingAssembly();
                    //    htmlStream = assembly.GetManifestResourceStream(strAssetEmailHTMLLayoutFile);

                    //    var reader = new StreamReader(htmlStream);

                    string strBody = File.ReadAllText(strAssetEmailHTMLLayoutFile);
                    //    strBody = reader.ReadToEnd();
                    //}
                    //catch (FileNotFoundException ex)
                    //{
                    //    XtraMessageBox.Show("An error occurred reading an HTML email file. Please contact administrator.", "Error Reading File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //    errorOccured = true;
                    //    return;
                    //}


                    // Get the HTML from an embedded resource.
                    //htmlStream.read
                    //htmlStream = assembly.GetManifestResourceStream(strAssetEmailHTMLLayoutFile);

                    //// Perform replacements on the HTML file (if you're using it as a template).
                    //var reader = new StreamReader(htmlStream);
                    //var body = reader
                    //    .ReadToEnd()
                    strBody = strBody.Replace("Summary", strSummary);
                    strBody  = strBody.Replace("FullName", strSendFullName);
                    strBody = strBody.Replace("AssetList", strAssetList);
                    mm.Body = strBody;
                    // Create an alternate view and add it to the email.
                    //var altView = AlternateView.CreateAlternateViewFromString(strBody, null, System.Net.Mime.MediaTypeNames.Text.Html);
                    //mm.AlternateViews.Add(altView);     
                    //// Get the image from an embedded resource. The <img> tag in the HTML is:
                    ////     <img src="pid:IMAGE.PNG">
                    //string IMAGE_RESOURCE_PATH1 = string.Format(@"{0}\{1}\{2}", strAppBase,"AssetCheck_HTML_Layout_files","image001.jpg");
                    //string IMAGE_RESOURCE_PATH2 = string.Format(@"{0}\{1}\{2}", strAppBase,"AssetCheck_HTML_Layout_files","image002.jpg");
                    //imageStream1 = assembly.GetManifestResourceStream(IMAGE_RESOURCE_PATH1);
                    //imageStream2 = assembly.GetManifestResourceStream(IMAGE_RESOURCE_PATH2);
                    //var linkedImage = new LinkedResource(imageStream1, "image001/jpg") { ContentId = "image001.jpg" };
                    //altView.LinkedResources.Add(linkedImage);
                    //var linkedImage2 = new LinkedResource(imageStream2, "image002/jpg") { ContentId = "image002.jpg" };
                    //altView.LinkedResources.Add(linkedImage2);
 
                    //// Get the attachment from an embedded resource.
                    //fileStream = assembly.GetManifestResourceStream(FILE_RESOURCE_PATH);
                    //var file = new Attachment(fileStream, System.Net.Mime.MediaTypeNames.Application.Pdf);
                    //file.Name = "FILE.PDF";
                    //mm.Attachments.Add(file);
 
                    // Send the email
                    //var client = new SmtpClient(...);
                    //client.Credentials = new NetworkCredential(...);
                    //client.Send(mm);
                }
                catch (FileNotFoundException ex)
                {
                    XtraMessageBox.Show("An error occurred reading an HTML email file. Please contact administrator.", "Error Reading File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //finally
                //{
                //    //if (imageStream1 != null) imageStream1.Dispose();
                //    //if (imageStream2 != null) imageStream2.Dispose();
                //   // if (htmlStream != null) htmlStream.Dispose();
                //}
                mm.IsBodyHtml = true;

                ////create the LinkedResource (embedded image)
                //System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                //logo.ContentId = "companylogo";
                ////add the LinkedResource to the appropriate view
                //htmlView.LinkedResources.Add(logo);

                ////create the LinkedResource (embedded image)
                //System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                //logo2.ContentId = "companyfooter";
                ////add the LinkedResource to the appropriate view
                //htmlView.LinkedResources.Add(logo2);

                //msg.AlternateViews.Add(plainView);
                //msg.AlternateViews.Add(htmlView);

                // Configure the mail server
                SmtpClient sc = new SmtpClient(strSMTPMailServerAddress,Convert.ToInt32(strSMTPMailServerPort));
                sc.EnableSsl = false;
                if (!String.IsNullOrEmpty(strSMTPMailServerUsername)) sc.Credentials = new NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);

                // Send the message and notify the user of success
                sc.Send(mm);
                this.sp_AS_11112_Keeper_Email_ItemTableAdapter.Update("smtpmsg", strRecordSelected.ToString(), true, strRecordSelected, 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), false, "Message sent successfully.", false, 0, true);

                //MessageBox.Show("Message sent successfully.",
                //    "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException)
            {
                this.sp_AS_11112_Keeper_Email_ItemTableAdapter.Update("smtpmsg", strRecordSelected.ToString(), true, strRecordSelected, 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), true, "Invalid input, you must type from and to e-mail addresses." , false, 0, true);
                //MessageBox.Show("You must type from and to e-mail addresses",
                //    "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (FormatException)
            {
                this.sp_AS_11112_Keeper_Email_ItemTableAdapter.Update("smtpmsg", strRecordSelected.ToString(), true, strRecordSelected, 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), true, "Invalid input,you must provide valid from and to e-mail addresses.", false, 0, true);
                //MessageBox.Show("You must provide valid from and to e-mail addresses",
                //    "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (InvalidOperationException)
            {
                this.sp_AS_11112_Keeper_Email_ItemTableAdapter.Update("smtpmsg", strRecordSelected.ToString(), true, strRecordSelected, 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), true, "No SMTP server provided, please provide a server name.", false, 0, true);
                //MessageBox.Show("Please provide a server name.",
                //    "No SMTP server provided", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (SmtpFailedRecipientException)
            {
                this.sp_AS_11112_Keeper_Email_ItemTableAdapter.Update("smtpmsg", strRecordSelected.ToString(), true, strRecordSelected, 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), true, "Invalid recipient, the mail server says that there is no mailbox for " + strSendEmailTo + ".", false, 0, true);
                //MessageBox.Show("The mail server says that there is no mailbox for " + strSendEmailTo + ".",
                //    "Invalid recipient", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (SmtpException ex)
            {
                // Invalid hostnames result in a WebException InnerException that provides
                // a more descriptive error, so get the base exception
                Exception inner = ex.GetBaseException();
                this.sp_AS_11112_Keeper_Email_ItemTableAdapter.Update("smtpmsg", strRecordSelected.ToString(), true, strRecordSelected, 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), true, "Could not send message: " + inner.Message, false, 0, true);
                //MessageBox.Show("Could not send message: " + inner.Message,
                //    "Problem sending message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            updateKeeperList("edit");
            sp_AS_11111_Keeper_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11111_Keeper_List, null, null);
            sp_AS_11112_Keeper_Email_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_Item, null, null);
        }

        private void bbiFindNewKeeper_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            updateKeeperList("add");
        }

        private void bbiExportExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExportGrid("Excel");
        }

        private void bbiExportOld_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ExportGrid("Excelold");
        }

        private void ExportGrid(string strFormat)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();

            folderDlg.ShowNewFolderButton = true;

            DialogResult result = folderDlg.ShowDialog();

            Environment.SpecialFolder root = folderDlg.RootFolder;
            string FileName;
            GridControl Grid_To_Export;
            switch (i_int_FocusedGrid)
            {
                case 0://keeperGrid
                    Grid_To_Export = keeperGridControl;
                    break;
                case 1://activeEmailGrid
                    Grid_To_Export = activeEmailGridControl;
                    break;
                case 2://activeInvalid
                    Grid_To_Export = activeInvalidGridControl;
                    break;
                case 3://activeUntickedEmail
                    Grid_To_Export = activeUntickedEmailGridControl;
                    break;
                default:
                    Grid_To_Export = keeperGridControl;
                    break;
            }
            if (result == DialogResult.OK)
            {
                try
                {
                    if (strFormat == "Excelold")
                    {
                        FileName = string.Format("{0}\\{1}_{2:yyyy-MM-dd_HH_mm_ss}.xls", folderDlg.SelectedPath, Grid_To_Export.Tag, DateTime.Now);
                        Grid_To_Export.ExportToXls((FileName));
                    }
                    else if (strFormat == "Excel")
                    {
                        FileName = string.Format("{0}\\{1}_{2:yyyy-MM-dd_HH_mm_ss}.xlsx", folderDlg.SelectedPath, Grid_To_Export.Tag, DateTime.Now);
                        Grid_To_Export.ExportToXlsx((FileName));
                    }
                    
                }
                catch (Exception)
                {
                    XtraMessageBox.Show("Please contact ICT to upgrade or install a compatible  and enable the feature", "Compatibility Issue", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
           
        }
        
        private void bbiNextEmailDate_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;
            DateTime sendDate = Convert.ToDateTime(((DevExpress.XtraBars.BarEditItem)(sender)).EditValue);
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter updateSetting = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            updateSetting.ChangeConnectionString(strConnectionString);
            updateSetting.sp_AS_11115_Update_Settings("AssetAutoSendDate", (sendDate).ToString());
            
            DataSet_ATTableAdapters.QueriesTableAdapter GetNewSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetNewSetting.ChangeConnectionString(strConnectionString);
            autoSendDate = Convert.ToDateTime(GetNewSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetAutoSendDate"));

            //insert new date in schedule
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter updateSchedule = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            updateSchedule.ChangeConnectionString(strConnectionString);           
            updateSchedule.sp_AS_11161_Update_Email_Schedule_Date("add",autoSendDate);

            XtraMessageBox.Show("The Next Email Date has been successfully updated to " + autoSendDate.ToString("dd-MM-yyyy"), "Changes Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiAutoSend_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bool_FormLoading)
                return;
            bool sendChecked = Convert.ToBoolean(((DevExpress.XtraBars.BarEditItem)(e.Item)).EditValue);
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter updateSetting = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            updateSetting.ChangeConnectionString(strConnectionString);
            updateSetting.sp_AS_11115_Update_Settings("AssetAutoSendEmail", (Convert.ToInt16(!sendChecked)).ToString());
            bbiAutoSend.EditValue = !sendChecked;
            DataSet_ATTableAdapters.QueriesTableAdapter GetNewSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetNewSetting.ChangeConnectionString(strConnectionString);

            autoSendDate = Convert.ToDateTime(GetNewSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetAutoSendDate"));
            autoSendEmail = (Convert.ToInt32(GetNewSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetAutoSendEmail")) == 1 ? true : false);
            string strMessage ="";
            if (autoSendEmail)
            {
                DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter updateSchedule = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
                updateSchedule.ChangeConnectionString(strConnectionString);
                //insert new date in schedule
                updateSchedule.sp_AS_11161_Update_Email_Schedule_Date("add", autoSendDate);

                strMessage = "Emails will be automatically generated monthly starting on " + autoSendDate.ToString("dd-MM-yyyy");
                bbiNextEmailDate.Enabled = autoSendEmail;
            }
            else
            {
                //cancel schedule
                DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter cancelSchedule = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
                cancelSchedule.ChangeConnectionString(strConnectionString);
                cancelSchedule.sp_AS_11161_Update_Email_Schedule_Date("cancel", autoSendDate);

                strMessage = "You have successfully disabled the email auto generation option.";
                bbiNextEmailDate.Enabled = autoSendEmail;
            }
            XtraMessageBox.Show(strMessage , "Changes Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void hplAssetList_Click(object sender, EventArgs e)
        {
            string strKeeperType = "";
            string strKeeperID = "";
            DataRowView currentRow = null;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    currentRow = (DataRowView)bsAS11112KeeperActiveValid.Current;
                    break;
                case 2:
                    currentRow = (DataRowView)bsAS11112KeeperActiveInvalid.Current;
                    break;
                case 3:
                    currentRow = (DataRowView)bsAS11112KeeperActiveUnticked.Current;
                    break;
            }

            if (currentRow != null)
            {
                strKeeperType = currentRow["KeeperTypeID"].ToString();
                strKeeperID = currentRow["EmployeeNumber"].ToString();
            }
            mailTabControl.SelectedTabPage = keeperListPage;
            spAS11111KeeperListBindingSource.Filter = "[KeeperTypeID] = '" + strKeeperType + "'" + " and [EmployeeNumber] = '" + strKeeperID + "'";
        }

        private void bbiClearFilter_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            spAS11111KeeperListBindingSource.Filter = "";
        }

        private void mailTabControl_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (e.Page == keeperListPage)
            {
                bbiClearFilter.Enabled = true;
            }
            else
            {
                bbiClearFilter.Enabled = false;
            }
        }

        private void bbiRefreshAll_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadAdapters();
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);

            strFromAddress = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetFromAddress"));
            strFromAddressDisplay = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetFromAddressDisplay"));
            strAssetSubjectLine = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetReminderSubjectLine"));
            string strAppBase = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetEmailHTMLPath"));
            strAssetEmailHTMLLayoutFile = string.Format(@"{0}\{1}", strAppBase, Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetEmailHTMLLayoutFile")));
            strSMTPMailServerAddress = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerAddress"));
            strSMTPMailServerUsername = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerUsername"));
            strSMTPMailServerPassword = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerPassword"));
            strSMTPMailServerPort = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SMTPMailServerPort"));
            autoSendDate = Convert.ToDateTime(GetSetting.sp00043_RetrieveSingleSystemSetting(11, "AssetAutoSendDate"));
            loadSettings("load");
        }


        private void btnDown_Click(object sender, EventArgs e)
        {    
            moveRecordsAcross(activeEmailGridControl, false);
        }

        private void moveRecordsAcross(GridControl gridControl, bool blnSelected)
        {
            GridView view = null;
            strRecordsToLoad = "";
            int[] intRowHandles;
            int intCount = 0;
            string strRecordSelected = "";

            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            foreach (int intRowHandle in intRowHandles)
            {
                DataRow dr = view.GetDataRow(intRowHandle);
                strRecordsToLoad += (((WoodPlan5.DataSet_AS_DataEntry.sp_AS_11112_Keeper_Email_ItemRow)(dr)).EmailTrackerID).ToString() + ',';
            }

            if (strRecordsToLoad != "")
            {
                this.sp_AS_11112_Keeper_Email_ItemTableAdapter.Update("selected", strRecordsToLoad, blnSelected, Convert.ToInt32(0), 0, "", "", 0, 0, "", "", getCurrentDate(), getCurrentDate(), false, "", false, 0, true);
            }

            RefreshAdapters();
        }

        private void btnMoveUP_Click(object sender, EventArgs e)
        {
            moveRecordsAcross(activeUntickedEmailGridControl, true);           
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            Linked_Documents_To_Record();
        }

        private void Linked_Documents_To_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Keeper //
                    //if (!iBool_AllowEditKeeper )
                    //    return;

                    // Drilldown to Linked Document Manager //
                    GridView view = keeperGridView;
                    int intRecordType = 201;  // Asset -  //
                    int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, colKeeperAllocationID));

                    string startDate = "Unknown Date";
                    if (view.GetRowCellValue(view.FocusedRowHandle, colAllocationDate) != DBNull.Value)
                    {
                        startDate = (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, colAllocationDate)).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date");
                    }

                    string endDate = "Unknown Date";
                    if (view.GetRowCellValue(view.FocusedRowHandle, colAllocationEndDate) != DBNull.Value)
                    {
                        endDate = (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, colAllocationEndDate)).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date");
                    }

                    string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, colAssetMiniDescription).ToString()
                        + " :"
                        + view.GetRowCellValue(view.FocusedRowHandle, colFullName1).ToString()
                        + ", Duration: "
                        + startDate
                        + " - "
                        + endDate;
                    Linked_Document_Drill_Down(intRecordType, intRecordID, strRecordDescription);

                    break;
                default:
                    break;
            }
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";

            System.Reflection.MethodInfo method = null;

            var frmChild = new frm_AT_Linked_Document_Manager(this.FormID);
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            frmChild.splashScreenManager = splashScreenManager1;
            frmChild.splashScreenManager.ShowWaitForm();

            frmChild.MdiParent = this.MdiParent;

            frmChild.GlobalSettings = this.GlobalSettings;
            frmChild.strPassedInLinkedDocumentIDs = strRecordIDs;
            frmChild.intPassedInRecordTypeID = intRecordType;
            frmChild.intPassedInLinkedToRecordID = intRecordID;
            frmChild.strPassedInLinkedToRecord = strRecordDescription;
            frmChild.Show();

            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmChild, new object[] { null });

            frmChild.LinkedDocumentsUpdated += new System.EventHandler(this.LinkedDocumentsUpdated);
        }

        private void LinkedDocumentsUpdated(object sender, EventArgs e)
        {
            //Just want to refresh the Keeper Grid
            sp_AS_11111_Keeper_ListTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11111_Keeper_List, null, null);
        }

        private void keeperGridView_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void keeperGridView_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }
    }
}


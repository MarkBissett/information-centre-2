﻿namespace WoodPlan5
{
    partial class frm_AS_Rental_Details_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Rental_Details_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11108RentalDetailsItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.lueRentalCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLRentalCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueRentalStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLRentalStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueRentalSpecificationID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLRentalSpecificationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spnRate = new DevExpress.XtraEditors.SpinEdit();
            this.txtCustomerReference = new DevExpress.XtraEditors.TextEdit();
            this.deStartDate = new DevExpress.XtraEditors.DateEdit();
            this.deEndDate = new DevExpress.XtraEditors.DateEdit();
            this.ceSetAsMain = new DevExpress.XtraEditors.CheckEdit();
            this.meNotes = new DevExpress.XtraEditors.MemoEdit();
            this.deActualEndDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRentalStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCustomerReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRentalSpecificationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRentalCategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSetAsMain = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11000_PL_Rental_CategoryTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_CategoryTableAdapter();
            this.sp_AS_11000_PL_Rental_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_StatusTableAdapter();
            this.sp_AS_11000_PL_Rental_SpecificationTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_SpecificationTableAdapter();
            this.sp_AS_11108_Rental_Details_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11108_Rental_Details_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11108RentalDetailsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalSpecificationID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalSpecificationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSetAsMain.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActualEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActualEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalSpecificationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSetAsMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(797, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 454);
            this.barDockControlBottom.Size = new System.Drawing.Size(797, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 428);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(797, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 428);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(797, 428);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(145, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(241, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11108RentalDetailsItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(157, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(237, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // spAS11108RentalDetailsItemBindingSource
            // 
            this.spAS11108RentalDetailsItemBindingSource.DataMember = "sp_AS_11108_Rental_Details_Item";
            this.spAS11108RentalDetailsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEquipmentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueRentalCategoryID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueRentalStatusID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueRentalSpecificationID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnRate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtCustomerReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deStartDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deEndDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceSetAsMain);
            this.editFormDataLayoutControlGroup.Controls.Add(this.meNotes);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deActualEndDate);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11108RentalDetailsItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2419, 301, 388, 512);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(797, 428);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(152, 35);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(244, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling1);
            this.txtEquipmentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEquipmentReference.TabIndex = 123;
            // 
            // lueRentalCategoryID
            // 
            this.lueRentalCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "RentalCategoryID", true));
            this.lueRentalCategoryID.Location = new System.Drawing.Point(540, 35);
            this.lueRentalCategoryID.MenuManager = this.barManager1;
            this.lueRentalCategoryID.Name = "lueRentalCategoryID";
            this.lueRentalCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueRentalCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Category", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueRentalCategoryID.Properties.DataSource = this.spAS11000PLRentalCategoryBindingSource;
            this.lueRentalCategoryID.Properties.DisplayMember = "Value";
            this.lueRentalCategoryID.Properties.NullText = "";
            this.lueRentalCategoryID.Properties.NullValuePrompt = "-- Please  Select Category --";
            this.lueRentalCategoryID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueRentalCategoryID.Properties.ValueMember = "PickListID";
            this.lueRentalCategoryID.Size = new System.Drawing.Size(245, 20);
            this.lueRentalCategoryID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueRentalCategoryID.TabIndex = 124;
            this.lueRentalCategoryID.Tag = "Rental Category";
            this.lueRentalCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lueRentalCategoryID_Validating);
            // 
            // spAS11000PLRentalCategoryBindingSource
            // 
            this.spAS11000PLRentalCategoryBindingSource.DataMember = "sp_AS_11000_PL_Rental_Category";
            this.spAS11000PLRentalCategoryBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueRentalStatusID
            // 
            this.lueRentalStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "RentalStatusID", true));
            this.lueRentalStatusID.Location = new System.Drawing.Point(540, 59);
            this.lueRentalStatusID.MenuManager = this.barManager1;
            this.lueRentalStatusID.Name = "lueRentalStatusID";
            this.lueRentalStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueRentalStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Rental Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueRentalStatusID.Properties.DataSource = this.spAS11000PLRentalStatusBindingSource;
            this.lueRentalStatusID.Properties.DisplayMember = "Value";
            this.lueRentalStatusID.Properties.NullText = "";
            this.lueRentalStatusID.Properties.NullValuePrompt = "-- Please Select Status --";
            this.lueRentalStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueRentalStatusID.Properties.ValueMember = "PickListID";
            this.lueRentalStatusID.Size = new System.Drawing.Size(245, 20);
            this.lueRentalStatusID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueRentalStatusID.TabIndex = 125;
            this.lueRentalStatusID.Tag = "Status";
            this.lueRentalStatusID.EditValueChanged += new System.EventHandler(this.lueRentalStatusID_EditValueChanged);
            this.lueRentalStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.lueRentalStatusID_Validating);
            // 
            // spAS11000PLRentalStatusBindingSource
            // 
            this.spAS11000PLRentalStatusBindingSource.DataMember = "sp_AS_11000_PL_Rental_Status";
            this.spAS11000PLRentalStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueRentalSpecificationID
            // 
            this.lueRentalSpecificationID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "RentalSpecificationID", true));
            this.lueRentalSpecificationID.Location = new System.Drawing.Point(540, 131);
            this.lueRentalSpecificationID.MenuManager = this.barManager1;
            this.lueRentalSpecificationID.Name = "lueRentalSpecificationID";
            this.lueRentalSpecificationID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueRentalSpecificationID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Specification", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueRentalSpecificationID.Properties.DataSource = this.spAS11000PLRentalSpecificationBindingSource;
            this.lueRentalSpecificationID.Properties.DisplayMember = "Value";
            this.lueRentalSpecificationID.Properties.NullText = "";
            this.lueRentalSpecificationID.Properties.NullValuePrompt = "-- Please Select Specification --";
            this.lueRentalSpecificationID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueRentalSpecificationID.Properties.ValueMember = "PickListID";
            this.lueRentalSpecificationID.Size = new System.Drawing.Size(245, 20);
            this.lueRentalSpecificationID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueRentalSpecificationID.TabIndex = 126;
            this.lueRentalSpecificationID.Tag = "Specification";
            this.lueRentalSpecificationID.Validating += new System.ComponentModel.CancelEventHandler(this.lueRentalSpecificationID_Validating);
            // 
            // spAS11000PLRentalSpecificationBindingSource
            // 
            this.spAS11000PLRentalSpecificationBindingSource.DataMember = "sp_AS_11000_PL_Rental_Specification";
            this.spAS11000PLRentalSpecificationBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spnRate
            // 
            this.spnRate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "Rate", true));
            this.spnRate.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnRate.Location = new System.Drawing.Point(152, 83);
            this.spnRate.MenuManager = this.barManager1;
            this.spnRate.Name = "spnRate";
            this.spnRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnRate.Properties.DisplayFormat.FormatString = "c2";
            this.spnRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnRate.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnRate.Size = new System.Drawing.Size(244, 20);
            this.spnRate.StyleController = this.editFormDataLayoutControlGroup;
            this.spnRate.TabIndex = 127;
            this.spnRate.Tag = "Rate";
            this.spnRate.Validating += new System.ComponentModel.CancelEventHandler(this.spnRate_Validating);
            // 
            // txtCustomerReference
            // 
            this.txtCustomerReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "CustomerReference", true));
            this.txtCustomerReference.Location = new System.Drawing.Point(540, 83);
            this.txtCustomerReference.MenuManager = this.barManager1;
            this.txtCustomerReference.Name = "txtCustomerReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtCustomerReference, true);
            this.txtCustomerReference.Size = new System.Drawing.Size(245, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtCustomerReference, optionsSpelling2);
            this.txtCustomerReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtCustomerReference.TabIndex = 128;
            this.txtCustomerReference.Tag = "Customer Order Reference";
            this.txtCustomerReference.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustomerReference_Validating);
            // 
            // deStartDate
            // 
            this.deStartDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "StartDate", true));
            this.deStartDate.EditValue = null;
            this.deStartDate.Location = new System.Drawing.Point(152, 107);
            this.deStartDate.MenuManager = this.barManager1;
            this.deStartDate.Name = "deStartDate";
            this.deStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStartDate.Size = new System.Drawing.Size(244, 20);
            this.deStartDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deStartDate.TabIndex = 129;
            this.deStartDate.Tag = "Start Date";
            this.deStartDate.Validating += new System.ComponentModel.CancelEventHandler(this.deStartDate_Validating);
            // 
            // deEndDate
            // 
            this.deEndDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "EndDate", true));
            this.deEndDate.EditValue = null;
            this.deEndDate.Location = new System.Drawing.Point(540, 107);
            this.deEndDate.MenuManager = this.barManager1;
            this.deEndDate.Name = "deEndDate";
            this.deEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEndDate.Size = new System.Drawing.Size(245, 20);
            this.deEndDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deEndDate.TabIndex = 131;
            this.deEndDate.Tag = "End Date";
            this.deEndDate.EditValueChanged += new System.EventHandler(this.deEndDate_EditValueChanged);
            this.deEndDate.Validating += new System.ComponentModel.CancelEventHandler(this.deEndDate_Validating);
            // 
            // ceSetAsMain
            // 
            this.ceSetAsMain.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "SetAsMain", true));
            this.ceSetAsMain.Location = new System.Drawing.Point(12, 59);
            this.ceSetAsMain.MenuManager = this.barManager1;
            this.ceSetAsMain.Name = "ceSetAsMain";
            this.ceSetAsMain.Properties.Caption = "Set As Main :";
            this.ceSetAsMain.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ceSetAsMain.Size = new System.Drawing.Size(384, 19);
            this.ceSetAsMain.StyleController = this.editFormDataLayoutControlGroup;
            this.ceSetAsMain.TabIndex = 132;
            this.ceSetAsMain.Tag = "Main";
            this.ceSetAsMain.Validating += new System.ComponentModel.CancelEventHandler(this.ceSetAsMain_Validating);
            // 
            // meNotes
            // 
            this.meNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "Notes", true));
            this.meNotes.Location = new System.Drawing.Point(24, 191);
            this.meNotes.MenuManager = this.barManager1;
            this.meNotes.Name = "meNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.meNotes, true);
            this.meNotes.Size = new System.Drawing.Size(749, 213);
            this.scSpellChecker.SetSpellCheckerOptions(this.meNotes, optionsSpelling3);
            this.meNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.meNotes.TabIndex = 133;
            this.meNotes.Tag = "Notes";
            // 
            // deActualEndDate
            // 
            this.deActualEndDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11108RentalDetailsItemBindingSource, "ActualEndDate", true));
            this.deActualEndDate.EditValue = null;
            this.deActualEndDate.Location = new System.Drawing.Point(152, 131);
            this.deActualEndDate.MenuManager = this.barManager1;
            this.deActualEndDate.Name = "deActualEndDate";
            this.deActualEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deActualEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deActualEndDate.Size = new System.Drawing.Size(244, 20);
            this.deActualEndDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deActualEndDate.TabIndex = 134;
            this.deActualEndDate.Validating += new System.ComponentModel.CancelEventHandler(this.deActualEndDate_Validating);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRate,
            this.ItemForStartDate,
            this.layoutControlGroup2,
            this.ItemForActualEndDate,
            this.ItemForRentalStatusID,
            this.ItemForCustomerReference,
            this.ItemForEndDate,
            this.ItemForRentalSpecificationID,
            this.ItemForEquipmentReference,
            this.ItemForRentalCategoryID,
            this.ItemForSetAsMain});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(777, 385);
            // 
            // ItemForRate
            // 
            this.ItemForRate.Control = this.spnRate;
            this.ItemForRate.CustomizationFormText = "Rate";
            this.ItemForRate.Location = new System.Drawing.Point(0, 48);
            this.ItemForRate.Name = "ItemForRate";
            this.ItemForRate.Size = new System.Drawing.Size(388, 24);
            this.ItemForRate.Text = "Daily Rate :";
            this.ItemForRate.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.deStartDate;
            this.ItemForStartDate.CustomizationFormText = "Start Date";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 72);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(388, 24);
            this.ItemForStartDate.Text = "Start Date :";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(137, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup2.CaptionImage")));
            this.layoutControlGroup2.CustomizationFormText = "Notes";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 120);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(777, 265);
            this.layoutControlGroup2.Text = "Notes";
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.meNotes;
            this.ItemForNotes.CustomizationFormText = "Notes";
            this.ItemForNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(753, 217);
            this.ItemForNotes.Text = "Notes";
            this.ItemForNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNotes.TextVisible = false;
            // 
            // ItemForActualEndDate
            // 
            this.ItemForActualEndDate.Control = this.deActualEndDate;
            this.ItemForActualEndDate.CustomizationFormText = "Actual End Date";
            this.ItemForActualEndDate.Location = new System.Drawing.Point(0, 96);
            this.ItemForActualEndDate.Name = "ItemForActualEndDate";
            this.ItemForActualEndDate.Size = new System.Drawing.Size(388, 24);
            this.ItemForActualEndDate.Text = "Actual End Date";
            this.ItemForActualEndDate.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForRentalStatusID
            // 
            this.ItemForRentalStatusID.Control = this.lueRentalStatusID;
            this.ItemForRentalStatusID.CustomizationFormText = "Rental Status ID";
            this.ItemForRentalStatusID.Location = new System.Drawing.Point(388, 24);
            this.ItemForRentalStatusID.Name = "ItemForRentalStatusID";
            this.ItemForRentalStatusID.Size = new System.Drawing.Size(389, 24);
            this.ItemForRentalStatusID.Text = "Rental Status :";
            this.ItemForRentalStatusID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForCustomerReference
            // 
            this.ItemForCustomerReference.Control = this.txtCustomerReference;
            this.ItemForCustomerReference.CustomizationFormText = "Customer Reference";
            this.ItemForCustomerReference.Location = new System.Drawing.Point(388, 48);
            this.ItemForCustomerReference.Name = "ItemForCustomerReference";
            this.ItemForCustomerReference.Size = new System.Drawing.Size(389, 24);
            this.ItemForCustomerReference.Text = "Customer Order Reference :";
            this.ItemForCustomerReference.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.deEndDate;
            this.ItemForEndDate.CustomizationFormText = "End Date";
            this.ItemForEndDate.Location = new System.Drawing.Point(388, 72);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(389, 24);
            this.ItemForEndDate.Text = "End Date :";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForRentalSpecificationID
            // 
            this.ItemForRentalSpecificationID.Control = this.lueRentalSpecificationID;
            this.ItemForRentalSpecificationID.CustomizationFormText = "Rental Specification ID";
            this.ItemForRentalSpecificationID.Location = new System.Drawing.Point(388, 96);
            this.ItemForRentalSpecificationID.Name = "ItemForRentalSpecificationID";
            this.ItemForRentalSpecificationID.Size = new System.Drawing.Size(389, 24);
            this.ItemForRentalSpecificationID.Text = "Rental Specification :";
            this.ItemForRentalSpecificationID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.txtEquipmentReference;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(388, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference :";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForRentalCategoryID
            // 
            this.ItemForRentalCategoryID.Control = this.lueRentalCategoryID;
            this.ItemForRentalCategoryID.CustomizationFormText = "Rental Category ID";
            this.ItemForRentalCategoryID.Location = new System.Drawing.Point(388, 0);
            this.ItemForRentalCategoryID.Name = "ItemForRentalCategoryID";
            this.ItemForRentalCategoryID.Size = new System.Drawing.Size(389, 24);
            this.ItemForRentalCategoryID.Text = "Rental Category :";
            this.ItemForRentalCategoryID.TextSize = new System.Drawing.Size(137, 13);
            // 
            // ItemForSetAsMain
            // 
            this.ItemForSetAsMain.Control = this.ceSetAsMain;
            this.ItemForSetAsMain.CustomizationFormText = "Set As Main";
            this.ItemForSetAsMain.Location = new System.Drawing.Point(0, 24);
            this.ItemForSetAsMain.Name = "ItemForSetAsMain";
            this.ItemForSetAsMain.Size = new System.Drawing.Size(388, 24);
            this.ItemForSetAsMain.Text = "Set As Main";
            this.ItemForSetAsMain.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForSetAsMain.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(145, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(386, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(391, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11000_PL_Rental_CategoryTableAdapter
            // 
            this.sp_AS_11000_PL_Rental_CategoryTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Rental_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Rental_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Rental_SpecificationTableAdapter
            // 
            this.sp_AS_11000_PL_Rental_SpecificationTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11108_Rental_Details_ItemTableAdapter
            // 
            this.sp_AS_11108_Rental_Details_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Rental_Details_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(797, 484);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Rental_Details_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Rental Detail";
            this.Activated += new System.EventHandler(this.frm_AS_Rental_Details_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Rental_Details_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Rental_Details_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11108RentalDetailsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRentalSpecificationID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLRentalSpecificationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSetAsMain.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActualEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deActualEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCustomerReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalSpecificationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRentalCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSetAsMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private DevExpress.XtraEditors.LookUpEdit lueRentalCategoryID;
        private DevExpress.XtraEditors.LookUpEdit lueRentalStatusID;
        private DevExpress.XtraEditors.LookUpEdit lueRentalSpecificationID;
        private DevExpress.XtraEditors.SpinEdit spnRate;
        private DevExpress.XtraEditors.TextEdit txtCustomerReference;
        private DevExpress.XtraEditors.DateEdit deStartDate;
        private DevExpress.XtraEditors.DateEdit deEndDate;
        private DevExpress.XtraEditors.CheckEdit ceSetAsMain;
        private DevExpress.XtraEditors.MemoEdit meNotes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRentalCategoryID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRentalStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRentalSpecificationID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCustomerReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSetAsMain;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private System.Windows.Forms.BindingSource spAS11000PLRentalCategoryBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLRentalStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLRentalSpecificationBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_CategoryTableAdapter sp_AS_11000_PL_Rental_CategoryTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_StatusTableAdapter sp_AS_11000_PL_Rental_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Rental_SpecificationTableAdapter sp_AS_11000_PL_Rental_SpecificationTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private System.Windows.Forms.BindingSource spAS11108RentalDetailsItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11108_Rental_Details_ItemTableAdapter sp_AS_11108_Rental_Details_ItemTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.DateEdit deActualEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualEndDate;


    }
}

namespace WoodPlan5
{
    partial class frm_AS_Office_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Office_Manager));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.equipmentGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11092OfficeManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_Core = new WoodPlan5.DataSet_AS_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.equipmentGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUniqueItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMake = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAvailability = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplier = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGCMarked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActiveKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepreciationStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExchequerReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSageAssetReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArchive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArchiveDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOccurrenceFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOccursUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumOccurrence = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDayOfMonthApplyDepreciation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOfficeCategoryID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOfficeCategory1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepreciationApplied = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentValue2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepreciationPeriodNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentBilling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ceCheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.moneyTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.equipmentChildTabControl = new DevExpress.XtraTab.XtraTabControl();
            this.vehicleTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.vehicleGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11005VehicleItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.vehicleGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVehicleCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationPlate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLogBookNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModelSpecifications = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEngineSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmissions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTyreNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTowbarTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTowbarType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegistrationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMOTDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentMileage1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceDueMileage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRadioCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElectronicCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.NoDetailsTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.plantTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.plantGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11008PlantItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.plantGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentID6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShortDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlantCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEngineSize1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gadgetTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.gadgetGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11026GadgetItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gadgetGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGadgetTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGadgetType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCasing_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCasing = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.hardwareTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.hardwareGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11032HardwareItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.hardwareGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentID8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHardwareTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHardwareType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecification = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemHyperLinkEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.softwareTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.softwareGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11035SoftwareItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.softwareGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentID9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVersionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLicence = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colParentProgram = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcquisationMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcquisationMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantityLimit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLicenceKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLicenceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLicenceTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValidUntil = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemHyperLinkEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.officeTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.officeGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11029OfficeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.officeGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentID10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOfficeCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOfficeCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemHyperLinkEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.keeperTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.keeperGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11044KeeperAllocationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.keeperGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colKeeperAllocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeper = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueKeeperNotes = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.transactionTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.transactionGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11041TransactionItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.transactionsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTransactionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchaseInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedOnExchequer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetTransactionPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ceOnExchequer = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.billingTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.billingGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11050DepreciationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.billingGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepreciationID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNarrative1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceSheetCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfitLossCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepreciationAmount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPreviousValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowEdit1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.notificationTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.notificationGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11126NotificationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.notificationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNotificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotificationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriorityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateToRemind = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.depreciationTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.depreciationGridControl = new DevExpress.XtraGrid.GridControl();
            this.depreciationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepreciationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNarrative = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceSheetCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProfitLossCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepreciationAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEditDep = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPreviousValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPeriodEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowEdit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.workDetailTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.workDetailGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11078WorkDetailItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.workDetailGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkDetailID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransactionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentMileage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkCompletionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceIntervalID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.incidentTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.incidentGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11081IncidentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.incidentGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateHappened = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRepairDueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRepairDue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeverityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSeverity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWitness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAtFaultID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKeeperAtFault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFollowUpDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.incidentNotesMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.purposeTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.purposeGridControl = new DevExpress.XtraGrid.GridControl();
            this.spAS11084PurposeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.purposeGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEquipmentPurposeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentReference13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurpose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurposeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemMemoExEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.LinkedDocumentsTabPage = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlLinkedDocs = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridViewLinkedDocs = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocs = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLinkedDocDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEditLinkedDocs = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDocumentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spAS11129SoftwareAssignItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11054ServiceIntervalsItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spAS11047EquipmentBillingItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp09001_Employee_ManagerTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp09001_Employee_ManagerTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.tableAdapterManager1 = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.spAS11002EquipmentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11002_Equipment_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter();
            this.sp_AS_11008_Plant_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11008_Plant_ItemTableAdapter();
            this.sp_AS_11026_Gadget_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11026_Gadget_ItemTableAdapter();
            this.sp_AS_11032_Hardware_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11032_Hardware_ItemTableAdapter();
            this.sp_AS_11035_Software_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11035_Software_ItemTableAdapter();
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter();
            this.sp_AS_11041_Transaction_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter();
            this.sp_AS_11047_Equipment_Billing_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11047_Equipment_Billing_ItemTableAdapter();
            this.sp_AS_11050_Depreciation_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11050_Depreciation_ItemTableAdapter();
            this.sp_AS_11078_Work_Detail_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter();
            this.sp_AS_11084_Purpose_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11084_Purpose_ItemTableAdapter();
            this.sp_AS_11044_Keeper_Allocation_ListTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ListTableAdapter();
            this.sp_AS_11029_Office_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11029_Office_ItemTableAdapter();
            this.sp_AS_11081_Incident_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter();
            this.sp_AS_11005_Vehicle_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter();
            this.sp_AS_11126_Notification_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11126_Notification_ItemTableAdapter();
            this.sp_AS_11129_Software_Assign_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11129_Software_Assign_ItemTableAdapter();
            this.sp_AS_11092_Office_ManagerTableAdapter = new WoodPlan5.DataSet_AS_CoreTableAdapters.sp_AS_11092_Office_ManagerTableAdapter();
            this.sp_AS_11054_Service_Intervals_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Intervals_ItemTableAdapter();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiEmail = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11092OfficeManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceCheckEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentChildTabControl)).BeginInit();
            this.equipmentChildTabControl.SuspendLayout();
            this.vehicleTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11005VehicleItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            this.NoDetailsTabPage.SuspendLayout();
            this.plantTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.plantGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11008PlantItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            this.gadgetTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gadgetGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11026GadgetItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gadgetGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            this.hardwareTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hardwareGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11032HardwareItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hardwareGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).BeginInit();
            this.softwareTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.softwareGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11035SoftwareItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.softwareGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit5)).BeginInit();
            this.officeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.officeGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11029OfficeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).BeginInit();
            this.keeperTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperNotes)).BeginInit();
            this.transactionTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transactionGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOnExchequer)).BeginInit();
            this.billingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.billingGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11050DepreciationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            this.notificationTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11126NotificationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).BeginInit();
            this.depreciationTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEditDep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            this.workDetailTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).BeginInit();
            this.incidentTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentNotesMemoEdit)).BeginInit();
            this.purposeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11084PurposeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).BeginInit();
            this.LinkedDocumentsTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLinkedDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLinkedDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLinkedDocDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditLinkedDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11129SoftwareAssignItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceIntervalsItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11047EquipmentBillingItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11002EquipmentItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1362, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 562);
            this.barDockControlBottom.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 520);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1362, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 520);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiEmail});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 42);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.equipmentChildTabControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1362, 520);
            this.splitContainerControl1.SplitterPosition = 176;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.equipmentGridControl;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.equipmentGridControl);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1362, 338);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // equipmentGridControl
            // 
            this.equipmentGridControl.DataSource = this.spAS11092OfficeManagerBindingSource;
            this.equipmentGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.equipmentGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.equipmentGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, false, true, "Show On Map", "showOnMap")});
            this.equipmentGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.equipmentGridControl.Location = new System.Drawing.Point(0, 0);
            this.equipmentGridControl.MainView = this.equipmentGridView;
            this.equipmentGridControl.MenuManager = this.barManager1;
            this.equipmentGridControl.Name = "equipmentGridControl";
            this.equipmentGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEditDate,
            this.ceCheckEdit,
            this.moneyTextEdit4});
            this.equipmentGridControl.Size = new System.Drawing.Size(1362, 338);
            this.equipmentGridControl.TabIndex = 1;
            this.equipmentGridControl.UseEmbeddedNavigator = true;
            this.equipmentGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.equipmentGridView});
            // 
            // spAS11092OfficeManagerBindingSource
            // 
            this.spAS11092OfficeManagerBindingSource.DataMember = "sp_AS_11092_Office_Manager";
            this.spAS11092OfficeManagerBindingSource.DataSource = this.dataSet_AS_Core;
            // 
            // dataSet_AS_Core
            // 
            this.dataSet_AS_Core.DataSetName = "DataSet_AS_Core";
            this.dataSet_AS_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("country_16x16.png", "images/miscellaneous/country_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/country_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "country_16x16.png");
            // 
            // equipmentGridView
            // 
            this.equipmentGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentID4,
            this.colEquipmentReference2,
            this.colEquipmentCategoryID,
            this.colEquipmentCategory,
            this.colOwnershipStatus,
            this.colUniqueItemDescription,
            this.colMake,
            this.colModel,
            this.colAvailability,
            this.colSupplier,
            this.colGCMarked,
            this.colLastKeeper,
            this.colActiveKeeper,
            this.colDepreciationStartDate,
            this.colExchequerReference,
            this.colSageAssetReference,
            this.colArchive,
            this.colArchiveDate,
            this.colOccurrenceFrequency,
            this.colOccursUnitDescriptor,
            this.colMaximumOccurrence,
            this.colDayOfMonthApplyDepreciation,
            this.colItemDescription,
            this.colOfficeCategoryID1,
            this.colOfficeCategory1,
            this.colQuantity1,
            this.colDepreciationApplied,
            this.colCurrentValue2,
            this.colDepreciationPeriodNumber,
            this.colCurrentBilling,
            this.colCurrentCompany,
            this.colCurrentCostCentre,
            this.colCurrentDepartment,
            this.colNotes,
            this.colDateAdded,
            this.colMode17,
            this.colRecordID17});
            this.equipmentGridView.GridControl = this.equipmentGridControl;
            this.equipmentGridView.Name = "equipmentGridView";
            this.equipmentGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.equipmentGridView.OptionsFind.AlwaysVisible = true;
            this.equipmentGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.equipmentGridView.OptionsLayout.StoreAppearance = true;
            this.equipmentGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.equipmentGridView.OptionsSelection.MultiSelect = true;
            this.equipmentGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.equipmentGridView.OptionsView.ColumnAutoWidth = false;
            this.equipmentGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.equipmentGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSupplier, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.equipmentGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.equipmentGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.equipmentView_SelectionChanged);
            this.equipmentGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.equipmentGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.equipmentGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.equipmentGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentID4
            // 
            this.colEquipmentID4.FieldName = "EquipmentID";
            this.colEquipmentID4.Name = "colEquipmentID4";
            this.colEquipmentID4.OptionsColumn.AllowEdit = false;
            this.colEquipmentID4.OptionsColumn.AllowFocus = false;
            this.colEquipmentID4.OptionsColumn.ReadOnly = true;
            this.colEquipmentID4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID4.Width = 85;
            // 
            // colEquipmentReference2
            // 
            this.colEquipmentReference2.FieldName = "EquipmentReference";
            this.colEquipmentReference2.Name = "colEquipmentReference2";
            this.colEquipmentReference2.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference2.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference2.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference2.Visible = true;
            this.colEquipmentReference2.VisibleIndex = 0;
            this.colEquipmentReference2.Width = 124;
            // 
            // colEquipmentCategoryID
            // 
            this.colEquipmentCategoryID.FieldName = "EquipmentCategoryID";
            this.colEquipmentCategoryID.Name = "colEquipmentCategoryID";
            this.colEquipmentCategoryID.OptionsColumn.AllowEdit = false;
            this.colEquipmentCategoryID.OptionsColumn.AllowFocus = false;
            this.colEquipmentCategoryID.OptionsColumn.ReadOnly = true;
            this.colEquipmentCategoryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentCategoryID.Width = 133;
            // 
            // colEquipmentCategory
            // 
            this.colEquipmentCategory.FieldName = "EquipmentCategory";
            this.colEquipmentCategory.Name = "colEquipmentCategory";
            this.colEquipmentCategory.OptionsColumn.AllowEdit = false;
            this.colEquipmentCategory.OptionsColumn.AllowFocus = false;
            this.colEquipmentCategory.OptionsColumn.ReadOnly = true;
            this.colEquipmentCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentCategory.Visible = true;
            this.colEquipmentCategory.VisibleIndex = 1;
            this.colEquipmentCategory.Width = 119;
            // 
            // colOwnershipStatus
            // 
            this.colOwnershipStatus.FieldName = "OwnershipStatus";
            this.colOwnershipStatus.Name = "colOwnershipStatus";
            this.colOwnershipStatus.OptionsColumn.AllowEdit = false;
            this.colOwnershipStatus.OptionsColumn.AllowFocus = false;
            this.colOwnershipStatus.OptionsColumn.ReadOnly = true;
            this.colOwnershipStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOwnershipStatus.Width = 106;
            // 
            // colUniqueItemDescription
            // 
            this.colUniqueItemDescription.FieldName = "UniqueItemDescription";
            this.colUniqueItemDescription.Name = "colUniqueItemDescription";
            this.colUniqueItemDescription.OptionsColumn.AllowEdit = false;
            this.colUniqueItemDescription.OptionsColumn.AllowFocus = false;
            this.colUniqueItemDescription.OptionsColumn.ReadOnly = true;
            this.colUniqueItemDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUniqueItemDescription.Visible = true;
            this.colUniqueItemDescription.VisibleIndex = 3;
            this.colUniqueItemDescription.Width = 135;
            // 
            // colMake
            // 
            this.colMake.FieldName = "Make";
            this.colMake.Name = "colMake";
            this.colMake.OptionsColumn.AllowEdit = false;
            this.colMake.OptionsColumn.AllowFocus = false;
            this.colMake.OptionsColumn.ReadOnly = true;
            this.colMake.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMake.Visible = true;
            this.colMake.VisibleIndex = 4;
            // 
            // colModel
            // 
            this.colModel.FieldName = "Model";
            this.colModel.Name = "colModel";
            this.colModel.OptionsColumn.AllowEdit = false;
            this.colModel.OptionsColumn.AllowFocus = false;
            this.colModel.OptionsColumn.ReadOnly = true;
            this.colModel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colModel.Visible = true;
            this.colModel.VisibleIndex = 5;
            // 
            // colAvailability
            // 
            this.colAvailability.FieldName = "Availability";
            this.colAvailability.Name = "colAvailability";
            this.colAvailability.OptionsColumn.AllowEdit = false;
            this.colAvailability.OptionsColumn.AllowFocus = false;
            this.colAvailability.OptionsColumn.ReadOnly = true;
            this.colAvailability.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAvailability.Visible = true;
            this.colAvailability.VisibleIndex = 8;
            // 
            // colSupplier
            // 
            this.colSupplier.FieldName = "Supplier";
            this.colSupplier.Name = "colSupplier";
            this.colSupplier.OptionsColumn.AllowEdit = false;
            this.colSupplier.OptionsColumn.AllowFocus = false;
            this.colSupplier.OptionsColumn.ReadOnly = true;
            this.colSupplier.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplier.Visible = true;
            this.colSupplier.VisibleIndex = 11;
            // 
            // colGCMarked
            // 
            this.colGCMarked.FieldName = "GCMarked";
            this.colGCMarked.Name = "colGCMarked";
            this.colGCMarked.OptionsColumn.AllowEdit = false;
            this.colGCMarked.OptionsColumn.AllowFocus = false;
            this.colGCMarked.OptionsColumn.ReadOnly = true;
            this.colGCMarked.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGCMarked.Visible = true;
            this.colGCMarked.VisibleIndex = 12;
            // 
            // colLastKeeper
            // 
            this.colLastKeeper.FieldName = "LastKeeper";
            this.colLastKeeper.Name = "colLastKeeper";
            this.colLastKeeper.OptionsColumn.AllowEdit = false;
            this.colLastKeeper.OptionsColumn.AllowFocus = false;
            this.colLastKeeper.OptionsColumn.ReadOnly = true;
            this.colLastKeeper.Visible = true;
            this.colLastKeeper.VisibleIndex = 13;
            this.colLastKeeper.Width = 144;
            // 
            // colActiveKeeper
            // 
            this.colActiveKeeper.FieldName = "ActiveKeeper";
            this.colActiveKeeper.Name = "colActiveKeeper";
            this.colActiveKeeper.OptionsColumn.AllowEdit = false;
            this.colActiveKeeper.OptionsColumn.AllowFocus = false;
            this.colActiveKeeper.OptionsColumn.ReadOnly = true;
            this.colActiveKeeper.Visible = true;
            this.colActiveKeeper.VisibleIndex = 14;
            this.colActiveKeeper.Width = 153;
            // 
            // colDepreciationStartDate
            // 
            this.colDepreciationStartDate.FieldName = "DepreciationStartDate";
            this.colDepreciationStartDate.Name = "colDepreciationStartDate";
            this.colDepreciationStartDate.OptionsColumn.AllowEdit = false;
            this.colDepreciationStartDate.OptionsColumn.AllowFocus = false;
            this.colDepreciationStartDate.OptionsColumn.ReadOnly = true;
            this.colDepreciationStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepreciationStartDate.Visible = true;
            this.colDepreciationStartDate.VisibleIndex = 15;
            this.colDepreciationStartDate.Width = 134;
            // 
            // colExchequerReference
            // 
            this.colExchequerReference.FieldName = "ExchequerReference";
            this.colExchequerReference.Name = "colExchequerReference";
            this.colExchequerReference.OptionsColumn.AllowEdit = false;
            this.colExchequerReference.OptionsColumn.AllowFocus = false;
            this.colExchequerReference.OptionsColumn.ReadOnly = true;
            this.colExchequerReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExchequerReference.Visible = true;
            this.colExchequerReference.VisibleIndex = 16;
            this.colExchequerReference.Width = 125;
            // 
            // colSageAssetReference
            // 
            this.colSageAssetReference.FieldName = "SageAssetReference";
            this.colSageAssetReference.Name = "colSageAssetReference";
            this.colSageAssetReference.OptionsColumn.AllowEdit = false;
            this.colSageAssetReference.OptionsColumn.AllowFocus = false;
            this.colSageAssetReference.OptionsColumn.ReadOnly = true;
            this.colSageAssetReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSageAssetReference.Visible = true;
            this.colSageAssetReference.VisibleIndex = 17;
            this.colSageAssetReference.Width = 128;
            // 
            // colArchive
            // 
            this.colArchive.FieldName = "Archive";
            this.colArchive.Name = "colArchive";
            this.colArchive.OptionsColumn.AllowEdit = false;
            this.colArchive.OptionsColumn.AllowFocus = false;
            this.colArchive.OptionsColumn.ReadOnly = true;
            this.colArchive.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colArchiveDate
            // 
            this.colArchiveDate.FieldName = "ArchiveDate";
            this.colArchiveDate.Name = "colArchiveDate";
            this.colArchiveDate.OptionsColumn.AllowEdit = false;
            this.colArchiveDate.OptionsColumn.AllowFocus = false;
            this.colArchiveDate.OptionsColumn.ReadOnly = true;
            this.colArchiveDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colArchiveDate.Width = 83;
            // 
            // colOccurrenceFrequency
            // 
            this.colOccurrenceFrequency.FieldName = "OccurrenceFrequency";
            this.colOccurrenceFrequency.Name = "colOccurrenceFrequency";
            this.colOccurrenceFrequency.OptionsColumn.AllowEdit = false;
            this.colOccurrenceFrequency.OptionsColumn.AllowFocus = false;
            this.colOccurrenceFrequency.OptionsColumn.ReadOnly = true;
            this.colOccurrenceFrequency.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOccurrenceFrequency.Width = 130;
            // 
            // colOccursUnitDescriptor
            // 
            this.colOccursUnitDescriptor.FieldName = "OccursUnitDescriptor";
            this.colOccursUnitDescriptor.Name = "colOccursUnitDescriptor";
            this.colOccursUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colOccursUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colOccursUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colOccursUnitDescriptor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOccursUnitDescriptor.Width = 128;
            // 
            // colMaximumOccurrence
            // 
            this.colMaximumOccurrence.FieldName = "MaximumOccurrence";
            this.colMaximumOccurrence.Name = "colMaximumOccurrence";
            this.colMaximumOccurrence.OptionsColumn.AllowEdit = false;
            this.colMaximumOccurrence.OptionsColumn.AllowFocus = false;
            this.colMaximumOccurrence.OptionsColumn.ReadOnly = true;
            this.colMaximumOccurrence.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMaximumOccurrence.Width = 123;
            // 
            // colDayOfMonthApplyDepreciation
            // 
            this.colDayOfMonthApplyDepreciation.FieldName = "DayOfMonthApplyDepreciation";
            this.colDayOfMonthApplyDepreciation.Name = "colDayOfMonthApplyDepreciation";
            this.colDayOfMonthApplyDepreciation.OptionsColumn.AllowEdit = false;
            this.colDayOfMonthApplyDepreciation.OptionsColumn.AllowFocus = false;
            this.colDayOfMonthApplyDepreciation.OptionsColumn.ReadOnly = true;
            this.colDayOfMonthApplyDepreciation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDayOfMonthApplyDepreciation.Width = 181;
            // 
            // colItemDescription
            // 
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 18;
            this.colItemDescription.Width = 99;
            // 
            // colOfficeCategoryID1
            // 
            this.colOfficeCategoryID1.FieldName = "OfficeCategoryID";
            this.colOfficeCategoryID1.Name = "colOfficeCategoryID1";
            this.colOfficeCategoryID1.OptionsColumn.AllowEdit = false;
            this.colOfficeCategoryID1.OptionsColumn.AllowFocus = false;
            this.colOfficeCategoryID1.OptionsColumn.ReadOnly = true;
            this.colOfficeCategoryID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOfficeCategoryID1.Width = 112;
            // 
            // colOfficeCategory1
            // 
            this.colOfficeCategory1.FieldName = "OfficeCategory";
            this.colOfficeCategory1.Name = "colOfficeCategory1";
            this.colOfficeCategory1.OptionsColumn.AllowEdit = false;
            this.colOfficeCategory1.OptionsColumn.AllowFocus = false;
            this.colOfficeCategory1.OptionsColumn.ReadOnly = true;
            this.colOfficeCategory1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOfficeCategory1.Visible = true;
            this.colOfficeCategory1.VisibleIndex = 2;
            this.colOfficeCategory1.Width = 98;
            // 
            // colQuantity1
            // 
            this.colQuantity1.FieldName = "Quantity";
            this.colQuantity1.Name = "colQuantity1";
            this.colQuantity1.OptionsColumn.AllowEdit = false;
            this.colQuantity1.OptionsColumn.AllowFocus = false;
            this.colQuantity1.OptionsColumn.ReadOnly = true;
            this.colQuantity1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQuantity1.Visible = true;
            this.colQuantity1.VisibleIndex = 19;
            // 
            // colDepreciationApplied
            // 
            this.colDepreciationApplied.FieldName = "DepreciationApplied";
            this.colDepreciationApplied.Name = "colDepreciationApplied";
            this.colDepreciationApplied.OptionsColumn.AllowEdit = false;
            this.colDepreciationApplied.OptionsColumn.AllowFocus = false;
            this.colDepreciationApplied.OptionsColumn.ReadOnly = true;
            this.colDepreciationApplied.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepreciationApplied.Width = 119;
            // 
            // colCurrentValue2
            // 
            this.colCurrentValue2.FieldName = "CurrentValue";
            this.colCurrentValue2.Name = "colCurrentValue2";
            this.colCurrentValue2.OptionsColumn.AllowEdit = false;
            this.colCurrentValue2.OptionsColumn.AllowFocus = false;
            this.colCurrentValue2.OptionsColumn.ReadOnly = true;
            this.colCurrentValue2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentValue2.Width = 87;
            // 
            // colDepreciationPeriodNumber
            // 
            this.colDepreciationPeriodNumber.FieldName = "DepreciationPeriodNumber";
            this.colDepreciationPeriodNumber.Name = "colDepreciationPeriodNumber";
            this.colDepreciationPeriodNumber.OptionsColumn.AllowEdit = false;
            this.colDepreciationPeriodNumber.OptionsColumn.AllowFocus = false;
            this.colDepreciationPeriodNumber.OptionsColumn.ReadOnly = true;
            this.colDepreciationPeriodNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepreciationPeriodNumber.Width = 154;
            // 
            // colCurrentBilling
            // 
            this.colCurrentBilling.FieldName = "CurrentBilling";
            this.colCurrentBilling.Name = "colCurrentBilling";
            this.colCurrentBilling.OptionsColumn.AllowEdit = false;
            this.colCurrentBilling.OptionsColumn.AllowFocus = false;
            this.colCurrentBilling.OptionsColumn.ReadOnly = true;
            this.colCurrentBilling.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentBilling.Visible = true;
            this.colCurrentBilling.VisibleIndex = 10;
            // 
            // colCurrentCompany
            // 
            this.colCurrentCompany.FieldName = "CurrentCompany";
            this.colCurrentCompany.Name = "colCurrentCompany";
            this.colCurrentCompany.OptionsColumn.AllowEdit = false;
            this.colCurrentCompany.OptionsColumn.AllowFocus = false;
            this.colCurrentCompany.OptionsColumn.ReadOnly = true;
            this.colCurrentCompany.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentCompany.Visible = true;
            this.colCurrentCompany.VisibleIndex = 9;
            this.colCurrentCompany.Width = 103;
            // 
            // colCurrentCostCentre
            // 
            this.colCurrentCostCentre.FieldName = "CurrentCostCentre";
            this.colCurrentCostCentre.Name = "colCurrentCostCentre";
            this.colCurrentCostCentre.OptionsColumn.AllowEdit = false;
            this.colCurrentCostCentre.OptionsColumn.AllowFocus = false;
            this.colCurrentCostCentre.OptionsColumn.ReadOnly = true;
            this.colCurrentCostCentre.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentCostCentre.Visible = true;
            this.colCurrentCostCentre.VisibleIndex = 7;
            this.colCurrentCostCentre.Width = 107;
            // 
            // colCurrentDepartment
            // 
            this.colCurrentDepartment.FieldName = "CurrentDepartment";
            this.colCurrentDepartment.Name = "colCurrentDepartment";
            this.colCurrentDepartment.OptionsColumn.AllowEdit = false;
            this.colCurrentDepartment.OptionsColumn.AllowFocus = false;
            this.colCurrentDepartment.OptionsColumn.ReadOnly = true;
            this.colCurrentDepartment.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentDepartment.Visible = true;
            this.colCurrentDepartment.VisibleIndex = 6;
            this.colCurrentDepartment.Width = 112;
            // 
            // colNotes
            // 
            this.colNotes.FieldName = "Notes";
            this.colNotes.Name = "colNotes";
            this.colNotes.OptionsColumn.AllowEdit = false;
            this.colNotes.OptionsColumn.AllowFocus = false;
            this.colNotes.OptionsColumn.ReadOnly = true;
            this.colNotes.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes.Visible = true;
            this.colNotes.VisibleIndex = 20;
            this.colNotes.Width = 481;
            // 
            // colDateAdded
            // 
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateAdded.Width = 78;
            // 
            // colMode17
            // 
            this.colMode17.FieldName = "Mode";
            this.colMode17.Name = "colMode17";
            this.colMode17.OptionsColumn.AllowEdit = false;
            this.colMode17.OptionsColumn.AllowFocus = false;
            this.colMode17.OptionsColumn.ReadOnly = true;
            this.colMode17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID17
            // 
            this.colRecordID17.FieldName = "RecordID";
            this.colRecordID17.Name = "colRecordID17";
            this.colRecordID17.OptionsColumn.AllowEdit = false;
            this.colRecordID17.OptionsColumn.AllowFocus = false;
            this.colRecordID17.OptionsColumn.ReadOnly = true;
            this.colRecordID17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "d";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // ceCheckEdit
            // 
            this.ceCheckEdit.AutoHeight = false;
            this.ceCheckEdit.Caption = "Check";
            this.ceCheckEdit.Name = "ceCheckEdit";
            // 
            // moneyTextEdit4
            // 
            this.moneyTextEdit4.AutoHeight = false;
            this.moneyTextEdit4.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit4.Name = "moneyTextEdit4";
            // 
            // equipmentChildTabControl
            // 
            this.equipmentChildTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.equipmentChildTabControl.Location = new System.Drawing.Point(0, 0);
            this.equipmentChildTabControl.Name = "equipmentChildTabControl";
            this.equipmentChildTabControl.SelectedTabPage = this.vehicleTabPage;
            this.equipmentChildTabControl.Size = new System.Drawing.Size(1362, 176);
            this.equipmentChildTabControl.TabIndex = 0;
            this.equipmentChildTabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.NoDetailsTabPage,
            this.vehicleTabPage,
            this.plantTabPage,
            this.gadgetTabPage,
            this.hardwareTabPage,
            this.softwareTabPage,
            this.officeTabPage,
            this.keeperTabPage,
            this.transactionTabPage,
            this.billingTabPage,
            this.notificationTabPage,
            this.depreciationTabPage,
            this.workDetailTabPage,
            this.incidentTabPage,
            this.purposeTabPage,
            this.LinkedDocumentsTabPage});
            // 
            // vehicleTabPage
            // 
            this.vehicleTabPage.Controls.Add(this.gridSplitContainer2);
            this.vehicleTabPage.Name = "vehicleTabPage";
            this.vehicleTabPage.PageVisible = false;
            this.vehicleTabPage.Size = new System.Drawing.Size(1357, 150);
            this.vehicleTabPage.Text = "Vehicle Details";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.vehicleGridControl;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.vehicleGridControl);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1357, 150);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // vehicleGridControl
            // 
            this.vehicleGridControl.DataSource = this.spAS11005VehicleItemBindingSource;
            this.vehicleGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.vehicleGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.vehicleGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.vehicleGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.vehicleGridControl.Location = new System.Drawing.Point(0, 0);
            this.vehicleGridControl.MainView = this.vehicleGridView;
            this.vehicleGridControl.MenuManager = this.barManager1;
            this.vehicleGridControl.Name = "vehicleGridControl";
            this.vehicleGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemMemoExEdit3});
            this.vehicleGridControl.Size = new System.Drawing.Size(1357, 150);
            this.vehicleGridControl.TabIndex = 0;
            this.vehicleGridControl.UseEmbeddedNavigator = true;
            this.vehicleGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.vehicleGridView});
            // 
            // spAS11005VehicleItemBindingSource
            // 
            this.spAS11005VehicleItemBindingSource.DataMember = "sp_AS_11005_Vehicle_Item";
            this.spAS11005VehicleItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vehicleGridView
            // 
            this.vehicleGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentID5,
            this.colEquipmentReference3,
            this.colVehicleCategoryID,
            this.colVehicleCategory,
            this.colRegistrationPlate,
            this.colLogBookNumber,
            this.colModelSpecifications,
            this.colEngineSize,
            this.colFuelTypeID,
            this.colFuelType,
            this.colEmissions,
            this.colTotalTyreNumber,
            this.colTowbarTypeID,
            this.colTowbarType,
            this.colColour,
            this.colRegistrationDate,
            this.colMOTDueDate,
            this.colCurrentMileage1,
            this.colServiceDueMileage,
            this.colRadioCode,
            this.colKeyCode,
            this.colElectronicCode,
            this.colMode1,
            this.colRecordID1});
            this.vehicleGridView.GridControl = this.vehicleGridControl;
            this.vehicleGridView.Name = "vehicleGridView";
            this.vehicleGridView.OptionsCustomization.AllowFilter = false;
            this.vehicleGridView.OptionsCustomization.AllowGroup = false;
            this.vehicleGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.vehicleGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.vehicleGridView.OptionsLayout.StoreAppearance = true;
            this.vehicleGridView.OptionsSelection.MultiSelect = true;
            this.vehicleGridView.OptionsView.ColumnAutoWidth = false;
            this.vehicleGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.vehicleGridView.OptionsView.ShowGroupPanel = false;
            this.vehicleGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.vehicleGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.vehicleGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.vehicleGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.vehicleGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.vehicleGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.vehicleGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentID5
            // 
            this.colEquipmentID5.FieldName = "EquipmentID";
            this.colEquipmentID5.Name = "colEquipmentID5";
            this.colEquipmentID5.OptionsColumn.AllowEdit = false;
            this.colEquipmentID5.OptionsColumn.AllowFocus = false;
            this.colEquipmentID5.OptionsColumn.ReadOnly = true;
            this.colEquipmentID5.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID5.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID5.Width = 76;
            // 
            // colEquipmentReference3
            // 
            this.colEquipmentReference3.FieldName = "EquipmentReference";
            this.colEquipmentReference3.Name = "colEquipmentReference3";
            this.colEquipmentReference3.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference3.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference3.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference3.Visible = true;
            this.colEquipmentReference3.VisibleIndex = 0;
            this.colEquipmentReference3.Width = 115;
            // 
            // colVehicleCategoryID
            // 
            this.colVehicleCategoryID.FieldName = "VehicleCategoryID";
            this.colVehicleCategoryID.Name = "colVehicleCategoryID";
            this.colVehicleCategoryID.OptionsColumn.AllowEdit = false;
            this.colVehicleCategoryID.OptionsColumn.AllowFocus = false;
            this.colVehicleCategoryID.OptionsColumn.ReadOnly = true;
            this.colVehicleCategoryID.OptionsColumn.ShowInCustomizationForm = false;
            this.colVehicleCategoryID.OptionsColumn.ShowInExpressionEditor = false;
            this.colVehicleCategoryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVehicleCategoryID.Width = 107;
            // 
            // colVehicleCategory
            // 
            this.colVehicleCategory.FieldName = "VehicleCategory";
            this.colVehicleCategory.Name = "colVehicleCategory";
            this.colVehicleCategory.OptionsColumn.AllowEdit = false;
            this.colVehicleCategory.OptionsColumn.AllowFocus = false;
            this.colVehicleCategory.OptionsColumn.ReadOnly = true;
            this.colVehicleCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVehicleCategory.Visible = true;
            this.colVehicleCategory.VisibleIndex = 1;
            this.colVehicleCategory.Width = 93;
            // 
            // colRegistrationPlate
            // 
            this.colRegistrationPlate.FieldName = "RegistrationPlate";
            this.colRegistrationPlate.Name = "colRegistrationPlate";
            this.colRegistrationPlate.OptionsColumn.AllowEdit = false;
            this.colRegistrationPlate.OptionsColumn.AllowFocus = false;
            this.colRegistrationPlate.OptionsColumn.ReadOnly = true;
            this.colRegistrationPlate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistrationPlate.Visible = true;
            this.colRegistrationPlate.VisibleIndex = 2;
            this.colRegistrationPlate.Width = 97;
            // 
            // colLogBookNumber
            // 
            this.colLogBookNumber.FieldName = "LogBookNumber";
            this.colLogBookNumber.Name = "colLogBookNumber";
            this.colLogBookNumber.OptionsColumn.AllowEdit = false;
            this.colLogBookNumber.OptionsColumn.AllowFocus = false;
            this.colLogBookNumber.OptionsColumn.ReadOnly = true;
            this.colLogBookNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLogBookNumber.Visible = true;
            this.colLogBookNumber.VisibleIndex = 3;
            this.colLogBookNumber.Width = 95;
            // 
            // colModelSpecifications
            // 
            this.colModelSpecifications.FieldName = "ModelSpecifications";
            this.colModelSpecifications.Name = "colModelSpecifications";
            this.colModelSpecifications.OptionsColumn.AllowEdit = false;
            this.colModelSpecifications.OptionsColumn.AllowFocus = false;
            this.colModelSpecifications.OptionsColumn.ReadOnly = true;
            this.colModelSpecifications.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colModelSpecifications.Visible = true;
            this.colModelSpecifications.VisibleIndex = 4;
            this.colModelSpecifications.Width = 108;
            // 
            // colEngineSize
            // 
            this.colEngineSize.FieldName = "EngineSize";
            this.colEngineSize.Name = "colEngineSize";
            this.colEngineSize.OptionsColumn.AllowEdit = false;
            this.colEngineSize.OptionsColumn.AllowFocus = false;
            this.colEngineSize.OptionsColumn.ReadOnly = true;
            this.colEngineSize.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEngineSize.Visible = true;
            this.colEngineSize.VisibleIndex = 5;
            // 
            // colFuelTypeID
            // 
            this.colFuelTypeID.FieldName = "FuelTypeID";
            this.colFuelTypeID.Name = "colFuelTypeID";
            this.colFuelTypeID.OptionsColumn.AllowEdit = false;
            this.colFuelTypeID.OptionsColumn.AllowFocus = false;
            this.colFuelTypeID.OptionsColumn.ReadOnly = true;
            this.colFuelTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colFuelTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colFuelTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colFuelType
            // 
            this.colFuelType.FieldName = "FuelType";
            this.colFuelType.Name = "colFuelType";
            this.colFuelType.OptionsColumn.AllowEdit = false;
            this.colFuelType.OptionsColumn.AllowFocus = false;
            this.colFuelType.OptionsColumn.ReadOnly = true;
            this.colFuelType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFuelType.Visible = true;
            this.colFuelType.VisibleIndex = 6;
            // 
            // colEmissions
            // 
            this.colEmissions.FieldName = "Emissions";
            this.colEmissions.Name = "colEmissions";
            this.colEmissions.OptionsColumn.AllowEdit = false;
            this.colEmissions.OptionsColumn.AllowFocus = false;
            this.colEmissions.OptionsColumn.ReadOnly = true;
            this.colEmissions.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmissions.Visible = true;
            this.colEmissions.VisibleIndex = 7;
            // 
            // colTotalTyreNumber
            // 
            this.colTotalTyreNumber.FieldName = "TotalTyreNumber";
            this.colTotalTyreNumber.Name = "colTotalTyreNumber";
            this.colTotalTyreNumber.OptionsColumn.AllowEdit = false;
            this.colTotalTyreNumber.OptionsColumn.AllowFocus = false;
            this.colTotalTyreNumber.OptionsColumn.ReadOnly = true;
            this.colTotalTyreNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTotalTyreNumber.Visible = true;
            this.colTotalTyreNumber.VisibleIndex = 8;
            this.colTotalTyreNumber.Width = 101;
            // 
            // colTowbarTypeID
            // 
            this.colTowbarTypeID.FieldName = "TowbarTypeID";
            this.colTowbarTypeID.Name = "colTowbarTypeID";
            this.colTowbarTypeID.OptionsColumn.AllowEdit = false;
            this.colTowbarTypeID.OptionsColumn.AllowFocus = false;
            this.colTowbarTypeID.OptionsColumn.ReadOnly = true;
            this.colTowbarTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTowbarTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTowbarTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTowbarTypeID.Width = 89;
            // 
            // colTowbarType
            // 
            this.colTowbarType.FieldName = "TowbarType";
            this.colTowbarType.Name = "colTowbarType";
            this.colTowbarType.OptionsColumn.AllowEdit = false;
            this.colTowbarType.OptionsColumn.AllowFocus = false;
            this.colTowbarType.OptionsColumn.ReadOnly = true;
            this.colTowbarType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTowbarType.Visible = true;
            this.colTowbarType.VisibleIndex = 9;
            // 
            // colColour
            // 
            this.colColour.FieldName = "Colour";
            this.colColour.Name = "colColour";
            this.colColour.OptionsColumn.AllowEdit = false;
            this.colColour.OptionsColumn.AllowFocus = false;
            this.colColour.OptionsColumn.ReadOnly = true;
            this.colColour.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colColour.Visible = true;
            this.colColour.VisibleIndex = 10;
            // 
            // colRegistrationDate
            // 
            this.colRegistrationDate.FieldName = "RegistrationDate";
            this.colRegistrationDate.Name = "colRegistrationDate";
            this.colRegistrationDate.OptionsColumn.AllowEdit = false;
            this.colRegistrationDate.OptionsColumn.AllowFocus = false;
            this.colRegistrationDate.OptionsColumn.ReadOnly = true;
            this.colRegistrationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegistrationDate.Visible = true;
            this.colRegistrationDate.VisibleIndex = 11;
            this.colRegistrationDate.Width = 96;
            // 
            // colMOTDueDate
            // 
            this.colMOTDueDate.FieldName = "MOTDueDate";
            this.colMOTDueDate.Name = "colMOTDueDate";
            this.colMOTDueDate.OptionsColumn.AllowEdit = false;
            this.colMOTDueDate.OptionsColumn.AllowFocus = false;
            this.colMOTDueDate.OptionsColumn.ReadOnly = true;
            this.colMOTDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMOTDueDate.Visible = true;
            this.colMOTDueDate.VisibleIndex = 12;
            this.colMOTDueDate.Width = 82;
            // 
            // colCurrentMileage1
            // 
            this.colCurrentMileage1.FieldName = "CurrentMileage";
            this.colCurrentMileage1.Name = "colCurrentMileage1";
            this.colCurrentMileage1.OptionsColumn.AllowEdit = false;
            this.colCurrentMileage1.OptionsColumn.AllowFocus = false;
            this.colCurrentMileage1.OptionsColumn.ReadOnly = true;
            this.colCurrentMileage1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentMileage1.Visible = true;
            this.colCurrentMileage1.VisibleIndex = 13;
            this.colCurrentMileage1.Width = 88;
            // 
            // colServiceDueMileage
            // 
            this.colServiceDueMileage.FieldName = "ServiceDueMileage";
            this.colServiceDueMileage.Name = "colServiceDueMileage";
            this.colServiceDueMileage.OptionsColumn.AllowEdit = false;
            this.colServiceDueMileage.OptionsColumn.AllowFocus = false;
            this.colServiceDueMileage.OptionsColumn.ReadOnly = true;
            this.colServiceDueMileage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceDueMileage.Visible = true;
            this.colServiceDueMileage.VisibleIndex = 14;
            this.colServiceDueMileage.Width = 108;
            // 
            // colRadioCode
            // 
            this.colRadioCode.FieldName = "RadioCode";
            this.colRadioCode.Name = "colRadioCode";
            this.colRadioCode.OptionsColumn.AllowEdit = false;
            this.colRadioCode.OptionsColumn.AllowFocus = false;
            this.colRadioCode.OptionsColumn.ReadOnly = true;
            this.colRadioCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRadioCode.Visible = true;
            this.colRadioCode.VisibleIndex = 15;
            // 
            // colKeyCode
            // 
            this.colKeyCode.FieldName = "KeyCode";
            this.colKeyCode.Name = "colKeyCode";
            this.colKeyCode.OptionsColumn.AllowEdit = false;
            this.colKeyCode.OptionsColumn.AllowFocus = false;
            this.colKeyCode.OptionsColumn.ReadOnly = true;
            this.colKeyCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeyCode.Visible = true;
            this.colKeyCode.VisibleIndex = 16;
            // 
            // colElectronicCode
            // 
            this.colElectronicCode.FieldName = "ElectronicCode";
            this.colElectronicCode.Name = "colElectronicCode";
            this.colElectronicCode.OptionsColumn.AllowEdit = false;
            this.colElectronicCode.OptionsColumn.AllowFocus = false;
            this.colElectronicCode.OptionsColumn.ReadOnly = true;
            this.colElectronicCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colElectronicCode.Visible = true;
            this.colElectronicCode.VisibleIndex = 17;
            this.colElectronicCode.Width = 86;
            // 
            // colMode1
            // 
            this.colMode1.FieldName = "Mode";
            this.colMode1.Name = "colMode1";
            this.colMode1.OptionsColumn.AllowEdit = false;
            this.colMode1.OptionsColumn.AllowFocus = false;
            this.colMode1.OptionsColumn.ReadOnly = true;
            this.colMode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode1.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID1
            // 
            this.colRecordID1.FieldName = "RecordID";
            this.colRecordID1.Name = "colRecordID1";
            this.colRecordID1.OptionsColumn.AllowEdit = false;
            this.colRecordID1.OptionsColumn.AllowFocus = false;
            this.colRecordID1.OptionsColumn.ReadOnly = true;
            this.colRecordID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ReadOnly = true;
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ReadOnly = true;
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // NoDetailsTabPage
            // 
            this.NoDetailsTabPage.AutoScroll = true;
            this.NoDetailsTabPage.Controls.Add(this.label1);
            this.NoDetailsTabPage.Name = "NoDetailsTabPage";
            this.NoDetailsTabPage.Size = new System.Drawing.Size(1357, 150);
            this.NoDetailsTabPage.Text = "No Details Available";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(556, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Equipment Detailed View Not Available";
            // 
            // plantTabPage
            // 
            this.plantTabPage.Controls.Add(this.gridSplitContainer3);
            this.plantTabPage.Name = "plantTabPage";
            this.plantTabPage.PageVisible = false;
            this.plantTabPage.Size = new System.Drawing.Size(1357, 150);
            this.plantTabPage.Text = "Plant and Machinery Details";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.plantGridControl;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.plantGridControl);
            this.gridSplitContainer3.Size = new System.Drawing.Size(1357, 150);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // plantGridControl
            // 
            this.plantGridControl.DataSource = this.spAS11008PlantItemBindingSource;
            this.plantGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plantGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.plantGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.plantGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.plantGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.plantGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.plantGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.plantGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.plantGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.plantGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.plantGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.plantGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.plantGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.plantGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.plantGridControl.Location = new System.Drawing.Point(0, 0);
            this.plantGridControl.MainView = this.plantGridView;
            this.plantGridControl.MenuManager = this.barManager1;
            this.plantGridControl.Name = "plantGridControl";
            this.plantGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2});
            this.plantGridControl.Size = new System.Drawing.Size(1357, 150);
            this.plantGridControl.TabIndex = 0;
            this.plantGridControl.UseEmbeddedNavigator = true;
            this.plantGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.plantGridView});
            // 
            // spAS11008PlantItemBindingSource
            // 
            this.spAS11008PlantItemBindingSource.DataMember = "sp_AS_11008_Plant_Item";
            this.spAS11008PlantItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // plantGridView
            // 
            this.plantGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentID6,
            this.colEquipmentReference4,
            this.colPlantCategory,
            this.colShortDescription,
            this.colPlantCategoryID,
            this.colEngineSize1,
            this.colFuelType1,
            this.colFuelTypeID1,
            this.colMode2,
            this.colRecordID2});
            this.plantGridView.GridControl = this.plantGridControl;
            this.plantGridView.Name = "plantGridView";
            this.plantGridView.OptionsCustomization.AllowGroup = false;
            this.plantGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.plantGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.plantGridView.OptionsLayout.StoreAppearance = true;
            this.plantGridView.OptionsSelection.MultiSelect = true;
            this.plantGridView.OptionsView.ColumnAutoWidth = false;
            this.plantGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.plantGridView.OptionsView.ShowGroupPanel = false;
            this.plantGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.plantGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.plantGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.plantGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.plantGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.plantGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.plantGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentID6
            // 
            this.colEquipmentID6.FieldName = "EquipmentID";
            this.colEquipmentID6.Name = "colEquipmentID6";
            this.colEquipmentID6.OptionsColumn.AllowEdit = false;
            this.colEquipmentID6.OptionsColumn.AllowFocus = false;
            this.colEquipmentID6.OptionsColumn.ReadOnly = true;
            this.colEquipmentID6.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID6.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID6.Width = 86;
            // 
            // colEquipmentReference4
            // 
            this.colEquipmentReference4.FieldName = "EquipmentReference";
            this.colEquipmentReference4.Name = "colEquipmentReference4";
            this.colEquipmentReference4.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference4.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference4.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference4.Visible = true;
            this.colEquipmentReference4.VisibleIndex = 0;
            this.colEquipmentReference4.Width = 158;
            // 
            // colPlantCategory
            // 
            this.colPlantCategory.FieldName = "PlantCategory";
            this.colPlantCategory.Name = "colPlantCategory";
            this.colPlantCategory.OptionsColumn.AllowEdit = false;
            this.colPlantCategory.OptionsColumn.AllowFocus = false;
            this.colPlantCategory.OptionsColumn.ReadOnly = true;
            this.colPlantCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlantCategory.Visible = true;
            this.colPlantCategory.VisibleIndex = 2;
            this.colPlantCategory.Width = 186;
            // 
            // colShortDescription
            // 
            this.colShortDescription.FieldName = "ShortDescription";
            this.colShortDescription.Name = "colShortDescription";
            this.colShortDescription.OptionsColumn.AllowEdit = false;
            this.colShortDescription.OptionsColumn.AllowFocus = false;
            this.colShortDescription.OptionsColumn.ReadOnly = true;
            this.colShortDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShortDescription.Visible = true;
            this.colShortDescription.VisibleIndex = 1;
            this.colShortDescription.Width = 260;
            // 
            // colPlantCategoryID
            // 
            this.colPlantCategoryID.FieldName = "PlantCategoryID";
            this.colPlantCategoryID.Name = "colPlantCategoryID";
            this.colPlantCategoryID.OptionsColumn.AllowEdit = false;
            this.colPlantCategoryID.OptionsColumn.AllowFocus = false;
            this.colPlantCategoryID.OptionsColumn.ReadOnly = true;
            this.colPlantCategoryID.OptionsColumn.ShowInCustomizationForm = false;
            this.colPlantCategoryID.OptionsColumn.ShowInExpressionEditor = false;
            this.colPlantCategoryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPlantCategoryID.Width = 108;
            // 
            // colEngineSize1
            // 
            this.colEngineSize1.FieldName = "EngineSize";
            this.colEngineSize1.Name = "colEngineSize1";
            this.colEngineSize1.OptionsColumn.AllowEdit = false;
            this.colEngineSize1.OptionsColumn.AllowFocus = false;
            this.colEngineSize1.OptionsColumn.ReadOnly = true;
            this.colEngineSize1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEngineSize1.Visible = true;
            this.colEngineSize1.VisibleIndex = 3;
            this.colEngineSize1.Width = 158;
            // 
            // colFuelType1
            // 
            this.colFuelType1.FieldName = "FuelType";
            this.colFuelType1.Name = "colFuelType1";
            this.colFuelType1.OptionsColumn.AllowEdit = false;
            this.colFuelType1.OptionsColumn.AllowFocus = false;
            this.colFuelType1.OptionsColumn.ReadOnly = true;
            this.colFuelType1.OptionsColumn.ShowInCustomizationForm = false;
            this.colFuelType1.OptionsColumn.ShowInExpressionEditor = false;
            this.colFuelType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFuelType1.Visible = true;
            this.colFuelType1.VisibleIndex = 4;
            this.colFuelType1.Width = 152;
            // 
            // colFuelTypeID1
            // 
            this.colFuelTypeID1.FieldName = "FuelTypeID";
            this.colFuelTypeID1.Name = "colFuelTypeID1";
            this.colFuelTypeID1.OptionsColumn.AllowEdit = false;
            this.colFuelTypeID1.OptionsColumn.AllowFocus = false;
            this.colFuelTypeID1.OptionsColumn.ReadOnly = true;
            this.colFuelTypeID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colFuelTypeID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colFuelTypeID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFuelTypeID1.Width = 83;
            // 
            // colMode2
            // 
            this.colMode2.FieldName = "Mode";
            this.colMode2.Name = "colMode2";
            this.colMode2.OptionsColumn.AllowEdit = false;
            this.colMode2.OptionsColumn.AllowFocus = false;
            this.colMode2.OptionsColumn.ReadOnly = true;
            this.colMode2.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode2.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID2
            // 
            this.colRecordID2.FieldName = "RecordID";
            this.colRecordID2.Name = "colRecordID2";
            this.colRecordID2.OptionsColumn.AllowEdit = false;
            this.colRecordID2.OptionsColumn.AllowFocus = false;
            this.colRecordID2.OptionsColumn.ReadOnly = true;
            this.colRecordID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID2.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // gadgetTabPage
            // 
            this.gadgetTabPage.Controls.Add(this.gadgetGridControl);
            this.gadgetTabPage.Name = "gadgetTabPage";
            this.gadgetTabPage.PageVisible = false;
            this.gadgetTabPage.Size = new System.Drawing.Size(1357, 150);
            this.gadgetTabPage.Text = "Gadget Details";
            // 
            // gadgetGridControl
            // 
            this.gadgetGridControl.DataSource = this.spAS11026GadgetItemBindingSource;
            this.gadgetGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gadgetGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gadgetGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gadgetGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.gadgetGridControl.Location = new System.Drawing.Point(0, 0);
            this.gadgetGridControl.MainView = this.gadgetGridView;
            this.gadgetGridControl.MenuManager = this.barManager1;
            this.gadgetGridControl.Name = "gadgetGridControl";
            this.gadgetGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemHyperLinkEdit3});
            this.gadgetGridControl.Size = new System.Drawing.Size(1357, 150);
            this.gadgetGridControl.TabIndex = 1;
            this.gadgetGridControl.UseEmbeddedNavigator = true;
            this.gadgetGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gadgetGridView});
            // 
            // spAS11026GadgetItemBindingSource
            // 
            this.spAS11026GadgetItemBindingSource.DataMember = "sp_AS_11026_Gadget_Item";
            this.spAS11026GadgetItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // gadgetGridView
            // 
            this.gadgetGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentID7,
            this.colEquipmentReference5,
            this.colGadgetTypeID,
            this.colGadgetType,
            this.colIMEI,
            this.colCasing_ID,
            this.colCasing,
            this.colMode3,
            this.colRecordID3});
            this.gadgetGridView.GridControl = this.gadgetGridControl;
            this.gadgetGridView.Name = "gadgetGridView";
            this.gadgetGridView.OptionsCustomization.AllowGroup = false;
            this.gadgetGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gadgetGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.gadgetGridView.OptionsLayout.StoreAppearance = true;
            this.gadgetGridView.OptionsSelection.MultiSelect = true;
            this.gadgetGridView.OptionsView.ColumnAutoWidth = false;
            this.gadgetGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.gadgetGridView.OptionsView.ShowGroupPanel = false;
            this.gadgetGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.gadgetGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.gadgetGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.gadgetGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.gadgetGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.gadgetGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.gadgetGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentID7
            // 
            this.colEquipmentID7.FieldName = "EquipmentID";
            this.colEquipmentID7.Name = "colEquipmentID7";
            this.colEquipmentID7.OptionsColumn.AllowEdit = false;
            this.colEquipmentID7.OptionsColumn.AllowFocus = false;
            this.colEquipmentID7.OptionsColumn.ReadOnly = true;
            this.colEquipmentID7.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID7.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID7.Width = 86;
            // 
            // colEquipmentReference5
            // 
            this.colEquipmentReference5.FieldName = "EquipmentReference";
            this.colEquipmentReference5.Name = "colEquipmentReference5";
            this.colEquipmentReference5.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference5.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference5.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference5.Visible = true;
            this.colEquipmentReference5.VisibleIndex = 0;
            this.colEquipmentReference5.Width = 158;
            // 
            // colGadgetTypeID
            // 
            this.colGadgetTypeID.FieldName = "GadgetTypeID";
            this.colGadgetTypeID.Name = "colGadgetTypeID";
            this.colGadgetTypeID.OptionsColumn.AllowEdit = false;
            this.colGadgetTypeID.OptionsColumn.AllowFocus = false;
            this.colGadgetTypeID.OptionsColumn.ReadOnly = true;
            this.colGadgetTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colGadgetTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colGadgetTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGadgetTypeID.Width = 98;
            // 
            // colGadgetType
            // 
            this.colGadgetType.FieldName = "GadgetType";
            this.colGadgetType.Name = "colGadgetType";
            this.colGadgetType.OptionsColumn.AllowEdit = false;
            this.colGadgetType.OptionsColumn.AllowFocus = false;
            this.colGadgetType.OptionsColumn.ReadOnly = true;
            this.colGadgetType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGadgetType.Visible = true;
            this.colGadgetType.VisibleIndex = 1;
            this.colGadgetType.Width = 287;
            // 
            // colIMEI
            // 
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsColumn.AllowFocus = false;
            this.colIMEI.OptionsColumn.ReadOnly = true;
            this.colIMEI.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 2;
            this.colIMEI.Width = 459;
            // 
            // colCasing_ID
            // 
            this.colCasing_ID.FieldName = "Casing_ID";
            this.colCasing_ID.Name = "colCasing_ID";
            this.colCasing_ID.OptionsColumn.AllowEdit = false;
            this.colCasing_ID.OptionsColumn.AllowFocus = false;
            this.colCasing_ID.OptionsColumn.ReadOnly = true;
            this.colCasing_ID.OptionsColumn.ShowInCustomizationForm = false;
            this.colCasing_ID.OptionsColumn.ShowInExpressionEditor = false;
            this.colCasing_ID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCasing
            // 
            this.colCasing.FieldName = "Casing";
            this.colCasing.Name = "colCasing";
            this.colCasing.OptionsColumn.AllowEdit = false;
            this.colCasing.OptionsColumn.AllowFocus = false;
            this.colCasing.OptionsColumn.ReadOnly = true;
            this.colCasing.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCasing.Visible = true;
            this.colCasing.VisibleIndex = 3;
            this.colCasing.Width = 239;
            // 
            // colMode3
            // 
            this.colMode3.FieldName = "Mode";
            this.colMode3.Name = "colMode3";
            this.colMode3.OptionsColumn.AllowEdit = false;
            this.colMode3.OptionsColumn.AllowFocus = false;
            this.colMode3.OptionsColumn.ReadOnly = true;
            this.colMode3.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode3.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID3
            // 
            this.colRecordID3.FieldName = "RecordID";
            this.colRecordID3.Name = "colRecordID3";
            this.colRecordID3.OptionsColumn.AllowEdit = false;
            this.colRecordID3.OptionsColumn.AllowFocus = false;
            this.colRecordID3.OptionsColumn.ReadOnly = true;
            this.colRecordID3.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID3.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            // 
            // hardwareTabPage
            // 
            this.hardwareTabPage.Controls.Add(this.hardwareGridControl);
            this.hardwareTabPage.Name = "hardwareTabPage";
            this.hardwareTabPage.PageVisible = false;
            this.hardwareTabPage.Size = new System.Drawing.Size(1357, 150);
            this.hardwareTabPage.Text = "Hardware Details";
            // 
            // hardwareGridControl
            // 
            this.hardwareGridControl.DataSource = this.spAS11032HardwareItemBindingSource;
            this.hardwareGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.hardwareGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.hardwareGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.hardwareGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.hardwareGridControl.Location = new System.Drawing.Point(0, 0);
            this.hardwareGridControl.MainView = this.hardwareGridView;
            this.hardwareGridControl.MenuManager = this.barManager1;
            this.hardwareGridControl.Name = "hardwareGridControl";
            this.hardwareGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemHyperLinkEdit4});
            this.hardwareGridControl.Size = new System.Drawing.Size(1357, 150);
            this.hardwareGridControl.TabIndex = 1;
            this.hardwareGridControl.UseEmbeddedNavigator = true;
            this.hardwareGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.hardwareGridView});
            // 
            // spAS11032HardwareItemBindingSource
            // 
            this.spAS11032HardwareItemBindingSource.DataMember = "sp_AS_11032_Hardware_Item";
            this.spAS11032HardwareItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // hardwareGridView
            // 
            this.hardwareGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentID8,
            this.colEquipmentReference6,
            this.colHardwareTypeID,
            this.colHardwareType,
            this.colSpecificationID,
            this.colSpecification,
            this.colDefaultPassword,
            this.colMode4,
            this.colRecordID4});
            this.hardwareGridView.GridControl = this.hardwareGridControl;
            this.hardwareGridView.Name = "hardwareGridView";
            this.hardwareGridView.OptionsCustomization.AllowGroup = false;
            this.hardwareGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.hardwareGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.hardwareGridView.OptionsLayout.StoreAppearance = true;
            this.hardwareGridView.OptionsSelection.MultiSelect = true;
            this.hardwareGridView.OptionsView.ColumnAutoWidth = false;
            this.hardwareGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.hardwareGridView.OptionsView.ShowGroupPanel = false;
            this.hardwareGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.hardwareGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.hardwareGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.hardwareGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.hardwareGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.hardwareGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.hardwareGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentID8
            // 
            this.colEquipmentID8.FieldName = "EquipmentID";
            this.colEquipmentID8.Name = "colEquipmentID8";
            this.colEquipmentID8.OptionsColumn.AllowEdit = false;
            this.colEquipmentID8.OptionsColumn.AllowFocus = false;
            this.colEquipmentID8.OptionsColumn.ReadOnly = true;
            this.colEquipmentID8.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID8.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentReference6
            // 
            this.colEquipmentReference6.FieldName = "EquipmentReference";
            this.colEquipmentReference6.Name = "colEquipmentReference6";
            this.colEquipmentReference6.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference6.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference6.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference6.Visible = true;
            this.colEquipmentReference6.VisibleIndex = 0;
            this.colEquipmentReference6.Width = 125;
            // 
            // colHardwareTypeID
            // 
            this.colHardwareTypeID.FieldName = "HardwareTypeID";
            this.colHardwareTypeID.Name = "colHardwareTypeID";
            this.colHardwareTypeID.OptionsColumn.AllowEdit = false;
            this.colHardwareTypeID.OptionsColumn.AllowFocus = false;
            this.colHardwareTypeID.OptionsColumn.ReadOnly = true;
            this.colHardwareTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colHardwareTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colHardwareTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHardwareTypeID.Width = 110;
            // 
            // colHardwareType
            // 
            this.colHardwareType.FieldName = "HardwareType";
            this.colHardwareType.Name = "colHardwareType";
            this.colHardwareType.OptionsColumn.AllowEdit = false;
            this.colHardwareType.OptionsColumn.AllowFocus = false;
            this.colHardwareType.OptionsColumn.ReadOnly = true;
            this.colHardwareType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHardwareType.Visible = true;
            this.colHardwareType.VisibleIndex = 1;
            this.colHardwareType.Width = 172;
            // 
            // colSpecificationID
            // 
            this.colSpecificationID.FieldName = "SpecificationID";
            this.colSpecificationID.Name = "colSpecificationID";
            this.colSpecificationID.OptionsColumn.AllowEdit = false;
            this.colSpecificationID.OptionsColumn.AllowFocus = false;
            this.colSpecificationID.OptionsColumn.ReadOnly = true;
            this.colSpecificationID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSpecificationID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSpecificationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpecificationID.Width = 96;
            // 
            // colSpecification
            // 
            this.colSpecification.FieldName = "Specification";
            this.colSpecification.Name = "colSpecification";
            this.colSpecification.OptionsColumn.AllowEdit = false;
            this.colSpecification.OptionsColumn.AllowFocus = false;
            this.colSpecification.OptionsColumn.ReadOnly = true;
            this.colSpecification.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpecification.Visible = true;
            this.colSpecification.VisibleIndex = 2;
            this.colSpecification.Width = 909;
            // 
            // colDefaultPassword
            // 
            this.colDefaultPassword.FieldName = "DefaultPassword";
            this.colDefaultPassword.Name = "colDefaultPassword";
            this.colDefaultPassword.OptionsColumn.AllowEdit = false;
            this.colDefaultPassword.OptionsColumn.AllowFocus = false;
            this.colDefaultPassword.OptionsColumn.ReadOnly = true;
            this.colDefaultPassword.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDefaultPassword.Visible = true;
            this.colDefaultPassword.VisibleIndex = 3;
            this.colDefaultPassword.Width = 279;
            // 
            // colMode4
            // 
            this.colMode4.FieldName = "Mode";
            this.colMode4.Name = "colMode4";
            this.colMode4.OptionsColumn.AllowEdit = false;
            this.colMode4.OptionsColumn.AllowFocus = false;
            this.colMode4.OptionsColumn.ReadOnly = true;
            this.colMode4.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode4.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID4
            // 
            this.colRecordID4.FieldName = "RecordID";
            this.colRecordID4.Name = "colRecordID4";
            this.colRecordID4.OptionsColumn.AllowEdit = false;
            this.colRecordID4.OptionsColumn.AllowFocus = false;
            this.colRecordID4.OptionsColumn.ReadOnly = true;
            this.colRecordID4.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID4.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // repositoryItemHyperLinkEdit4
            // 
            this.repositoryItemHyperLinkEdit4.AutoHeight = false;
            this.repositoryItemHyperLinkEdit4.Name = "repositoryItemHyperLinkEdit4";
            this.repositoryItemHyperLinkEdit4.SingleClick = true;
            // 
            // softwareTabPage
            // 
            this.softwareTabPage.Controls.Add(this.softwareGridControl);
            this.softwareTabPage.Name = "softwareTabPage";
            this.softwareTabPage.PageVisible = false;
            this.softwareTabPage.Size = new System.Drawing.Size(1357, 150);
            this.softwareTabPage.Text = "Software Details";
            // 
            // softwareGridControl
            // 
            this.softwareGridControl.DataSource = this.spAS11035SoftwareItemBindingSource;
            this.softwareGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.softwareGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.softwareGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.softwareGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.softwareGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.softwareGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.softwareGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.softwareGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.softwareGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.softwareGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.softwareGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.softwareGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.softwareGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.softwareGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.softwareGridControl.Location = new System.Drawing.Point(0, 0);
            this.softwareGridControl.MainView = this.softwareGridView;
            this.softwareGridControl.MenuManager = this.barManager1;
            this.softwareGridControl.Name = "softwareGridControl";
            this.softwareGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryItemHyperLinkEdit5});
            this.softwareGridControl.Size = new System.Drawing.Size(1357, 150);
            this.softwareGridControl.TabIndex = 1;
            this.softwareGridControl.UseEmbeddedNavigator = true;
            this.softwareGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.softwareGridView});
            // 
            // spAS11035SoftwareItemBindingSource
            // 
            this.spAS11035SoftwareItemBindingSource.DataMember = "sp_AS_11035_Software_Item";
            this.spAS11035SoftwareItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // softwareGridView
            // 
            this.softwareGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentID9,
            this.colEquipmentReference7,
            this.colProduct,
            this.colProductID,
            this.colVersion,
            this.colVersionID,
            this.colYear,
            this.colLicence,
            this.colParentProgram,
            this.colAcquisationMethod,
            this.colAcquisationMethodID,
            this.colQuantityLimit,
            this.colLicenceKey,
            this.colLicenceType,
            this.colLicenceTypeID,
            this.colValidUntil,
            this.colMode5,
            this.colRecordID5});
            this.softwareGridView.GridControl = this.softwareGridControl;
            this.softwareGridView.Name = "softwareGridView";
            this.softwareGridView.OptionsCustomization.AllowGroup = false;
            this.softwareGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.softwareGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.softwareGridView.OptionsLayout.StoreAppearance = true;
            this.softwareGridView.OptionsSelection.MultiSelect = true;
            this.softwareGridView.OptionsView.ColumnAutoWidth = false;
            this.softwareGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.softwareGridView.OptionsView.ShowGroupPanel = false;
            this.softwareGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.softwareGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.softwareGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.softwareGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.softwareGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.softwareGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.softwareGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentID9
            // 
            this.colEquipmentID9.FieldName = "EquipmentID";
            this.colEquipmentID9.Name = "colEquipmentID9";
            this.colEquipmentID9.OptionsColumn.AllowEdit = false;
            this.colEquipmentID9.OptionsColumn.AllowFocus = false;
            this.colEquipmentID9.OptionsColumn.ReadOnly = true;
            this.colEquipmentID9.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID9.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID9.Width = 86;
            // 
            // colEquipmentReference7
            // 
            this.colEquipmentReference7.FieldName = "EquipmentReference";
            this.colEquipmentReference7.Name = "colEquipmentReference7";
            this.colEquipmentReference7.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference7.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference7.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference7.Visible = true;
            this.colEquipmentReference7.VisibleIndex = 0;
            this.colEquipmentReference7.Width = 125;
            // 
            // colProduct
            // 
            this.colProduct.FieldName = "Product";
            this.colProduct.Name = "colProduct";
            this.colProduct.OptionsColumn.AllowEdit = false;
            this.colProduct.OptionsColumn.AllowFocus = false;
            this.colProduct.OptionsColumn.ReadOnly = true;
            this.colProduct.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProduct.Visible = true;
            this.colProduct.VisibleIndex = 1;
            this.colProduct.Width = 181;
            // 
            // colProductID
            // 
            this.colProductID.FieldName = "ProductID";
            this.colProductID.Name = "colProductID";
            this.colProductID.OptionsColumn.AllowEdit = false;
            this.colProductID.OptionsColumn.AllowFocus = false;
            this.colProductID.OptionsColumn.ReadOnly = true;
            this.colProductID.OptionsColumn.ShowInCustomizationForm = false;
            this.colProductID.OptionsColumn.ShowInExpressionEditor = false;
            this.colProductID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colVersion
            // 
            this.colVersion.FieldName = "Version";
            this.colVersion.Name = "colVersion";
            this.colVersion.OptionsColumn.AllowEdit = false;
            this.colVersion.OptionsColumn.AllowFocus = false;
            this.colVersion.OptionsColumn.ReadOnly = true;
            this.colVersion.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVersion.Visible = true;
            this.colVersion.VisibleIndex = 2;
            this.colVersion.Width = 146;
            // 
            // colVersionID
            // 
            this.colVersionID.FieldName = "VersionID";
            this.colVersionID.Name = "colVersionID";
            this.colVersionID.OptionsColumn.AllowEdit = false;
            this.colVersionID.OptionsColumn.AllowFocus = false;
            this.colVersionID.OptionsColumn.ReadOnly = true;
            this.colVersionID.OptionsColumn.ShowInCustomizationForm = false;
            this.colVersionID.OptionsColumn.ShowInExpressionEditor = false;
            this.colVersionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colYear
            // 
            this.colYear.FieldName = "Year";
            this.colYear.Name = "colYear";
            this.colYear.OptionsColumn.AllowEdit = false;
            this.colYear.OptionsColumn.AllowFocus = false;
            this.colYear.OptionsColumn.ReadOnly = true;
            this.colYear.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colYear.Visible = true;
            this.colYear.VisibleIndex = 3;
            // 
            // colLicence
            // 
            this.colLicence.FieldName = "Licence";
            this.colLicence.Name = "colLicence";
            this.colLicence.OptionsColumn.AllowEdit = false;
            this.colLicence.OptionsColumn.AllowFocus = false;
            this.colLicence.OptionsColumn.ReadOnly = true;
            this.colLicence.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLicence.Visible = true;
            this.colLicence.VisibleIndex = 4;
            this.colLicence.Width = 282;
            // 
            // colParentProgram
            // 
            this.colParentProgram.FieldName = "ParentProgram";
            this.colParentProgram.Name = "colParentProgram";
            this.colParentProgram.OptionsColumn.AllowEdit = false;
            this.colParentProgram.OptionsColumn.AllowFocus = false;
            this.colParentProgram.OptionsColumn.ReadOnly = true;
            this.colParentProgram.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colParentProgram.Visible = true;
            this.colParentProgram.VisibleIndex = 5;
            this.colParentProgram.Width = 189;
            // 
            // colAcquisationMethod
            // 
            this.colAcquisationMethod.FieldName = "AcquisationMethod";
            this.colAcquisationMethod.Name = "colAcquisationMethod";
            this.colAcquisationMethod.OptionsColumn.AllowEdit = false;
            this.colAcquisationMethod.OptionsColumn.AllowFocus = false;
            this.colAcquisationMethod.OptionsColumn.ReadOnly = true;
            this.colAcquisationMethod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAcquisationMethod.Visible = true;
            this.colAcquisationMethod.VisibleIndex = 6;
            this.colAcquisationMethod.Width = 167;
            // 
            // colAcquisationMethodID
            // 
            this.colAcquisationMethodID.FieldName = "AcquisationMethodID";
            this.colAcquisationMethodID.Name = "colAcquisationMethodID";
            this.colAcquisationMethodID.OptionsColumn.AllowEdit = false;
            this.colAcquisationMethodID.OptionsColumn.AllowFocus = false;
            this.colAcquisationMethodID.OptionsColumn.ReadOnly = true;
            this.colAcquisationMethodID.OptionsColumn.ShowInCustomizationForm = false;
            this.colAcquisationMethodID.OptionsColumn.ShowInExpressionEditor = false;
            this.colAcquisationMethodID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAcquisationMethodID.Width = 130;
            // 
            // colQuantityLimit
            // 
            this.colQuantityLimit.FieldName = "QuantityLimit";
            this.colQuantityLimit.Name = "colQuantityLimit";
            this.colQuantityLimit.OptionsColumn.AllowEdit = false;
            this.colQuantityLimit.OptionsColumn.AllowFocus = false;
            this.colQuantityLimit.OptionsColumn.ReadOnly = true;
            this.colQuantityLimit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQuantityLimit.Visible = true;
            this.colQuantityLimit.VisibleIndex = 7;
            this.colQuantityLimit.Width = 88;
            // 
            // colLicenceKey
            // 
            this.colLicenceKey.FieldName = "LicenceKey";
            this.colLicenceKey.Name = "colLicenceKey";
            this.colLicenceKey.OptionsColumn.AllowEdit = false;
            this.colLicenceKey.OptionsColumn.AllowFocus = false;
            this.colLicenceKey.OptionsColumn.ReadOnly = true;
            this.colLicenceKey.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLicenceKey.Visible = true;
            this.colLicenceKey.VisibleIndex = 8;
            this.colLicenceKey.Width = 274;
            // 
            // colLicenceType
            // 
            this.colLicenceType.FieldName = "LicenceType";
            this.colLicenceType.Name = "colLicenceType";
            this.colLicenceType.OptionsColumn.AllowEdit = false;
            this.colLicenceType.OptionsColumn.AllowFocus = false;
            this.colLicenceType.OptionsColumn.ReadOnly = true;
            this.colLicenceType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLicenceType.Visible = true;
            this.colLicenceType.VisibleIndex = 9;
            this.colLicenceType.Width = 156;
            // 
            // colLicenceTypeID
            // 
            this.colLicenceTypeID.FieldName = "LicenceTypeID";
            this.colLicenceTypeID.Name = "colLicenceTypeID";
            this.colLicenceTypeID.OptionsColumn.AllowEdit = false;
            this.colLicenceTypeID.OptionsColumn.AllowFocus = false;
            this.colLicenceTypeID.OptionsColumn.ReadOnly = true;
            this.colLicenceTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colLicenceTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colLicenceTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLicenceTypeID.Width = 98;
            // 
            // colValidUntil
            // 
            this.colValidUntil.FieldName = "ValidUntil";
            this.colValidUntil.Name = "colValidUntil";
            this.colValidUntil.OptionsColumn.AllowEdit = false;
            this.colValidUntil.OptionsColumn.AllowFocus = false;
            this.colValidUntil.OptionsColumn.ReadOnly = true;
            this.colValidUntil.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colValidUntil.Visible = true;
            this.colValidUntil.VisibleIndex = 10;
            // 
            // colMode5
            // 
            this.colMode5.FieldName = "Mode";
            this.colMode5.Name = "colMode5";
            this.colMode5.OptionsColumn.AllowEdit = false;
            this.colMode5.OptionsColumn.AllowFocus = false;
            this.colMode5.OptionsColumn.ReadOnly = true;
            this.colMode5.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode5.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID5
            // 
            this.colRecordID5.FieldName = "RecordID";
            this.colRecordID5.Name = "colRecordID5";
            this.colRecordID5.OptionsColumn.AllowEdit = false;
            this.colRecordID5.OptionsColumn.AllowFocus = false;
            this.colRecordID5.OptionsColumn.ReadOnly = true;
            this.colRecordID5.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID5.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // repositoryItemHyperLinkEdit5
            // 
            this.repositoryItemHyperLinkEdit5.AutoHeight = false;
            this.repositoryItemHyperLinkEdit5.Name = "repositoryItemHyperLinkEdit5";
            this.repositoryItemHyperLinkEdit5.SingleClick = true;
            // 
            // officeTabPage
            // 
            this.officeTabPage.Controls.Add(this.officeGridControl);
            this.officeTabPage.Name = "officeTabPage";
            this.officeTabPage.PageVisible = false;
            this.officeTabPage.Size = new System.Drawing.Size(1357, 150);
            this.officeTabPage.Text = "Office Details";
            // 
            // officeGridControl
            // 
            this.officeGridControl.DataSource = this.spAS11029OfficeItemBindingSource;
            this.officeGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.officeGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.officeGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.officeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.officeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.officeGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.officeGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.officeGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.officeGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.officeGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.officeGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.officeGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.officeGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.officeGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.officeGridControl.Location = new System.Drawing.Point(0, 0);
            this.officeGridControl.MainView = this.officeGridView;
            this.officeGridControl.MenuManager = this.barManager1;
            this.officeGridControl.Name = "officeGridControl";
            this.officeGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit8,
            this.repositoryItemHyperLinkEdit6});
            this.officeGridControl.Size = new System.Drawing.Size(1357, 150);
            this.officeGridControl.TabIndex = 1;
            this.officeGridControl.UseEmbeddedNavigator = true;
            this.officeGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.officeGridView});
            // 
            // spAS11029OfficeItemBindingSource
            // 
            this.spAS11029OfficeItemBindingSource.DataMember = "sp_AS_11029_Office_Item";
            this.spAS11029OfficeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // officeGridView
            // 
            this.officeGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentID10,
            this.colEquipmentReference8,
            this.colOfficeCategoryID,
            this.colOfficeCategory,
            this.colDescription,
            this.colQuantity,
            this.colMode6,
            this.colRecordID6});
            this.officeGridView.GridControl = this.officeGridControl;
            this.officeGridView.Name = "officeGridView";
            this.officeGridView.OptionsCustomization.AllowGroup = false;
            this.officeGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.officeGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.officeGridView.OptionsLayout.StoreAppearance = true;
            this.officeGridView.OptionsSelection.MultiSelect = true;
            this.officeGridView.OptionsView.ColumnAutoWidth = false;
            this.officeGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.officeGridView.OptionsView.ShowGroupPanel = false;
            this.officeGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.officeGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.officeGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.officeGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.officeGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.officeGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.officeGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentID10
            // 
            this.colEquipmentID10.FieldName = "EquipmentID";
            this.colEquipmentID10.Name = "colEquipmentID10";
            this.colEquipmentID10.OptionsColumn.AllowEdit = false;
            this.colEquipmentID10.OptionsColumn.AllowFocus = false;
            this.colEquipmentID10.OptionsColumn.ReadOnly = true;
            this.colEquipmentID10.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID10.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID10.Width = 86;
            // 
            // colEquipmentReference8
            // 
            this.colEquipmentReference8.FieldName = "EquipmentReference";
            this.colEquipmentReference8.Name = "colEquipmentReference8";
            this.colEquipmentReference8.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference8.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference8.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference8.Visible = true;
            this.colEquipmentReference8.VisibleIndex = 0;
            this.colEquipmentReference8.Width = 125;
            // 
            // colOfficeCategoryID
            // 
            this.colOfficeCategoryID.FieldName = "OfficeCategoryID";
            this.colOfficeCategoryID.Name = "colOfficeCategoryID";
            this.colOfficeCategoryID.OptionsColumn.AllowEdit = false;
            this.colOfficeCategoryID.OptionsColumn.AllowFocus = false;
            this.colOfficeCategoryID.OptionsColumn.ReadOnly = true;
            this.colOfficeCategoryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOfficeCategoryID.Width = 113;
            // 
            // colOfficeCategory
            // 
            this.colOfficeCategory.FieldName = "OfficeCategory";
            this.colOfficeCategory.Name = "colOfficeCategory";
            this.colOfficeCategory.OptionsColumn.AllowEdit = false;
            this.colOfficeCategory.OptionsColumn.AllowFocus = false;
            this.colOfficeCategory.OptionsColumn.ReadOnly = true;
            this.colOfficeCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOfficeCategory.Visible = true;
            this.colOfficeCategory.VisibleIndex = 1;
            this.colOfficeCategory.Width = 260;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 2;
            this.colDescription.Width = 653;
            // 
            // colQuantity
            // 
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.AllowEdit = false;
            this.colQuantity.OptionsColumn.AllowFocus = false;
            this.colQuantity.OptionsColumn.ReadOnly = true;
            this.colQuantity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 3;
            // 
            // colMode6
            // 
            this.colMode6.FieldName = "Mode";
            this.colMode6.Name = "colMode6";
            this.colMode6.OptionsColumn.AllowEdit = false;
            this.colMode6.OptionsColumn.AllowFocus = false;
            this.colMode6.OptionsColumn.ReadOnly = true;
            this.colMode6.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode6.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID6
            // 
            this.colRecordID6.FieldName = "RecordID";
            this.colRecordID6.Name = "colRecordID6";
            this.colRecordID6.OptionsColumn.AllowEdit = false;
            this.colRecordID6.OptionsColumn.AllowFocus = false;
            this.colRecordID6.OptionsColumn.ReadOnly = true;
            this.colRecordID6.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID6.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // repositoryItemHyperLinkEdit6
            // 
            this.repositoryItemHyperLinkEdit6.AutoHeight = false;
            this.repositoryItemHyperLinkEdit6.Name = "repositoryItemHyperLinkEdit6";
            this.repositoryItemHyperLinkEdit6.SingleClick = true;
            // 
            // keeperTabPage
            // 
            this.keeperTabPage.AutoScroll = true;
            this.keeperTabPage.Controls.Add(this.keeperGridControl);
            this.keeperTabPage.Name = "keeperTabPage";
            this.keeperTabPage.Size = new System.Drawing.Size(1357, 150);
            this.keeperTabPage.Text = "Keeper Allocations";
            // 
            // keeperGridControl
            // 
            this.keeperGridControl.DataSource = this.spAS11044KeeperAllocationItemBindingSource;
            this.keeperGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.keeperGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.keeperGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.keeperGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.keeperGridControl.Location = new System.Drawing.Point(0, 0);
            this.keeperGridControl.MainView = this.keeperGridView;
            this.keeperGridControl.MenuManager = this.barManager1;
            this.keeperGridControl.Name = "keeperGridControl";
            this.keeperGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lueKeeperNotes});
            this.keeperGridControl.Size = new System.Drawing.Size(1357, 150);
            this.keeperGridControl.TabIndex = 2;
            this.keeperGridControl.UseEmbeddedNavigator = true;
            this.keeperGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.keeperGridView});
            // 
            // spAS11044KeeperAllocationItemBindingSource
            // 
            this.spAS11044KeeperAllocationItemBindingSource.DataMember = "sp_AS_11044_Keeper_Allocation_Item";
            this.spAS11044KeeperAllocationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // keeperGridView
            // 
            this.keeperGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colKeeperAllocationID,
            this.colEquipmentReference9,
            this.colKeeperTypeID,
            this.colKeeperType,
            this.colKeeperID,
            this.colKeeper,
            this.colEquipmentID11,
            this.colAllocationStatusID,
            this.colAllocationStatus,
            this.colAllocationDate,
            this.colAllocationEndDate,
            this.colNotes1,
            this.colMode7,
            this.colRecordID7});
            this.keeperGridView.GridControl = this.keeperGridControl;
            this.keeperGridView.Name = "keeperGridView";
            this.keeperGridView.OptionsCustomization.AllowGroup = false;
            this.keeperGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.keeperGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.keeperGridView.OptionsLayout.StoreAppearance = true;
            this.keeperGridView.OptionsSelection.MultiSelect = true;
            this.keeperGridView.OptionsView.ColumnAutoWidth = false;
            this.keeperGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.keeperGridView.OptionsView.ShowGroupPanel = false;
            this.keeperGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.keeperGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.keeperGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.keeperGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.keeperGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.keeperGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.keeperGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colKeeperAllocationID
            // 
            this.colKeeperAllocationID.FieldName = "KeeperAllocationID";
            this.colKeeperAllocationID.Name = "colKeeperAllocationID";
            this.colKeeperAllocationID.OptionsColumn.AllowEdit = false;
            this.colKeeperAllocationID.OptionsColumn.AllowFocus = false;
            this.colKeeperAllocationID.OptionsColumn.ReadOnly = true;
            this.colKeeperAllocationID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAllocationID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAllocationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAllocationID.Width = 119;
            // 
            // colEquipmentReference9
            // 
            this.colEquipmentReference9.FieldName = "EquipmentReference";
            this.colEquipmentReference9.Name = "colEquipmentReference9";
            this.colEquipmentReference9.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference9.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference9.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference9.Visible = true;
            this.colEquipmentReference9.VisibleIndex = 0;
            this.colEquipmentReference9.Width = 125;
            // 
            // colKeeperTypeID
            // 
            this.colKeeperTypeID.FieldName = "KeeperTypeID";
            this.colKeeperTypeID.Name = "colKeeperTypeID";
            this.colKeeperTypeID.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperTypeID.Width = 97;
            // 
            // colKeeperType
            // 
            this.colKeeperType.FieldName = "KeeperType";
            this.colKeeperType.Name = "colKeeperType";
            this.colKeeperType.OptionsColumn.AllowEdit = false;
            this.colKeeperType.OptionsColumn.AllowFocus = false;
            this.colKeeperType.OptionsColumn.ReadOnly = true;
            this.colKeeperType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperType.Visible = true;
            this.colKeeperType.VisibleIndex = 1;
            this.colKeeperType.Width = 136;
            // 
            // colKeeperID
            // 
            this.colKeeperID.FieldName = "KeeperID";
            this.colKeeperID.Name = "colKeeperID";
            this.colKeeperID.OptionsColumn.AllowEdit = false;
            this.colKeeperID.OptionsColumn.AllowFocus = false;
            this.colKeeperID.OptionsColumn.ReadOnly = true;
            this.colKeeperID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colKeeper
            // 
            this.colKeeper.FieldName = "Keeper";
            this.colKeeper.Name = "colKeeper";
            this.colKeeper.OptionsColumn.AllowEdit = false;
            this.colKeeper.OptionsColumn.AllowFocus = false;
            this.colKeeper.OptionsColumn.ReadOnly = true;
            this.colKeeper.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeper.Visible = true;
            this.colKeeper.VisibleIndex = 2;
            this.colKeeper.Width = 419;
            // 
            // colEquipmentID11
            // 
            this.colEquipmentID11.FieldName = "EquipmentID";
            this.colEquipmentID11.Name = "colEquipmentID11";
            this.colEquipmentID11.OptionsColumn.AllowEdit = false;
            this.colEquipmentID11.OptionsColumn.AllowFocus = false;
            this.colEquipmentID11.OptionsColumn.ReadOnly = true;
            this.colEquipmentID11.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID11.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID11.Width = 86;
            // 
            // colAllocationStatusID
            // 
            this.colAllocationStatusID.FieldName = "AllocationStatusID";
            this.colAllocationStatusID.Name = "colAllocationStatusID";
            this.colAllocationStatusID.OptionsColumn.AllowEdit = false;
            this.colAllocationStatusID.OptionsColumn.AllowFocus = false;
            this.colAllocationStatusID.OptionsColumn.ReadOnly = true;
            this.colAllocationStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colAllocationStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colAllocationStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationStatusID.Width = 116;
            // 
            // colAllocationStatus
            // 
            this.colAllocationStatus.FieldName = "AllocationStatus";
            this.colAllocationStatus.Name = "colAllocationStatus";
            this.colAllocationStatus.OptionsColumn.AllowEdit = false;
            this.colAllocationStatus.OptionsColumn.AllowFocus = false;
            this.colAllocationStatus.OptionsColumn.ReadOnly = true;
            this.colAllocationStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationStatus.Visible = true;
            this.colAllocationStatus.VisibleIndex = 3;
            this.colAllocationStatus.Width = 155;
            // 
            // colAllocationDate
            // 
            this.colAllocationDate.FieldName = "AllocationDate";
            this.colAllocationDate.Name = "colAllocationDate";
            this.colAllocationDate.OptionsColumn.AllowEdit = false;
            this.colAllocationDate.OptionsColumn.AllowFocus = false;
            this.colAllocationDate.OptionsColumn.ReadOnly = true;
            this.colAllocationDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationDate.Visible = true;
            this.colAllocationDate.VisibleIndex = 4;
            this.colAllocationDate.Width = 94;
            // 
            // colAllocationEndDate
            // 
            this.colAllocationEndDate.FieldName = "AllocationEndDate";
            this.colAllocationEndDate.Name = "colAllocationEndDate";
            this.colAllocationEndDate.OptionsColumn.AllowEdit = false;
            this.colAllocationEndDate.OptionsColumn.AllowFocus = false;
            this.colAllocationEndDate.OptionsColumn.ReadOnly = true;
            this.colAllocationEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAllocationEndDate.Visible = true;
            this.colAllocationEndDate.VisibleIndex = 5;
            this.colAllocationEndDate.Width = 115;
            // 
            // colNotes1
            // 
            this.colNotes1.FieldName = "Notes";
            this.colNotes1.Name = "colNotes1";
            this.colNotes1.OptionsColumn.AllowEdit = false;
            this.colNotes1.OptionsColumn.AllowFocus = false;
            this.colNotes1.OptionsColumn.ReadOnly = true;
            this.colNotes1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes1.Visible = true;
            this.colNotes1.VisibleIndex = 6;
            this.colNotes1.Width = 598;
            // 
            // colMode7
            // 
            this.colMode7.FieldName = "Mode";
            this.colMode7.Name = "colMode7";
            this.colMode7.OptionsColumn.AllowEdit = false;
            this.colMode7.OptionsColumn.AllowFocus = false;
            this.colMode7.OptionsColumn.ReadOnly = true;
            this.colMode7.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode7.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID7
            // 
            this.colRecordID7.FieldName = "RecordID";
            this.colRecordID7.Name = "colRecordID7";
            this.colRecordID7.OptionsColumn.AllowEdit = false;
            this.colRecordID7.OptionsColumn.AllowFocus = false;
            this.colRecordID7.OptionsColumn.ReadOnly = true;
            this.colRecordID7.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID7.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // lueKeeperNotes
            // 
            this.lueKeeperNotes.Name = "lueKeeperNotes";
            // 
            // transactionTabPage
            // 
            this.transactionTabPage.AutoScroll = true;
            this.transactionTabPage.Controls.Add(this.transactionGridControl);
            this.transactionTabPage.Name = "transactionTabPage";
            this.transactionTabPage.Size = new System.Drawing.Size(1357, 150);
            this.transactionTabPage.Text = "Transactions";
            // 
            // transactionGridControl
            // 
            this.transactionGridControl.DataSource = this.spAS11041TransactionItemBindingSource;
            this.transactionGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.transactionGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.transactionGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.transactionGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.transactionGridControl.Location = new System.Drawing.Point(0, 0);
            this.transactionGridControl.MainView = this.transactionsGridView;
            this.transactionGridControl.MenuManager = this.barManager1;
            this.transactionGridControl.Name = "transactionGridControl";
            this.transactionGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ceOnExchequer,
            this.moneyTextEdit});
            this.transactionGridControl.Size = new System.Drawing.Size(1357, 150);
            this.transactionGridControl.TabIndex = 3;
            this.transactionGridControl.UseEmbeddedNavigator = true;
            this.transactionGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.transactionsGridView});
            // 
            // spAS11041TransactionItemBindingSource
            // 
            this.spAS11041TransactionItemBindingSource.DataMember = "sp_AS_11041_Transaction_Item";
            this.spAS11041TransactionItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // transactionsGridView
            // 
            this.transactionsGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTransactionID1,
            this.colEquipmentReference10,
            this.colEquipmentID12,
            this.colTransactionTypeID,
            this.colTransactionType,
            this.colTransactionDate,
            this.colTransactionOrderNumber,
            this.colPurchaseInvoice,
            this.colDescription1,
            this.colCompletedOnExchequer,
            this.colNetTransactionPrice,
            this.colVAT,
            this.colMode8,
            this.colRecordID8});
            this.transactionsGridView.GridControl = this.transactionGridControl;
            this.transactionsGridView.Name = "transactionsGridView";
            this.transactionsGridView.OptionsCustomization.AllowGroup = false;
            this.transactionsGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.transactionsGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.transactionsGridView.OptionsLayout.StoreAppearance = true;
            this.transactionsGridView.OptionsSelection.MultiSelect = true;
            this.transactionsGridView.OptionsView.ColumnAutoWidth = false;
            this.transactionsGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.transactionsGridView.OptionsView.ShowGroupPanel = false;
            this.transactionsGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.transactionsGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.transactionsGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.transactionsGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.transactionsGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.transactionsGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.transactionsGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colTransactionID1
            // 
            this.colTransactionID1.FieldName = "TransactionID";
            this.colTransactionID1.Name = "colTransactionID1";
            this.colTransactionID1.OptionsColumn.AllowEdit = false;
            this.colTransactionID1.OptionsColumn.AllowFocus = false;
            this.colTransactionID1.OptionsColumn.ReadOnly = true;
            this.colTransactionID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionID1.Width = 92;
            // 
            // colEquipmentReference10
            // 
            this.colEquipmentReference10.FieldName = "EquipmentReference";
            this.colEquipmentReference10.Name = "colEquipmentReference10";
            this.colEquipmentReference10.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference10.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference10.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference10.Visible = true;
            this.colEquipmentReference10.VisibleIndex = 0;
            this.colEquipmentReference10.Width = 125;
            // 
            // colEquipmentID12
            // 
            this.colEquipmentID12.FieldName = "EquipmentID";
            this.colEquipmentID12.Name = "colEquipmentID12";
            this.colEquipmentID12.OptionsColumn.AllowEdit = false;
            this.colEquipmentID12.OptionsColumn.AllowFocus = false;
            this.colEquipmentID12.OptionsColumn.ReadOnly = true;
            this.colEquipmentID12.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID12.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID12.Width = 86;
            // 
            // colTransactionTypeID
            // 
            this.colTransactionTypeID.FieldName = "TransactionTypeID";
            this.colTransactionTypeID.Name = "colTransactionTypeID";
            this.colTransactionTypeID.OptionsColumn.AllowEdit = false;
            this.colTransactionTypeID.OptionsColumn.AllowFocus = false;
            this.colTransactionTypeID.OptionsColumn.ReadOnly = true;
            this.colTransactionTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionTypeID.Width = 119;
            // 
            // colTransactionType
            // 
            this.colTransactionType.FieldName = "TransactionType";
            this.colTransactionType.Name = "colTransactionType";
            this.colTransactionType.OptionsColumn.AllowEdit = false;
            this.colTransactionType.OptionsColumn.AllowFocus = false;
            this.colTransactionType.OptionsColumn.ReadOnly = true;
            this.colTransactionType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionType.Visible = true;
            this.colTransactionType.VisibleIndex = 1;
            this.colTransactionType.Width = 170;
            // 
            // colTransactionDate
            // 
            this.colTransactionDate.FieldName = "TransactionDate";
            this.colTransactionDate.Name = "colTransactionDate";
            this.colTransactionDate.OptionsColumn.AllowEdit = false;
            this.colTransactionDate.OptionsColumn.AllowFocus = false;
            this.colTransactionDate.OptionsColumn.ReadOnly = true;
            this.colTransactionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionDate.Visible = true;
            this.colTransactionDate.VisibleIndex = 2;
            this.colTransactionDate.Width = 104;
            // 
            // colTransactionOrderNumber
            // 
            this.colTransactionOrderNumber.FieldName = "TransactionOrderNumber";
            this.colTransactionOrderNumber.Name = "colTransactionOrderNumber";
            this.colTransactionOrderNumber.OptionsColumn.AllowEdit = false;
            this.colTransactionOrderNumber.OptionsColumn.AllowFocus = false;
            this.colTransactionOrderNumber.OptionsColumn.ReadOnly = true;
            this.colTransactionOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionOrderNumber.Visible = true;
            this.colTransactionOrderNumber.VisibleIndex = 3;
            this.colTransactionOrderNumber.Width = 242;
            // 
            // colPurchaseInvoice
            // 
            this.colPurchaseInvoice.FieldName = "PurchaseInvoice";
            this.colPurchaseInvoice.Name = "colPurchaseInvoice";
            this.colPurchaseInvoice.OptionsColumn.AllowEdit = false;
            this.colPurchaseInvoice.OptionsColumn.AllowFocus = false;
            this.colPurchaseInvoice.OptionsColumn.ReadOnly = true;
            this.colPurchaseInvoice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPurchaseInvoice.Visible = true;
            this.colPurchaseInvoice.VisibleIndex = 4;
            this.colPurchaseInvoice.Width = 104;
            // 
            // colDescription1
            // 
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 5;
            this.colDescription1.Width = 727;
            // 
            // colCompletedOnExchequer
            // 
            this.colCompletedOnExchequer.FieldName = "CompletedOnExchequer";
            this.colCompletedOnExchequer.Name = "colCompletedOnExchequer";
            this.colCompletedOnExchequer.OptionsColumn.AllowEdit = false;
            this.colCompletedOnExchequer.OptionsColumn.AllowFocus = false;
            this.colCompletedOnExchequer.OptionsColumn.ReadOnly = true;
            this.colCompletedOnExchequer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompletedOnExchequer.Width = 144;
            // 
            // colNetTransactionPrice
            // 
            this.colNetTransactionPrice.ColumnEdit = this.moneyTextEdit;
            this.colNetTransactionPrice.FieldName = "NetTransactionPrice";
            this.colNetTransactionPrice.Name = "colNetTransactionPrice";
            this.colNetTransactionPrice.OptionsColumn.AllowEdit = false;
            this.colNetTransactionPrice.OptionsColumn.AllowFocus = false;
            this.colNetTransactionPrice.OptionsColumn.ReadOnly = true;
            this.colNetTransactionPrice.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNetTransactionPrice.Visible = true;
            this.colNetTransactionPrice.VisibleIndex = 6;
            this.colNetTransactionPrice.Width = 124;
            // 
            // moneyTextEdit
            // 
            this.moneyTextEdit.AutoHeight = false;
            this.moneyTextEdit.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit.Name = "moneyTextEdit";
            // 
            // colVAT
            // 
            this.colVAT.ColumnEdit = this.moneyTextEdit;
            this.colVAT.FieldName = "VAT";
            this.colVAT.Name = "colVAT";
            this.colVAT.OptionsColumn.AllowEdit = false;
            this.colVAT.OptionsColumn.AllowFocus = false;
            this.colVAT.OptionsColumn.ReadOnly = true;
            this.colVAT.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVAT.Visible = true;
            this.colVAT.VisibleIndex = 7;
            // 
            // colMode8
            // 
            this.colMode8.FieldName = "Mode";
            this.colMode8.Name = "colMode8";
            this.colMode8.OptionsColumn.AllowEdit = false;
            this.colMode8.OptionsColumn.AllowFocus = false;
            this.colMode8.OptionsColumn.ReadOnly = true;
            this.colMode8.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode8.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID8
            // 
            this.colRecordID8.FieldName = "RecordID";
            this.colRecordID8.Name = "colRecordID8";
            this.colRecordID8.OptionsColumn.AllowEdit = false;
            this.colRecordID8.OptionsColumn.AllowFocus = false;
            this.colRecordID8.OptionsColumn.ReadOnly = true;
            this.colRecordID8.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID8.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ceOnExchequer
            // 
            this.ceOnExchequer.AutoHeight = false;
            this.ceOnExchequer.Caption = "Check";
            this.ceOnExchequer.Name = "ceOnExchequer";
            // 
            // billingTabPage
            // 
            this.billingTabPage.AutoScroll = true;
            this.billingTabPage.Controls.Add(this.billingGridControl);
            this.billingTabPage.Name = "billingTabPage";
            this.billingTabPage.Size = new System.Drawing.Size(1357, 150);
            this.billingTabPage.Text = "Billing Details";
            // 
            // billingGridControl
            // 
            this.billingGridControl.DataSource = this.spAS11050DepreciationItemBindingSource;
            this.billingGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billingGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.billingGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.billingGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.billingGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, -1, true, false, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, -1, true, false, "", "delete")});
            this.billingGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.billingGridControl.Location = new System.Drawing.Point(0, 0);
            this.billingGridControl.MainView = this.billingGridView;
            this.billingGridControl.MenuManager = this.barManager1;
            this.billingGridControl.Name = "billingGridControl";
            this.billingGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.moneyTextEdit2});
            this.billingGridControl.Size = new System.Drawing.Size(1357, 150);
            this.billingGridControl.TabIndex = 4;
            this.billingGridControl.UseEmbeddedNavigator = true;
            this.billingGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.billingGridView});
            // 
            // spAS11050DepreciationItemBindingSource
            // 
            this.spAS11050DepreciationItemBindingSource.DataMember = "sp_AS_11050_Depreciation_Item";
            this.spAS11050DepreciationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // billingGridView
            // 
            this.billingGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepreciationID1,
            this.colEquipmentID1,
            this.colEquipmentReference,
            this.colNarrative1,
            this.colBalanceSheetCode1,
            this.colProfitLossCode1,
            this.colBillingCentreCodeID1,
            this.colCompanyCode1,
            this.colDepartmentCode1,
            this.colCostCentreCode1,
            this.colDepreciationAmount1,
            this.colPreviousValue1,
            this.colCurrentValue1,
            this.colPeriodNumber1,
            this.colPeriodStartDate1,
            this.colPeriodEndDate1,
            this.colRemarks1,
            this.colAllowEdit1,
            this.colMode9,
            this.colRecordID9});
            this.billingGridView.GridControl = this.billingGridControl;
            this.billingGridView.Name = "billingGridView";
            this.billingGridView.OptionsCustomization.AllowGroup = false;
            this.billingGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.billingGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.billingGridView.OptionsLayout.StoreAppearance = true;
            this.billingGridView.OptionsSelection.MultiSelect = true;
            this.billingGridView.OptionsView.ColumnAutoWidth = false;
            this.billingGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.billingGridView.OptionsView.ShowGroupPanel = false;
            this.billingGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.billingGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.billingGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.billingGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.billingGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.billingGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.billingGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colDepreciationID1
            // 
            this.colDepreciationID1.FieldName = "DepreciationID";
            this.colDepreciationID1.Name = "colDepreciationID1";
            this.colDepreciationID1.OptionsColumn.AllowEdit = false;
            this.colDepreciationID1.OptionsColumn.AllowFocus = false;
            this.colDepreciationID1.OptionsColumn.ReadOnly = true;
            this.colDepreciationID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colDepreciationID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colDepreciationID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID1
            // 
            this.colEquipmentID1.FieldName = "EquipmentID";
            this.colEquipmentID1.Name = "colEquipmentID1";
            this.colEquipmentID1.OptionsColumn.AllowEdit = false;
            this.colEquipmentID1.OptionsColumn.AllowFocus = false;
            this.colEquipmentID1.OptionsColumn.ReadOnly = true;
            this.colEquipmentID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID1.Width = 86;
            // 
            // colEquipmentReference
            // 
            this.colEquipmentReference.FieldName = "EquipmentReference";
            this.colEquipmentReference.Name = "colEquipmentReference";
            this.colEquipmentReference.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference.Visible = true;
            this.colEquipmentReference.VisibleIndex = 0;
            this.colEquipmentReference.Width = 125;
            // 
            // colNarrative1
            // 
            this.colNarrative1.FieldName = "Narrative";
            this.colNarrative1.Name = "colNarrative1";
            this.colNarrative1.OptionsColumn.AllowEdit = false;
            this.colNarrative1.OptionsColumn.AllowFocus = false;
            this.colNarrative1.OptionsColumn.ReadOnly = true;
            this.colNarrative1.OptionsColumn.ShowInCustomizationForm = false;
            this.colNarrative1.OptionsColumn.ShowInExpressionEditor = false;
            this.colNarrative1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colBalanceSheetCode1
            // 
            this.colBalanceSheetCode1.FieldName = "BalanceSheetCode";
            this.colBalanceSheetCode1.Name = "colBalanceSheetCode1";
            this.colBalanceSheetCode1.OptionsColumn.AllowEdit = false;
            this.colBalanceSheetCode1.OptionsColumn.AllowFocus = false;
            this.colBalanceSheetCode1.OptionsColumn.ReadOnly = true;
            this.colBalanceSheetCode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colBalanceSheetCode1.OptionsColumn.ShowInExpressionEditor = false;
            this.colBalanceSheetCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBalanceSheetCode1.Width = 118;
            // 
            // colProfitLossCode1
            // 
            this.colProfitLossCode1.FieldName = "ProfitLossCode";
            this.colProfitLossCode1.Name = "colProfitLossCode1";
            this.colProfitLossCode1.OptionsColumn.AllowEdit = false;
            this.colProfitLossCode1.OptionsColumn.AllowFocus = false;
            this.colProfitLossCode1.OptionsColumn.ReadOnly = true;
            this.colProfitLossCode1.OptionsColumn.ShowInCustomizationForm = false;
            this.colProfitLossCode1.OptionsColumn.ShowInExpressionEditor = false;
            this.colProfitLossCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProfitLossCode1.Width = 100;
            // 
            // colBillingCentreCodeID1
            // 
            this.colBillingCentreCodeID1.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID1.Name = "colBillingCentreCodeID1";
            this.colBillingCentreCodeID1.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID1.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID1.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID1.OptionsColumn.ShowInCustomizationForm = false;
            this.colBillingCentreCodeID1.OptionsColumn.ShowInExpressionEditor = false;
            this.colBillingCentreCodeID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBillingCentreCodeID1.Width = 126;
            // 
            // colCompanyCode1
            // 
            this.colCompanyCode1.FieldName = "CompanyCode";
            this.colCompanyCode1.Name = "colCompanyCode1";
            this.colCompanyCode1.OptionsColumn.AllowEdit = false;
            this.colCompanyCode1.OptionsColumn.AllowFocus = false;
            this.colCompanyCode1.OptionsColumn.ReadOnly = true;
            this.colCompanyCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyCode1.Visible = true;
            this.colCompanyCode1.VisibleIndex = 1;
            this.colCompanyCode1.Width = 210;
            // 
            // colDepartmentCode1
            // 
            this.colDepartmentCode1.FieldName = "DepartmentCode";
            this.colDepartmentCode1.Name = "colDepartmentCode1";
            this.colDepartmentCode1.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode1.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode1.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentCode1.Visible = true;
            this.colDepartmentCode1.VisibleIndex = 2;
            this.colDepartmentCode1.Width = 286;
            // 
            // colCostCentreCode1
            // 
            this.colCostCentreCode1.FieldName = "CostCentreCode";
            this.colCostCentreCode1.Name = "colCostCentreCode1";
            this.colCostCentreCode1.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode1.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode1.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreCode1.Visible = true;
            this.colCostCentreCode1.VisibleIndex = 3;
            this.colCostCentreCode1.Width = 257;
            // 
            // colDepreciationAmount1
            // 
            this.colDepreciationAmount1.ColumnEdit = this.moneyTextEdit2;
            this.colDepreciationAmount1.FieldName = "DepreciationAmount";
            this.colDepreciationAmount1.Name = "colDepreciationAmount1";
            this.colDepreciationAmount1.OptionsColumn.AllowEdit = false;
            this.colDepreciationAmount1.OptionsColumn.AllowFocus = false;
            this.colDepreciationAmount1.OptionsColumn.ReadOnly = true;
            this.colDepreciationAmount1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepreciationAmount1.Visible = true;
            this.colDepreciationAmount1.VisibleIndex = 4;
            this.colDepreciationAmount1.Width = 122;
            // 
            // moneyTextEdit2
            // 
            this.moneyTextEdit2.AutoHeight = false;
            this.moneyTextEdit2.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit2.Name = "moneyTextEdit2";
            // 
            // colPreviousValue1
            // 
            this.colPreviousValue1.FieldName = "PreviousValue";
            this.colPreviousValue1.Name = "colPreviousValue1";
            this.colPreviousValue1.OptionsColumn.AllowEdit = false;
            this.colPreviousValue1.OptionsColumn.AllowFocus = false;
            this.colPreviousValue1.OptionsColumn.ReadOnly = true;
            this.colPreviousValue1.OptionsColumn.ShowInCustomizationForm = false;
            this.colPreviousValue1.OptionsColumn.ShowInExpressionEditor = false;
            this.colPreviousValue1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPreviousValue1.Width = 92;
            // 
            // colCurrentValue1
            // 
            this.colCurrentValue1.FieldName = "CurrentValue";
            this.colCurrentValue1.Name = "colCurrentValue1";
            this.colCurrentValue1.OptionsColumn.AllowEdit = false;
            this.colCurrentValue1.OptionsColumn.AllowFocus = false;
            this.colCurrentValue1.OptionsColumn.ReadOnly = true;
            this.colCurrentValue1.OptionsColumn.ShowInCustomizationForm = false;
            this.colCurrentValue1.OptionsColumn.ShowInExpressionEditor = false;
            this.colCurrentValue1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentValue1.Width = 88;
            // 
            // colPeriodNumber1
            // 
            this.colPeriodNumber1.FieldName = "PeriodNumber";
            this.colPeriodNumber1.Name = "colPeriodNumber1";
            this.colPeriodNumber1.OptionsColumn.AllowEdit = false;
            this.colPeriodNumber1.OptionsColumn.AllowFocus = false;
            this.colPeriodNumber1.OptionsColumn.ReadOnly = true;
            this.colPeriodNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodNumber1.Visible = true;
            this.colPeriodNumber1.VisibleIndex = 5;
            this.colPeriodNumber1.Width = 92;
            // 
            // colPeriodStartDate1
            // 
            this.colPeriodStartDate1.FieldName = "PeriodStartDate";
            this.colPeriodStartDate1.Name = "colPeriodStartDate1";
            this.colPeriodStartDate1.OptionsColumn.AllowEdit = false;
            this.colPeriodStartDate1.OptionsColumn.AllowFocus = false;
            this.colPeriodStartDate1.OptionsColumn.ReadOnly = true;
            this.colPeriodStartDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodStartDate1.Visible = true;
            this.colPeriodStartDate1.VisibleIndex = 6;
            this.colPeriodStartDate1.Width = 105;
            // 
            // colPeriodEndDate1
            // 
            this.colPeriodEndDate1.FieldName = "PeriodEndDate";
            this.colPeriodEndDate1.Name = "colPeriodEndDate1";
            this.colPeriodEndDate1.OptionsColumn.AllowEdit = false;
            this.colPeriodEndDate1.OptionsColumn.AllowFocus = false;
            this.colPeriodEndDate1.OptionsColumn.ReadOnly = true;
            this.colPeriodEndDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodEndDate1.Visible = true;
            this.colPeriodEndDate1.VisibleIndex = 7;
            this.colPeriodEndDate1.Width = 99;
            // 
            // colRemarks1
            // 
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.AllowEdit = false;
            this.colRemarks1.OptionsColumn.AllowFocus = false;
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.OptionsColumn.ShowInCustomizationForm = false;
            this.colRemarks1.OptionsColumn.ShowInExpressionEditor = false;
            this.colRemarks1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colAllowEdit1
            // 
            this.colAllowEdit1.FieldName = "AllowEdit";
            this.colAllowEdit1.Name = "colAllowEdit1";
            this.colAllowEdit1.OptionsColumn.AllowEdit = false;
            this.colAllowEdit1.OptionsColumn.AllowFocus = false;
            this.colAllowEdit1.OptionsColumn.ReadOnly = true;
            this.colAllowEdit1.OptionsColumn.ShowInCustomizationForm = false;
            this.colAllowEdit1.OptionsColumn.ShowInExpressionEditor = false;
            this.colAllowEdit1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode9
            // 
            this.colMode9.FieldName = "Mode";
            this.colMode9.Name = "colMode9";
            this.colMode9.OptionsColumn.AllowEdit = false;
            this.colMode9.OptionsColumn.AllowFocus = false;
            this.colMode9.OptionsColumn.ReadOnly = true;
            this.colMode9.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode9.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID9
            // 
            this.colRecordID9.FieldName = "RecordID";
            this.colRecordID9.Name = "colRecordID9";
            this.colRecordID9.OptionsColumn.AllowEdit = false;
            this.colRecordID9.OptionsColumn.AllowFocus = false;
            this.colRecordID9.OptionsColumn.ReadOnly = true;
            this.colRecordID9.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID9.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // notificationTabPage
            // 
            this.notificationTabPage.Controls.Add(this.notificationGridControl);
            this.notificationTabPage.Name = "notificationTabPage";
            this.notificationTabPage.Size = new System.Drawing.Size(1357, 150);
            this.notificationTabPage.Text = "Notification Details";
            // 
            // notificationGridControl
            // 
            this.notificationGridControl.DataSource = this.spAS11126NotificationItemBindingSource;
            this.notificationGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.notificationGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.notificationGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.notificationGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.notificationGridControl.Location = new System.Drawing.Point(0, 0);
            this.notificationGridControl.MainView = this.notificationGridView;
            this.notificationGridControl.MenuManager = this.barManager1;
            this.notificationGridControl.Name = "notificationGridControl";
            this.notificationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit10,
            this.repositoryItemMemoExEdit15});
            this.notificationGridControl.Size = new System.Drawing.Size(1357, 150);
            this.notificationGridControl.TabIndex = 1;
            this.notificationGridControl.UseEmbeddedNavigator = true;
            this.notificationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.notificationGridView});
            // 
            // spAS11126NotificationItemBindingSource
            // 
            this.spAS11126NotificationItemBindingSource.DataMember = "sp_AS_11126_Notification_Item";
            this.spAS11126NotificationItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // notificationGridView
            // 
            this.notificationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNotificationID,
            this.colNotificationTypeID,
            this.colNotificationType,
            this.colLinkedToRecordID,
            this.colLinkedToRecordType,
            this.colEquipmentReference12,
            this.colPriorityID,
            this.colPriority,
            this.colDateToRemind,
            this.colDateCreated,
            this.colKeeperTypeID1,
            this.colKeeperType1,
            this.colKeeperID1,
            this.colFullName,
            this.colMessage,
            this.colStatusID1,
            this.colStatus1,
            this.colMode13,
            this.colRecordID13});
            this.notificationGridView.GridControl = this.notificationGridControl;
            this.notificationGridView.Name = "notificationGridView";
            this.notificationGridView.OptionsCustomization.AllowFilter = false;
            this.notificationGridView.OptionsCustomization.AllowGroup = false;
            this.notificationGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.notificationGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.notificationGridView.OptionsLayout.StoreAppearance = true;
            this.notificationGridView.OptionsSelection.MultiSelect = true;
            this.notificationGridView.OptionsView.ColumnAutoWidth = false;
            this.notificationGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.notificationGridView.OptionsView.ShowGroupPanel = false;
            this.notificationGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.notificationGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.notificationGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.notificationGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.notificationGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.notificationGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.notificationGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colNotificationID
            // 
            this.colNotificationID.FieldName = "NotificationID";
            this.colNotificationID.Name = "colNotificationID";
            this.colNotificationID.OptionsColumn.AllowEdit = false;
            this.colNotificationID.OptionsColumn.AllowFocus = false;
            this.colNotificationID.OptionsColumn.ReadOnly = true;
            this.colNotificationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotificationID.Width = 80;
            // 
            // colNotificationTypeID
            // 
            this.colNotificationTypeID.FieldName = "NotificationTypeID";
            this.colNotificationTypeID.Name = "colNotificationTypeID";
            this.colNotificationTypeID.OptionsColumn.AllowEdit = false;
            this.colNotificationTypeID.OptionsColumn.AllowFocus = false;
            this.colNotificationTypeID.OptionsColumn.ReadOnly = true;
            this.colNotificationTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotificationTypeID.Width = 107;
            // 
            // colNotificationType
            // 
            this.colNotificationType.FieldName = "NotificationType";
            this.colNotificationType.Name = "colNotificationType";
            this.colNotificationType.OptionsColumn.AllowEdit = false;
            this.colNotificationType.OptionsColumn.AllowFocus = false;
            this.colNotificationType.OptionsColumn.ReadOnly = true;
            this.colNotificationType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotificationType.Visible = true;
            this.colNotificationType.VisibleIndex = 1;
            this.colNotificationType.Width = 157;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToRecordID.Width = 108;
            // 
            // colLinkedToRecordType
            // 
            this.colLinkedToRecordType.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType.Name = "colLinkedToRecordType";
            this.colLinkedToRecordType.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToRecordType.Width = 121;
            // 
            // colEquipmentReference12
            // 
            this.colEquipmentReference12.FieldName = "EquipmentReference";
            this.colEquipmentReference12.Name = "colEquipmentReference12";
            this.colEquipmentReference12.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference12.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference12.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference12.Visible = true;
            this.colEquipmentReference12.VisibleIndex = 0;
            this.colEquipmentReference12.Width = 117;
            // 
            // colPriorityID
            // 
            this.colPriorityID.FieldName = "PriorityID";
            this.colPriorityID.Name = "colPriorityID";
            this.colPriorityID.OptionsColumn.AllowEdit = false;
            this.colPriorityID.OptionsColumn.AllowFocus = false;
            this.colPriorityID.OptionsColumn.ReadOnly = true;
            this.colPriorityID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPriority
            // 
            this.colPriority.FieldName = "Priority";
            this.colPriority.Name = "colPriority";
            this.colPriority.OptionsColumn.AllowEdit = false;
            this.colPriority.OptionsColumn.AllowFocus = false;
            this.colPriority.OptionsColumn.ReadOnly = true;
            this.colPriority.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPriority.Visible = true;
            this.colPriority.VisibleIndex = 2;
            this.colPriority.Width = 133;
            // 
            // colDateToRemind
            // 
            this.colDateToRemind.FieldName = "DateToRemind";
            this.colDateToRemind.Name = "colDateToRemind";
            this.colDateToRemind.OptionsColumn.AllowEdit = false;
            this.colDateToRemind.OptionsColumn.AllowFocus = false;
            this.colDateToRemind.OptionsColumn.ReadOnly = true;
            this.colDateToRemind.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateToRemind.Visible = true;
            this.colDateToRemind.VisibleIndex = 3;
            this.colDateToRemind.Width = 88;
            // 
            // colDateCreated
            // 
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 4;
            this.colDateCreated.Width = 77;
            // 
            // colKeeperTypeID1
            // 
            this.colKeeperTypeID1.FieldName = "KeeperTypeID";
            this.colKeeperTypeID1.Name = "colKeeperTypeID1";
            this.colKeeperTypeID1.OptionsColumn.AllowEdit = false;
            this.colKeeperTypeID1.OptionsColumn.AllowFocus = false;
            this.colKeeperTypeID1.OptionsColumn.ReadOnly = true;
            this.colKeeperTypeID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperTypeID1.Width = 87;
            // 
            // colKeeperType1
            // 
            this.colKeeperType1.FieldName = "KeeperType";
            this.colKeeperType1.Name = "colKeeperType1";
            this.colKeeperType1.OptionsColumn.AllowEdit = false;
            this.colKeeperType1.OptionsColumn.AllowFocus = false;
            this.colKeeperType1.OptionsColumn.ReadOnly = true;
            this.colKeeperType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperType1.Visible = true;
            this.colKeeperType1.VisibleIndex = 5;
            this.colKeeperType1.Width = 148;
            // 
            // colKeeperID1
            // 
            this.colKeeperID1.FieldName = "KeeperID";
            this.colKeeperID1.Name = "colKeeperID1";
            this.colKeeperID1.OptionsColumn.AllowEdit = false;
            this.colKeeperID1.OptionsColumn.AllowFocus = false;
            this.colKeeperID1.OptionsColumn.ReadOnly = true;
            this.colKeeperID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colFullName
            // 
            this.colFullName.FieldName = "FullName";
            this.colFullName.Name = "colFullName";
            this.colFullName.OptionsColumn.AllowEdit = false;
            this.colFullName.OptionsColumn.AllowFocus = false;
            this.colFullName.OptionsColumn.ReadOnly = true;
            this.colFullName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 6;
            this.colFullName.Width = 202;
            // 
            // colMessage
            // 
            this.colMessage.FieldName = "Message";
            this.colMessage.Name = "colMessage";
            this.colMessage.OptionsColumn.AllowEdit = false;
            this.colMessage.OptionsColumn.AllowFocus = false;
            this.colMessage.OptionsColumn.ReadOnly = true;
            this.colMessage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMessage.Visible = true;
            this.colMessage.VisibleIndex = 8;
            this.colMessage.Width = 728;
            // 
            // colStatusID1
            // 
            this.colStatusID1.FieldName = "StatusID";
            this.colStatusID1.Name = "colStatusID1";
            this.colStatusID1.OptionsColumn.AllowEdit = false;
            this.colStatusID1.OptionsColumn.AllowFocus = false;
            this.colStatusID1.OptionsColumn.ReadOnly = true;
            this.colStatusID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus1
            // 
            this.colStatus1.FieldName = "Status";
            this.colStatus1.Name = "colStatus1";
            this.colStatus1.OptionsColumn.AllowEdit = false;
            this.colStatus1.OptionsColumn.AllowFocus = false;
            this.colStatus1.OptionsColumn.ReadOnly = true;
            this.colStatus1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus1.Visible = true;
            this.colStatus1.VisibleIndex = 7;
            this.colStatus1.Width = 128;
            // 
            // colMode13
            // 
            this.colMode13.FieldName = "Mode";
            this.colMode13.Name = "colMode13";
            this.colMode13.OptionsColumn.AllowEdit = false;
            this.colMode13.OptionsColumn.AllowFocus = false;
            this.colMode13.OptionsColumn.ReadOnly = true;
            this.colMode13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID13
            // 
            this.colRecordID13.FieldName = "RecordID";
            this.colRecordID13.Name = "colRecordID13";
            this.colRecordID13.OptionsColumn.AllowEdit = false;
            this.colRecordID13.OptionsColumn.AllowFocus = false;
            this.colRecordID13.OptionsColumn.ReadOnly = true;
            this.colRecordID13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ReadOnly = true;
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit15
            // 
            this.repositoryItemMemoExEdit15.AutoHeight = false;
            this.repositoryItemMemoExEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit15.Name = "repositoryItemMemoExEdit15";
            this.repositoryItemMemoExEdit15.ReadOnly = true;
            this.repositoryItemMemoExEdit15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit15.ShowIcon = false;
            // 
            // depreciationTabPage
            // 
            this.depreciationTabPage.Controls.Add(this.depreciationGridControl);
            this.depreciationTabPage.Name = "depreciationTabPage";
            this.depreciationTabPage.Size = new System.Drawing.Size(1357, 150);
            this.depreciationTabPage.Text = "Depreciation Data";
            // 
            // depreciationGridControl
            // 
            this.depreciationGridControl.DataSource = this.spAS11050DepreciationItemBindingSource;
            this.depreciationGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.depreciationGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.depreciationGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.depreciationGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.depreciationGridControl.Location = new System.Drawing.Point(0, 0);
            this.depreciationGridControl.MainView = this.depreciationGridView;
            this.depreciationGridControl.MenuManager = this.barManager1;
            this.depreciationGridControl.Name = "depreciationGridControl";
            this.depreciationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.moneyTextEditDep});
            this.depreciationGridControl.Size = new System.Drawing.Size(1357, 150);
            this.depreciationGridControl.TabIndex = 3;
            this.depreciationGridControl.UseEmbeddedNavigator = true;
            this.depreciationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.depreciationGridView});
            // 
            // depreciationGridView
            // 
            this.depreciationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepreciationID,
            this.colEquipmentID,
            this.colEquipmentReference1,
            this.colNarrative,
            this.colBalanceSheetCode,
            this.colProfitLossCode,
            this.colBillingCentreCodeID,
            this.colCompanyCode,
            this.colDepartmentCode,
            this.colCostCentreCode,
            this.colDepreciationAmount,
            this.colPreviousValue,
            this.colCurrentValue,
            this.colPeriodNumber,
            this.colPeriodStartDate,
            this.colPeriodEndDate,
            this.colRemarks,
            this.colAllowEdit,
            this.colMode,
            this.colRecordID});
            this.depreciationGridView.GridControl = this.depreciationGridControl;
            this.depreciationGridView.Name = "depreciationGridView";
            this.depreciationGridView.OptionsCustomization.AllowGroup = false;
            this.depreciationGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.depreciationGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.depreciationGridView.OptionsLayout.StoreAppearance = true;
            this.depreciationGridView.OptionsSelection.MultiSelect = true;
            this.depreciationGridView.OptionsView.ColumnAutoWidth = false;
            this.depreciationGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.depreciationGridView.OptionsView.ShowGroupPanel = false;
            this.depreciationGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colNarrative, DevExpress.Data.ColumnSortOrder.Descending)});
            this.depreciationGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.depreciationGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.depreciationGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.depreciationGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.depreciationGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colDepreciationID
            // 
            this.colDepreciationID.FieldName = "DepreciationID";
            this.colDepreciationID.Name = "colDepreciationID";
            this.colDepreciationID.OptionsColumn.AllowEdit = false;
            this.colDepreciationID.OptionsColumn.AllowFocus = false;
            this.colDepreciationID.OptionsColumn.ReadOnly = true;
            this.colDepreciationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentID
            // 
            this.colEquipmentID.FieldName = "EquipmentID";
            this.colEquipmentID.Name = "colEquipmentID";
            this.colEquipmentID.OptionsColumn.AllowEdit = false;
            this.colEquipmentID.OptionsColumn.AllowFocus = false;
            this.colEquipmentID.OptionsColumn.ReadOnly = true;
            this.colEquipmentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEquipmentReference1
            // 
            this.colEquipmentReference1.FieldName = "EquipmentReference";
            this.colEquipmentReference1.Name = "colEquipmentReference1";
            this.colEquipmentReference1.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference1.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference1.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference1.Visible = true;
            this.colEquipmentReference1.VisibleIndex = 0;
            this.colEquipmentReference1.Width = 125;
            // 
            // colNarrative
            // 
            this.colNarrative.FieldName = "Narrative";
            this.colNarrative.Name = "colNarrative";
            this.colNarrative.OptionsColumn.AllowEdit = false;
            this.colNarrative.OptionsColumn.AllowFocus = false;
            this.colNarrative.OptionsColumn.ReadOnly = true;
            this.colNarrative.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNarrative.Visible = true;
            this.colNarrative.VisibleIndex = 1;
            this.colNarrative.Width = 80;
            // 
            // colBalanceSheetCode
            // 
            this.colBalanceSheetCode.FieldName = "BalanceSheetCode";
            this.colBalanceSheetCode.Name = "colBalanceSheetCode";
            this.colBalanceSheetCode.OptionsColumn.AllowEdit = false;
            this.colBalanceSheetCode.OptionsColumn.AllowFocus = false;
            this.colBalanceSheetCode.OptionsColumn.ReadOnly = true;
            this.colBalanceSheetCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBalanceSheetCode.Visible = true;
            this.colBalanceSheetCode.VisibleIndex = 2;
            this.colBalanceSheetCode.Width = 118;
            // 
            // colProfitLossCode
            // 
            this.colProfitLossCode.FieldName = "ProfitLossCode";
            this.colProfitLossCode.Name = "colProfitLossCode";
            this.colProfitLossCode.OptionsColumn.AllowEdit = false;
            this.colProfitLossCode.OptionsColumn.AllowFocus = false;
            this.colProfitLossCode.OptionsColumn.ReadOnly = true;
            this.colProfitLossCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProfitLossCode.Visible = true;
            this.colProfitLossCode.VisibleIndex = 3;
            this.colProfitLossCode.Width = 168;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBillingCentreCodeID.Width = 126;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyCode.Visible = true;
            this.colCompanyCode.VisibleIndex = 4;
            this.colCompanyCode.Width = 158;
            // 
            // colDepartmentCode
            // 
            this.colDepartmentCode.FieldName = "DepartmentCode";
            this.colDepartmentCode.Name = "colDepartmentCode";
            this.colDepartmentCode.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentCode.Visible = true;
            this.colDepartmentCode.VisibleIndex = 5;
            this.colDepartmentCode.Width = 150;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreCode.Visible = true;
            this.colCostCentreCode.VisibleIndex = 6;
            this.colCostCentreCode.Width = 128;
            // 
            // colDepreciationAmount
            // 
            this.colDepreciationAmount.ColumnEdit = this.moneyTextEditDep;
            this.colDepreciationAmount.FieldName = "DepreciationAmount";
            this.colDepreciationAmount.Name = "colDepreciationAmount";
            this.colDepreciationAmount.OptionsColumn.AllowEdit = false;
            this.colDepreciationAmount.OptionsColumn.AllowFocus = false;
            this.colDepreciationAmount.OptionsColumn.ReadOnly = true;
            this.colDepreciationAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepreciationAmount.Visible = true;
            this.colDepreciationAmount.VisibleIndex = 7;
            this.colDepreciationAmount.Width = 122;
            // 
            // moneyTextEditDep
            // 
            this.moneyTextEditDep.AutoHeight = false;
            this.moneyTextEditDep.DisplayFormat.FormatString = "c2";
            this.moneyTextEditDep.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEditDep.Name = "moneyTextEditDep";
            // 
            // colPreviousValue
            // 
            this.colPreviousValue.ColumnEdit = this.moneyTextEditDep;
            this.colPreviousValue.FieldName = "PreviousValue";
            this.colPreviousValue.Name = "colPreviousValue";
            this.colPreviousValue.OptionsColumn.AllowEdit = false;
            this.colPreviousValue.OptionsColumn.AllowFocus = false;
            this.colPreviousValue.OptionsColumn.ReadOnly = true;
            this.colPreviousValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPreviousValue.Visible = true;
            this.colPreviousValue.VisibleIndex = 8;
            this.colPreviousValue.Width = 92;
            // 
            // colCurrentValue
            // 
            this.colCurrentValue.ColumnEdit = this.moneyTextEditDep;
            this.colCurrentValue.FieldName = "CurrentValue";
            this.colCurrentValue.Name = "colCurrentValue";
            this.colCurrentValue.OptionsColumn.AllowEdit = false;
            this.colCurrentValue.OptionsColumn.AllowFocus = false;
            this.colCurrentValue.OptionsColumn.ReadOnly = true;
            this.colCurrentValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentValue.Visible = true;
            this.colCurrentValue.VisibleIndex = 9;
            this.colCurrentValue.Width = 88;
            // 
            // colPeriodNumber
            // 
            this.colPeriodNumber.FieldName = "PeriodNumber";
            this.colPeriodNumber.Name = "colPeriodNumber";
            this.colPeriodNumber.OptionsColumn.AllowEdit = false;
            this.colPeriodNumber.OptionsColumn.AllowFocus = false;
            this.colPeriodNumber.OptionsColumn.ReadOnly = true;
            this.colPeriodNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodNumber.Visible = true;
            this.colPeriodNumber.VisibleIndex = 10;
            this.colPeriodNumber.Width = 92;
            // 
            // colPeriodStartDate
            // 
            this.colPeriodStartDate.FieldName = "PeriodStartDate";
            this.colPeriodStartDate.Name = "colPeriodStartDate";
            this.colPeriodStartDate.OptionsColumn.AllowEdit = false;
            this.colPeriodStartDate.OptionsColumn.AllowFocus = false;
            this.colPeriodStartDate.OptionsColumn.ReadOnly = true;
            this.colPeriodStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodStartDate.Visible = true;
            this.colPeriodStartDate.VisibleIndex = 11;
            this.colPeriodStartDate.Width = 105;
            // 
            // colPeriodEndDate
            // 
            this.colPeriodEndDate.FieldName = "PeriodEndDate";
            this.colPeriodEndDate.Name = "colPeriodEndDate";
            this.colPeriodEndDate.OptionsColumn.AllowEdit = false;
            this.colPeriodEndDate.OptionsColumn.AllowFocus = false;
            this.colPeriodEndDate.OptionsColumn.ReadOnly = true;
            this.colPeriodEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPeriodEndDate.Visible = true;
            this.colPeriodEndDate.VisibleIndex = 12;
            this.colPeriodEndDate.Width = 99;
            // 
            // colRemarks
            // 
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 13;
            this.colRemarks.Width = 670;
            // 
            // colAllowEdit
            // 
            this.colAllowEdit.FieldName = "AllowEdit";
            this.colAllowEdit.Name = "colAllowEdit";
            this.colAllowEdit.OptionsColumn.AllowEdit = false;
            this.colAllowEdit.OptionsColumn.AllowFocus = false;
            this.colAllowEdit.OptionsColumn.ReadOnly = true;
            this.colAllowEdit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // workDetailTabPage
            // 
            this.workDetailTabPage.Controls.Add(this.workDetailGridControl);
            this.workDetailTabPage.Name = "workDetailTabPage";
            this.workDetailTabPage.Size = new System.Drawing.Size(1357, 150);
            this.workDetailTabPage.Text = "Work Order Details";
            // 
            // workDetailGridControl
            // 
            this.workDetailGridControl.DataSource = this.spAS11078WorkDetailItemBindingSource;
            this.workDetailGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.workDetailGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.workDetailGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.workDetailGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.workDetailGridControl.Location = new System.Drawing.Point(0, 0);
            this.workDetailGridControl.MainView = this.workDetailGridView;
            this.workDetailGridControl.MenuManager = this.barManager1;
            this.workDetailGridControl.Name = "workDetailGridControl";
            this.workDetailGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit13,
            this.repositoryItemMemoExEdit14});
            this.workDetailGridControl.Size = new System.Drawing.Size(1357, 150);
            this.workDetailGridControl.TabIndex = 1;
            this.workDetailGridControl.UseEmbeddedNavigator = true;
            this.workDetailGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.workDetailGridView});
            // 
            // spAS11078WorkDetailItemBindingSource
            // 
            this.spAS11078WorkDetailItemBindingSource.DataMember = "sp_AS_11078_Work_Detail_Item";
            this.spAS11078WorkDetailItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // workDetailGridView
            // 
            this.workDetailGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkDetailID,
            this.colEquipmentID2,
            this.colEquipmentReference11,
            this.colWorkType,
            this.colWorkTypeID,
            this.colDateRaised,
            this.colSupplierID,
            this.colSupplierReference,
            this.colStatusID,
            this.colStatus,
            this.colDescription2,
            this.colTransactionID,
            this.colOrderNumber,
            this.colCurrentMileage,
            this.colWorkCompletionDate,
            this.colServiceIntervalID,
            this.colServiceType,
            this.colNotes2,
            this.colMode10,
            this.colRecordID10});
            this.workDetailGridView.GridControl = this.workDetailGridControl;
            this.workDetailGridView.Name = "workDetailGridView";
            this.workDetailGridView.OptionsCustomization.AllowFilter = false;
            this.workDetailGridView.OptionsCustomization.AllowGroup = false;
            this.workDetailGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.workDetailGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.workDetailGridView.OptionsLayout.StoreAppearance = true;
            this.workDetailGridView.OptionsSelection.MultiSelect = true;
            this.workDetailGridView.OptionsView.ColumnAutoWidth = false;
            this.workDetailGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.workDetailGridView.OptionsView.ShowGroupPanel = false;
            this.workDetailGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.workDetailGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.workDetailGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.workDetailGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.workDetailGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.workDetailGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.workDetailGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colWorkDetailID
            // 
            this.colWorkDetailID.FieldName = "WorkDetailID";
            this.colWorkDetailID.Name = "colWorkDetailID";
            this.colWorkDetailID.OptionsColumn.AllowEdit = false;
            this.colWorkDetailID.OptionsColumn.AllowFocus = false;
            this.colWorkDetailID.OptionsColumn.ReadOnly = true;
            this.colWorkDetailID.OptionsColumn.ShowInCustomizationForm = false;
            this.colWorkDetailID.OptionsColumn.ShowInExpressionEditor = false;
            this.colWorkDetailID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkDetailID.Width = 81;
            // 
            // colEquipmentID2
            // 
            this.colEquipmentID2.FieldName = "EquipmentID";
            this.colEquipmentID2.Name = "colEquipmentID2";
            this.colEquipmentID2.OptionsColumn.AllowEdit = false;
            this.colEquipmentID2.OptionsColumn.AllowFocus = false;
            this.colEquipmentID2.OptionsColumn.ReadOnly = true;
            this.colEquipmentID2.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID2.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID2.Width = 76;
            // 
            // colEquipmentReference11
            // 
            this.colEquipmentReference11.FieldName = "EquipmentReference";
            this.colEquipmentReference11.Name = "colEquipmentReference11";
            this.colEquipmentReference11.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference11.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference11.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference11.Visible = true;
            this.colEquipmentReference11.VisibleIndex = 0;
            this.colEquipmentReference11.Width = 115;
            // 
            // colWorkType
            // 
            this.colWorkType.FieldName = "WorkType";
            this.colWorkType.Name = "colWorkType";
            this.colWorkType.OptionsColumn.AllowEdit = false;
            this.colWorkType.OptionsColumn.AllowFocus = false;
            this.colWorkType.OptionsColumn.ReadOnly = true;
            this.colWorkType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkType.Visible = true;
            this.colWorkType.VisibleIndex = 1;
            this.colWorkType.Width = 193;
            // 
            // colWorkTypeID
            // 
            this.colWorkTypeID.FieldName = "WorkTypeID";
            this.colWorkTypeID.Name = "colWorkTypeID";
            this.colWorkTypeID.OptionsColumn.AllowEdit = false;
            this.colWorkTypeID.OptionsColumn.AllowFocus = false;
            this.colWorkTypeID.OptionsColumn.ReadOnly = true;
            this.colWorkTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colWorkTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colWorkTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkTypeID.Width = 78;
            // 
            // colDateRaised
            // 
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 2;
            // 
            // colSupplierID
            // 
            this.colSupplierID.FieldName = "SupplierID";
            this.colSupplierID.Name = "colSupplierID";
            this.colSupplierID.OptionsColumn.AllowEdit = false;
            this.colSupplierID.OptionsColumn.AllowFocus = false;
            this.colSupplierID.OptionsColumn.ReadOnly = true;
            this.colSupplierID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSupplierID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSupplierID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSupplierReference
            // 
            this.colSupplierReference.FieldName = "SupplierReference";
            this.colSupplierReference.Name = "colSupplierReference";
            this.colSupplierReference.OptionsColumn.AllowEdit = false;
            this.colSupplierReference.OptionsColumn.AllowFocus = false;
            this.colSupplierReference.OptionsColumn.ReadOnly = true;
            this.colSupplierReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSupplierReference.Visible = true;
            this.colSupplierReference.VisibleIndex = 3;
            this.colSupplierReference.Width = 211;
            // 
            // colStatusID
            // 
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 4;
            this.colStatus.Width = 180;
            // 
            // colDescription2
            // 
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 5;
            this.colDescription2.Width = 353;
            // 
            // colTransactionID
            // 
            this.colTransactionID.FieldName = "TransactionID";
            this.colTransactionID.Name = "colTransactionID";
            this.colTransactionID.OptionsColumn.AllowEdit = false;
            this.colTransactionID.OptionsColumn.AllowFocus = false;
            this.colTransactionID.OptionsColumn.ReadOnly = true;
            this.colTransactionID.OptionsColumn.ShowInCustomizationForm = false;
            this.colTransactionID.OptionsColumn.ShowInExpressionEditor = false;
            this.colTransactionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransactionID.Width = 82;
            // 
            // colOrderNumber
            // 
            this.colOrderNumber.FieldName = "OrderNumber";
            this.colOrderNumber.Name = "colOrderNumber";
            this.colOrderNumber.OptionsColumn.AllowEdit = false;
            this.colOrderNumber.OptionsColumn.AllowFocus = false;
            this.colOrderNumber.OptionsColumn.ReadOnly = true;
            this.colOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOrderNumber.Visible = true;
            this.colOrderNumber.VisibleIndex = 6;
            this.colOrderNumber.Width = 80;
            // 
            // colCurrentMileage
            // 
            this.colCurrentMileage.FieldName = "CurrentMileage";
            this.colCurrentMileage.Name = "colCurrentMileage";
            this.colCurrentMileage.OptionsColumn.AllowEdit = false;
            this.colCurrentMileage.OptionsColumn.AllowFocus = false;
            this.colCurrentMileage.OptionsColumn.ReadOnly = true;
            this.colCurrentMileage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentMileage.Visible = true;
            this.colCurrentMileage.VisibleIndex = 7;
            this.colCurrentMileage.Width = 135;
            // 
            // colWorkCompletionDate
            // 
            this.colWorkCompletionDate.FieldName = "WorkCompletionDate";
            this.colWorkCompletionDate.Name = "colWorkCompletionDate";
            this.colWorkCompletionDate.OptionsColumn.AllowEdit = false;
            this.colWorkCompletionDate.OptionsColumn.AllowFocus = false;
            this.colWorkCompletionDate.OptionsColumn.ReadOnly = true;
            this.colWorkCompletionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkCompletionDate.Visible = true;
            this.colWorkCompletionDate.VisibleIndex = 8;
            this.colWorkCompletionDate.Width = 119;
            // 
            // colServiceIntervalID
            // 
            this.colServiceIntervalID.FieldName = "ServiceIntervalID";
            this.colServiceIntervalID.Name = "colServiceIntervalID";
            this.colServiceIntervalID.OptionsColumn.AllowEdit = false;
            this.colServiceIntervalID.OptionsColumn.AllowFocus = false;
            this.colServiceIntervalID.OptionsColumn.ReadOnly = true;
            this.colServiceIntervalID.OptionsColumn.ShowInCustomizationForm = false;
            this.colServiceIntervalID.OptionsColumn.ShowInExpressionEditor = false;
            this.colServiceIntervalID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceIntervalID.Width = 102;
            // 
            // colServiceType
            // 
            this.colServiceType.FieldName = "ServiceType";
            this.colServiceType.Name = "colServiceType";
            this.colServiceType.OptionsColumn.AllowEdit = false;
            this.colServiceType.OptionsColumn.AllowFocus = false;
            this.colServiceType.OptionsColumn.ReadOnly = true;
            this.colServiceType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colServiceType.Visible = true;
            this.colServiceType.VisibleIndex = 9;
            // 
            // colNotes2
            // 
            this.colNotes2.FieldName = "Notes";
            this.colNotes2.Name = "colNotes2";
            this.colNotes2.OptionsColumn.AllowEdit = false;
            this.colNotes2.OptionsColumn.AllowFocus = false;
            this.colNotes2.OptionsColumn.ReadOnly = true;
            this.colNotes2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes2.Visible = true;
            this.colNotes2.VisibleIndex = 10;
            this.colNotes2.Width = 586;
            // 
            // colMode10
            // 
            this.colMode10.FieldName = "Mode";
            this.colMode10.Name = "colMode10";
            this.colMode10.OptionsColumn.AllowEdit = false;
            this.colMode10.OptionsColumn.AllowFocus = false;
            this.colMode10.OptionsColumn.ReadOnly = true;
            this.colMode10.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode10.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID10
            // 
            this.colRecordID10.FieldName = "RecordID";
            this.colRecordID10.Name = "colRecordID10";
            this.colRecordID10.OptionsColumn.AllowEdit = false;
            this.colRecordID10.OptionsColumn.AllowFocus = false;
            this.colRecordID10.OptionsColumn.ReadOnly = true;
            this.colRecordID10.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID10.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit13
            // 
            this.repositoryItemMemoExEdit13.AutoHeight = false;
            this.repositoryItemMemoExEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit13.Name = "repositoryItemMemoExEdit13";
            this.repositoryItemMemoExEdit13.ReadOnly = true;
            this.repositoryItemMemoExEdit13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit13.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit14
            // 
            this.repositoryItemMemoExEdit14.AutoHeight = false;
            this.repositoryItemMemoExEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit14.Name = "repositoryItemMemoExEdit14";
            this.repositoryItemMemoExEdit14.ReadOnly = true;
            this.repositoryItemMemoExEdit14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit14.ShowIcon = false;
            // 
            // incidentTabPage
            // 
            this.incidentTabPage.Controls.Add(this.incidentGridControl);
            this.incidentTabPage.Name = "incidentTabPage";
            this.incidentTabPage.Size = new System.Drawing.Size(1357, 150);
            this.incidentTabPage.Text = "Incident Details";
            // 
            // incidentGridControl
            // 
            this.incidentGridControl.DataSource = this.spAS11081IncidentItemBindingSource;
            this.incidentGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.incidentGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.incidentGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.incidentGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.incidentGridControl.Location = new System.Drawing.Point(0, 0);
            this.incidentGridControl.MainView = this.incidentGridView;
            this.incidentGridControl.MenuManager = this.barManager1;
            this.incidentGridControl.Name = "incidentGridControl";
            this.incidentGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.incidentNotesMemoEdit});
            this.incidentGridControl.Size = new System.Drawing.Size(1357, 150);
            this.incidentGridControl.TabIndex = 3;
            this.incidentGridControl.UseEmbeddedNavigator = true;
            this.incidentGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.incidentGridView});
            // 
            // spAS11081IncidentItemBindingSource
            // 
            this.spAS11081IncidentItemBindingSource.DataMember = "sp_AS_11081_Incident_Item";
            this.spAS11081IncidentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // incidentGridView
            // 
            this.incidentGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIncidentID,
            this.gridColumn1,
            this.gridColumn2,
            this.colIncidentStatusID,
            this.colIncidentStatus,
            this.colIncidentTypeID,
            this.colIncidentType,
            this.colIncidentReference,
            this.colDateHappened,
            this.colLocation,
            this.colRepairDueID,
            this.colRepairDue,
            this.colSeverityID,
            this.colSeverity,
            this.colWitness,
            this.colKeeperAtFaultID,
            this.colKeeperAtFault,
            this.colLegalActionID,
            this.colLegalAction,
            this.colFollowUpDate,
            this.colCost,
            this.colNotes4,
            this.gridColumn3,
            this.gridColumn4});
            this.incidentGridView.GridControl = this.incidentGridControl;
            this.incidentGridView.Name = "incidentGridView";
            this.incidentGridView.OptionsCustomization.AllowFilter = false;
            this.incidentGridView.OptionsCustomization.AllowGroup = false;
            this.incidentGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.incidentGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.incidentGridView.OptionsLayout.StoreAppearance = true;
            this.incidentGridView.OptionsSelection.MultiSelect = true;
            this.incidentGridView.OptionsView.ColumnAutoWidth = false;
            this.incidentGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.incidentGridView.OptionsView.ShowGroupPanel = false;
            this.incidentGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.incidentGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.incidentGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.incidentGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.incidentGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.incidentGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.incidentGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colIncidentID
            // 
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            this.colIncidentID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn1
            // 
            this.gridColumn1.FieldName = "EquipmentID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn1.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 76;
            // 
            // gridColumn2
            // 
            this.gridColumn2.FieldName = "EquipmentReference";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 115;
            // 
            // colIncidentStatusID
            // 
            this.colIncidentStatusID.FieldName = "IncidentStatusID";
            this.colIncidentStatusID.Name = "colIncidentStatusID";
            this.colIncidentStatusID.OptionsColumn.AllowEdit = false;
            this.colIncidentStatusID.OptionsColumn.AllowFocus = false;
            this.colIncidentStatusID.OptionsColumn.ReadOnly = true;
            this.colIncidentStatusID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentStatusID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentStatusID.Width = 99;
            // 
            // colIncidentStatus
            // 
            this.colIncidentStatus.FieldName = "IncidentStatus";
            this.colIncidentStatus.Name = "colIncidentStatus";
            this.colIncidentStatus.OptionsColumn.AllowEdit = false;
            this.colIncidentStatus.OptionsColumn.AllowFocus = false;
            this.colIncidentStatus.OptionsColumn.ReadOnly = true;
            this.colIncidentStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentStatus.Visible = true;
            this.colIncidentStatus.VisibleIndex = 2;
            this.colIncidentStatus.Width = 96;
            // 
            // colIncidentTypeID
            // 
            this.colIncidentTypeID.FieldName = "IncidentTypeID";
            this.colIncidentTypeID.Name = "colIncidentTypeID";
            this.colIncidentTypeID.OptionsColumn.AllowEdit = false;
            this.colIncidentTypeID.OptionsColumn.AllowFocus = false;
            this.colIncidentTypeID.OptionsColumn.ReadOnly = true;
            this.colIncidentTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colIncidentTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colIncidentTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentTypeID.Width = 92;
            // 
            // colIncidentType
            // 
            this.colIncidentType.FieldName = "IncidentType";
            this.colIncidentType.Name = "colIncidentType";
            this.colIncidentType.OptionsColumn.AllowEdit = false;
            this.colIncidentType.OptionsColumn.AllowFocus = false;
            this.colIncidentType.OptionsColumn.ReadOnly = true;
            this.colIncidentType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentType.Visible = true;
            this.colIncidentType.VisibleIndex = 3;
            this.colIncidentType.Width = 97;
            // 
            // colIncidentReference
            // 
            this.colIncidentReference.FieldName = "IncidentReference";
            this.colIncidentReference.Name = "colIncidentReference";
            this.colIncidentReference.OptionsColumn.AllowEdit = false;
            this.colIncidentReference.OptionsColumn.AllowFocus = false;
            this.colIncidentReference.OptionsColumn.ReadOnly = true;
            this.colIncidentReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIncidentReference.Visible = true;
            this.colIncidentReference.VisibleIndex = 1;
            this.colIncidentReference.Width = 104;
            // 
            // colDateHappened
            // 
            this.colDateHappened.FieldName = "DateHappened";
            this.colDateHappened.Name = "colDateHappened";
            this.colDateHappened.OptionsColumn.AllowEdit = false;
            this.colDateHappened.OptionsColumn.AllowFocus = false;
            this.colDateHappened.OptionsColumn.ReadOnly = true;
            this.colDateHappened.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateHappened.Visible = true;
            this.colDateHappened.VisibleIndex = 4;
            this.colDateHappened.Width = 87;
            // 
            // colLocation
            // 
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 5;
            this.colLocation.Width = 233;
            // 
            // colRepairDueID
            // 
            this.colRepairDueID.FieldName = "RepairDueID";
            this.colRepairDueID.Name = "colRepairDueID";
            this.colRepairDueID.OptionsColumn.AllowEdit = false;
            this.colRepairDueID.OptionsColumn.AllowFocus = false;
            this.colRepairDueID.OptionsColumn.ReadOnly = true;
            this.colRepairDueID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRepairDueID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRepairDueID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRepairDueID.Width = 79;
            // 
            // colRepairDue
            // 
            this.colRepairDue.FieldName = "RepairDue";
            this.colRepairDue.Name = "colRepairDue";
            this.colRepairDue.OptionsColumn.AllowEdit = false;
            this.colRepairDue.OptionsColumn.AllowFocus = false;
            this.colRepairDue.OptionsColumn.ReadOnly = true;
            this.colRepairDue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRepairDue.Visible = true;
            this.colRepairDue.VisibleIndex = 6;
            this.colRepairDue.Width = 91;
            // 
            // colSeverityID
            // 
            this.colSeverityID.FieldName = "SeverityID";
            this.colSeverityID.Name = "colSeverityID";
            this.colSeverityID.OptionsColumn.AllowEdit = false;
            this.colSeverityID.OptionsColumn.AllowFocus = false;
            this.colSeverityID.OptionsColumn.ReadOnly = true;
            this.colSeverityID.OptionsColumn.ShowInCustomizationForm = false;
            this.colSeverityID.OptionsColumn.ShowInExpressionEditor = false;
            this.colSeverityID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSeverity
            // 
            this.colSeverity.FieldName = "Severity";
            this.colSeverity.Name = "colSeverity";
            this.colSeverity.OptionsColumn.AllowEdit = false;
            this.colSeverity.OptionsColumn.AllowFocus = false;
            this.colSeverity.OptionsColumn.ReadOnly = true;
            this.colSeverity.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSeverity.Visible = true;
            this.colSeverity.VisibleIndex = 7;
            this.colSeverity.Width = 92;
            // 
            // colWitness
            // 
            this.colWitness.FieldName = "Witness";
            this.colWitness.Name = "colWitness";
            this.colWitness.OptionsColumn.AllowEdit = false;
            this.colWitness.OptionsColumn.AllowFocus = false;
            this.colWitness.OptionsColumn.ReadOnly = true;
            this.colWitness.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWitness.Visible = true;
            this.colWitness.VisibleIndex = 8;
            this.colWitness.Width = 248;
            // 
            // colKeeperAtFaultID
            // 
            this.colKeeperAtFaultID.FieldName = "KeeperAtFaultID";
            this.colKeeperAtFaultID.Name = "colKeeperAtFaultID";
            this.colKeeperAtFaultID.OptionsColumn.AllowEdit = false;
            this.colKeeperAtFaultID.OptionsColumn.AllowFocus = false;
            this.colKeeperAtFaultID.OptionsColumn.ReadOnly = true;
            this.colKeeperAtFaultID.OptionsColumn.ShowInCustomizationForm = false;
            this.colKeeperAtFaultID.OptionsColumn.ShowInExpressionEditor = false;
            this.colKeeperAtFaultID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAtFaultID.Width = 101;
            // 
            // colKeeperAtFault
            // 
            this.colKeeperAtFault.FieldName = "KeeperAtFault";
            this.colKeeperAtFault.Name = "colKeeperAtFault";
            this.colKeeperAtFault.OptionsColumn.AllowEdit = false;
            this.colKeeperAtFault.OptionsColumn.AllowFocus = false;
            this.colKeeperAtFault.OptionsColumn.ReadOnly = true;
            this.colKeeperAtFault.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colKeeperAtFault.Visible = true;
            this.colKeeperAtFault.VisibleIndex = 9;
            this.colKeeperAtFault.Width = 93;
            // 
            // colLegalActionID
            // 
            this.colLegalActionID.FieldName = "LegalActionID";
            this.colLegalActionID.Name = "colLegalActionID";
            this.colLegalActionID.OptionsColumn.AllowEdit = false;
            this.colLegalActionID.OptionsColumn.AllowFocus = false;
            this.colLegalActionID.OptionsColumn.ReadOnly = true;
            this.colLegalActionID.OptionsColumn.ShowInCustomizationForm = false;
            this.colLegalActionID.OptionsColumn.ShowInExpressionEditor = false;
            this.colLegalActionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLegalActionID.Width = 84;
            // 
            // colLegalAction
            // 
            this.colLegalAction.FieldName = "LegalAction";
            this.colLegalAction.Name = "colLegalAction";
            this.colLegalAction.OptionsColumn.AllowEdit = false;
            this.colLegalAction.OptionsColumn.AllowFocus = false;
            this.colLegalAction.OptionsColumn.ReadOnly = true;
            this.colLegalAction.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLegalAction.Visible = true;
            this.colLegalAction.VisibleIndex = 10;
            this.colLegalAction.Width = 119;
            // 
            // colFollowUpDate
            // 
            this.colFollowUpDate.FieldName = "FollowUpDate";
            this.colFollowUpDate.Name = "colFollowUpDate";
            this.colFollowUpDate.OptionsColumn.AllowEdit = false;
            this.colFollowUpDate.OptionsColumn.AllowFocus = false;
            this.colFollowUpDate.OptionsColumn.ReadOnly = true;
            this.colFollowUpDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFollowUpDate.Visible = true;
            this.colFollowUpDate.VisibleIndex = 11;
            this.colFollowUpDate.Width = 99;
            // 
            // colCost
            // 
            this.colCost.FieldName = "Cost";
            this.colCost.Name = "colCost";
            this.colCost.OptionsColumn.AllowEdit = false;
            this.colCost.OptionsColumn.AllowFocus = false;
            this.colCost.OptionsColumn.ReadOnly = true;
            this.colCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCost.Visible = true;
            this.colCost.VisibleIndex = 12;
            this.colCost.Width = 84;
            // 
            // colNotes4
            // 
            this.colNotes4.ColumnEdit = this.incidentNotesMemoEdit;
            this.colNotes4.FieldName = "Notes";
            this.colNotes4.Name = "colNotes4";
            this.colNotes4.OptionsColumn.AllowEdit = false;
            this.colNotes4.OptionsColumn.AllowFocus = false;
            this.colNotes4.OptionsColumn.ReadOnly = true;
            this.colNotes4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotes4.Visible = true;
            this.colNotes4.VisibleIndex = 13;
            this.colNotes4.Width = 472;
            // 
            // incidentNotesMemoEdit
            // 
            this.incidentNotesMemoEdit.Name = "incidentNotesMemoEdit";
            // 
            // gridColumn3
            // 
            this.gridColumn3.FieldName = "Mode";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn3.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn4
            // 
            this.gridColumn4.FieldName = "RecordID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsColumn.ShowInCustomizationForm = false;
            this.gridColumn4.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // purposeTabPage
            // 
            this.purposeTabPage.Controls.Add(this.purposeGridControl);
            this.purposeTabPage.Name = "purposeTabPage";
            this.purposeTabPage.Size = new System.Drawing.Size(1357, 150);
            this.purposeTabPage.Text = "Asset Purpose";
            // 
            // purposeGridControl
            // 
            this.purposeGridControl.DataSource = this.spAS11084PurposeItemBindingSource;
            this.purposeGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.purposeGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.purposeGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.purposeGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.commonGridControl_EmbeddedNavigator_ButtonClick);
            this.purposeGridControl.Location = new System.Drawing.Point(0, 0);
            this.purposeGridControl.MainView = this.purposeGridView;
            this.purposeGridControl.MenuManager = this.barManager1;
            this.purposeGridControl.Name = "purposeGridControl";
            this.purposeGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit16,
            this.repositoryItemMemoExEdit17});
            this.purposeGridControl.Size = new System.Drawing.Size(1357, 150);
            this.purposeGridControl.TabIndex = 3;
            this.purposeGridControl.UseEmbeddedNavigator = true;
            this.purposeGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.purposeGridView});
            // 
            // spAS11084PurposeItemBindingSource
            // 
            this.spAS11084PurposeItemBindingSource.DataMember = "sp_AS_11084_Purpose_Item";
            this.spAS11084PurposeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // purposeGridView
            // 
            this.purposeGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEquipmentPurposeID,
            this.colEquipmentID15,
            this.colEquipmentReference13,
            this.colPurpose,
            this.colPurposeID,
            this.colMode14,
            this.colRecordID14});
            this.purposeGridView.GridControl = this.purposeGridControl;
            this.purposeGridView.Name = "purposeGridView";
            this.purposeGridView.OptionsCustomization.AllowFilter = false;
            this.purposeGridView.OptionsCustomization.AllowGroup = false;
            this.purposeGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.purposeGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.purposeGridView.OptionsLayout.StoreAppearance = true;
            this.purposeGridView.OptionsSelection.MultiSelect = true;
            this.purposeGridView.OptionsView.ColumnAutoWidth = false;
            this.purposeGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.purposeGridView.OptionsView.ShowGroupPanel = false;
            this.purposeGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.equipmentView_PopupMenuShowing);
            this.purposeGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.childGridView_SelectionChanged);
            this.purposeGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.purposeGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.commonView_CustomFilterDialog);
            this.purposeGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.commonGridView_MouseUp);
            this.purposeGridView.DoubleClick += new System.EventHandler(this.commonGridView_DoubleClick);
            this.purposeGridView.GotFocus += new System.EventHandler(this.commonGridView_GotFocus);
            // 
            // colEquipmentPurposeID
            // 
            this.colEquipmentPurposeID.FieldName = "EquipmentPurposeID";
            this.colEquipmentPurposeID.Name = "colEquipmentPurposeID";
            this.colEquipmentPurposeID.OptionsColumn.AllowEdit = false;
            this.colEquipmentPurposeID.OptionsColumn.AllowFocus = false;
            this.colEquipmentPurposeID.OptionsColumn.ReadOnly = true;
            this.colEquipmentPurposeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentPurposeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentPurposeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentPurposeID.Width = 118;
            // 
            // colEquipmentID15
            // 
            this.colEquipmentID15.FieldName = "EquipmentID";
            this.colEquipmentID15.Name = "colEquipmentID15";
            this.colEquipmentID15.OptionsColumn.AllowEdit = false;
            this.colEquipmentID15.OptionsColumn.AllowFocus = false;
            this.colEquipmentID15.OptionsColumn.ReadOnly = true;
            this.colEquipmentID15.OptionsColumn.ShowInCustomizationForm = false;
            this.colEquipmentID15.OptionsColumn.ShowInExpressionEditor = false;
            this.colEquipmentID15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentID15.Width = 76;
            // 
            // colEquipmentReference13
            // 
            this.colEquipmentReference13.FieldName = "EquipmentReference";
            this.colEquipmentReference13.Name = "colEquipmentReference13";
            this.colEquipmentReference13.OptionsColumn.AllowEdit = false;
            this.colEquipmentReference13.OptionsColumn.AllowFocus = false;
            this.colEquipmentReference13.OptionsColumn.ReadOnly = true;
            this.colEquipmentReference13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEquipmentReference13.Visible = true;
            this.colEquipmentReference13.VisibleIndex = 0;
            this.colEquipmentReference13.Width = 115;
            // 
            // colPurpose
            // 
            this.colPurpose.FieldName = "Purpose";
            this.colPurpose.Name = "colPurpose";
            this.colPurpose.OptionsColumn.AllowEdit = false;
            this.colPurpose.OptionsColumn.AllowFocus = false;
            this.colPurpose.OptionsColumn.ReadOnly = true;
            this.colPurpose.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPurpose.Visible = true;
            this.colPurpose.VisibleIndex = 1;
            this.colPurpose.Width = 573;
            // 
            // colPurposeID
            // 
            this.colPurposeID.FieldName = "PurposeID";
            this.colPurposeID.Name = "colPurposeID";
            this.colPurposeID.OptionsColumn.AllowEdit = false;
            this.colPurposeID.OptionsColumn.AllowFocus = false;
            this.colPurposeID.OptionsColumn.ReadOnly = true;
            this.colPurposeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colPurposeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colPurposeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colMode14
            // 
            this.colMode14.FieldName = "Mode";
            this.colMode14.Name = "colMode14";
            this.colMode14.OptionsColumn.AllowEdit = false;
            this.colMode14.OptionsColumn.AllowFocus = false;
            this.colMode14.OptionsColumn.ReadOnly = true;
            this.colMode14.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode14.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID14
            // 
            this.colRecordID14.FieldName = "RecordID";
            this.colRecordID14.Name = "colRecordID14";
            this.colRecordID14.OptionsColumn.AllowEdit = false;
            this.colRecordID14.OptionsColumn.AllowFocus = false;
            this.colRecordID14.OptionsColumn.ReadOnly = true;
            this.colRecordID14.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID14.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit16
            // 
            this.repositoryItemMemoExEdit16.AutoHeight = false;
            this.repositoryItemMemoExEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit16.Name = "repositoryItemMemoExEdit16";
            this.repositoryItemMemoExEdit16.ReadOnly = true;
            this.repositoryItemMemoExEdit16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit16.ShowIcon = false;
            // 
            // repositoryItemMemoExEdit17
            // 
            this.repositoryItemMemoExEdit17.AutoHeight = false;
            this.repositoryItemMemoExEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit17.Name = "repositoryItemMemoExEdit17";
            this.repositoryItemMemoExEdit17.ReadOnly = true;
            this.repositoryItemMemoExEdit17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit17.ShowIcon = false;
            // 
            // LinkedDocumentsTabPage
            // 
            this.LinkedDocumentsTabPage.Controls.Add(this.gridControlLinkedDocs);
            this.LinkedDocumentsTabPage.Name = "LinkedDocumentsTabPage";
            this.LinkedDocumentsTabPage.Size = new System.Drawing.Size(1357, 150);
            this.LinkedDocumentsTabPage.Text = "Linked Documents";
            // 
            // gridControlLinkedDocs
            // 
            this.gridControlLinkedDocs.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControlLinkedDocs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlLinkedDocs.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControlLinkedDocs.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlLinkedDocs_EmbeddedNavigator_ButtonClick);
            this.gridControlLinkedDocs.Location = new System.Drawing.Point(0, 0);
            this.gridControlLinkedDocs.MainView = this.gridViewLinkedDocs;
            this.gridControlLinkedDocs.MenuManager = this.barManager1;
            this.gridControlLinkedDocs.Name = "gridControlLinkedDocs";
            this.gridControlLinkedDocs.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEditLinkedDocs,
            this.repositoryItemHyperLinkEditLinkedDocs,
            this.repositoryItemTextEditLinkedDocDate});
            this.gridControlLinkedDocs.Size = new System.Drawing.Size(1357, 150);
            this.gridControlLinkedDocs.TabIndex = 2;
            this.gridControlLinkedDocs.UseEmbeddedNavigator = true;
            this.gridControlLinkedDocs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLinkedDocs});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewLinkedDocs
            // 
            this.gridViewLinkedDocs.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.gridColumn13,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.gridColumn14,
            this.colAddedByStaffID,
            this.gridColumn15,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks,
            this.colDocumentType});
            this.gridViewLinkedDocs.GridControl = this.gridControlLinkedDocs;
            this.gridViewLinkedDocs.GroupCount = 1;
            this.gridViewLinkedDocs.Name = "gridViewLinkedDocs";
            this.gridViewLinkedDocs.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewLinkedDocs.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewLinkedDocs.OptionsLayout.StoreAppearance = true;
            this.gridViewLinkedDocs.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewLinkedDocs.OptionsSelection.MultiSelect = true;
            this.gridViewLinkedDocs.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewLinkedDocs.OptionsView.ColumnAutoWidth = false;
            this.gridViewLinkedDocs.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewLinkedDocs.OptionsView.ShowGroupPanel = false;
            this.gridViewLinkedDocs.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn14, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewLinkedDocs.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridViewLinkedDocs_PopupMenuShowing);
            this.gridViewLinkedDocs.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridViewLinkedDocs_SelectionChanged);
            this.gridViewLinkedDocs.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.childGridView_CustomDrawEmptyForeground);
            this.gridViewLinkedDocs.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridViewLinkedDocs_CustomFilterDialog);
            this.gridViewLinkedDocs.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridViewLinkedDocs_FilterEditorCreated);
            this.gridViewLinkedDocs.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewLinkedDocs_MouseUp);
            this.gridViewLinkedDocs.DoubleClick += new System.EventHandler(this.gridViewLinkedDocs_DoubleClick);
            this.gridViewLinkedDocs.GotFocus += new System.EventHandler(this.gridViewLinkedDocs_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Linked Record ID";
            this.gridColumn13.FieldName = "LinkedToRecordID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocs;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 3;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEditLinkedDocs
            // 
            this.repositoryItemHyperLinkEditLinkedDocs.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocs.Name = "repositoryItemHyperLinkEditLinkedDocs";
            this.repositoryItemHyperLinkEditLinkedDocs.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocs.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocs_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "File Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 4;
            this.colDocumentExtension.Width = 84;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Description";
            this.gridColumn14.FieldName = "Description";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            this.gridColumn14.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Date Added";
            this.gridColumn15.ColumnEdit = this.repositoryItemTextEditLinkedDocDate;
            this.gridColumn15.FieldName = "DateAdded";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 91;
            // 
            // repositoryItemTextEditLinkedDocDate
            // 
            this.repositoryItemTextEditLinkedDocDate.AutoHeight = false;
            this.repositoryItemTextEditLinkedDocDate.Mask.EditMask = "g";
            this.repositoryItemTextEditLinkedDocDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditLinkedDocDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLinkedDocDate.Name = "repositoryItemTextEditLinkedDocDate";
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 5;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEditLinkedDocs;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 6;
            // 
            // repositoryItemMemoExEditLinkedDocs
            // 
            this.repositoryItemMemoExEditLinkedDocs.AutoHeight = false;
            this.repositoryItemMemoExEditLinkedDocs.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEditLinkedDocs.Name = "repositoryItemMemoExEditLinkedDocs";
            this.repositoryItemMemoExEditLinkedDocs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEditLinkedDocs.ShowIcon = false;
            // 
            // colDocumentType
            // 
            this.colDocumentType.FieldName = "DocumentType";
            this.colDocumentType.Name = "colDocumentType";
            this.colDocumentType.OptionsColumn.AllowEdit = false;
            this.colDocumentType.OptionsColumn.AllowFocus = false;
            this.colDocumentType.OptionsColumn.ReadOnly = true;
            this.colDocumentType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDocumentType.Visible = true;
            this.colDocumentType.VisibleIndex = 2;
            this.colDocumentType.Width = 159;
            // 
            // spAS11129SoftwareAssignItemBindingSource
            // 
            this.spAS11129SoftwareAssignItemBindingSource.DataMember = "sp_AS_11129_Software_Assign_Item";
            this.spAS11129SoftwareAssignItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11054ServiceIntervalsItemBindingSource
            // 
            this.spAS11054ServiceIntervalsItemBindingSource.DataMember = "sp_AS_11054_Service_Intervals_Item";
            this.spAS11054ServiceIntervalsItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // spAS11047EquipmentBillingItemBindingSource
            // 
            this.spAS11047EquipmentBillingItemBindingSource.DataMember = "sp_AS_11047_Equipment_Billing_Item";
            this.spAS11047EquipmentBillingItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp09001_Employee_ManagerTableAdapter
            // 
            this.sp09001_Employee_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_CoreTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.Connection = null;
            this.tableAdapterManager1.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // spAS11002EquipmentItemBindingSource
            // 
            this.spAS11002EquipmentItemBindingSource.DataMember = "sp_AS_11002_Equipment_Item";
            this.spAS11002EquipmentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // sp_AS_11002_Equipment_ItemTableAdapter
            // 
            this.sp_AS_11002_Equipment_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11008_Plant_ItemTableAdapter
            // 
            this.sp_AS_11008_Plant_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11026_Gadget_ItemTableAdapter
            // 
            this.sp_AS_11026_Gadget_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11032_Hardware_ItemTableAdapter
            // 
            this.sp_AS_11032_Hardware_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11035_Software_ItemTableAdapter
            // 
            this.sp_AS_11035_Software_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keeper_Allocation_ItemTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11041_Transaction_ItemTableAdapter
            // 
            this.sp_AS_11041_Transaction_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11047_Equipment_Billing_ItemTableAdapter
            // 
            this.sp_AS_11047_Equipment_Billing_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11050_Depreciation_ItemTableAdapter
            // 
            this.sp_AS_11050_Depreciation_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11078_Work_Detail_ItemTableAdapter
            // 
            this.sp_AS_11078_Work_Detail_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11084_Purpose_ItemTableAdapter
            // 
            this.sp_AS_11084_Purpose_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11044_Keeper_Allocation_ListTableAdapter
            // 
            this.sp_AS_11044_Keeper_Allocation_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11029_Office_ItemTableAdapter
            // 
            this.sp_AS_11029_Office_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11081_Incident_ItemTableAdapter
            // 
            this.sp_AS_11081_Incident_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11005_Vehicle_ItemTableAdapter
            // 
            this.sp_AS_11005_Vehicle_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11126_Notification_ItemTableAdapter
            // 
            this.sp_AS_11126_Notification_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11129_Software_Assign_ItemTableAdapter
            // 
            this.sp_AS_11129_Software_Assign_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11092_Office_ManagerTableAdapter
            // 
            this.sp_AS_11092_Office_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11054_Service_Intervals_ItemTableAdapter
            // 
            this.sp_AS_11054_Service_Intervals_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEmail)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.Text = "Custom 1";
            // 
            // bbiEmail
            // 
            this.bbiEmail.Caption = "Email Keepers";
            this.bbiEmail.Id = 29;
            this.bbiEmail.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Send_To_Employee_32x32;
            this.bbiEmail.Name = "bbiEmail";
            this.bbiEmail.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiEmail.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEmail_ItemClick);
            // 
            // frm_AS_Office_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1362, 562);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Office_Manager";
            this.Text = "Office Manager";
            this.Activated += new System.EventHandler(this.frm_AS_Office_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_AS_Office_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.equipmentGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11092OfficeManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceCheckEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.equipmentChildTabControl)).EndInit();
            this.equipmentChildTabControl.ResumeLayout(false);
            this.vehicleTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vehicleGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11005VehicleItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            this.NoDetailsTabPage.ResumeLayout(false);
            this.NoDetailsTabPage.PerformLayout();
            this.plantTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.plantGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11008PlantItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plantGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            this.gadgetTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gadgetGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11026GadgetItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gadgetGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            this.hardwareTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.hardwareGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11032HardwareItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hardwareGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).EndInit();
            this.softwareTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.softwareGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11035SoftwareItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.softwareGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit5)).EndInit();
            this.officeTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.officeGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11029OfficeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).EndInit();
            this.keeperTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11044KeeperAllocationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.keeperGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperNotes)).EndInit();
            this.transactionTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.transactionGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11041TransactionItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceOnExchequer)).EndInit();
            this.billingTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.billingGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11050DepreciationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            this.notificationTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11126NotificationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).EndInit();
            this.depreciationTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.depreciationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEditDep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            this.workDetailTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11078WorkDetailItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workDetailGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).EndInit();
            this.incidentTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.incidentNotesMemoEdit)).EndInit();
            this.purposeTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11084PurposeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purposeGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).EndInit();
            this.LinkedDocumentsTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLinkedDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLinkedDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLinkedDocDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditLinkedDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11129SoftwareAssignItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11054ServiceIntervalsItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11047EquipmentBillingItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11002EquipmentItemBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl equipmentChildTabControl;
        private DevExpress.XtraTab.XtraTabPage vehicleTabPage;
        private DevExpress.XtraTab.XtraTabPage plantTabPage;
        private DevExpress.XtraGrid.GridControl vehicleGridControl;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.GridControl plantGridControl;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_HR_CoreTableAdapters.sp09001_Employee_ManagerTableAdapter sp09001_Employee_ManagerTableAdapter;
        private DataSet_AS_CoreTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraTab.XtraTabPage NoDetailsTabPage;
        private System.Windows.Forms.Label label1;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraGrid.Views.Grid.GridView vehicleGridView;
        private DevExpress.XtraGrid.Views.Grid.GridView plantGridView;
        private DevExpress.XtraTab.XtraTabPage gadgetTabPage;
        private DevExpress.XtraTab.XtraTabPage hardwareTabPage;
        private DevExpress.XtraTab.XtraTabPage softwareTabPage;
        private DevExpress.XtraTab.XtraTabPage officeTabPage;
        private DevExpress.XtraGrid.GridControl gadgetGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gadgetGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.GridControl hardwareGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView hardwareGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit4;
        private DevExpress.XtraGrid.GridControl softwareGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView softwareGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit5;
        private DevExpress.XtraGrid.GridControl officeGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView officeGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit6;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DataSet_AS_Core dataSet_AS_Core;
        private DevExpress.XtraGrid.GridControl equipmentGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView equipmentGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceCheckEdit;
        private DevExpress.XtraTab.XtraTabPage keeperTabPage;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager1;
        private DevExpress.XtraGrid.GridControl keeperGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView keeperGridView;
        private DevExpress.XtraTab.XtraTabPage transactionTabPage;
        private DevExpress.XtraGrid.GridControl transactionGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView transactionsGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceOnExchequer;
        private DevExpress.XtraTab.XtraTabPage billingTabPage;
        private DevExpress.XtraGrid.GridControl billingGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView billingGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit lueKeeperNotes;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraTab.XtraTabPage depreciationTabPage;
        private DevExpress.XtraGrid.GridControl depreciationGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView depreciationGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEditDep;
        private DevExpress.XtraTab.XtraTabPage workDetailTabPage;
        private DevExpress.XtraGrid.GridControl workDetailGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView workDetailGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit13;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit14;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colNarrative;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceSheetCode;
        private DevExpress.XtraGrid.Columns.GridColumn colProfitLossCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colPreviousValue;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID5;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference3;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationPlate;
        private DevExpress.XtraGrid.Columns.GridColumn colLogBookNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colModelSpecifications;
        private DevExpress.XtraGrid.Columns.GridColumn colEngineSize;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelType;
        private DevExpress.XtraGrid.Columns.GridColumn colEmissions;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTyreNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTowbarTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTowbarType;
        private DevExpress.XtraGrid.Columns.GridColumn colColour;
        private DevExpress.XtraGrid.Columns.GridColumn colRegistrationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMOTDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentMileage1;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceDueMileage;
        private DevExpress.XtraGrid.Columns.GridColumn colRadioCode;
        private DevExpress.XtraGrid.Columns.GridColumn colKeyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colElectronicCode;
        private DevExpress.XtraGrid.Columns.GridColumn colMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID1;
        private System.Windows.Forms.BindingSource spAS11008PlantItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID6;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference4;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colPlantCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colEngineSize1;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelType1;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID2;
        private System.Windows.Forms.BindingSource spAS11026GadgetItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID7;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference5;
        private DevExpress.XtraGrid.Columns.GridColumn colGadgetTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colGadgetType;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colCasing_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colCasing;
        private DevExpress.XtraGrid.Columns.GridColumn colMode3;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID3;
        private System.Windows.Forms.BindingSource spAS11032HardwareItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID8;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference6;
        private DevExpress.XtraGrid.Columns.GridColumn colHardwareTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colHardwareType;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecificationID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecification;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colMode4;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID4;
        private System.Windows.Forms.BindingSource spAS11035SoftwareItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID9;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference7;
        private DevExpress.XtraGrid.Columns.GridColumn colProduct;
        private DevExpress.XtraGrid.Columns.GridColumn colProductID;
        private DevExpress.XtraGrid.Columns.GridColumn colVersion;
        private DevExpress.XtraGrid.Columns.GridColumn colVersionID;
        private DevExpress.XtraGrid.Columns.GridColumn colYear;
        private DevExpress.XtraGrid.Columns.GridColumn colLicence;
        private DevExpress.XtraGrid.Columns.GridColumn colParentProgram;
        private DevExpress.XtraGrid.Columns.GridColumn colAcquisationMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colAcquisationMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantityLimit;
        private DevExpress.XtraGrid.Columns.GridColumn colLicenceKey;
        private DevExpress.XtraGrid.Columns.GridColumn colLicenceType;
        private DevExpress.XtraGrid.Columns.GridColumn colLicenceTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colValidUntil;
        private DevExpress.XtraGrid.Columns.GridColumn colMode5;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID5;
        private System.Windows.Forms.BindingSource spAS11002EquipmentItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11002_Equipment_ItemTableAdapter sp_AS_11002_Equipment_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11008_Plant_ItemTableAdapter sp_AS_11008_Plant_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11026_Gadget_ItemTableAdapter sp_AS_11026_Gadget_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11032_Hardware_ItemTableAdapter sp_AS_11032_Hardware_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11035_Software_ItemTableAdapter sp_AS_11035_Software_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11044KeeperAllocationItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAllocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference9;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID11;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode7;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID7;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ItemTableAdapter sp_AS_11044_Keeper_Allocation_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11041TransactionItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference10;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID12;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionType;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchaseInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedOnExchequer;
        private DevExpress.XtraGrid.Columns.GridColumn colNetTransactionPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colMode8;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID8;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11041_Transaction_ItemTableAdapter sp_AS_11041_Transaction_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11047EquipmentBillingItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11050DepreciationItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11047_Equipment_Billing_ItemTableAdapter sp_AS_11047_Equipment_Billing_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11050_Depreciation_ItemTableAdapter sp_AS_11050_Depreciation_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11078WorkDetailItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkDetailID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID2;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference11;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkType;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierID;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierReference;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colTransactionID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentMileage;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkCompletionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceIntervalID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceType;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes2;
        private DevExpress.XtraGrid.Columns.GridColumn colMode10;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID10;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11078_Work_Detail_ItemTableAdapter sp_AS_11078_Work_Detail_ItemTableAdapter;
        private DevExpress.XtraTab.XtraTabPage incidentTabPage;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colNarrative1;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceSheetCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colProfitLossCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationAmount1;
        private DevExpress.XtraGrid.Columns.GridColumn colPreviousValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colPeriodEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode9;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID9;
        private DevExpress.XtraTab.XtraTabPage purposeTabPage;
        private DevExpress.XtraGrid.GridControl purposeGridControl;
        private System.Windows.Forms.BindingSource spAS11084PurposeItemBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView purposeGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentPurposeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID15;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference13;
        private DevExpress.XtraGrid.Columns.GridColumn colPurpose;
        private DevExpress.XtraGrid.Columns.GridColumn colPurposeID;
        private DevExpress.XtraGrid.Columns.GridColumn colMode14;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit16;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit17;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11084_Purpose_ItemTableAdapter sp_AS_11084_Purpose_ItemTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11044_Keeper_Allocation_ListTableAdapter sp_AS_11044_Keeper_Allocation_ListTableAdapter;
        private System.Windows.Forms.BindingSource spAS11029OfficeItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID10;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference8;
        private DevExpress.XtraGrid.Columns.GridColumn colOfficeCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colOfficeCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colMode6;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID6;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11029_Office_ItemTableAdapter sp_AS_11029_Office_ItemTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit4;
        private DevExpress.XtraGrid.GridControl incidentGridControl;
        private System.Windows.Forms.BindingSource spAS11081IncidentItemBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView incidentGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentType;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colDateHappened;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colRepairDueID;
        private DevExpress.XtraGrid.Columns.GridColumn colRepairDue;
        private DevExpress.XtraGrid.Columns.GridColumn colSeverityID;
        private DevExpress.XtraGrid.Columns.GridColumn colSeverity;
        private DevExpress.XtraGrid.Columns.GridColumn colWitness;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAtFaultID;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperAtFault;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalAction;
        private DevExpress.XtraGrid.Columns.GridColumn colFollowUpDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCost;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit incidentNotesMemoEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter sp_AS_11081_Incident_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11005VehicleItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11005_Vehicle_ItemTableAdapter sp_AS_11005_Vehicle_ItemTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colShortDescription;
        private DevExpress.XtraTab.XtraTabPage notificationTabPage;
        private DevExpress.XtraGrid.GridControl notificationGridControl;
        private System.Windows.Forms.BindingSource spAS11126NotificationItemBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView notificationGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationID;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference12;
        private DevExpress.XtraGrid.Columns.GridColumn colPriorityID;
        private DevExpress.XtraGrid.Columns.GridColumn colPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colDateToRemind;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperType1;
        private DevExpress.XtraGrid.Columns.GridColumn colKeeperID1;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraGrid.Columns.GridColumn colMessage;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colMode13;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID13;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit15;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11126_Notification_ItemTableAdapter sp_AS_11126_Notification_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11129SoftwareAssignItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11129_Software_Assign_ItemTableAdapter sp_AS_11129_Software_Assign_ItemTableAdapter;
        private System.Windows.Forms.BindingSource spAS11092OfficeManagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID4;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentReference2;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colUniqueItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMake;
        private DevExpress.XtraGrid.Columns.GridColumn colModel;
        private DevExpress.XtraGrid.Columns.GridColumn colAvailability;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplier;
        private DevExpress.XtraGrid.Columns.GridColumn colGCMarked;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExchequerReference;
        private DevExpress.XtraGrid.Columns.GridColumn colSageAssetReference;
        private DevExpress.XtraGrid.Columns.GridColumn colArchive;
        private DevExpress.XtraGrid.Columns.GridColumn colArchiveDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOccurrenceFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colOccursUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumOccurrence;
        private DevExpress.XtraGrid.Columns.GridColumn colDayOfMonthApplyDepreciation;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOfficeCategoryID1;
        private DevExpress.XtraGrid.Columns.GridColumn colOfficeCategory1;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationApplied;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentValue2;
        private DevExpress.XtraGrid.Columns.GridColumn colDepreciationPeriodNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colMode17;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID17;
        private DataSet_AS_CoreTableAdapters.sp_AS_11092_Office_ManagerTableAdapter sp_AS_11092_Office_ManagerTableAdapter;
        private System.Windows.Forms.BindingSource spAS11054ServiceIntervalsItemBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11054_Service_Intervals_ItemTableAdapter sp_AS_11054_Service_Intervals_ItemTableAdapter;
        private DevExpress.XtraTab.XtraTabPage LinkedDocumentsTabPage;
        private DevExpress.XtraGrid.GridControl gridControlLinkedDocs;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLinkedDocs;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocs;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLinkedDocDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEditLinkedDocs;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentType;
        private DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLastKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveKeeper;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentBilling;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentDepartment;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiEmail;
    }
}

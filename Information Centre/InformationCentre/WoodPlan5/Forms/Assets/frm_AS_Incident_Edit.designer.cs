﻿namespace WoodPlan5
{
    partial class frm_AS_Incident_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AS_Incident_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spAS11081IncidentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtEquipmentReference = new DevExpress.XtraEditors.TextEdit();
            this.lueIncidentStatusID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLIncidentStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueIncidentTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLIncidentTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtIncidentReference = new DevExpress.XtraEditors.TextEdit();
            this.deDateHappened = new DevExpress.XtraEditors.DateEdit();
            this.txtLocation = new DevExpress.XtraEditors.TextEdit();
            this.lueRepairDueID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLIncidentRepairDueBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueSeverityID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLSeverityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtWitness = new DevExpress.XtraEditors.TextEdit();
            this.lueKeeperAtFaultID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLKeeperAtFaultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueLegalActionID = new DevExpress.XtraEditors.LookUpEdit();
            this.spAS11000PLLegalActionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.deFollowUpDate = new DevExpress.XtraEditors.DateEdit();
            this.spnCost = new DevExpress.XtraEditors.SpinEdit();
            this.meNotes = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForIncidentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForNotes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipmentReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIncidentTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSeverityID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIncidentStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLocation = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLegalActionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFollowUpDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWitness = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateHappened = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForKeeperAtFaultID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRepairDueID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp_AS_11000_PL_Incident_StatusTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_StatusTableAdapter();
            this.sp_AS_11000_PL_Incident_TypeTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_TypeTableAdapter();
            this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_Repair_DueTableAdapter();
            this.sp_AS_11081_Incident_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter();
            this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_At_FaultTableAdapter();
            this.sp_AS_11000_PL_SeverityTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_SeverityTableAdapter();
            this.sp_AS_11000_PL_Legal_ActionTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Legal_ActionTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueIncidentStatusID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLIncidentStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueIncidentTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLIncidentTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIncidentReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateHappened.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateHappened.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRepairDueID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLIncidentRepairDueBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSeverityID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSeverityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWitness.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperAtFaultID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperAtFaultBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLegalActionID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLLegalActionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFollowUpDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFollowUpDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSeverityID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLegalActionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFollowUpDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWitness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateHappened)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperAtFaultID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRepairDueID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1070, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 586);
            this.barDockControlBottom.Size = new System.Drawing.Size(1070, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 560);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1070, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 560);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1070, 560);
            this.editFormLayoutControlGroup.Text = "Root";
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(132, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(314, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.spAS11081IncidentItemBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(144, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(310, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.editFormDataNavigator.TextStringFormat = "Incident Record {0} of {1}";
            // 
            // spAS11081IncidentItemBindingSource
            // 
            this.spAS11081IncidentItemBindingSource.DataMember = "sp_AS_11081_Incident_Item";
            this.spAS11081IncidentItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtEquipmentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueIncidentStatusID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueIncidentTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtIncidentReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deDateHappened);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtLocation);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueRepairDueID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueSeverityID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtWitness);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueKeeperAtFaultID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueLegalActionID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deFollowUpDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnCost);
            this.editFormDataLayoutControlGroup.Controls.Add(this.meNotes);
            this.editFormDataLayoutControlGroup.DataSource = this.spAS11081IncidentItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(2216, 252, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1070, 560);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtEquipmentReference
            // 
            this.txtEquipmentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "EquipmentReference", true));
            this.txtEquipmentReference.Location = new System.Drawing.Point(137, 68);
            this.txtEquipmentReference.MenuManager = this.barManager1;
            this.txtEquipmentReference.Name = "txtEquipmentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtEquipmentReference, true);
            this.txtEquipmentReference.Size = new System.Drawing.Size(909, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtEquipmentReference, optionsSpelling1);
            this.txtEquipmentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtEquipmentReference.TabIndex = 131;
            // 
            // lueIncidentStatusID
            // 
            this.lueIncidentStatusID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "IncidentStatusID", true));
            this.lueIncidentStatusID.Location = new System.Drawing.Point(137, 116);
            this.lueIncidentStatusID.MenuManager = this.barManager1;
            this.lueIncidentStatusID.Name = "lueIncidentStatusID";
            this.lueIncidentStatusID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueIncidentStatusID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Incident Status", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueIncidentStatusID.Properties.DataSource = this.spAS11000PLIncidentStatusBindingSource;
            this.lueIncidentStatusID.Properties.DisplayMember = "Value";
            this.lueIncidentStatusID.Properties.NullText = "";
            this.lueIncidentStatusID.Properties.NullValuePrompt = "-- Select Current Incident Status --";
            this.lueIncidentStatusID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueIncidentStatusID.Properties.ValueMember = "PickListID";
            this.lueIncidentStatusID.Size = new System.Drawing.Size(396, 20);
            this.lueIncidentStatusID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueIncidentStatusID.TabIndex = 132;
            this.lueIncidentStatusID.Tag = "Incident Status";
            this.lueIncidentStatusID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLIncidentStatusBindingSource
            // 
            this.spAS11000PLIncidentStatusBindingSource.DataMember = "sp_AS_11000_PL_Incident_Status";
            this.spAS11000PLIncidentStatusBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueIncidentTypeID
            // 
            this.lueIncidentTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "IncidentTypeID", true));
            this.lueIncidentTypeID.Location = new System.Drawing.Point(650, 92);
            this.lueIncidentTypeID.MenuManager = this.barManager1;
            this.lueIncidentTypeID.Name = "lueIncidentTypeID";
            this.lueIncidentTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueIncidentTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Incident Type", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueIncidentTypeID.Properties.DataSource = this.spAS11000PLIncidentTypeBindingSource;
            this.lueIncidentTypeID.Properties.DisplayMember = "Value";
            this.lueIncidentTypeID.Properties.NullText = "";
            this.lueIncidentTypeID.Properties.NullValuePrompt = "-- Select Type Of Incident --";
            this.lueIncidentTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueIncidentTypeID.Properties.ValueMember = "PickListID";
            this.lueIncidentTypeID.Size = new System.Drawing.Size(396, 20);
            this.lueIncidentTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueIncidentTypeID.TabIndex = 133;
            this.lueIncidentTypeID.Tag = "Incident Type";
            this.lueIncidentTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLIncidentTypeBindingSource
            // 
            this.spAS11000PLIncidentTypeBindingSource.DataMember = "sp_AS_11000_PL_Incident_Type";
            this.spAS11000PLIncidentTypeBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // txtIncidentReference
            // 
            this.txtIncidentReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "IncidentReference", true));
            this.txtIncidentReference.Location = new System.Drawing.Point(137, 92);
            this.txtIncidentReference.MenuManager = this.barManager1;
            this.txtIncidentReference.Name = "txtIncidentReference";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtIncidentReference, true);
            this.txtIncidentReference.Size = new System.Drawing.Size(396, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtIncidentReference, optionsSpelling2);
            this.txtIncidentReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtIncidentReference.TabIndex = 134;
            this.txtIncidentReference.Tag = "Incident Reference";
            this.txtIncidentReference.EditValueChanged += new System.EventHandler(this.txtIncidentReference_EditValueChanged);
            this.txtIncidentReference.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit_Validating);
            // 
            // deDateHappened
            // 
            this.deDateHappened.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "DateHappened", true));
            this.deDateHappened.EditValue = null;
            this.deDateHappened.Location = new System.Drawing.Point(650, 140);
            this.deDateHappened.MenuManager = this.barManager1;
            this.deDateHappened.Name = "deDateHappened";
            this.deDateHappened.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateHappened.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deDateHappened.Size = new System.Drawing.Size(396, 20);
            this.deDateHappened.StyleController = this.editFormDataLayoutControlGroup;
            this.deDateHappened.TabIndex = 135;
            this.deDateHappened.Tag = "Date Happened";
            this.deDateHappened.Validating += new System.ComponentModel.CancelEventHandler(this.deDateHappened_Validating);
            // 
            // txtLocation
            // 
            this.txtLocation.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "Location", true));
            this.txtLocation.Location = new System.Drawing.Point(137, 140);
            this.txtLocation.MenuManager = this.barManager1;
            this.txtLocation.Name = "txtLocation";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtLocation, true);
            this.txtLocation.Size = new System.Drawing.Size(396, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtLocation, optionsSpelling3);
            this.txtLocation.StyleController = this.editFormDataLayoutControlGroup;
            this.txtLocation.TabIndex = 136;
            this.txtLocation.Tag = "Location";
            this.txtLocation.EditValueChanged += new System.EventHandler(this.txtLocation_EditValueChanged);
            this.txtLocation.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit_Validating);
            // 
            // lueRepairDueID
            // 
            this.lueRepairDueID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "RepairDueID", true));
            this.lueRepairDueID.Location = new System.Drawing.Point(137, 212);
            this.lueRepairDueID.MenuManager = this.barManager1;
            this.lueRepairDueID.Name = "lueRepairDueID";
            this.lueRepairDueID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueRepairDueID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Repair Action", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueRepairDueID.Properties.DataSource = this.spAS11000PLIncidentRepairDueBindingSource;
            this.lueRepairDueID.Properties.DisplayMember = "Value";
            this.lueRepairDueID.Properties.NullText = "";
            this.lueRepairDueID.Properties.NullValuePrompt = "-- Please Select Repair Action --";
            this.lueRepairDueID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueRepairDueID.Properties.ValueMember = "PickListID";
            this.lueRepairDueID.Size = new System.Drawing.Size(396, 20);
            this.lueRepairDueID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueRepairDueID.TabIndex = 137;
            this.lueRepairDueID.Tag = "Repair Action";
            this.lueRepairDueID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLIncidentRepairDueBindingSource
            // 
            this.spAS11000PLIncidentRepairDueBindingSource.DataMember = "sp_AS_11000_PL_Incident_Repair_Due";
            this.spAS11000PLIncidentRepairDueBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueSeverityID
            // 
            this.lueSeverityID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "SeverityID", true));
            this.lueSeverityID.Location = new System.Drawing.Point(650, 116);
            this.lueSeverityID.MenuManager = this.barManager1;
            this.lueSeverityID.Name = "lueSeverityID";
            this.lueSeverityID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSeverityID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Severity", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSeverityID.Properties.DataSource = this.spAS11000PLSeverityBindingSource;
            this.lueSeverityID.Properties.DisplayMember = "Value";
            this.lueSeverityID.Properties.NullText = "";
            this.lueSeverityID.Properties.NullValuePrompt = "-- Please Select Severity --";
            this.lueSeverityID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSeverityID.Properties.ValueMember = "PickListID";
            this.lueSeverityID.Size = new System.Drawing.Size(396, 20);
            this.lueSeverityID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueSeverityID.TabIndex = 138;
            this.lueSeverityID.Tag = "Incident Severity";
            this.lueSeverityID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLSeverityBindingSource
            // 
            this.spAS11000PLSeverityBindingSource.DataMember = "sp_AS_11000_PL_Severity";
            this.spAS11000PLSeverityBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // txtWitness
            // 
            this.txtWitness.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "Witness", true));
            this.txtWitness.Location = new System.Drawing.Point(137, 164);
            this.txtWitness.MenuManager = this.barManager1;
            this.txtWitness.Name = "txtWitness";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtWitness, true);
            this.txtWitness.Size = new System.Drawing.Size(396, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtWitness, optionsSpelling4);
            this.txtWitness.StyleController = this.editFormDataLayoutControlGroup;
            this.txtWitness.TabIndex = 139;
            this.txtWitness.Tag = "Incident Witness";
            this.txtWitness.EditValueChanged += new System.EventHandler(this.txtWitness_EditValueChanged);
            this.txtWitness.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit_Validating);
            // 
            // lueKeeperAtFaultID
            // 
            this.lueKeeperAtFaultID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "KeeperAtFaultID", true));
            this.lueKeeperAtFaultID.Location = new System.Drawing.Point(137, 188);
            this.lueKeeperAtFaultID.MenuManager = this.barManager1;
            this.lueKeeperAtFaultID.Name = "lueKeeperAtFaultID";
            this.lueKeeperAtFaultID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKeeperAtFaultID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Repair Action", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueKeeperAtFaultID.Properties.DataSource = this.spAS11000PLKeeperAtFaultBindingSource;
            this.lueKeeperAtFaultID.Properties.DisplayMember = "Value";
            this.lueKeeperAtFaultID.Properties.NullText = "";
            this.lueKeeperAtFaultID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueKeeperAtFaultID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueKeeperAtFaultID.Properties.ValueMember = "PickListID";
            this.lueKeeperAtFaultID.Size = new System.Drawing.Size(396, 20);
            this.lueKeeperAtFaultID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueKeeperAtFaultID.TabIndex = 140;
            this.lueKeeperAtFaultID.Tag = "Keeper At Fault";
            this.lueKeeperAtFaultID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLKeeperAtFaultBindingSource
            // 
            this.spAS11000PLKeeperAtFaultBindingSource.DataMember = "sp_AS_11000_PL_Keeper_At_Fault";
            this.spAS11000PLKeeperAtFaultBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // lueLegalActionID
            // 
            this.lueLegalActionID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "LegalActionID", true));
            this.lueLegalActionID.Location = new System.Drawing.Point(650, 164);
            this.lueLegalActionID.MenuManager = this.barManager1;
            this.lueLegalActionID.Name = "lueLegalActionID";
            this.lueLegalActionID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLegalActionID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Legal Action", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueLegalActionID.Properties.DataSource = this.spAS11000PLLegalActionBindingSource;
            this.lueLegalActionID.Properties.DisplayMember = "Value";
            this.lueLegalActionID.Properties.NullText = "";
            this.lueLegalActionID.Properties.NullValuePrompt = "-- Please Select Legal Action --";
            this.lueLegalActionID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueLegalActionID.Properties.ValueMember = "PickListID";
            this.lueLegalActionID.Size = new System.Drawing.Size(396, 20);
            this.lueLegalActionID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueLegalActionID.TabIndex = 141;
            this.lueLegalActionID.Tag = "Legal Action";
            this.lueLegalActionID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // spAS11000PLLegalActionBindingSource
            // 
            this.spAS11000PLLegalActionBindingSource.DataMember = "sp_AS_11000_PL_Legal_Action";
            this.spAS11000PLLegalActionBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // deFollowUpDate
            // 
            this.deFollowUpDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "FollowUpDate", true));
            this.deFollowUpDate.EditValue = null;
            this.deFollowUpDate.Location = new System.Drawing.Point(650, 188);
            this.deFollowUpDate.MenuManager = this.barManager1;
            this.deFollowUpDate.Name = "deFollowUpDate";
            this.deFollowUpDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFollowUpDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFollowUpDate.Size = new System.Drawing.Size(396, 20);
            this.deFollowUpDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deFollowUpDate.TabIndex = 142;
            this.deFollowUpDate.Tag = "Follow Up Date";
            // 
            // spnCost
            // 
            this.spnCost.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "Cost", true));
            this.spnCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnCost.Location = new System.Drawing.Point(650, 212);
            this.spnCost.MenuManager = this.barManager1;
            this.spnCost.Name = "spnCost";
            this.spnCost.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnCost.Properties.DisplayFormat.FormatString = "c2";
            this.spnCost.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnCost.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnCost.Size = new System.Drawing.Size(396, 20);
            this.spnCost.StyleController = this.editFormDataLayoutControlGroup;
            this.spnCost.TabIndex = 143;
            this.spnCost.Tag = "Cost";
            // 
            // meNotes
            // 
            this.meNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spAS11081IncidentItemBindingSource, "Notes", true));
            this.meNotes.Location = new System.Drawing.Point(36, 272);
            this.meNotes.MenuManager = this.barManager1;
            this.meNotes.Name = "meNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.meNotes, true);
            this.meNotes.Size = new System.Drawing.Size(998, 252);
            this.scSpellChecker.SetSpellCheckerOptions(this.meNotes, optionsSpelling5);
            this.meNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.meNotes.TabIndex = 144;
            this.meNotes.Tag = "Notes";
            this.meNotes.UseOptimizedRendering = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1050, 517);
            this.layoutControlGroup1.Text = "autoGeneratedGroup0";
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(1050, 517);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.tabbedControlGroup2.Text = "tabbedControlGroup2";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Incident Details";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIncidentReference,
            this.ItemForCost,
            this.tabbedControlGroup1,
            this.ItemForEquipmentReference,
            this.ItemForIncidentTypeID,
            this.ItemForSeverityID,
            this.ItemForIncidentStatusID,
            this.ItemForLocation,
            this.ItemForLegalActionID,
            this.ItemForFollowUpDate,
            this.ItemForWitness,
            this.ItemForDateHappened,
            this.ItemForKeeperAtFaultID,
            this.ItemForRepairDueID});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1026, 472);
            this.layoutControlGroup2.Text = "Incident Details";
            // 
            // ItemForIncidentReference
            // 
            this.ItemForIncidentReference.Control = this.txtIncidentReference;
            this.ItemForIncidentReference.CustomizationFormText = "Incident Reference :";
            this.ItemForIncidentReference.Location = new System.Drawing.Point(0, 24);
            this.ItemForIncidentReference.Name = "ItemForIncidentReference";
            this.ItemForIncidentReference.Size = new System.Drawing.Size(513, 24);
            this.ItemForIncidentReference.Text = "Incident Reference :";
            this.ItemForIncidentReference.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForCost
            // 
            this.ItemForCost.Control = this.spnCost;
            this.ItemForCost.CustomizationFormText = "Cost :";
            this.ItemForCost.Location = new System.Drawing.Point(513, 144);
            this.ItemForCost.Name = "ItemForCost";
            this.ItemForCost.Size = new System.Drawing.Size(513, 24);
            this.ItemForCost.Text = "Cost :";
            this.ItemForCost.TextSize = new System.Drawing.Size(110, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 168);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1026, 304);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImage")));
            this.layoutControlGroup3.CustomizationFormText = "Notes";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForNotes});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1002, 256);
            this.layoutControlGroup3.Text = "Notes";
            // 
            // ItemForNotes
            // 
            this.ItemForNotes.Control = this.meNotes;
            this.ItemForNotes.CustomizationFormText = "Notes :";
            this.ItemForNotes.Location = new System.Drawing.Point(0, 0);
            this.ItemForNotes.Name = "ItemForNotes";
            this.ItemForNotes.Size = new System.Drawing.Size(1002, 256);
            this.ItemForNotes.Text = "Notes :";
            this.ItemForNotes.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForNotes.TextToControlDistance = 0;
            this.ItemForNotes.TextVisible = false;
            // 
            // ItemForEquipmentReference
            // 
            this.ItemForEquipmentReference.Control = this.txtEquipmentReference;
            this.ItemForEquipmentReference.CustomizationFormText = "Equipment Reference :";
            this.ItemForEquipmentReference.Location = new System.Drawing.Point(0, 0);
            this.ItemForEquipmentReference.Name = "ItemForEquipmentReference";
            this.ItemForEquipmentReference.Size = new System.Drawing.Size(1026, 24);
            this.ItemForEquipmentReference.Text = "Equipment Reference :";
            this.ItemForEquipmentReference.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForIncidentTypeID
            // 
            this.ItemForIncidentTypeID.Control = this.lueIncidentTypeID;
            this.ItemForIncidentTypeID.CustomizationFormText = "Incident Type :";
            this.ItemForIncidentTypeID.Location = new System.Drawing.Point(513, 24);
            this.ItemForIncidentTypeID.Name = "ItemForIncidentTypeID";
            this.ItemForIncidentTypeID.Size = new System.Drawing.Size(513, 24);
            this.ItemForIncidentTypeID.Text = "Incident Type :";
            this.ItemForIncidentTypeID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForSeverityID
            // 
            this.ItemForSeverityID.Control = this.lueSeverityID;
            this.ItemForSeverityID.CustomizationFormText = "Severity :";
            this.ItemForSeverityID.Location = new System.Drawing.Point(513, 48);
            this.ItemForSeverityID.Name = "ItemForSeverityID";
            this.ItemForSeverityID.Size = new System.Drawing.Size(513, 24);
            this.ItemForSeverityID.Text = "Severity :";
            this.ItemForSeverityID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForIncidentStatusID
            // 
            this.ItemForIncidentStatusID.Control = this.lueIncidentStatusID;
            this.ItemForIncidentStatusID.CustomizationFormText = "Incident Status :";
            this.ItemForIncidentStatusID.Location = new System.Drawing.Point(0, 48);
            this.ItemForIncidentStatusID.Name = "ItemForIncidentStatusID";
            this.ItemForIncidentStatusID.Size = new System.Drawing.Size(513, 24);
            this.ItemForIncidentStatusID.Text = "Incident Status :";
            this.ItemForIncidentStatusID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForLocation
            // 
            this.ItemForLocation.Control = this.txtLocation;
            this.ItemForLocation.CustomizationFormText = "Location :";
            this.ItemForLocation.Location = new System.Drawing.Point(0, 72);
            this.ItemForLocation.Name = "ItemForLocation";
            this.ItemForLocation.Size = new System.Drawing.Size(513, 24);
            this.ItemForLocation.Text = "Location :";
            this.ItemForLocation.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForLegalActionID
            // 
            this.ItemForLegalActionID.Control = this.lueLegalActionID;
            this.ItemForLegalActionID.CustomizationFormText = "Legal Action :";
            this.ItemForLegalActionID.Location = new System.Drawing.Point(513, 96);
            this.ItemForLegalActionID.Name = "ItemForLegalActionID";
            this.ItemForLegalActionID.Size = new System.Drawing.Size(513, 24);
            this.ItemForLegalActionID.Text = "Legal Action :";
            this.ItemForLegalActionID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForFollowUpDate
            // 
            this.ItemForFollowUpDate.Control = this.deFollowUpDate;
            this.ItemForFollowUpDate.CustomizationFormText = "Follow Up Date :";
            this.ItemForFollowUpDate.Location = new System.Drawing.Point(513, 120);
            this.ItemForFollowUpDate.Name = "ItemForFollowUpDate";
            this.ItemForFollowUpDate.Size = new System.Drawing.Size(513, 24);
            this.ItemForFollowUpDate.Text = "Follow Up Date :";
            this.ItemForFollowUpDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForWitness
            // 
            this.ItemForWitness.Control = this.txtWitness;
            this.ItemForWitness.CustomizationFormText = "Witness :";
            this.ItemForWitness.Location = new System.Drawing.Point(0, 96);
            this.ItemForWitness.Name = "ItemForWitness";
            this.ItemForWitness.Size = new System.Drawing.Size(513, 24);
            this.ItemForWitness.Text = "Witness :";
            this.ItemForWitness.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForDateHappened
            // 
            this.ItemForDateHappened.Control = this.deDateHappened;
            this.ItemForDateHappened.CustomizationFormText = "Date Happened :";
            this.ItemForDateHappened.Location = new System.Drawing.Point(513, 72);
            this.ItemForDateHappened.Name = "ItemForDateHappened";
            this.ItemForDateHappened.Size = new System.Drawing.Size(513, 24);
            this.ItemForDateHappened.Text = "Date Happened :";
            this.ItemForDateHappened.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForKeeperAtFaultID
            // 
            this.ItemForKeeperAtFaultID.Control = this.lueKeeperAtFaultID;
            this.ItemForKeeperAtFaultID.CustomizationFormText = "Keeper At Fault :";
            this.ItemForKeeperAtFaultID.Location = new System.Drawing.Point(0, 120);
            this.ItemForKeeperAtFaultID.Name = "ItemForKeeperAtFaultID";
            this.ItemForKeeperAtFaultID.Size = new System.Drawing.Size(513, 24);
            this.ItemForKeeperAtFaultID.Text = "Keeper At Fault :";
            this.ItemForKeeperAtFaultID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForRepairDueID
            // 
            this.ItemForRepairDueID.Control = this.lueRepairDueID;
            this.ItemForRepairDueID.CustomizationFormText = "Repair Action :";
            this.ItemForRepairDueID.Location = new System.Drawing.Point(0, 144);
            this.ItemForRepairDueID.Name = "ItemForRepairDueID";
            this.ItemForRepairDueID.Size = new System.Drawing.Size(513, 24);
            this.ItemForRepairDueID.Text = "Repair Action :";
            this.ItemForRepairDueID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(132, 23);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(446, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(604, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp_AS_11000_PL_Incident_StatusTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_TypeTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Incident_Repair_DueTableAdapter
            // 
            this.sp_AS_11000_PL_Incident_Repair_DueTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11081_Incident_ItemTableAdapter
            // 
            this.sp_AS_11081_Incident_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Keeper_At_FaultTableAdapter
            // 
            this.sp_AS_11000_PL_Keeper_At_FaultTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_SeverityTableAdapter
            // 
            this.sp_AS_11000_PL_SeverityTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11000_PL_Legal_ActionTableAdapter
            // 
            this.sp_AS_11000_PL_Legal_ActionTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AS_Incident_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1070, 616);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AS_Incident_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Incidents";
            this.Activated += new System.EventHandler(this.frm_AS_Incident_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Incident_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Incident_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11081IncidentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEquipmentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueIncidentStatusID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLIncidentStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueIncidentTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLIncidentTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIncidentReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateHappened.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deDateHappened.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRepairDueID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLIncidentRepairDueBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSeverityID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLSeverityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWitness.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKeeperAtFaultID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLKeeperAtFaultBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLegalActionID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000PLLegalActionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFollowUpDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFollowUpDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNotes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipmentReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSeverityID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLegalActionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFollowUpDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWitness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateHappened)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForKeeperAtFaultID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRepairDueID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource spAS11000PLIncidentStatusBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLIncidentTypeBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLIncidentRepairDueBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_StatusTableAdapter sp_AS_11000_PL_Incident_StatusTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_TypeTableAdapter sp_AS_11000_PL_Incident_TypeTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Incident_Repair_DueTableAdapter sp_AS_11000_PL_Incident_Repair_DueTableAdapter;
        private DevExpress.XtraEditors.TextEdit txtEquipmentReference;
        private System.Windows.Forms.BindingSource spAS11081IncidentItemBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueIncidentStatusID;
        private DevExpress.XtraEditors.LookUpEdit lueIncidentTypeID;
        private DevExpress.XtraEditors.TextEdit txtIncidentReference;
        private DevExpress.XtraEditors.DateEdit deDateHappened;
        private DevExpress.XtraEditors.TextEdit txtLocation;
        private DevExpress.XtraEditors.LookUpEdit lueRepairDueID;
        private DevExpress.XtraEditors.LookUpEdit lueSeverityID;
        private DevExpress.XtraEditors.TextEdit txtWitness;
        private DevExpress.XtraEditors.LookUpEdit lueKeeperAtFaultID;
        private DevExpress.XtraEditors.LookUpEdit lueLegalActionID;
        private DevExpress.XtraEditors.DateEdit deFollowUpDate;
        private DevExpress.XtraEditors.SpinEdit spnCost;
        private DevExpress.XtraEditors.MemoEdit meNotes;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11081_Incident_ItemTableAdapter sp_AS_11081_Incident_ItemTableAdapter;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLocation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncidentStatusID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSeverityID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncidentReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncidentTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateHappened;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWitness;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLegalActionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForKeeperAtFaultID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRepairDueID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFollowUpDate;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNotes;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipmentReference;
        private System.Windows.Forms.BindingSource spAS11000PLSeverityBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLKeeperAtFaultBindingSource;
        private System.Windows.Forms.BindingSource spAS11000PLLegalActionBindingSource;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Keeper_At_FaultTableAdapter sp_AS_11000_PL_Keeper_At_FaultTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_SeverityTableAdapter sp_AS_11000_PL_SeverityTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11000_PL_Legal_ActionTableAdapter sp_AS_11000_PL_Legal_ActionTableAdapter;


    }
}

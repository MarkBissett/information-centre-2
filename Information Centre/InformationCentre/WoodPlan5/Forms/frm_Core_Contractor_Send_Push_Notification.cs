using System;
using System.Collections;
using System.ComponentModel;
using System.Data;

using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Contractor_Send_Push_Notification : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string _PassedInPersonIDs = "";
        public int _PassedInPersonTypeID = 0;  // 0 = Staff, 1 = Contractor //

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        public frm_Core_Contractor_Send_Push_Notification()
        {
            InitializeComponent();
        }

        private void frm_Core_Contractor_Send_Push_Notification_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 500260;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            sp01013_Core_Push_Notifications_Allocated_PDAsTableAdapter.Connection.ConnectionString = strConnectionString;

            textEditTitle.Text = "";
            memoEditMessage.Text = "";

            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            gridControl1.ForceInitialize();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            LoadLastSavedUserScreenSettings();

            Load_Data();
            selection1.SelectAll();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        private void frm_Core_Contractor_Send_Push_Notification_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_Contractor_Send_Push_Notification_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Title", textEditTitle.Text);
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "Message", memoEditMessage.Text);
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }
        
        
        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                string strText = default_screen_settings.RetrieveSetting("Title");
                if (!string.IsNullOrWhiteSpace(strText)) textEditTitle.Text = strText;
                
                strText = default_screen_settings.RetrieveSetting("Message");
                if (!string.IsNullOrWhiteSpace(strText)) memoEditMessage.Text = strText;
            }
        }

        private void ProcessPermissionsForForm()
        {
            /*for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }*/
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            view = (GridView)gridControl1.MainView;

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01013_Core_Push_Notifications_Allocated_PDAsTableAdapter.Fill(dataSet_Common_Functionality.sp01013_Core_Push_Notifications_Allocated_PDAs, _PassedInPersonIDs, _PassedInPersonTypeID);
            view.ExpandAllGroups();
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No handsets available for sending push notifications t");
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //if (iBool_AllowEdit)
                //{
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                //}
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
        }

        #endregion


        private void btnSend_Click(object sender, EventArgs e)
        {
            string str_selected_ids = "";
            GridView view = (GridView)gridControl1.MainView;
            bool boolValid = true;
            if (view.DataRowCount <= 0)
            {
                boolValid = false;
            }
            else if (selection1.SelectedCount <= 0)
            {
                boolValid = false;
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection"))) str_selected_ids += Convert.ToString(view.GetRowCellValue(i, "PDALoginToken")) + ",";
                }
            }
            if (string.IsNullOrWhiteSpace(str_selected_ids) || !boolValid)
            {
                XtraMessageBox.Show("Select one or more handsets to send the push notification to before proceeding.", "Send Push Notification", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strMessage = (memoEditMessage.EditValue == null ? "" : memoEditMessage.EditValue.ToString());
            if (string.IsNullOrWhiteSpace(strMessage))
            {
                XtraMessageBox.Show("Enter the message to send to the handset(s) before proceeding.", "Send Push Notification", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strTitle = (textEditTitle.EditValue == null ? "" : textEditTitle.EditValue.ToString());
            if (string.IsNullOrWhiteSpace(strTitle)) strTitle = "";
            using (var GetCloudID = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
            {
                GetCloudID.ChangeConnectionString(strConnectionString);
                try
                {
                    char[] delimiters = new char[] { ',' };
                    Array arrayItems = str_selected_ids.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string strElement in arrayItems)
                    {
                        if (string.IsNullOrWhiteSpace(strElement.Trim())) continue;
                        GoogleCloudMessaging GCM = new GoogleCloudMessaging();
                        string[] strPhoneIDs = GetCloudID.sp01014_Core_Get_Phone_IDs_For_Messaging(strElement).ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (strPhoneIDs.Length <= 0) continue;  // Abort and move to next visit //
                        GCM.SendUsingGoogleCloudMessaging(strPhoneIDs, strTitle, strMessage, 0, 0, "", strConnectionString);
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while attempting to send the push notification - error: " + ex.Message + "\n\nPlease try again. If problems persist contact Technical Support.", "Send Push Notification", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            XtraMessageBox.Show("Push Notification(s) sent successfully.", "Send Push Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }





    }
}


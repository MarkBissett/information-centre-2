﻿namespace WoodPlan5
{
    partial class frm_Core_Package_Recipient_Details_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Package_Recipient_Details_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.lueJobPackageID = new DevExpress.XtraEditors.LookUpEdit();
            this.spPC922703PackageEmailListItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_PC_Core = new WoodPlan5.DataSet_PC_Core();
            this.ceActive = new DevExpress.XtraEditors.CheckEdit();
            this.lueRecipientTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.lueRecipient = new DevExpress.XtraEditors.LookUpEdit();
            this.ceNotifyFailure = new DevExpress.XtraEditors.CheckEdit();
            this.ceNotifySuccess = new DevExpress.XtraEditors.CheckEdit();
            this.deLastSendDate = new DevExpress.XtraEditors.DateEdit();
            this.ReportLevelSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ceSendReport = new DevExpress.XtraEditors.CheckEdit();
            this.ceIsReportOwner = new DevExpress.XtraEditors.CheckEdit();
            this.txtRecipientEmail = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRecipientEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.spPC922706PackageUserTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_PC_922706_Package_UserTypeTableAdapter = new WoodPlan5.DataSet_PC_CoreTableAdapters.sp_PC_922706_Package_UserTypeTableAdapter();
            this.spPC922705PackageUsersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_PC_922705_Package_UsersTableAdapter = new WoodPlan5.DataSet_PC_CoreTableAdapters.sp_PC_922705_Package_UsersTableAdapter();
            this.spPC922701PackageConfigurationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_PC_922701_Package_Configuration_ItemTableAdapter = new WoodPlan5.DataSet_PC_CoreTableAdapters.sp_PC_922701_Package_Configuration_ItemTableAdapter();
            this.sp_PC_922703_Package_Email_List_ItemTableAdapter = new WoodPlan5.DataSet_PC_CoreTableAdapters.sp_PC_922703_Package_Email_List_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueJobPackageID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922703PackageEmailListItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_PC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRecipientTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRecipient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNotifyFailure.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNotifySuccess.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLastSendDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLastSendDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportLevelSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSendReport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsReportOwner.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRecipientEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecipientEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922706PackageUserTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922705PackageUsersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922701PackageConfigurationItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1059, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 702);
            this.barDockControlBottom.Size = new System.Drawing.Size(1059, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 671);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1059, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 671);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation,
            this.bbiRefresh});
            this.barManager1.MaxItemId = 36;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefresh, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh Picklists";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 35;
            this.bbiRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.LargeGlyph")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1059, 671);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(334, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(245, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.Location = new System.Drawing.Point(346, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(241, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueJobPackageID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceActive);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueRecipientTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueRecipient);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceNotifyFailure);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceNotifySuccess);
            this.editFormDataLayoutControlGroup.Controls.Add(this.deLastSendDate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ReportLevelSpinEdit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceSendReport);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceIsReportOwner);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtRecipientEmail);
            this.editFormDataLayoutControlGroup.DataSource = this.spPC922703PackageEmailListItemBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 31);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(861, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1059, 671);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // lueJobPackageID
            // 
            this.lueJobPackageID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "JobPackageID", true));
            this.lueJobPackageID.Enabled = false;
            this.lueJobPackageID.Location = new System.Drawing.Point(105, 35);
            this.lueJobPackageID.MenuManager = this.barManager1;
            this.lueJobPackageID.Name = "lueJobPackageID";
            this.lueJobPackageID.Properties.Appearance.Options.UseTextOptions = true;
            this.lueJobPackageID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lueJobPackageID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueJobPackageID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("JobPackageID", "Job Package ID", 97, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("JobPackageDescription", "Job Package Name", 126, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueJobPackageID.Properties.DataSource = this.spPC922701PackageConfigurationItemBindingSource;
            this.lueJobPackageID.Properties.DisplayMember = "JobPackageDescription";
            this.lueJobPackageID.Properties.NullText = "";
            this.lueJobPackageID.Properties.ValueMember = "JobPackageID";
            this.lueJobPackageID.Size = new System.Drawing.Size(422, 20);
            this.lueJobPackageID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueJobPackageID.TabIndex = 133;
            this.lueJobPackageID.Tag = "Job Package Name";
            this.lueJobPackageID.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // spPC922703PackageEmailListItemBindingSource
            // 
            this.spPC922703PackageEmailListItemBindingSource.DataMember = "sp_PC_922703_Package_Email_List_Item";
            this.spPC922703PackageEmailListItemBindingSource.DataSource = this.dataSet_PC_Core;
            // 
            // dataSet_PC_Core
            // 
            this.dataSet_PC_Core.DataSetName = "DataSet_PC_Core";
            this.dataSet_PC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ceActive
            // 
            this.ceActive.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "Active", true));
            this.ceActive.Location = new System.Drawing.Point(105, 130);
            this.ceActive.MenuManager = this.barManager1;
            this.ceActive.Name = "ceActive";
            this.ceActive.Properties.Caption = "Active";
            this.ceActive.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ceActive.Size = new System.Drawing.Size(422, 19);
            this.ceActive.StyleController = this.editFormDataLayoutControlGroup;
            this.ceActive.TabIndex = 134;
            // 
            // lueRecipientTypeID
            // 
            this.lueRecipientTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "UserTypeID", true));
            this.lueRecipientTypeID.Location = new System.Drawing.Point(105, 59);
            this.lueRecipientTypeID.MenuManager = this.barManager1;
            this.lueRecipientTypeID.Name = "lueRecipientTypeID";
            this.lueRecipientTypeID.Properties.Appearance.Options.UseTextOptions = true;
            this.lueRecipientTypeID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lueRecipientTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueRecipientTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserTypeID", "Recipient Type ID", 86, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserType", "Recipient Type", 59, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueRecipientTypeID.Properties.DataSource = this.spPC922706PackageUserTypeBindingSource;
            this.lueRecipientTypeID.Properties.DisplayMember = "UserType";
            this.lueRecipientTypeID.Properties.NullText = "";
            this.lueRecipientTypeID.Properties.ValueMember = "UserTypeID";
            this.lueRecipientTypeID.Size = new System.Drawing.Size(422, 20);
            this.lueRecipientTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueRecipientTypeID.TabIndex = 135;
            this.lueRecipientTypeID.Tag = "Recipient Type";
            this.lueRecipientTypeID.EditValueChanged += new System.EventHandler(this.lueRecipientTypeID_EditValueChanged);
            this.lueRecipientTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // lueRecipient
            // 
            this.lueRecipient.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "UserID", true));
            this.lueRecipient.Location = new System.Drawing.Point(624, 59);
            this.lueRecipient.MenuManager = this.barManager1;
            this.lueRecipient.Name = "lueRecipient";
            this.lueRecipient.Properties.Appearance.Options.UseTextOptions = true;
            this.lueRecipient.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lueRecipient.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueRecipient.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("UserType", "Recipient Type", 59, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FullName", "Full Name", 69, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EmailAddress", "Email Address", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueRecipient.Properties.DataSource = this.spPC922705PackageUsersBindingSource;
            this.lueRecipient.Properties.DisplayMember = "FullName";
            this.lueRecipient.Properties.NullText = "";
            this.lueRecipient.Properties.ValueMember = "UserID";
            this.lueRecipient.Size = new System.Drawing.Size(423, 20);
            this.lueRecipient.StyleController = this.editFormDataLayoutControlGroup;
            this.lueRecipient.TabIndex = 136;
            this.lueRecipient.Tag = "Recipient";
            this.lueRecipient.EditValueChanged += new System.EventHandler(this.lueRecipient_EditValueChanged);
            this.lueRecipient.Enter += new System.EventHandler(this.lueRecipient_Enter);
            this.lueRecipient.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // ceNotifyFailure
            // 
            this.ceNotifyFailure.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "NotifyFailure", true));
            this.ceNotifyFailure.Location = new System.Drawing.Point(105, 107);
            this.ceNotifyFailure.MenuManager = this.barManager1;
            this.ceNotifyFailure.Name = "ceNotifyFailure";
            this.ceNotifyFailure.Properties.Caption = "Notify Failure";
            this.ceNotifyFailure.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ceNotifyFailure.Size = new System.Drawing.Size(422, 19);
            this.ceNotifyFailure.StyleController = this.editFormDataLayoutControlGroup;
            this.ceNotifyFailure.TabIndex = 137;
            // 
            // ceNotifySuccess
            // 
            this.ceNotifySuccess.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "NotifySuccess", true));
            this.ceNotifySuccess.Location = new System.Drawing.Point(624, 107);
            this.ceNotifySuccess.MenuManager = this.barManager1;
            this.ceNotifySuccess.Name = "ceNotifySuccess";
            this.ceNotifySuccess.Properties.Caption = "Notify Success";
            this.ceNotifySuccess.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ceNotifySuccess.Size = new System.Drawing.Size(423, 19);
            this.ceNotifySuccess.StyleController = this.editFormDataLayoutControlGroup;
            this.ceNotifySuccess.TabIndex = 138;
            // 
            // deLastSendDate
            // 
            this.deLastSendDate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "LastSendDate", true));
            this.deLastSendDate.EditValue = null;
            this.deLastSendDate.Location = new System.Drawing.Point(624, 153);
            this.deLastSendDate.MenuManager = this.barManager1;
            this.deLastSendDate.Name = "deLastSendDate";
            this.deLastSendDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deLastSendDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deLastSendDate.Size = new System.Drawing.Size(423, 20);
            this.deLastSendDate.StyleController = this.editFormDataLayoutControlGroup;
            this.deLastSendDate.TabIndex = 139;
            this.deLastSendDate.Tag = "Last Sent Date";
            this.deLastSendDate.Validating += new System.ComponentModel.CancelEventHandler(this.dateEdit_Validating);
            // 
            // ReportLevelSpinEdit
            // 
            this.ReportLevelSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "ReportLevel", true));
            this.ReportLevelSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ReportLevelSpinEdit.Location = new System.Drawing.Point(105, 83);
            this.ReportLevelSpinEdit.MenuManager = this.barManager1;
            this.ReportLevelSpinEdit.Name = "ReportLevelSpinEdit";
            this.ReportLevelSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ReportLevelSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ReportLevelSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ReportLevelSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.ReportLevelSpinEdit.Properties.Mask.EditMask = "N0";
            this.ReportLevelSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ReportLevelSpinEdit.Size = new System.Drawing.Size(422, 20);
            this.ReportLevelSpinEdit.StyleController = this.editFormDataLayoutControlGroup;
            this.ReportLevelSpinEdit.TabIndex = 140;
            this.ReportLevelSpinEdit.Tag = "Report Level";
            // 
            // ceSendReport
            // 
            this.ceSendReport.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "SendReport", true));
            this.ceSendReport.Location = new System.Drawing.Point(105, 153);
            this.ceSendReport.MenuManager = this.barManager1;
            this.ceSendReport.Name = "ceSendReport";
            this.ceSendReport.Properties.Caption = "Send Report";
            this.ceSendReport.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ceSendReport.Size = new System.Drawing.Size(422, 19);
            this.ceSendReport.StyleController = this.editFormDataLayoutControlGroup;
            this.ceSendReport.TabIndex = 141;
            // 
            // ceIsReportOwner
            // 
            this.ceIsReportOwner.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "IsReportOwner", true));
            this.ceIsReportOwner.Location = new System.Drawing.Point(624, 130);
            this.ceIsReportOwner.MenuManager = this.barManager1;
            this.ceIsReportOwner.Name = "ceIsReportOwner";
            this.ceIsReportOwner.Properties.Caption = "Is Report Owner";
            this.ceIsReportOwner.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ceIsReportOwner.Size = new System.Drawing.Size(423, 19);
            this.ceIsReportOwner.StyleController = this.editFormDataLayoutControlGroup;
            this.ceIsReportOwner.TabIndex = 142;
            // 
            // txtRecipientEmail
            // 
            this.txtRecipientEmail.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spPC922703PackageEmailListItemBindingSource, "RecipientEmail", true));
            this.txtRecipientEmail.Location = new System.Drawing.Point(624, 83);
            this.txtRecipientEmail.MenuManager = this.barManager1;
            this.txtRecipientEmail.Name = "txtRecipientEmail";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtRecipientEmail, true);
            this.txtRecipientEmail.Size = new System.Drawing.Size(423, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtRecipientEmail, optionsSpelling1);
            this.txtRecipientEmail.StyleController = this.editFormDataLayoutControlGroup;
            this.txtRecipientEmail.TabIndex = 143;
            this.txtRecipientEmail.Validating += new System.ComponentModel.CancelEventHandler(this.textBox_Validating);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem6,
            this.layoutControlItem10,
            this.emptySpaceItem3,
            this.layoutControlItem7,
            this.emptySpaceItem4,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem9,
            this.layoutControlItem3,
            this.layoutControlItem11,
            this.layoutControlItem8,
            this.ItemForRecipientEmail});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1039, 628);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lueJobPackageID;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "ItemForJobPackageID";
            this.layoutControlItem2.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem2.Text = "Job Package Name";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.ceNotifyFailure;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem6.Name = "ItemForNotifyFailure";
            this.layoutControlItem6.Size = new System.Drawing.Size(519, 23);
            this.layoutControlItem6.Text = "Notify Failure";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.ceSendReport;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 118);
            this.layoutControlItem10.Name = "ItemForSendReport";
            this.layoutControlItem10.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem10.Text = "Send Report";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 142);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1039, 486);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.ceNotifySuccess;
            this.layoutControlItem7.Location = new System.Drawing.Point(519, 72);
            this.layoutControlItem7.Name = "ItemForNotifySuccess";
            this.layoutControlItem7.Size = new System.Drawing.Size(520, 23);
            this.layoutControlItem7.Text = "Notify Success";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(519, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(520, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lueRecipientTypeID;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "ItemForUserTypeID";
            this.layoutControlItem4.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem4.Text = "Recipient Type";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lueRecipient;
            this.layoutControlItem5.Location = new System.Drawing.Point(519, 24);
            this.layoutControlItem5.Name = "ItemForUserID";
            this.layoutControlItem5.Size = new System.Drawing.Size(520, 24);
            this.layoutControlItem5.Text = "Recipient";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.ReportLevelSpinEdit;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem9.Name = "ItemForReportLevel";
            this.layoutControlItem9.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem9.Text = "Report Level";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.ceActive;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 95);
            this.layoutControlItem3.Name = "ItemForActive";
            this.layoutControlItem3.Size = new System.Drawing.Size(519, 23);
            this.layoutControlItem3.Text = "Active";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.ceIsReportOwner;
            this.layoutControlItem11.Location = new System.Drawing.Point(519, 95);
            this.layoutControlItem11.Name = "ItemForIsReportOwner";
            this.layoutControlItem11.Size = new System.Drawing.Size(520, 23);
            this.layoutControlItem11.Text = "Is Report Owner";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.deLastSendDate;
            this.layoutControlItem8.Location = new System.Drawing.Point(519, 118);
            this.layoutControlItem8.Name = "ItemForLastSendDate";
            this.layoutControlItem8.Size = new System.Drawing.Size(520, 24);
            this.layoutControlItem8.Text = "Last Sent Date";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(90, 13);
            // 
            // ItemForRecipientEmail
            // 
            this.ItemForRecipientEmail.Control = this.txtRecipientEmail;
            this.ItemForRecipientEmail.Location = new System.Drawing.Point(519, 48);
            this.ItemForRecipientEmail.Name = "ItemForRecipientEmail";
            this.ItemForRecipientEmail.Size = new System.Drawing.Size(520, 24);
            this.ItemForRecipientEmail.Text = "Recipient Email";
            this.ItemForRecipientEmail.TextSize = new System.Drawing.Size(90, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(334, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(579, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(460, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // spPC922706PackageUserTypeBindingSource
            // 
            this.spPC922706PackageUserTypeBindingSource.DataMember = "sp_PC_922706_Package_UserType";
            this.spPC922706PackageUserTypeBindingSource.DataSource = this.dataSet_PC_Core;
            // 
            // sp_PC_922706_Package_UserTypeTableAdapter
            // 
            this.sp_PC_922706_Package_UserTypeTableAdapter.ClearBeforeFill = true;
            // 
            // spPC922705PackageUsersBindingSource
            // 
            this.spPC922705PackageUsersBindingSource.DataMember = "sp_PC_922705_Package_Users";
            this.spPC922705PackageUsersBindingSource.DataSource = this.dataSet_PC_Core;
            // 
            // sp_PC_922705_Package_UsersTableAdapter
            // 
            this.sp_PC_922705_Package_UsersTableAdapter.ClearBeforeFill = true;
            // 
            // spPC922701PackageConfigurationItemBindingSource
            // 
            this.spPC922701PackageConfigurationItemBindingSource.DataMember = "sp_PC_922701_Package_Configuration_Item";
            this.spPC922701PackageConfigurationItemBindingSource.DataSource = this.dataSet_PC_Core;
            // 
            // sp_PC_922701_Package_Configuration_ItemTableAdapter
            // 
            this.sp_PC_922701_Package_Configuration_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_PC_922703_Package_Email_List_ItemTableAdapter
            // 
            this.sp_PC_922703_Package_Email_List_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Package_Recipient_Details_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1059, 729);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1065, 586);
            this.Name = "frm_Core_Package_Recipient_Details_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Package Recipient Details";
            this.Activated += new System.EventHandler(this.frm_Core_Package_Recipient_Details_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Package_Recipient_Details_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Package_Recipient_Details_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueJobPackageID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922703PackageEmailListItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_PC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRecipientTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRecipient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNotifyFailure.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceNotifySuccess.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLastSendDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deLastSendDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportLevelSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceSendReport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsReportOwner.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRecipientEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecipientEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922706PackageUserTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922705PackageUsersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spPC922701PackageConfigurationItemBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DataSet_PC_Core dataSet_PC_Core;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.LookUpEdit lueJobPackageID;
        private DevExpress.XtraEditors.CheckEdit ceActive;
        private DevExpress.XtraEditors.LookUpEdit lueRecipientTypeID;
        private DevExpress.XtraEditors.LookUpEdit lueRecipient;
        private DevExpress.XtraEditors.CheckEdit ceNotifyFailure;
        private DevExpress.XtraEditors.CheckEdit ceNotifySuccess;
        private DevExpress.XtraEditors.DateEdit deLastSendDate;
        private DevExpress.XtraEditors.SpinEdit ReportLevelSpinEdit;
        private DevExpress.XtraEditors.CheckEdit ceSendReport;
        private DevExpress.XtraEditors.CheckEdit ceIsReportOwner;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit txtRecipientEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRecipientEmail;
        private System.Windows.Forms.BindingSource spPC922706PackageUserTypeBindingSource;
        private DataSet_PC_CoreTableAdapters.sp_PC_922706_Package_UserTypeTableAdapter sp_PC_922706_Package_UserTypeTableAdapter;
        private System.Windows.Forms.BindingSource spPC922705PackageUsersBindingSource;
        private DataSet_PC_CoreTableAdapters.sp_PC_922705_Package_UsersTableAdapter sp_PC_922705_Package_UsersTableAdapter;
        private System.Windows.Forms.BindingSource spPC922703PackageEmailListItemBindingSource;
        private System.Windows.Forms.BindingSource spPC922701PackageConfigurationItemBindingSource;
        private DataSet_PC_CoreTableAdapters.sp_PC_922701_Package_Configuration_ItemTableAdapter sp_PC_922701_Package_Configuration_ItemTableAdapter;
        private DataSet_PC_CoreTableAdapters.sp_PC_922703_Package_Email_List_ItemTableAdapter sp_PC_922703_Package_Email_List_ItemTableAdapter;
    }
}

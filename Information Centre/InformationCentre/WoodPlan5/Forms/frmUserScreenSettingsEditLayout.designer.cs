namespace WoodPlan5
{
    partial class frmUserScreenSettingsEditLayout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUserScreenSettingsEditLayout));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.dataSet_Groups = new WoodPlan5.DataSet_Groups();
            this.dxValidationProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.sp00088edituserscreenlayoutBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.LayoutDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LayoutRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DefaultLayoutCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLayoutDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLayoutRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDefaultLayout = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00088_edit_user_screen_layoutTableAdapter = new WoodPlan5.DataSet_GroupsTableAdapters.sp00088_edit_user_screen_layoutTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Groups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00088edituserscreenlayoutBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultLayoutCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLayoutDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLayoutRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(543, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 399);
            this.barDockControlBottom.Size = new System.Drawing.Size(543, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(543, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // dataSet_Groups
            // 
            this.dataSet_Groups.DataSetName = "DataSet_Groups";
            this.dataSet_Groups.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.LayoutDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LayoutRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DefaultLayoutCheckEdit);
            this.dataLayoutControl1.DataSource = this.sp00088edituserscreenlayoutBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.MenuManager = this.barManager1;
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(720, 199, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(543, 399);
            this.dataLayoutControl1.TabIndex = 21;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Hint = "Cancel changes and close screen";
            this.dataNavigator1.Buttons.CancelEdit.ImageIndex = 1;
            this.dataNavigator1.Buttons.EndEdit.Hint = "Save changes and close screen";
            this.dataNavigator1.Buttons.EndEdit.ImageIndex = 0;
            this.dataNavigator1.Buttons.ImageList = this.imageCollection1;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00088edituserscreenlayoutBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.ShowToolTips = true;
            this.dataNavigator1.Size = new System.Drawing.Size(153, 24);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 10;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.SaveAndClose_16x16, "SaveAndClose_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "SaveAndClose_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.close_16x16, "close_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "close_16x16");
            // 
            // sp00088edituserscreenlayoutBindingSource
            // 
            this.sp00088edituserscreenlayoutBindingSource.DataMember = "sp00088_edit_user_screen_layout";
            this.sp00088edituserscreenlayoutBindingSource.DataSource = this.dataSet_Groups;
            // 
            // LayoutDescriptionTextEdit
            // 
            this.LayoutDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00088edituserscreenlayoutBindingSource, "LayoutDescription", true));
            this.LayoutDescriptionTextEdit.Location = new System.Drawing.Point(108, 40);
            this.LayoutDescriptionTextEdit.MenuManager = this.barManager1;
            this.LayoutDescriptionTextEdit.Name = "LayoutDescriptionTextEdit";
            this.LayoutDescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LayoutDescriptionTextEdit, true);
            this.LayoutDescriptionTextEdit.Size = new System.Drawing.Size(423, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LayoutDescriptionTextEdit, optionsSpelling1);
            this.LayoutDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.LayoutDescriptionTextEdit.TabIndex = 5;
            // 
            // LayoutRemarksMemoEdit
            // 
            this.LayoutRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00088edituserscreenlayoutBindingSource, "LayoutRemarks", true));
            this.LayoutRemarksMemoEdit.Location = new System.Drawing.Point(108, 64);
            this.LayoutRemarksMemoEdit.MenuManager = this.barManager1;
            this.LayoutRemarksMemoEdit.Name = "LayoutRemarksMemoEdit";
            this.LayoutRemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LayoutRemarksMemoEdit, true);
            this.LayoutRemarksMemoEdit.Size = new System.Drawing.Size(423, 154);
            this.scSpellChecker.SetSpellCheckerOptions(this.LayoutRemarksMemoEdit, optionsSpelling2);
            this.LayoutRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.LayoutRemarksMemoEdit.TabIndex = 6;
            // 
            // DefaultLayoutCheckEdit
            // 
            this.DefaultLayoutCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00088edituserscreenlayoutBindingSource, "DefaultLayout", true));
            this.DefaultLayoutCheckEdit.Location = new System.Drawing.Point(24, 262);
            this.DefaultLayoutCheckEdit.MenuManager = this.barManager1;
            this.DefaultLayoutCheckEdit.Name = "DefaultLayoutCheckEdit";
            this.DefaultLayoutCheckEdit.Properties.Caption = "Set Layout as Default";
            this.DefaultLayoutCheckEdit.Properties.ValueChecked = 1;
            this.DefaultLayoutCheckEdit.Properties.ValueUnchecked = 0;
            this.DefaultLayoutCheckEdit.Size = new System.Drawing.Size(495, 19);
            this.DefaultLayoutCheckEdit.StyleController = this.dataLayoutControl1;
            this.DefaultLayoutCheckEdit.TabIndex = 8;
            this.DefaultLayoutCheckEdit.TabStop = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLayoutDescription,
            this.ItemForLayoutRemarks,
            this.layoutControlGroup2,
            this.splitterItem1,
            this.layoutControlItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.layoutControlGroup1.Size = new System.Drawing.Size(543, 399);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // ItemForLayoutDescription
            // 
            this.ItemForLayoutDescription.AllowHtmlStringInCaption = true;
            this.ItemForLayoutDescription.Control = this.LayoutDescriptionTextEdit;
            this.ItemForLayoutDescription.CustomizationFormText = "Layout Description:";
            this.ItemForLayoutDescription.Location = new System.Drawing.Point(0, 28);
            this.ItemForLayoutDescription.Name = "ItemForLayoutDescription";
            this.ItemForLayoutDescription.Size = new System.Drawing.Size(523, 24);
            this.ItemForLayoutDescription.Text = "Layout Description:";
            this.ItemForLayoutDescription.TextSize = new System.Drawing.Size(93, 13);
            // 
            // ItemForLayoutRemarks
            // 
            this.ItemForLayoutRemarks.AllowHtmlStringInCaption = true;
            this.ItemForLayoutRemarks.Control = this.LayoutRemarksMemoEdit;
            this.ItemForLayoutRemarks.CustomizationFormText = "Layout Remarks:";
            this.ItemForLayoutRemarks.Location = new System.Drawing.Point(0, 52);
            this.ItemForLayoutRemarks.Name = "ItemForLayoutRemarks";
            this.ItemForLayoutRemarks.Size = new System.Drawing.Size(523, 158);
            this.ItemForLayoutRemarks.Text = "Layout Remarks:";
            this.ItemForLayoutRemarks.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Default";
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDefaultLayout,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 216);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(523, 163);
            this.layoutControlGroup2.Text = "Default";
            // 
            // ItemForDefaultLayout
            // 
            this.ItemForDefaultLayout.Control = this.DefaultLayoutCheckEdit;
            this.ItemForDefaultLayout.CustomizationFormText = "Set Layout as Default:";
            this.ItemForDefaultLayout.Location = new System.Drawing.Point(0, 0);
            this.ItemForDefaultLayout.Name = "ItemForDefaultLayout";
            this.ItemForDefaultLayout.Size = new System.Drawing.Size(499, 23);
            this.ItemForDefaultLayout.Text = "Set Layout as Default:";
            this.ItemForDefaultLayout.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForDefaultLayout.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 23);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(499, 94);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 210);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(523, 6);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(157, 28);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(157, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(366, 28);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00088_edit_user_screen_layoutTableAdapter
            // 
            this.sp00088_edit_user_screen_layoutTableAdapter.ClearBeforeFill = true;
            // 
            // frmUserScreenSettingsEditLayout
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(543, 399);
            this.ControlBox = false;
            this.Controls.Add(this.dataLayoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUserScreenSettingsEditLayout";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Screen Layout";
            this.Load += new System.EventHandler(this.frmUserScreenSettingsEditLayout_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Groups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00088edituserscreenlayoutBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultLayoutCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLayoutDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLayoutRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private DataSet_Groups dataSet_Groups;
        private System.Windows.Forms.BindingSource sp00088edituserscreenlayoutBindingSource;
        private DevExpress.XtraEditors.TextEdit LayoutDescriptionTextEdit;
        private DevExpress.XtraEditors.MemoEdit LayoutRemarksMemoEdit;
        private DevExpress.XtraEditors.CheckEdit DefaultLayoutCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLayoutDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLayoutRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultLayout;
        private WoodPlan5.DataSet_GroupsTableAdapters.sp00088_edit_user_screen_layoutTableAdapter sp00088_edit_user_screen_layoutTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;

    }
}

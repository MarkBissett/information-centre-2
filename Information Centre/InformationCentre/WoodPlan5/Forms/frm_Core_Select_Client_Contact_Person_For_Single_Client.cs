using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Select_Client_Contact_Person_For_Single_Client : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //
        public int intOriginalClientContactID = 0;
        public string strOriginalClientContactIDs = "";
        public int intSelectedClientContactID = 0;
        public string strPassedInClientIDsFilter = "";
        public string strSelectedClientContactIDs = "";
        public string strSelectedPersonName = "";
        public string strSelectedTitle = "";
        public string strSelectedPosition = "";
        public string strSelectedTelephone = "";
        public string strSelectedMobile = "";
        public string strSelectedEmail = "";
        public int _SelectedCount = 0;
        BaseObjects.GridCheckMarksSelection selection1;
       
        #endregion

        public frm_Core_Select_Client_Contact_Person_For_Single_Client()
        {
            InitializeComponent();
        }

        private void frm_Core_Select_Client_Contact_Person_For_Single_Client_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500280;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp01004_GC_Client_Contact_PersonTableAdapter.Connection.ConnectionString = strConnectionString;
            GridView view = (GridView)gridControl1.MainView;

            LoadData();
            gridControl1.ForceInitialize();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.Width = 30;
                selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                selection1.CheckMarkColumn.VisibleIndex = 0;

                Array arrayRecords = strOriginalClientContactIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["ClientContactPersonID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalClientContactID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["ClientContactPersonID"], intOriginalClientContactID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01004_GC_Client_Contact_PersonTableAdapter.Fill(dataSet_Common_Functionality.sp01004_GC_Client_Contact_Person, strPassedInClientIDsFilter);            
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Client Contact People Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(strSelectedClientContactIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (intSelectedClientContactID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (_Mode != "single")
            {
                strSelectedClientContactIDs = "";    // Reset any prior values first //
                strSelectedPersonName = "";  // Reset any prior values first //
                string strDescriptor1 = "";
                string strTelephone = "";
                string strMobile = "";
                string strEmail = "";
                if (selection1.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedClientContactIDs += Convert.ToString(view.GetRowCellValue(i, "ClientContactPersonID")) + ",";
                        strDescriptor1 = Convert.ToString(view.GetRowCellValue(i, "PersonName"));
                        strTelephone = Convert.ToString(view.GetRowCellValue(i, "ClientContactTelephone"));
                        strMobile = Convert.ToString(view.GetRowCellValue(i, "ClientContactMobile"));
                        strEmail = Convert.ToString(view.GetRowCellValue(i, "ClientContactEmail"));
                        if (intCount == 0)
                        {
                            strSelectedPersonName = strDescriptor1;
                            strSelectedTelephone = strTelephone;
                            strSelectedMobile = strMobile;
                            strSelectedEmail = strEmail;
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedPersonName += ", " + strDescriptor1;
                            strSelectedTelephone += ", " + strTelephone;
                            strSelectedMobile += ", " + strMobile;
                            strSelectedEmail += ", " + strEmail;
                        }
                        intCount++;
                    }
                }
                _SelectedCount = intCount;
            }
            else  // Single record selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    var currentRowView = (DataRowView)sp01004GCClientContactPersonBindingSource.Current;
                    var currentRow = (DataSet_Common_Functionality.sp01004_GC_Client_Contact_PersonRow)currentRowView.Row;
                    if (currentRow == null) return;
                    intSelectedClientContactID = (string.IsNullOrEmpty(currentRow.ClientContactPersonID.ToString()) ? 0 : currentRow.ClientContactPersonID);
                    strSelectedPersonName = (string.IsNullOrEmpty(currentRow.PersonName.ToString()) ? "" : currentRow.PersonName);
                    strSelectedTitle = (string.IsNullOrEmpty(currentRow.Title.ToString()) ? "" : currentRow.Title);
                    strSelectedPosition = (string.IsNullOrEmpty(currentRow.Position.ToString()) ? "" : currentRow.Position);
                    strSelectedTelephone = (string.IsNullOrEmpty(currentRow.ClientContactTelephone.ToString()) ? "" : currentRow.ClientContactTelephone);
                    strSelectedMobile = (string.IsNullOrEmpty(currentRow.ClientContactMobile.ToString()) ? "" : currentRow.ClientContactMobile);
                    strSelectedEmail = (string.IsNullOrEmpty(currentRow.ClientContactEmail.ToString()) ? "" : currentRow.ClientContactEmail);
                }
            }
        }




    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frmAvailableUsers : BaseObjects.frmBase
    {
        private string strConnectionString = "";
        private Settings set = Settings.Default;
        private string strUserType = "";

        public string strPassword;
        private ArrayList alNames = new ArrayList();
        private Int32 intStaffID;
        private Int32 intSystemID;
        public frmMain2 frmParent;
        ArrayList alDBDetails = new ArrayList();
        public string strServerName = "";
        public string strDatabaseName = "";
        public string strFriendlyName = "";

        public frmAvailableUsers(ArrayList alPassedNames, Int32 intStaff, Int32 intSystem)
        {
            InitializeComponent();

            alNames = alPassedNames;
            intStaffID = intStaff;
            intSystemID = intSystem;

            strConnectionString = set.WoodPlanConnectionString;
            sp00062AvailableUsersListTableAdapter.Connection.ConnectionString = strConnectionString;
        }

        private void frmAvailableUsers_Load(object sender, EventArgs e)
        {
            Set_Grid_Highlighter_Transparent(this.Controls);

            sp00062AvailableUsersListTableAdapter.Fill(this.woodPlanDataSet.sp00062AvailableUsersList, strUserType);
        }

        public string UserType
        {
            get
            {
                return strUserType;
            }
            set
            {
                strUserType = value;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gvMain = (DevExpress.XtraGrid.Views.Grid.GridView)gcAvailableUsers.MainView;

            // We don't have a password so we need to use Windows Login credentials for connection to DB //
            strConnectionString = "data source=" + set.ServerName + ";" +
                                      "initial catalog=" + set.DatabaseName + ";" +
                                      "integrated security=SSPI;" +
                                      "persist security info=False;" +
                                      "Trusted_Connection=Yes";

            set.UserName = gvMain.GetFocusedRowCellValue("NetworkID").ToString();
            set.WoodPlanConnectionString = strConnectionString;
            set.Save();

            // Change the logged in user...
            intStaffID = Convert.ToInt32(gvMain.GetFocusedRowCellValue("StaffID"));
            //strNetworkID = gvMain.GetFocusedRowCellValue("NetworkID").ToString();
            strUserType = gvMain.GetFocusedRowCellValue("UserType").ToString();

            StartInitialisation();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void gvAvailableUsers_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gvMain = (DevExpress.XtraGrid.Views.Grid.GridView)sender;

            try
            {
                if (gvMain.GetRowCellValue(e.RowHandle, "UserType").ToString().ToUpper() == "SUPER")
                {
                    e.Appearance.ForeColor = Color.FromArgb(255, 0, 0);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void StartInitialisation()
        {
            stcLinkItemIDs sliIDs;
            WoodPlan5.WoodPlanDataSet.sp00001PopulateSwitchboardDataTable dtSwitchBoardTable = new WoodPlan5.WoodPlanDataSet.sp00001PopulateSwitchboardDataTable();
            string strBitmap;
            //string strConnectionString;
            int i = 0;
            DevExpress.XtraNavBar.NavBarGroup nbgSwitchboardGroup;
            DevExpress.XtraNavBar.NavBarGroup nbgReportGroup;
            DevExpress.XtraNavBar.NavBarItem nbiItem;
            ArrayList alGroupIDs = new ArrayList();
            int intIndex;

            frmParent.nbcSwitchboard.Groups.Clear();
            frmParent.nbcReports.Groups.Clear();

            foreach (stcItems sItems in alNames)
            {
                nbgSwitchboardGroup = new DevExpress.XtraNavBar.NavBarGroup(sItems.strItemName);
                nbgSwitchboardGroup.Tag = sItems.iItemID;
                switch (sItems.strItemName)
                {
                    case "Amenity Trees":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[0];
                        break;
                    case "Asset Management":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[1];
                        break;
                    case "Winter Maintenance":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[2];
                        break;
                    case "Tender Register":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[6];
                        break;
                    case "CRM":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[7];
                        break;
                    case "System Administration":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[3];
                        break;
                    case "Summer Maintenance":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[8];
                        break;
                    case "Utility Arb":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[9];
                        break;
                    case "HR":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[10];
                        break;
                    case "Equipment":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[11];
                        break;
                    case "Core":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[12];
                        break;
                    default:
                        break;
                }

                nbgReportGroup = new DevExpress.XtraNavBar.NavBarGroup(sItems.strItemName);
                nbgReportGroup.Tag = sItems.iItemID;
                switch (sItems.strItemName)
                {
                    case "Amenity Trees":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[0];
                        break;
                    case "Asset Management":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[1];
                        break;
                    case "Winter Maintenance":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[2];
                        break;
                    case "Tender Register":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[6];
                        break;
                    case "CRM":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[7];
                        break;
                    case "Summer Maintenance":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[8];
                        break;
                    case "Utility Arb":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[9];
                        break;
                    case "HR":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[10];
                        break;
                    case "Equipment":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[11];
                        break;
                    case "Core":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[12];
                        break;
                    default:
                        break;
                }

                frmParent.nbcSwitchboard.Groups.Add(nbgSwitchboardGroup);
                frmParent.nbcReports.Groups.Add(nbgReportGroup);
                this.Refresh();
            }

            nbgSwitchboardGroup = new DevExpress.XtraNavBar.NavBarGroup("My Options");
            nbgSwitchboardGroup.Name = "My Options";
            nbgSwitchboardGroup.Tag = 9998;
            nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[4];

            frmParent.nbcSwitchboard.Groups.Add(nbgSwitchboardGroup);

            nbgSwitchboardGroup = new DevExpress.XtraNavBar.NavBarGroup("Favourites");
            nbgSwitchboardGroup.Tag = -1;
            nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[5];

            frmParent.nbcSwitchboard.Groups.Add(nbgSwitchboardGroup);

            nbgReportGroup = new DevExpress.XtraNavBar.NavBarGroup("Favourites");
            nbgReportGroup.Tag = -1;
            nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[5];

            frmParent.nbcReports.Groups.Add(nbgReportGroup);

            sp00001PopulateSwitchboardTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00001PopulateSwitchboardTableAdapter.Fill(dtSwitchBoardTable, intStaffID);

            foreach (DataRow row in dtSwitchBoardTable.Rows)
            {
                if (Convert.ToInt32(row["Type"]) == 2)  // Screen //
                {
                    nbiItem = new DevExpress.XtraNavBar.NavBarItem(row["PartDescription"].ToString());

                    for (i = 0; i < frmParent.nbcSwitchboard.Groups.Count; i++)
                    {
                        // Compare our Group ID from the DB with the one pulled in via the interfaces...
                        if ((int)frmParent.nbcSwitchboard.Groups[i].Tag == (int)row["GroupID"])
                        {
                            nbiItem = new DevExpress.XtraNavBar.NavBarItem(row["PartDescription"].ToString());

                            sliIDs = new stcLinkItemIDs();
                            sliIDs.intModuleID = Convert.ToInt32(row["ModuleID"]);
                            sliIDs.intFormID = Convert.ToInt32(row["PartID"]);
                            sliIDs.intType = Convert.ToInt32(row["Type"]);
                            nbiItem.Tag = sliIDs;
                            if ((int)frmParent.nbcSwitchboard.Groups[i].Tag == (int)-1)
                            {
                                strBitmap = "13";  // Hardcoded icon //
                            }
                            else
                            {
                                strBitmap = row["ImageName"].ToString();
                            }
                            if (!string.IsNullOrEmpty(strBitmap)) nbiItem.SmallImage = frmParent.imageCollection1.Images[Convert.ToInt32(strBitmap)];
                            frmParent.nbcSwitchboard.Groups[i].ItemLinks.Add(nbiItem);
                            break;
                        }
                        this.Refresh();
                    }
                }
            }
            foreach (DataRow row in dtSwitchBoardTable.Rows)
            {
                if (Convert.ToInt32(row["Type"]) == 3)  // Report //
                {
                    nbiItem = new DevExpress.XtraNavBar.NavBarItem(row["PartDescription"].ToString());

                    for (i = 0; i < frmParent.nbcReports.Groups.Count; i++)
                    {
                        // Compare our Group ID from the DB with the one pulled in via the interfaces...
                        if ((int)frmParent.nbcReports.Groups[i].Tag == (int)row["GroupID"])
                        {
                            nbiItem = new DevExpress.XtraNavBar.NavBarItem(row["PartDescription"].ToString());

                            sliIDs = new stcLinkItemIDs();
                            sliIDs.intModuleID = Convert.ToInt32(row["ModuleID"]);
                            sliIDs.intFormID = Convert.ToInt32(row["PartID"]);
                            sliIDs.intType = Convert.ToInt32(row["Type"]);
                            nbiItem.Tag = sliIDs;
                            if ((int)frmParent.nbcReports.Groups[i].Tag == (int)-1)
                            {
                                strBitmap = "13";  // Hardcoded icon //
                            }
                            else
                            {
                                strBitmap = row["ImageName"].ToString();
                            }
                            if (!string.IsNullOrEmpty(strBitmap)) nbiItem.SmallImage = frmParent.imageCollection1.Images[Convert.ToInt32(strBitmap)];
                            frmParent.nbcReports.Groups[i].ItemLinks.Add(nbiItem);
                            break;
                        }
                        this.Refresh();
                    }
                }
            }

            for (i = 0; i < frmParent.nbcSwitchboard.Groups.Count; i++)
            {
                if (frmParent.nbcSwitchboard.Groups[i].ItemLinks.Count == 0)
                {
                    if (frmParent.nbcSwitchboard.Groups[i].Caption != "Favourites")
                    {
                        alGroupIDs.Add(i);
                    }
                }
            }

            alGroupIDs.Reverse();

            for (i = 0; i < alGroupIDs.Count; i++)
            {
                intIndex = (int)alGroupIDs[i];
                frmParent.nbcSwitchboard.Groups.RemoveAt(intIndex);
            }

            alGroupIDs.Clear();

            for (i = 0; i < frmParent.nbcReports.Groups.Count; i++)
            {
                if (frmParent.nbcReports.Groups[i].ItemLinks.Count == 0)
                {
                    if (frmParent.nbcReports.Groups[i].Caption != "Favourites")
                    {
                        alGroupIDs.Add(i);
                    }
                }
            }

            alGroupIDs.Reverse();

            for (i = 0; i < alGroupIDs.Count; i++)
            {
                intIndex = (int)alGroupIDs[i];
                frmParent.nbcReports.Groups.RemoveAt(intIndex);
            }
        }

        private void gvAvailableUsers_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Users Available");
        }
    }
}


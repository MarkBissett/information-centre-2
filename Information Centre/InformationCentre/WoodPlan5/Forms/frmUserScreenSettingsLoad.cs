using System;
using System.Drawing;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;  // Mouse down Args for GridView Mouse Down //

namespace WoodPlan5
{
    public partial class frmUserScreenSettingsLoad : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;
        private frmBase frmToProcess;
        /// <summary>
        /// 
        /// </summary>
        public int intLayoutID = 0;
        /// <summary>
        /// 
        /// </summary>
        public int intLayoutCreatedByID = 0;
        /// <summary>
        /// 
        /// </summary>
        public RefreshGridState RefreshGridViewState;  // Used by Grid View State Facilities //

        #endregion

        public frmUserScreenSettingsLoad(frmBase activeForm)
        {
            InitializeComponent();
            frmToProcess = activeForm;
        }

        private void frmUserScreenSettingsLoad_Load(object sender, EventArgs e)
        {
            this.Text += "  -  " + frmToProcess.Text;
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            GridView view = (GridView)gridControl1.MainView;
            RefreshGridViewState = new RefreshGridState(view, "LayoutID");
            sp00086_available_user_screen_layoutsTableAdapter.Connection.ConnectionString = strConnectionString;
            LoadData(view, false);
        }

        private void LoadData(GridView view, Boolean boolLoadGridState)
        {
            view.BeginUpdate();
            sp00086_available_user_screen_layoutsTableAdapter.Fill(this.dataSet_Groups.sp00086_available_user_screen_layouts, frmToProcess.FormID, this.GlobalSettings.UserID, this.GlobalSettings.PersonType);
            if (boolLoadGridState) this.RefreshGridViewState.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the layout to be edited before proceeding then try again.", "Edit Screen Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CreatedByID")) != this.GlobalSettings.UserID && this.GlobalSettings.PersonType != 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You can only edit screen layouts which you created!\n\nThe currently selected layout was created by a different user - editing cancelled.", "Edit Screen Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strRecordIDs = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "LayoutID")) + ',';

            this.RefreshGridViewState.SaveViewInfo();  // Store Grid View State //

            frmUserScreenSettingsEditLayout fChildForm = new frmUserScreenSettingsEditLayout();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = strRecordIDs;
            fChildForm.strFormMode = "edit";
            fChildForm.strCaller = "frmUserScreenSettingsLoad";
            fChildForm.intRecordCount = 1;
            fChildForm.boolAllowEdit = true;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.frmToProcess = this.frmToProcess;
            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                LoadData(view, false);  // Changes made on child screen so reload data //
            }
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the layout to be deleted before proceeding then try again.", "Delete Screen Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CreatedByID")) != this.GlobalSettings.UserID && this.GlobalSettings.PersonType != 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You can only delete screen layouts which you created!\n\nThe currently selected layout was created by a different user - deletion cancelled.", "Delete Screen Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strMessage  = "You are about to delete screen layout [" + view.GetRowCellValue(view.FocusedRowHandle, "LayoutDescription").ToString() + "]!\n\nAre you sure you wish to proceed?\n\nIf you proceed this layout will no longer be available.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete Screen Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                string strLayoutIDs = view.GetRowCellValue(view.FocusedRowHandle, "LayoutID").ToString() + ",";

                DataSet_GroupsTableAdapters.QueriesTableAdapter RemoveLayout = new DataSet_GroupsTableAdapters.QueriesTableAdapter();
                RemoveLayout.ChangeConnectionString(strConnectionString);
                gridControl1.BeginUpdate();
                RemoveLayout.sp00087_delete_user_screen_layout(strLayoutIDs);  // Delete record from back end //
                view.DeleteRow(view.FocusedRowHandle);  // Delete record from grid //
                gridControl1.EndUpdate();
                intLayoutID = 0;
                intLayoutCreatedByID = 0;
                if (this.GlobalSettings.ShowConfirmations == 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Screen Layout Deleted.", "Delete Screen Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        #region gridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No saved screen layouts available for this screen");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);

            GridView view = sender as GridView;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bsiAdd.Enabled = false;
                bbiBlockEdit.Enabled = false;
                bbiSave.Enabled = false;
                bsiDataset.Enabled = false;

                int[] intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length <= 0)
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                    bbiDelete.Enabled = false;
                }
                else
                {
                    foreach (int intRowHandle in intRowHandles)
                    {
                        intLayoutID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "LayoutID"));
                        intLayoutCreatedByID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByID"));
                    }
                    if (intLayoutCreatedByID == this.GlobalSettings.UserID || this.GlobalSettings.PersonType == 0)  // Current User is creator of layout or super user //
                    {
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (GlobalSettings.DeleteScreenLayout)
                        {
                            bbiDelete.Enabled = true;
                        }
                        else
                        {
                            bbiDelete.Enabled = false;
                        }
                    }
                    else
                    {
                        bsiEdit.Enabled = false;
                        bbiSingleEdit.Enabled = false;
                        bbiDelete.Enabled = false;
                    }
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

            // ***** Stop Group Row Being Selected ***** //
            if (hitInfo.HitTest == GridHitTest.RowGroupButton)
            {
                if (view.GetRowExpanded(hitInfo.RowHandle))
                    view.CollapseGroupRow(hitInfo.RowHandle);
                else
                    view.ExpandGroupRow(hitInfo.RowHandle);

                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
            }
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Screen Layout to load from the list before proceeding!", "Load Screen Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intLayoutID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LayoutID"));
            intLayoutCreatedByID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CreatedByID"));

            // Load the Layout //


            frmToProcess.LoadedViewID = intLayoutID;
            frmToProcess.LoadedViewCreatedByID = intLayoutCreatedByID;
            Load_Saved_Layout LoadSaved = new Load_Saved_Layout();
            LoadSaved.Load_User_Specified_Screen_Layout(strConnectionString, frmToProcess, intLayoutID);

            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails validation test earlier in this process //
            this.Close();
        }



    }
}


namespace WoodPlan5
{
    partial class frm_Core_Contractor_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Contractor_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SupplierCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp00183ContractorItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.NoSelfBillingCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsTestCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.OMPaidDiscountedVisitRateCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.BusinessAreaNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.BusinessAreaIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RegionNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.RegionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.IsWindowCleaningCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsRoofingCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsFencingCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsConstructionCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsPestControlCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsRailArbCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.UniqueTaxRefTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LegalStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00244CoreLegalStatusesPicklistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IsOperationsTeamCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsUtilityArbTeamCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsVatRegisteredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsSnowClearerCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsWoodPlanVisibleCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.IsGritterCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TextNumberMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.LongitudeSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LatitudeSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.intContractorIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.strCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strAddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strAddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strAddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strAddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strAddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strTel1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strTel2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strMobileTelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strEmailPasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strWebsiteTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strVatRegTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.bDisabledCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.InternalContractorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.intTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00186ContractorTypeListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IsPotholeTeamCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SpecialistContractorCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.joinedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.approvedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.terminationDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ItemForintContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBusinessAreaID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInternalContractor = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLegalStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsVatRegistered = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForUniqueTaxRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrVatReg = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForIsGritter = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsSnowClearer = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsWoodPlanVisisble = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsUtilityArbTeam = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsRailArb = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsPestControl = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsFencing = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsRoofing = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsWindowCleaning = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsOperationsTeam = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsConstruction = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsPotholeTeam = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSpecialistContractor = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRegionName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBusinessAreaName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJoinedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForApprovedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTerminationDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.ItemForNoSelfBilling = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemforSupplierCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForOMPaidDiscountedVisitRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrTel1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTextNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForstrTel2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrMobileTel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrWebsite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrEmailPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForbDisabled = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsTest = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.sp00183_Contractor_ItemTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00183_Contractor_ItemTableAdapter();
            this.sp00186_Contractor_Type_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00186_Contractor_Type_List_With_BlankTableAdapter();
            this.sp00244_Core_Legal_Statuses_PicklistTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00244_Core_Legal_Statuses_PicklistTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SupplierCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00183ContractorItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoSelfBillingCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsTestCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OMPaidDiscountedVisitRateCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsWindowCleaningCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsRoofingCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFencingCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsConstructionCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsPestControlCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsRailArbCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UniqueTaxRefTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LegalStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00244CoreLegalStatusesPicklistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsOperationsTeamCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsUtilityArbTeamCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsVatRegisteredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsSnowClearerCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsWoodPlanVisibleCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsGritterCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextNumberMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intContractorIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTel1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTel2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strMobileTelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmailPasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strWebsiteTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strVatRegTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bDisabledCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternalContractorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00186ContractorTypeListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsPotholeTeamCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialistContractorCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.joinedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.joinedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.approvedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.approvedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.terminationDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.terminationDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInternalContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLegalStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsVatRegistered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUniqueTaxRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrVatReg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsGritter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsSnowClearer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsWoodPlanVisisble)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsUtilityArbTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsRailArb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsPestControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsFencing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsRoofing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsWindowCleaning)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsOperationsTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsConstruction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsPotholeTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpecialistContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJoinedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApprovedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTerminationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoSelfBilling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforSupplierCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOMPaidDiscountedVisitRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTextNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrMobileTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrWebsite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmailPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbDisabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1101, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 706);
            this.barDockControlBottom.Size = new System.Drawing.Size(1101, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 680);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1101, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 680);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "ID";
            this.gridColumn2.FieldName = "ID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 59;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Legal Status";
            this.gridColumn1.FieldName = "Description";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 229;
            // 
            // colID
            // 
            this.colID.Caption = "Type ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.Width = 59;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1101, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 706);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1101, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 680);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1101, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 680);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SupplierCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NoSelfBillingCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsTestCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.OMPaidDiscountedVisitRateCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.BusinessAreaNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.BusinessAreaIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.IsWindowCleaningCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsRoofingCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsFencingCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsConstructionCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsPestControlCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsRailArbCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.UniqueTaxRefTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LegalStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.IsOperationsTeamCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsUtilityArbTeamCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsVatRegisteredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsSnowClearerCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsWoodPlanVisibleCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.IsGritterCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TextNumberMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.LongitudeSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LatitudeSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intContractorIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.strCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strAddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strAddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strAddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strAddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strAddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strPostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strTel1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strTel2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strMobileTelTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strEmailPasswordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strWebsiteTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strVatRegTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.bDisabledCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.InternalContractorCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.intTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.IsPotholeTeamCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SpecialistContractorCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.joinedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.approvedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.terminationDateDateEdit);
            this.dataLayoutControl1.DataSource = this.sp00183ContractorItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintContractorID,
            this.ItemForRegionID,
            this.ItemForBusinessAreaID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(10, 64, 504, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1101, 680);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SupplierCodeTextEdit
            // 
            this.SupplierCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "SupplierCode", true));
            this.SupplierCodeTextEdit.Location = new System.Drawing.Point(142, 413);
            this.SupplierCodeTextEdit.MenuManager = this.barManager1;
            this.SupplierCodeTextEdit.Name = "SupplierCodeTextEdit";
            this.SupplierCodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SupplierCodeTextEdit, true);
            this.SupplierCodeTextEdit.Size = new System.Drawing.Size(483, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SupplierCodeTextEdit, optionsSpelling1);
            this.SupplierCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.SupplierCodeTextEdit.TabIndex = 13;
            // 
            // sp00183ContractorItemBindingSource
            // 
            this.sp00183ContractorItemBindingSource.DataMember = "sp00183_Contractor_Item";
            this.sp00183ContractorItemBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // NoSelfBillingCheckEdit
            // 
            this.NoSelfBillingCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "NoSelfBilling", true));
            this.NoSelfBillingCheckEdit.Location = new System.Drawing.Point(142, 390);
            this.NoSelfBillingCheckEdit.MenuManager = this.barManager1;
            this.NoSelfBillingCheckEdit.Name = "NoSelfBillingCheckEdit";
            this.NoSelfBillingCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.NoSelfBillingCheckEdit.Properties.ValueChecked = 1;
            this.NoSelfBillingCheckEdit.Properties.ValueUnchecked = 0;
            this.NoSelfBillingCheckEdit.Size = new System.Drawing.Size(88, 19);
            this.NoSelfBillingCheckEdit.StyleController = this.dataLayoutControl1;
            this.NoSelfBillingCheckEdit.TabIndex = 34;
            // 
            // IsTestCheckEdit
            // 
            this.IsTestCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsTest", true));
            this.IsTestCheckEdit.Location = new System.Drawing.Point(118, 165);
            this.IsTestCheckEdit.MenuManager = this.barManager1;
            this.IsTestCheckEdit.Name = "IsTestCheckEdit";
            this.IsTestCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsTestCheckEdit.Properties.ValueChecked = 1;
            this.IsTestCheckEdit.Properties.ValueUnchecked = 0;
            this.IsTestCheckEdit.Size = new System.Drawing.Size(139, 19);
            this.IsTestCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsTestCheckEdit.TabIndex = 33;
            // 
            // OMPaidDiscountedVisitRateCheckEdit
            // 
            this.OMPaidDiscountedVisitRateCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "OMPaidDiscountedVisitRate", true));
            this.OMPaidDiscountedVisitRateCheckEdit.Location = new System.Drawing.Point(169, 268);
            this.OMPaidDiscountedVisitRateCheckEdit.MenuManager = this.barManager1;
            this.OMPaidDiscountedVisitRateCheckEdit.Name = "OMPaidDiscountedVisitRateCheckEdit";
            this.OMPaidDiscountedVisitRateCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.OMPaidDiscountedVisitRateCheckEdit.Properties.ValueChecked = 1;
            this.OMPaidDiscountedVisitRateCheckEdit.Properties.ValueUnchecked = 0;
            this.OMPaidDiscountedVisitRateCheckEdit.Size = new System.Drawing.Size(95, 19);
            this.OMPaidDiscountedVisitRateCheckEdit.StyleController = this.dataLayoutControl1;
            this.OMPaidDiscountedVisitRateCheckEdit.TabIndex = 32;
            this.OMPaidDiscountedVisitRateCheckEdit.ToolTip = "Usually only relevent to DE teams. Discount % set on Site Contract Rates.";
            // 
            // BusinessAreaNameButtonEdit
            // 
            this.BusinessAreaNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "BusinessAreaName", true));
            this.BusinessAreaNameButtonEdit.Location = new System.Drawing.Point(142, 268);
            this.BusinessAreaNameButtonEdit.MenuManager = this.barManager1;
            this.BusinessAreaNameButtonEdit.Name = "BusinessAreaNameButtonEdit";
            this.BusinessAreaNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Business Area screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Clear the Business Area.", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.BusinessAreaNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.BusinessAreaNameButtonEdit.Size = new System.Drawing.Size(483, 20);
            this.BusinessAreaNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.BusinessAreaNameButtonEdit.TabIndex = 31;
            this.BusinessAreaNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BusinessAreaNameButtonEdit_ButtonClick);
            // 
            // BusinessAreaIDTextEdit
            // 
            this.BusinessAreaIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "BusinessAreaID", true));
            this.BusinessAreaIDTextEdit.Location = new System.Drawing.Point(136, 245);
            this.BusinessAreaIDTextEdit.MenuManager = this.barManager1;
            this.BusinessAreaIDTextEdit.Name = "BusinessAreaIDTextEdit";
            this.BusinessAreaIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BusinessAreaIDTextEdit, true);
            this.BusinessAreaIDTextEdit.Size = new System.Drawing.Size(448, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BusinessAreaIDTextEdit, optionsSpelling2);
            this.BusinessAreaIDTextEdit.StyleController = this.dataLayoutControl1;
            this.BusinessAreaIDTextEdit.TabIndex = 31;
            // 
            // RegionNameButtonEdit
            // 
            this.RegionNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "RegionName", true));
            this.RegionNameButtonEdit.Location = new System.Drawing.Point(142, 292);
            this.RegionNameButtonEdit.MenuManager = this.barManager1;
            this.RegionNameButtonEdit.Name = "RegionNameButtonEdit";
            this.RegionNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to open the Select Region screen.", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click me to Clear the Region.", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.RegionNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.RegionNameButtonEdit.Size = new System.Drawing.Size(483, 20);
            this.RegionNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.RegionNameButtonEdit.TabIndex = 30;
            this.RegionNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.RegionNameButtonEdit_ButtonClick);
            // 
            // RegionIDTextEdit
            // 
            this.RegionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "RegionID", true));
            this.RegionIDTextEdit.Location = new System.Drawing.Point(112, 119);
            this.RegionIDTextEdit.MenuManager = this.barManager1;
            this.RegionIDTextEdit.Name = "RegionIDTextEdit";
            this.RegionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegionIDTextEdit, true);
            this.RegionIDTextEdit.Size = new System.Drawing.Size(694, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegionIDTextEdit, optionsSpelling3);
            this.RegionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionIDTextEdit.TabIndex = 29;
            // 
            // IsWindowCleaningCheckEdit
            // 
            this.IsWindowCleaningCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsWindowCleaning", true));
            this.IsWindowCleaningCheckEdit.Location = new System.Drawing.Point(753, 565);
            this.IsWindowCleaningCheckEdit.MenuManager = this.barManager1;
            this.IsWindowCleaningCheckEdit.Name = "IsWindowCleaningCheckEdit";
            this.IsWindowCleaningCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsWindowCleaningCheckEdit.Properties.ValueChecked = 1;
            this.IsWindowCleaningCheckEdit.Properties.ValueUnchecked = 0;
            this.IsWindowCleaningCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsWindowCleaningCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsWindowCleaningCheckEdit.TabIndex = 14;
            this.IsWindowCleaningCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsWindowCleaningCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // IsRoofingCheckEdit
            // 
            this.IsRoofingCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsRoofing", true));
            this.IsRoofingCheckEdit.Location = new System.Drawing.Point(753, 542);
            this.IsRoofingCheckEdit.MenuManager = this.barManager1;
            this.IsRoofingCheckEdit.Name = "IsRoofingCheckEdit";
            this.IsRoofingCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsRoofingCheckEdit.Properties.ValueChecked = 1;
            this.IsRoofingCheckEdit.Properties.ValueUnchecked = 0;
            this.IsRoofingCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsRoofingCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsRoofingCheckEdit.TabIndex = 14;
            this.IsRoofingCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsRoofingCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // IsFencingCheckEdit
            // 
            this.IsFencingCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsFencing", true));
            this.IsFencingCheckEdit.Location = new System.Drawing.Point(753, 519);
            this.IsFencingCheckEdit.MenuManager = this.barManager1;
            this.IsFencingCheckEdit.Name = "IsFencingCheckEdit";
            this.IsFencingCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsFencingCheckEdit.Properties.ValueChecked = 1;
            this.IsFencingCheckEdit.Properties.ValueUnchecked = 0;
            this.IsFencingCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsFencingCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsFencingCheckEdit.TabIndex = 14;
            this.IsFencingCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsFencingCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // IsConstructionCheckEdit
            // 
            this.IsConstructionCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsConstruction", true));
            this.IsConstructionCheckEdit.Location = new System.Drawing.Point(753, 404);
            this.IsConstructionCheckEdit.MenuManager = this.barManager1;
            this.IsConstructionCheckEdit.Name = "IsConstructionCheckEdit";
            this.IsConstructionCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsConstructionCheckEdit.Properties.ValueChecked = 1;
            this.IsConstructionCheckEdit.Properties.ValueUnchecked = 0;
            this.IsConstructionCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsConstructionCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsConstructionCheckEdit.TabIndex = 14;
            this.IsConstructionCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsConstructionCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // IsPestControlCheckEdit
            // 
            this.IsPestControlCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsPestControl", true));
            this.IsPestControlCheckEdit.Location = new System.Drawing.Point(753, 496);
            this.IsPestControlCheckEdit.MenuManager = this.barManager1;
            this.IsPestControlCheckEdit.Name = "IsPestControlCheckEdit";
            this.IsPestControlCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsPestControlCheckEdit.Properties.ValueChecked = 1;
            this.IsPestControlCheckEdit.Properties.ValueUnchecked = 0;
            this.IsPestControlCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsPestControlCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsPestControlCheckEdit.TabIndex = 14;
            this.IsPestControlCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsPestControlCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // IsRailArbCheckEdit
            // 
            this.IsRailArbCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsRailArb", true));
            this.IsRailArbCheckEdit.Location = new System.Drawing.Point(753, 473);
            this.IsRailArbCheckEdit.MenuManager = this.barManager1;
            this.IsRailArbCheckEdit.Name = "IsRailArbCheckEdit";
            this.IsRailArbCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsRailArbCheckEdit.Properties.ValueChecked = 1;
            this.IsRailArbCheckEdit.Properties.ValueUnchecked = 0;
            this.IsRailArbCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsRailArbCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsRailArbCheckEdit.TabIndex = 13;
            this.IsRailArbCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsRailArbCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // UniqueTaxRefTextEdit
            // 
            this.UniqueTaxRefTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "UniqueTaxRef", true));
            this.UniqueTaxRefTextEdit.Location = new System.Drawing.Point(142, 366);
            this.UniqueTaxRefTextEdit.MenuManager = this.barManager1;
            this.UniqueTaxRefTextEdit.Name = "UniqueTaxRefTextEdit";
            this.UniqueTaxRefTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.UniqueTaxRefTextEdit, true);
            this.UniqueTaxRefTextEdit.Size = new System.Drawing.Size(483, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.UniqueTaxRefTextEdit, optionsSpelling4);
            this.UniqueTaxRefTextEdit.StyleController = this.dataLayoutControl1;
            this.UniqueTaxRefTextEdit.TabIndex = 27;
            // 
            // LegalStatusIDGridLookUpEdit
            // 
            this.LegalStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "LegalStatusID", true));
            this.LegalStatusIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LegalStatusIDGridLookUpEdit.Location = new System.Drawing.Point(142, 316);
            this.LegalStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.LegalStatusIDGridLookUpEdit.Name = "LegalStatusIDGridLookUpEdit";
            editorButtonImageOptions5.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions6.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.LegalStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Edit underlying data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Reload underlying data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LegalStatusIDGridLookUpEdit.Properties.DataSource = this.sp00244CoreLegalStatusesPicklistBindingSource;
            this.LegalStatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.LegalStatusIDGridLookUpEdit.Properties.NullText = "";
            this.LegalStatusIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.LegalStatusIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.LegalStatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.LegalStatusIDGridLookUpEdit.Size = new System.Drawing.Size(483, 22);
            this.LegalStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.LegalStatusIDGridLookUpEdit.TabIndex = 13;
            this.LegalStatusIDGridLookUpEdit.TabStop = false;
            this.LegalStatusIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LegalStatusIDGridLookUpEdit_ButtonClick);
            // 
            // sp00244CoreLegalStatusesPicklistBindingSource
            // 
            this.sp00244CoreLegalStatusesPicklistBindingSource.DataMember = "sp00244_Core_Legal_Statuses_Picklist";
            this.sp00244CoreLegalStatusesPicklistBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn2;
            gridFormatRule1.ColumnApplyTo = this.gridColumn1;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 99;
            // 
            // IsOperationsTeamCheckEdit
            // 
            this.IsOperationsTeamCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsOperationsTeam", true));
            this.IsOperationsTeamCheckEdit.Location = new System.Drawing.Point(753, 381);
            this.IsOperationsTeamCheckEdit.MenuManager = this.barManager1;
            this.IsOperationsTeamCheckEdit.Name = "IsOperationsTeamCheckEdit";
            this.IsOperationsTeamCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.IsOperationsTeamCheckEdit.Properties.ValueChecked = 1;
            this.IsOperationsTeamCheckEdit.Properties.ValueUnchecked = 0;
            this.IsOperationsTeamCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsOperationsTeamCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsOperationsTeamCheckEdit.TabIndex = 5;
            this.IsOperationsTeamCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsOperationsTeamCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // IsUtilityArbTeamCheckEdit
            // 
            this.IsUtilityArbTeamCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsUtilityArbTeam", true));
            this.IsUtilityArbTeamCheckEdit.Location = new System.Drawing.Point(753, 450);
            this.IsUtilityArbTeamCheckEdit.MenuManager = this.barManager1;
            this.IsUtilityArbTeamCheckEdit.Name = "IsUtilityArbTeamCheckEdit";
            this.IsUtilityArbTeamCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsUtilityArbTeamCheckEdit.Properties.ValueChecked = 1;
            this.IsUtilityArbTeamCheckEdit.Properties.ValueUnchecked = 0;
            this.IsUtilityArbTeamCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsUtilityArbTeamCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsUtilityArbTeamCheckEdit.TabIndex = 9;
            this.IsUtilityArbTeamCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsUtilityArbTeamCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // IsVatRegisteredCheckEdit
            // 
            this.IsVatRegisteredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsVatRegistered", true));
            this.IsVatRegisteredCheckEdit.Location = new System.Drawing.Point(142, 342);
            this.IsVatRegisteredCheckEdit.MenuManager = this.barManager1;
            this.IsVatRegisteredCheckEdit.Name = "IsVatRegisteredCheckEdit";
            this.IsVatRegisteredCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsVatRegisteredCheckEdit.Properties.ValueChecked = 1;
            this.IsVatRegisteredCheckEdit.Properties.ValueUnchecked = 0;
            this.IsVatRegisteredCheckEdit.Size = new System.Drawing.Size(128, 19);
            this.IsVatRegisteredCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsVatRegisteredCheckEdit.TabIndex = 22;
            this.IsVatRegisteredCheckEdit.Validated += new System.EventHandler(this.IsVatRegisteredCheckEdit_Validated);
            // 
            // IsSnowClearerCheckEdit
            // 
            this.IsSnowClearerCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsSnowClearer", true));
            this.IsSnowClearerCheckEdit.Location = new System.Drawing.Point(753, 358);
            this.IsSnowClearerCheckEdit.MenuManager = this.barManager1;
            this.IsSnowClearerCheckEdit.Name = "IsSnowClearerCheckEdit";
            this.IsSnowClearerCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsSnowClearerCheckEdit.Properties.ValueChecked = 1;
            this.IsSnowClearerCheckEdit.Properties.ValueUnchecked = 0;
            this.IsSnowClearerCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsSnowClearerCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsSnowClearerCheckEdit.TabIndex = 7;
            this.IsSnowClearerCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsSnowClearerCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // IsWoodPlanVisibleCheckEdit
            // 
            this.IsWoodPlanVisibleCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsWoodPlanVisible", true));
            this.IsWoodPlanVisibleCheckEdit.Location = new System.Drawing.Point(753, 427);
            this.IsWoodPlanVisibleCheckEdit.MenuManager = this.barManager1;
            this.IsWoodPlanVisibleCheckEdit.Name = "IsWoodPlanVisibleCheckEdit";
            this.IsWoodPlanVisibleCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsWoodPlanVisibleCheckEdit.Properties.ValueChecked = 1;
            this.IsWoodPlanVisibleCheckEdit.Properties.ValueUnchecked = 0;
            this.IsWoodPlanVisibleCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsWoodPlanVisibleCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsWoodPlanVisibleCheckEdit.TabIndex = 8;
            this.IsWoodPlanVisibleCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsWoodPlanVisibleCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // IsGritterCheckEdit
            // 
            this.IsGritterCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsGritter", true));
            this.IsGritterCheckEdit.Location = new System.Drawing.Point(753, 335);
            this.IsGritterCheckEdit.MenuManager = this.barManager1;
            this.IsGritterCheckEdit.Name = "IsGritterCheckEdit";
            this.IsGritterCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsGritterCheckEdit.Properties.ValueChecked = 1;
            this.IsGritterCheckEdit.Properties.ValueUnchecked = 0;
            this.IsGritterCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsGritterCheckEdit.StyleController = this.dataLayoutControl1;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Gritter - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.IsGritterCheckEdit.SuperTip = superToolTip4;
            this.IsGritterCheckEdit.TabIndex = 6;
            this.IsGritterCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsGritterCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // TextNumberMemoEdit
            // 
            this.TextNumberMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "TextNumber", true));
            this.TextNumberMemoEdit.Location = new System.Drawing.Point(142, 544);
            this.TextNumberMemoEdit.MenuManager = this.barManager1;
            this.TextNumberMemoEdit.Name = "TextNumberMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.TextNumberMemoEdit, true);
            this.TextNumberMemoEdit.Size = new System.Drawing.Size(906, 60);
            this.scSpellChecker.SetSpellCheckerOptions(this.TextNumberMemoEdit, optionsSpelling5);
            this.TextNumberMemoEdit.StyleController = this.dataLayoutControl1;
            this.TextNumberMemoEdit.TabIndex = 21;
            // 
            // LongitudeSpinEdit
            // 
            this.LongitudeSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "Longitude", true));
            this.LongitudeSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LongitudeSpinEdit.Location = new System.Drawing.Point(142, 471);
            this.LongitudeSpinEdit.MenuManager = this.barManager1;
            this.LongitudeSpinEdit.Name = "LongitudeSpinEdit";
            this.LongitudeSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LongitudeSpinEdit.Properties.ReadOnly = true;
            this.LongitudeSpinEdit.Size = new System.Drawing.Size(128, 20);
            this.LongitudeSpinEdit.StyleController = this.dataLayoutControl1;
            this.LongitudeSpinEdit.TabIndex = 25;
            // 
            // LatitudeSpinEdit
            // 
            this.LatitudeSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "Latitude", true));
            this.LatitudeSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LatitudeSpinEdit.Location = new System.Drawing.Point(142, 447);
            this.LatitudeSpinEdit.MenuManager = this.barManager1;
            this.LatitudeSpinEdit.Name = "LatitudeSpinEdit";
            this.LatitudeSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LatitudeSpinEdit.Properties.ReadOnly = true;
            this.LatitudeSpinEdit.Size = new System.Drawing.Size(128, 20);
            this.LatitudeSpinEdit.StyleController = this.dataLayoutControl1;
            this.LatitudeSpinEdit.TabIndex = 24;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00183ContractorItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(118, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(190, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 26;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // intContractorIDSpinEdit
            // 
            this.intContractorIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "intContractorID", true));
            this.intContractorIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intContractorIDSpinEdit.Location = new System.Drawing.Point(113, 12);
            this.intContractorIDSpinEdit.MenuManager = this.barManager1;
            this.intContractorIDSpinEdit.Name = "intContractorIDSpinEdit";
            this.intContractorIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intContractorIDSpinEdit.Properties.ReadOnly = true;
            this.intContractorIDSpinEdit.Size = new System.Drawing.Size(486, 20);
            this.intContractorIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.intContractorIDSpinEdit.TabIndex = 4;
            // 
            // strCodeTextEdit
            // 
            this.strCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strCode", true));
            this.strCodeTextEdit.Location = new System.Drawing.Point(118, 35);
            this.strCodeTextEdit.MenuManager = this.barManager1;
            this.strCodeTextEdit.Name = "strCodeTextEdit";
            this.strCodeTextEdit.Properties.MaxLength = 15;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strCodeTextEdit, true);
            this.strCodeTextEdit.Size = new System.Drawing.Size(190, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strCodeTextEdit, optionsSpelling6);
            this.strCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.strCodeTextEdit.TabIndex = 0;
            // 
            // strNameTextEdit
            // 
            this.strNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strName", true));
            this.strNameTextEdit.Location = new System.Drawing.Point(118, 59);
            this.strNameTextEdit.MenuManager = this.barManager1;
            this.strNameTextEdit.Name = "strNameTextEdit";
            this.strNameTextEdit.Properties.MaxLength = 50;
            this.strNameTextEdit.Properties.NullValuePrompt = "Enter a Value";
            this.strNameTextEdit.Properties.NullValuePromptShowForEmptyValue = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strNameTextEdit, true);
            this.strNameTextEdit.Size = new System.Drawing.Size(469, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strNameTextEdit, optionsSpelling7);
            this.strNameTextEdit.StyleController = this.dataLayoutControl1;
            this.strNameTextEdit.TabIndex = 1;
            this.strNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strNameTextEdit_Validating);
            // 
            // strAddressLine1TextEdit
            // 
            this.strAddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strAddressLine1", true));
            this.strAddressLine1TextEdit.Location = new System.Drawing.Point(142, 268);
            this.strAddressLine1TextEdit.MenuManager = this.barManager1;
            this.strAddressLine1TextEdit.Name = "strAddressLine1TextEdit";
            this.strAddressLine1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strAddressLine1TextEdit, true);
            this.strAddressLine1TextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strAddressLine1TextEdit, optionsSpelling8);
            this.strAddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.strAddressLine1TextEdit.TabIndex = 10;
            // 
            // strAddressLine2TextEdit
            // 
            this.strAddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strAddressLine2", true));
            this.strAddressLine2TextEdit.Location = new System.Drawing.Point(142, 292);
            this.strAddressLine2TextEdit.MenuManager = this.barManager1;
            this.strAddressLine2TextEdit.Name = "strAddressLine2TextEdit";
            this.strAddressLine2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strAddressLine2TextEdit, true);
            this.strAddressLine2TextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strAddressLine2TextEdit, optionsSpelling9);
            this.strAddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.strAddressLine2TextEdit.TabIndex = 11;
            // 
            // strAddressLine3TextEdit
            // 
            this.strAddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strAddressLine3", true));
            this.strAddressLine3TextEdit.Location = new System.Drawing.Point(142, 316);
            this.strAddressLine3TextEdit.MenuManager = this.barManager1;
            this.strAddressLine3TextEdit.Name = "strAddressLine3TextEdit";
            this.strAddressLine3TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strAddressLine3TextEdit, true);
            this.strAddressLine3TextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strAddressLine3TextEdit, optionsSpelling10);
            this.strAddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.strAddressLine3TextEdit.TabIndex = 12;
            // 
            // strAddressLine4TextEdit
            // 
            this.strAddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strAddressLine4", true));
            this.strAddressLine4TextEdit.Location = new System.Drawing.Point(142, 340);
            this.strAddressLine4TextEdit.MenuManager = this.barManager1;
            this.strAddressLine4TextEdit.Name = "strAddressLine4TextEdit";
            this.strAddressLine4TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strAddressLine4TextEdit, true);
            this.strAddressLine4TextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strAddressLine4TextEdit, optionsSpelling11);
            this.strAddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.strAddressLine4TextEdit.TabIndex = 13;
            // 
            // strAddressLine5TextEdit
            // 
            this.strAddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strAddressLine5", true));
            this.strAddressLine5TextEdit.Location = new System.Drawing.Point(142, 364);
            this.strAddressLine5TextEdit.MenuManager = this.barManager1;
            this.strAddressLine5TextEdit.Name = "strAddressLine5TextEdit";
            this.strAddressLine5TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strAddressLine5TextEdit, true);
            this.strAddressLine5TextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strAddressLine5TextEdit, optionsSpelling12);
            this.strAddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.strAddressLine5TextEdit.TabIndex = 14;
            // 
            // strPostcodeTextEdit
            // 
            this.strPostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strPostcode", true));
            this.strPostcodeTextEdit.Location = new System.Drawing.Point(142, 388);
            this.strPostcodeTextEdit.MenuManager = this.barManager1;
            this.strPostcodeTextEdit.Name = "strPostcodeTextEdit";
            this.strPostcodeTextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strPostcodeTextEdit, true);
            this.strPostcodeTextEdit.Size = new System.Drawing.Size(177, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strPostcodeTextEdit, optionsSpelling13);
            this.strPostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.strPostcodeTextEdit.TabIndex = 15;
            // 
            // strTel1TextEdit
            // 
            this.strTel1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strTel1", true));
            this.strTel1TextEdit.Location = new System.Drawing.Point(142, 424);
            this.strTel1TextEdit.MenuManager = this.barManager1;
            this.strTel1TextEdit.Name = "strTel1TextEdit";
            this.strTel1TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strTel1TextEdit, true);
            this.strTel1TextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strTel1TextEdit, optionsSpelling14);
            this.strTel1TextEdit.StyleController = this.dataLayoutControl1;
            this.strTel1TextEdit.TabIndex = 16;
            // 
            // strTel2TextEdit
            // 
            this.strTel2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strTel2", true));
            this.strTel2TextEdit.Location = new System.Drawing.Point(142, 448);
            this.strTel2TextEdit.MenuManager = this.barManager1;
            this.strTel2TextEdit.Name = "strTel2TextEdit";
            this.strTel2TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strTel2TextEdit, true);
            this.strTel2TextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strTel2TextEdit, optionsSpelling15);
            this.strTel2TextEdit.StyleController = this.dataLayoutControl1;
            this.strTel2TextEdit.TabIndex = 17;
            // 
            // strMobileTelTextEdit
            // 
            this.strMobileTelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strMobileTel", true));
            this.strMobileTelTextEdit.Location = new System.Drawing.Point(142, 472);
            this.strMobileTelTextEdit.MenuManager = this.barManager1;
            this.strMobileTelTextEdit.Name = "strMobileTelTextEdit";
            this.strMobileTelTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strMobileTelTextEdit, true);
            this.strMobileTelTextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strMobileTelTextEdit, optionsSpelling16);
            this.strMobileTelTextEdit.StyleController = this.dataLayoutControl1;
            this.strMobileTelTextEdit.TabIndex = 18;
            // 
            // strEmailPasswordTextEdit
            // 
            this.strEmailPasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strEmailPassword", true));
            this.strEmailPasswordTextEdit.Location = new System.Drawing.Point(142, 520);
            this.strEmailPasswordTextEdit.MenuManager = this.barManager1;
            this.strEmailPasswordTextEdit.Name = "strEmailPasswordTextEdit";
            this.strEmailPasswordTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strEmailPasswordTextEdit, true);
            this.strEmailPasswordTextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strEmailPasswordTextEdit, optionsSpelling17);
            this.strEmailPasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.strEmailPasswordTextEdit.TabIndex = 20;
            // 
            // strWebsiteTextEdit
            // 
            this.strWebsiteTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strWebsite", true));
            this.strWebsiteTextEdit.Location = new System.Drawing.Point(142, 496);
            this.strWebsiteTextEdit.MenuManager = this.barManager1;
            this.strWebsiteTextEdit.Name = "strWebsiteTextEdit";
            this.strWebsiteTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strWebsiteTextEdit, true);
            this.strWebsiteTextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strWebsiteTextEdit, optionsSpelling18);
            this.strWebsiteTextEdit.StyleController = this.dataLayoutControl1;
            this.strWebsiteTextEdit.TabIndex = 19;
            // 
            // strVatRegTextEdit
            // 
            this.strVatRegTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strVatReg", true));
            this.strVatRegTextEdit.Location = new System.Drawing.Point(380, 342);
            this.strVatRegTextEdit.MenuManager = this.barManager1;
            this.strVatRegTextEdit.Name = "strVatRegTextEdit";
            this.strVatRegTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strVatRegTextEdit, true);
            this.strVatRegTextEdit.Size = new System.Drawing.Size(187, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strVatRegTextEdit, optionsSpelling19);
            this.strVatRegTextEdit.StyleController = this.dataLayoutControl1;
            this.strVatRegTextEdit.TabIndex = 23;
            // 
            // strUser1TextEdit
            // 
            this.strUser1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strUser1", true));
            this.strUser1TextEdit.Location = new System.Drawing.Point(142, 495);
            this.strUser1TextEdit.MenuManager = this.barManager1;
            this.strUser1TextEdit.Name = "strUser1TextEdit";
            this.strUser1TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser1TextEdit, true);
            this.strUser1TextEdit.Size = new System.Drawing.Size(483, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser1TextEdit, optionsSpelling20);
            this.strUser1TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser1TextEdit.TabIndex = 26;
            // 
            // strUser2TextEdit
            // 
            this.strUser2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strUser2", true));
            this.strUser2TextEdit.Location = new System.Drawing.Point(142, 519);
            this.strUser2TextEdit.MenuManager = this.barManager1;
            this.strUser2TextEdit.Name = "strUser2TextEdit";
            this.strUser2TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser2TextEdit, true);
            this.strUser2TextEdit.Size = new System.Drawing.Size(483, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser2TextEdit, optionsSpelling21);
            this.strUser2TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser2TextEdit.TabIndex = 27;
            // 
            // strUser3TextEdit
            // 
            this.strUser3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strUser3", true));
            this.strUser3TextEdit.Location = new System.Drawing.Point(142, 543);
            this.strUser3TextEdit.MenuManager = this.barManager1;
            this.strUser3TextEdit.Name = "strUser3TextEdit";
            this.strUser3TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser3TextEdit, true);
            this.strUser3TextEdit.Size = new System.Drawing.Size(483, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser3TextEdit, optionsSpelling22);
            this.strUser3TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser3TextEdit.TabIndex = 28;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(36, 268);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(1012, 387);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling23);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 23;
            // 
            // bDisabledCheckEdit
            // 
            this.bDisabledCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "bDisabled", true));
            this.bDisabledCheckEdit.Location = new System.Drawing.Point(118, 142);
            this.bDisabledCheckEdit.MenuManager = this.barManager1;
            this.bDisabledCheckEdit.Name = "bDisabledCheckEdit";
            this.bDisabledCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.bDisabledCheckEdit.Size = new System.Drawing.Size(139, 19);
            this.bDisabledCheckEdit.StyleController = this.dataLayoutControl1;
            this.bDisabledCheckEdit.TabIndex = 4;
            this.bDisabledCheckEdit.ToolTip = "Tick me to hide contractor from Contractor Selection list on Edit Insepction and " +
    "Edit Action screens.";
            // 
            // InternalContractorCheckEdit
            // 
            this.InternalContractorCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "InternalContractor", true));
            this.InternalContractorCheckEdit.EditValue = null;
            this.InternalContractorCheckEdit.Location = new System.Drawing.Point(118, 119);
            this.InternalContractorCheckEdit.MenuManager = this.barManager1;
            this.InternalContractorCheckEdit.Name = "InternalContractorCheckEdit";
            this.InternalContractorCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.InternalContractorCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.InternalContractorCheckEdit.Properties.ValueChecked = 1;
            this.InternalContractorCheckEdit.Properties.ValueUnchecked = 0;
            this.InternalContractorCheckEdit.Size = new System.Drawing.Size(139, 19);
            this.InternalContractorCheckEdit.StyleController = this.dataLayoutControl1;
            this.InternalContractorCheckEdit.TabIndex = 3;
            this.InternalContractorCheckEdit.TabStop = false;
            // 
            // intTypeIDGridLookUpEdit
            // 
            this.intTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "intTypeID", true));
            this.intTypeIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intTypeIDGridLookUpEdit.Location = new System.Drawing.Point(118, 83);
            this.intTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intTypeIDGridLookUpEdit.Name = "intTypeIDGridLookUpEdit";
            editorButtonImageOptions7.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions8.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Edit underlying data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Reload underlying data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intTypeIDGridLookUpEdit.Properties.DataSource = this.sp00186ContractorTypeListWithBlankBindingSource;
            this.intTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.intTypeIDGridLookUpEdit.Properties.NullText = "";
            this.intTypeIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.intTypeIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.intTypeIDGridLookUpEdit.Size = new System.Drawing.Size(254, 22);
            this.intTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intTypeIDGridLookUpEdit.TabIndex = 2;
            this.intTypeIDGridLookUpEdit.TabStop = false;
            this.intTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intTypeIDGridLookUpEdit_ButtonClick);
            // 
            // sp00186ContractorTypeListWithBlankBindingSource
            // 
            this.sp00186ContractorTypeListWithBlankBindingSource.DataMember = "sp00186_Contractor_Type_List_With_Blank";
            this.sp00186ContractorTypeListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colintOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colID;
            gridFormatRule2.ColumnApplyTo = this.colID;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridLookUpEdit1View.FormatRules.Add(gridFormatRule2);
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colintOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Contractor Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 229;
            // 
            // colintOrder
            // 
            this.colintOrder.Caption = "Order";
            this.colintOrder.FieldName = "intOrder";
            this.colintOrder.Name = "colintOrder";
            this.colintOrder.OptionsColumn.AllowEdit = false;
            this.colintOrder.OptionsColumn.AllowFocus = false;
            this.colintOrder.OptionsColumn.ReadOnly = true;
            this.colintOrder.Width = 99;
            // 
            // IsPotholeTeamCheckEdit
            // 
            this.IsPotholeTeamCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsPotholeTeam", true));
            this.IsPotholeTeamCheckEdit.Location = new System.Drawing.Point(753, 588);
            this.IsPotholeTeamCheckEdit.Name = "IsPotholeTeamCheckEdit";
            this.IsPotholeTeamCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsPotholeTeamCheckEdit.Properties.ValueChecked = 1;
            this.IsPotholeTeamCheckEdit.Properties.ValueUnchecked = 0;
            this.IsPotholeTeamCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.IsPotholeTeamCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsPotholeTeamCheckEdit.TabIndex = 14;
            this.IsPotholeTeamCheckEdit.CheckedChanged += new System.EventHandler(this.WorkTypeCheckEdit_CheckedChanged);
            this.IsPotholeTeamCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.WorkTypeCheckEdit_Validating);
            // 
            // SpecialistContractorCheckEdit
            // 
            this.SpecialistContractorCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "IsSpecialist", true));
            this.SpecialistContractorCheckEdit.EditValue = null;
            this.SpecialistContractorCheckEdit.Location = new System.Drawing.Point(753, 302);
            this.SpecialistContractorCheckEdit.MenuManager = this.barManager1;
            this.SpecialistContractorCheckEdit.Name = "SpecialistContractorCheckEdit";
            this.SpecialistContractorCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.SpecialistContractorCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SpecialistContractorCheckEdit.Properties.ValueChecked = 1;
            this.SpecialistContractorCheckEdit.Properties.ValueUnchecked = 0;
            this.SpecialistContractorCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.SpecialistContractorCheckEdit.StyleController = this.dataLayoutControl1;
            this.SpecialistContractorCheckEdit.TabIndex = 3;
            this.SpecialistContractorCheckEdit.TabStop = false;
            this.SpecialistContractorCheckEdit.CheckedChanged += new System.EventHandler(this.SpecialistContractorCheckEdit_CheckedChanged);
            this.SpecialistContractorCheckEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SpecialistContractorCheckEdit_Validating);
            // 
            // joinedDateDateEdit
            // 
            this.joinedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "JoinedDate", true));
            this.joinedDateDateEdit.EditValue = null;
            this.joinedDateDateEdit.Location = new System.Drawing.Point(142, 577);
            this.joinedDateDateEdit.Name = "joinedDateDateEdit";
            this.joinedDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.joinedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.joinedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.joinedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.joinedDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.joinedDateDateEdit.Size = new System.Drawing.Size(128, 20);
            this.joinedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.joinedDateDateEdit.TabIndex = 21;
            // 
            // approvedDateDateEdit
            // 
            this.approvedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "ApprovedDate", true));
            this.approvedDateDateEdit.EditValue = null;
            this.approvedDateDateEdit.Location = new System.Drawing.Point(142, 601);
            this.approvedDateDateEdit.Name = "approvedDateDateEdit";
            this.approvedDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.approvedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.approvedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.approvedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.approvedDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.approvedDateDateEdit.Size = new System.Drawing.Size(128, 20);
            this.approvedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.approvedDateDateEdit.TabIndex = 21;
            // 
            // terminationDateDateEdit
            // 
            this.terminationDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00183ContractorItemBindingSource, "TerminationDate", true));
            this.terminationDateDateEdit.EditValue = null;
            this.terminationDateDateEdit.Location = new System.Drawing.Point(142, 625);
            this.terminationDateDateEdit.Name = "terminationDateDateEdit";
            this.terminationDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.terminationDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.terminationDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.terminationDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.terminationDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.terminationDateDateEdit.Size = new System.Drawing.Size(128, 20);
            this.terminationDateDateEdit.StyleController = this.dataLayoutControl1;
            this.terminationDateDateEdit.TabIndex = 21;
            // 
            // ItemForintContractorID
            // 
            this.ItemForintContractorID.Control = this.intContractorIDSpinEdit;
            this.ItemForintContractorID.CustomizationFormText = "Contractor ID:";
            this.ItemForintContractorID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintContractorID.Name = "ItemForintContractorID";
            this.ItemForintContractorID.Size = new System.Drawing.Size(591, 24);
            this.ItemForintContractorID.Text = "Contractor ID:";
            this.ItemForintContractorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForRegionID
            // 
            this.ItemForRegionID.Control = this.RegionIDTextEdit;
            this.ItemForRegionID.Location = new System.Drawing.Point(0, 107);
            this.ItemForRegionID.Name = "ItemForRegionID";
            this.ItemForRegionID.Size = new System.Drawing.Size(798, 24);
            this.ItemForRegionID.Text = "Region ID:";
            this.ItemForRegionID.TextSize = new System.Drawing.Size(97, 13);
            // 
            // ItemForBusinessAreaID
            // 
            this.ItemForBusinessAreaID.Control = this.BusinessAreaIDTextEdit;
            this.ItemForBusinessAreaID.Location = new System.Drawing.Point(0, 0);
            this.ItemForBusinessAreaID.Name = "ItemForBusinessAreaID";
            this.ItemForBusinessAreaID.Size = new System.Drawing.Size(552, 24);
            this.ItemForBusinessAreaID.Text = "Business Area ID:";
            this.ItemForBusinessAreaID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1084, 702);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrCode,
            this.ItemForintTypeID,
            this.ItemForInternalContractor,
            this.emptySpaceItem1,
            this.emptySpaceItem5,
            this.ItemForstrName,
            this.layoutControlItem1,
            this.emptySpaceItem7,
            this.emptySpaceItem8,
            this.layoutControlGroup6,
            this.emptySpaceItem2,
            this.ItemForbDisabled,
            this.ItemForIsTest,
            this.emptySpaceItem18,
            this.emptySpaceItem19,
            this.emptySpaceItem20,
            this.emptySpaceItem9});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1064, 681);
            // 
            // ItemForstrCode
            // 
            this.ItemForstrCode.Control = this.strCodeTextEdit;
            this.ItemForstrCode.CustomizationFormText = "Contractor Code:";
            this.ItemForstrCode.Location = new System.Drawing.Point(0, 23);
            this.ItemForstrCode.MaxSize = new System.Drawing.Size(300, 24);
            this.ItemForstrCode.MinSize = new System.Drawing.Size(300, 24);
            this.ItemForstrCode.Name = "ItemForstrCode";
            this.ItemForstrCode.Size = new System.Drawing.Size(300, 24);
            this.ItemForstrCode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrCode.Text = "Team Code:";
            this.ItemForstrCode.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForintTypeID
            // 
            this.ItemForintTypeID.Control = this.intTypeIDGridLookUpEdit;
            this.ItemForintTypeID.CustomizationFormText = "Contractor Type:";
            this.ItemForintTypeID.Location = new System.Drawing.Point(0, 71);
            this.ItemForintTypeID.MaxSize = new System.Drawing.Size(364, 26);
            this.ItemForintTypeID.MinSize = new System.Drawing.Size(364, 26);
            this.ItemForintTypeID.Name = "ItemForintTypeID";
            this.ItemForintTypeID.Size = new System.Drawing.Size(364, 26);
            this.ItemForintTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForintTypeID.Text = "Team Type:";
            this.ItemForintTypeID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForInternalContractor
            // 
            this.ItemForInternalContractor.Control = this.InternalContractorCheckEdit;
            this.ItemForInternalContractor.CustomizationFormText = "Internal Contractor:";
            this.ItemForInternalContractor.Location = new System.Drawing.Point(0, 107);
            this.ItemForInternalContractor.MaxSize = new System.Drawing.Size(249, 23);
            this.ItemForInternalContractor.MinSize = new System.Drawing.Size(249, 23);
            this.ItemForInternalContractor.Name = "ItemForInternalContractor";
            this.ItemForInternalContractor.Size = new System.Drawing.Size(249, 23);
            this.ItemForInternalContractor.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForInternalContractor.Text = "Internal Contractor:";
            this.ItemForInternalContractor.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 176);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1064, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 97);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1064, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrName
            // 
            this.ItemForstrName.Control = this.strNameTextEdit;
            this.ItemForstrName.CustomizationFormText = "Contractor Name:";
            this.ItemForstrName.Location = new System.Drawing.Point(0, 47);
            this.ItemForstrName.MaxSize = new System.Drawing.Size(579, 24);
            this.ItemForstrName.MinSize = new System.Drawing.Size(579, 24);
            this.ItemForstrName.Name = "ItemForstrName";
            this.ItemForstrName.Size = new System.Drawing.Size(579, 24);
            this.ItemForstrName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrName.Text = "Team Name:";
            this.ItemForstrName.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(106, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(194, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(106, 0);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(106, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(106, 23);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(300, 0);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(764, 23);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 186);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1064, 485);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1040, 439);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup9,
            this.layoutControlGroup4,
            this.layoutControlGroup5});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLegalStatusID,
            this.ItemForIsVatRegistered,
            this.ItemForLatitude,
            this.ItemForstrUser1,
            this.ItemForstrUser2,
            this.ItemForstrUser3,
            this.emptySpaceItem3,
            this.ItemForUniqueTaxRef,
            this.ItemForstrVatReg,
            this.layoutControlGroup8,
            this.ItemForRegionName,
            this.ItemForBusinessAreaName,
            this.ItemForJoinedDate,
            this.emptySpaceItem14,
            this.emptySpaceItem16,
            this.ItemForApprovedDate,
            this.emptySpaceItem17,
            this.ItemForLongitude,
            this.ItemForTerminationDate,
            this.emptySpaceItem12,
            this.emptySpaceItem15,
            this.emptySpaceItem13,
            this.splitterItem1,
            this.ItemForNoSelfBilling,
            this.emptySpaceItem24,
            this.ItemforSupplierCode});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1016, 391);
            this.layoutControlGroup7.Text = "Details";
            // 
            // ItemForLegalStatusID
            // 
            this.ItemForLegalStatusID.Control = this.LegalStatusIDGridLookUpEdit;
            this.ItemForLegalStatusID.Location = new System.Drawing.Point(0, 48);
            this.ItemForLegalStatusID.Name = "ItemForLegalStatusID";
            this.ItemForLegalStatusID.Size = new System.Drawing.Size(593, 26);
            this.ItemForLegalStatusID.Text = "Legal Status:";
            this.ItemForLegalStatusID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsVatRegistered
            // 
            this.ItemForIsVatRegistered.Control = this.IsVatRegisteredCheckEdit;
            this.ItemForIsVatRegistered.CustomizationFormText = "Is Vat Registered:";
            this.ItemForIsVatRegistered.Location = new System.Drawing.Point(0, 74);
            this.ItemForIsVatRegistered.MaxSize = new System.Drawing.Size(238, 23);
            this.ItemForIsVatRegistered.MinSize = new System.Drawing.Size(238, 23);
            this.ItemForIsVatRegistered.Name = "ItemForIsVatRegistered";
            this.ItemForIsVatRegistered.Size = new System.Drawing.Size(238, 24);
            this.ItemForIsVatRegistered.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIsVatRegistered.Text = "Is Vat Registered:";
            this.ItemForIsVatRegistered.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForLatitude
            // 
            this.ItemForLatitude.Control = this.LatitudeSpinEdit;
            this.ItemForLatitude.CustomizationFormText = "Latitude:";
            this.ItemForLatitude.Location = new System.Drawing.Point(0, 179);
            this.ItemForLatitude.MaxSize = new System.Drawing.Size(238, 24);
            this.ItemForLatitude.MinSize = new System.Drawing.Size(238, 24);
            this.ItemForLatitude.Name = "ItemForLatitude";
            this.ItemForLatitude.Size = new System.Drawing.Size(238, 24);
            this.ItemForLatitude.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLatitude.Text = "Latitude:";
            this.ItemForLatitude.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrUser1
            // 
            this.ItemForstrUser1.Control = this.strUser1TextEdit;
            this.ItemForstrUser1.CustomizationFormText = "User Defined 1:";
            this.ItemForstrUser1.Location = new System.Drawing.Point(0, 227);
            this.ItemForstrUser1.Name = "ItemForstrUser1";
            this.ItemForstrUser1.Size = new System.Drawing.Size(593, 24);
            this.ItemForstrUser1.Text = "User Defined 1:";
            this.ItemForstrUser1.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrUser2
            // 
            this.ItemForstrUser2.Control = this.strUser2TextEdit;
            this.ItemForstrUser2.CustomizationFormText = "User Defined 2:";
            this.ItemForstrUser2.Location = new System.Drawing.Point(0, 251);
            this.ItemForstrUser2.Name = "ItemForstrUser2";
            this.ItemForstrUser2.Size = new System.Drawing.Size(593, 24);
            this.ItemForstrUser2.Text = "User Defined 2:";
            this.ItemForstrUser2.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrUser3
            // 
            this.ItemForstrUser3.Control = this.strUser3TextEdit;
            this.ItemForstrUser3.CustomizationFormText = "User Defined 3:";
            this.ItemForstrUser3.Location = new System.Drawing.Point(0, 275);
            this.ItemForstrUser3.Name = "ItemForstrUser3";
            this.ItemForstrUser3.Size = new System.Drawing.Size(593, 24);
            this.ItemForstrUser3.Text = "User Defined 3:";
            this.ItemForstrUser3.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 169);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(593, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForUniqueTaxRef
            // 
            this.ItemForUniqueTaxRef.Control = this.UniqueTaxRefTextEdit;
            this.ItemForUniqueTaxRef.Location = new System.Drawing.Point(0, 98);
            this.ItemForUniqueTaxRef.Name = "ItemForUniqueTaxRef";
            this.ItemForUniqueTaxRef.Size = new System.Drawing.Size(593, 24);
            this.ItemForUniqueTaxRef.Text = "Unique Tax Ref:";
            this.ItemForUniqueTaxRef.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrVatReg
            // 
            this.ItemForstrVatReg.Control = this.strVatRegTextEdit;
            this.ItemForstrVatReg.CustomizationFormText = "VAT Reg No:";
            this.ItemForstrVatReg.Location = new System.Drawing.Point(238, 74);
            this.ItemForstrVatReg.MaxSize = new System.Drawing.Size(297, 24);
            this.ItemForstrVatReg.MinSize = new System.Drawing.Size(297, 24);
            this.ItemForstrVatReg.Name = "ItemForstrVatReg";
            this.ItemForstrVatReg.Size = new System.Drawing.Size(297, 24);
            this.ItemForstrVatReg.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrVatReg.Text = "VAT Reg No:";
            this.ItemForstrVatReg.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIsGritter,
            this.ItemForIsSnowClearer,
            this.ItemForIsWoodPlanVisisble,
            this.ItemForIsUtilityArbTeam,
            this.ItemForIsRailArb,
            this.ItemForIsPestControl,
            this.ItemForIsFencing,
            this.ItemForIsRoofing,
            this.ItemForIsWindowCleaning,
            this.ItemForIsOperationsTeam,
            this.ItemForIsConstruction,
            this.ItemForIsPotholeTeam,
            this.ItemForSpecialistContractor,
            this.emptySpaceItem4});
            this.layoutControlGroup8.Location = new System.Drawing.Point(599, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(253, 391);
            this.layoutControlGroup8.Text = "Skill Set";
            // 
            // ItemForIsGritter
            // 
            this.ItemForIsGritter.Control = this.IsGritterCheckEdit;
            this.ItemForIsGritter.CustomizationFormText = "Gritting:";
            this.ItemForIsGritter.Location = new System.Drawing.Point(0, 33);
            this.ItemForIsGritter.Name = "ItemForIsGritter";
            this.ItemForIsGritter.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsGritter.Text = "Gritting:";
            this.ItemForIsGritter.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsSnowClearer
            // 
            this.ItemForIsSnowClearer.Control = this.IsSnowClearerCheckEdit;
            this.ItemForIsSnowClearer.CustomizationFormText = "Snow Clearance:";
            this.ItemForIsSnowClearer.Location = new System.Drawing.Point(0, 56);
            this.ItemForIsSnowClearer.Name = "ItemForIsSnowClearer";
            this.ItemForIsSnowClearer.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsSnowClearer.Text = "Snow Clearance:";
            this.ItemForIsSnowClearer.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsWoodPlanVisisble
            // 
            this.ItemForIsWoodPlanVisisble.Control = this.IsWoodPlanVisibleCheckEdit;
            this.ItemForIsWoodPlanVisisble.CustomizationFormText = "Amenity Arb:";
            this.ItemForIsWoodPlanVisisble.Location = new System.Drawing.Point(0, 125);
            this.ItemForIsWoodPlanVisisble.Name = "ItemForIsWoodPlanVisisble";
            this.ItemForIsWoodPlanVisisble.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsWoodPlanVisisble.Text = "Amenity Arb:";
            this.ItemForIsWoodPlanVisisble.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsUtilityArbTeam
            // 
            this.ItemForIsUtilityArbTeam.Control = this.IsUtilityArbTeamCheckEdit;
            this.ItemForIsUtilityArbTeam.CustomizationFormText = "Utility Arb:";
            this.ItemForIsUtilityArbTeam.Location = new System.Drawing.Point(0, 148);
            this.ItemForIsUtilityArbTeam.Name = "ItemForIsUtilityArbTeam";
            this.ItemForIsUtilityArbTeam.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsUtilityArbTeam.Text = "Utility Arb:";
            this.ItemForIsUtilityArbTeam.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsRailArb
            // 
            this.ItemForIsRailArb.Control = this.IsRailArbCheckEdit;
            this.ItemForIsRailArb.Location = new System.Drawing.Point(0, 171);
            this.ItemForIsRailArb.Name = "ItemForIsRailArb";
            this.ItemForIsRailArb.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsRailArb.Text = "Rail Arb:";
            this.ItemForIsRailArb.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsPestControl
            // 
            this.ItemForIsPestControl.Control = this.IsPestControlCheckEdit;
            this.ItemForIsPestControl.Location = new System.Drawing.Point(0, 194);
            this.ItemForIsPestControl.Name = "ItemForIsPestControl";
            this.ItemForIsPestControl.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsPestControl.Text = "Pest Control:";
            this.ItemForIsPestControl.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsFencing
            // 
            this.ItemForIsFencing.Control = this.IsFencingCheckEdit;
            this.ItemForIsFencing.Location = new System.Drawing.Point(0, 217);
            this.ItemForIsFencing.Name = "ItemForIsFencing";
            this.ItemForIsFencing.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsFencing.Text = "Fencing:";
            this.ItemForIsFencing.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsRoofing
            // 
            this.ItemForIsRoofing.Control = this.IsRoofingCheckEdit;
            this.ItemForIsRoofing.Location = new System.Drawing.Point(0, 240);
            this.ItemForIsRoofing.Name = "ItemForIsRoofing";
            this.ItemForIsRoofing.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsRoofing.Text = "Roofing:";
            this.ItemForIsRoofing.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsWindowCleaning
            // 
            this.ItemForIsWindowCleaning.Control = this.IsWindowCleaningCheckEdit;
            this.ItemForIsWindowCleaning.Location = new System.Drawing.Point(0, 263);
            this.ItemForIsWindowCleaning.Name = "ItemForIsWindowCleaning";
            this.ItemForIsWindowCleaning.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsWindowCleaning.Text = "Window Cleaning:";
            this.ItemForIsWindowCleaning.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsOperationsTeam
            // 
            this.ItemForIsOperationsTeam.Control = this.IsOperationsTeamCheckEdit;
            this.ItemForIsOperationsTeam.CustomizationFormText = "Maintenance:";
            this.ItemForIsOperationsTeam.Location = new System.Drawing.Point(0, 79);
            this.ItemForIsOperationsTeam.Name = "ItemForIsOperationsTeam";
            this.ItemForIsOperationsTeam.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsOperationsTeam.Text = "Maintenance:";
            this.ItemForIsOperationsTeam.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsConstruction
            // 
            this.ItemForIsConstruction.Control = this.IsConstructionCheckEdit;
            this.ItemForIsConstruction.Location = new System.Drawing.Point(0, 102);
            this.ItemForIsConstruction.Name = "ItemForIsConstruction";
            this.ItemForIsConstruction.Size = new System.Drawing.Size(229, 23);
            this.ItemForIsConstruction.Text = "Construction:";
            this.ItemForIsConstruction.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsPotholeTeam
            // 
            this.ItemForIsPotholeTeam.Control = this.IsPotholeTeamCheckEdit;
            this.ItemForIsPotholeTeam.CustomizationFormText = "Pothole Team:";
            this.ItemForIsPotholeTeam.Location = new System.Drawing.Point(0, 286);
            this.ItemForIsPotholeTeam.Name = "ItemForIsPotholeTeam";
            this.ItemForIsPotholeTeam.Size = new System.Drawing.Size(229, 59);
            this.ItemForIsPotholeTeam.Text = "Pothole Team:";
            this.ItemForIsPotholeTeam.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForSpecialistContractor
            // 
            this.ItemForSpecialistContractor.Control = this.SpecialistContractorCheckEdit;
            this.ItemForSpecialistContractor.CustomizationFormText = "Specialist Contractor:";
            this.ItemForSpecialistContractor.Location = new System.Drawing.Point(0, 0);
            this.ItemForSpecialistContractor.MaxSize = new System.Drawing.Size(229, 23);
            this.ItemForSpecialistContractor.MinSize = new System.Drawing.Size(229, 23);
            this.ItemForSpecialistContractor.Name = "ItemForSpecialistContractor";
            this.ItemForSpecialistContractor.OptionsToolTip.ToolTip = "Contractor used for specialised work and is not permitted to be included in the w" +
    "ork types used for scheduled or ad-hoc work";
            this.ItemForSpecialistContractor.Size = new System.Drawing.Size(229, 23);
            this.ItemForSpecialistContractor.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSpecialistContractor.Text = "Specialist Contractor:";
            this.ItemForSpecialistContractor.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 23);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(229, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRegionName
            // 
            this.ItemForRegionName.Control = this.RegionNameButtonEdit;
            this.ItemForRegionName.Location = new System.Drawing.Point(0, 24);
            this.ItemForRegionName.Name = "ItemForRegionName";
            this.ItemForRegionName.Size = new System.Drawing.Size(593, 24);
            this.ItemForRegionName.Text = "Region:";
            this.ItemForRegionName.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForBusinessAreaName
            // 
            this.ItemForBusinessAreaName.Control = this.BusinessAreaNameButtonEdit;
            this.ItemForBusinessAreaName.Location = new System.Drawing.Point(0, 0);
            this.ItemForBusinessAreaName.Name = "ItemForBusinessAreaName";
            this.ItemForBusinessAreaName.Size = new System.Drawing.Size(593, 24);
            this.ItemForBusinessAreaName.Text = "Business Area:";
            this.ItemForBusinessAreaName.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForJoinedDate
            // 
            this.ItemForJoinedDate.Control = this.joinedDateDateEdit;
            this.ItemForJoinedDate.CustomizationFormText = "Joined Date:";
            this.ItemForJoinedDate.Location = new System.Drawing.Point(0, 309);
            this.ItemForJoinedDate.MaxSize = new System.Drawing.Size(238, 24);
            this.ItemForJoinedDate.MinSize = new System.Drawing.Size(238, 24);
            this.ItemForJoinedDate.Name = "ItemForJoinedDate";
            this.ItemForJoinedDate.Size = new System.Drawing.Size(238, 24);
            this.ItemForJoinedDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForJoinedDate.Text = "Joined Date:";
            this.ItemForJoinedDate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(852, 0);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(164, 391);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(238, 309);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(355, 72);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForApprovedDate
            // 
            this.ItemForApprovedDate.Control = this.approvedDateDateEdit;
            this.ItemForApprovedDate.CustomizationFormText = "Approved Date:";
            this.ItemForApprovedDate.Location = new System.Drawing.Point(0, 333);
            this.ItemForApprovedDate.Name = "ItemForApprovedDate";
            this.ItemForApprovedDate.Size = new System.Drawing.Size(238, 24);
            this.ItemForApprovedDate.Text = "Approved Date:";
            this.ItemForApprovedDate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(238, 179);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(355, 48);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLongitude
            // 
            this.ItemForLongitude.Control = this.LongitudeSpinEdit;
            this.ItemForLongitude.CustomizationFormText = "Longitude:";
            this.ItemForLongitude.Location = new System.Drawing.Point(0, 203);
            this.ItemForLongitude.Name = "ItemForLongitude";
            this.ItemForLongitude.Size = new System.Drawing.Size(238, 24);
            this.ItemForLongitude.Text = "Longitude:";
            this.ItemForLongitude.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForTerminationDate
            // 
            this.ItemForTerminationDate.Control = this.terminationDateDateEdit;
            this.ItemForTerminationDate.CustomizationFormText = "Termination Date:";
            this.ItemForTerminationDate.Location = new System.Drawing.Point(0, 357);
            this.ItemForTerminationDate.Name = "ItemForTerminationDate";
            this.ItemForTerminationDate.Size = new System.Drawing.Size(238, 24);
            this.ItemForTerminationDate.Text = "Termination Date:";
            this.ItemForTerminationDate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 381);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(593, 10);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 299);
            this.emptySpaceItem15.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem15.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(593, 10);
            this.emptySpaceItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(535, 74);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(58, 24);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(593, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 391);
            // 
            // ItemForNoSelfBilling
            // 
            this.ItemForNoSelfBilling.Control = this.NoSelfBillingCheckEdit;
            this.ItemForNoSelfBilling.Location = new System.Drawing.Point(0, 122);
            this.ItemForNoSelfBilling.MaxSize = new System.Drawing.Size(198, 23);
            this.ItemForNoSelfBilling.MinSize = new System.Drawing.Size(198, 23);
            this.ItemForNoSelfBilling.Name = "ItemForNoSelfBilling";
            this.ItemForNoSelfBilling.Size = new System.Drawing.Size(198, 23);
            this.ItemForNoSelfBilling.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNoSelfBilling.Text = "OM - No Self-Billing:";
            this.ItemForNoSelfBilling.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.Location = new System.Drawing.Point(198, 122);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(395, 23);
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemforSupplierCode
            // 
            this.ItemforSupplierCode.Control = this.SupplierCodeTextEdit;
            this.ItemforSupplierCode.Location = new System.Drawing.Point(0, 145);
            this.ItemforSupplierCode.Name = "ItemforSupplierCode";
            this.ItemforSupplierCode.Size = new System.Drawing.Size(593, 24);
            this.ItemforSupplierCode.Text = "Supplier Code:";
            this.ItemforSupplierCode.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForOMPaidDiscountedVisitRate,
            this.emptySpaceItem10,
            this.emptySpaceItem11});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1016, 391);
            this.layoutControlGroup9.Text = "Maintenance";
            // 
            // ItemForOMPaidDiscountedVisitRate
            // 
            this.ItemForOMPaidDiscountedVisitRate.Control = this.OMPaidDiscountedVisitRateCheckEdit;
            this.ItemForOMPaidDiscountedVisitRate.Location = new System.Drawing.Point(0, 0);
            this.ItemForOMPaidDiscountedVisitRate.MaxSize = new System.Drawing.Size(232, 23);
            this.ItemForOMPaidDiscountedVisitRate.MinSize = new System.Drawing.Size(232, 23);
            this.ItemForOMPaidDiscountedVisitRate.Name = "ItemForOMPaidDiscountedVisitRate";
            this.ItemForOMPaidDiscountedVisitRate.Size = new System.Drawing.Size(232, 23);
            this.ItemForOMPaidDiscountedVisitRate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForOMPaidDiscountedVisitRate.Text = "Paid Discounted Visit Rate:";
            this.ItemForOMPaidDiscountedVisitRate.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForOMPaidDiscountedVisitRate.TextSize = new System.Drawing.Size(128, 13);
            this.ItemForOMPaidDiscountedVisitRate.TextToControlDistance = 5;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 23);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(1016, 368);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(232, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(784, 23);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImageOptions.Image")));
            this.layoutControlGroup4.CustomizationFormText = "Contact Details";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrAddressLine1,
            this.ItemForstrPostcode,
            this.ItemForstrTel1,
            this.emptySpaceItem6,
            this.ItemForTextNumber,
            this.emptySpaceItem21,
            this.emptySpaceItem22,
            this.ItemForstrAddressLine2,
            this.ItemForstrAddressLine3,
            this.ItemForstrAddressLine4,
            this.ItemForstrAddressLine5,
            this.emptySpaceItem23,
            this.ItemForstrTel2,
            this.ItemForstrMobileTel,
            this.ItemForstrWebsite,
            this.ItemForstrEmailPassword});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1016, 391);
            this.layoutControlGroup4.Text = "Contact Details";
            // 
            // ItemForstrAddressLine1
            // 
            this.ItemForstrAddressLine1.Control = this.strAddressLine1TextEdit;
            this.ItemForstrAddressLine1.CustomizationFormText = "Address Line 1:";
            this.ItemForstrAddressLine1.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrAddressLine1.MaxSize = new System.Drawing.Size(508, 24);
            this.ItemForstrAddressLine1.MinSize = new System.Drawing.Size(508, 24);
            this.ItemForstrAddressLine1.Name = "ItemForstrAddressLine1";
            this.ItemForstrAddressLine1.Size = new System.Drawing.Size(508, 24);
            this.ItemForstrAddressLine1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrAddressLine1.Text = "Address Line 1:";
            this.ItemForstrAddressLine1.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrPostcode
            // 
            this.ItemForstrPostcode.Control = this.strPostcodeTextEdit;
            this.ItemForstrPostcode.CustomizationFormText = "Postcode:";
            this.ItemForstrPostcode.Location = new System.Drawing.Point(0, 120);
            this.ItemForstrPostcode.MaxSize = new System.Drawing.Size(287, 24);
            this.ItemForstrPostcode.MinSize = new System.Drawing.Size(287, 24);
            this.ItemForstrPostcode.Name = "ItemForstrPostcode";
            this.ItemForstrPostcode.Size = new System.Drawing.Size(287, 24);
            this.ItemForstrPostcode.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrPostcode.Text = "Postcode:";
            this.ItemForstrPostcode.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrTel1
            // 
            this.ItemForstrTel1.Control = this.strTel1TextEdit;
            this.ItemForstrTel1.CustomizationFormText = "Telephone 1:";
            this.ItemForstrTel1.Location = new System.Drawing.Point(0, 156);
            this.ItemForstrTel1.MaxSize = new System.Drawing.Size(508, 24);
            this.ItemForstrTel1.MinSize = new System.Drawing.Size(508, 24);
            this.ItemForstrTel1.Name = "ItemForstrTel1";
            this.ItemForstrTel1.Size = new System.Drawing.Size(508, 24);
            this.ItemForstrTel1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrTel1.Text = "Telephone 1:";
            this.ItemForstrTel1.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 144);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1016, 12);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTextNumber
            // 
            this.ItemForTextNumber.Control = this.TextNumberMemoEdit;
            this.ItemForTextNumber.CustomizationFormText = "Text Number(s):";
            this.ItemForTextNumber.Location = new System.Drawing.Point(0, 276);
            this.ItemForTextNumber.MaxSize = new System.Drawing.Size(0, 64);
            this.ItemForTextNumber.MinSize = new System.Drawing.Size(115, 64);
            this.ItemForTextNumber.Name = "ItemForTextNumber";
            this.ItemForTextNumber.Size = new System.Drawing.Size(1016, 115);
            this.ItemForTextNumber.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTextNumber.Text = "Text Number(s):";
            this.ItemForTextNumber.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.Location = new System.Drawing.Point(287, 120);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(729, 24);
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.Location = new System.Drawing.Point(508, 0);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(508, 120);
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrAddressLine2
            // 
            this.ItemForstrAddressLine2.Control = this.strAddressLine2TextEdit;
            this.ItemForstrAddressLine2.CustomizationFormText = "Address Line 2:";
            this.ItemForstrAddressLine2.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrAddressLine2.Name = "ItemForstrAddressLine2";
            this.ItemForstrAddressLine2.Size = new System.Drawing.Size(508, 24);
            this.ItemForstrAddressLine2.Text = "Address Line 2:";
            this.ItemForstrAddressLine2.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrAddressLine3
            // 
            this.ItemForstrAddressLine3.Control = this.strAddressLine3TextEdit;
            this.ItemForstrAddressLine3.CustomizationFormText = "Address Line 3:";
            this.ItemForstrAddressLine3.Location = new System.Drawing.Point(0, 48);
            this.ItemForstrAddressLine3.Name = "ItemForstrAddressLine3";
            this.ItemForstrAddressLine3.Size = new System.Drawing.Size(508, 24);
            this.ItemForstrAddressLine3.Text = "Address Line 3:";
            this.ItemForstrAddressLine3.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrAddressLine4
            // 
            this.ItemForstrAddressLine4.Control = this.strAddressLine4TextEdit;
            this.ItemForstrAddressLine4.CustomizationFormText = "Address Line 4:";
            this.ItemForstrAddressLine4.Location = new System.Drawing.Point(0, 72);
            this.ItemForstrAddressLine4.Name = "ItemForstrAddressLine4";
            this.ItemForstrAddressLine4.Size = new System.Drawing.Size(508, 24);
            this.ItemForstrAddressLine4.Text = "Address Line 4:";
            this.ItemForstrAddressLine4.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrAddressLine5
            // 
            this.ItemForstrAddressLine5.Control = this.strAddressLine5TextEdit;
            this.ItemForstrAddressLine5.CustomizationFormText = "Address Line 5:";
            this.ItemForstrAddressLine5.Location = new System.Drawing.Point(0, 96);
            this.ItemForstrAddressLine5.Name = "ItemForstrAddressLine5";
            this.ItemForstrAddressLine5.Size = new System.Drawing.Size(508, 24);
            this.ItemForstrAddressLine5.Text = "Address Line 5:";
            this.ItemForstrAddressLine5.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.Location = new System.Drawing.Point(508, 156);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(508, 120);
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForstrTel2
            // 
            this.ItemForstrTel2.Control = this.strTel2TextEdit;
            this.ItemForstrTel2.CustomizationFormText = "Telephone 2:";
            this.ItemForstrTel2.Location = new System.Drawing.Point(0, 180);
            this.ItemForstrTel2.Name = "ItemForstrTel2";
            this.ItemForstrTel2.Size = new System.Drawing.Size(508, 24);
            this.ItemForstrTel2.Text = "Telephone 2:";
            this.ItemForstrTel2.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrMobileTel
            // 
            this.ItemForstrMobileTel.Control = this.strMobileTelTextEdit;
            this.ItemForstrMobileTel.CustomizationFormText = "Mobile:";
            this.ItemForstrMobileTel.Location = new System.Drawing.Point(0, 204);
            this.ItemForstrMobileTel.Name = "ItemForstrMobileTel";
            this.ItemForstrMobileTel.Size = new System.Drawing.Size(508, 24);
            this.ItemForstrMobileTel.Text = "Mobile:";
            this.ItemForstrMobileTel.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrWebsite
            // 
            this.ItemForstrWebsite.Control = this.strWebsiteTextEdit;
            this.ItemForstrWebsite.CustomizationFormText = "Website:";
            this.ItemForstrWebsite.Location = new System.Drawing.Point(0, 228);
            this.ItemForstrWebsite.Name = "ItemForstrWebsite";
            this.ItemForstrWebsite.Size = new System.Drawing.Size(508, 24);
            this.ItemForstrWebsite.Text = "Website:";
            this.ItemForstrWebsite.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForstrEmailPassword
            // 
            this.ItemForstrEmailPassword.Control = this.strEmailPasswordTextEdit;
            this.ItemForstrEmailPassword.CustomizationFormText = "Email Password:";
            this.ItemForstrEmailPassword.Location = new System.Drawing.Point(0, 252);
            this.ItemForstrEmailPassword.MaxSize = new System.Drawing.Size(508, 24);
            this.ItemForstrEmailPassword.MinSize = new System.Drawing.Size(508, 24);
            this.ItemForstrEmailPassword.Name = "ItemForstrEmailPassword";
            this.ItemForstrEmailPassword.Size = new System.Drawing.Size(508, 24);
            this.ItemForstrEmailPassword.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrEmailPassword.Text = "Email Password:";
            this.ItemForstrEmailPassword.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImageOptions.Image")));
            this.layoutControlGroup5.CustomizationFormText = "Remarks";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1016, 391);
            this.layoutControlGroup5.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(1016, 391);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(249, 107);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(815, 69);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForbDisabled
            // 
            this.ItemForbDisabled.Control = this.bDisabledCheckEdit;
            this.ItemForbDisabled.CustomizationFormText = "Disabled:";
            this.ItemForbDisabled.Location = new System.Drawing.Point(0, 130);
            this.ItemForbDisabled.Name = "ItemForbDisabled";
            this.ItemForbDisabled.Size = new System.Drawing.Size(249, 23);
            this.ItemForbDisabled.Text = "Disabled:";
            this.ItemForbDisabled.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForIsTest
            // 
            this.ItemForIsTest.Control = this.IsTestCheckEdit;
            this.ItemForIsTest.Location = new System.Drawing.Point(0, 153);
            this.ItemForIsTest.Name = "ItemForIsTest";
            this.ItemForIsTest.Size = new System.Drawing.Size(249, 23);
            this.ItemForIsTest.Text = "Test Team:";
            this.ItemForIsTest.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(300, 23);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(764, 24);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(364, 71);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(700, 26);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(579, 47);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(485, 24);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 671);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1064, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 681);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1064, 1);
            // 
            // sp00183_Contractor_ItemTableAdapter
            // 
            this.sp00183_Contractor_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp00186_Contractor_Type_List_With_BlankTableAdapter
            // 
            this.sp00186_Contractor_Type_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00244_Core_Legal_Statuses_PicklistTableAdapter
            // 
            this.sp00244_Core_Legal_Statuses_PicklistTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Contractor_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1101, 736);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Contractor_Edit";
            this.Text = "Edit Team";
            this.Activated += new System.EventHandler(this.frm_Core_Contractor_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Contractor_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Contractor_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SupplierCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00183ContractorItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoSelfBillingCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsTestCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OMPaidDiscountedVisitRateCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsWindowCleaningCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsRoofingCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsFencingCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsConstructionCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsPestControlCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsRailArbCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UniqueTaxRefTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LegalStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00244CoreLegalStatusesPicklistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsOperationsTeamCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsUtilityArbTeamCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsVatRegisteredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsSnowClearerCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsWoodPlanVisibleCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsGritterCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextNumberMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intContractorIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strAddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTel1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTel2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strMobileTelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strEmailPasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strWebsiteTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strVatRegTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bDisabledCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternalContractorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00186ContractorTypeListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsPotholeTeamCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialistContractorCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.joinedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.joinedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.approvedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.approvedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.terminationDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.terminationDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInternalContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLegalStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsVatRegistered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUniqueTaxRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrVatReg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsGritter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsSnowClearer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsWoodPlanVisisble)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsUtilityArbTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsRailArb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsPestControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsFencing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsRoofing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsWindowCleaning)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsOperationsTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsConstruction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsPotholeTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpecialistContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJoinedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApprovedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTerminationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoSelfBilling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforSupplierCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOMPaidDiscountedVisitRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTextNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrMobileTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrWebsite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrEmailPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForbDisabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.SpinEdit intContractorIDSpinEdit;
        private System.Windows.Forms.BindingSource sp00183ContractorItemBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraEditors.TextEdit strCodeTextEdit;
        private DevExpress.XtraEditors.TextEdit strNameTextEdit;
        private DevExpress.XtraEditors.TextEdit strAddressLine1TextEdit;
        private DevExpress.XtraEditors.TextEdit strAddressLine2TextEdit;
        private DevExpress.XtraEditors.TextEdit strAddressLine3TextEdit;
        private DevExpress.XtraEditors.TextEdit strAddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit strAddressLine5TextEdit;
        private DevExpress.XtraEditors.TextEdit strPostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit strTel1TextEdit;
        private DevExpress.XtraEditors.TextEdit strTel2TextEdit;
        private DevExpress.XtraEditors.TextEdit strMobileTelTextEdit;
        private DevExpress.XtraEditors.TextEdit strEmailPasswordTextEdit;
        private DevExpress.XtraEditors.TextEdit strWebsiteTextEdit;
        private DevExpress.XtraEditors.TextEdit strVatRegTextEdit;
        private DevExpress.XtraEditors.TextEdit strUser1TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser2TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser3TextEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraEditors.CheckEdit bDisabledCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintContractorID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrVatReg;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForbDisabled;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInternalContractor;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00183_Contractor_ItemTableAdapter sp00183_Contractor_ItemTableAdapter;
        private DevExpress.XtraEditors.CheckEdit InternalContractorCheckEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrAddressLine1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrAddressLine2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrAddressLine4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrAddressLine5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrPostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrTel1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrTel2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrMobileTel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrWebsite;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrEmailPassword;
        private DevExpress.XtraEditors.GridLookUpEdit intTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private System.Windows.Forms.BindingSource sp00186ContractorTypeListWithBlankBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00186_Contractor_Type_List_With_BlankTableAdapter sp00186_Contractor_Type_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colintOrder;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.SpinEdit LongitudeSpinEdit;
        private DevExpress.XtraEditors.SpinEdit LatitudeSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLatitude;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLongitude;
        private DevExpress.XtraEditors.MemoEdit TextNumberMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTextNumber;
        private DevExpress.XtraEditors.CheckEdit IsWoodPlanVisibleCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IsGritterCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsGritter;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsWoodPlanVisisble;
        private DevExpress.XtraEditors.CheckEdit IsVatRegisteredCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IsSnowClearerCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsSnowClearer;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsVatRegistered;
        private DevExpress.XtraEditors.CheckEdit IsUtilityArbTeamCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsUtilityArbTeam;
        private DevExpress.XtraEditors.CheckEdit IsOperationsTeamCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsOperationsTeam;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.GridLookUpEdit LegalStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLegalStatusID;
        private System.Windows.Forms.BindingSource sp00244CoreLegalStatusesPicklistBindingSource;
        private WoodPlanDataSetTableAdapters.sp00244_Core_Legal_Statuses_PicklistTableAdapter sp00244_Core_Legal_Statuses_PicklistTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.TextEdit UniqueTaxRefTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUniqueTaxRef;
        private DevExpress.XtraEditors.CheckEdit IsRailArbCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsRailArb;
        private DevExpress.XtraEditors.CheckEdit IsWindowCleaningCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IsRoofingCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IsFencingCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IsConstructionCheckEdit;
        private DevExpress.XtraEditors.CheckEdit IsPestControlCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsPestControl;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsConstruction;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsFencing;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsRoofing;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsWindowCleaning;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.TextEdit RegionIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionID;
        private DevExpress.XtraEditors.ButtonEdit RegionNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionName;
        private DevExpress.XtraEditors.TextEdit BusinessAreaIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBusinessAreaID;
        private DevExpress.XtraEditors.ButtonEdit BusinessAreaNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBusinessAreaName;
        private DevExpress.XtraEditors.CheckEdit OMPaidDiscountedVisitRateCheckEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOMPaidDiscountedVisitRate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraEditors.CheckEdit IsTestCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsTest;
        private DevExpress.XtraEditors.CheckEdit IsPotholeTeamCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsPotholeTeam;
        private DevExpress.XtraEditors.CheckEdit SpecialistContractorCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSpecialistContractor;
        private DevExpress.XtraEditors.DateEdit joinedDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJoinedDate;
        private DevExpress.XtraEditors.DateEdit approvedDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForApprovedDate;
        private DevExpress.XtraEditors.DateEdit terminationDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTerminationDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.CheckEdit NoSelfBillingCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoSelfBilling;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraEditors.TextEdit SupplierCodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemforSupplierCode;
    }
}

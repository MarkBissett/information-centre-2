using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Master_Job_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
 
        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        bool iBool_MasterPicklistItemsButtonEnabled = false;

        private GridEnumerations.enmFocusedGrid _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobType;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState5;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState6;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState7;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState14;  // Linked Visit Types //
        public RefreshGridState RefreshGridViewStateJobRate;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateJobRateCriteria;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        RepositoryItemTextEdit PercentageMaskEditor;
        RepositoryItemTextEdit CurrencyMaskEditor;
        int i_int_RateCriteriaType = 0;  // 1 = crown diameters, 2 = dbh bands, 3 = 'districts', 4 = 'height bands', 5 = risk categories, 6 = site hazard classes, 7 = sizes, 8 = 'species' //
        int i_int_DiscountRateCriteriaType = 0;

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs5 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs4 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs6 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs7 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs14 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded [Linked Visit Types] //
        string i_str_AddedRecordIDsJobRate = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsJobRateCriteria = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private string strDefaultPath = "";

        #endregion

        public frm_Core_Master_Job_Manager()
        {
            InitializeComponent();
        }

        private void frm_Core_Master_Job_Manager_Load(object sender, EventArgs e)
        {
             this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 10013;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            emptyEditor = new RepositoryItem();
            
            #region Prepare AT Job Rates

            PercentageMaskEditor = new RepositoryItemTextEdit();
            PercentageMaskEditor.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            PercentageMaskEditor.Mask.EditMask = "P";
            PercentageMaskEditor.Mask.UseMaskAsDisplayFormat = true;

            CurrencyMaskEditor = new RepositoryItemTextEdit();
            CurrencyMaskEditor.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            CurrencyMaskEditor.Mask.EditMask = "c";
            CurrencyMaskEditor.Mask.UseMaskAsDisplayFormat = true;

            gridControlJobRate.RepositoryItems.Add(emptyEditor);
            gridControlJobRate.RepositoryItems.Add(PercentageMaskEditor);
            gridControlJobRate.RepositoryItems.Add(CurrencyMaskEditor);

            gridControlJobRateCriteria.RepositoryItems.Add(emptyEditor);
            gridControlJobRateCriteria.RepositoryItems.Add(PercentageMaskEditor);
            gridControlJobRateCriteria.RepositoryItems.Add(CurrencyMaskEditor);

            // Get Job Rate and Job Discount Rate Criteria //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting2 = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting2.ChangeConnectionString(strConnectionString);
            i_int_RateCriteriaType = Convert.ToInt32(GetSetting2.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesJobRateCriteriaID"));
            i_int_DiscountRateCriteriaType = Convert.ToInt32(GetSetting2.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesJobDiscountRateCriteriaID"));

            sp00205_Job_Rate_Manager_Job_RatesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateJobRate = new RefreshGridState(gridViewJobRate, "RateID");

            sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateJobRateCriteria = new RefreshGridState(gridViewJobRateCriteria, "intParameterID");

            #endregion

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            sp00204_Job_Rate_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "JobID");
            
            sp07161_UT_Job_Manager_Linked_Default_MaterialsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "DefaultRequiredMaterialID");

            sp07162_UT_Job_Manager_Linked_Default_EquipmentTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState5 = new RefreshGridState(gridView5, "DefaultRequiredEquipmentID");

            sp07234_UT_Linked_Reference_FilesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "ReferenceFileID");

            sp06159_OM_Job_Manager_Linked_Job_Sub_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView4, "JobSubTypeID");

            sp06178_OM_Job_Manager_Linked_Job_Sub_Type_CompetenciesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState6 = new RefreshGridState(gridView6, "JobCompetencyID");

            sp06195_OM_Job_Manager_Linked_Master_Job_AttributesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState7 = new RefreshGridState(gridView7, "MasterJobAttributeID");
            
            sp06313_OM_Job_Type_Visit_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState14 = new RefreshGridState(gridView14, "VisitTypeJobID");

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedReferenceFiles").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Reference Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Reference Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();
            ConfigurePanelHeader();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigurePanelHeader();
        }

        private void ConfigurePanelHeader()
        {

            string strRateCriteria = "";
            switch (i_int_RateCriteriaType)
            {
                case 1:
                    strRateCriteria = "Crown Diameters";
                    break;
                case 2:
                    strRateCriteria = "DBH Bands";
                    break;
                case 3:
                    strRateCriteria = "Districts";
                    break;
                case 4:
                    strRateCriteria = "Height Bands";
                    break;
                case 5:
                    strRateCriteria = "Risk Categorys";
                    break;
                case 6:
                    strRateCriteria = "Site Hazard Classes";
                    break;
                case 7:
                    strRateCriteria = "Sizes";
                    break;
                case 8:
                    strRateCriteria = "Species";
                    break;
                default:
                    break;
            }
            string strDiscountRateCriteria = "";
            switch (i_int_DiscountRateCriteriaType)
            {
                case 1:
                    strDiscountRateCriteria = "Crown Diameters";
                    break;
                case 2:
                    strDiscountRateCriteria = "DBH Bands";
                    break;
                case 3:
                    strDiscountRateCriteria = "Districts";
                    break;
                case 4:
                    strDiscountRateCriteria = "Height Bands";
                    break;
                case 5:
                    strDiscountRateCriteria = "Risk Categorys";
                    break;
                case 6:
                    strDiscountRateCriteria = "Site Hazard Classes";
                    break;
                case 7:
                    strDiscountRateCriteria = "Sizes";
                    break;
                case 8:
                    strDiscountRateCriteria = "Species";
                    break;
                default:
                    break;
            }
            splitContainerControlJobRates.Panel2.Text = "Linked Contractors \\ Criteria...  [Criteria:  Job Rates = " + strRateCriteria + ",  Job Discount Rates = " + strDiscountRateCriteria + "]";
        }


        private void frm_Core_Master_Job_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1)) Load_Data();
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDs2) || !string.IsNullOrEmpty(i_str_AddedRecordIDs3) || !string.IsNullOrEmpty(i_str_AddedRecordIDs4) || !string.IsNullOrEmpty(i_str_AddedRecordIDs5) || !string.IsNullOrEmpty(i_str_AddedRecordIDs14)) LoadLinkedRecords();
                if (UpdateRefreshStatus == 3 || !string.IsNullOrEmpty(i_str_AddedRecordIDs6)) Load_Linked_Job_Competencies();
                if (UpdateRefreshStatus == 4 || !string.IsNullOrEmpty(i_str_AddedRecordIDs7)) Load_Linked_Job_Attributes();
                if (UpdateRefreshStatus == 5 || !string.IsNullOrEmpty(i_str_AddedRecordIDsJobRate)) Load_Linked_Job_Rates("", 0);
                if (UpdateRefreshStatus == 6 || !string.IsNullOrEmpty(i_str_AddedRecordIDsJobRateCriteria)) Load_Linked_Job_Rate_Criteria();
            }
            SetMenuStatus();
        }

        private void frm_Core_Master_Job_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
            }
        }


        public void LoadLastSavedUserScreenSettings()
        {
            bbiRefresh.PerformClick();
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs3, string strNewIDs5, string strNewIDs2, string strNewIDs4, string strNewIDs6, string strNewIDs7, string strNewIDs14)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
            if (strNewIDs5 != "") i_str_AddedRecordIDs5 = strNewIDs5;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs4 != "") i_str_AddedRecordIDs4 = strNewIDs4;
            if (strNewIDs6 != "") i_str_AddedRecordIDs6 = strNewIDs6;
            if (strNewIDs7 != "") i_str_AddedRecordIDs7 = strNewIDs7;
            if (strNewIDs14 != "") i_str_AddedRecordIDs14 = strNewIDs14;
        }
        public void UpdateFormRefreshStatus(int status, GridEnumerations.enmFocusedGrid grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case GridEnumerations.enmFocusedGrid.MasterJobTypeRate:
                    i_str_AddedRecordIDsJobRate = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsJobRate : newIds);
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeRateCriteria:
                    i_str_AddedRecordIDsJobRateCriteria = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsJobRateCriteria : newIds);
                    break;
            }
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
            // check if valid to enable permissions to open Mast Picklist Attribute Manager for OM //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, 7012, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);
            if (this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count == 1)
            {
                DataRow dr = dataSet_AT.sp00039GetFormPermissionsForUser.Rows[0];
                iBool_MasterPicklistItemsButtonEnabled = Convert.ToBoolean(dr["ReadAccess"]);
            }

        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView viewParent = (GridView)gridControl1.MainView;
            int[] intRowHandlesParent;
            intRowHandlesParent = viewParent.GetSelectedRows();
            int[] intRowHandles;
 
            GridView view = null;

            switch (_enmFocusedGrid)
            {
                case GridEnumerations.enmFocusedGrid.MasterJobType:
                    {
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                        }
                        bbiBlockAdd.Enabled = false;
                        if (iBool_AllowEdit && intRowHandlesParent.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandlesParent.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandlesParent.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubType:
                    {
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            if (intRowHandlesParent.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeCompetency:
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();

                        GridView viewParent2 = (GridView)gridControl4.MainView;
                        int[] intRowHandlesParent2 = viewParent2.GetSelectedRows();

                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            if (intRowHandlesParent2.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeAttribute:
                    {
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();

                        GridView viewParent2 = (GridView)gridControl4.MainView;
                        int[] intRowHandlesParent2 = viewParent2.GetSelectedRows();

                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            if (intRowHandlesParent2.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeDefaultMaterial:
                    {
                        if (iBool_AllowAdd)
                        {
                            if (intRowHandlesParent.Length >= 1)
                            {
                                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                                bsiAdd.Enabled = true;
                                bbiSingleAdd.Enabled = true;
                            }
                        }
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeDefaultEquipment:
                    {
                        if (iBool_AllowAdd)
                        {
                            if (intRowHandlesParent.Length >= 1)
                            {
                                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                                bsiAdd.Enabled = true;
                                bbiSingleAdd.Enabled = true;
                            }
                        }
                        view = (GridView)gridControl5.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeReferenceFile:
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            if (intRowHandlesParent.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeVisitType:
                    {
                        view = (GridView)gridControl14.MainView;
                        intRowHandles = view.GetSelectedRows();

                        GridView viewParent2 = (GridView)gridControl1.MainView;
                        int[] intRowHandlesParent2 = viewParent2.GetSelectedRows();

                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;

                            if (intRowHandlesParent2.Length >= 2)
                            {
                                alItems.Add("iBlockAdd");
                                bbiBlockAdd.Enabled = true;
                            }
                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeRate:
                    {
                        view = (GridView)gridControlJobRate.MainView;
                        intRowHandles = view.GetSelectedRows();

                        GridView viewParent2 = (GridView)gridControl1.MainView;
                        int[] intRowHandlesParent2 = viewParent2.GetSelectedRows();

                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                            bbiBlockAdd.Enabled = false;

                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeRateCriteria:
                    {
                        view = (GridView)gridControlJobRateCriteria.MainView;
                        intRowHandles = view.GetSelectedRows();

                        GridView viewParent2 = (GridView)gridControlJobRate.MainView;
                        int[] intRowHandlesParent2 = viewParent2.GetSelectedRows();

                        if (iBool_AllowAdd)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                            bbiBlockAdd.Enabled = false;

                        }
                        if (iBool_AllowEdit && intRowHandles.Length >= 1)
                        {
                            bsiEdit.Enabled = false;
                            bbiSingleEdit.Enabled = false;
                            bbiBlockEdit.Enabled = false;
                        }
                        if (iBool_AllowDelete && intRowHandles.Length >= 1)
                        {
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                default:
                    break;
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandlesParent.Length > 0 ? true : false);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandlesParent.Length > 0 ? true : false);

            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intRowHandlesParent.Length > 0 ? true : false);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);

            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intRowHandlesParent.Length > 0 ? true : false);
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);

            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0 ? true : false);

            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0 ? true : false);

            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0 ? true : false);

            view = (GridView)gridControl7.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0 ? true : false);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (iBool_MasterPicklistItemsButtonEnabled);

            view = (GridView)gridControl14.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0 ? true : false);

            view = (GridView)gridControlJobRate.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlJobRate.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlJobRate.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControlJobRate.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControlJobRate.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = true;
            gridControlJobRate.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);  // Rate Uplift //
            
            view = (GridView)gridControlJobRateCriteria.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlJobRateCriteria.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlJobRateCriteria.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            gridControlJobRateCriteria.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControlJobRateCriteria.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = true;
        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            this.RefreshGridViewState1.SaveViewInfo();
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            try
            {
                sp00204_Job_Rate_ManagerTableAdapter.Fill(dataSet_AT.sp00204_Job_Rate_Manager);
            }
            catch (Exception) { }
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["JobID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobID"])) + ',';
            }

            // Populate Linked Equipment //
            gridControl5.MainView.BeginUpdate();
            this.RefreshGridViewState5.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07162_UT_Job_Manager_Linked_Default_Equipment.Clear();
            }
            else
            {
                try
                {
                    sp07162_UT_Job_Manager_Linked_Default_EquipmentTableAdapter.Fill(dataSet_UT.sp07162_UT_Job_Manager_Linked_Default_Equipment, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception) { }
                RefreshGridViewState5.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl5.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs5 != "")
            {
                strArray = i_str_AddedRecordIDs5.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl5.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["DefaultRequiredEquipmentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs5 = "";
            }

            // Populate Linked Materials //
            gridControl3.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07161_UT_Job_Manager_Linked_Default_Materials.Clear();
            }
            else
            {
                try
                {
                    sp07161_UT_Job_Manager_Linked_Default_MaterialsTableAdapter.Fill(dataSet_UT.sp07161_UT_Job_Manager_Linked_Default_Materials, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception) { }
                RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl3.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["DefaultRequiredMaterialID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }

            // Populate Linked Reference Files //
            gridControl2.MainView.BeginUpdate();
            RefreshGridViewState2.SaveViewInfo();  // Reload any expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07234_UT_Linked_Reference_Files.Clear();
            }
            else
            {
                try
                {
                    sp07234_UT_Linked_Reference_FilesTableAdapter.Fill(dataSet_UT.sp07234_UT_Linked_Reference_Files, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 3, strDefaultPath);
                }
                catch (Exception) { }
                RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl2.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl2.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ReferenceFileID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }

            // Populate Linked Job Sub-Types //
            gridControl4.MainView.BeginUpdate();
            RefreshGridViewState4.SaveViewInfo();  // Reload any expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_OM_Job.sp06159_OM_Job_Manager_Linked_Job_Sub_Types.Clear();
            }
            else
            {
                try
                {
                    sp06159_OM_Job_Manager_Linked_Job_Sub_TypesTableAdapter.Fill(dataSet_OM_Job.sp06159_OM_Job_Manager_Linked_Job_Sub_Types, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception) { }
                RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl4.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs4 != "")
            {
                strArray = i_str_AddedRecordIDs4.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl4.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["JobSubTypeID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs4 = "";
            }

            // Populate Visit Type Information //
            gridControl14.MainView.BeginUpdate();
            this.RefreshGridViewState14.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_OM_Core.sp06313_OM_Job_Type_Visit_Types.Clear();
            }
            else
            {
                sp06313_OM_Job_Type_Visit_TypesTableAdapter.Fill(dataSet_OM_Core.sp06313_OM_Job_Type_Visit_Types, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState14.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }

            gridControl14.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs14 != "")
            {
                strArray = i_str_AddedRecordIDs14.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl14.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["VisitTypeJobID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs14 = "";
            }

           // Populate AT Job Rates //
            Load_Linked_Job_Rates((string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), intCount);
        }

        private void Load_Linked_Job_Competencies()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl4.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobSubTypeID"])) + ',';
            }

            // Populate Linked Equipment //
            gridControl6.MainView.BeginUpdate();
            this.RefreshGridViewState6.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06178_OM_Job_Manager_Linked_Job_Sub_Type_Competencies.Clear();
            }
            else
            {
                try
                {
                    sp06178_OM_Job_Manager_Linked_Job_Sub_Type_CompetenciesTableAdapter.Fill(dataSet_OM_Job.sp06178_OM_Job_Manager_Linked_Job_Sub_Type_Competencies, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception) { }
                RefreshGridViewState6.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl6.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs6 != "")
            {
                strArray = i_str_AddedRecordIDs6.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl6.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["JobCompetencyID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs6 = "";
            }
        }

        private void Load_Linked_Job_Attributes()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl4.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobSubTypeID"])) + ',';
            }

            // Populate Linked Attributes //
            gridControl7.MainView.BeginUpdate();
            this.RefreshGridViewState7.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06195_OM_Job_Manager_Linked_Master_Job_Attributes.Clear();
            }
            else
            {
                try
                {
                    sp06195_OM_Job_Manager_Linked_Master_Job_AttributesTableAdapter.Fill(dataSet_OM_Job.sp06195_OM_Job_Manager_Linked_Master_Job_Attributes, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception) { }
                RefreshGridViewState7.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl7.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs7 != "")
            {
                strArray = i_str_AddedRecordIDs7.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl7.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["MasterJobAttributeID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs7 = "";
            }
        }

        private void Load_Linked_Job_Rates(string strSelectedIDs, int intCount)
        {
            GridView view = null;
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (string.IsNullOrWhiteSpace(strSelectedIDs))
            {
                view = (GridView)gridControl1.MainView;
                int[] intRowHandles = view.GetSelectedRows();
                intCount = intRowHandles.Length;
                foreach (int intRowHandle in intRowHandles)
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobID"])) + ',';
                }
            }
            //Populate Linked Rates //
            gridControlJobRate.MainView.BeginUpdate();
            this.RefreshGridViewStateJobRate.SaveViewInfo();
            if (intCount == 0)
            {
                this.dataSet_AT.sp00205_Job_Rate_Manager_Job_Rates.Clear();
            }
            else
            {
                sp00205_Job_Rate_Manager_Job_RatesTableAdapter.Fill(dataSet_AT.sp00205_Job_Rate_Manager_Job_Rates, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewStateJobRate.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlJobRate.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsJobRate != "")
            {
                strArray = i_str_AddedRecordIDsJobRate.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlJobRate.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["RateID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsJobRate = "";
            }
        }

        private void Load_Linked_Job_Rate_Criteria()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlJobRate.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["RateID"])) + ',';
            }

            //Populate Linked Contacts //
            gridControlJobRateCriteria.MainView.BeginUpdate();
            this.RefreshGridViewStateJobRateCriteria.SaveViewInfo();
            if (intCount == 0)
            {
                this.dataSet_AT.sp00206_Job_Rate_Manager_Job_Rate_Parameters.Clear();
            }
            else
            {
                sp00206_Job_Rate_Manager_Job_Rate_ParametersTableAdapter.Fill(dataSet_AT.sp00206_Job_Rate_Manager_Job_Rate_Parameters, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), i_int_RateCriteriaType, i_int_DiscountRateCriteriaType);
                this.RefreshGridViewStateJobRateCriteria.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlJobRateCriteria.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsJobRateCriteria != "")
            {
                strArray = i_str_AddedRecordIDsJobRateCriteria.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlJobRateCriteria.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["intParameterID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsJobRateCriteria = "";
            }
        }


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }
        
        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            int intCount = 0;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            switch (_enmFocusedGrid)
            {
                case GridEnumerations.enmFocusedGrid.MasterJobType:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl1.MainView;
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Job_Rate_Master_Job_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                     }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeDefaultMaterial:
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more job records to add default materials to before proceeding.", "Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        var fChildForm = new frm_UT_Surveyed_Tree_Work_Block_Add_Material();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        string strSelectedIDs = "";
                        int intSelectedCount = 0;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;
                        strSelectedIDs = fChildForm.strSelectedIDs;
                        intSelectedCount = fChildForm.intSelectedCount;

                        if (intSelectedCount <= 0) return;
                        DataSet_UT_EditTableAdapters.QueriesTableAdapter AddRecords = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                        AddRecords.ChangeConnectionString(strConnectionString);

                        int intJobID = 0;
                        string strTempReturn = "";
                        try
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                intJobID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobID"));
                                strTempReturn = AddRecords.sp07164_UT_Job_Manager_Add_Default_Materials(intJobID, strSelectedIDs).ToString();
                                if (!string.IsNullOrEmpty(strTempReturn)) i_str_AddedRecordIDs3 += strTempReturn + ";";
                            }
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add new material(s) - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Materials", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            break;
                        }
                        LoadLinkedRecords();
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeDefaultEquipment:
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more job records to add default equipment to before proceeding.", "Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        var fChildForm = new frm_UT_Surveyed_Tree_Work_Block_Add_Equipment();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        string strSelectedIDs = "";
                        int intSelectedCount = 0;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;
                        strSelectedIDs = fChildForm.strSelectedIDs;
                        intSelectedCount = fChildForm.intSelectedCount;

                        if (intSelectedCount <= 0) return;
                        DataSet_UT_EditTableAdapters.QueriesTableAdapter AddRecords = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                        AddRecords.ChangeConnectionString(strConnectionString);

                        int intJobID = 0;
                        string strTempReturn = "";
                        try
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                intJobID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobID"));
                                strTempReturn = AddRecords.sp07163_UT_Job_Manager_Add_Default_Equipment(intJobID, strSelectedIDs).ToString();
                                if (!string.IsNullOrEmpty(strTempReturn)) i_str_AddedRecordIDs5 += strTempReturn + ";";
                            }
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add new equipment - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Equipment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            break;
                        }
                        LoadLinkedRecords();
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeReferenceFile:
                    {
                        // Check if only one parent job selected - if yes, pass it to child screen otherwise pass no job //
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_UT_Reference_File_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "JobID")) : 0);
                        fChildForm.strLinkedToRecordDesc = (intRowHandles.Length == 1 ? Convert.ToString(view.GetRowCellValue(intRowHandles[0], "JobDescription")) : "");
                        fChildForm.intRecordTypeID = 3;  // Master Job //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubType:
                    {
                        view = (GridView)gridControl4.MainView;
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        // Check if only one parent job selected - if yes, pass it to child screen otherwise pass no job //
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_AT.sp00204_Job_Rate_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.JobID;
                            fChildForm.strLinkedToRecordDesc = row.JobDescription;
                            int intMaxOrder = 0;
                            if (row.JobID != 0)
                            {
                                for (int i = 0; i < view.DataRowCount; i++)
                                {
                                    if (Convert.ToInt32(view.GetRowCellValue(i, "JobTypeID")) == row.JobID && Convert.ToInt32(view.GetRowCellValue(i, "RecordOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "RecordOrder"));
                                }
                                fChildForm.intMaxOrder = intMaxOrder;
                            }
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeCompetency:
                    {
                        view = (GridView)gridControl6.MainView;
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Competency_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        // Check if only one parent job sub-type selected - if yes, pass it to child screen otherwise pass no job sub-type //
                        ParentView = (GridView)gridControl4.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06159_OM_Job_Manager_Linked_Job_Sub_TypesRow)rowView.Row;
                            fChildForm.intLinkedToRecordID1 = row.JobTypeID;
                            fChildForm.intLinkedToRecordID2 = row.JobSubTypeID;
                            fChildForm.strLinkedToRecordDesc1 = row.JobDescription;
                            fChildForm.strLinkedToRecordDesc2 = row.Description;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeAttribute:
                    {
                        view = (GridView)gridControl7.MainView;
                        this.RefreshGridViewState7.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Attribute_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        // Check if only one parent job sub-type selected - if yes, pass it to child screen otherwise pass no job sub-type //
                        int intMaxOrder = 0;
                        ParentView = (GridView)gridControl4.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06159_OM_Job_Manager_Linked_Job_Sub_TypesRow)rowView.Row;
                            fChildForm.intLinkedToRecordID1 = row.JobTypeID;
                            fChildForm.intLinkedToRecordID2 = row.JobSubTypeID;
                            fChildForm.strLinkedToRecordDesc1 = row.JobDescription;
                            fChildForm.strLinkedToRecordDesc2 = row.Description;

                            intRowHandles = ParentView.GetSelectedRows();
                            if (intRowHandles.Length == 1)
                            {
                                for (int i = 0; i < view.DataRowCount; i++)
                                {
                                    if (Convert.ToInt32(view.GetRowCellValue(i, "MasterJobSubTypeID")) == row.JobSubTypeID && Convert.ToInt32(view.GetRowCellValue(i, "AttributeOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "AttributeOrder"));
                                }
                            }
                        }
                        fChildForm._LastOrder = intMaxOrder;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeVisitType:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Visit_Type_Job_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_AT.sp00204_Job_Rate_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.JobID;
                            fChildForm.strLinkedToRecordDescription = row.JobDescription;
                        }
                        fChildForm.intRecordTypeID = 2;  // 1 = People (Staff \ contractor), 2 = Master Job //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeRate:
                    {
                        // Check if only one parent Master Job selected - if yes, pass it to child screen otherwise pass no master job //
                        view = (GridView)gridControl1.MainView;  // Parent GridView //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewStateJobRate.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Job_Rate_Job_Rate_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intJobID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "JobID")) : 0);
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeRateCriteria:
                    {
                        // Get list of IDs from selected parent Rates - abort if none selected //
                        view = (GridView)gridControlJobRate.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Job Rates to add the Job Rate Criteria to before proceeding.", "Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RateID")) + ',';
                        }

                        this.RefreshGridViewStateJobRateCriteria.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Job_Rate_Criteria_Add();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.i_int_RateCriteriaType = (Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "RateTypeID")) == 0 ? this.i_int_RateCriteriaType : this.i_int_DiscountRateCriteriaType);
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case GridEnumerations.enmFocusedGrid.MasterJobTypeReferenceFile:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_UT_Reference_File_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.intRecordTypeID = 3;  // Master Job //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubType:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;  // Parent gridview //
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeCompetency:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl4.MainView;  // Parent gridview //
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobSubTypeID")) + ',';
                        }
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Competency_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeAttribute:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl4.MainView;  // Parent gridview //
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobSubTypeID")) + ',';
                        }
                        this.RefreshGridViewState7.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Attribute_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeVisitType:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm = new frm_OM_Visit_Type_Job_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 2;  // 1 = People (Staff \ contractor), 2 = Master Job //
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case GridEnumerations.enmFocusedGrid.MasterJobSubType:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobSubTypeID")) + ',';
                        }
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeCompetency:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobCompetencyID")) + ',';
                        }
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Competency_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeAttribute:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl7.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MasterJobAttributeID")) + ',';
                        }
                        this.RefreshGridViewState7.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Attribute_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeVisitType:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitTypeJobID")) + ',';
                        }

                        var fChildForm = new frm_OM_Visit_Type_Job_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 2;  // 1 = People (Staff \ contractor), 2 = Master Job //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeRate:
                    {
                        view = (GridView)gridControlJobRate.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RateID")) + ',';
                        }
                        this.RefreshGridViewStateJobRate.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Job_Rate_Job_Rate_Edit fChildForm2 = new frm_AT_Job_Rate_Job_Rate_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case GridEnumerations.enmFocusedGrid.MasterJobType:
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Job_Rate_Master_Job_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case GridEnumerations.enmFocusedGrid.MasterJobTypeReferenceFile:
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ReferenceFileID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_UT_Reference_File_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 3;  // Master Job //
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubType:
                    {
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobSubTypeID")) + ',';
                        }
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeCompetency:
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobCompetencyID")) + ',';
                        }
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Competency_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeAttribute:
                    {
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MasterJobAttributeID")) + ',';
                        }
                        this.RefreshGridViewState7.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Attribute_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeVisitType:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitTypeJobID")) + ',';
                        }
                        var fChildForm = new frm_OM_Visit_Type_Job_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 2;  // 1 = People (Staff \ contractor), 2 = Master Job //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeRate:
                    {
                        view = (GridView)gridControlJobRate.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RateID")) + ',';
                        }
                        this.RefreshGridViewStateJobRate.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_AT_Job_Rate_Job_Rate_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (_enmFocusedGrid)
            {
                case GridEnumerations.enmFocusedGrid.MasterJobType:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Master Jobs to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Master Job" : Convert.ToString(intRowHandles.Length) + " Master Jobs") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this master jon" : "these master jobs") + " will no longer be available for selection and any related Job Rates and Job Rate Criteria will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "JobID")) + ",";
                            }

                            DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp00209_Job_Rate_Master_Job_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case GridEnumerations.enmFocusedGrid.MasterJobTypeDefaultMaterial:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Default Materials to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Default Material" : Convert.ToString(intRowHandles.Length) + " Default Materials") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Default Material" : "these Default Materials") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "DefaultRequiredMaterialID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("default_material", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case GridEnumerations.enmFocusedGrid.MasterJobTypeDefaultEquipment:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl5.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Default Pieces of Equipment to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Default Piece of Equipment" : Convert.ToString(intRowHandles.Length) + " Default Pieces of Equipment") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Default Piece of Equipment" : "these Default Pieces of Equipment") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "DefaultRequiredEquipmentID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("default_equipment", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case GridEnumerations.enmFocusedGrid.MasterJobTypeReferenceFile:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Reference Files to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Reference File" : Convert.ToString(intRowHandles.Length) + " Linked Reference Files") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Reference File" : "these Linked Reference Files") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ReferenceFileID")) + ",";
                            }

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("reference_file", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubType:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Job Sub-Types to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Job Sub-Type" : Convert.ToString(intRowHandles.Length) + " Job Sub-Types") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Job Sub-Type" : "these Linked Job Sub-Types") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "JobSubTypeID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("master_job_sub_type", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeCompetency:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Job Sub-Type Competencies to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Job Sub-Type Competency" : Convert.ToString(intRowHandles.Length) + " Job Sub-Type Competencies") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Job Sub-Type Competency" : "these Linked Job Sub-Type Competencies") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "JobCompetencyID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("master_job_sub_type_competency", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Linked_Job_Competencies();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeAttribute:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Job Sub-Type Attributes to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Job Sub-Type Attribute" : Convert.ToString(intRowHandles.Length) + " Job Sub-Type Attributes") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Job Sub-Type Attribute" : "these Linked Job Sub-Type Attributes") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "MasterJobAttributeID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("master_job_sub_type_attribute", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Linked_Job_Attributes();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeVisitType:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl14.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Visit Types to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Visit Type" : Convert.ToString(intRowHandles.Length) + " Visit Types") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Visit Type" : "these Linked Visit Types") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitTypeJobID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_visit_type", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeRate:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlJobRate.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Job Rates to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Job Rate" : Convert.ToString(intRowHandles.Length) + " Job Rates") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Job Rate" : "these Job Rates") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "RateID")) + ",";
                            }

                            DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewStateJobRate.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            try
                            {
                                RemoveRecords.sp00212_Job_Rate_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Linked_Job_Rates("", 0);

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case GridEnumerations.enmFocusedGrid.MasterJobTypeRateCriteria:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlJobRateCriteria.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Contractor Certificates to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Job Rate Criteria" : Convert.ToString(intRowHandles.Length) + " Job Rate Criteria") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Job Rate Criteria" : "these Job Rate Criteria") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "intParameterID")) + ",";
                            }

                            DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewStateJobRateCriteria.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            try
                            {
                                RemoveRecords.sp00219_Job_Rate_Parameter_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Linked_Job_Rate_Criteria();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case GridEnumerations.enmFocusedGrid.MasterJobSubType:
                    {
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobSubTypeID")) + ',';
                        }
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeCompetency:
                    {
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobCompetencyID")) + ',';
                        }
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Competency_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case GridEnumerations.enmFocusedGrid.MasterJobSubTypeAttribute:
                    {
                        view = (GridView)gridControl7.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "MasterJobAttributeID")) + ',';
                        }
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_OM_Master_Job_Sub_Type_Attribute_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case GridEnumerations.enmFocusedGrid.MasterJobTypeVisitType:
                    {
                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitTypeJobID")) + ',';
                        }
                        var fChildForm = new frm_OM_Visit_Type_Job_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 2;  // 1 = People (Staff \ contractor), 2 = Master Job //
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Open_Picklist_Manager()
        {
            System.Reflection.MethodInfo method = null;
            var fChildForm = new frm_OM_Master_Attributes_Picklist_Manager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Master Jobs - Click Load Jobs button";
                    break;
                case "gridView3":
                    message = "No Default Materials Available - Select one or more Master Jobs to view Linked Default Materials";
                    break;
                case "gridView5":
                    message = "No Default Equipment Available - Select one or more Master Jobs to view Linked Default Equipment";
                    break;
                case "gridView2":
                    message = "No Linked Reference Files Available";
                    break;
                case "gridView4":
                    message = "No Linked Job Sub-Types Available - Select one or more Master Jobs to view Linked Job Sub-Types";
                    break;
                case "gridView6":
                    message = "No Linked Job Sub-Type Competencies Available - Select one or more Master Job Sub-Types to view Linked Job Sub-Type Competencies";
                    break;
                case "gridView7":
                    message = "No Linked Job Sub-Type Attributes Available - Select one or more Master Job Sub-Types to view Linked Job Sub-Type Attribut";
                    break;
                case "gridView14":
                    message = "No Linked Visit Types - Select one or more Master Jobs to view linked Visit Type";
                    break;
                case "gridViewJobRate":
                    message = "No Job Rates Available - Select one or more Master Jobs to view linked Job Rates";
                    break;
                case "gridViewJobRateCriteria":
                    message = "No Job Rate Contractors\\Criteria Available - Select one or more Job Rates to view linked Job Rate Contractors\\Criteria";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedRecords();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl5.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl2.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl4.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl14.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlJobRate.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView4":
                    Load_Linked_Job_Competencies();
                    view = (GridView)gridControl6.MainView;
                    view.ExpandAllGroups();

                    Load_Linked_Job_Attributes();
                    view = (GridView)gridControl7.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        //View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobType;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobType;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        #endregion


        #region GridView3

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView3_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "UnitsInStock")
            {
                if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "UnitsInStock")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "ReorderLevel")))
                {
                    e.Appearance.BackColor = Color.LightCoral;
                    e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    return;
                }
                else if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "UnitsInStock")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "WarningLevel")))
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    return;
                }
                else
                {
                    //e.Appearance.BackColor = Color.Transparent;
                    //e.Appearance.BackColor2 = Color.Transparent;
                    //e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    return;
                }
            }
        }

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeDefaultMaterial;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeDefaultMaterial;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView5

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView5_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeDefaultEquipment;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeDefaultEquipment;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView2

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeReferenceFile;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeReferenceFile;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemHyperLinkEditDocumentPath_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        #endregion


        #region GridView4

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobSubType;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobSubType;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView6

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView6_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobSubTypeCompetency;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobSubTypeCompetency;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView7

        private void gridControl7_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("picklists".Equals(e.Button.Tag))
                    {
                        Open_Picklist_Manager();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView7_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobSubTypeAttribute;
            SetMenuStatus();
        }

        private void gridView7_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobSubTypeAttribute;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView14

        private void gridView14_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView14_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeVisitType;
            SetMenuStatus();
        }

        private void gridView14_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeVisitType;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl14_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridViewJobRate

        private void gridViewJobRate_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "RateUnits":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RateTypeID")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "JobRate":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RateTypeID")) == 0)
                    {
                        e.RepositoryItem = CurrencyMaskEditor;
                    }
                    else
                    {
                        e.RepositoryItem = PercentageMaskEditor;
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewJobRate_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewJobRate_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeRate;
            SetMenuStatus();
        }

        private void gridViewJobRate_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeRate;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewJobRate_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            // This Event Prevents Record Type Spanning //
            if (isRunning) return;
            isRunning = true;

            GridView view = sender as GridView;
            if (e.Action == CollectionChangeAction.Add)
            {
                if (view.IsGroupRow(e.ControllerRow)) view.UnselectRow(e.ControllerRow);
                else
                {
                    // Prevent Record Type Spanning when Control Key is held down for multiple row selection //
                    int intType = -1;
                    int[] intRowHandles = view.GetSelectedRows();
                    if (intRowHandles.Length > 0)
                    {
                        if (e.ControllerRow != intRowHandles[0] || intRowHandles.Length == 1)
                        {
                            intType = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "RateTypeID"));
                        }
                        else
                        {
                            intType = Convert.ToInt32(view.GetRowCellValue(intRowHandles[1], "RateTypeID"));
                        }
                        if (Convert.ToInt32(view.GetRowCellValue(e.ControllerRow, "RateTypeID")) != intType) view.UnselectRow(e.ControllerRow);
                    }
                }
            }
            if (e.Action == CollectionChangeAction.Refresh && view.SelectedRowsCount > 0)
            {
                // Prevent Group Spanning when Shift Key is held down for multiple row selection [following 2 lines and code inside ForEach loop below] //
                int intType = -1;
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle) intType = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "RateTypeID"));

                view.BeginSelection();
                foreach (int Row in view.GetSelectedRows())
                {
                    if (view.IsGroupRow(Row)) view.UnselectRow(Row);
                    else
                    {
                        // Prevent Group Spanning when Shift Key is held down for multiple row selection //
                        if (Convert.ToInt32(view.GetRowCellValue(Row, "RateTypeID")) != intType) view.UnselectRow(Row);
                    }
                }
                view.EndSelection();
            }
            isRunning = false;
            Load_Linked_Job_Rate_Criteria();
            view = (GridView)gridControlJobRateCriteria.MainView;
            view.ExpandAllGroups();
            SetMenuStatus();
        }

        private void gridControlJobRate_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        Load_Linked_Job_Rates("", 0);
                    }
                    else if ("adjust rates".Equals(e.Button.Tag))
                    {
                        Adjust_Rates();
                    }
                    break;
                default:
                    break;
            }
        }


        private void Adjust_Rates()
        {
            GridView view = (GridView)gridControlJobRate.MainView; ;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strRecordIDs = "";
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to adjust the rates of before proceeding.", "Adjust Rates by Uplift Amount", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RateID")) + ',';
            }
            var fChildForm = new frm_AT_Job_Rate_Adjust_By_Uplift_Amount();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = strRecordIDs;
            fChildForm.intRecordCount = intCount;
            fChildForm.strFormMode = "edit";
            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                this.RefreshGridViewStateJobRate.SaveViewInfo();  // Store Grid View State //
                Load_Linked_Job_Rates("", 0);
            }
        }

        #endregion


        #region GridViewJobRateCriteria

        private void gridViewJobRateCriteria_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return; 
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "decRateUnits":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "intRateType")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "monRate":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "intRateType")) == 0)
                    {
                        e.RepositoryItem = CurrencyMaskEditor;
                    }
                    else
                    {
                        e.RepositoryItem = PercentageMaskEditor;
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewJobRateCriteria_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewJobRateCriteria_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeRateCriteria;
            SetMenuStatus();
        }

        private void gridViewJobRateCriteria_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = GridEnumerations.enmFocusedGrid.MasterJobTypeRateCriteria;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControlJobRateCriteria_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        Load_Linked_Job_Rate_Criteria();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }



 

    }
}


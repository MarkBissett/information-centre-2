using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_Core_Sequence_Edit : BaseObjects.frmBase
    {

        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        #endregion
     
        public frm_Core_Sequence_Edit()
        {
            InitializeComponent();
        }

        private void frm_Core_Sequence_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 151;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            this.sp03009_EP_Client_List_With_BlankTableAdapter.Fill(this.dataSet_EP_DataEntry.sp03009_EP_Client_List_With_Blank);

            sp00165_Sequence_Picklist_Fields_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00165_Sequence_Picklist_Fields_ListTableAdapter.Fill(woodPlanDataSet.sp00165_Sequence_Picklist_Fields_List);
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00164_Sequence_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Populate Main Dataset //
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp00164_Sequence_Item.NewRow();
                        drNewRow["intLengthOfNumericPart"] = 6;
                        this.woodPlanDataSet.sp00164_Sequence_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp00164_Sequence_ItemTableAdapter.Fill(this.woodPlanDataSet.sp00164_Sequence_Item, strRecordIDs);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Attempt to set focused row in GridLookupEdit so it can be used in editor Validation code //
            if (gridView1.DataRowCount > 0)
            {
                //int intFoundRow = gridView1.LocateByValue(0, gridView1.Columns["strField"], strFieldGridLookupEdit.EditValue.ToString());

                // Search on Multiple columns for matching gridview row //
                ExtendedGridViewFunctions extGridViewFunction;
                extGridViewFunction = new ExtendedGridViewFunctions();
                GridColumn[] cols = new GridColumn[] { colstrTable, colstrField };  // Array of columns to search for a match on //
                object[] values = new object[] { strTableTextEdit.EditValue.ToString(), strFieldGridLookupEdit.EditValue.ToString() };
                int intFoundRow = extGridViewFunction.LocateRowByMultipleValues(gridView1, cols, values, 0);  // Function declared in BaseObjects... Classes... ExtendedGridViewFunctions //

                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    gridView1.FocusedRowHandle = intFoundRow;
                }
            }

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // nabled Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.woodPlanDataSet.sp00164_Sequence_Item.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Sequence", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        strFieldGridLookupEdit.Focus();
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        strFieldGridLookupEdit.Focus();
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        strRemarksMemoExEdit.Focus();
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        strFieldGridLookupEdit.Focus();
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            Set_Client_And_Site_Enabled_Status();
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.woodPlanDataSet.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_Core_Sequence_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_Sequence_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
                    
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp00164SequenceItemBindingSource.EndEdit();
            try
            {
                this.sp00164_Sequence_ItemTableAdapter.Update(woodPlanDataSet);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp00164SequenceItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["intID"]) + ";";
            }
            
            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_Core_Sequence_Manager")
                    {
                        var fParentForm = (frm_Core_Sequence_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(true, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.woodPlanDataSet.sp00164_Sequence_Item.Rows.Count; i++)
            {
                switch (this.woodPlanDataSet.sp00164_Sequence_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();

        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                 //if (!dxValidationProvider1.Validate())
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else 
                {
                    Set_Client_And_Site_Enabled_Status();
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            //foreach (Control c in dxValidationProvider.GetInvalidControls())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    //strErrorMessages += "--> " + item.Text.ToString() + "  " + dxValidationProvider.GetValidationRule(c).ErrorText + "\n";
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void strFieldGridLookupEdit_EditValueChanged(object sender, EventArgs e)
        {
            // strField value changed so pick up it's parent strTable and enter in strTable field //
            GridView view = (GridView)gridView1;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                string strTable = view.GetRowCellValue(view.FocusedRowHandle, "strTable").ToString();
                strTableTextEdit.EditValue = strTable;
            }
        }

        private void strFieldGridLookupEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                Set_Client_And_Site_Enabled_Status();
                dxErrorProvider1.SetError(strFieldGridLookupEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(strFieldGridLookupEdit, "");
                if (string.IsNullOrEmpty(strSequenceTextEdit.EditValue.ToString()))
                {
                    GridView view = (GridView)gridView1;
                    int intBlankCount = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BlanksSequenceCount"));
                    int intBlankSequenceID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BlankSequenceID"));

                    DataRowView currentRow = (DataRowView)sp00164SequenceItemBindingSource.Current;
                    if (currentRow == null) return;
                    int intID = (int)currentRow["intID"];

                    //int rowNum = sp00164SequenceItemBindingSource.Position;
                    //if (rowNum < 0) return;
                    //DataRow dr = this.woodPlanDataSet.sp00164_Sequence_Item.Rows[rowNum];
                    //int? intID = Convert.ToInt32(dr["intID"]);
                    if (intBlankCount > 0 && intBlankSequenceID != intID)
                    {
                        dxErrorProvider1.SetError(strSequenceTextEdit, "Sequence cannot be blank - a blank Sequence is already present in the database for this Table and Field combination.");
                        Set_Client_And_Site_Enabled_Status();
                        return;
                    }
                    else
                    {
                        dxErrorProvider1.SetError(strSequenceTextEdit, "");
                    }
                }
                Set_Client_And_Site_Enabled_Status();
            }
        }

        private void Set_Client_And_Site_Enabled_Status()
        {
            sp00164SequenceItemBindingSource.EndEdit();
            DataRowView currentRow = (DataRowView)sp00164SequenceItemBindingSource.Current;
            if (currentRow == null) return;
            string strAllowedValues = ",AssetNumber,ActionNumber,InspectionNumber,";  // Values from stored proc: SP00165 - column strField //
            if (strAllowedValues.Contains("," + currentRow["strField"].ToString() + ","))  // Enable Linked Client and Site fields //
            {
                ClientNameButtonEdit.Properties.ReadOnly = false;
                ClientNameButtonEdit.Properties.Buttons[0].Enabled = true;
                ClientNameButtonEdit.Properties.Buttons[1].Enabled = true;
                SiteNameButtonEdit.Properties.ReadOnly = false;
                SiteNameButtonEdit.Properties.Buttons[0].Enabled = true;
                SiteNameButtonEdit.Properties.Buttons[1].Enabled = true;
            }
            else  // Disable Linked Client and Site fields //
            {
                ClientNameButtonEdit.Properties.ReadOnly = true;
                ClientNameButtonEdit.Properties.Buttons[0].Enabled = false;
                ClientNameButtonEdit.Properties.Buttons[1].Enabled = false;
                SiteNameButtonEdit.Properties.ReadOnly = true;
                SiteNameButtonEdit.Properties.Buttons[0].Enabled = false;
                SiteNameButtonEdit.Properties.Buttons[1].Enabled = false;

                // Make sure the linked Client and Site are cleared as they are not relevent to Amenity Trees //
                if (currentRow["ClientName"].ToString() != "N\\A" || currentRow["SiteName"].ToString() != "N\\A")
                {
                    currentRow["ClientID"] = 0;
                    currentRow["ClientName"] = "N\\A";
                    currentRow["SiteID"] = 0;
                    currentRow["SiteName"] = "N\\A";
                    sp00164SequenceItemBindingSource.EndEdit();
                }
            }
        }

        private void strSequenceTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (!string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                if (te.EditValue.ToString().Length + Convert.ToInt32(intLengthOfNumericPartSpinEdit.EditValue) > 20)
                {
                    dxErrorProvider1.SetError(strSequenceTextEdit, "Total length of Sequence and Length of Calculated Digits must not exceed 20.");
                    dxErrorProvider1.SetError(intLengthOfNumericPartSpinEdit, "Total length of Sequence and Length of Calculated Digits must not exceed 20.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
                else
                {
                    dxErrorProvider1.SetError(strSequenceTextEdit, "");
                    dxErrorProvider1.SetError(intLengthOfNumericPartSpinEdit, "");
                    return;
                }
            }
            else  // Sequence is blank //
            {
                dxErrorProvider1.SetError(strSequenceTextEdit, "");
                dxErrorProvider1.SetError(intLengthOfNumericPartSpinEdit, "");
       
                GridView view = (GridView)gridView1;
                int intBlankCount = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BlanksSequenceCount"));
                int intBlankSequenceID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BlankSequenceID"));


                DataRowView currentRow = (DataRowView)sp00164SequenceItemBindingSource.Current;
                if (currentRow == null) return;
                int intID = (int)currentRow["intID"];

                //int rowNum = sp00164SequenceItemBindingSource.Position;
                //if (rowNum < 0) return;
                //DataRow dr = this.woodPlanDataSet.sp00164_Sequence_Item.Rows[rowNum];
                //int? intID = Convert.ToInt32(dr["intID"]);
                if (intBlankCount > 0 && intBlankSequenceID != intID)
                {
                    dxErrorProvider1.SetError(strSequenceTextEdit, "Sequence cannot be blank - a blank Sequence is already present in the database for this Table and Field combination.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
            }
        }

        private void intLengthOfNumericPartSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if (!string.IsNullOrEmpty(strSequenceTextEdit.EditValue.ToString()))
            {
                if (strSequenceTextEdit.EditValue.ToString().Length + Convert.ToInt32(se.EditValue) > 20)
                {
                    dxErrorProvider1.SetError(strSequenceTextEdit, "Total length of Sequence and Length of Calculated Digits must not exceed 20.");
                    dxErrorProvider1.SetError(intLengthOfNumericPartSpinEdit, "Total length of Sequence and Length of Calculated Digits must not exceed 20.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
                else
                {
                     dxErrorProvider1.SetError(strSequenceTextEdit, "");
                     dxErrorProvider1.SetError(intLengthOfNumericPartSpinEdit, "");
                }
            }
        }

        private void ClientNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp00164SequenceItemBindingSource.Current;
            if (currentRow == null) return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the selected Client.\n\nAre you sure you wish to proceed?", "Clear Client", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    currentRow["ClientID"] = 0;
                    currentRow["ClientName"] = "N\\A";
                    currentRow["SiteID"] = 0;
                    currentRow["SiteName"] = "N\\A";
                    sp00164SequenceItemBindingSource.EndEdit();
                }
            }
            else
            {
                frm_EP_Select_Client fChildForm = new frm_EP_Select_Client();
                fChildForm.GlobalSettings = this.GlobalSettings;
                int intOriginalValue = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));
                fChildForm.intOriginalClientID = intOriginalValue;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    currentRow["ClientID"] = fChildForm.intSelectedClientID;
                    currentRow["ClientName"] = fChildForm.strSelectedClientName;
                    currentRow["SiteID"] = 0;
                    currentRow["SiteName"] = "N\\A";
                    sp00164SequenceItemBindingSource.EndEdit();
                }
            }
        }

        private void SiteNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp00164SequenceItemBindingSource.Current;
            if (currentRow == null) return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the selected Site.\n\nAre you sure you wish to proceed?", "Clear Site", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    currentRow["SiteID"] = 0;
                    currentRow["SiteName"] = "N\\A";
                    sp00164SequenceItemBindingSource.EndEdit();
                }
            }
            else
            {
                var fChildForm = new frm_Core_Select_Site();
                fChildForm.GlobalSettings = this.GlobalSettings;
                int intOriginalClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));
                fChildForm.intOriginalClientID = intOriginalClientID;
                int intOriginalSiteID = (string.IsNullOrEmpty(currentRow["SiteID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SiteID"]));
                fChildForm.intOriginalSiteID = intOriginalSiteID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    currentRow["SiteID"] = fChildForm.intSelectedID;
                    currentRow["SiteName"] = fChildForm.strSelectedValue;
                    currentRow["ClientID"] = fChildForm.intSelectedClientID;
                    currentRow["ClientName"] = fChildForm.strSelectedClientName;
                    sp00164SequenceItemBindingSource.EndEdit();
                }
            }
        }

        #endregion





    }
}

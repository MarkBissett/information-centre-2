using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frmAlternativeLogin : BaseObjects.frmBase
    {
        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private int intStaffID = 0;
        private string strNetworkID = "";
        
        public frmAlternativeLogin(int intUserID)
        {
            InitializeComponent();
            UserID = intUserID;
            strConnectionString = set.WoodPlanConnectionString;
        }

        public int UserID
        {
            get
            {
                return intStaffID;
            }
            set
            {
                intStaffID = value;
            }
        }

        public string NetworkID
        {
            get
            {
                return strNetworkID;
            }
            set
            {
                strNetworkID = value;
            }
        }

        private void frmAlternativeLogin_Load(object sender, EventArgs e)
        {
            sp00071_get_available_loginsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00071_get_available_loginsTableAdapter.Fill(this.woodPlanDataSet.sp00071_get_available_logins, intStaffID);

            AvailableLoginsLookUpEdit.EditValue = intStaffID;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            intStaffID = Convert.ToInt32(AvailableLoginsLookUpEdit.EditValue);
            strNetworkID = AvailableLoginsLookUpEdit.GetColumnValue("NetworkID").ToString();
            this.Close();
        }

        private void AvailableLoginsLookUpEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                btnOK_Click(sender, new EventArgs());
            }
        }
    }
}


namespace WoodPlan5
{
    partial class frm_Core_Accident_Picture_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Accident_Picture_Edit));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.PicturePathButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.sp00319AccidentPictureEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Accident = new WoodPlan5.DataSet_Accident();
            this.DateTimeTakenDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.AccidentDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccidentTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VisitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameContractDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccidentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddedByStaffNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.AccidentPictureIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.LinkedToAccidentButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForAccidentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentPictureID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedToAccident = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAddedByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateTimeTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientNameContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPicturePath = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00319_Accident_Picture_EditTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00319_Accident_Picture_EditTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicturePathButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00319AccidentPictureEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Accident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateTimeTakenDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateTimeTakenDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByStaffNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentPictureIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToAccidentButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentPictureID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToAccident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateTimeTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPicturePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 616);
            this.barDockControlBottom.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 590);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(686, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 590);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(686, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 616);
            this.barDockControl2.Size = new System.Drawing.Size(686, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 590);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(686, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 590);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.PicturePathButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.DateTimeTakenDateEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VisitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameContractDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddedByStaffNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentPictureIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.LinkedToAccidentButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp00319AccidentPictureEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAccidentID,
            this.ItemForAccidentType,
            this.ItemForAccidentDate,
            this.ItemForAccidentPictureID,
            this.ItemForAddedByStaffID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1234, 162, 301, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(686, 590);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // PicturePathButtonEdit
            // 
            this.PicturePathButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00319AccidentPictureEditBindingSource, "PicturePath", true));
            this.PicturePathButtonEdit.Location = new System.Drawing.Point(108, 131);
            this.PicturePathButtonEdit.MenuManager = this.barManager1;
            this.PicturePathButtonEdit.Name = "PicturePathButtonEdit";
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "Edit File - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to edit the selected image (draw on top of it) or capture an image with " +
    "the camera.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.PicturePathButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose File", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleLeft, ((System.Drawing.Image)(resources.GetObject("PicturePathButtonEdit.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Select File", "select file", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View File", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "view file", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit File", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "edit file", superToolTip4, true)});
            this.PicturePathButtonEdit.Properties.MaxLength = 50;
            this.PicturePathButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.PicturePathButtonEdit.Size = new System.Drawing.Size(566, 22);
            this.PicturePathButtonEdit.StyleController = this.dataLayoutControl1;
            this.PicturePathButtonEdit.TabIndex = 13;
            this.PicturePathButtonEdit.TabStop = false;
            this.PicturePathButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PicturePathButtonEdit_ButtonClick);
            this.PicturePathButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.PicturePathButtonEdit_Validating);
            // 
            // sp00319AccidentPictureEditBindingSource
            // 
            this.sp00319AccidentPictureEditBindingSource.DataMember = "sp00319_Accident_Picture_Edit";
            this.sp00319AccidentPictureEditBindingSource.DataSource = this.dataSet_Accident;
            // 
            // dataSet_Accident
            // 
            this.dataSet_Accident.DataSetName = "DataSet_Accident";
            this.dataSet_Accident.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DateTimeTakenDateEdit
            // 
            this.DateTimeTakenDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00319AccidentPictureEditBindingSource, "DateTimeTaken", true));
            this.DateTimeTakenDateEdit.EditValue = null;
            this.DateTimeTakenDateEdit.Location = new System.Drawing.Point(132, 262);
            this.DateTimeTakenDateEdit.MenuManager = this.barManager1;
            this.DateTimeTakenDateEdit.Name = "DateTimeTakenDateEdit";
            this.DateTimeTakenDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateTimeTakenDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateTimeTakenDateEdit.Size = new System.Drawing.Size(151, 20);
            this.DateTimeTakenDateEdit.StyleController = this.dataLayoutControl1;
            this.DateTimeTakenDateEdit.TabIndex = 75;
            // 
            // AccidentDateTextEdit
            // 
            this.AccidentDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00319AccidentPictureEditBindingSource, "AccidentDate", true));
            this.AccidentDateTextEdit.Location = new System.Drawing.Point(141, 341);
            this.AccidentDateTextEdit.MenuManager = this.barManager1;
            this.AccidentDateTextEdit.Name = "AccidentDateTextEdit";
            this.AccidentDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentDateTextEdit, true);
            this.AccidentDateTextEdit.Size = new System.Drawing.Size(509, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentDateTextEdit, optionsSpelling1);
            this.AccidentDateTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentDateTextEdit.TabIndex = 73;
            // 
            // AccidentTypeTextEdit
            // 
            this.AccidentTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00319AccidentPictureEditBindingSource, "AccidentType", true));
            this.AccidentTypeTextEdit.Location = new System.Drawing.Point(129, 353);
            this.AccidentTypeTextEdit.MenuManager = this.barManager1;
            this.AccidentTypeTextEdit.Name = "AccidentTypeTextEdit";
            this.AccidentTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentTypeTextEdit, true);
            this.AccidentTypeTextEdit.Size = new System.Drawing.Size(533, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentTypeTextEdit, optionsSpelling2);
            this.AccidentTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentTypeTextEdit.TabIndex = 72;
            // 
            // VisitNumberTextEdit
            // 
            this.VisitNumberTextEdit.Location = new System.Drawing.Point(108, 83);
            this.VisitNumberTextEdit.MenuManager = this.barManager1;
            this.VisitNumberTextEdit.Name = "VisitNumberTextEdit";
            this.VisitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitNumberTextEdit, true);
            this.VisitNumberTextEdit.Size = new System.Drawing.Size(566, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitNumberTextEdit, optionsSpelling3);
            this.VisitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitNumberTextEdit.TabIndex = 56;
            this.VisitNumberTextEdit.TabStop = false;
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.Location = new System.Drawing.Point(108, 59);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(566, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling4);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 55;
            this.SiteNameTextEdit.TabStop = false;
            // 
            // ClientNameContractDescriptionTextEdit
            // 
            this.ClientNameContractDescriptionTextEdit.Location = new System.Drawing.Point(108, 35);
            this.ClientNameContractDescriptionTextEdit.MenuManager = this.barManager1;
            this.ClientNameContractDescriptionTextEdit.Name = "ClientNameContractDescriptionTextEdit";
            this.ClientNameContractDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameContractDescriptionTextEdit, true);
            this.ClientNameContractDescriptionTextEdit.Size = new System.Drawing.Size(566, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameContractDescriptionTextEdit, optionsSpelling5);
            this.ClientNameContractDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameContractDescriptionTextEdit.TabIndex = 54;
            this.ClientNameContractDescriptionTextEdit.TabStop = false;
            // 
            // AccidentIDTextEdit
            // 
            this.AccidentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00319AccidentPictureEditBindingSource, "AccidentID", true));
            this.AccidentIDTextEdit.Location = new System.Drawing.Point(154, 534);
            this.AccidentIDTextEdit.MenuManager = this.barManager1;
            this.AccidentIDTextEdit.Name = "AccidentIDTextEdit";
            this.AccidentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentIDTextEdit, true);
            this.AccidentIDTextEdit.Size = new System.Drawing.Size(491, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentIDTextEdit, optionsSpelling6);
            this.AccidentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentIDTextEdit.TabIndex = 53;
            // 
            // AddedByStaffNameButtonEdit
            // 
            this.AddedByStaffNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00319AccidentPictureEditBindingSource, "AddedByStaffName", true));
            this.AddedByStaffNameButtonEdit.EditValue = "";
            this.AddedByStaffNameButtonEdit.Location = new System.Drawing.Point(132, 238);
            this.AddedByStaffNameButtonEdit.MenuManager = this.barManager1;
            this.AddedByStaffNameButtonEdit.Name = "AddedByStaffNameButtonEdit";
            this.AddedByStaffNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click me to open the Select Waste Type screen", "choose", null, true)});
            this.AddedByStaffNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.AddedByStaffNameButtonEdit.Size = new System.Drawing.Size(518, 20);
            this.AddedByStaffNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.AddedByStaffNameButtonEdit.TabIndex = 13;
            this.AddedByStaffNameButtonEdit.TabStop = false;
            this.AddedByStaffNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AddedByStaffNameButtonEdit_ButtonClick);
            // 
            // AccidentPictureIDTextEdit
            // 
            this.AccidentPictureIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00319AccidentPictureEditBindingSource, "AccidentPictureID", true));
            this.AccidentPictureIDTextEdit.Location = new System.Drawing.Point(173, 338);
            this.AccidentPictureIDTextEdit.MenuManager = this.barManager1;
            this.AccidentPictureIDTextEdit.Name = "AccidentPictureIDTextEdit";
            this.AccidentPictureIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentPictureIDTextEdit, true);
            this.AccidentPictureIDTextEdit.Size = new System.Drawing.Size(477, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentPictureIDTextEdit, optionsSpelling7);
            this.AccidentPictureIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentPictureIDTextEdit.TabIndex = 42;
            // 
            // AddedByStaffIDTextEdit
            // 
            this.AddedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00319AccidentPictureEditBindingSource, "AddedByStaffID", true));
            this.AddedByStaffIDTextEdit.Location = new System.Drawing.Point(173, 362);
            this.AddedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.AddedByStaffIDTextEdit.Name = "AddedByStaffIDTextEdit";
            this.AddedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddedByStaffIDTextEdit, true);
            this.AddedByStaffIDTextEdit.Size = new System.Drawing.Size(477, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddedByStaffIDTextEdit, optionsSpelling8);
            this.AddedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AddedByStaffIDTextEdit.TabIndex = 44;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00319AccidentPictureEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 238);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(614, 198);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling9);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 35;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00319AccidentPictureEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(108, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // LinkedToAccidentButtonEdit
            // 
            this.LinkedToAccidentButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00319AccidentPictureEditBindingSource, "LinkedToAccident", true));
            this.LinkedToAccidentButtonEdit.EditValue = "";
            this.LinkedToAccidentButtonEdit.Location = new System.Drawing.Point(108, 107);
            this.LinkedToAccidentButtonEdit.MenuManager = this.barManager1;
            this.LinkedToAccidentButtonEdit.Name = "LinkedToAccidentButtonEdit";
            this.LinkedToAccidentButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click me to open the Select Job screen", "choose", null, true)});
            this.LinkedToAccidentButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToAccidentButtonEdit.Size = new System.Drawing.Size(566, 20);
            this.LinkedToAccidentButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToAccidentButtonEdit.TabIndex = 6;
            this.LinkedToAccidentButtonEdit.TabStop = false;
            this.LinkedToAccidentButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToAccidentButtonEdit_ButtonClick);
            this.LinkedToAccidentButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LinkedToAccidentButtonEdit_Validating);
            // 
            // ItemForAccidentID
            // 
            this.ItemForAccidentID.Control = this.AccidentIDTextEdit;
            this.ItemForAccidentID.CustomizationFormText = "Accident ID:";
            this.ItemForAccidentID.Location = new System.Drawing.Point(0, 418);
            this.ItemForAccidentID.Name = "ItemForAccidentID";
            this.ItemForAccidentID.Size = new System.Drawing.Size(625, 24);
            this.ItemForAccidentID.Text = "Accident ID:";
            this.ItemForAccidentID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAccidentType
            // 
            this.ItemForAccidentType.Control = this.AccidentTypeTextEdit;
            this.ItemForAccidentType.CustomizationFormText = "Accident Type:";
            this.ItemForAccidentType.Location = new System.Drawing.Point(0, 154);
            this.ItemForAccidentType.Name = "ItemForAccidentType";
            this.ItemForAccidentType.Size = new System.Drawing.Size(642, 24);
            this.ItemForAccidentType.Text = "Accident Type:";
            this.ItemForAccidentType.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAccidentDate
            // 
            this.ItemForAccidentDate.Control = this.AccidentDateTextEdit;
            this.ItemForAccidentDate.CustomizationFormText = "Accident Date:";
            this.ItemForAccidentDate.Location = new System.Drawing.Point(0, 106);
            this.ItemForAccidentDate.Name = "ItemForAccidentDate";
            this.ItemForAccidentDate.Size = new System.Drawing.Size(618, 24);
            this.ItemForAccidentDate.Text = "Accident Date:";
            this.ItemForAccidentDate.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAccidentPictureID
            // 
            this.ItemForAccidentPictureID.Control = this.AccidentPictureIDTextEdit;
            this.ItemForAccidentPictureID.CustomizationFormText = "Accident Picture ID";
            this.ItemForAccidentPictureID.Location = new System.Drawing.Point(0, 103);
            this.ItemForAccidentPictureID.Name = "ItemForAccidentPictureID";
            this.ItemForAccidentPictureID.Size = new System.Drawing.Size(618, 24);
            this.ItemForAccidentPictureID.Text = "Accident Picture ID:";
            this.ItemForAccidentPictureID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAddedByStaffID
            // 
            this.ItemForAddedByStaffID.Control = this.AddedByStaffIDTextEdit;
            this.ItemForAddedByStaffID.CustomizationFormText = "Added By Staff ID:";
            this.ItemForAddedByStaffID.Location = new System.Drawing.Point(0, 127);
            this.ItemForAddedByStaffID.Name = "ItemForAddedByStaffID";
            this.ItemForAddedByStaffID.Size = new System.Drawing.Size(618, 24);
            this.ItemForAddedByStaffID.Text = "Added By Staff ID:";
            this.ItemForAddedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(686, 590);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedToAccident,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlGroup6,
            this.ItemForClientNameContractDescription,
            this.ItemForSiteName,
            this.ItemForVisitNumber,
            this.emptySpaceItem6,
            this.ItemForPicturePath});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(666, 452);
            // 
            // ItemForLinkedToAccident
            // 
            this.ItemForLinkedToAccident.AllowHide = false;
            this.ItemForLinkedToAccident.Control = this.LinkedToAccidentButtonEdit;
            this.ItemForLinkedToAccident.CustomizationFormText = "Linked To Accident:";
            this.ItemForLinkedToAccident.Location = new System.Drawing.Point(0, 95);
            this.ItemForLinkedToAccident.Name = "ItemForLinkedToAccident";
            this.ItemForLinkedToAccident.Size = new System.Drawing.Size(666, 24);
            this.ItemForLinkedToAccident.Text = "Linked To Accident:";
            this.ItemForLinkedToAccident.TextSize = new System.Drawing.Size(93, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(96, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(96, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(96, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(273, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(393, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(96, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 156);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(666, 296);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpAddress;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(642, 250);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpAddress,
            this.layoutControlGroup4});
            // 
            // layGrpAddress
            // 
            this.layGrpAddress.CustomizationFormText = "Address";
            this.layGrpAddress.ExpandButtonVisible = true;
            this.layGrpAddress.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.ItemForAddedByStaffName,
            this.ItemForDateTimeTaken,
            this.emptySpaceItem5});
            this.layGrpAddress.Location = new System.Drawing.Point(0, 0);
            this.layGrpAddress.Name = "layGrpAddress";
            this.layGrpAddress.Size = new System.Drawing.Size(618, 202);
            this.layGrpAddress.Text = "Details";
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(618, 154);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAddedByStaffName
            // 
            this.ItemForAddedByStaffName.Control = this.AddedByStaffNameButtonEdit;
            this.ItemForAddedByStaffName.CustomizationFormText = "Added By Staff:";
            this.ItemForAddedByStaffName.Location = new System.Drawing.Point(0, 0);
            this.ItemForAddedByStaffName.Name = "ItemForAddedByStaffName";
            this.ItemForAddedByStaffName.Size = new System.Drawing.Size(618, 24);
            this.ItemForAddedByStaffName.Text = "Added By Staff:";
            this.ItemForAddedByStaffName.TextSize = new System.Drawing.Size(93, 13);
            // 
            // ItemForDateTimeTaken
            // 
            this.ItemForDateTimeTaken.Control = this.DateTimeTakenDateEdit;
            this.ItemForDateTimeTaken.CustomizationFormText = "Date Taken:";
            this.ItemForDateTimeTaken.Location = new System.Drawing.Point(0, 24);
            this.ItemForDateTimeTaken.MaxSize = new System.Drawing.Size(251, 24);
            this.ItemForDateTimeTaken.MinSize = new System.Drawing.Size(251, 24);
            this.ItemForDateTimeTaken.Name = "ItemForDateTimeTaken";
            this.ItemForDateTimeTaken.Size = new System.Drawing.Size(251, 24);
            this.ItemForDateTimeTaken.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDateTimeTaken.Text = "Date Taken:";
            this.ItemForDateTimeTaken.TextSize = new System.Drawing.Size(93, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(251, 24);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(367, 24);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(618, 202);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(618, 202);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForClientNameContractDescription
            // 
            this.ItemForClientNameContractDescription.Control = this.ClientNameContractDescriptionTextEdit;
            this.ItemForClientNameContractDescription.CustomizationFormText = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientNameContractDescription.Name = "ItemForClientNameContractDescription";
            this.ItemForClientNameContractDescription.Size = new System.Drawing.Size(666, 24);
            this.ItemForClientNameContractDescription.Text = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.TextSize = new System.Drawing.Size(93, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 47);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(666, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(93, 13);
            // 
            // ItemForVisitNumber
            // 
            this.ItemForVisitNumber.Control = this.VisitNumberTextEdit;
            this.ItemForVisitNumber.CustomizationFormText = "Visit Number:";
            this.ItemForVisitNumber.Location = new System.Drawing.Point(0, 71);
            this.ItemForVisitNumber.Name = "ItemForVisitNumber";
            this.ItemForVisitNumber.Size = new System.Drawing.Size(666, 24);
            this.ItemForVisitNumber.Text = "Visit Number:";
            this.ItemForVisitNumber.TextSize = new System.Drawing.Size(93, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 145);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 11);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 11);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(666, 11);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPicturePath
            // 
            this.ItemForPicturePath.Control = this.PicturePathButtonEdit;
            this.ItemForPicturePath.CustomizationFormText = "Picture Path:";
            this.ItemForPicturePath.Location = new System.Drawing.Point(0, 119);
            this.ItemForPicturePath.Name = "ItemForPicturePath";
            this.ItemForPicturePath.Size = new System.Drawing.Size(666, 26);
            this.ItemForPicturePath.Text = "Picture Path:";
            this.ItemForPicturePath.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 452);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(666, 118);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(666, 118);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00319_Accident_Picture_EditTableAdapter
            // 
            this.sp00319_Accident_Picture_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_Accident_Picture_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(686, 644);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Accident_Picture_Edit";
            this.Text = "Edit Accident Picture";
            this.Activated += new System.EventHandler(this.frm_Core_Accident_Picture_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Accident_Picture_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Accident_Picture_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicturePathButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00319AccidentPictureEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Accident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateTimeTakenDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateTimeTakenDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByStaffNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentPictureIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToAccidentButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentPictureID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToAccident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddedByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateTimeTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPicturePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToAccident;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.ButtonEdit LinkedToAccidentButtonEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpAddress;
        private DevExpress.XtraEditors.TextEdit AddedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddedByStaffID;
        private DevExpress.XtraEditors.TextEdit AccidentPictureIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentPictureID;
        private DevExpress.XtraEditors.ButtonEdit AddedByStaffNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddedByStaffName;
        private DevExpress.XtraEditors.TextEdit AccidentIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentID;
        private DevExpress.XtraEditors.TextEdit ClientNameContractDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientNameContractDescription;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.TextEdit VisitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DataSet_Accident dataSet_Accident;
        private DevExpress.XtraEditors.TextEdit AccidentTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentType;
        private DevExpress.XtraEditors.TextEdit AccidentDateTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentDate;
        private System.Windows.Forms.BindingSource sp00319AccidentPictureEditBindingSource;
        private DataSet_AccidentTableAdapters.sp00319_Accident_Picture_EditTableAdapter sp00319_Accident_Picture_EditTableAdapter;
        private DevExpress.XtraEditors.DateEdit DateTimeTakenDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateTimeTaken;
        private DevExpress.XtraEditors.ButtonEdit PicturePathButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPicturePath;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

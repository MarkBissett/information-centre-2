using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Select_Team_Contact_Email : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //
        public string _PassedInTeamIDs = "";
        public int intOriginalID = 0;
        public string strOriginalIDs = "";
        public int intSelectedID = 0;
        public string strSelectedIDs = "";
        public string strSelectedContactName = "";
        public string strSelectedEmailAddress = "";
        public string strSelectedTeamIDs = "";
        public int intSelectedTeamID = 0;
        public int _SelectedCount = 0;
        BaseObjects.GridCheckMarksSelection selection1;

        SqlDataAdapter sdaData_Recipients = new SqlDataAdapter();  // This is populated by caller [Optional] //
        DataSet dsData_Recipients = new DataSet("NewDataSet");  // This is populated by caller [Optional] //

        #endregion

        public frm_Core_Select_Team_Contact_Email()
        {
            InitializeComponent();
        }

        private void frm_Core_Select_Team_Contact_Email_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 412;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp01017_Core_Contractor_Email_AddressesTableAdapter.Connection.ConnectionString = strConnectionString;
            GridView view = (GridView)gridControl1.MainView;

            LoadData();
            gridControl1.ForceInitialize();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.Width = 30;
                selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                selection1.CheckMarkColumn.VisibleIndex = 0;

                Array arrayRecords = strOriginalIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["ContactID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["ContactID"], intOriginalID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            view.ExpandAllGroups();
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01017_Core_Contractor_Email_AddressesTableAdapter.Fill(dataSet_Common_Functionality.sp01017_Core_Contractor_Email_Addresses, _PassedInTeamIDs);            
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Team Contacts Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(strSelectedIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (intSelectedID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (_Mode != "single")
            {
                strSelectedIDs = "";    // Reset any prior values first //
                strSelectedContactName = "";  // Reset any prior values first //
                string strDescriptor1 = "";
                string strDescriptor2 = "";
                int intTeamID = 0;
                strSelectedTeamIDs = ",";

                if (selection1.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, "ContactID")) + ",";
                        strDescriptor1 = Convert.ToString(view.GetRowCellValue(i, "ContactName"));
                        strDescriptor2 = Convert.ToString(view.GetRowCellValue(i, "Email"));
                        intTeamID = Convert.ToInt32(view.GetRowCellValue(i, "ContractorID"));
                        
                        if (intCount == 0)
                        {
                            strSelectedContactName = strDescriptor1;
                            strSelectedEmailAddress = strDescriptor2;
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedContactName += ", " + strDescriptor1;
                            strSelectedEmailAddress = strDescriptor2;
                        }
                        if (!strSelectedTeamIDs.Contains("," + view.GetRowCellValue(i, "ContractorID").ToString() + ",")) strSelectedTeamIDs += view.GetRowCellValue(i, "ContractorID").ToString() + ",";
                        intCount++;
                    }
                }
                if (!string.IsNullOrWhiteSpace(strSelectedTeamIDs)) strSelectedTeamIDs.TrimStart(',');
                _SelectedCount = intCount;
            }
            else  // Single record selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    var currentRowView = (DataRowView)sp01003ClientContactSelectBindingSource.Current;
                    var currentRow = (DataSet_Common_Functionality.sp01017_Core_Contractor_Email_AddressesRow)currentRowView.Row;
                    if (currentRow == null) return;
                    intSelectedID = (string.IsNullOrEmpty(currentRow.ContactID.ToString()) ? 0 : currentRow.ContactID);
                    strSelectedContactName = (string.IsNullOrEmpty(currentRow.ContactName.ToString()) ? "" : currentRow.ContactName);
                    strSelectedEmailAddress = (string.IsNullOrEmpty(currentRow.Email.ToString()) ? "" : currentRow.Email);
                    intSelectedTeamID = (string.IsNullOrEmpty(currentRow.ContractorID.ToString()) ? 0 : currentRow.ContractorID);
                }
            }
        }




    }
}


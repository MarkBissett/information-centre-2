namespace WoodPlan5
{
    partial class frm_Core_Billing_Requirement_Template_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Billing_Requirement_Template_Manager));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01029CoreBillingRequirementTemplateHeadersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBillingRequirementTemplateHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.repositoryItemCheckEditShowActiveEmployeesOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp01030CoreBillingRequirementTemplateItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBillingRequirementTemplateItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingRequirementTemplateHeaderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingRequirementID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colBillingRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientBillRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTeamSelfBillRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sp01029_Core_Billing_Requirement_Template_HeadersTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01029_Core_Billing_Requirement_Template_HeadersTableAdapter();
            this.sp01030_Core_Billing_Requirement_Template_ItemsTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01030_Core_Billing_Requirement_Template_ItemsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01029CoreBillingRequirementTemplateHeadersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveEmployeesOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01030CoreBillingRequirementTemplateItemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(805, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 557);
            this.barDockControlBottom.Size = new System.Drawing.Size(805, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 557);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(805, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 557);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.MaxItemId = 33;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemCheckEditShowActiveEmployeesOnly});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 49;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01029CoreBillingRequirementTemplateHeadersBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Reload Data", "reload")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(780, 296);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01029CoreBillingRequirementTemplateHeadersBindingSource
            // 
            this.sp01029CoreBillingRequirementTemplateHeadersBindingSource.DataMember = "sp01029_Core_Billing_Requirement_Template_Headers";
            this.sp01029CoreBillingRequirementTemplateHeadersBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingRequirementTemplateHeaderID,
            this.colHeaderDescription1,
            this.colCreatedByStaffID,
            this.colActive,
            this.colRemarks1,
            this.colCreatedByStaffName});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHeaderDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colBillingRequirementTemplateHeaderID
            // 
            this.colBillingRequirementTemplateHeaderID.Caption = "Template Header ID";
            this.colBillingRequirementTemplateHeaderID.FieldName = "BillingRequirementTemplateHeaderID";
            this.colBillingRequirementTemplateHeaderID.Name = "colBillingRequirementTemplateHeaderID";
            this.colBillingRequirementTemplateHeaderID.OptionsColumn.ReadOnly = true;
            this.colBillingRequirementTemplateHeaderID.Width = 115;
            // 
            // colHeaderDescription1
            // 
            this.colHeaderDescription1.Caption = "Template Description";
            this.colHeaderDescription1.FieldName = "HeaderDescription";
            this.colHeaderDescription1.Name = "colHeaderDescription1";
            this.colHeaderDescription1.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription1.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription1.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription1.Visible = true;
            this.colHeaderDescription1.VisibleIndex = 0;
            this.colHeaderDescription1.Width = 418;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 87;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.AllowEdit = false;
            this.colRemarks1.OptionsColumn.AllowFocus = false;
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 3;
            this.colRemarks1.Width = 110;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By Name";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Visible = true;
            this.colCreatedByStaffName.VisibleIndex = 2;
            this.colCreatedByStaffName.Width = 141;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemCheckEditShowActiveEmployeesOnly
            // 
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Caption = "";
            this.repositoryItemCheckEditShowActiveEmployeesOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.Name = "repositoryItemCheckEditShowActiveEmployeesOnly";
            this.repositoryItemCheckEditShowActiveEmployeesOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveEmployeesOnly.ValueUnchecked = 0;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Template Headers";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Linked Template Items";
            this.splitContainerControl1.Size = new System.Drawing.Size(805, 557);
            this.splitContainerControl1.SplitterPosition = 252;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(780, 296);
            this.gridSplitContainer1.TabIndex = 2;
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl8;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl8);
            this.gridSplitContainer2.Size = new System.Drawing.Size(780, 249);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl8
            // 
            this.gridControl8.DataSource = this.sp01030CoreBillingRequirementTemplateItemsBindingSource;
            this.gridControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Reload Data", "reload")});
            this.gridControl8.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl8_EmbeddedNavigator_ButtonClick);
            this.gridControl8.Location = new System.Drawing.Point(0, 0);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemCheckEdit2});
            this.gridControl8.Size = new System.Drawing.Size(780, 249);
            this.gridControl8.TabIndex = 3;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp01030CoreBillingRequirementTemplateItemsBindingSource
            // 
            this.sp01030CoreBillingRequirementTemplateItemsBindingSource.DataMember = "sp01030_Core_Billing_Requirement_Template_Items";
            this.sp01030CoreBillingRequirementTemplateItemsBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingRequirementTemplateItemID,
            this.colBillingRequirementTemplateHeaderID1,
            this.colBillingRequirementID,
            this.colRemarks,
            this.colBillingRequirement,
            this.colHeaderDescription,
            this.colClientBillRequirement,
            this.colTeamSelfBillRequirement});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 1;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowFilterEditor = false;
            this.gridView8.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView8.OptionsFilter.AllowMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView8.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView8.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView8.OptionsFind.FindDelay = 2000;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsLayout.StoreFormatRules = true;
            this.gridView8.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHeaderDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView8_CustomDrawCell);
            this.gridView8.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView8_CustomRowCellEdit);
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView8.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.DoubleClick += new System.EventHandler(this.gridView8_DoubleClick);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colBillingRequirementTemplateItemID
            // 
            this.colBillingRequirementTemplateItemID.Caption = "Template Item ID";
            this.colBillingRequirementTemplateItemID.FieldName = "BillingRequirementTemplateItemID";
            this.colBillingRequirementTemplateItemID.Name = "colBillingRequirementTemplateItemID";
            this.colBillingRequirementTemplateItemID.OptionsColumn.AllowEdit = false;
            this.colBillingRequirementTemplateItemID.OptionsColumn.AllowFocus = false;
            this.colBillingRequirementTemplateItemID.OptionsColumn.ReadOnly = true;
            this.colBillingRequirementTemplateItemID.Width = 102;
            // 
            // colBillingRequirementTemplateHeaderID1
            // 
            this.colBillingRequirementTemplateHeaderID1.Caption = "Template Header ID";
            this.colBillingRequirementTemplateHeaderID1.FieldName = "BillingRequirementTemplateHeaderID";
            this.colBillingRequirementTemplateHeaderID1.Name = "colBillingRequirementTemplateHeaderID1";
            this.colBillingRequirementTemplateHeaderID1.OptionsColumn.AllowEdit = false;
            this.colBillingRequirementTemplateHeaderID1.OptionsColumn.AllowFocus = false;
            this.colBillingRequirementTemplateHeaderID1.OptionsColumn.ReadOnly = true;
            this.colBillingRequirementTemplateHeaderID1.Width = 115;
            // 
            // colBillingRequirementID
            // 
            this.colBillingRequirementID.Caption = "Billing Requirement ID";
            this.colBillingRequirementID.FieldName = "BillingRequirementID";
            this.colBillingRequirementID.Name = "colBillingRequirementID";
            this.colBillingRequirementID.OptionsColumn.AllowEdit = false;
            this.colBillingRequirementID.OptionsColumn.AllowFocus = false;
            this.colBillingRequirementID.OptionsColumn.ReadOnly = true;
            this.colBillingRequirementID.Width = 123;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 3;
            this.colRemarks.Width = 89;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colBillingRequirement
            // 
            this.colBillingRequirement.Caption = "Billing Requirement";
            this.colBillingRequirement.FieldName = "BillingRequirement";
            this.colBillingRequirement.Name = "colBillingRequirement";
            this.colBillingRequirement.OptionsColumn.AllowEdit = false;
            this.colBillingRequirement.OptionsColumn.AllowFocus = false;
            this.colBillingRequirement.OptionsColumn.ReadOnly = true;
            this.colBillingRequirement.Visible = true;
            this.colBillingRequirement.VisibleIndex = 0;
            this.colBillingRequirement.Width = 343;
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Template Header Description";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.Visible = true;
            this.colHeaderDescription.VisibleIndex = 2;
            this.colHeaderDescription.Width = 356;
            // 
            // colClientBillRequirement
            // 
            this.colClientBillRequirement.Caption = "Client Bill Requirement";
            this.colClientBillRequirement.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colClientBillRequirement.FieldName = "ClientBillRequirement";
            this.colClientBillRequirement.Name = "colClientBillRequirement";
            this.colClientBillRequirement.OptionsColumn.AllowEdit = false;
            this.colClientBillRequirement.OptionsColumn.AllowFocus = false;
            this.colClientBillRequirement.OptionsColumn.ReadOnly = true;
            this.colClientBillRequirement.Visible = true;
            this.colClientBillRequirement.VisibleIndex = 1;
            this.colClientBillRequirement.Width = 125;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colTeamSelfBillRequirement
            // 
            this.colTeamSelfBillRequirement.Caption = "Team Self-Bill Requirement";
            this.colTeamSelfBillRequirement.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTeamSelfBillRequirement.FieldName = "TeamSelfBillRequirement";
            this.colTeamSelfBillRequirement.Name = "colTeamSelfBillRequirement";
            this.colTeamSelfBillRequirement.OptionsColumn.AllowEdit = false;
            this.colTeamSelfBillRequirement.OptionsColumn.AllowFocus = false;
            this.colTeamSelfBillRequirement.OptionsColumn.ReadOnly = true;
            this.colTeamSelfBillRequirement.Visible = true;
            this.colTeamSelfBillRequirement.VisibleIndex = 2;
            this.colTeamSelfBillRequirement.Width = 146;
            // 
            // gridView3
            // 
            this.gridView3.Name = "gridView3";
            // 
            // sp01029_Core_Billing_Requirement_Template_HeadersTableAdapter
            // 
            this.sp01029_Core_Billing_Requirement_Template_HeadersTableAdapter.ClearBeforeFill = true;
            // 
            // sp01030_Core_Billing_Requirement_Template_ItemsTableAdapter
            // 
            this.sp01030_Core_Billing_Requirement_Template_ItemsTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Billing_Requirement_Template_Manager
            // 
            this.ClientSize = new System.Drawing.Size(805, 557);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Billing_Requirement_Template_Manager";
            this.Text = "Billing Requirement Template Manager";
            this.Activated += new System.EventHandler(this.frm_Core_Billing_Requirement_Template_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Billing_Requirement_Template_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Billing_Requirement_Template_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01029CoreBillingRequirementTemplateHeadersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveEmployeesOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01030CoreBillingRequirementTemplateItemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveEmployeesOnly;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private System.Windows.Forms.BindingSource sp01029CoreBillingRequirementTemplateHeadersBindingSource;
        private DataSet_Common_FunctionalityTableAdapters.sp01029_Core_Billing_Requirement_Template_HeadersTableAdapter sp01029_Core_Billing_Requirement_Template_HeadersTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirementTemplateHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.BindingSource sp01030CoreBillingRequirementTemplateItemsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirementTemplateItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirementTemplateHeaderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirementID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingRequirement;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DataSet_Common_FunctionalityTableAdapters.sp01030_Core_Billing_Requirement_Template_ItemsTableAdapter sp01030_Core_Billing_Requirement_Template_ItemsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientBillRequirement;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamSelfBillRequirement;
    }
}

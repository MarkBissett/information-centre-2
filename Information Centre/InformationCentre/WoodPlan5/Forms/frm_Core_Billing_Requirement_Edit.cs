using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class frm_Core_Billing_Requirement_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int ParentRecordId { get; set; }
        public string ParentRecordDescription { get; set; }
        public int LinkedToRecordTypeID { get; set; }
        public string LinkedToRecordType { get; set; }
 
        #endregion


        public frm_Core_Billing_Requirement_Edit()
        {
            InitializeComponent();
        }

        private void frm_Core_Billing_Requirement_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500269;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp01036_Core_Billing_Requirements_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01036_Core_Billing_Requirements_With_BlankTableAdapter.Fill(dataSet_Common_Functionality.sp01036_Core_Billing_Requirements_With_Blank, 1);

            // Populate Main Dataset //
            sp01038_Core_Billing_Requirement_Item_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        var drNewRow = dataSet_Common_Functionality.sp01038_Core_Billing_Requirement_Item_Edit.Newsp01038_Core_Billing_Requirement_Item_EditRow();
                        drNewRow.Mode = strFormMode;
                        drNewRow.RecordIds = strRecordIDs;
                        drNewRow.LinkedToRecordID = ParentRecordId;
                        drNewRow.LinkedToRecord = ParentRecordDescription;
                        drNewRow.LinkedToRecordTypeID = LinkedToRecordTypeID;
                        drNewRow.LinkedToRecordType = LinkedToRecordType;
                        drNewRow.RequirementID = 0;
                        dataSet_Common_Functionality.sp01038_Core_Billing_Requirement_Item_Edit.Addsp01038_Core_Billing_Requirement_Item_EditRow(drNewRow);
                        sp01038CoreBillingRequirementItemEditBindingSource.EndEdit();
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        var drNewRow = dataSet_Common_Functionality.sp01038_Core_Billing_Requirement_Item_Edit.Newsp01038_Core_Billing_Requirement_Item_EditRow();
                        drNewRow.Mode = strFormMode;
                        drNewRow.RecordIds = strRecordIDs;
                        dataSet_Common_Functionality.sp01038_Core_Billing_Requirement_Item_Edit.Addsp01038_Core_Billing_Requirement_Item_EditRow(drNewRow);
                        drNewRow.AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp01038_Core_Billing_Requirement_Item_EditTableAdapter.Fill(dataSet_Common_Functionality.sp01038_Core_Billing_Requirement_Item_Edit, strFormMode, strRecordIDs, LinkedToRecordTypeID);
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (dataSet_Common_Functionality.sp01038_Core_Billing_Requirement_Item_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Billing Requirement", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
         }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        LinkedToRecordButtonEdit.Focus();

                        LinkedToRecordButtonEdit.Properties.ReadOnly = false;

                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount;
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        RequirementIDGridLookUpEdit.Focus();

                        LinkedToRecordButtonEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        RequirementIDGridLookUpEdit.Focus();

                        LinkedToRecordButtonEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        RequirementIDGridLookUpEdit.Focus();

                        LinkedToRecordButtonEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            if (LinkedToRecordTypeID != 3)
            {
                ItemForRequirementFulfilled.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                ItemForCheckedByStaff.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                RequirementFulfilledCheckEdit.ReadOnly = true;
            }
            else
            {
                ItemForRequirementFulfilled.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                ItemForCheckedByStaff.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                RequirementFulfilledCheckEdit.ReadOnly = false;

            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            ibool_ignoreValidation = false;
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = dataSet_Common_Functionality.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_Core_Billing_Requirement_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_Billing_Requirement_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp01038CoreBillingRequirementItemEditBindingSource.EndEdit();
            try
            {
                sp01038_Core_Billing_Requirement_Item_EditTableAdapter.Update(dataSet_Common_Functionality);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)sp01038CoreBillingRequirementItemEditBindingSource.Current;
                var currentRow = (DataSet_Common_Functionality.sp01038_Core_Billing_Requirement_Item_EditRow)currentRowView.Row;
                if (currentRow != null) strNewIDs = currentRow.BillingRequirementID + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow.Mode = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                var currentRowView = (DataRowView)sp01038CoreBillingRequirementItemEditBindingSource.Current;
                var currentRow = (DataSet_Common_Functionality.sp01038_Core_Billing_Requirement_Item_EditRow)currentRowView.Row;
                this.strFormMode = "blockedit";  // Switch mode to BlockEdit so than any subsequent changes update these record //
                if (currentRow != null)
                {
                    currentRow.Mode = "blockedit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = Convert.ToString(currentRow["RecordIDs"]);  // Values returned from Update SP //
                }
            }

            // Notify any open instances of Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    switch (frmChild.Name)
                    {
                        case "frm_OM_Client_Contract_Manager":
                            {
                                var fParentForm = (frm_OM_Client_Contract_Manager)frmChild;
                                fParentForm.UpdateFormRefreshStatus(2, WoodPlan5.Classes.Operations.Utils.enmFocusedGrid.BillingRequirement, strNewIDs);
                            }
                            break;
                        case "frm_OM_Site_Contract_Manager":
                            {
                                var fParentForm = (frm_OM_Site_Contract_Manager)frmChild;
                                fParentForm.UpdateFormRefreshStatus(2, WoodPlan5.Classes.Operations.Utils.enmFocusedGrid.BillingRequirement, strNewIDs);
                            }
                            break;
                        case "frm_OM_Visit_Manager":
                            {
                                var fParentForm = (frm_OM_Visit_Manager)frmChild;
                                fParentForm.UpdateFormRefreshStatus(13, WoodPlan5.Classes.Operations.Utils.enmFocusedGrid.BillingRequirement, strNewIDs);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_Common_Functionality.sp01038_Core_Billing_Requirement_Item_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_Common_Functionality.sp01038_Core_Billing_Requirement_Item_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void LinkedToRecordButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp01038CoreBillingRequirementItemEditBindingSource.Current;
            if (currentRow == null) return;
            int RecordTypeID = (string.IsNullOrEmpty(currentRow["LinkedToRecordTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordTypeID"]));
            if (RecordTypeID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Linked Record Type before proceeding.", "Link Parent Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intOriginalID = (string.IsNullOrEmpty(currentRow["LinkedToRecordID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordID"]));
            switch (LinkedToRecordTypeID)
            {
                case 1:  // Client Contract // 
                    {
                        var fChildForm = new frm_OM_Select_Client_Contract();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalSelectedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedToRecord"] = fChildForm.strSelectedDescription;
                            sp01038CoreBillingRequirementItemEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 2:  // Site Contract // 
                    {
                        var fChildForm = new frm_OM_Select_Site_Contract();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalParentSelectedID = 0;
                        fChildForm.intOriginalChildSelectedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedChildID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedChildID;
                            currentRow["LinkedToRecord"] = fChildForm.strSelectedChildDescription;
                            sp01038CoreBillingRequirementItemEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 3:  // Visit // 
                    {
                        var fChildForm = new frm_OM_Select_Visit();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalClientContractID = 0;
                        fChildForm.intOriginalSiteContractID = 0;
                        fChildForm.intOriginalVisitID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedVisitID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedVisitID;
                            currentRow["LinkedToRecord"] = fChildForm.strSelectedChildDescription;
                            sp01038CoreBillingRequirementItemEditBindingSource.EndEdit();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        private void LinkedToRecordButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit glue = (ButtonEdit)sender;
            string strValue = glue.EditValue.ToString();
            if (this.strFormMode != "blockadd" && this.strFormMode != "blockedit" && String.IsNullOrEmpty(strValue))
            {
                dxErrorProvider1.SetError(LinkedToRecordButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(LinkedToRecordButtonEdit, "");
            }
        }

        private void RequirementIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(RequirementIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(RequirementIDGridLookUpEdit, "");
            }
        }

        private void RequirementFulfilledCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void RequirementFulfilledCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;
            //if (this.strFormMode == "blockedit") return;

            CheckEdit ce = (CheckEdit)sender;
            bool boolChecked = ce.Checked;
            
            DataRowView currentRow = (DataRowView)sp01038CoreBillingRequirementItemEditBindingSource.Current;
            if (currentRow == null) return;
            currentRow["CheckedByStaffID"] = (boolChecked ? GlobalSettings.UserID : 0);
            currentRow["CheckedByStaff"] = (boolChecked ? GlobalSettings.UserForename + " " + GlobalSettings.UserSurname : "");
            sp01038CoreBillingRequirementItemEditBindingSource.EndEdit();
        }
        
        #endregion

 
 






 



    }
}


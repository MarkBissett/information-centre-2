﻿namespace WoodPlan5
{
    partial class frm_Core_Generic_Mapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Generic_Mapping));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            this.myMap = new GMap.NET.WindowsForms.GMapControl();
            this.trackBar1 = new DevExpress.XtraEditors.TrackBarControl();
            this.textEditStartLocation = new DevExpress.XtraEditors.TextEdit();
            this.btnGetRoute = new DevExpress.XtraEditors.SimpleButton();
            this.textEditEndLocation = new DevExpress.XtraEditors.TextEdit();
            this.czuZoomUp = new DevExpress.XtraEditors.SimpleButton();
            this.czuZoomDown = new DevExpress.XtraEditors.SimpleButton();
            this.btnClearRoute = new DevExpress.XtraEditors.SimpleButton();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiNone = new DevExpress.XtraBars.BarCheckItem();
            this.bciAddMarker = new DevExpress.XtraBars.BarCheckItem();
            this.bciAddPolygon = new DevExpress.XtraBars.BarCheckItem();
            this.bciAddPolyline = new DevExpress.XtraBars.BarCheckItem();
            this.bciEditPolygon = new DevExpress.XtraBars.BarCheckItem();
            this.bciEditPolyline = new DevExpress.XtraBars.BarCheckItem();
            this.bciMeasureArea = new DevExpress.XtraBars.BarCheckItem();
            this.bciMeasureLength = new DevExpress.XtraBars.BarCheckItem();
            this.bciScaleBar = new DevExpress.XtraBars.BarCheckItem();
            this.bciZoom = new DevExpress.XtraBars.BarCheckItem();
            this.bciRouting = new DevExpress.XtraBars.BarCheckItem();
            this.bciFindPlace = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.beiMapType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBoxMapType = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiZoom = new DevExpress.XtraBars.BarStaticItem();
            this.bsiPositionXY = new DevExpress.XtraBars.BarStaticItem();
            this.bsiMarkerCount = new DevExpress.XtraBars.BarStaticItem();
            this.beiProgress = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemMarqueeProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar();
            this.bbiScaleToFit = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPanLeft = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUp = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDown = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRight = new DevExpress.XtraBars.BarButtonItem();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnLoadSites = new DevExpress.XtraEditors.SimpleButton();
            this.btnClearSites = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.dockPanelMapZoom = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dockPanelFindPlace = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer2 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.buttonEditFindPlace = new DevExpress.XtraEditors.ButtonEdit();
            this.dockPanelRouting = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pmMapMarker = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiAddPolygon = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreatePolyline = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteMarker = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemPadding = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStartLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEndLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxMapType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            this.dockPanelMapZoom.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.dockPanelFindPlace.SuspendLayout();
            this.controlContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditFindPlace.Properties)).BeginInit();
            this.dockPanelRouting.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmMapMarker)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1035, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 509);
            this.barDockControlBottom.Size = new System.Drawing.Size(1035, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 467);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1035, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 467);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar1});
            this.barManager1.Controller = this.barAndDockingController1;
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiPositionXY,
            this.bsiMarkerCount,
            this.beiProgress,
            this.beiMapType,
            this.bbiUp,
            this.bbiDown,
            this.bbiPanLeft,
            this.bbiRight,
            this.bciRouting,
            this.bciZoom,
            this.bciAddMarker,
            this.bciFindPlace,
            this.bbiDeleteMarker,
            this.bbiNone,
            this.bsiZoom,
            this.bbiScaleToFit,
            this.barStaticItem1,
            this.barStaticItemPadding,
            this.barButtonItem1,
            this.bciAddPolygon,
            this.bciAddPolyline,
            this.bciMeasureArea,
            this.bciMeasureLength,
            this.bciEditPolygon,
            this.bciEditPolyline,
            this.bciScaleBar,
            this.bbiAddPolygon,
            this.bbiCreatePolyline});
            this.barManager1.MaxItemId = 61;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMarqueeProgressBar1,
            this.repositoryItemComboBoxMapType,
            this.repositoryItemPopupContainerEdit1});
            this.barManager1.StatusBar = this.bar1;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // myMap
            // 
            this.myMap.Bearing = 0F;
            this.myMap.CanDragMap = true;
            this.myMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myMap.EmptyTileColor = System.Drawing.Color.Navy;
            this.myMap.GrayScaleMode = false;
            this.myMap.LevelsKeepInMemmory = 5;
            this.myMap.Location = new System.Drawing.Point(0, 0);
            this.myMap.MarkersEnabled = true;
            this.myMap.MaxZoom = 2;
            this.myMap.MinZoom = 2;
            this.myMap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.myMap.Name = "myMap";
            this.myMap.NegativeMode = false;
            this.myMap.PolygonsEnabled = true;
            this.myMap.RetryLoadTile = 0;
            this.myMap.RoutesEnabled = true;
            this.myMap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.myMap.ShowTileGridLines = false;
            this.myMap.Size = new System.Drawing.Size(773, 467);
            this.myMap.TabIndex = 4;
            this.myMap.Zoom = 0D;
            this.myMap.Paint += new System.Windows.Forms.PaintEventHandler(this.myMap_Paint);
            this.myMap.DoubleClick += new System.EventHandler(this.myMap_DoubleClick);
            this.myMap.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.myMap_KeyPress);
            this.myMap.MouseClick += new System.Windows.Forms.MouseEventHandler(this.myMap_MouseClick);
            this.myMap.MouseEnter += new System.EventHandler(this.myMap_MouseEnter);
            this.myMap.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.myMap_PreviewKeyDown);
            // 
            // trackBar1
            // 
            this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar1.EditValue = 3;
            this.trackBar1.Location = new System.Drawing.Point(22, 33);
            this.trackBar1.MenuManager = this.barManager1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Properties.Maximum = 17;
            this.trackBar1.Properties.Minimum = 3;
            this.trackBar1.Properties.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar1.Size = new System.Drawing.Size(45, 248);
            this.trackBar1.TabIndex = 0;
            this.trackBar1.Value = 3;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // textEditStartLocation
            // 
            this.textEditStartLocation.Location = new System.Drawing.Point(43, 7);
            this.textEditStartLocation.MenuManager = this.barManager1;
            this.textEditStartLocation.Name = "textEditStartLocation";
            this.textEditStartLocation.Properties.NullValuePrompt = "Enter Start Location";
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditStartLocation, true);
            this.textEditStartLocation.Size = new System.Drawing.Size(142, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditStartLocation, optionsSpelling2);
            this.textEditStartLocation.TabIndex = 0;
            // 
            // btnGetRoute
            // 
            this.btnGetRoute.Location = new System.Drawing.Point(43, 58);
            this.btnGetRoute.Name = "btnGetRoute";
            this.btnGetRoute.Size = new System.Drawing.Size(64, 23);
            this.btnGetRoute.TabIndex = 0;
            this.btnGetRoute.Text = "Get Route";
            this.btnGetRoute.Click += new System.EventHandler(this.btnGetRoute_Click);
            // 
            // textEditEndLocation
            // 
            this.textEditEndLocation.Location = new System.Drawing.Point(43, 32);
            this.textEditEndLocation.MenuManager = this.barManager1;
            this.textEditEndLocation.Name = "textEditEndLocation";
            this.textEditEndLocation.Properties.NullValuePrompt = "Enter End Location";
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditEndLocation, true);
            this.textEditEndLocation.Size = new System.Drawing.Size(142, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditEndLocation, optionsSpelling1);
            this.textEditEndLocation.TabIndex = 0;
            // 
            // czuZoomUp
            // 
            this.czuZoomUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.czuZoomUp.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("czuZoomUp.ImageOptions.Image")));
            this.czuZoomUp.Location = new System.Drawing.Point(22, 3);
            this.czuZoomUp.Name = "czuZoomUp";
            this.czuZoomUp.Size = new System.Drawing.Size(24, 25);
            this.czuZoomUp.TabIndex = 0;
            this.czuZoomUp.Click += new System.EventHandler(this.czuZoomUp_Click);
            // 
            // czuZoomDown
            // 
            this.czuZoomDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.czuZoomDown.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("czuZoomDown.ImageOptions.Image")));
            this.czuZoomDown.Location = new System.Drawing.Point(22, 285);
            this.czuZoomDown.Name = "czuZoomDown";
            this.czuZoomDown.Size = new System.Drawing.Size(24, 25);
            this.czuZoomDown.TabIndex = 0;
            this.czuZoomDown.Click += new System.EventHandler(this.czuZoomDown_Click);
            // 
            // btnClearRoute
            // 
            this.btnClearRoute.Location = new System.Drawing.Point(121, 57);
            this.btnClearRoute.Name = "btnClearRoute";
            this.btnClearRoute.Size = new System.Drawing.Size(64, 23);
            this.btnClearRoute.TabIndex = 0;
            this.btnClearRoute.Text = "Clear Route";
            this.btnClearRoute.Click += new System.EventHandler(this.btnClearRoute_Click);
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiNone, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciAddMarker, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciAddPolygon),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciAddPolyline),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciEditPolygon, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciEditPolyline),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciMeasureArea, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciMeasureLength),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciScaleBar, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciZoom, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciRouting, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFindPlace, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Mapping Toolbar";
            // 
            // bbiNone
            // 
            this.bbiNone.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bbiNone.BindableChecked = true;
            this.bbiNone.Caption = "None";
            this.bbiNone.Checked = true;
            this.bbiNone.GroupIndex = 1;
            this.bbiNone.Id = 45;
            this.bbiNone.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Pointer;
            this.bbiNone.Name = "bbiNone";
            this.bbiNone.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiNone.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNone_CheckedChanged);
            // 
            // bciAddMarker
            // 
            this.bciAddMarker.Caption = "Marker";
            this.bciAddMarker.GroupIndex = 1;
            this.bciAddMarker.Id = 42;
            this.bciAddMarker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciAddMarker.ImageOptions.Image")));
            this.bciAddMarker.Name = "bciAddMarker";
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Add Marker - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to set the active map tool to Add Marker.\r\n\r\nOn clicking on the map, a n" +
    "ew marker will be added where the mouse is clicked.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bciAddMarker.SuperTip = superToolTip1;
            this.bciAddMarker.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciAddMarker_CheckedChanged);
            // 
            // bciAddPolygon
            // 
            this.bciAddPolygon.Caption = "Polygon";
            this.bciAddPolygon.GroupIndex = 1;
            this.bciAddPolygon.Id = 52;
            this.bciAddPolygon.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciAddPolygon.ImageOptions.Image")));
            this.bciAddPolygon.Name = "bciAddPolygon";
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Add Polygon - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = resources.GetString("toolTipItem2.Text");
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bciAddPolygon.SuperTip = superToolTip2;
            this.bciAddPolygon.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciAddPolygon_CheckedChanged);
            // 
            // bciAddPolyline
            // 
            this.bciAddPolyline.Caption = "Line";
            this.bciAddPolyline.GroupIndex = 1;
            this.bciAddPolyline.Id = 53;
            this.bciAddPolyline.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciAddPolyline.ImageOptions.Image")));
            this.bciAddPolyline.Name = "bciAddPolyline";
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Add Line - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = resources.GetString("toolTipItem3.Text");
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bciAddPolyline.SuperTip = superToolTip3;
            this.bciAddPolyline.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciAddPolyline_CheckedChanged);
            // 
            // bciEditPolygon
            // 
            this.bciEditPolygon.Caption = "Edit Polygon";
            this.bciEditPolygon.GroupIndex = 1;
            this.bciEditPolygon.Id = 56;
            this.bciEditPolygon.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciEditPolygon.ImageOptions.Image")));
            this.bciEditPolygon.Name = "bciEditPolygon";
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Edit Polygon - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bciEditPolygon.SuperTip = superToolTip4;
            this.bciEditPolygon.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciEditPolygon_CheckedChanged);
            // 
            // bciEditPolyline
            // 
            this.bciEditPolyline.Caption = "Edit Line";
            this.bciEditPolyline.GroupIndex = 1;
            this.bciEditPolyline.Id = 57;
            this.bciEditPolyline.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciEditPolyline.ImageOptions.Image")));
            this.bciEditPolyline.Name = "bciEditPolyline";
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Edit Line - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to set the active map tool to Edit Polygon.\r\n\r\nOn clicking on a line in " +
    "the map, the lines vertices are shown allowing them to be dragged to a new locat" +
    "ion to adjust the shape of the line.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bciEditPolyline.SuperTip = superToolTip5;
            this.bciEditPolyline.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciEditPolyline_CheckedChanged);
            // 
            // bciMeasureArea
            // 
            this.bciMeasureArea.Caption = "Area";
            this.bciMeasureArea.GroupIndex = 1;
            this.bciMeasureArea.Id = 54;
            this.bciMeasureArea.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciMeasureArea.ImageOptions.Image")));
            this.bciMeasureArea.Name = "bciMeasureArea";
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Measure Area - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = resources.GetString("toolTipItem6.Text");
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bciMeasureArea.SuperTip = superToolTip6;
            this.bciMeasureArea.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciMeasureArea_CheckedChanged);
            // 
            // bciMeasureLength
            // 
            this.bciMeasureLength.Caption = "Length";
            this.bciMeasureLength.GroupIndex = 1;
            this.bciMeasureLength.Id = 55;
            this.bciMeasureLength.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciMeasureLength.ImageOptions.Image")));
            this.bciMeasureLength.Name = "bciMeasureLength";
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Measure Length - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = resources.GetString("toolTipItem7.Text");
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bciMeasureLength.SuperTip = superToolTip7;
            this.bciMeasureLength.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciMeasureLength_CheckedChanged);
            // 
            // bciScaleBar
            // 
            this.bciScaleBar.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bciScaleBar.Caption = "Scale Bar";
            this.bciScaleBar.Id = 58;
            this.bciScaleBar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciScaleBar.ImageOptions.Image")));
            this.bciScaleBar.Name = "bciScaleBar";
            this.bciScaleBar.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciScaleBar_CheckedChanged);
            // 
            // bciZoom
            // 
            this.bciZoom.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bciZoom.Caption = "Zoom";
            this.bciZoom.Id = 41;
            this.bciZoom.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ZoomInLarge;
            this.bciZoom.Name = "bciZoom";
            this.bciZoom.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciZoom_CheckedChanged);
            // 
            // bciRouting
            // 
            this.bciRouting.Caption = "Routing";
            this.bciRouting.Id = 39;
            this.bciRouting.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciRouting.ImageOptions.Image")));
            this.bciRouting.Name = "bciRouting";
            this.bciRouting.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciRouting_CheckedChanged);
            // 
            // bciFindPlace
            // 
            this.bciFindPlace.Caption = "Find Place";
            this.bciFindPlace.Id = 43;
            this.bciFindPlace.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FindLarge;
            this.bciFindPlace.Name = "bciFindPlace";
            this.bciFindPlace.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFindPlace_CheckedChanged);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Test";
            this.barButtonItem1.Id = 51;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // beiMapType
            // 
            this.beiMapType.Caption = "Map Type:";
            this.beiMapType.Edit = this.repositoryItemComboBoxMapType;
            this.beiMapType.EditValue = "None";
            this.beiMapType.EditWidth = 117;
            this.beiMapType.Id = 30;
            this.beiMapType.Name = "beiMapType";
            this.beiMapType.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemComboBoxMapType
            // 
            this.repositoryItemComboBoxMapType.AutoHeight = false;
            this.repositoryItemComboBoxMapType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBoxMapType.Name = "repositoryItemComboBoxMapType";
            this.repositoryItemComboBoxMapType.SelectedValueChanged += new System.EventHandler(this.repositoryItemComboBoxMapType_SelectedValueChanged);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 4";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiMapType),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiZoom, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiPositionXY),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiMarkerCount),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiProgress),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiScaleToFit),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPanLeft),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUp),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDown),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRight)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 4";
            // 
            // bsiZoom
            // 
            this.bsiZoom.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiZoom.Caption = "Zoom:";
            this.bsiZoom.Id = 46;
            this.bsiZoom.Name = "bsiZoom";
            this.bsiZoom.Size = new System.Drawing.Size(60, 0);
            this.bsiZoom.Width = 60;
            // 
            // bsiPositionXY
            // 
            this.bsiPositionXY.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiPositionXY.Caption = "Position:";
            this.bsiPositionXY.Id = 27;
            this.bsiPositionXY.Name = "bsiPositionXY";
            this.bsiPositionXY.Size = new System.Drawing.Size(310, 0);
            this.bsiPositionXY.Width = 310;
            // 
            // bsiMarkerCount
            // 
            this.bsiMarkerCount.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.bsiMarkerCount.Caption = "Objects: 0";
            this.bsiMarkerCount.Id = 28;
            this.bsiMarkerCount.Name = "bsiMarkerCount";
            this.bsiMarkerCount.Size = new System.Drawing.Size(80, 0);
            this.bsiMarkerCount.Width = 80;
            // 
            // beiProgress
            // 
            this.beiProgress.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.beiProgress.Caption = "barEditItem1";
            this.beiProgress.Edit = this.repositoryItemMarqueeProgressBar1;
            this.beiProgress.EditWidth = 106;
            this.beiProgress.Id = 29;
            this.beiProgress.Name = "beiProgress";
            // 
            // repositoryItemMarqueeProgressBar1
            // 
            this.repositoryItemMarqueeProgressBar1.Name = "repositoryItemMarqueeProgressBar1";
            this.repositoryItemMarqueeProgressBar1.Stopped = true;
            // 
            // bbiScaleToFit
            // 
            this.bbiScaleToFit.Caption = "Scale and Centre Map to Fit all Objects";
            this.bbiScaleToFit.Id = 47;
            this.bbiScaleToFit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiScaleToFit.ImageOptions.Image")));
            this.bbiScaleToFit.Name = "bbiScaleToFit";
            toolTipTitleItem8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem8.Text = "Scale Map to Fit - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to Scale and centre the map to show all loaded map objects.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiScaleToFit.SuperTip = superToolTip8;
            this.bbiScaleToFit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiScaleToFit_ItemClick);
            // 
            // bbiPanLeft
            // 
            this.bbiPanLeft.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiPanLeft.Caption = "Pan Left";
            this.bbiPanLeft.Id = 34;
            this.bbiPanLeft.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPanLeft.ImageOptions.Image")));
            this.bbiPanLeft.Name = "bbiPanLeft";
            this.bbiPanLeft.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPanLeft_ItemClick);
            // 
            // bbiUp
            // 
            this.bbiUp.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiUp.Caption = "Pan Up";
            this.bbiUp.Id = 32;
            this.bbiUp.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUp.ImageOptions.Image")));
            this.bbiUp.Name = "bbiUp";
            this.bbiUp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUp_ItemClick);
            // 
            // bbiDown
            // 
            this.bbiDown.Caption = "Pan Down";
            this.bbiDown.Id = 33;
            this.bbiDown.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDown.ImageOptions.Image")));
            this.bbiDown.Name = "bbiDown";
            this.bbiDown.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDown_ItemClick);
            // 
            // bbiRight
            // 
            this.bbiRight.Caption = "Pan Right";
            this.bbiRight.Id = 35;
            this.bbiRight.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRight.ImageOptions.Image")));
            this.bbiRight.Name = "bbiRight";
            this.bbiRight.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRight_ItemClick);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 42);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.myMap);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1035, 467);
            this.splitContainerControl1.SplitterPosition = 256;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(256, 467);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage3,
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Controls.Add(this.btnLoadSites);
            this.xtraTabPage1.Controls.Add(this.btnClearSites);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(251, 441);
            this.xtraTabPage1.Text = "Sites";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(4, 4);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(244, 408);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            // 
            // btnLoadSites
            // 
            this.btnLoadSites.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadSites.Location = new System.Drawing.Point(3, 415);
            this.btnLoadSites.Name = "btnLoadSites";
            this.btnLoadSites.Size = new System.Drawing.Size(83, 22);
            this.btnLoadSites.TabIndex = 4;
            this.btnLoadSites.Text = "Load Sites";
            this.btnLoadSites.Click += new System.EventHandler(this.btnLoadSites_Click);
            // 
            // btnClearSites
            // 
            this.btnClearSites.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearSites.Location = new System.Drawing.Point(92, 415);
            this.btnClearSites.Name = "btnClearSites";
            this.btnClearSites.Size = new System.Drawing.Size(85, 22);
            this.btnClearSites.TabIndex = 5;
            this.btnClearSites.Text = "Clear Sites";
            this.btnClearSites.Click += new System.EventHandler(this.btnClearSites_Click);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl2);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(251, 441);
            this.xtraTabPage3.Text = "Layers";
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(251, 441);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(251, 441);
            this.xtraTabPage2.Text = "Teams";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            // 
            // dockManager1
            // 
            this.dockManager1.Controller = this.barAndDockingController1;
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelMapZoom,
            this.dockPanelFindPlace,
            this.dockPanelRouting});
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController1.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // dockPanelMapZoom
            // 
            this.dockPanelMapZoom.Controls.Add(this.dockPanel1_Container);
            this.dockPanelMapZoom.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanelMapZoom.FloatLocation = new System.Drawing.Point(718, 342);
            this.dockPanelMapZoom.FloatSize = new System.Drawing.Size(71, 343);
            this.dockPanelMapZoom.ID = new System.Guid("377cecca-9dab-4a68-813a-182078b6e1b5");
            this.dockPanelMapZoom.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanelMapZoom.Name = "dockPanelMapZoom";
            this.dockPanelMapZoom.OriginalSize = new System.Drawing.Size(76, 200);
            this.dockPanelMapZoom.SavedIndex = 0;
            this.dockPanelMapZoom.Size = new System.Drawing.Size(71, 343);
            this.dockPanelMapZoom.Text = "Zoom";
            this.dockPanelMapZoom.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelMapZoom.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelMapZoom_ClosingPanel);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.czuZoomUp);
            this.dockPanel1_Container.Controls.Add(this.czuZoomDown);
            this.dockPanel1_Container.Controls.Add(this.trackBar1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(2, 28);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(67, 313);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // dockPanelFindPlace
            // 
            this.dockPanelFindPlace.Controls.Add(this.controlContainer2);
            this.dockPanelFindPlace.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanelFindPlace.FloatLocation = new System.Drawing.Point(920, 220);
            this.dockPanelFindPlace.FloatSize = new System.Drawing.Size(225, 56);
            this.dockPanelFindPlace.ID = new System.Guid("aa3bef2b-6ed3-4d6e-a315-671f8feb2c69");
            this.dockPanelFindPlace.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanelFindPlace.Name = "dockPanelFindPlace";
            this.dockPanelFindPlace.OriginalSize = new System.Drawing.Size(227, 200);
            this.dockPanelFindPlace.SavedIndex = 1;
            this.dockPanelFindPlace.Size = new System.Drawing.Size(225, 56);
            this.dockPanelFindPlace.Text = "Find Place";
            this.dockPanelFindPlace.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelFindPlace.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelFindPlace_ClosingPanel);
            // 
            // controlContainer2
            // 
            this.controlContainer2.Controls.Add(this.buttonEditFindPlace);
            this.controlContainer2.Location = new System.Drawing.Point(2, 28);
            this.controlContainer2.Name = "controlContainer2";
            this.controlContainer2.Size = new System.Drawing.Size(221, 26);
            this.controlContainer2.TabIndex = 0;
            // 
            // buttonEditFindPlace
            // 
            this.buttonEditFindPlace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditFindPlace.Location = new System.Drawing.Point(2, 3);
            this.buttonEditFindPlace.MenuManager = this.barManager1;
            this.buttonEditFindPlace.Name = "buttonEditFindPlace";
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem9.Text = "Find Place - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = resources.GetString("toolTipItem9.Text");
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.buttonEditFindPlace.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Find", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "FindPlace", superToolTip9, DevExpress.Utils.ToolTipAnchor.Default)});
            this.buttonEditFindPlace.Properties.NullValuePrompt = "Place Name";
            this.buttonEditFindPlace.Properties.NullValuePromptShowForEmptyValue = true;
            this.buttonEditFindPlace.Size = new System.Drawing.Size(217, 20);
            this.buttonEditFindPlace.TabIndex = 2;
            this.buttonEditFindPlace.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditFindPlace_ButtonClick);
            // 
            // dockPanelRouting
            // 
            this.dockPanelRouting.Controls.Add(this.controlContainer1);
            this.dockPanelRouting.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanelRouting.FloatLocation = new System.Drawing.Point(718, 220);
            this.dockPanelRouting.FloatSize = new System.Drawing.Size(200, 120);
            this.dockPanelRouting.ID = new System.Guid("a2518ee0-063b-4bf5-a416-1d56dd94c906");
            this.dockPanelRouting.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanelRouting.Name = "dockPanelRouting";
            this.dockPanelRouting.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelRouting.SavedIndex = 0;
            this.dockPanelRouting.Size = new System.Drawing.Size(200, 120);
            this.dockPanelRouting.Text = "Routing";
            this.dockPanelRouting.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelRouting.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelRouting_ClosingPanel);
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.labelControl2);
            this.controlContainer1.Controls.Add(this.labelControl1);
            this.controlContainer1.Controls.Add(this.btnGetRoute);
            this.controlContainer1.Controls.Add(this.textEditEndLocation);
            this.controlContainer1.Controls.Add(this.btnClearRoute);
            this.controlContainer1.Controls.Add(this.textEditStartLocation);
            this.controlContainer1.Location = new System.Drawing.Point(2, 28);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(196, 90);
            this.controlContainer1.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(9, 36);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(22, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "End:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(9, 13);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Start:";
            // 
            // pmMapMarker
            // 
            this.pmMapMarker.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPolygon),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreatePolyline),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteMarker)});
            this.pmMapMarker.Manager = this.barManager1;
            this.pmMapMarker.MenuCaption = "Marker Menu";
            this.pmMapMarker.Name = "pmMapMarker";
            this.pmMapMarker.ShowCaption = true;
            // 
            // bbiAddPolygon
            // 
            this.bbiAddPolygon.Caption = "Create Polygon";
            this.bbiAddPolygon.Id = 59;
            this.bbiAddPolygon.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddPolygon.ImageOptions.Image")));
            this.bbiAddPolygon.Name = "bbiAddPolygon";
            this.bbiAddPolygon.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPolygon_ItemClick);
            // 
            // bbiCreatePolyline
            // 
            this.bbiCreatePolyline.Caption = "Create Polyline";
            this.bbiCreatePolyline.Id = 60;
            this.bbiCreatePolyline.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCreatePolyline.ImageOptions.Image")));
            this.bbiCreatePolyline.Name = "bbiCreatePolyline";
            this.bbiCreatePolyline.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreatePolyline_ItemClick);
            // 
            // bbiDeleteMarker
            // 
            this.bbiDeleteMarker.Caption = "Delete Marker";
            this.bbiDeleteMarker.Id = 44;
            this.bbiDeleteMarker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDeleteMarker.ImageOptions.Image")));
            this.bbiDeleteMarker.Name = "bbiDeleteMarker";
            this.bbiDeleteMarker.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeleteMarker_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 48;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barStaticItemPadding
            // 
            this.barStaticItemPadding.Id = 50;
            this.barStaticItemPadding.Name = "barStaticItemPadding";
            // 
            // frm_Core_Generic_Mapping
            // 
            this.ClientSize = new System.Drawing.Size(1035, 537);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Generic_Mapping";
            this.Text = "Mapping";
            this.Activated += new System.EventHandler(this.frm_Core_Generic_Mapping_Activated);
            this.Deactivate += new System.EventHandler(this.frm_Core_Generic_Mapping_Deactivate);
            this.Load += new System.EventHandler(this.frm_Core_Generic_Mapping_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditStartLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditEndLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBoxMapType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            this.dockPanelMapZoom.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.dockPanel1_Container.PerformLayout();
            this.dockPanelFindPlace.ResumeLayout(false);
            this.controlContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditFindPlace.Properties)).EndInit();
            this.dockPanelRouting.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            this.controlContainer1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmMapMarker)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GMap.NET.WindowsForms.GMapControl myMap;
        private DevExpress.XtraEditors.TrackBarControl trackBar1;
        private DevExpress.XtraEditors.TextEdit textEditStartLocation;
        private DevExpress.XtraEditors.SimpleButton btnGetRoute;
        private DevExpress.XtraEditors.TextEdit textEditEndLocation;
        private DevExpress.XtraEditors.SimpleButton czuZoomUp;
        private DevExpress.XtraEditors.SimpleButton czuZoomDown;
        private DevExpress.XtraEditors.SimpleButton btnClearRoute;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiPositionXY;
        private DevExpress.XtraBars.BarStaticItem bsiMarkerCount;
        private DevExpress.XtraBars.BarEditItem beiProgress;
        private DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar repositoryItemMarqueeProgressBar1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.BarEditItem beiMapType;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBoxMapType;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelMapZoom;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelRouting;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarButtonItem bbiPanLeft;
        private DevExpress.XtraBars.BarButtonItem bbiUp;
        private DevExpress.XtraBars.BarButtonItem bbiDown;
        private DevExpress.XtraBars.BarButtonItem bbiRight;
        private DevExpress.XtraBars.BarCheckItem bciRouting;
        private DevExpress.XtraBars.BarCheckItem bciZoom;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraBars.BarCheckItem bciAddMarker;
        private DevExpress.XtraBars.BarCheckItem bciFindPlace;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteMarker;
        private DevExpress.XtraBars.PopupMenu pmMapMarker;
        private DevExpress.XtraBars.BarCheckItem bbiNone;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnLoadSites;
        private DevExpress.XtraEditors.SimpleButton btnClearSites;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFindPlace;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer2;
        private DevExpress.XtraEditors.ButtonEdit buttonEditFindPlace;
        private DevExpress.XtraBars.BarStaticItem bsiZoom;
        private DevExpress.XtraBars.BarButtonItem bbiScaleToFit;
        private DevExpress.XtraBars.BarStaticItem barStaticItemPadding;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarCheckItem bciAddPolygon;
        private DevExpress.XtraBars.BarCheckItem bciAddPolyline;
        private DevExpress.XtraBars.BarCheckItem bciMeasureArea;
        private DevExpress.XtraBars.BarCheckItem bciMeasureLength;
        private DevExpress.XtraBars.BarCheckItem bciEditPolygon;
        private DevExpress.XtraBars.BarCheckItem bciEditPolyline;
        private DevExpress.XtraBars.BarCheckItem bciScaleBar;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraBars.BarButtonItem bbiAddPolygon;
        private DevExpress.XtraBars.BarButtonItem bbiCreatePolyline;
    }
}

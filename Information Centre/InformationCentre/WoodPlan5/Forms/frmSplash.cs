using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Xml;
using System.Diagnostics;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public struct stcLinkItemIDs
    {
        public int intModuleID;
        public int intFormID;
        public int intType;
    }

    public partial class frmSplash : frmBase
    {
        private string strProgressText;
        public string strPassword;
        private Int32 intProgressCount;
        private Settings set = Settings.Default;
        private ArrayList alNames = new ArrayList();
        private Int32 intStaffID;
        private Int32 intSystemID;
        public frmMain2 frmParent;
        public string strActualID;
        public string strPassedDatabase = "";
        public Thread aThread;
        public String strUserType = "";
        public string strConnectionString = "";
        ArrayList alDBDetails = new ArrayList();
        private int intSeconds = 5;
        public string strServerName = "";
        public string strDatabaseName = "";
        public string strFriendlyName = "";
        public string strUsername = "";
        public int intUserID = 0;

        public frmSplash(int intMode)
        {
            this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);

            // Required for Windows Form Designer support
            InitializeComponent();
            InitializeForm();
         }
        private Boolean boolShowPassword = false;

        private void InitializeForm()
        {
            XmlTextReader xtrReader = new XmlTextReader("InformationCentre_Config.xml");
            stcDatabase sdDetails = new stcDatabase();
            alDBDetails = new ArrayList();

            progressBarControl1.Visible = false;

            //timerSplash.Start();
            //timerSplash.Enabled = true;

            intSeconds = 5;
            lblSeconds.Text = intSeconds.ToString();

            DevExpress.XtraEditors.Controls.ComboBoxItemCollection coll = this.cboDatabaseDetails.Properties.Items;
            int i = 1;
            int intIndex = 0;
            string strLoginMode = "";
            try
            {
                while (xtrReader.Read())
                {
                    switch (xtrReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (xtrReader.Name == "LoginMode")
                            {
                                while (xtrReader.MoveToNextAttribute())
                                {
                                    strLoginMode = xtrReader.Value;
                                }
                                if (strLoginMode.ToUpper() == "PASSWORD") boolShowPassword = true;
                            }

                            if (xtrReader.Name == "database" + i.ToString())
                            {
                                sdDetails = new stcDatabase();
                            }
                            if (xtrReader.Name == "databasename")
                            {
                                while (xtrReader.MoveToNextAttribute())
                                {
                                    sdDetails.strDatabaseName = xtrReader.Value;
                                }
                            }
                            if (xtrReader.Name == "servername")
                            {
                                while (xtrReader.MoveToNextAttribute())
                                {
                                    sdDetails.strServerName = xtrReader.Value;
                                }
                            }
                            if (xtrReader.Name == "friendlyname")
                            {
                                while (xtrReader.MoveToNextAttribute())
                                {
                                    sdDetails.strFriendlyName = xtrReader.Value;
                                }
                            }
                            if (xtrReader.Name == "default")
                            {
                                while (xtrReader.MoveToNextAttribute())
                                {
                                    sdDetails.intDefault = Convert.ToInt32(xtrReader.Value);
                                }
                            }
                            break;
                        case XmlNodeType.EndElement:
                            if (xtrReader.Name == "database" + i.ToString())
                            {
                                alDBDetails.Add(sdDetails);
                                intIndex = coll.Add(sdDetails.strFriendlyName);
                                if (sdDetails.intDefault == 1)
                                {
                                    cboDatabaseDetails.SelectedIndex = intIndex;
                                }
                                i++;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to read the application config file!\n\nError: " + ex.Message, "Read Application #config File - Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (boolShowPassword)
            {
                txtUsername.Visible = true;
                txtUsername.Enabled = true;
                txtUsername.Text = Environment.UserName;
                label4.Visible = true;
                txtPassword.Visible = true;
                txtPassword.Enabled = true;
                label5.Visible = true;
            }
            else
            {
                cboDatabaseDetails.Location = new Point(77, 33);  // Centre database box and label in panel //
                label3.Location = new Point(17, 36);
            }
        }
 
        public void BeginStartupProcess(ArrayList alPassedNames, Int32 intStaff, Int32 intSystem)
        {
            alNames = alPassedNames;
            intStaffID = intStaff;
            intSystemID = intSystem;
        }

        public void ProgressStep(string strItemName,Int32 intAddText)
        {
            if (intAddText == 1)
            {
                strProgressText = strItemName + " Initialising...";
            }
            else
            {
                strProgressText = strItemName;
            }

            intProgressCount += 1;

            this.lblProgress.Text = strProgressText;
            this.progressBarControl1.Increment(intProgressCount);
        }

        public void SetUpProgressBar(Int32 intCount)
        {
            this.progressBarControl1.Properties.Minimum = 0;
            this.progressBarControl1.Properties.Maximum = intCount;
            this.progressBarControl1.Properties.Step = 1;
        }

        private void timerSplash_Tick(object sender, EventArgs e)
        {
            string strTempUserName = "";
            string strTempPassword = "";
            if (boolShowPassword)
            {
                strTempUserName = txtUsername.Text.ToString().Trim();
                strTempPassword = txtPassword.Text.ToString().Trim();
                if (strTempUserName == "" || strTempUserName == null || strTempPassword == "" || strTempPassword == null)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Enter a Username and Password before proceeding.", "Information Centre Login", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            
            int intAlternativeID = 0;
            string strAlternativeID = "";
            string strDefaultFont = "Tahoma";
            int intDefaultFontSize = 8;
            stcDatabase sdDetails;
            int i = 0;

            intSeconds = intSeconds - 1;
            lblSeconds.Text = intSeconds.ToString();
            
            if (intSeconds == 0)
            {
                for (i = 0; i < alDBDetails.Count; i++)
                {
                    sdDetails = (stcDatabase)alDBDetails[i];

                    if (sdDetails.strFriendlyName == cboDatabaseDetails.GetRealEditValue())
                    {
                        strServerName = sdDetails.strServerName;
                        strDatabaseName = sdDetails.strDatabaseName;
                        strFriendlyName = sdDetails.strFriendlyName;
                        set.ServerName = strServerName;
                        set.DatabaseName = strDatabaseName;

                        if (boolShowPassword)
                        {
                            strConnectionString = "Data Source=" + strServerName + ";" +
                                                  "Initial Catalog=" + strDatabaseName + ";" +
                                                  "User ID=" + strTempUserName + ";" +
                                                  "Password=" + strTempPassword + ";";
                        }
                        else
                        {
                            strTempUserName = Environment.UserName;
                            strConnectionString = "data source=" + strServerName + ";" +
                                                  "initial catalog=" + strDatabaseName + ";" +
                                                  "integrated security=SSPI;" +
                                                  "persist security info=False;" +
                                                  "Trusted_Connection=Yes;";
                        }
                        set.ServerName = strServerName;
                        set.DatabaseName = strDatabaseName;
                        set.WoodPlanConnectionString = strConnectionString;
                        set.Save();

                        try
                        {
                            sp00019GetUserDetailsForStartUpTableAdapter.Connection.ConnectionString = strConnectionString;
                            sp00019GetUserDetailsForStartUpTableAdapter.Fill(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp, strTempUserName);
                        }
                        catch (Exception)
                        {
                            if (boolShowPassword)
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Invalid username and / or password entered. Please try again before proceeding.", "Information Centre Login - Invalid Login Details", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return;
                            }
                            else
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Account unavailable - your username does not have a database login associated with it. Please contact your System Administrator.", "Information Centre Login - User Account Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                this.Close();
                                this.DialogResult = DialogResult.Abort;
                                return;
                            }
                        }
                        label3.Visible = false;
                        cboDatabaseDetails.Visible = false;
                        cboDatabaseDetails.Enabled = false;
                        txtUsername.Visible = false;
                        txtUsername.Enabled = false;
                        label4.Visible = false;
                        txtPassword.Visible = false;
                        txtPassword.Enabled = false;
                        label5.Visible = false;
                        btnOK.Visible = false;
                        btnOK.Enabled = false;
                        btnCancel.Visible = false;
                        btnCancel.Enabled = false;

                        progressBarControl1.Visible = true;
                        lblProgress.Visible = true;

                        if (woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows.Count == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Account unavailable - the current user does not exist in the Information Centre Staff table. Please contact your System Administrator.", "Information Centre Login - User Account Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            this.Close();
                            this.DialogResult = DialogResult.Abort;
                            return;
                        }

                        sp00071_get_available_loginsTableAdapter.Connection.ConnectionString = strConnectionString;
                        sp00071_get_available_loginsTableAdapter.Fill(this.woodPlanDataSet.sp00071_get_available_logins, Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]));

                        if (this.woodPlanDataSet.sp00071_get_available_logins.Rows.Count > 1)
                        {
                            frmAlternativeLogin falLogins = new frmAlternativeLogin(Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]));
                            falLogins.TopMost = true;
                            falLogins.ShowDialog();
                            intAlternativeID = falLogins.UserID;
                            strAlternativeID = falLogins.NetworkID;
                        }
                        try
                        {
                            if (strAlternativeID != "")
                            {
                                strUsername = strAlternativeID;
                                //strActualID = Environment.UserName;
                                strActualID = strTempUserName;
                            }
                            else
                            {
                                //strUsername = Environment.UserName;
                                strUsername = strTempUserName;
                                strActualID = strUsername;
                            }

                            sp00019GetUserDetailsForStartUpTableAdapter.Fill(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp, strUsername);

                            intUserID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]);
                            intStaffID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]);
                            strUserType = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType1"].ToString();

                            // For Font Sizing...
                            strDefaultFont = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["DefaultFontName"].ToString();
                            intDefaultFontSize = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["DefaultFontSize"]);

                            DevExpress.Utils.AppearanceObject.DefaultFont = new Font(strDefaultFont, intDefaultFontSize);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            intStaffID = 0;
                            strUserType = "NONE";
                            DevExpress.Utils.AppearanceObject.DefaultFont = new Font("Tahoma", 8);
                        }
                        break;
                    }
                }
                timerSplash.Stop();

                if (intStaffID != 0)
                {
                    StartInitialisation();
                    //Thread t = new Thread(StartInitialisation);
                    //t.Start();
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Account Unavailable. Please contact your System Administrator.", "Information Centre Login - User Account Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                    this.DialogResult = DialogResult.Abort;
                }
            }
        }

        private void StartInitialisation()
        {
            stcItems sItems;
            stcLinkItemIDs sliIDs;
            WoodPlan5.WoodPlanDataSet.sp00001PopulateSwitchboardDataTable dtSwitchBoardTable = new WoodPlan5.WoodPlanDataSet.sp00001PopulateSwitchboardDataTable();
            string strBitmap;
            int i = 0;
            int j = 0;
            DevExpress.XtraNavBar.NavBarGroup nbgSwitchboardGroup;
            DevExpress.XtraNavBar.NavBarGroup nbgReportGroup;
            DevExpress.XtraNavBar.NavBarItem nbiItem;
            ArrayList alGroupIDs = new ArrayList();
            DataRow row;
            int intIndex;
            string strProgress = "";

            for (i = 0; i < alNames.Count; i++)
            {
                sItems = (stcItems)alNames[i];

                nbgSwitchboardGroup = new DevExpress.XtraNavBar.NavBarGroup(sItems.strItemName);
                nbgSwitchboardGroup.Tag = sItems.iItemID;
                switch (sItems.strItemName)
                {
                    case "Amenity Trees":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[0];
                        break;
                    case "Asset Management":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[1];
                        break;
                    case "Winter Maintenance":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[2];
                        break;
                    case "Tender Register":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[6];
                        break;
                    case "CRM":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[7];
                        break;
                    case "System Administration":
                        nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[3];
                       break;
                    case "Summer Maintenance":
                       nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[8];
                       break;
                    case "Utility Arb":
                       nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[9];
                       break;
                    case "HR":
                       nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[10];
                       break;
                    case "Training":
                       nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[13];
                       break;
                    case "Equipment":
                       nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[11];
                       break;
                    case "Core":
                       nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[12];
                       break;
                    default:
                        break;
                }
                nbgReportGroup = new DevExpress.XtraNavBar.NavBarGroup(sItems.strItemName);
                nbgReportGroup.Tag = sItems.iItemID;
                switch (sItems.strItemName)
                {
                    case "Amenity Trees":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[0];
                        break;
                    case "Asset Management":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[1];
                        break;
                    case "Winter Maintenance":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[2];
                        break;
                    case "Tender Register":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[6];
                        break;
                    case "CRM":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[7];
                        break;
                    case "Summer Maintenance":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[8];
                        break;
                    case "Utility Arb":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[9];
                        break;
                    case "HR":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[10];
                        break;
                    case "Training":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[13];
                        break;
                    case "Equipment":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[11];
                        break;
                    case "Core":
                        nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[12];
                        break;
                    default:
                        break;
                }

                frmParent.nbcSwitchboard.Groups.Add(nbgSwitchboardGroup);
                frmParent.nbcReports.Groups.Add(nbgReportGroup);
                strProgress = sItems.strItemName + " Initialising...";
                
                this.lblProgress.Text = strProgress;
                this.progressBarControl1.Increment(1);
                this.Refresh();
            }

            nbgSwitchboardGroup = new DevExpress.XtraNavBar.NavBarGroup("My Options");
            nbgSwitchboardGroup.Name = "My Options";
            nbgSwitchboardGroup.Tag = 9998;
            nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[4];

            frmParent.nbcSwitchboard.Groups.Add(nbgSwitchboardGroup);

            nbgSwitchboardGroup = new DevExpress.XtraNavBar.NavBarGroup("Favourites");
            nbgSwitchboardGroup.Tag = -1;
            nbgSwitchboardGroup.LargeImage = frmParent.imageCollection3.Images[5];
            
            nbgReportGroup = new DevExpress.XtraNavBar.NavBarGroup("Favourites");
            nbgReportGroup.Tag = -1;
            nbgReportGroup.LargeImage = frmParent.imageCollection3.Images[5];

            frmParent.nbcSwitchboard.Groups.Add(nbgSwitchboardGroup);
            frmParent.nbcReports.Groups.Add(nbgReportGroup);

            sp00001PopulateSwitchboardTableAdapter1.Connection.ConnectionString = strConnectionString;
            sp00001PopulateSwitchboardTableAdapter1.Fill(dtSwitchBoardTable, intStaffID);

            for(i = 0; i < dtSwitchBoardTable.Rows.Count; i++)
            {
                row = dtSwitchBoardTable.Rows[i];

                if (j == frmParent.nbcSwitchboard.Groups.Count)
                {
                    j = 0;
                }

                if (Convert.ToInt32(row["ReadAccess"]) > 0)
                {
                    if (Convert.ToInt32(row["Type"]) == 2)  // Screen //
                    {
                        if ((int)frmParent.nbcSwitchboard.Groups[j].Tag != (int)row["GroupID"])
                        {
                            for (j = 0; j < frmParent.nbcSwitchboard.Groups.Count; j++)
                            {
                                // Compare our Group ID from the DB with the one pulled in via the interfaces...
                                if ((int)frmParent.nbcSwitchboard.Groups[j].Tag == (int)row["GroupID"])
                                {
                                    nbiItem = new DevExpress.XtraNavBar.NavBarItem(row["PartDescription"].ToString());

                                    sliIDs = new stcLinkItemIDs();
                                    sliIDs.intModuleID = Convert.ToInt32(row["ModuleID"]);
                                    sliIDs.intFormID = Convert.ToInt32(row["PartID"]);
                                    sliIDs.intType = Convert.ToInt32(row["Type"]);
                                    nbiItem.Tag = sliIDs;
                                    if ((int)frmParent.nbcSwitchboard.Groups[j].Tag == (int)-1)
                                    {
                                        strBitmap = "13";  // Hardcoded icon //
                                    }
                                    else
                                    {
                                        strBitmap = row["ImageName"].ToString();
                                    }
                                    if (!string.IsNullOrEmpty(strBitmap)) nbiItem.SmallImage = frmParent.imageCollection1.Images[Convert.ToInt32(strBitmap)];
                                    frmParent.nbcSwitchboard.Groups[j].ItemLinks.Add(nbiItem);
                                    break;
                                }
                            }
                            strProgress = "Initialising Switchboard...";
                            this.lblProgress.Text = strProgress;
                            this.progressBarControl1.Increment(1);
                            this.Refresh();
                        }
                        else
                        {
                            nbiItem = new DevExpress.XtraNavBar.NavBarItem(row["PartDescription"].ToString());
                            sliIDs = new stcLinkItemIDs();
                            sliIDs.intModuleID = Convert.ToInt32(row["ModuleID"]);
                            sliIDs.intFormID = Convert.ToInt32(row["PartID"]);
                            sliIDs.intType = Convert.ToInt32(row["Type"]);
                            nbiItem.Tag = sliIDs;
                            if ((int)frmParent.nbcSwitchboard.Groups[j].Tag == (int)-1)
                            {
                                strBitmap = "13";  // Hardcoded icon //
                            }
                            else
                            {
                                strBitmap = row["ImageName"].ToString();
                            }
                            if (!string.IsNullOrEmpty(strBitmap)) nbiItem.SmallImage = frmParent.imageCollection1.Images[Convert.ToInt32(strBitmap)];
                            frmParent.nbcSwitchboard.Groups[j].ItemLinks.Add(nbiItem);
                        }
                    }
                }
            }
            //******************

            for (i = 0; i < dtSwitchBoardTable.Rows.Count; i++)
            {
                row = dtSwitchBoardTable.Rows[i];

                if (j == frmParent.nbcReports.Groups.Count)
                {
                    j = 0;
                }

                if (Convert.ToInt32(row["ReadAccess"]) > 0)
                {
                    if (Convert.ToInt32(row["Type"]) == 3)  // Report //
                    {
                        if ((int)frmParent.nbcReports.Groups[j].Tag != (int)row["GroupID"])
                        {
                            for (j = 0; j < frmParent.nbcReports.Groups.Count; j++)
                            {
                                // Compare our Group ID from the DB with the one pulled in via the interfaces...
                                if ((int)frmParent.nbcReports.Groups[j].Tag == (int)row["GroupID"])
                                {
                                    nbiItem = new DevExpress.XtraNavBar.NavBarItem(row["PartDescription"].ToString());

                                    sliIDs = new stcLinkItemIDs();
                                    sliIDs.intModuleID = Convert.ToInt32(row["ModuleID"]);
                                    sliIDs.intFormID = Convert.ToInt32(row["PartID"]);
                                    sliIDs.intType = Convert.ToInt32(row["Type"]);
                                    nbiItem.Tag = sliIDs;
                                    if ((int)frmParent.nbcReports.Groups[j].Tag == (int)-1)
                                    {
                                        strBitmap = "13";  // Hardcoded icon //
                                    }
                                    else
                                    {
                                        strBitmap = row["ImageName"].ToString();
                                    }
                                    if (!string.IsNullOrEmpty(strBitmap)) nbiItem.SmallImage = frmParent.imageCollection1.Images[Convert.ToInt32(strBitmap)];
                                    frmParent.nbcReports.Groups[j].ItemLinks.Add(nbiItem);
                                    break;
                                }
                            }
                            strProgress = "Initialising Reports...";
                            this.lblProgress.Text = strProgress;
                            this.progressBarControl1.Increment(1);
                            this.Refresh();
                        }
                        else
                        {
                            nbiItem = new DevExpress.XtraNavBar.NavBarItem(row["PartDescription"].ToString());
                            sliIDs = new stcLinkItemIDs();
                            sliIDs.intModuleID = Convert.ToInt32(row["ModuleID"]);
                            sliIDs.intFormID = Convert.ToInt32(row["PartID"]);
                            sliIDs.intType = Convert.ToInt32(row["Type"]);
                            nbiItem.Tag = sliIDs;
                            if ((int)frmParent.nbcReports.Groups[j].Tag == (int)-1)
                            {
                                strBitmap = "13";  // Hardcoded icon //
                            }
                            else
                            {
                                strBitmap = row["ImageName"].ToString();
                            }
                            if (!string.IsNullOrEmpty(strBitmap)) nbiItem.SmallImage = frmParent.imageCollection1.Images[Convert.ToInt32(strBitmap)];
                            frmParent.nbcReports.Groups[j].ItemLinks.Add(nbiItem);
                        }
                    }
                }
            }
            for (i = 0; i < frmParent.nbcSwitchboard.Groups.Count; i++)
            {
                if (frmParent.nbcSwitchboard.Groups[i].ItemLinks.Count == 0)
                {
                    if (frmParent.nbcSwitchboard.Groups[i].Caption != "Favourites")
                    {
                        alGroupIDs.Add(i);
                    }
                }
            }

            alGroupIDs.Reverse();

            for (i = 0; i < alGroupIDs.Count; i++)
            {
                intIndex = (int)alGroupIDs[i];
                frmParent.nbcSwitchboard.Groups.RemoveAt(intIndex);
            }

            alGroupIDs.Clear();

            for (i = 0; i < frmParent.nbcReports.Groups.Count; i++)
            {
                if (frmParent.nbcReports.Groups[i].ItemLinks.Count == 0)
                {
                    if (frmParent.nbcReports.Groups[i].Caption != "Favourites")
                    {
                        alGroupIDs.Add(i);
                    }
                }
            }

            alGroupIDs.Reverse();

            for (i = 0; i < alGroupIDs.Count; i++)
            {
                intIndex = (int)alGroupIDs[i];
                frmParent.nbcReports.Groups.RemoveAt(intIndex);
            }

            this.DialogResult = DialogResult.Yes;
        }

        delegate void InitialiseMainFormCallBack(string strProgress, int intProgressStep);

        void InitialiseMainForm(string strProgress, int intProgressStep)
        {
            this.lblProgress.Text = strProgress;
            this.progressBarControl1.Increment(intProgressStep);
            this.Refresh();
        }

        private void frmSplash_Load(object sender, EventArgs e)
        {
            System.Reflection.Assembly oAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo oFileVersionInfo = FileVersionInfo.GetVersionInfo(oAssembly.Location);
            Version v = oAssembly.GetName().Version;
            lblVersion.Text = "Version: " + v.ToString().Substring(0, v.ToString().LastIndexOf('.'));  // Remove the last ".0" from end //
            lblCopyright.Text = oFileVersionInfo.LegalCopyright;

            if (strPassedDatabase != "")
            {
                foreach (stcDatabase sdDetails in alDBDetails)
                {
                    if (sdDetails.strDatabaseName.ToUpper() == strPassedDatabase)
                    {
                        strFriendlyName = sdDetails.strFriendlyName;
                        break;
                    }
                }

                for (int i = 0; i < cboDatabaseDetails.Properties.Items.Count; i++)
                {
                    if (cboDatabaseDetails.Properties.Items[i].ToString() == strFriendlyName)
                    {
                        cboDatabaseDetails.SelectedIndex = i;
                        intSeconds = 1;
                        timerSplash_Tick(this, e);
                        break;
                    }
                }
            }

            try
            {
                if (aThread != null)
                {
                    aThread.Abort();
                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            // Size set on frmMain2 form //
            //this.Height = 285;
            //this.Width = 368;

        }

        private void cboDatabaseDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            timerSplash.Stop();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            intSeconds = 1;
            timerSplash_Tick(this, e);
        }

        private void cboDatabaseDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                intSeconds = 1;
                timerSplash_Tick(this, e);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.Cancel;
        }

        private void cboDatabaseDetails_Click(object sender, EventArgs e)
        {
            timerSplash.Stop();
        }

        private void btnOK_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                intSeconds = 1;
                timerSplash_Tick(this, e);
            }
        }  
    }
}

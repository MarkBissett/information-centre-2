namespace WoodPlan5
{
    partial class frm_Core_Site_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Site_Manager));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp03004EPSiteManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP = new WoodPlan5.DataSet_EP();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteFax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEditRemarks = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedAssetCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colContactPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactPersonPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMappingWorkspaceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkspaceName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHubID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientsSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientsSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsSnowClearanceSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCOAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGeoFenceDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMetres = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSiteKML = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditViewSiteKML = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colSiteImageFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditAppSiteImage = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.popupContainerControlCompanies = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnCompanyFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.sp04237GCCompanyFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlSites = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnClientFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp03005EPClientListAllBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDailyGrittingReport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlShowTabPages = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp04022CoreDummyTabPageListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTabPageName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnOK2 = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlLinkedGrittingCalloutsFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04001GCJobCallOutStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnGritCalloutFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageLinkedDocuments = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp04332GCServiceAreasLinkedToSiteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteServiceAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescriptionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppliesToGritting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colAreaOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSiteName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAreaDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppliesToSnowClearance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp03010EPSiteManagerLinkedInspectionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.colInspectionEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTimeEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.colTotalTimeDeductions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTimeDeductionsRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colTotalTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp04003GCDrawingsLinkedToSiteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDrawingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDrawingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDrawingFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDrawingDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFileTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer5 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp04004GCSiteHolidaysLinkedToSiteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteHolidayID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSkipGritting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colSiteCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colHolidayType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSkipSnowClearance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerEditCompanies = new DevExpress.XtraEditors.PopupContainerEdit();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.btnLoadSites = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.comboBoxEditLinkedRecordType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.popupContainerEdit1 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerEdit3 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp03004_EP_Site_ManagerTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp03004_EP_Site_ManagerTableAdapter();
            this.sp03005_EP_Client_List_AllTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp03005_EP_Client_List_AllTableAdapter();
            this.sp03010_EP_Site_Manager_Linked_InspectionsTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp03010_EP_Site_Manager_Linked_InspectionsTableAdapter();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter();
            this.sp04003_GC_Drawings_Linked_To_SiteTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04003_GC_Drawings_Linked_To_SiteTableAdapter();
            this.sp04004_GC_Site_Holidays_Linked_To_SiteTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04004_GC_Site_Holidays_Linked_To_SiteTableAdapter();
            this.sp04022_Core_Dummy_TabPageListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04022_Core_Dummy_TabPageListTableAdapter();
            this.sp04237_GC_Company_Filter_ListTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter();
            this.sp04332_GC_Service_Areas_Linked_To_SiteTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04332_GC_Service_Areas_Linked_To_SiteTableAdapter();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.hideContainerLeft = new DevExpress.XtraBars.Docking.AutoHideContainer();
            this.dockPanelFilters = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefreshData = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterData = new DevExpress.XtraBars.BarCheckItem();
            this.bsiSelectedCount = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03004EPSiteManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMetres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditViewSiteKML)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditAppSiteImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).BeginInit();
            this.popupContainerControlCompanies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSites)).BeginInit();
            this.popupContainerControlSites.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03005EPClientListAllBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlShowTabPages)).BeginInit();
            this.popupContainerControlShowTabPages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04022CoreDummyTabPageListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).BeginInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageLinkedDocuments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04332GCServiceAreasLinkedToSiteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03010EPSiteManagerLinkedInspectionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04003GCDrawingsLinkedToSiteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).BeginInit();
            this.gridSplitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04004GCSiteHolidaysLinkedToSiteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCompanies.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditLinkedRecordType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.hideContainerLeft.SuspendLayout();
            this.dockPanelFilters.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1051, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 618);
            this.barDockControlBottom.Size = new System.Drawing.Size(1051, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 618);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1051, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 618);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefreshData,
            this.bciFilterData,
            this.bsiSelectedCount});
            this.barManager1.MaxItemId = 32;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(23, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl2.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl2.Panel1.Text = "Sites";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl2.Panel2.Text = "Linked Inspections";
            this.splitContainerControl2.Size = new System.Drawing.Size(1028, 618);
            this.splitContainerControl2.SplitterPosition = 207;
            this.splitContainerControl2.TabIndex = 2;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 42);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlCompanies);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlSites);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlShowTabPages);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlLinkedGrittingCalloutsFilter);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1023, 363);
            this.gridSplitContainer1.TabIndex = 6;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp03004EPSiteManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Move Item Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Move Item Down", "down"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Preserve Sort Order", "set_order"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Show on Map", "show_on_map"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Set Map Location from Postcode", "set_position")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEditRemarks,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEditMetres,
            this.repositoryItemHyperLinkEditViewSiteKML,
            this.repositoryItemHyperLinkEditAppSiteImage});
            this.gridControl1.Size = new System.Drawing.Size(1023, 363);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp03004EPSiteManagerBindingSource
            // 
            this.sp03004EPSiteManagerBindingSource.DataMember = "sp03004_EP_Site_Manager";
            this.sp03004EPSiteManagerBindingSource.DataSource = this.dataSet_EP;
            // 
            // dataSet_EP
            // 
            this.dataSet_EP.DataSetName = "DataSet_EP";
            this.dataSet_EP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "set_map_position_16.png");
            this.imageCollection1.Images.SetKeyName(7, "show_map_16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 8);
            this.imageCollection1.Images.SetKeyName(8, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 9);
            this.imageCollection1.Images.SetKeyName(9, "refresh_16x16");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colSiteID,
            this.colClientName,
            this.colClientCode,
            this.colSiteCode,
            this.colSiteName,
            this.colSiteTypeDescription,
            this.colSiteTypeID,
            this.colSiteAddressLine1,
            this.colSiteAddressLine2,
            this.colSiteAddressLine3,
            this.colSiteAddressLine4,
            this.colSiteAddressLine5,
            this.colSitePostcode,
            this.colSiteTelephone,
            this.colSiteMobile,
            this.colSiteFax,
            this.colSiteEmail,
            this.colXCoordinate,
            this.colYCoordinate,
            this.colClientRemarks,
            this.colSiteRemarks,
            this.colLinkedAssetCount,
            this.colContactPerson,
            this.colContactPersonPosition,
            this.colMappingWorkspaceID,
            this.colWorkspaceName,
            this.colCompanyCode,
            this.colCompanyName,
            this.colSiteOrder,
            this.colCompanyID,
            this.colHubID,
            this.colGrittingSite,
            this.colClientsSiteCode,
            this.colClientsSiteID,
            this.colIsSnowClearanceSite,
            this.colLocationX,
            this.colLocationY,
            this.colCOAddress,
            this.colSelected,
            this.colGeoFenceDistance,
            this.colSiteKML,
            this.colSiteImageFile});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.Width = 64;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Width = 56;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 227;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 1;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 243;
            // 
            // colSiteTypeDescription
            // 
            this.colSiteTypeDescription.Caption = "Site Type";
            this.colSiteTypeDescription.FieldName = "SiteTypeDescription";
            this.colSiteTypeDescription.Name = "colSiteTypeDescription";
            this.colSiteTypeDescription.OptionsColumn.AllowEdit = false;
            this.colSiteTypeDescription.OptionsColumn.AllowFocus = false;
            this.colSiteTypeDescription.OptionsColumn.ReadOnly = true;
            this.colSiteTypeDescription.Visible = true;
            this.colSiteTypeDescription.VisibleIndex = 5;
            this.colSiteTypeDescription.Width = 101;
            // 
            // colSiteTypeID
            // 
            this.colSiteTypeID.Caption = "Site Type ID";
            this.colSiteTypeID.FieldName = "SiteTypeID";
            this.colSiteTypeID.Name = "colSiteTypeID";
            this.colSiteTypeID.OptionsColumn.AllowEdit = false;
            this.colSiteTypeID.OptionsColumn.AllowFocus = false;
            this.colSiteTypeID.OptionsColumn.ReadOnly = true;
            this.colSiteTypeID.Width = 80;
            // 
            // colSiteAddressLine1
            // 
            this.colSiteAddressLine1.Caption = "Address Line 1";
            this.colSiteAddressLine1.FieldName = "SiteAddressLine1";
            this.colSiteAddressLine1.Name = "colSiteAddressLine1";
            this.colSiteAddressLine1.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine1.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine1.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine1.Visible = true;
            this.colSiteAddressLine1.VisibleIndex = 13;
            this.colSiteAddressLine1.Width = 111;
            // 
            // colSiteAddressLine2
            // 
            this.colSiteAddressLine2.Caption = "Address Line 2";
            this.colSiteAddressLine2.FieldName = "SiteAddressLine2";
            this.colSiteAddressLine2.Name = "colSiteAddressLine2";
            this.colSiteAddressLine2.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine2.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine2.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine2.Visible = true;
            this.colSiteAddressLine2.VisibleIndex = 14;
            this.colSiteAddressLine2.Width = 107;
            // 
            // colSiteAddressLine3
            // 
            this.colSiteAddressLine3.Caption = "Address Line 3";
            this.colSiteAddressLine3.FieldName = "SiteAddressLine3";
            this.colSiteAddressLine3.Name = "colSiteAddressLine3";
            this.colSiteAddressLine3.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine3.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine3.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine3.Visible = true;
            this.colSiteAddressLine3.VisibleIndex = 15;
            this.colSiteAddressLine3.Width = 106;
            // 
            // colSiteAddressLine4
            // 
            this.colSiteAddressLine4.Caption = "Address Line 4";
            this.colSiteAddressLine4.FieldName = "SiteAddressLine4";
            this.colSiteAddressLine4.Name = "colSiteAddressLine4";
            this.colSiteAddressLine4.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine4.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine4.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine4.Visible = true;
            this.colSiteAddressLine4.VisibleIndex = 16;
            this.colSiteAddressLine4.Width = 108;
            // 
            // colSiteAddressLine5
            // 
            this.colSiteAddressLine5.Caption = "Address Line 5";
            this.colSiteAddressLine5.FieldName = "SiteAddressLine5";
            this.colSiteAddressLine5.Name = "colSiteAddressLine5";
            this.colSiteAddressLine5.OptionsColumn.AllowEdit = false;
            this.colSiteAddressLine5.OptionsColumn.AllowFocus = false;
            this.colSiteAddressLine5.OptionsColumn.ReadOnly = true;
            this.colSiteAddressLine5.Visible = true;
            this.colSiteAddressLine5.VisibleIndex = 17;
            this.colSiteAddressLine5.Width = 104;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Visible = true;
            this.colSitePostcode.VisibleIndex = 18;
            // 
            // colSiteTelephone
            // 
            this.colSiteTelephone.Caption = "Telephone";
            this.colSiteTelephone.FieldName = "SiteTelephone";
            this.colSiteTelephone.Name = "colSiteTelephone";
            this.colSiteTelephone.OptionsColumn.AllowEdit = false;
            this.colSiteTelephone.OptionsColumn.AllowFocus = false;
            this.colSiteTelephone.OptionsColumn.ReadOnly = true;
            this.colSiteTelephone.Visible = true;
            this.colSiteTelephone.VisibleIndex = 25;
            this.colSiteTelephone.Width = 92;
            // 
            // colSiteMobile
            // 
            this.colSiteMobile.Caption = "Mobile";
            this.colSiteMobile.FieldName = "SiteMobile";
            this.colSiteMobile.Name = "colSiteMobile";
            this.colSiteMobile.OptionsColumn.AllowEdit = false;
            this.colSiteMobile.OptionsColumn.AllowFocus = false;
            this.colSiteMobile.OptionsColumn.ReadOnly = true;
            this.colSiteMobile.Visible = true;
            this.colSiteMobile.VisibleIndex = 26;
            this.colSiteMobile.Width = 92;
            // 
            // colSiteFax
            // 
            this.colSiteFax.Caption = "Fax";
            this.colSiteFax.FieldName = "SiteFax";
            this.colSiteFax.Name = "colSiteFax";
            this.colSiteFax.OptionsColumn.AllowEdit = false;
            this.colSiteFax.OptionsColumn.AllowFocus = false;
            this.colSiteFax.OptionsColumn.ReadOnly = true;
            this.colSiteFax.Visible = true;
            this.colSiteFax.VisibleIndex = 27;
            this.colSiteFax.Width = 99;
            // 
            // colSiteEmail
            // 
            this.colSiteEmail.Caption = "Email";
            this.colSiteEmail.FieldName = "SiteEmail";
            this.colSiteEmail.Name = "colSiteEmail";
            this.colSiteEmail.OptionsColumn.AllowEdit = false;
            this.colSiteEmail.OptionsColumn.AllowFocus = false;
            this.colSiteEmail.OptionsColumn.ReadOnly = true;
            this.colSiteEmail.Visible = true;
            this.colSiteEmail.VisibleIndex = 28;
            this.colSiteEmail.Width = 150;
            // 
            // colXCoordinate
            // 
            this.colXCoordinate.Caption = "Easting";
            this.colXCoordinate.FieldName = "XCoordinate";
            this.colXCoordinate.Name = "colXCoordinate";
            this.colXCoordinate.OptionsColumn.AllowEdit = false;
            this.colXCoordinate.OptionsColumn.AllowFocus = false;
            this.colXCoordinate.OptionsColumn.ReadOnly = true;
            this.colXCoordinate.Visible = true;
            this.colXCoordinate.VisibleIndex = 20;
            this.colXCoordinate.Width = 83;
            // 
            // colYCoordinate
            // 
            this.colYCoordinate.Caption = "Northing";
            this.colYCoordinate.FieldName = "YCoordinate";
            this.colYCoordinate.Name = "colYCoordinate";
            this.colYCoordinate.OptionsColumn.AllowEdit = false;
            this.colYCoordinate.OptionsColumn.AllowFocus = false;
            this.colYCoordinate.OptionsColumn.ReadOnly = true;
            this.colYCoordinate.Visible = true;
            this.colYCoordinate.VisibleIndex = 21;
            this.colYCoordinate.Width = 89;
            // 
            // colClientRemarks
            // 
            this.colClientRemarks.ColumnEdit = this.repositoryItemMemoExEditRemarks;
            this.colClientRemarks.FieldName = "ClientRemarks";
            this.colClientRemarks.Name = "colClientRemarks";
            this.colClientRemarks.OptionsColumn.ReadOnly = true;
            this.colClientRemarks.Visible = true;
            this.colClientRemarks.VisibleIndex = 33;
            this.colClientRemarks.Width = 100;
            // 
            // repositoryItemMemoExEditRemarks
            // 
            this.repositoryItemMemoExEditRemarks.AutoHeight = false;
            this.repositoryItemMemoExEditRemarks.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEditRemarks.Name = "repositoryItemMemoExEditRemarks";
            this.repositoryItemMemoExEditRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEditRemarks.ShowIcon = false;
            // 
            // colSiteRemarks
            // 
            this.colSiteRemarks.ColumnEdit = this.repositoryItemMemoExEditRemarks;
            this.colSiteRemarks.FieldName = "SiteRemarks";
            this.colSiteRemarks.Name = "colSiteRemarks";
            this.colSiteRemarks.OptionsColumn.ReadOnly = true;
            this.colSiteRemarks.Visible = true;
            this.colSiteRemarks.VisibleIndex = 32;
            this.colSiteRemarks.Width = 91;
            // 
            // colLinkedAssetCount
            // 
            this.colLinkedAssetCount.Caption = " Linked Records";
            this.colLinkedAssetCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedAssetCount.FieldName = "LinkedAssetCount";
            this.colLinkedAssetCount.Name = "colLinkedAssetCount";
            this.colLinkedAssetCount.OptionsColumn.ReadOnly = true;
            this.colLinkedAssetCount.Visible = true;
            this.colLinkedAssetCount.VisibleIndex = 6;
            this.colLinkedAssetCount.Width = 112;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colContactPerson
            // 
            this.colContactPerson.Caption = "Contact Person";
            this.colContactPerson.FieldName = "ContactPerson";
            this.colContactPerson.Name = "colContactPerson";
            this.colContactPerson.OptionsColumn.AllowEdit = false;
            this.colContactPerson.OptionsColumn.AllowFocus = false;
            this.colContactPerson.OptionsColumn.ReadOnly = true;
            this.colContactPerson.Visible = true;
            this.colContactPerson.VisibleIndex = 10;
            this.colContactPerson.Width = 105;
            // 
            // colContactPersonPosition
            // 
            this.colContactPersonPosition.Caption = "Contact Person Position";
            this.colContactPersonPosition.FieldName = "ContactPersonPosition";
            this.colContactPersonPosition.Name = "colContactPersonPosition";
            this.colContactPersonPosition.OptionsColumn.AllowEdit = false;
            this.colContactPersonPosition.OptionsColumn.AllowFocus = false;
            this.colContactPersonPosition.OptionsColumn.ReadOnly = true;
            this.colContactPersonPosition.Visible = true;
            this.colContactPersonPosition.VisibleIndex = 11;
            this.colContactPersonPosition.Width = 138;
            // 
            // colMappingWorkspaceID
            // 
            this.colMappingWorkspaceID.Caption = "Mapping Workspace ID";
            this.colMappingWorkspaceID.FieldName = "MappingWorkspaceID";
            this.colMappingWorkspaceID.Name = "colMappingWorkspaceID";
            this.colMappingWorkspaceID.OptionsColumn.AllowEdit = false;
            this.colMappingWorkspaceID.OptionsColumn.AllowFocus = false;
            this.colMappingWorkspaceID.OptionsColumn.ReadOnly = true;
            this.colMappingWorkspaceID.Width = 135;
            // 
            // colWorkspaceName
            // 
            this.colWorkspaceName.Caption = "Mapping Workspace";
            this.colWorkspaceName.FieldName = "WorkspaceName";
            this.colWorkspaceName.Name = "colWorkspaceName";
            this.colWorkspaceName.OptionsColumn.AllowEdit = false;
            this.colWorkspaceName.OptionsColumn.AllowFocus = false;
            this.colWorkspaceName.OptionsColumn.ReadOnly = true;
            this.colWorkspaceName.Visible = true;
            this.colWorkspaceName.VisibleIndex = 29;
            this.colWorkspaceName.Width = 165;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.Caption = "Company Code";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 94;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 4;
            this.colCompanyName.Width = 120;
            // 
            // colSiteOrder
            // 
            this.colSiteOrder.Caption = "Site Order";
            this.colSiteOrder.FieldName = "SiteOrder";
            this.colSiteOrder.Name = "colSiteOrder";
            this.colSiteOrder.OptionsColumn.AllowEdit = false;
            this.colSiteOrder.OptionsColumn.AllowFocus = false;
            this.colSiteOrder.OptionsColumn.ReadOnly = true;
            this.colSiteOrder.Visible = true;
            this.colSiteOrder.VisibleIndex = 12;
            this.colSiteOrder.Width = 85;
            // 
            // colCompanyID
            // 
            this.colCompanyID.CustomizationCaption = "Company ID";
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            // 
            // colHubID
            // 
            this.colHubID.CustomizationCaption = "Hub ID";
            this.colHubID.FieldName = "HubID";
            this.colHubID.Name = "colHubID";
            this.colHubID.OptionsColumn.AllowEdit = false;
            this.colHubID.OptionsColumn.AllowFocus = false;
            this.colHubID.OptionsColumn.ReadOnly = true;
            this.colHubID.Visible = true;
            this.colHubID.VisibleIndex = 2;
            // 
            // colGrittingSite
            // 
            this.colGrittingSite.Caption = "Gritting";
            this.colGrittingSite.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colGrittingSite.FieldName = "GrittingSite";
            this.colGrittingSite.Name = "colGrittingSite";
            this.colGrittingSite.OptionsColumn.AllowEdit = false;
            this.colGrittingSite.OptionsColumn.AllowFocus = false;
            this.colGrittingSite.OptionsColumn.ReadOnly = true;
            this.colGrittingSite.Visible = true;
            this.colGrittingSite.VisibleIndex = 8;
            this.colGrittingSite.Width = 57;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colClientsSiteCode
            // 
            this.colClientsSiteCode.Caption = "Clients Site Code";
            this.colClientsSiteCode.FieldName = "ClientsSiteCode";
            this.colClientsSiteCode.Name = "colClientsSiteCode";
            this.colClientsSiteCode.OptionsColumn.AllowEdit = false;
            this.colClientsSiteCode.OptionsColumn.AllowFocus = false;
            this.colClientsSiteCode.OptionsColumn.ReadOnly = true;
            this.colClientsSiteCode.Visible = true;
            this.colClientsSiteCode.VisibleIndex = 7;
            this.colClientsSiteCode.Width = 113;
            // 
            // colClientsSiteID
            // 
            this.colClientsSiteID.Caption = "Clients Site ID";
            this.colClientsSiteID.FieldName = "ClientsSiteID";
            this.colClientsSiteID.Name = "colClientsSiteID";
            this.colClientsSiteID.OptionsColumn.AllowEdit = false;
            this.colClientsSiteID.OptionsColumn.AllowFocus = false;
            this.colClientsSiteID.OptionsColumn.ReadOnly = true;
            this.colClientsSiteID.Visible = true;
            this.colClientsSiteID.VisibleIndex = 3;
            this.colClientsSiteID.Width = 88;
            // 
            // colIsSnowClearanceSite
            // 
            this.colIsSnowClearanceSite.Caption = "Snow Clearance";
            this.colIsSnowClearanceSite.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colIsSnowClearanceSite.FieldName = "IsSnowClearanceSite";
            this.colIsSnowClearanceSite.Name = "colIsSnowClearanceSite";
            this.colIsSnowClearanceSite.OptionsColumn.AllowEdit = false;
            this.colIsSnowClearanceSite.OptionsColumn.AllowFocus = false;
            this.colIsSnowClearanceSite.OptionsColumn.ReadOnly = true;
            this.colIsSnowClearanceSite.Visible = true;
            this.colIsSnowClearanceSite.VisibleIndex = 9;
            this.colIsSnowClearanceSite.Width = 98;
            // 
            // colLocationX
            // 
            this.colLocationX.Caption = "Latitude";
            this.colLocationX.FieldName = "LocationX";
            this.colLocationX.Name = "colLocationX";
            this.colLocationX.OptionsColumn.AllowEdit = false;
            this.colLocationX.OptionsColumn.AllowFocus = false;
            this.colLocationX.OptionsColumn.ReadOnly = true;
            this.colLocationX.Visible = true;
            this.colLocationX.VisibleIndex = 22;
            this.colLocationX.Width = 87;
            // 
            // colLocationY
            // 
            this.colLocationY.Caption = "Longitude";
            this.colLocationY.FieldName = "LocationY";
            this.colLocationY.Name = "colLocationY";
            this.colLocationY.OptionsColumn.AllowEdit = false;
            this.colLocationY.OptionsColumn.AllowFocus = false;
            this.colLocationY.OptionsColumn.ReadOnly = true;
            this.colLocationY.Visible = true;
            this.colLocationY.VisibleIndex = 23;
            this.colLocationY.Width = 88;
            // 
            // colCOAddress
            // 
            this.colCOAddress.Caption = "C\\O Address";
            this.colCOAddress.ColumnEdit = this.repositoryItemMemoExEditRemarks;
            this.colCOAddress.FieldName = "COAddress";
            this.colCOAddress.Name = "colCOAddress";
            this.colCOAddress.OptionsColumn.ReadOnly = true;
            this.colCOAddress.Visible = true;
            this.colCOAddress.VisibleIndex = 19;
            this.colCOAddress.Width = 124;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            // 
            // colGeoFenceDistance
            // 
            this.colGeoFenceDistance.Caption = "Geo Fence Distance";
            this.colGeoFenceDistance.ColumnEdit = this.repositoryItemTextEditMetres;
            this.colGeoFenceDistance.FieldName = "GeoFenceDistance";
            this.colGeoFenceDistance.Name = "colGeoFenceDistance";
            this.colGeoFenceDistance.OptionsColumn.AllowEdit = false;
            this.colGeoFenceDistance.OptionsColumn.AllowFocus = false;
            this.colGeoFenceDistance.OptionsColumn.ReadOnly = true;
            this.colGeoFenceDistance.Visible = true;
            this.colGeoFenceDistance.VisibleIndex = 24;
            this.colGeoFenceDistance.Width = 114;
            // 
            // repositoryItemTextEditMetres
            // 
            this.repositoryItemTextEditMetres.AutoHeight = false;
            this.repositoryItemTextEditMetres.Mask.EditMask = "######0 Metres";
            this.repositoryItemTextEditMetres.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMetres.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMetres.Name = "repositoryItemTextEditMetres";
            // 
            // colSiteKML
            // 
            this.colSiteKML.Caption = "Site KML";
            this.colSiteKML.ColumnEdit = this.repositoryItemHyperLinkEditViewSiteKML;
            this.colSiteKML.FieldName = "SiteKML";
            this.colSiteKML.Name = "colSiteKML";
            this.colSiteKML.OptionsColumn.ReadOnly = true;
            this.colSiteKML.Visible = true;
            this.colSiteKML.VisibleIndex = 30;
            this.colSiteKML.Width = 59;
            // 
            // repositoryItemHyperLinkEditViewSiteKML
            // 
            this.repositoryItemHyperLinkEditViewSiteKML.AutoHeight = false;
            this.repositoryItemHyperLinkEditViewSiteKML.Name = "repositoryItemHyperLinkEditViewSiteKML";
            this.repositoryItemHyperLinkEditViewSiteKML.SingleClick = true;
            this.repositoryItemHyperLinkEditViewSiteKML.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditViewSiteKML_OpenLink);
            // 
            // colSiteImageFile
            // 
            this.colSiteImageFile.Caption = "App Site Image ";
            this.colSiteImageFile.ColumnEdit = this.repositoryItemHyperLinkEditAppSiteImage;
            this.colSiteImageFile.FieldName = "SiteImageFile";
            this.colSiteImageFile.Name = "colSiteImageFile";
            this.colSiteImageFile.OptionsColumn.ReadOnly = true;
            this.colSiteImageFile.Visible = true;
            this.colSiteImageFile.VisibleIndex = 31;
            this.colSiteImageFile.Width = 117;
            // 
            // repositoryItemHyperLinkEditAppSiteImage
            // 
            this.repositoryItemHyperLinkEditAppSiteImage.AutoHeight = false;
            this.repositoryItemHyperLinkEditAppSiteImage.Name = "repositoryItemHyperLinkEditAppSiteImage";
            this.repositoryItemHyperLinkEditAppSiteImage.SingleClick = true;
            this.repositoryItemHyperLinkEditAppSiteImage.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditAppSiteImage_OpenLink);
            // 
            // popupContainerControlCompanies
            // 
            this.popupContainerControlCompanies.Controls.Add(this.btnCompanyFilterOK);
            this.popupContainerControlCompanies.Controls.Add(this.gridControl9);
            this.popupContainerControlCompanies.Location = new System.Drawing.Point(570, 92);
            this.popupContainerControlCompanies.Name = "popupContainerControlCompanies";
            this.popupContainerControlCompanies.Size = new System.Drawing.Size(189, 163);
            this.popupContainerControlCompanies.TabIndex = 16;
            // 
            // btnCompanyFilterOK
            // 
            this.btnCompanyFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCompanyFilterOK.Location = new System.Drawing.Point(3, 138);
            this.btnCompanyFilterOK.Name = "btnCompanyFilterOK";
            this.btnCompanyFilterOK.Size = new System.Drawing.Size(77, 22);
            this.btnCompanyFilterOK.TabIndex = 18;
            this.btnCompanyFilterOK.Text = "OK";
            this.btnCompanyFilterOK.Click += new System.EventHandler(this.btnCompanyFilterOK_Click);
            // 
            // gridControl9
            // 
            this.gridControl9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl9.DataSource = this.sp04237GCCompanyFilterListBindingSource;
            this.gridControl9.Location = new System.Drawing.Point(3, 3);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.Size = new System.Drawing.Size(183, 133);
            this.gridControl9.TabIndex = 1;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // sp04237GCCompanyFilterListBindingSource
            // 
            this.sp04237GCCompanyFilterListBindingSource.DataMember = "sp04237_GC_Company_Filter_List";
            this.sp04237GCCompanyFilterListBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn1,
            this.colCompanyOrder});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView9.OptionsView.ShowIndicator = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompanyOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Company ID";
            this.gridColumn5.FieldName = "CompanyID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 80;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Company Name";
            this.gridColumn6.FieldName = "CompanyName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 152;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Company Code";
            this.gridColumn1.FieldName = "CompanyCode";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 94;
            // 
            // colCompanyOrder
            // 
            this.colCompanyOrder.Caption = "Order";
            this.colCompanyOrder.FieldName = "CompanyOrder";
            this.colCompanyOrder.Name = "colCompanyOrder";
            this.colCompanyOrder.OptionsColumn.AllowEdit = false;
            this.colCompanyOrder.OptionsColumn.AllowFocus = false;
            this.colCompanyOrder.OptionsColumn.ReadOnly = true;
            // 
            // popupContainerControlSites
            // 
            this.popupContainerControlSites.Controls.Add(this.btnClientFilterOK);
            this.popupContainerControlSites.Controls.Add(this.gridControl2);
            this.popupContainerControlSites.Location = new System.Drawing.Point(573, 261);
            this.popupContainerControlSites.Name = "popupContainerControlSites";
            this.popupContainerControlSites.Size = new System.Drawing.Size(380, 300);
            this.popupContainerControlSites.TabIndex = 1;
            // 
            // btnClientFilterOK
            // 
            this.btnClientFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClientFilterOK.Location = new System.Drawing.Point(3, 274);
            this.btnClientFilterOK.Name = "btnClientFilterOK";
            this.btnClientFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnClientFilterOK.TabIndex = 1;
            this.btnClientFilterOK.Text = "OK";
            this.btnClientFilterOK.Click += new System.EventHandler(this.btnClientFilterOK_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp03005EPClientListAllBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(374, 268);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp03005EPClientListAllBindingSource
            // 
            this.sp03005EPClientListAllBindingSource.DataMember = "sp03005_EP_Client_List_All";
            this.sp03005EPClientListAllBindingSource.DataSource = this.dataSet_EP;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID1,
            this.colClientName1,
            this.colClientCode1,
            this.colClientTypeID,
            this.colClientTypeDescription,
            this.colRemarks,
            this.colDailyGrittingReport});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 0;
            this.colClientName1.Width = 210;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            this.colClientCode1.Visible = true;
            this.colClientCode1.VisibleIndex = 1;
            this.colClientCode1.Width = 76;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            this.colClientTypeID.Width = 92;
            // 
            // colClientTypeDescription
            // 
            this.colClientTypeDescription.Caption = "Client Type";
            this.colClientTypeDescription.FieldName = "ClientTypeDescription";
            this.colClientTypeDescription.Name = "colClientTypeDescription";
            this.colClientTypeDescription.OptionsColumn.AllowEdit = false;
            this.colClientTypeDescription.OptionsColumn.AllowFocus = false;
            this.colClientTypeDescription.OptionsColumn.ReadOnly = true;
            this.colClientTypeDescription.Visible = true;
            this.colClientTypeDescription.VisibleIndex = 2;
            this.colClientTypeDescription.Width = 131;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            // 
            // colDailyGrittingReport
            // 
            this.colDailyGrittingReport.Caption = "Daily Gritting Report";
            this.colDailyGrittingReport.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDailyGrittingReport.FieldName = "DailyGrittingReport";
            this.colDailyGrittingReport.Name = "colDailyGrittingReport";
            this.colDailyGrittingReport.OptionsColumn.AllowEdit = false;
            this.colDailyGrittingReport.OptionsColumn.AllowFocus = false;
            this.colDailyGrittingReport.OptionsColumn.ReadOnly = true;
            this.colDailyGrittingReport.Visible = true;
            this.colDailyGrittingReport.VisibleIndex = 3;
            this.colDailyGrittingReport.Width = 128;
            // 
            // popupContainerControlShowTabPages
            // 
            this.popupContainerControlShowTabPages.Controls.Add(this.gridControl8);
            this.popupContainerControlShowTabPages.Controls.Add(this.btnOK2);
            this.popupContainerControlShowTabPages.Location = new System.Drawing.Point(350, 92);
            this.popupContainerControlShowTabPages.Name = "popupContainerControlShowTabPages";
            this.popupContainerControlShowTabPages.Size = new System.Drawing.Size(217, 245);
            this.popupContainerControlShowTabPages.TabIndex = 7;
            // 
            // gridControl8
            // 
            this.gridControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl8.DataSource = this.sp04022CoreDummyTabPageListBindingSource;
            this.gridControl8.Location = new System.Drawing.Point(3, 3);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit7});
            this.gridControl8.Size = new System.Drawing.Size(211, 215);
            this.gridControl8.TabIndex = 1;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp04022CoreDummyTabPageListBindingSource
            // 
            this.sp04022CoreDummyTabPageListBindingSource.DataMember = "sp04022_Core_Dummy_TabPageList";
            this.sp04022CoreDummyTabPageListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTabPageName});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colTabPageName
            // 
            this.colTabPageName.Caption = "Linked Records";
            this.colTabPageName.FieldName = "TabPageName";
            this.colTabPageName.Name = "colTabPageName";
            this.colTabPageName.OptionsColumn.AllowEdit = false;
            this.colTabPageName.OptionsColumn.AllowFocus = false;
            this.colTabPageName.OptionsColumn.ReadOnly = true;
            this.colTabPageName.Visible = true;
            this.colTabPageName.VisibleIndex = 0;
            this.colTabPageName.Width = 179;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Caption = "Check";
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // btnOK2
            // 
            this.btnOK2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK2.Location = new System.Drawing.Point(3, 220);
            this.btnOK2.Name = "btnOK2";
            this.btnOK2.Size = new System.Drawing.Size(75, 23);
            this.btnOK2.TabIndex = 0;
            this.btnOK2.Text = "OK";
            this.btnOK2.Click += new System.EventHandler(this.btnOK2_Click);
            // 
            // popupContainerControlLinkedGrittingCalloutsFilter
            // 
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.layoutControl2);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.btnGritCalloutFilterOK);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Location = new System.Drawing.Point(4, 92);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Name = "popupContainerControlLinkedGrittingCalloutsFilter";
            this.popupContainerControlLinkedGrittingCalloutsFilter.Size = new System.Drawing.Size(340, 245);
            this.popupContainerControlLinkedGrittingCalloutsFilter.TabIndex = 2;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl2.Controls.Add(this.gridControl5);
            this.layoutControl2.Controls.Add(this.dateEditToDate);
            this.layoutControl2.Controls.Add(this.dateEditFromDate);
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1185, 135, 250, 350);
            this.layoutControl2.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(340, 220);
            this.layoutControl2.TabIndex = 12;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp04001GCJobCallOutStatusesBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(12, 32);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(316, 152);
            this.gridControl5.TabIndex = 4;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04001GCJobCallOutStatusesBindingSource
            // 
            this.sp04001GCJobCallOutStatusesBindingSource.DataMember = "sp04001_GC_Job_CallOut_Statuses";
            this.sp04001GCJobCallOutStatusesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colValue,
            this.colOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Callout Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 263;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(216, 188);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all ca" +
    "llouts will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(112, 20);
            this.dateEditToDate.StyleController = this.layoutControl2;
            this.dateEditToDate.TabIndex = 11;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(69, 188);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Clear Date - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all ca" +
    "llouts will be loaded.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip3, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(98, 20);
            this.dateEditFromDate.StyleController = this.layoutControl2;
            this.dateEditFromDate.TabIndex = 10;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(340, 220);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Linked Gritting Callouts - Data Filter";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(336, 216);
            this.layoutControlGroup3.Text = "Linked Gritting Callouts - Data Filter";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl5;
            this.layoutControlItem3.CustomizationFormText = "Callout Job Status Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(320, 156);
            this.layoutControlItem3.Text = "Callout Job Status Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateEditFromDate;
            this.layoutControlItem4.CustomizationFormText = "From Date:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(159, 24);
            this.layoutControlItem4.Text = "From Date:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dateEditToDate;
            this.layoutControlItem5.CustomizationFormText = "To Date:";
            this.layoutControlItem5.Location = new System.Drawing.Point(159, 156);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem5.Text = "To Date:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(42, 13);
            // 
            // btnGritCalloutFilterOK
            // 
            this.btnGritCalloutFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGritCalloutFilterOK.Location = new System.Drawing.Point(3, 220);
            this.btnGritCalloutFilterOK.Name = "btnGritCalloutFilterOK";
            this.btnGritCalloutFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnGritCalloutFilterOK.TabIndex = 12;
            this.btnGritCalloutFilterOK.Text = "OK";
            this.btnGritCalloutFilterOK.Click += new System.EventHandler(this.btnGritCalloutFilterOK_Click);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1026, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageLinkedDocuments;
            this.xtraTabControl1.Size = new System.Drawing.Size(1024, 203);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageLinkedDocuments,
            this.xtraTabPage5,
            this.xtraTabPage1,
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPageLinkedDocuments
            // 
            this.xtraTabPageLinkedDocuments.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPageLinkedDocuments.Name = "xtraTabPageLinkedDocuments";
            this.xtraTabPageLinkedDocuments.Size = new System.Drawing.Size(1019, 177);
            this.xtraTabPageLinkedDocuments.Text = "Linked Documents";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl4;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1019, 177);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemHyperLinkEdit3});
            this.gridControl4.Size = new System.Drawing.Size(1019, 177);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.gridControl10);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1019, 177);
            this.xtraTabPage5.Text = "Linked Service Areas";
            // 
            // gridControl10
            // 
            this.gridControl10.DataSource = this.sp04332GCServiceAreasLinkedToSiteBindingSource;
            this.gridControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl10.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Move Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Move Down", "down"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Save Sort", "set_order")});
            this.gridControl10.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl10_EmbeddedNavigator_ButtonClick);
            this.gridControl10.Location = new System.Drawing.Point(0, 0);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemCheckEdit4});
            this.gridControl10.Size = new System.Drawing.Size(1019, 177);
            this.gridControl10.TabIndex = 1;
            this.gridControl10.UseEmbeddedNavigator = true;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp04332GCServiceAreasLinkedToSiteBindingSource
            // 
            this.sp04332GCServiceAreasLinkedToSiteBindingSource.DataMember = "sp04332_GC_Service_Areas_Linked_To_Site";
            this.sp04332GCServiceAreasLinkedToSiteBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteServiceAreaID,
            this.colSiteID4,
            this.colDescriptionID,
            this.colAppliesToGritting,
            this.colAreaOrder,
            this.colRemarks4,
            this.colSiteName4,
            this.colAreaDescription,
            this.colAppliesToSnowClearance,
            this.colGroupID});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.GroupCount = 1;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.MultiSelect = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAreaOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAreaDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView10.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView10.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView10.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView10.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView10.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView10_MouseUp);
            this.gridView10.DoubleClick += new System.EventHandler(this.gridView10_DoubleClick);
            this.gridView10.GotFocus += new System.EventHandler(this.gridView10_GotFocus);
            // 
            // colSiteServiceAreaID
            // 
            this.colSiteServiceAreaID.Caption = "Site Service Area ID";
            this.colSiteServiceAreaID.FieldName = "SiteServiceAreaID";
            this.colSiteServiceAreaID.Name = "colSiteServiceAreaID";
            this.colSiteServiceAreaID.OptionsColumn.AllowEdit = false;
            this.colSiteServiceAreaID.OptionsColumn.AllowFocus = false;
            this.colSiteServiceAreaID.OptionsColumn.ReadOnly = true;
            this.colSiteServiceAreaID.Width = 117;
            // 
            // colSiteID4
            // 
            this.colSiteID4.Caption = "Site ID";
            this.colSiteID4.FieldName = "SiteID";
            this.colSiteID4.Name = "colSiteID4";
            this.colSiteID4.OptionsColumn.AllowEdit = false;
            this.colSiteID4.OptionsColumn.AllowFocus = false;
            this.colSiteID4.OptionsColumn.ReadOnly = true;
            // 
            // colDescriptionID
            // 
            this.colDescriptionID.Caption = "Service Area ID";
            this.colDescriptionID.FieldName = "DescriptionID";
            this.colDescriptionID.Name = "colDescriptionID";
            this.colDescriptionID.OptionsColumn.AllowEdit = false;
            this.colDescriptionID.OptionsColumn.AllowFocus = false;
            this.colDescriptionID.OptionsColumn.ReadOnly = true;
            this.colDescriptionID.Width = 96;
            // 
            // colAppliesToGritting
            // 
            this.colAppliesToGritting.Caption = "Applies To Gritting";
            this.colAppliesToGritting.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colAppliesToGritting.FieldName = "AppliesToGritting";
            this.colAppliesToGritting.Name = "colAppliesToGritting";
            this.colAppliesToGritting.OptionsColumn.AllowEdit = false;
            this.colAppliesToGritting.OptionsColumn.AllowFocus = false;
            this.colAppliesToGritting.OptionsColumn.ReadOnly = true;
            this.colAppliesToGritting.Visible = true;
            this.colAppliesToGritting.VisibleIndex = 1;
            this.colAppliesToGritting.Width = 108;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colAreaOrder
            // 
            this.colAreaOrder.Caption = "Order";
            this.colAreaOrder.FieldName = "AreaOrder";
            this.colAreaOrder.Name = "colAreaOrder";
            this.colAreaOrder.OptionsColumn.AllowEdit = false;
            this.colAreaOrder.OptionsColumn.AllowFocus = false;
            this.colAreaOrder.OptionsColumn.ReadOnly = true;
            this.colAreaOrder.Visible = true;
            this.colAreaOrder.VisibleIndex = 4;
            this.colAreaOrder.Width = 62;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 5;
            this.colRemarks4.Width = 224;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colSiteName4
            // 
            this.colSiteName4.Caption = "Linked To";
            this.colSiteName4.FieldName = "SiteName";
            this.colSiteName4.Name = "colSiteName4";
            this.colSiteName4.OptionsColumn.AllowEdit = false;
            this.colSiteName4.OptionsColumn.AllowFocus = false;
            this.colSiteName4.OptionsColumn.ReadOnly = true;
            this.colSiteName4.Visible = true;
            this.colSiteName4.VisibleIndex = 4;
            this.colSiteName4.Width = 243;
            // 
            // colAreaDescription
            // 
            this.colAreaDescription.Caption = "Service Area";
            this.colAreaDescription.FieldName = "AreaDescription";
            this.colAreaDescription.Name = "colAreaDescription";
            this.colAreaDescription.OptionsColumn.AllowEdit = false;
            this.colAreaDescription.OptionsColumn.AllowFocus = false;
            this.colAreaDescription.OptionsColumn.ReadOnly = true;
            this.colAreaDescription.Visible = true;
            this.colAreaDescription.VisibleIndex = 0;
            this.colAreaDescription.Width = 254;
            // 
            // colAppliesToSnowClearance
            // 
            this.colAppliesToSnowClearance.Caption = "Applies To Snow Clearance";
            this.colAppliesToSnowClearance.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colAppliesToSnowClearance.FieldName = "AppliesToSnowClearance";
            this.colAppliesToSnowClearance.Name = "colAppliesToSnowClearance";
            this.colAppliesToSnowClearance.OptionsColumn.AllowEdit = false;
            this.colAppliesToSnowClearance.OptionsColumn.AllowFocus = false;
            this.colAppliesToSnowClearance.OptionsColumn.ReadOnly = true;
            this.colAppliesToSnowClearance.Visible = true;
            this.colAppliesToSnowClearance.VisibleIndex = 2;
            this.colAppliesToSnowClearance.Width = 150;
            // 
            // colGroupID
            // 
            this.colGroupID.Caption = "Group ID";
            this.colGroupID.FieldName = "GroupID";
            this.colGroupID.Name = "colGroupID";
            this.colGroupID.OptionsColumn.AllowEdit = false;
            this.colGroupID.OptionsColumn.AllowFocus = false;
            this.colGroupID.OptionsColumn.ReadOnly = true;
            this.colGroupID.Visible = true;
            this.colGroupID.VisibleIndex = 3;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridSplitContainer3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1019, 177);
            this.xtraTabPage1.Text = "Linked Inspections";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl3;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer3.Size = new System.Drawing.Size(1019, 177);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp03010EPSiteManagerLinkedInspectionsBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTimeEdit1,
            this.repositoryItemTimeEdit2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemHyperLinkEdit2});
            this.gridControl3.Size = new System.Drawing.Size(1019, 177);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp03010EPSiteManagerLinkedInspectionsBindingSource
            // 
            this.sp03010EPSiteManagerLinkedInspectionsBindingSource.DataMember = "sp03010_EP_Site_Manager_Linked_Inspections";
            this.sp03010EPSiteManagerLinkedInspectionsBindingSource.DataSource = this.dataSet_EP;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID,
            this.colSiteID1,
            this.colSiteName1,
            this.colClientID2,
            this.colInspectionDate,
            this.colInspectorID,
            this.colInspector,
            this.colInspectionStartTime,
            this.colInspectionEndTime,
            this.colTotalTimeDeductions,
            this.colTotalTimeDeductionsRemarks,
            this.colTotalTimeTaken,
            this.colRemarks1,
            this.colLinkedActionCount});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEdit);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView3_ShowingEditor);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            this.colInspectionID.Width = 87;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            this.colSiteID1.Width = 54;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Width = 164;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.Width = 66;
            // 
            // colInspectionDate
            // 
            this.colInspectionDate.Caption = "Inspection Date";
            this.colInspectionDate.FieldName = "InspectionDate";
            this.colInspectionDate.Name = "colInspectionDate";
            this.colInspectionDate.OptionsColumn.AllowEdit = false;
            this.colInspectionDate.OptionsColumn.AllowFocus = false;
            this.colInspectionDate.OptionsColumn.ReadOnly = true;
            this.colInspectionDate.Visible = true;
            this.colInspectionDate.VisibleIndex = 0;
            this.colInspectionDate.Width = 112;
            // 
            // colInspectorID
            // 
            this.colInspectorID.Caption = "Inspection ID";
            this.colInspectorID.FieldName = "InspectorID";
            this.colInspectorID.Name = "colInspectorID";
            this.colInspectorID.OptionsColumn.AllowEdit = false;
            this.colInspectorID.OptionsColumn.AllowFocus = false;
            this.colInspectorID.OptionsColumn.ReadOnly = true;
            this.colInspectorID.Width = 87;
            // 
            // colInspector
            // 
            this.colInspector.Caption = "Inspector";
            this.colInspector.FieldName = "Inspector";
            this.colInspector.Name = "colInspector";
            this.colInspector.OptionsColumn.AllowEdit = false;
            this.colInspector.OptionsColumn.AllowFocus = false;
            this.colInspector.OptionsColumn.ReadOnly = true;
            this.colInspector.Visible = true;
            this.colInspector.VisibleIndex = 1;
            this.colInspector.Width = 140;
            // 
            // colInspectionStartTime
            // 
            this.colInspectionStartTime.Caption = "Start Time";
            this.colInspectionStartTime.ColumnEdit = this.repositoryItemTimeEdit1;
            this.colInspectionStartTime.FieldName = "InspectionStartTime";
            this.colInspectionStartTime.Name = "colInspectionStartTime";
            this.colInspectionStartTime.OptionsColumn.AllowEdit = false;
            this.colInspectionStartTime.OptionsColumn.AllowFocus = false;
            this.colInspectionStartTime.OptionsColumn.ReadOnly = true;
            this.colInspectionStartTime.Visible = true;
            this.colInspectionStartTime.VisibleIndex = 2;
            this.colInspectionStartTime.Width = 100;
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit1.Mask.EditMask = "g";
            this.repositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // colInspectionEndTime
            // 
            this.colInspectionEndTime.Caption = "End Time";
            this.colInspectionEndTime.ColumnEdit = this.repositoryItemTimeEdit2;
            this.colInspectionEndTime.FieldName = "InspectionEndTime";
            this.colInspectionEndTime.Name = "colInspectionEndTime";
            this.colInspectionEndTime.OptionsColumn.AllowEdit = false;
            this.colInspectionEndTime.OptionsColumn.AllowFocus = false;
            this.colInspectionEndTime.OptionsColumn.ReadOnly = true;
            this.colInspectionEndTime.Visible = true;
            this.colInspectionEndTime.VisibleIndex = 3;
            this.colInspectionEndTime.Width = 100;
            // 
            // repositoryItemTimeEdit2
            // 
            this.repositoryItemTimeEdit2.AutoHeight = false;
            this.repositoryItemTimeEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemTimeEdit2.Mask.EditMask = "g";
            this.repositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTimeEdit2.Name = "repositoryItemTimeEdit2";
            // 
            // colTotalTimeDeductions
            // 
            this.colTotalTimeDeductions.Caption = "Total Time Deductions";
            this.colTotalTimeDeductions.FieldName = "TotalTimeDeductions";
            this.colTotalTimeDeductions.Name = "colTotalTimeDeductions";
            this.colTotalTimeDeductions.OptionsColumn.AllowEdit = false;
            this.colTotalTimeDeductions.OptionsColumn.AllowFocus = false;
            this.colTotalTimeDeductions.OptionsColumn.ReadOnly = true;
            this.colTotalTimeDeductions.Visible = true;
            this.colTotalTimeDeductions.VisibleIndex = 4;
            this.colTotalTimeDeductions.Width = 128;
            // 
            // colTotalTimeDeductionsRemarks
            // 
            this.colTotalTimeDeductionsRemarks.Caption = "Deduction Remarks";
            this.colTotalTimeDeductionsRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colTotalTimeDeductionsRemarks.FieldName = "TotalTimeDeductionsRemarks";
            this.colTotalTimeDeductionsRemarks.Name = "colTotalTimeDeductionsRemarks";
            this.colTotalTimeDeductionsRemarks.OptionsColumn.ReadOnly = true;
            this.colTotalTimeDeductionsRemarks.Visible = true;
            this.colTotalTimeDeductionsRemarks.VisibleIndex = 5;
            this.colTotalTimeDeductionsRemarks.Width = 119;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colTotalTimeTaken
            // 
            this.colTotalTimeTaken.Caption = "Total Time Taken";
            this.colTotalTimeTaken.FieldName = "TotalTimeTaken";
            this.colTotalTimeTaken.Name = "colTotalTimeTaken";
            this.colTotalTimeTaken.OptionsColumn.AllowEdit = false;
            this.colTotalTimeTaken.OptionsColumn.AllowFocus = false;
            this.colTotalTimeTaken.OptionsColumn.ReadOnly = true;
            this.colTotalTimeTaken.Visible = true;
            this.colTotalTimeTaken.VisibleIndex = 6;
            this.colTotalTimeTaken.Width = 106;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 8;
            this.colRemarks1.Width = 106;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Linked Actions";
            this.colLinkedActionCount.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 7;
            this.colLinkedActionCount.Width = 91;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridSplitContainer4);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1019, 177);
            this.xtraTabPage3.Text = "Linked Site Drawings";
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.gridControl6;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControl6);
            this.gridSplitContainer4.Size = new System.Drawing.Size(1019, 177);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp04003GCDrawingsLinkedToSiteBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit4});
            this.gridControl6.Size = new System.Drawing.Size(1019, 177);
            this.gridControl6.TabIndex = 0;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp04003GCDrawingsLinkedToSiteBindingSource
            // 
            this.sp04003GCDrawingsLinkedToSiteBindingSource.DataMember = "sp04003_GC_Drawings_Linked_To_Site";
            this.sp04003GCDrawingsLinkedToSiteBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDrawingID,
            this.colDrawingTypeID,
            this.colSiteID2,
            this.colDrawingFileName,
            this.colDrawingDescription,
            this.colDocumentPath1,
            this.colSiteCode1,
            this.colSiteName2,
            this.colFileTypeDescription,
            this.colAddedByStaffID1,
            this.colDateAdded1,
            this.colAddedByStaffName1,
            this.colRemarks2});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 1;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDrawingFileName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView6_CustomRowCellEdit);
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView6_ShowingEditor);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colDrawingID
            // 
            this.colDrawingID.Caption = "Drawing ID";
            this.colDrawingID.FieldName = "DrawingID";
            this.colDrawingID.Name = "colDrawingID";
            this.colDrawingID.OptionsColumn.AllowEdit = false;
            this.colDrawingID.OptionsColumn.AllowFocus = false;
            this.colDrawingID.OptionsColumn.ReadOnly = true;
            // 
            // colDrawingTypeID
            // 
            this.colDrawingTypeID.Caption = "Drawing Type ID";
            this.colDrawingTypeID.FieldName = "DrawingTypeID";
            this.colDrawingTypeID.Name = "colDrawingTypeID";
            this.colDrawingTypeID.OptionsColumn.AllowEdit = false;
            this.colDrawingTypeID.OptionsColumn.AllowFocus = false;
            this.colDrawingTypeID.OptionsColumn.ReadOnly = true;
            this.colDrawingTypeID.Width = 94;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            // 
            // colDrawingFileName
            // 
            this.colDrawingFileName.Caption = "File Name";
            this.colDrawingFileName.ColumnEdit = this.repositoryItemHyperLinkEdit4;
            this.colDrawingFileName.FieldName = "DrawingFileName";
            this.colDrawingFileName.Name = "colDrawingFileName";
            this.colDrawingFileName.OptionsColumn.ReadOnly = true;
            this.colDrawingFileName.Visible = true;
            this.colDrawingFileName.VisibleIndex = 1;
            this.colDrawingFileName.Width = 222;
            // 
            // repositoryItemHyperLinkEdit4
            // 
            this.repositoryItemHyperLinkEdit4.AutoHeight = false;
            this.repositoryItemHyperLinkEdit4.Name = "repositoryItemHyperLinkEdit4";
            this.repositoryItemHyperLinkEdit4.SingleClick = true;
            this.repositoryItemHyperLinkEdit4.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit4_OpenLink);
            // 
            // colDrawingDescription
            // 
            this.colDrawingDescription.Caption = "Description";
            this.colDrawingDescription.FieldName = "DrawingDescription";
            this.colDrawingDescription.Name = "colDrawingDescription";
            this.colDrawingDescription.OptionsColumn.AllowEdit = false;
            this.colDrawingDescription.OptionsColumn.AllowFocus = false;
            this.colDrawingDescription.OptionsColumn.ReadOnly = true;
            this.colDrawingDescription.Visible = true;
            this.colDrawingDescription.VisibleIndex = 0;
            this.colDrawingDescription.Width = 331;
            // 
            // colDocumentPath1
            // 
            this.colDocumentPath1.Caption = "Document Path";
            this.colDocumentPath1.FieldName = "DocumentPath";
            this.colDocumentPath1.Name = "colDocumentPath1";
            this.colDocumentPath1.OptionsColumn.AllowEdit = false;
            this.colDocumentPath1.OptionsColumn.AllowFocus = false;
            this.colDocumentPath1.OptionsColumn.ReadOnly = true;
            this.colDocumentPath1.Width = 132;
            // 
            // colSiteCode1
            // 
            this.colSiteCode1.Caption = "Site Code";
            this.colSiteCode1.FieldName = "SiteCode";
            this.colSiteCode1.Name = "colSiteCode1";
            this.colSiteCode1.OptionsColumn.AllowEdit = false;
            this.colSiteCode1.OptionsColumn.AllowFocus = false;
            this.colSiteCode1.OptionsColumn.ReadOnly = true;
            this.colSiteCode1.Width = 99;
            // 
            // colSiteName2
            // 
            this.colSiteName2.Caption = "Site Name";
            this.colSiteName2.FieldName = "SiteName";
            this.colSiteName2.Name = "colSiteName2";
            this.colSiteName2.OptionsColumn.AllowEdit = false;
            this.colSiteName2.OptionsColumn.AllowFocus = false;
            this.colSiteName2.OptionsColumn.ReadOnly = true;
            this.colSiteName2.Visible = true;
            this.colSiteName2.VisibleIndex = 2;
            this.colSiteName2.Width = 239;
            // 
            // colFileTypeDescription
            // 
            this.colFileTypeDescription.Caption = "File Type";
            this.colFileTypeDescription.FieldName = "FileTypeDescription";
            this.colFileTypeDescription.Name = "colFileTypeDescription";
            this.colFileTypeDescription.OptionsColumn.AllowEdit = false;
            this.colFileTypeDescription.OptionsColumn.AllowFocus = false;
            this.colFileTypeDescription.OptionsColumn.ReadOnly = true;
            this.colFileTypeDescription.Visible = true;
            this.colFileTypeDescription.VisibleIndex = 2;
            this.colFileTypeDescription.Width = 102;
            // 
            // colAddedByStaffID1
            // 
            this.colAddedByStaffID1.Caption = "Added By Staff ID";
            this.colAddedByStaffID1.FieldName = "AddedByStaffID";
            this.colAddedByStaffID1.Name = "colAddedByStaffID1";
            this.colAddedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID1.Width = 116;
            // 
            // colDateAdded1
            // 
            this.colDateAdded1.Caption = "Date Added";
            this.colDateAdded1.FieldName = "DateAdded";
            this.colDateAdded1.Name = "colDateAdded1";
            this.colDateAdded1.OptionsColumn.AllowEdit = false;
            this.colDateAdded1.OptionsColumn.AllowFocus = false;
            this.colDateAdded1.OptionsColumn.ReadOnly = true;
            this.colDateAdded1.Visible = true;
            this.colDateAdded1.VisibleIndex = 3;
            // 
            // colAddedByStaffName1
            // 
            this.colAddedByStaffName1.Caption = "Added By Staff Name";
            this.colAddedByStaffName1.FieldName = "AddedByStaffName";
            this.colAddedByStaffName1.Name = "colAddedByStaffName1";
            this.colAddedByStaffName1.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName1.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName1.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName1.Visible = true;
            this.colAddedByStaffName1.VisibleIndex = 4;
            this.colAddedByStaffName1.Width = 129;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridSplitContainer5);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1019, 177);
            this.xtraTabPage4.Text = "Linked Site Holidays";
            // 
            // gridSplitContainer5
            // 
            this.gridSplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer5.Grid = this.gridControl7;
            this.gridSplitContainer5.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer5.Name = "gridSplitContainer5";
            this.gridSplitContainer5.Panel1.Controls.Add(this.gridControl7);
            this.gridSplitContainer5.Size = new System.Drawing.Size(1019, 177);
            this.gridSplitContainer5.TabIndex = 0;
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp04004GCSiteHolidaysLinkedToSiteBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEdit1});
            this.gridControl7.Size = new System.Drawing.Size(1019, 177);
            this.gridControl7.TabIndex = 0;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp04004GCSiteHolidaysLinkedToSiteBindingSource
            // 
            this.sp04004GCSiteHolidaysLinkedToSiteBindingSource.DataMember = "sp04004_GC_Site_Holidays_Linked_To_Site";
            this.sp04004GCSiteHolidaysLinkedToSiteBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteHolidayID,
            this.colSiteID3,
            this.colHolidayDate,
            this.colSkipGritting,
            this.colSiteCode2,
            this.colSiteName3,
            this.colRemarks3,
            this.colHolidayType,
            this.colHolidayTypeID,
            this.colSkipSnowClearance});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHolidayDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView7.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.DoubleClick += new System.EventHandler(this.gridView7_DoubleClick);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colSiteHolidayID
            // 
            this.colSiteHolidayID.Caption = "Site Holiday ID";
            this.colSiteHolidayID.FieldName = "SiteHolidayID";
            this.colSiteHolidayID.Name = "colSiteHolidayID";
            this.colSiteHolidayID.OptionsColumn.AllowEdit = false;
            this.colSiteHolidayID.OptionsColumn.AllowFocus = false;
            this.colSiteHolidayID.OptionsColumn.ReadOnly = true;
            this.colSiteHolidayID.Width = 110;
            // 
            // colSiteID3
            // 
            this.colSiteID3.Caption = "Site ID";
            this.colSiteID3.FieldName = "SiteID";
            this.colSiteID3.Name = "colSiteID3";
            this.colSiteID3.OptionsColumn.AllowEdit = false;
            this.colSiteID3.OptionsColumn.AllowFocus = false;
            this.colSiteID3.OptionsColumn.ReadOnly = true;
            // 
            // colHolidayDate
            // 
            this.colHolidayDate.Caption = "Holiday Date";
            this.colHolidayDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colHolidayDate.FieldName = "HolidayDate";
            this.colHolidayDate.Name = "colHolidayDate";
            this.colHolidayDate.OptionsColumn.AllowEdit = false;
            this.colHolidayDate.OptionsColumn.AllowFocus = false;
            this.colHolidayDate.OptionsColumn.ReadOnly = true;
            this.colHolidayDate.Visible = true;
            this.colHolidayDate.VisibleIndex = 0;
            this.colHolidayDate.Width = 133;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "ddd dd MMM yyyy";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colSkipGritting
            // 
            this.colSkipGritting.Caption = "Skip Gritting";
            this.colSkipGritting.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSkipGritting.FieldName = "SkipGritting";
            this.colSkipGritting.Name = "colSkipGritting";
            this.colSkipGritting.OptionsColumn.AllowEdit = false;
            this.colSkipGritting.OptionsColumn.AllowFocus = false;
            this.colSkipGritting.OptionsColumn.ReadOnly = true;
            this.colSkipGritting.Visible = true;
            this.colSkipGritting.VisibleIndex = 2;
            this.colSkipGritting.Width = 87;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colSiteCode2
            // 
            this.colSiteCode2.Caption = "Site Code";
            this.colSiteCode2.FieldName = "SiteCode";
            this.colSiteCode2.Name = "colSiteCode2";
            this.colSiteCode2.OptionsColumn.AllowEdit = false;
            this.colSiteCode2.OptionsColumn.AllowFocus = false;
            this.colSiteCode2.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName3
            // 
            this.colSiteName3.Caption = "Site Name";
            this.colSiteName3.FieldName = "SiteName";
            this.colSiteName3.Name = "colSiteName3";
            this.colSiteName3.OptionsColumn.AllowEdit = false;
            this.colSiteName3.OptionsColumn.AllowFocus = false;
            this.colSiteName3.OptionsColumn.ReadOnly = true;
            this.colSiteName3.Width = 205;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 4;
            this.colRemarks3.Width = 304;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colHolidayType
            // 
            this.colHolidayType.Caption = "Holiday Type";
            this.colHolidayType.FieldName = "HolidayType";
            this.colHolidayType.Name = "colHolidayType";
            this.colHolidayType.OptionsColumn.AllowEdit = false;
            this.colHolidayType.OptionsColumn.AllowFocus = false;
            this.colHolidayType.OptionsColumn.ReadOnly = true;
            this.colHolidayType.Visible = true;
            this.colHolidayType.VisibleIndex = 1;
            this.colHolidayType.Width = 191;
            // 
            // colHolidayTypeID
            // 
            this.colHolidayTypeID.Caption = "Holiday Type ID";
            this.colHolidayTypeID.FieldName = "HolidayTypeID";
            this.colHolidayTypeID.Name = "colHolidayTypeID";
            this.colHolidayTypeID.OptionsColumn.AllowEdit = false;
            this.colHolidayTypeID.OptionsColumn.AllowFocus = false;
            this.colHolidayTypeID.OptionsColumn.ReadOnly = true;
            this.colHolidayTypeID.Width = 100;
            // 
            // colSkipSnowClearance
            // 
            this.colSkipSnowClearance.Caption = "Skip Snow Clearance";
            this.colSkipSnowClearance.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSkipSnowClearance.FieldName = "SkipSnowClearance";
            this.colSkipSnowClearance.Name = "colSkipSnowClearance";
            this.colSkipSnowClearance.OptionsColumn.AllowEdit = false;
            this.colSkipSnowClearance.OptionsColumn.AllowFocus = false;
            this.colSkipSnowClearance.OptionsColumn.ReadOnly = true;
            this.colSkipSnowClearance.Visible = true;
            this.colSkipSnowClearance.VisibleIndex = 3;
            this.colSkipSnowClearance.Width = 120;
            // 
            // popupContainerEditCompanies
            // 
            this.popupContainerEditCompanies.EditValue = "No Company Filter";
            this.popupContainerEditCompanies.Location = new System.Drawing.Point(95, 63);
            this.popupContainerEditCompanies.MenuManager = this.barManager1;
            this.popupContainerEditCompanies.Name = "popupContainerEditCompanies";
            this.popupContainerEditCompanies.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditCompanies.Properties.PopupControl = this.popupContainerControlCompanies;
            this.popupContainerEditCompanies.Properties.ShowPopupCloseButton = false;
            this.popupContainerEditCompanies.Size = new System.Drawing.Size(223, 20);
            this.popupContainerEditCompanies.StyleController = this.layoutControl3;
            this.popupContainerEditCompanies.TabIndex = 13;
            this.popupContainerEditCompanies.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditCompanies_QueryResultValue);
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.btnLoadSites);
            this.layoutControl3.Controls.Add(this.popupContainerEdit2);
            this.layoutControl3.Controls.Add(this.comboBoxEditLinkedRecordType);
            this.layoutControl3.Controls.Add(this.popupContainerEditCompanies);
            this.layoutControl3.Controls.Add(this.popupContainerEdit1);
            this.layoutControl3.Controls.Add(this.popupContainerEdit3);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1299, 242, 250, 350);
            this.layoutControl3.Root = this.layoutControlGroup4;
            this.layoutControl3.Size = new System.Drawing.Size(337, 586);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // btnLoadSites
            // 
            this.btnLoadSites.ImageOptions.ImageIndex = 9;
            this.btnLoadSites.ImageOptions.ImageList = this.imageCollection1;
            this.btnLoadSites.Location = new System.Drawing.Point(253, 235);
            this.btnLoadSites.Name = "btnLoadSites";
            this.btnLoadSites.Size = new System.Drawing.Size(77, 22);
            this.btnLoadSites.StyleController = this.layoutControl3;
            this.btnLoadSites.TabIndex = 5;
            this.btnLoadSites.Text = "Load Sites";
            this.btnLoadSites.Click += new System.EventHandler(this.btnLoadSites_Click);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.EditValue = "No Callout Status Filter";
            this.popupContainerEdit2.Location = new System.Drawing.Point(95, 121);
            this.popupContainerEdit2.MenuManager = this.barManager1;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            this.popupContainerEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit2.Properties.PopupControl = this.popupContainerControlLinkedGrittingCalloutsFilter;
            this.popupContainerEdit2.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit2.Size = new System.Drawing.Size(223, 20);
            this.popupContainerEdit2.StyleController = this.layoutControl3;
            this.popupContainerEdit2.TabIndex = 6;
            this.popupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit2_QueryResultValue);
            // 
            // comboBoxEditLinkedRecordType
            // 
            this.comboBoxEditLinkedRecordType.Location = new System.Drawing.Point(95, 97);
            this.comboBoxEditLinkedRecordType.MenuManager = this.barManager1;
            this.comboBoxEditLinkedRecordType.Name = "comboBoxEditLinkedRecordType";
            this.comboBoxEditLinkedRecordType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditLinkedRecordType.Properties.Items.AddRange(new object[] {
            "Assets",
            "Gritting Callouts",
            "Snow Clearance Callouts",
            "Trees"});
            this.comboBoxEditLinkedRecordType.Size = new System.Drawing.Size(223, 20);
            this.comboBoxEditLinkedRecordType.StyleController = this.layoutControl3;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Text = "Linked Record Type - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I control the values calculated and shown in the Linked Records hyperlink column " +
    "of the Site grid.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.comboBoxEditLinkedRecordType.SuperTip = superToolTip4;
            this.comboBoxEditLinkedRecordType.TabIndex = 8;
            this.comboBoxEditLinkedRecordType.SelectedValueChanged += new System.EventHandler(this.comboBoxEditLinkedRecordType_SelectedValueChanged);
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.EditValue = "No Client Filter";
            this.popupContainerEdit1.Location = new System.Drawing.Point(95, 39);
            this.popupContainerEdit1.MenuManager = this.barManager1;
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            this.popupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit1.Properties.PopupControl = this.popupContainerControlSites;
            this.popupContainerEdit1.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit1.Size = new System.Drawing.Size(223, 20);
            this.popupContainerEdit1.StyleController = this.layoutControl3;
            this.popupContainerEdit1.TabIndex = 4;
            this.popupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit1_QueryResultValue);
            // 
            // popupContainerEdit3
            // 
            this.popupContainerEdit3.EditValue = "No Related Data";
            this.popupContainerEdit3.Location = new System.Drawing.Point(95, 199);
            this.popupContainerEdit3.MenuManager = this.barManager1;
            this.popupContainerEdit3.Name = "popupContainerEdit3";
            this.popupContainerEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit3.Properties.PopupControl = this.popupContainerControlShowTabPages;
            this.popupContainerEdit3.Size = new System.Drawing.Size(223, 20);
            this.popupContainerEdit3.StyleController = this.layoutControl3;
            this.popupContainerEdit3.TabIndex = 12;
            this.popupContainerEdit3.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit3_QueryResultValue);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem2,
            this.emptySpaceItem4});
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup4.Size = new System.Drawing.Size(337, 586);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem8,
            this.emptySpaceItem3,
            this.layoutControlItem9,
            this.layoutControlItem7});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(327, 150);
            this.layoutControlGroup5.Text = "Site Filter:";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.popupContainerEdit1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem1.Text = "Client:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.popupContainerEditCompanies;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem8.Text = "Company:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(73, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(303, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.comboBoxEditLinkedRecordType;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem9.Text = "Drilldown Data:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.popupContainerEdit2;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 82);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem7.Text = "Status:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(73, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 160);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(327, 68);
            this.layoutControlGroup6.Text = "Linked Data:";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.popupContainerEdit3;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem6.Text = "Data To Show:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(73, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 254);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(327, 322);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 150);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(327, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnLoadSites;
            this.layoutControlItem2.Location = new System.Drawing.Point(246, 228);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(81, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(81, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(81, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 228);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(246, 26);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp03004_EP_Site_ManagerTableAdapter
            // 
            this.sp03004_EP_Site_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp03005_EP_Client_List_AllTableAdapter
            // 
            this.sp03005_EP_Client_List_AllTableAdapter.ClearBeforeFill = true;
            // 
            // sp03010_EP_Site_Manager_Linked_InspectionsTableAdapter
            // 
            this.sp03010_EP_Site_Manager_Linked_InspectionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04001_GC_Job_CallOut_StatusesTableAdapter
            // 
            this.sp04001_GC_Job_CallOut_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04003_GC_Drawings_Linked_To_SiteTableAdapter
            // 
            this.sp04003_GC_Drawings_Linked_To_SiteTableAdapter.ClearBeforeFill = true;
            // 
            // sp04004_GC_Site_Holidays_Linked_To_SiteTableAdapter
            // 
            this.sp04004_GC_Site_Holidays_Linked_To_SiteTableAdapter.ClearBeforeFill = true;
            // 
            // sp04022_Core_Dummy_TabPageListTableAdapter
            // 
            this.sp04022_Core_Dummy_TabPageListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04237_GC_Company_Filter_ListTableAdapter
            // 
            this.sp04237_GC_Company_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04332_GC_Service_Areas_Linked_To_SiteTableAdapter
            // 
            this.sp04332_GC_Service_Areas_Linked_To_SiteTableAdapter.ClearBeforeFill = true;
            // 
            // dockManager1
            // 
            this.dockManager1.AutoHideContainers.AddRange(new DevExpress.XtraBars.Docking.AutoHideContainer[] {
            this.hideContainerLeft});
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // hideContainerLeft
            // 
            this.hideContainerLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.hideContainerLeft.Controls.Add(this.dockPanelFilters);
            this.hideContainerLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.hideContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.hideContainerLeft.Name = "hideContainerLeft";
            this.hideContainerLeft.Size = new System.Drawing.Size(23, 618);
            // 
            // dockPanelFilters
            // 
            this.dockPanelFilters.Controls.Add(this.dockPanel1_Container);
            this.dockPanelFilters.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.ID = new System.Guid("00f7921e-25d5-4c7d-9a1f-e0fbd91752be");
            this.dockPanelFilters.Location = new System.Drawing.Point(-344, 0);
            this.dockPanelFilters.Name = "dockPanelFilters";
            this.dockPanelFilters.Options.ShowCloseButton = false;
            this.dockPanelFilters.OriginalSize = new System.Drawing.Size(344, 200);
            this.dockPanelFilters.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanelFilters.SavedIndex = 0;
            this.dockPanelFilters.Size = new System.Drawing.Size(344, 618);
            this.dockPanelFilters.Text = "Data Filter";
            this.dockPanelFilters.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.layoutControl3);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(337, 586);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // bar1
            // 
            this.bar1.BarName = "Filter";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshData, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterData),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSelectedCount, true)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // bbiRefreshData
            // 
            this.bbiRefreshData.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefreshData.Caption = "Refresh";
            this.bbiRefreshData.Id = 29;
            this.bbiRefreshData.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiRefreshData.Name = "bbiRefreshData";
            this.bbiRefreshData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshData_ItemClick);
            // 
            // bciFilterData
            // 
            this.bciFilterData.Caption = "Filter Selected";
            this.bciFilterData.Id = 30;
            this.bciFilterData.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Filter_32x32;
            this.bciFilterData.Name = "bciFilterData";
            this.bciFilterData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Filter Selected - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = resources.GetString("toolTipItem1.Text");
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bciFilterData.SuperTip = superToolTip1;
            this.bciFilterData.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterData_CheckedChanged);
            // 
            // bsiSelectedCount
            // 
            this.bsiSelectedCount.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSelectedCount.Caption = "0 Sites Selected";
            this.bsiSelectedCount.Id = 31;
            this.bsiSelectedCount.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_16x16;
            this.bsiSelectedCount.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.bsiSelectedCount.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.bsiSelectedCount.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.bsiSelectedCount.Name = "bsiSelectedCount";
            this.bsiSelectedCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bsiSelectedCount.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // frm_Core_Site_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1051, 618);
            this.Controls.Add(this.splitContainerControl2);
            this.Controls.Add(this.hideContainerLeft);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Site_Manager";
            this.Text = "Site Manager";
            this.Activated += new System.EventHandler(this.frm_Core_Site_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Site_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Site_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.hideContainerLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03004EPSiteManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMetres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditViewSiteKML)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditAppSiteImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCompanies)).EndInit();
            this.popupContainerControlCompanies.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04237GCCompanyFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSites)).EndInit();
            this.popupContainerControlSites.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03005EPClientListAllBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlShowTabPages)).EndInit();
            this.popupContainerControlShowTabPages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04022CoreDummyTabPageListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).EndInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04001GCJobCallOutStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageLinkedDocuments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04332GCServiceAreasLinkedToSiteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03010EPSiteManagerLinkedInspectionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04003GCDrawingsLinkedToSiteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).EndInit();
            this.gridSplitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04004GCSiteHolidaysLinkedToSiteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditCompanies.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditLinkedRecordType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.hideContainerLeft.ResumeLayout(false);
            this.dockPanelFilters.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit1;
        private DevExpress.XtraEditors.SimpleButton btnLoadSites;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSites;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SimpleButton btnClientFilterOK;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource sp03004EPSiteManagerBindingSource;
        private DataSet_EP dataSet_EP;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteFax;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate;
        private DevExpress.XtraGrid.Columns.GridColumn colClientRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedAssetCount;
        private WoodPlan5.DataSet_EPTableAdapters.sp03004_EP_Site_ManagerTableAdapter sp03004_EP_Site_ManagerTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEditRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colContactPersonPosition;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private System.Windows.Forms.BindingSource sp03005EPClientListAllBindingSource;
        private WoodPlan5.DataSet_EPTableAdapters.sp03005_EP_Client_List_AllTableAdapter sp03005_EP_Client_List_AllTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp03010EPSiteManagerLinkedInspectionsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectorID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspector;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTimeDeductions;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTimeDeductionsRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTimeTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private WoodPlan5.DataSet_EPTableAdapters.sp03010_EP_Site_Manager_Linked_InspectionsTableAdapter sp03010_EP_Site_Manager_Linked_InspectionsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedDocuments;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colMappingWorkspaceID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkspaceName;
        private DevExpress.XtraGrid.Columns.GridColumn colDailyGrittingReport;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlLinkedGrittingCalloutsFilter;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnGritCalloutFilterOK;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DataSet_GC_Core dataSet_GC_Core;
        private System.Windows.Forms.BindingSource sp04001GCJobCallOutStatusesBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04001_GC_Job_CallOut_StatusesTableAdapter sp04001_GC_Job_CallOut_StatusesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteOrder;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditLinkedRecordType;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private System.Windows.Forms.BindingSource sp04003GCDrawingsLinkedToSiteBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDrawingID;
        private DevExpress.XtraGrid.Columns.GridColumn colDrawingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDrawingFileName;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colDrawingDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName2;
        private DevExpress.XtraGrid.Columns.GridColumn colFileTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DataSet_GC_CoreTableAdapters.sp04003_GC_Drawings_Linked_To_SiteTableAdapter sp04003_GC_Drawings_Linked_To_SiteTableAdapter;
        private System.Windows.Forms.BindingSource sp04004GCSiteHolidaysLinkedToSiteBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteHolidayID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID3;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSkipGritting;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DataSet_GC_CoreTableAdapters.sp04004_GC_Site_Holidays_Linked_To_SiteTableAdapter sp04004_GC_Site_Holidays_Linked_To_SiteTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayType;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayTypeID;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlShowTabPages;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn colTabPageName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraEditors.SimpleButton btnOK2;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit3;
        private System.Windows.Forms.BindingSource sp04022CoreDummyTabPageListBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04022_Core_Dummy_TabPageListTableAdapter sp04022_Core_Dummy_TabPageListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colHubID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingSite;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientsSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSkipSnowClearance;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSnowClearanceSite;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCompanies;
        private DevExpress.XtraEditors.SimpleButton btnCompanyFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOrder;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private System.Windows.Forms.BindingSource sp04237GCCompanyFilterListBindingSource;
        private DataSet_GC_ReportsTableAdapters.sp04237_GC_Company_Filter_ListTableAdapter sp04237_GC_Company_Filter_ListTableAdapter;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditCompanies;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private System.Windows.Forms.BindingSource sp04332GCServiceAreasLinkedToSiteBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteServiceAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID4;
        private DevExpress.XtraGrid.Columns.GridColumn colDescriptionID;
        private DevExpress.XtraGrid.Columns.GridColumn colAppliesToGritting;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName4;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaDescription;
        private DataSet_GC_CoreTableAdapters.sp04332_GC_Service_Areas_Linked_To_SiteTableAdapter sp04332_GC_Service_Areas_Linked_To_SiteTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAppliesToSnowClearance;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationY;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colCOAddress;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelFilters;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshData;
        private DevExpress.XtraBars.Docking.AutoHideContainer hideContainerLeft;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraBars.BarCheckItem bciFilterData;
        private DevExpress.XtraBars.BarStaticItem bsiSelectedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colGeoFenceDistance;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMetres;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteKML;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditViewSiteKML;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteImageFile;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditAppSiteImage;
    }
}

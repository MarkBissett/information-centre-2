using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_Core_Staff_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        #endregion

        public frm_Core_Staff_Edit()
        {
            InitializeComponent();
        }

        private void frm_Core_Staff_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 301;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            //sp00186_Contractor_Type_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            //sp00186_Contractor_Type_List_With_BlankTableAdapter.Fill(woodPlanDataSet.sp00186_Contractor_Type_List_With_Blank);

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp00197_Staff_ItemTableAdapter.Connection.ConnectionString = strConnectionString;

            sp00202_Staff_User_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00202_Staff_User_TypesTableAdapter.Fill(woodPlanDataSet.sp00202_Staff_User_Types);

            // Following tableadapter populated in DataNavigator.PositionChanged event - calls LoadListOfAvailableFirstScreensLoaded event. //
            sp00047AvailableStartUpScreensForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Restrict list of fonts available //
            int intRows = DefaultFontNameFontEdit.Properties.Items.Count;
            string fontName = "";
            for (int i = intRows - 1; i >= 0; i--)
            {
                fontName = DefaultFontNameFontEdit.Properties.Items[i].ToString(); // repositoryItemFontEdit1.Properties.Items[i].ToString();
                if (fontName != "Tahoma" && fontName != "Comic Sans MS" && fontName != "Microsoft Sans Serif" && fontName != "Arial" && fontName != "Times New Roman")
                {
                    DefaultFontNameFontEdit.Properties.Items.RemoveAt(i);
                }
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Populate Main Dataset //
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp00197_Staff_Item.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strStaffName"] = "";
                        drNewRow["bActive"] = 0;
                        drNewRow["bLoginCreated"] = 0;
                        drNewRow["WoodPlanWebActionDoneDate"] = 0;
                        drNewRow["WoodPlanWebWorkOrderDoneDate"] = 0;
                        drNewRow["WoodPlanWebFileManagerAdmin"] = 0;
                        drNewRow["WoodPlanWebAdminUser"] = 0;
                        this.woodPlanDataSet.sp00197_Staff_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp00197_Staff_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["strStaffName"] = "";
                        drNewRow["bActive"] = 0;
                        drNewRow["bLoginCreated"] = 0;
                        drNewRow["WoodPlanWebActionDoneDate"] = 0;
                        drNewRow["WoodPlanWebWorkOrderDoneDate"] = 0;
                        drNewRow["WoodPlanWebFileManagerAdmin"] = 0;
                        drNewRow["WoodPlanWebAdminUser"] = 0;
                        this.woodPlanDataSet.sp00197_Staff_Item.Rows.Add(drNewRow);
                        this.woodPlanDataSet.sp00197_Staff_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp00197_Staff_ItemTableAdapter.Fill(this.woodPlanDataSet.sp00197_Staff_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.woodPlanDataSet.sp00197_Staff_Item.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Staff", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        strNetworkIDTextEdit.Focus();

                        strNetworkIDTextEdit.Properties.ReadOnly = false;
                        strForenameTextEdit.Properties.ReadOnly = false;
                        strSurnameTextEdit.Properties.ReadOnly = false;
                        strStaffNameTextEdit.Properties.ReadOnly = false;
                        strUserTypeGridLookUpEdit.Properties.ReadOnly = false;
                        strEmailTextEdit.Properties.ReadOnly = false;
                        SetLoginButtonText();

                        WebSiteUserNameTextEdit.Properties.ReadOnly = false;
                        WebSitePasswordTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        strEmailTextEdit.Focus();

                        strNetworkIDTextEdit.Properties.ReadOnly = false;
                        strForenameTextEdit.Properties.ReadOnly = false;
                        strSurnameTextEdit.Properties.ReadOnly = false;
                        strStaffNameTextEdit.Properties.ReadOnly = false;
                        strUserTypeGridLookUpEdit.Properties.ReadOnly = false;
                        strEmailTextEdit.Properties.ReadOnly = false;
                        SetLoginButtonText();

                        WebSiteUserNameTextEdit.Properties.ReadOnly = true;
                        WebSitePasswordTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        strEmailTextEdit.Focus();

                        strNetworkIDTextEdit.Properties.ReadOnly = false;
                        strForenameTextEdit.Properties.ReadOnly = false;
                        strSurnameTextEdit.Properties.ReadOnly = false;
                        strStaffNameTextEdit.Properties.ReadOnly = false;
                        strUserTypeGridLookUpEdit.Properties.ReadOnly = false;
                        strEmailTextEdit.Properties.ReadOnly = false;
                        SetLoginButtonText();

                        WebSiteUserNameTextEdit.Properties.ReadOnly = false;
                        WebSitePasswordTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        strEmailTextEdit.Focus();

                        strNetworkIDTextEdit.Properties.ReadOnly = true;
                        strForenameTextEdit.Properties.ReadOnly = true;
                        strSurnameTextEdit.Properties.ReadOnly = true;
                        strStaffNameTextEdit.Properties.ReadOnly = true;
                        strUserTypeGridLookUpEdit.Properties.ReadOnly = true;
                        strEmailTextEdit.Properties.ReadOnly = true;
                        SetLoginButtonText();

                        WebSiteUserNameTextEdit.Properties.ReadOnly = true;
                        WebSitePasswordTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.woodPlanDataSet.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_Core_Staff_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_Staff_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp00197StaffItemBindingSource.EndEdit();
            try
            {
                this.sp00197_Staff_ItemTableAdapter.Update(woodPlanDataSet);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp00197StaffItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["intStaffID"]) + ";";
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_Core_Staff_Manager")
                    {
                        var fParentForm = (frm_Core_Staff_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "", "");
                    }
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.woodPlanDataSet.sp00197_Staff_Item.Rows.Count; i++)
            {
                switch (this.woodPlanDataSet.sp00197_Staff_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    SetLoginButtonText();
                    LoadListOfAvailableFirstScreensLoaded();
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            //foreach (Control c in dxValidationProvider.GetInvalidControls())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    //strErrorMessages += "--> " + item.Text.ToString() + "  " + dxValidationProvider.GetValidationRule(c).ErrorText + "\n";
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        private void SetLoginButtonText()
        {
            DataRowView currentRow = (DataRowView)sp00197StaffItemBindingSource.Current;
            if (currentRow != null)
            {
                int intLoginCreated = Convert.ToInt32(currentRow["bLoginCreated"]);
                string strNetworkID = Convert.ToString(currentRow["strNetworkID"]);              
                btnCreateLogin.Text = (intLoginCreated == 0 ? "Create Login" : "Set Login Password");
                btnCreateLogin.Enabled = (string.IsNullOrEmpty(strNetworkID) ? false : true);
            }
        }

        private void LoadListOfAvailableFirstScreensLoaded()
        {
            DataRowView currentRow = (DataRowView)sp00197StaffItemBindingSource.Current;
            if (currentRow != null)
            {
                int? intStaffID = (string.IsNullOrEmpty(currentRow["intStaffID"].ToString()) ? 0 : Convert.ToInt32(currentRow["intStaffID"]));
                sp00047AvailableStartUpScreensForUserTableAdapter.Fill(this.woodPlanDataSet.sp00047AvailableStartUpScreensForUser, intStaffID);
            }
        }

        #endregion


        #region Editors

        private void DefaultFontNameFontEdit_Properties_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                (sender as BaseEdit).EditValue = null;
            }
        }

        private void DefaultFontSizeSpinEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                (sender as BaseEdit).EditValue = null;
            }
        }

        private void strNetworkIDTextEdit_Validated(object sender, EventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && !string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                btnCreateLogin.Enabled = true;
            }
            else
            {
                btnCreateLogin.Enabled = false;
            }
        }

        private void strNetworkIDTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(strNetworkIDTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(strNetworkIDTextEdit, "");
            }
        }

        private void strStaffNameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(strStaffNameTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(strStaffNameTextEdit, "");
            }
        }

        private void strEmailTextEdit_Validating(object sender, CancelEventArgs e)
        {
            dxErrorProvider1.SetError((Control)sender, "");
            TextEdit te = (TextEdit)sender;

            string strEmail = te.EditValue.ToString();

            // If block editing - prevent email data entry as it should be unique //
            if (this.strFormMode == "blockedit")
            {
                if (strEmail.Length != 0)
                {
                    dxErrorProvider1.SetError((Control)sender, "Work Email must be unique so cannot be entered when block editing");
                    e.Cancel = true;
                }
                return;
            }

            var currentRowView = (DataRowView)sp00197StaffItemBindingSource.Current;
            var currentRow = (WoodPlanDataSet.sp00197_Staff_ItemRow)currentRowView.Row;
            if (currentRow == null) return;
            int intStaffID = (string.IsNullOrEmpty(currentRow.intStaffID.ToString()) ? 0 : Convert.ToInt32(currentRow.intStaffID));
            int intCount = 0;
            try
            {
                DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter GetValue = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter();
                GetValue.ChangeConnectionString(strConnectionString);
                intCount = Convert.ToInt32(GetValue.sp01054_Core_Get_Email_Use_Count("Staff", strEmail, intStaffID));
            }
            catch (Exception) { }

            if (intCount > 0)
            {
                dxErrorProvider1.SetError((Control)sender, "This Email Address is already in use by another staff record.");
                e.Cancel = true;
                return;
            }
        }

        #endregion


        private void btnCreateLogin_Click(object sender, EventArgs e)
        {
            if (btnCreateLogin.Text == "Create Login")
            {    
                frm_Core_Staff_Create_DB_User_Login fCreateLogin = new frm_Core_Staff_Create_DB_User_Login();
                fCreateLogin.strNetworkID = strNetworkIDTextEdit.Text.ToString();
                fCreateLogin.GlobalSettings = this.GlobalSettings;
                if (fCreateLogin.ShowDialog() == DialogResult.OK)  // Login Created Successfully //
                {
                    DataRowView currentRow = (DataRowView)sp00197StaffItemBindingSource.Current;
                    if (currentRow != null)
                    {
                        currentRow["bLoginCreated"] = 1;
                    }
                    SetLoginButtonText();
                }
            }
            else  // Set Password //
            {
                frm_Core_Staff_Set_DB_User_Password fSetLoginPassword = new frm_Core_Staff_Set_DB_User_Password();
                fSetLoginPassword.strNetworkID = strNetworkIDTextEdit.Text.ToString();
                fSetLoginPassword.GlobalSettings = this.GlobalSettings;
                fSetLoginPassword.ShowDialog();
            }
        }

     }
}


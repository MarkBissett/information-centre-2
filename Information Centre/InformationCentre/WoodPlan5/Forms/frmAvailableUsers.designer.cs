namespace WoodPlan5
{
    partial class frmAvailableUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAvailableUsers));
            this.gcAvailableUsers = new DevExpress.XtraGrid.GridControl();
            this.sp00062AvailableUsersListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gvAvailableUsers = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLoginCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetworkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp00062AvailableUsersListTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00062AvailableUsersListTableAdapter();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sp00001PopulateSwitchboardBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00001PopulateSwitchboardTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00001PopulateSwitchboardTableAdapter();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAvailableUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00062AvailableUsersListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAvailableUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00001PopulateSwitchboardBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(453, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(453, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(453, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gcAvailableUsers
            // 
            this.gcAvailableUsers.DataSource = this.sp00062AvailableUsersListBindingSource;
            this.gcAvailableUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcAvailableUsers.Location = new System.Drawing.Point(0, 0);
            this.gcAvailableUsers.MainView = this.gvAvailableUsers;
            this.gcAvailableUsers.MenuManager = this.barManager1;
            this.gcAvailableUsers.Name = "gcAvailableUsers";
            this.gcAvailableUsers.Size = new System.Drawing.Size(453, 495);
            this.gcAvailableUsers.TabIndex = 4;
            this.gcAvailableUsers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAvailableUsers});
            // 
            // sp00062AvailableUsersListBindingSource
            // 
            this.sp00062AvailableUsersListBindingSource.DataMember = "sp00062AvailableUsersList";
            this.sp00062AvailableUsersListBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gvAvailableUsers
            // 
            this.gvAvailableUsers.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStaffID,
            this.colSurname,
            this.colForename,
            this.colLoginCreated,
            this.colNetworkID,
            this.colUserType});
            this.gvAvailableUsers.GridControl = this.gcAvailableUsers;
            this.gvAvailableUsers.Name = "gvAvailableUsers";
            this.gvAvailableUsers.OptionsBehavior.Editable = false;
            this.gvAvailableUsers.OptionsLayout.Columns.StoreAllOptions = true;
            this.gvAvailableUsers.OptionsLayout.StoreAppearance = true;
            this.gvAvailableUsers.OptionsLayout.StoreFormatRules = true;
            this.gvAvailableUsers.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gvAvailableUsers.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gvAvailableUsers.OptionsView.AllowHtmlDrawHeaders = true;
            this.gvAvailableUsers.OptionsView.EnableAppearanceEvenRow = true;
            this.gvAvailableUsers.OptionsView.ShowGroupPanel = false;
            this.gvAvailableUsers.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colForename, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gvAvailableUsers.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gvAvailableUsers_RowStyle);
            this.gvAvailableUsers.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gvAvailableUsers_CustomDrawEmptyForeground);
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "StaffID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.OptionsColumn.TabStop = false;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.OptionsColumn.TabStop = false;
            this.colSurname.Visible = true;
            this.colSurname.VisibleIndex = 0;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.OptionsColumn.TabStop = false;
            this.colForename.Visible = true;
            this.colForename.VisibleIndex = 1;
            // 
            // colLoginCreated
            // 
            this.colLoginCreated.Caption = "LoginCreated";
            this.colLoginCreated.FieldName = "LoginCreated";
            this.colLoginCreated.Name = "colLoginCreated";
            this.colLoginCreated.OptionsColumn.AllowEdit = false;
            this.colLoginCreated.OptionsColumn.AllowFocus = false;
            this.colLoginCreated.OptionsColumn.ReadOnly = true;
            this.colLoginCreated.OptionsColumn.TabStop = false;
            // 
            // colNetworkID
            // 
            this.colNetworkID.Caption = "NetworkID";
            this.colNetworkID.FieldName = "NetworkID";
            this.colNetworkID.Name = "colNetworkID";
            this.colNetworkID.OptionsColumn.AllowEdit = false;
            this.colNetworkID.OptionsColumn.AllowFocus = false;
            this.colNetworkID.OptionsColumn.ReadOnly = true;
            this.colNetworkID.OptionsColumn.TabStop = false;
            // 
            // colUserType
            // 
            this.colUserType.Caption = "UserType";
            this.colUserType.FieldName = "UserType";
            this.colUserType.Name = "colUserType";
            this.colUserType.OptionsColumn.AllowEdit = false;
            this.colUserType.OptionsColumn.AllowFocus = false;
            this.colUserType.OptionsColumn.ReadOnly = true;
            this.colUserType.OptionsColumn.TabStop = false;
            // 
            // sp00062AvailableUsersListTableAdapter
            // 
            this.sp00062AvailableUsersListTableAdapter.ClearBeforeFill = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(239, 505);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOK.Location = new System.Drawing.Point(138, 505);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // sp00001PopulateSwitchboardBindingSource
            // 
            this.sp00001PopulateSwitchboardBindingSource.DataMember = "sp00001PopulateSwitchboard";
            this.sp00001PopulateSwitchboardBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // sp00001PopulateSwitchboardTableAdapter
            // 
            this.sp00001PopulateSwitchboardTableAdapter.ClearBeforeFill = true;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gcAvailableUsers;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 1);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gcAvailableUsers);
            this.gridSplitContainer1.Size = new System.Drawing.Size(453, 495);
            this.gridSplitContainer1.TabIndex = 4;
            // 
            // frmAvailableUsers
            // 
            this.AcceptButton = this.btnOK;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(453, 537);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frmAvailableUsers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Change Logged In User";
            this.Load += new System.EventHandler(this.frmAvailableUsers_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAvailableUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00062AvailableUsersListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAvailableUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00001PopulateSwitchboardBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcAvailableUsers;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAvailableUsers;
        private WoodPlanDataSet woodPlanDataSet;
        private System.Windows.Forms.BindingSource sp00062AvailableUsersListBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00062AvailableUsersListTableAdapter sp00062AvailableUsersListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colLoginCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colNetworkID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserType;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private System.Windows.Forms.BindingSource sp00001PopulateSwitchboardBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00001PopulateSwitchboardTableAdapter sp00001PopulateSwitchboardTableAdapter;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
    }
}

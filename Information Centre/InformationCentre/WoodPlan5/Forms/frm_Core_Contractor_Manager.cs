using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Base.ViewInfo;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Drawing;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Contractor_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        bool iBool_AllowVisibleTraining = false;
        bool iBool_AllowAddTraining = false;
        bool iBool_AllowEditTraining = false;
        bool iBool_AllowDeleteTraining = false;

        bool iBool_AllowDeleteLinkedDocument = false;
        bool iBool_AllowAddLinkedDocument = false;
        bool iBool_AllowEditLinkedDocument = false;
        bool iBool_LinkedDocument = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;
        public RefreshGridState RefreshGridViewState2;
        public RefreshGridState RefreshGridViewState3;
        public RefreshGridState RefreshGridViewState4;
        public RefreshGridState RefreshGridViewState5;
        public RefreshGridState RefreshGridViewState6;
        public RefreshGridState RefreshGridViewState7;
        public RefreshGridState RefreshGridViewState8;
        public RefreshGridState RefreshGridViewState11;  // Team Holidays //
        public RefreshGridState RefreshGridViewState12;  // Snow Clearance Rate Bands //
        public RefreshGridState RefreshGridViewState13;  // Allocated PDAs Linked Team Members //
        public RefreshGridState RefreshGridViewState14;  // Visit Types //
        public RefreshGridState RefreshGridViewState15;  // Team Qualifications //
        public RefreshGridState RefreshGridViewState16;  // Team Member Qualifications //
        public RefreshGridState RefreshGridViewState17;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState25;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateResponsibility;
        public RefreshGridState RefreshGridViewInsuranceCategory;

        int i_int_FocusedGrid = 1;
        string i_str_AddedRecordIDs1 = "";
        string i_str_AddedRecordIDs2 = "";
        string i_str_AddedRecordIDs3 = "";
        string i_str_AddedRecordIDs4 = "";
        string i_str_AddedRecordIDs5 = "";
        string i_str_AddedRecordIDs6 = "";
        string i_str_AddedRecordIDs7 = "";
        string i_str_AddedRecordIDs11 = "";  // Team Holidays //
        string i_str_AddedRecordIDs12 = "";  // Snow Clearance Rate Bands //
        string i_str_AddedRecordIDs13 = "";  // Allocated PDAs Linked Team Members //
        string i_str_AddedRecordIDs14 = "";  // Visit Types //
        string i_str_AddedRecordIDs15 = "";  // Team Qualifications//
        string i_str_AddedRecordIDs16 = "";  // Team Member Qualifications//
        string i_str_AddedRecordIDs17 = "";  // Team Member Qualifications//
        string i_str_AddedRecordIDs25 = "";  // Team Member Qualifications//
        string i_str_AddedRecordIDsResponsibility = "";
        string i_str_AddedRecordIDsInsuranceCategory = ""; //Team Insurance Categories//

        public string strCaller = "";

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        private string i_str_selected_CallOutType_ids = "";
        private string i_str_selected_CallOutType_names = "";
        private string i_str_selected_TabPages = "";

        public string strLinkedRecordType = "Gritting Callouts";
        int intLinkedRecordType = 0;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        private string strCertificatePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        public string i_str_PDFDirectoryName = "";
        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table for Linked Documents //
        public string strPassedInDrillDownIDs = "";

        #endregion

        public frm_Core_Contractor_Manager()
        {
            InitializeComponent();
        }

        private void frm_Core_Contractor_Manager_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 4004;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            // Just in case we didn't get the permission for the screen using Pick List Manager - Contractor Manager permisison, we can try using Core - Team Manager //
            if (dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count <= 0)
            {
                sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, 4004, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
                ProcessPermissionsForForm();
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            #region populate_Available_TabPages_List

            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            gridControl4.BeginUpdate();
            this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.Rows.Clear();
            foreach (XtraTabPage i in xtraTabControl1.TabPages)
            {
                if (i.Text != "Training" || (i.Text == "Training" && iBool_AllowVisibleTraining))
                {
                    DataRow drNewRow;
                    drNewRow = this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.NewRow();
                    drNewRow["TabPageName"] = i.Text;
                    this.dataSet_GC_Core.sp04022_Core_Dummy_TabPageList.Rows.Add(drNewRow);
                }
            }
            Set_Training_Visibility();
            gridControl4.EndUpdate();
            gridControl4.ForceInitialize();

            #endregion

            // Add record selection checkboxes to popup Callout Type grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            sp00249_Core_Record_Statuses_For_FilteringTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp00249_Core_Record_Statuses_For_FilteringTableAdapter.Fill(woodPlanDataSet.sp00249_Core_Record_Statuses_For_Filtering, intLinkedRecordType);
            }
            catch (Exception) { }      
            gridControl5.ForceInitialize();
            dateEditFromDate.DateTime = DateTime.Today.AddMonths(-6);
            dateEditToDate.DateTime = DateTime.Today.AddDays(1).AddMilliseconds(-5);  // Should give date + 23:59:59.995 //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strCertificatePath = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GroundControlContractorCertificatesPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Contractor Certificate Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get DefaultContractor Certificate Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            sp00180_Contractor_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "ContractorID");

            sp00181_Contractor_Manager_ContactsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "ContactID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00182_Contractor_Manager_CertificatesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "CertificateID");

            sp04023_GC_SubContractor_Team_MembersTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView6, "TeamMemberID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04027_GC_SubContractor_Allocated_PDAsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState5 = new RefreshGridState(gridView7, "AllocatedPdaID");
 
            sp04032_GC_Team_Gritting_InfoTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState6 = new RefreshGridState(gridView8, "SubContractorGritInformationID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04082_GC_SubContractor_Sent_Text_MessagesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState7 = new RefreshGridState(gridView9, "SentTextMessageID");

            sp04131_GC_Team_Self_Billing_InvoicesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState8 = new RefreshGridState(gridView10, "GrittingInvoiceID");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04152_GC_Holidays_Linked_To_TeamTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState11 = new RefreshGridState(gridView11, "SubContractorHolidayID");

            sp04156_GC_Snow_Clearance_Team_RatesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState12 = new RefreshGridState(gridView12, "SnowClearanceSubContractorRateRule");
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState13 = new RefreshGridState(gridView13, "AllocatedPdaLinkedTeamMemberID");

            sp06309_Person_Visit_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState14 = new RefreshGridState(gridView14, "VisitTypePersonID");

            sp09119_HR_Qualifications_For_ContractorTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState15 = new RefreshGridState(gridViewTraining, "QualificationID");

            sp09120_HR_Qualifications_For_Team_MemberTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState16 = new RefreshGridState(gridViewTrainingTeam, "QualificationID");

            sp00245_Core_Team_InsuranceTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState17 = new RefreshGridState(gridView17, "InsuranceID");

            sp00250_Core_Team_Insurance_Category_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewInsuranceCategory = new RefreshGridState(gridViewInsuranceCategory, "TeamInsuranceCategoryID");
            
            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState25 = new RefreshGridState(gridView25, "LinkedDocumentID");

            sp01020_Core_Team_Manager_Linked_Responsible_PeopleTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateResponsibility = new RefreshGridState(gridViewResponsibility, "PersonResponsibilityID");

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_PDFDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingSelfBillingInvoicePDFPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Saved PDF Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Saved PDF Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_PDFDirectoryName.EndsWith("\\")) i_str_PDFDirectoryName += "\\";  // Add Backslash to end //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEdit2.Text = "Custom Filter";
                Load_Data();  // Load records //
            }
            else
            {
                Load_Data();
            }

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar // 
            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                LoadLastSavedUserScreenSettings();
            }

            gridControl25.Enabled = iBool_LinkedDocument;
            xtraTabPageLinkedDocuments.PageVisible = iBool_LinkedDocument;

            SetMenuStatus();
        }

        private void frm_Core_Contractor_Manager_Activated(object sender, EventArgs e)
        {
            string strSelectedIDs = "";
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDs2) || !string.IsNullOrEmpty(i_str_AddedRecordIDs3) || !string.IsNullOrEmpty(i_str_AddedRecordIDs4) || !string.IsNullOrEmpty(i_str_AddedRecordIDs5) || !string.IsNullOrEmpty(i_str_AddedRecordIDs6) || !string.IsNullOrEmpty(i_str_AddedRecordIDs11) || !string.IsNullOrEmpty(i_str_AddedRecordIDs12) || !string.IsNullOrEmpty(i_str_AddedRecordIDs14) || !string.IsNullOrEmpty(i_str_AddedRecordIDs15) || !string.IsNullOrEmpty(i_str_AddedRecordIDs17) || !string.IsNullOrEmpty(i_str_AddedRecordIDsInsuranceCategory) || !string.IsNullOrEmpty(i_str_AddedRecordIDs25))
                {
                    if (string.IsNullOrWhiteSpace(strSelectedIDs)) strSelectedIDs = GetSelectedParentIDs();
                    LoadLinkedRecords(strSelectedIDs);
                }
                if (UpdateRefreshStatus == 3|| !string.IsNullOrEmpty(i_str_AddedRecordIDs16))
                {
                   Load_Data_Team_Member_Training();
                }
                if (UpdateRefreshStatus == 4 || !string.IsNullOrEmpty(i_str_AddedRecordIDsResponsibility))
                {
                    if (string.IsNullOrWhiteSpace(strSelectedIDs)) strSelectedIDs = GetSelectedParentIDs();
                    LoadLinkedRecords_Person_Responsibilities(strSelectedIDs);
                }
            }
            SetMenuStatus();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAdd = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEdit = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDelete = true;
                            }
                        }
                        break;
                    case 1:    // Training //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddTraining = true;
                                iBool_AllowVisibleTraining = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditTraining = true;
                                iBool_AllowVisibleTraining = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteTraining = true;
                                iBool_AllowVisibleTraining = true;
                            }
                        }
                        break;
                    case 2:    // Linked Documents //
                        {
                            if (sfpPermissions.blCreate)
                            {
                                iBool_AllowAddLinkedDocument = true;
                            }
                            if (sfpPermissions.blUpdate)
                            {
                                iBool_AllowEditLinkedDocument = true;
                            }
                            if (sfpPermissions.blDelete)
                            {
                                iBool_AllowDeleteLinkedDocument = true;
                            }
                            iBool_LinkedDocument = (sfpPermissions.blRead || iBool_AllowAddLinkedDocument || iBool_AllowEditLinkedDocument || iBool_AllowDeleteLinkedDocument);
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;

            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        view = (GridView)gridControl1.MainView;
                    }
                    break;
                case 2:
                    {
                        view = (GridView)gridControl2.MainView;
                    }
                    break;
                case 3:
                    {
                        view = (GridView)gridControl3.MainView;
                    }
                    break;
                case 4:
                    {
                        view = (GridView)gridControl6.MainView;
                    }
                    break;
                case 5:
                    {
                        view = (GridView)gridControl7.MainView;
                    }
                    break;
                case 6:
                    {
                        view = (GridView)gridControl8.MainView;
                    }
                    break;
                case 7:
                    {
                        view = (GridView)gridControl9.MainView;
                    }
                    break;
                case 8:
                    {
                        view = (GridView)gridControl10.MainView;
                    }
                    break;
                case 11:
                    {
                        view = (GridView)gridControl11.MainView;
                    }
                    break;
                case 12:
                    {
                        view = (GridView)gridControl12.MainView;
                    }
                    break;
                case 13:
                    {
                        view = (GridView)gridControl13.MainView;
                    }
                    break;
                case 14:
                    {
                        view = (GridView)gridControl14.MainView;
                    }
                    break;
                case 15:
                    {
                        view = (GridView)gridControlTraining.MainView;
                    }
                    break;
                case 16:
                    {
                        view = (GridView)gridControlTrainingTeam.MainView;
                    }
                    break;
                case 17:
                    {
                        view = (GridView)gridControl17.MainView;
                    }
                    break;
                case 18:
                    {
                        view = (GridView)gridControlInsuranceCategory.MainView;
                    }
                    break;
                case 25:
                    {
                        view = (GridView)gridControl25.MainView;
                    }
                    break;
                case 30:
                    {
                        view = (GridView)gridControlResponsibility.MainView;
                    }
                    break;
                default:
                    {
                        view = (GridView)gridControl1.MainView;
                    }
                  break;
            }

            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            if (iBool_AllowAdd && !(i_int_FocusedGrid == 6 || i_int_FocusedGrid == 7 || i_int_FocusedGrid == 8 || i_int_FocusedGrid == 25))
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
            }
            else if (iBool_AllowAdd && (i_int_FocusedGrid == 15 || i_int_FocusedGrid == 16) && iBool_AllowAddTraining)
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
            }
            else if (iBool_AllowAddLinkedDocument && i_int_FocusedGrid == 25)
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
            }         

            GridView viewParent = (GridView)gridControl1.MainView;
            int[] intRowHandlesParent;
            intRowHandlesParent = viewParent.GetSelectedRows();
            if (iBool_AllowAdd && intRowHandlesParent.Length >= 2 && (i_int_FocusedGrid == 11 || i_int_FocusedGrid == 12 || i_int_FocusedGrid == 30) || i_int_FocusedGrid == 14 || i_int_FocusedGrid == 17 || i_int_FocusedGrid == 18)
            {
                alItems.Add("iBlockAdd");
                bbiBlockAdd.Enabled = true;
            }
            else if (iBool_AllowAdd && intRowHandlesParent.Length >= 2 && i_int_FocusedGrid == 15 && iBool_AllowAddTraining)
            {
                alItems.Add("iBlockAdd");
                bbiBlockAdd.Enabled = true;
            }
            else if (iBool_AllowAddLinkedDocument && intRowHandlesParent.Length >= 2 && i_int_FocusedGrid == 25)
            {
                alItems.Add("iBlockAdd");
                bbiBlockAdd.Enabled = true;
            }
            else if (iBool_AllowAdd && i_int_FocusedGrid == 16 && iBool_AllowAddTraining)
            {
                viewParent = (GridView)gridControlTraining.MainView;
                intRowHandlesParent = viewParent.GetSelectedRows();
                if (intRowHandlesParent.Length >= 2)
                {
                    alItems.Add("iBlockAdd");
                    bbiBlockAdd.Enabled = true;
                }
            }

            if (iBool_AllowEdit && intRowHandles.Length >= 1 && !(i_int_FocusedGrid == 7 || i_int_FocusedGrid == 8 || i_int_FocusedGrid == 15 || i_int_FocusedGrid == 16))
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
                if (intRowHandles.Length >= 2)
                {
                    alItems.Add("iBlockEdit");
                    bbiBlockEdit.Enabled = true;
                }
            }
            else if (iBool_AllowEdit && intRowHandles.Length >= 1 && (i_int_FocusedGrid == 15 || i_int_FocusedGrid == 16) && iBool_AllowAddTraining)
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
                if (intRowHandles.Length >= 2)
                {
                    alItems.Add("iBlockEdit");
                    bbiBlockEdit.Enabled = true;
                }
            }
            else if (iBool_AllowEditLinkedDocument && intRowHandles.Length >= 1 && i_int_FocusedGrid == 25)
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
                if (intRowHandles.Length >= 2)
                {
                    alItems.Add("iBlockEdit");
                    bbiBlockEdit.Enabled = true;
                }
            }
            
            if (iBool_AllowDelete && intRowHandles.Length >= 1 && !(i_int_FocusedGrid == 6 || i_int_FocusedGrid == 15 || i_int_FocusedGrid == 16))
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
            }
            else if (iBool_AllowDelete && intRowHandles.Length >= 1 && (i_int_FocusedGrid == 15 || i_int_FocusedGrid == 16) && iBool_AllowAddTraining)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
            }
            else if (iBool_AllowDeleteLinkedDocument && intRowHandles.Length >= 1 && i_int_FocusedGrid == 25)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
            }

            if (i_int_FocusedGrid == 6)
            {
                bbiRecalculateStock.Enabled = (view.SelectedRowsCount > 0 ? true : false);
            }
            else
            {
                bbiRecalculateStock.Enabled = false;
            }

            // Team Members linked to Allocated PDAs //
            if (iBool_AllowDelete && i_int_FocusedGrid == 13 && intRowHandles.Length >= 1)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
                viewParent = (GridView)gridControl7.MainView;
                intRowHandlesParent = viewParent.GetSelectedRows();
                if (iBool_AllowAdd && intRowHandlesParent.Length == 1)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length > 0);
          
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);

            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);

            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);

            view = (GridView)gridControl7.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl13.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length == 1);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = true;

            view = (GridView)gridControl8.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);

            view = (GridView)gridControl9.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl9.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);

            view = (GridView)gridControl10.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);

            view = (GridView)gridControl11.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl11.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl11.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl11.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);

            view = (GridView)gridControl11.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);

            view = (GridView)gridControl13.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl13.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl13.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;

            view = (GridView)gridControl14.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl14.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

            view = (GridView)gridControlTraining.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAddTraining;
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEditTraining && intRowHandles.Length > 0);
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDeleteTraining && intRowHandles.Length > 0);
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowVisibleTraining && intRowHandles.Length > 0);
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = iBool_AllowVisibleTraining;

            view = (GridView)gridControlTrainingTeam.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlTrainingTeam.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAddTraining;
            gridControlTrainingTeam.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEditTraining && intRowHandles.Length > 0);
            gridControlTrainingTeam.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDeleteTraining && intRowHandles.Length > 0);
            gridControlTrainingTeam.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowVisibleTraining && intRowHandles.Length > 0);
            gridControlTrainingTeam.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = iBool_AllowVisibleTraining;

            view = (GridView)gridControl17.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl17.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl17.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl17.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl17.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

            view = (GridView)gridControlInsuranceCategory.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlInsuranceCategory.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlInsuranceCategory.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlInsuranceCategory.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlInsuranceCategory.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

            view = (GridView)gridControl25.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl25.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAddLinkedDocument;
            gridControl25.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEditLinkedDocument && intRowHandles.Length > 0);
            gridControl25.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDeleteLinkedDocument && intRowHandles.Length > 0);

            view = (GridView)gridControlResponsibility.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
        }

        private void frm_Core_Contractor_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShownTabPages", i_str_selected_TabPages);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDate", dateEditFromDate.DateTime.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDate", dateEditToDate.DateTime.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "LinkedRecordType", strLinkedRecordType);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CallOutFilter", i_str_selected_CallOutType_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // From Date //
                string strFromDate = default_screen_settings.RetrieveSetting("FromDate");
                if (!string.IsNullOrEmpty(strFromDate)) dateEditFromDate.DateTime = Convert.ToDateTime(strFromDate);

                // To Date //
                string strToDate = default_screen_settings.RetrieveSetting("ToDate");
                if (!string.IsNullOrEmpty(strToDate)) dateEditToDate.DateTime = Convert.ToDateTime(strToDate);

                // Linked Record Type //
                strLinkedRecordType = default_screen_settings.RetrieveSetting("LinkedRecordType");
                if (string.IsNullOrEmpty(strLinkedRecordType)) strLinkedRecordType = "Gritting Callouts";
                comboBoxEditLinkedRecordType.Text = strLinkedRecordType;


                // Shown Tab Pages //
                int intFoundRow = 0;
                string strItemFilter = default_screen_settings.RetrieveSetting("ShownTabPages");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl4.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["TabPageName"], strElement.Trim());
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit1.Text = PopupContainerEdit1_Get_Selected();
                }

                // Gritting Callout Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("CallOutFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["Value"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit2.Text = PopupContainerEdit2_Get_Selected();
                }

                LoadContractorsBtn.PerformClick();
            }
        }

        private void LoadContractorsBtn_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;
            gridControl1.BeginUpdate();
            if (popupContainerEdit2.EditValue.ToString() == "Custom Filter" && strPassedInDrillDownIDs != "")  // Load passed in Callouts //
            {
                sp00180_Contractor_ManagerTableAdapter.Fill(woodPlanDataSet.sp00180_Contractor_Manager, strPassedInDrillDownIDs, intLinkedRecordType, dtFromDate, dtToDate, i_str_selected_CallOutType_ids);
            }
            else
            {
                sp00180_Contractor_ManagerTableAdapter.Fill(woodPlanDataSet.sp00180_Contractor_Manager, "", intLinkedRecordType, dtFromDate, dtToDate, i_str_selected_CallOutType_ids);
            }
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ContractorID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        private string GetSelectedParentIDs()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellValue(intRowHandle, view.Columns["ContractorID"]).ToString() + ",");
            }
            return sb.ToString();
        }

        private void LoadLinkedRecords(string strSelectedIDs)
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            int intCount = strSelectedIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length;
            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            GridView view = null;

            // Populate Linked Contacts //
            if (xtraTabPage1.PageVisible)
            {

                gridControl2.MainView.BeginUpdate();
                this.RefreshGridViewState2.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.woodPlanDataSet.sp00181_Contractor_Manager_Contacts.Clear();
                }
                else
                {
                    sp00181_Contractor_Manager_ContactsTableAdapter.Fill(woodPlanDataSet.sp00181_Contractor_Manager_Contacts, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl2.MainView.EndUpdate();

                strArray = null;
                intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs2 != "")
                {
                    strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl2.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["ContactID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs2 = "";
                }
            }

            // Populate Linked Certificates //
            if (xtraTabPage2.PageVisible)
            {
                gridControl3.MainView.BeginUpdate();
                this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.woodPlanDataSet.sp00182_Contractor_Manager_Certificates.Clear();
                }
                else
                {
                    sp00182_Contractor_Manager_CertificatesTableAdapter.Fill(woodPlanDataSet.sp00182_Contractor_Manager_Certificates, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), strCertificatePath);
                    this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl3.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs3 != "")
                {
                    strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl3.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["CertificateID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs3 = "";
                }
            }

            // Populate Linked Team Members //
            if (xtraTabPage3.PageVisible)
            {
                gridControl6.MainView.BeginUpdate();
                this.RefreshGridViewState4.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_GC_Core.sp04023_GC_SubContractor_Team_Members.Clear();
                }
                else
                {
                    sp04023_GC_SubContractor_Team_MembersTableAdapter.Fill(dataSet_GC_Core.sp04023_GC_SubContractor_Team_Members, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl6.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs4 != "")
                {
                    strArray = i_str_AddedRecordIDs4.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl6.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["TeamMemberID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs4 = "";
                }
            }

            // Populate Allocated PDAs //
            if (xtraTabPage4.PageVisible)
            {
                gridControl7.MainView.BeginUpdate();
                this.RefreshGridViewState5.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_GC_Core.sp04027_GC_SubContractor_Allocated_PDAs.Clear();
                }
                else
                {
                    sp04027_GC_SubContractor_Allocated_PDAsTableAdapter.Fill(dataSet_GC_Core.sp04027_GC_SubContractor_Allocated_PDAs, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 1);
                    this.RefreshGridViewState5.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl7.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs5 != "")
                {
                    strArray = i_str_AddedRecordIDs5.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl7.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["AllocatedPdaID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs5 = "";
                }
            }

            // Populate Gritting Information //
            if (xtraTabPage5.PageVisible)
            {
                gridControl8.MainView.BeginUpdate();
                this.RefreshGridViewState6.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_GC_Core.sp04032_GC_Team_Gritting_Info.Clear();
                }
                else
                {
                    sp04032_GC_Team_Gritting_InfoTableAdapter.Fill(dataSet_GC_Core.sp04032_GC_Team_Gritting_Info, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState6.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl8.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs6 != "")
                {
                    strArray = i_str_AddedRecordIDs6.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl8.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SubContractorGritInformationID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs6 = "";
                }
            }

            // Populate Sent Text Messages Information //
            if (xtraTabPage7.PageVisible)
            {
                gridControl9.MainView.BeginUpdate();
                this.RefreshGridViewState7.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_GC_Core.sp04082_GC_SubContractor_Sent_Text_Messages.Clear();
                }
                else
                {
                    DateTime? FromDate = dateEditFromDate.DateTime;
                    DateTime? ToDate = dateEditToDate.DateTime;
                    sp04082_GC_SubContractor_Sent_Text_MessagesTableAdapter.Fill(dataSet_GC_Core.sp04082_GC_SubContractor_Sent_Text_Messages, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), FromDate, ToDate);
                    this.RefreshGridViewState7.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }

                gridControl9.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs7 != "")
                {
                    strArray = i_str_AddedRecordIDs7.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl9.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SentTextMessageID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs7 = "";
                }
            }

            // Populate Self Billing Invoices //
            if (xtraTabPage6.PageVisible)
            {
                Load_Invoices();
            }

            // Populate Team Holiday Information //
            if (xtraTabPage8.PageVisible)
            {
                gridControl11.MainView.BeginUpdate();
                this.RefreshGridViewState11.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_GC_Core.sp04152_GC_Holidays_Linked_To_Team.Clear();
                }
                else
                {
                    DateTime? FromDate = dateEditFromDate.DateTime;
                    DateTime? ToDate = dateEditToDate.DateTime;
                    sp04152_GC_Holidays_Linked_To_TeamTableAdapter.Fill(dataSet_GC_Core.sp04152_GC_Holidays_Linked_To_Team, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), FromDate, ToDate);
                    this.RefreshGridViewState11.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }

                gridControl11.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs11 != "")
                {
                    strArray = i_str_AddedRecordIDs11.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl11.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SubContractorHolidayID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs11 = "";
                }
            }

            // Populate Team Rate Information //
            if (xtraTabPage9.PageVisible)
            {
                gridControl12.MainView.BeginUpdate();
                this.RefreshGridViewState12.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_GC_Snow_Core.sp04156_GC_Snow_Clearance_Team_Rates.Clear();
                }
                else
                {
                    sp04156_GC_Snow_Clearance_Team_RatesTableAdapter.Fill(dataSet_GC_Snow_Core.sp04156_GC_Snow_Clearance_Team_Rates, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState12.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }

                gridControl12.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs12 != "")
                {
                    strArray = i_str_AddedRecordIDs12.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl12.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SnowClearanceSubContractorRateRule"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs12 = "";
                }
            }

            // Populate Visit Type Information //
            if (xtraTabPage10.PageVisible)
            {
                gridControl14.MainView.BeginUpdate();
                this.RefreshGridViewState14.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_OM_Core.sp06309_Person_Visit_Types.Clear();
                }
                else
                {
                    sp06309_Person_Visit_TypesTableAdapter.Fill(dataSet_OM_Core.sp06309_Person_Visit_Types, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 1);
                    this.RefreshGridViewState14.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }

                gridControl14.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs14 != "")
                {
                    strArray = i_str_AddedRecordIDs14.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl14.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["VisitTypePersonID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs14 = "";
                }
            }

            // Populate Team Qualifications //
            if (xtraTabPage11.PageVisible && iBool_AllowVisibleTraining)
            {
                gridControlTraining.MainView.BeginUpdate();
                this.RefreshGridViewState15.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_Common_Functionality.sp09119_HR_Qualifications_For_Contractor.Clear();
                }
                else
                {
                    sp09119_HR_Qualifications_For_ContractorTableAdapter.Fill(dataSet_Common_Functionality.sp09119_HR_Qualifications_For_Contractor, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState15.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }

                gridControlTraining.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs15 != "")
                {
                    strArray = i_str_AddedRecordIDs15.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlTraining.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["QualificationID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs15 = "";
                }
            }

            // Populate Linked Insurance //
            if (xtraTabPageInsurance.PageVisible)
            {
                gridControl17.MainView.BeginUpdate();
                this.RefreshGridViewState17.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.woodPlanDataSet.sp00245_Core_Team_Insurance.Clear();
                }
                else
                {
                    sp00245_Core_Team_InsuranceTableAdapter.Fill(woodPlanDataSet.sp00245_Core_Team_Insurance, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState17.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl17.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs17 != "")
                {
                    strArray = i_str_AddedRecordIDs17.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl17.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["InsuranceID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs17 = "";
                }
            }

            // Populate Linked Insurance Categories //
            if (xtraTabPageInsuranceCategories.PageVisible)
            {
                gridControlInsuranceCategory.MainView.BeginUpdate();
                this.RefreshGridViewInsuranceCategory.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.woodPlanDataSet.sp00250_Core_Team_Insurance_Category_List.Clear();
                }
                else
                {
                    sp00250_Core_Team_Insurance_Category_ListTableAdapter.Fill(woodPlanDataSet.sp00250_Core_Team_Insurance_Category_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewInsuranceCategory.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlInsuranceCategory.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsInsuranceCategory != "")
                {
                    strArray = i_str_AddedRecordIDsInsuranceCategory.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlInsuranceCategory.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["TeamInsuranceCategoryID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsInsuranceCategory = "";
                }
            }


            // Populate Linked Documents //
            if (xtraTabPageLinkedDocuments.PageVisible)
            {
                gridControl25.MainView.BeginUpdate();
                this.RefreshGridViewState25.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_AT.sp00220_Linked_Documents_List.Clear();
                }
                else
                {
                    sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 101, strDefaultPath);
                    this.RefreshGridViewState25.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl25.MainView.EndUpdate();
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs25 != "")
                {
                    strArray = i_str_AddedRecordIDs25.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl25.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs25 = "";
                }
            }
        }

        private void LoadLinkedRecords2()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl7.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellValue(intRowHandle, view.Columns["AllocatedPdaID"]).ToString() + ",");
            }
            string strSelectedIDs = sb.ToString();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            // Populate Linked Contacts //
            if (xtraTabPage1.PageVisible)
            {

                gridControl13.MainView.BeginUpdate();
                this.RefreshGridViewState13.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_GC_Core.sp04344_GC_Allocated_PDA_Linked_Team_Members.Clear();
                }
                else
                {
                    sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter.Fill(dataSet_GC_Core.sp04344_GC_Allocated_PDA_Linked_Team_Members, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState13.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControl13.MainView.EndUpdate();

                strArray = null;
                intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs13 != "")
                {
                    strArray = i_str_AddedRecordIDs13.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl13.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["AllocatedPdaLinkedTeamMemberID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs13 = "";
                }
            }
        }

        private void LoadLinkedRecords_Person_Responsibilities(string strSelectedIDs)
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            int intCount = strSelectedIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length;
            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            GridView view = null;

            // Populate Linked Contacts //
            if (xtraTabPagePersonResponsibilities.PageVisible)
            {
                gridControlResponsibility.MainView.BeginUpdate();
                RefreshGridViewStateResponsibility.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_Common_Functionality.sp01020_Core_Team_Manager_Linked_Responsible_People.Clear();
                }
                else
                {
                    sp01020_Core_Team_Manager_Linked_Responsible_PeopleTableAdapter.Fill(dataSet_Common_Functionality.sp01020_Core_Team_Manager_Linked_Responsible_People, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateResponsibility.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlResponsibility.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsResponsibility != "")
                {
                    strArray = i_str_AddedRecordIDsResponsibility.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlResponsibility.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["PersonResponsibilityID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsResponsibility = "";
                    view.EndSelection();
                }
            }
        }

        private void Load_Data_Team_Member_Training()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl6.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellValue(intRowHandle, view.Columns["TeamMemberID"]).ToString() + ",");
            }
            string strSelectedIDs = sb.ToString();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            if (xtraTabPage11.PageVisible && iBool_AllowVisibleTraining)
            {

                gridControlTrainingTeam.MainView.BeginUpdate();
                this.RefreshGridViewState16.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_Common_Functionality.sp09120_HR_Qualifications_For_Team_Member.Clear();
                }
                else
                {
                    sp09120_HR_Qualifications_For_Team_MemberTableAdapter.Fill(dataSet_Common_Functionality.sp09120_HR_Qualifications_For_Team_Member, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewState16.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlTrainingTeam.MainView.EndUpdate();

                strArray = null;
                intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs16 != "")
                {
                    strArray = i_str_AddedRecordIDs16.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlTrainingTeam.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["QualificationID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs16 = "";
                }
            }
        }


        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3, string strNewIDs4, string strNewIDs5, string strNewIDs6, string strNewIDs7, string strNewIDs8, string strNewIDs14, string strNewIDs15, string strNewIDs16, string strNewIDs25)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (!string.IsNullOrWhiteSpace(strNewIDs1)) i_str_AddedRecordIDs1 = strNewIDs1;
            if (!string.IsNullOrWhiteSpace(strNewIDs2)) i_str_AddedRecordIDs2 = strNewIDs2;
            if (!string.IsNullOrWhiteSpace(strNewIDs3)) i_str_AddedRecordIDs3 = strNewIDs3;
            if (!string.IsNullOrWhiteSpace(strNewIDs4)) i_str_AddedRecordIDs4 = strNewIDs4;
            if (!string.IsNullOrWhiteSpace(strNewIDs5)) i_str_AddedRecordIDs5 = strNewIDs5;
            if (!string.IsNullOrWhiteSpace(strNewIDs6)) i_str_AddedRecordIDs6 = strNewIDs6;
            if (!string.IsNullOrWhiteSpace(strNewIDs7)) i_str_AddedRecordIDs11 = strNewIDs7;
            if (!string.IsNullOrWhiteSpace(strNewIDs8)) i_str_AddedRecordIDs12 = strNewIDs8;
            if (!string.IsNullOrWhiteSpace(strNewIDs14)) i_str_AddedRecordIDs14 = strNewIDs14;
            if (!string.IsNullOrWhiteSpace(strNewIDs15)) i_str_AddedRecordIDs15 = strNewIDs15;
            if (!string.IsNullOrWhiteSpace(strNewIDs16)) i_str_AddedRecordIDs16 = strNewIDs16;
            if (!string.IsNullOrWhiteSpace(strNewIDs25)) i_str_AddedRecordIDs25 = strNewIDs25;
        }
        public void UpdateFormRefreshStatus(int status, int intGridID, string strNewIDs)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (intGridID)
            {
                case 17:
                    if (!string.IsNullOrWhiteSpace(strNewIDs)) i_str_AddedRecordIDs17 = strNewIDs;
                    break;
                case 18:
                    if (!string.IsNullOrWhiteSpace(strNewIDs)) i_str_AddedRecordIDsInsuranceCategory = strNewIDs;
                    break;
                case 30:
                    if (!string.IsNullOrWhiteSpace(strNewIDs)) i_str_AddedRecordIDsResponsibility = strNewIDs;
                    break;
                default:
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Teams Available - Adjust any filters then click Refresh button";
                    break;
                case "gridView2":
                    message = "No Team Contacts Available - Select one or more Teams to view linked Contactse";
                    break;
                case "gridView3":
                    message = "No Team Certificates Available - Select one or more Teams to view linked Certificates";
                    break;
                case "gridView6":
                    message = "No Team Members Available - Select one or more Teams to view linked Team Members";
                    break;
                case "gridView7":
                    message = "No Team Allocated Devices Available - Select one or more Teams to view linked Allocated Devices";
                    break;
                case "gridView8":
                    message = "No Gritting Information Available - Select one or more Teams [with Is Gritter Ticked] to view linked Gritting Information";
                    break;
                case "gridView9":
                    message = "No Sent Text Messages Available - Select one or more Teams to view linked Sent Text Messages";
                    break;
                case "gridView10":
                    message = "No Self Billing Invoices Available";
                    break;
                case "gridView11":
                    message = "No Holidays Available - Select one or more Teams to view linked Holidays";
                    break;
                case "gridView12":
                    message = "No Team Rates Available - Select one or more Teams to view linked Rates";
                    break;
                case "gridView13":
                    message = "No Team Members Linked to Allocated Device - Select one or more Allocated Devices to view linked Team Members";
                    break;
                case "gridView14":
                    message = "No Linked Visit Types - Select one or more Teams to view linked Visit Types";
                    break;
                case "gridControlTraining":
                    message = "No Training - Select one or more Teams to see linked Training";
                    break;
                case "gridViewTrainingTeam":
                    message = "No Team Member Training - Select one or more Team Members to see linked Team Member Training";
                    break;
                case "gridView17":
                    message = "No Insurance Linked To Team - Select one or more Teams to view linked Insurance";
                    break;
                case "gridView18":
                    message = "No Insurance Category Linked To Team - Select one or more Teams to view linked Insurance Category";
                    break;
                case "gridView25":
                    message = "No Linked Documents Available - Select one or more Teams to view Linked Documents";
                    break;
                case "gridViewResponsibility":
                    message = "No Linked Person Responsibilities Available - Select one or more Visits to view Linked Person Responsibilities";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    string strSelectedIDs = GetSelectedParentIDs();
                    LoadLinkedRecords(strSelectedIDs);
                    LoadLinkedRecords_Person_Responsibilities(strSelectedIDs);

                    view = (GridView)gridControl2.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl6.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl11.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl7.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl8.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl10.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl9.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl12.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl14.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl17.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlTraining.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl25.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlInsuranceCategory.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlTrainingTeam.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlResponsibility.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView6":
                    Load_Data_Team_Member_Training();
                    view = (GridView)gridControlTrainingTeam.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView7":
                    LoadLinkedRecords2();
                    view = (GridView)gridControl13.MainView;
                    view.ExpandAllGroups();
                    SetMenuStatus();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("push_notification".Equals(e.Button.Tag))
                    {
                        Send_Push_Notification();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;

            bool boolColourCode = false;
            switch ((e.Column.FieldName).ToString())
            {
                case "ActiveTeamMembers":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ActiveTeamMembers")) <= 0) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "IsWoodPlanVisible":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsWoodPlanVisible")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "AmenityArbPublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "AmenityArbEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsGritter":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsGritter")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "GrittingPublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "GrittingEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsSnowClearer":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsSnowClearer")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SnowClearanceEmployersInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SnowClearanceEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsUtilityArbTeam":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsUtilityArbTeam")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "UtilityArbPublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "UtilityArbEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsOperationsTeam":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsOperationsTeam")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "MaintenancePublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "MaintenanceEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsPestControl":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsPestControl")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "PestControlPublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "PestControlEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsRailArb":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsRailArb")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RailArbPublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RailArbEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsConstruction":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsConstruction")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ConstructionPublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ConstructionEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsFencing":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsFencing")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "FencingPublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "FencingEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsRoofing":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsRoofing")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RoofingPublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RoofingEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsWindowCleaning":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsWindowCleaning")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "WindowCleaningPublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "WindowCleaningEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                case "IsPotholeTeam":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "IsPotholeTeam")) == 1 && (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "PotholePublicInsuranceValid")) != 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "PotholeEmployersInsuranceValid")) != 1)) boolColourCode = true;
                    }
                    break;
                default:
                    break;
            }
            if (boolColourCode)
            {
                e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            }

            //Default Contact & a default contact Email should be Mandatory
            if (e.Column == colDefaultContactEmail)
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, colDisabled)) == 0)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, colDefaultContactID)) == 0)
                    {
                        //No default Contact set up for an Active Team
                        e.Appearance.BackColor = Color.White;
                        e.Appearance.BackColor2 = Color.Red;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                    else
                    if (e.CellValue.ToString() == "")
                    {
                        //Active Team default contact has a blank email address
                        e.Appearance.BackColor = Color.White;
                        e.Appearance.BackColor2 = Color.Orange;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                }
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "LinkedRecordCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedRecordCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedRecordCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedRecordCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strParentIDs = view.GetRowCellValue(view.FocusedRowHandle, "ContractorID").ToString() + ",";
            DateTime FromDate = (string.IsNullOrEmpty(dateEditFromDate.DateTime.ToString()) ? Convert.ToDateTime("2011-01-01") : dateEditFromDate.DateTime);
            DateTime ToDate = (string.IsNullOrEmpty(dateEditToDate.DateTime.ToString()) ? Convert.ToDateTime("2500-12-31") : dateEditToDate.DateTime.AddDays(1).AddSeconds(-1));

            switch (comboBoxEditLinkedRecordType.EditValue.ToString())
            {
                case "Gritting Callouts":
                    {
                        using (var GetIDs = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter())
                        {
                            GetIDs.ChangeConnectionString(strConnectionString);
                            try
                            {
                                string strRecordIDs = GetIDs.sp04077_GC_DrillDown_Get_Linked_Callouts(strParentIDs, "contractor", i_str_selected_CallOutType_ids, FromDate, ToDate).ToString();
                                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "site_gritting_callouts");
                            }
                            catch (Exception) { }
                        }
                    }
                    break;
                 case "Snow Clearance Callouts":
                    {
                        using (var GetIDs = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter())
                        {
                            GetIDs.ChangeConnectionString(strConnectionString);
                            try
                            {
                                string strRecordIDs = GetIDs.sp04196_GC_Snow_DrillDown_Get_Linked_Callouts(strParentIDs, "contractor", i_str_selected_CallOutType_ids, FromDate, ToDate).ToString();
                                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "site_snow_clearance_callouts");
                            }
                            catch (Exception) { }
                        }
                    }
                    break;
                 case "Maintenance Visits":
                    {
                        using (var GetIDs = new DataSet_OM_CoreTableAdapters.QueriesTableAdapter())
                        {
                            GetIDs.ChangeConnectionString(strConnectionString);
                            try
                            {
                                string strRecordIDs = GetIDs.sp06403_OM_Drilldown_Get_IDs(strParentIDs, "visits_for_team", i_str_selected_CallOutType_ids, FromDate, ToDate).ToString();
                                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_visit");
                            }
                            catch (Exception) { }
                        }
                    }
                    break;
                  case "Maintenance Jobs":
                    {
                        using (var GetIDs = new DataSet_OM_CoreTableAdapters.QueriesTableAdapter())
                        {
                            GetIDs.ChangeConnectionString(strConnectionString);
                            try
                            {
                                string strRecordIDs = GetIDs.sp06403_OM_Drilldown_Get_IDs(strParentIDs, "jobs_for_teams", i_str_selected_CallOutType_ids, FromDate, ToDate).ToString();
                                Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_job");
                            }
                            catch (Exception) { }
                        }
                    }
                    break;
                default:
                    break;
            }

        }

        private void Send_Push_Notification()
        {
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Teams to send push notifications to before proceeding.", "Send Push Notifications", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContractorID")) + ',';
            }
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            var fChildForm = new frm_Core_Contractor_Send_Push_Notification();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._PassedInPersonIDs = strRecordIDs;
            fChildForm._PassedInPersonTypeID = 1;  // 0 = Staff, 1 = Contractor //
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "Email":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "Email").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView2_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "Email":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("Email").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit4_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strEmailAddress = view.GetRowCellValue(view.FocusedRowHandle, "Email").ToString();
            if (string.IsNullOrEmpty(strEmailAddress))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Email Address Available - unable to proceed.", "Email Contact", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start("mailto:" + strEmailAddress);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to email: " + strEmailAddress + ".", "Email Contact", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView3_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "CertificatePath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "CertificatePath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView3_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "CertificatePath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("CertificatePath").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "CertificatePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Contractor Certificate Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Contractor Certificate File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView6

        private void gridView6_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 4;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView6_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "Email":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "Email").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView6_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "Email":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("Email").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit3_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strEmailAddress = view.GetRowCellValue(view.FocusedRowHandle, "Email").ToString();
            if (string.IsNullOrEmpty(strEmailAddress))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Email Address Available - unable to proceed.", "Email Team Member", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start("mailto:" + strEmailAddress);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to email: " + strEmailAddress + ".", "Email Team Member", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView7

        private void gridView7_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView7_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 5;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl7_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("pda_manager".Equals(e.Button.Tag))
                    {
                        var fChildForm = new frm_Core_PDA_Manager();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strFormMode = "";
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView8

        private void gridView8_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 6;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView8_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "CurrentGritLevel")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "HoldsStock")) > 0 && Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "CurrentGritLevel")) < Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "UrgentGritLevel")) && Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "UrgentGritLevel")) > 0)
                {
                    e.Appearance.BackColor = Color.LightCoral;
                    e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "HoldsStock")) > 0 && Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "CurrentGritLevel")) < Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "WarningGritLevel")) && Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "WarningGritLevel")) > 0)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "GritDepotCurrentGrittingLevel")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "GritDepotCurrentGrittingLevel")) < Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "GritDepotUrgentGritLevel")) && Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "GritDepotUrgentGritLevel")) > 0)
                {
                    e.Appearance.BackColor = Color.LightCoral;
                    e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "GritDepotCurrentGrittingLevel")) < Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "GritDepotWarningGritLevel")) && Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "GritDepotWarningGritLevel")) > 0)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }

        }

        private void gridView8_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "GritDepotEmail":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "GritDepotEmail").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                case "GritDepotWebSite":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "GritDepotWebSite").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView8_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "GritDepotEmail":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("GritDepotEmail").ToString())) e.Cancel = true;
                    break;
                case "GritDepotWebSite":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("GritDepotWebSite").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit5_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.FieldName == "GritDepotEmail")
            {
                string strEmailAddress = view.GetRowCellValue(view.FocusedRowHandle, "GritDepotEmail").ToString();
                if (string.IsNullOrEmpty(strEmailAddress))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Email Address Available - unable to proceed.", "Email Gritting Depot", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    System.Diagnostics.Process.Start("mailto:" + strEmailAddress);
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to email: " + strEmailAddress + ".", "Email Gritting Depot", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else if (view.FocusedColumn.FieldName == "GritDepotWebSite")
            {
                string strWebsite = view.GetRowCellValue(view.FocusedRowHandle, "GritDepotWebSite").ToString();
                if (!strWebsite.ToLower().StartsWith("http://")) strWebsite = "http://" + strWebsite;
                if (string.IsNullOrEmpty(strWebsite))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Website Available - unable to proceed.", "Visit Gritting Depot Website", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    System.Diagnostics.Process.Start(strWebsite);
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to connect to: " + strWebsite + ".", "Visit Gritting Depot Website", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void gridControl8_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView9

        private void gridView9_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView9_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 7;
            SetMenuStatus();
        }

        private void gridView9_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 7;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl9_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView10

        private void gridView10_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView10_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 8;
            SetMenuStatus();
        }

        private void gridView10_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 8;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView10_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "LinkedFileName":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "LinkedFileName").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView10_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedFileName":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("LinkedFileName").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit6_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "LinkedFileName").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No PDF File Linked - unable to proceed.", "View Linked PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(i_str_PDFDirectoryName + strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + i_str_PDFDirectoryName + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked PDF File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl10_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView11

        private void gridView11_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView11_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 11;
            SetMenuStatus();
        }

        private void gridView11_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 11;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl11_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView12

        private void gridView12_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView12_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 12;
            SetMenuStatus();
        }

        private void gridView12_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 12;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl12_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridControl12_Paint(object sender, PaintEventArgs e)
        {
            // Following code paints different coloured column seperator line //
            /*GridControl grid = (GridControl)sender;
            GridView view = (GridView)grid.MainView;
            GridViewInfo viewInfo = view.GetViewInfo() as GridViewInfo;
            Point p1, p2;
            foreach (GridRowInfo rowInfo in viewInfo.RowsInfo)
                if (!rowInfo.IsGroupRow && !view.IsFilterRow(rowInfo.RowHandle))
                {
                    GridCellInfo cellInfo = viewInfo.GetGridCellInfo(rowInfo.RowHandle, colRate);
                    p1 = new Point(cellInfo.Bounds.Right, cellInfo.Bounds.Top);
                    p2 = new Point(cellInfo.Bounds.Right, cellInfo.Bounds.Bottom);
                    e.Graphics.DrawLine(Pens.Red, p1, p2);
                    cellInfo = viewInfo.GetGridCellInfo(rowInfo.RowHandle, colMachineRate2);
                    p1 = new Point(cellInfo.Bounds.Right, cellInfo.Bounds.Top);
                    p2 = new Point(cellInfo.Bounds.Right, cellInfo.Bounds.Bottom);
                    e.Graphics.DrawLine(Pens.Red, p1, p2);
                    cellInfo = viewInfo.GetGridCellInfo(rowInfo.RowHandle, colMachineRate3);
                    p1 = new Point(cellInfo.Bounds.Right, cellInfo.Bounds.Top);
                    p2 = new Point(cellInfo.Bounds.Right, cellInfo.Bounds.Bottom);
                    e.Graphics.DrawLine(Pens.Red, p1, p2);
                    cellInfo = viewInfo.GetGridCellInfo(rowInfo.RowHandle, colMachineRate4);
                    p1 = new Point(cellInfo.Bounds.Right, cellInfo.Bounds.Top);
                    p2 = new Point(cellInfo.Bounds.Right, cellInfo.Bounds.Bottom);
                    e.Graphics.DrawLine(Pens.Red, p1, p2);
                }*/
        }
        
        #endregion


        #region GridView13

        private void gridView13_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView13_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 13;
            SetMenuStatus();
        }

        private void gridView13_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 13;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl13_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView14

        private void gridView14_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView14_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 14;
            SetMenuStatus();
        }

        private void gridView14_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 14;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl14_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView Training

        private void gridControlTraining_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlTraining.MainView;
                        int intRecordType = 4;  // Training //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPersonName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "CourseName").ToString() + " [" + view.GetRowCellValue(view.FocusedRowHandle, "CourseNumber").ToString() + "]";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewTraining_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewTraining_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 15;
            SetMenuStatus();
        }

        private void gridViewTraining_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 15;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewTraining_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DaysUntilExpiry")
            {
                int intDaysToDo = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysUntilExpiry"));
                if (intDaysToDo < 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (intDaysToDo < 30)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewTraining_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "DaysUntilExpiry":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysUntilExpiry")) == 99999) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewTraining_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "CertificatePath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("CertificatePath").ToString())) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsTraining_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 4;  // Training //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPersonName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "CourseName").ToString() + " [" + view.GetRowCellValue(view.FocusedRowHandle, "CourseNumber").ToString() + "]";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView TrainingTeam

        private void gridControlTrainingTeam_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlTrainingTeam.MainView;
                        int intRecordType = 4;  // Training //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPersonName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "CourseName").ToString() + " [" + view.GetRowCellValue(view.FocusedRowHandle, "CourseNumber").ToString() + "]";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewTrainingTeam_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewTrainingTeam_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 16;
            SetMenuStatus();
        }

        private void gridViewTrainingTeam_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 16;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewTrainingTeam_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DaysUntilExpiry")
            {
                int intDaysToDo = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysUntilExpiry"));
                if (intDaysToDo < 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (intDaysToDo < 30)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewTrainingTeam_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "DaysUntilExpiry":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysUntilExpiry")) == 99999) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewTrainingTeam_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "CertificatePath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("CertificatePath").ToString())) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit7_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 4;  // Training //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPersonName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "CourseName").ToString() + " [" + view.GetRowCellValue(view.FocusedRowHandle, "CourseNumber").ToString() + "]";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView17

        private void gridControl17_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView17_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;

            if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "Active")) != 1) return;
            switch ((e.Column.FieldName).ToString())
            {
                case "GrittingPublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "GrittingPublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinGrittingPublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "GrittingEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "GrittingEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinGrittingEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "SnowClearancePublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "SnowClearancePublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinSnowClearancePublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "SnowClearanceEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "SnowClearanceEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinSnowClearanceEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "MaintenancePublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MaintenancePublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinMaintenancePublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "MaintenanceEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MaintenanceEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinMaintenanceEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "ConstructionPublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "ConstructionPublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinConstructionPublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "ConstructionEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "ConstructionEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinConstructionEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "AmenityArbPublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "AmenityArbPublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinAmenityArbPublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "AmenityArbEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "AmenityArbEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinAmenityArbEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "UtilityArbPublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "UtilityArbPublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinUtilityArbPublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "UtilityArbEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "UtilityArbEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinUtilityArbEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "RailArbPublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RailArbPublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinRailArbPublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "RailArbEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RailArbEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinRailArbEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "PestControlPublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "PestControlPublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinPestControlPublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "PestControlEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "PestControlEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinPestControlEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "FencingPublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "FencingPublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinFencingPublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "FencingEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "FencingEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinFencingEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "RoofingPublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RoofingPublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinRoofingPublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "RoofingEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RoofingEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinRoofingEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "WindowCleaningPublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "WindowCleaningPublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinWindowCleaningPublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "WindowCleaningEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "WindowCleaningEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinWindowCleaningEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "PotholePublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "PotholePublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinPotholePublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                case "PotholeEmployersValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "PotholeEmployersValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinPotholeEmployersValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView17_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView17_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 17;
            SetMenuStatus();
        }

        private void gridView17_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 17;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemHyperLinkEditInsurance_OpenLink(object sender, OpenLinkEventArgs e)
        {
            string strPath = "";
            try
            {
                DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter GetSetting = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strPath = GetSetting.sp01015_Get_Linked_Document_Folder_Path(102).ToString();
            }
            catch (Exception) { }

            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "LinkedCertificate").ToString();
            if (!strPath.EndsWith("\\") && !strFile.StartsWith("\\")) strPath += "\\";
            strFile = strPath + strFile;
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Insurance Certificate Linked - unable to proceed.", "View Linked Insurance Certificate", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Insurance Certificate", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView18   //Contractor insurance Category//

        private void gridControl18_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView18_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;

            if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "Active")) != 1) return;
            switch ((e.Column.FieldName).ToString())
            {
                case "GrittingPublicValue":
                    {
                        if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "GrittingPublicValue")) < Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "MinGrittingPublicValue"))) e.Appearance.ForeColor = Color.Red;
                    }
                    break;

                default:
                    break;
            }
        }

        private void gridView18_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView18_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 18;
            SetMenuStatus();
        }

        private void gridView18_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 18;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView25

        private void gridView25_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView25_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 25;
            SetMenuStatus();
        }

        private void gridView25_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 25;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView25_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "CertificatePath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "CertificatePath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView25_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "CertificatePath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("CertificatePath").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit8_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl25_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView - Person Responsibility

        private void gridControlResponsibility_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinkedRecords_Person_Responsibilities(GetSelectedParentIDs());
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewResponsibility_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewResponsibility_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 30;
            SetMenuStatus();
        }

        private void gridViewResponsibility_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 30;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_HR_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp_HR_00191_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "hr_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }


        #region Callout Status Filter Panel

        private void btnGritCalloutFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private void popupContainerEdit2_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_CallOutType_ids = "";    // Reset any prior values first //
            i_str_selected_CallOutType_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_CallOutType_ids = "";
                return "No Callout Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_CallOutType_ids += Convert.ToString(view.GetRowCellValue(i, "Value")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_CallOutType_names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_CallOutType_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_CallOutType_names;
        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        #endregion


        #region Tab Pages Shown Panel

        private void btnOK2_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            //i_int_FocusedGrid = 2;
            //SetMenuStatus();
        }

        private void popupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_TabPages = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_TabPages = "";
                foreach (XtraTabPage i in xtraTabControl1.TabPages)
                {
                    if (i.Text == "Training")
                    {
                        i.PageVisible = iBool_AllowVisibleTraining;
                    }
                    else
                    {
                        i.PageVisible = true;
                    }
                }
                return "All Related Data";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_TabPages = "";
                foreach (XtraTabPage i in xtraTabControl1.TabPages)
                {
                    if (i.Text == "Training")
                    {
                        i.PageVisible = iBool_AllowVisibleTraining;
                    }
                    else
                    {
                        i.PageVisible = true;
                    }
                }
                return "All Related Data";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        //i_str_selected_CallOutType_ids += Convert.ToString(view.GetRowCellValue(i, "Value")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_TabPages = Convert.ToString(view.GetRowCellValue(i, "TabPageName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_TabPages += ", " + Convert.ToString(view.GetRowCellValue(i, "TabPageName"));
                        }
                        intCount++;
                    }
                }
            }

            foreach (XtraTabPage i in xtraTabControl1.TabPages)
            {
                string strText = i.Text;
                i.PageVisible = i_str_selected_TabPages.Contains(strText);
            }

            return i_str_selected_TabPages;
        }

        private void Set_Training_Visibility()
        {
            xtraTabPage11.PageVisible = iBool_AllowVisibleTraining;
            splitContainerControlTeamMembers.PanelVisibility = (iBool_AllowVisibleTraining ? SplitPanelVisibility.Both : SplitPanelVisibility.Panel1);
            gridControlTrainingTeam.Visible = iBool_AllowVisibleTraining;
            gridControlTrainingTeam.Enabled = iBool_AllowVisibleTraining;
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            if (!iBool_AllowAdd) return;

            GridView view = null;
            GridView ParentView = null;
            System.Reflection.MethodInfo method = null;
            int[] intRowHandles = null;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        frmProgress fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Edit fChildForm = new frm_Core_Contractor_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:
                    {
                        // Check if only one parent contractor selected - if yes, pass it to child screen otherwise pass no contractor //
                        frmProgress fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;  // Parent GridView //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Contact_Edit fChildForm2 = new frm_Core_Contractor_Contact_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intContractorID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ContractorID")) : 0);
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 3:
                    {
                        // Check if only one parent contractor selected - if yes, pass it to child screen otherwise pass no contractor //
                        frmProgress fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;  // Parent GridView //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Certificate_Edit fChildForm3 = new frm_Core_Contractor_Certificate_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = "";
                        fChildForm3.strFormMode = "add";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = 0;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.intContractorID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ContractorID")) : 0);
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 4:
                    {
                        // Check if only one parent contractor selected - if yes, pass it to child screen otherwise pass no contractor //
                        frmProgress fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;  // Parent GridView //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Team_Member_Edit fChildForm3 = new frm_Core_Contractor_Team_Member_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = "";
                        fChildForm3.strFormMode = "add";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = 0;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.intContractorID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ContractorID")) : 0);
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 5:
                    {
                        // Check if only one parent contractor selected - if yes, pass it to child screen otherwise pass no contractor //
                        frmProgress fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;  // Parent GridView //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Allocate_PDA_Edit fChildForm3 = new frm_Core_Contractor_Allocate_PDA_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = "";
                        fChildForm3.strFormMode = "add";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = 0;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.intContractorID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ContractorID")) : 0);
                        fChildForm3._PassedInLabourName = (intRowHandles.Length == 1 ? view.GetRowCellValue(intRowHandles[0], "ContractorName").ToString() : "");
                        fChildForm3._PassedInLabourTypeID = 1;
                        fChildForm3._PassedInLabourType = "Contractor";
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                /*case 6:
                    {
                        // Check if only one parent contractor selected - if yes, pass it to child screen otherwise pass no contractor //
                        
                        frmProgress fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;  // Parent GridView //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Team_Gritting_Information_Edit fChildForm3 = new frm_GC_Team_Gritting_Information_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = "";
                        fChildForm3.strFormMode = "add";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = 0;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.intContractorID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ContractorID")) : 0);
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }*/
                case 11:  // Team Holidays //
                    {
                        // Check if only one parent contractor selected - if yes, pass it to child screen otherwise pass no contractor //
                        frmProgress fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;  // Parent GridView //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Team_Holiday_Edit fChildForm2 = new frm_GC_Team_Holiday_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intLinkedToRecordID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ContractorID")) : 0);
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 12:  // Team Snow Clearance Rates //
                    {
                        // Check if only one parent contractor selected - if yes, pass it to child screen otherwise pass no contractor //
                        frmProgress fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl1.MainView;  // Parent GridView //
                        intRowHandles = view.GetSelectedRows();

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Team_Rate_Edit fChildForm2 = new frm_GC_Snow_Team_Rate_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intLinkedToRecordID = (intRowHandles.Length == 1 ? Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ContractorID")) : 0);
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 13:  // Team Members Allocated to DEvice //
                    {
                        frmProgress fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(10); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                             Application.DoEvents();  // Allow Form time to repaint itself //
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one Allocated Device to link Team Members to before proceeding.", "Link Team Members to Allocated Device", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intAllocatedPDA = 0;
                        int intTeamID = 0;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            intAllocatedPDA += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "AllocatedPdaID"));
                            intTeamID += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SubContractorID"));
                        }
                        frm_GC_Allocated_PDA_Add_Linked_Team_Members fChildForm = new frm_GC_Allocated_PDA_Add_Linked_Team_Members();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intAllocatedPDAID = intAllocatedPDA;
                        fChildForm.intTeamID = intTeamID;
                        fChildForm._PassedInLabourTypeID = 1; // 0 = Staff, 1 = Contractor //
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form so reload data... //
                        {
                            i_str_AddedRecordIDs13 = fChildForm.strNewRecordIDs;
                            LoadLinkedRecords2();
                        }
                        break;
                    }
                case 14:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Visit_Type_Person_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (WoodPlanDataSet.sp00180_Contractor_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.ContractorID;
                            fChildForm.strLinkedToRecordDescription = row.ContractorName;
                        }
                        fChildForm.intLinkedtoPersonTypeID = 1;  // 0 = Staff, 1 = Contractor //
                        fChildForm.intLinkedtoPersonTypeID = 1;  // 0 = Staff, 1 = Contractor //
                        fChildForm.intRecordTypeID = 1;  // 1 = People (Staff \ contractor), 2 = Master Job //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 15:
                    {
                        if (!iBool_AllowAddTraining) return;

                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        int intPersonType = 1;  // Team //

                        frm_HR_Qualification_Edit fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = intPersonType;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (WoodPlanDataSet.sp00180_Contractor_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.ContractorID;
                            fChildForm.strLinkedToRecordDesc = row.ContractorName;
                        }
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 16:
                    {
                        if (!iBool_AllowAddTraining) return;

                        view = (GridView)gridControlTrainingTeam.MainView;
                        view.PostEditor();
                        int intPersonType = 2;  // Team Member//

                        frm_HR_Qualification_Edit fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = intPersonType;

                        ParentView = (GridView)gridControl6.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_GC_Core.sp04023_GC_SubContractor_Team_MembersRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.TeamMemberID;
                            fChildForm.strLinkedToRecordDesc = row.Name;
                        }
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 17:
                    {
                        view = (GridView)gridControl17.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Contractor_Insurance_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (WoodPlanDataSet.sp00180_Contractor_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.ContractorID;
                            fChildForm.strLinkedToRecordDesc = row.ContractorName;
                        }
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case 18: // Team Insurance Category
                    {
                        view = (GridView)gridControlInsuranceCategory.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Contractor_Insurance_Category_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (WoodPlanDataSet.sp00180_Contractor_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.ContractorID;
                            fChildForm.strLinkedToRecordDesc = row.ContractorName;
                        }
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case 25:  // Linked Documents //
                    {
                        if (!iBool_AllowAddLinkedDocument) return;

                        view = (GridView)gridControl25.MainView;
                        view.PostEditor();
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 101;  // Team //

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        fChildForm.intLinkedToRecordID = (intRowHandles.Length == 1 ? Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ContractorID")) : 0);
                        fChildForm.strLinkedToRecordDesc = (intRowHandles.Length == 1 ? ParentView.GetRowCellValue(intRowHandles[0], "ContractorName").ToString() : "");
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 30:  // Person Responsibility //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "51,52,53,54,55,56,57,58,59";
                        fChildForm.i_intPassedInRecordTypeID = 4;  // Team //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (WoodPlanDataSet.sp00180_Contractor_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.ContractorID;
                            fChildForm.strLinkedToRecordDesc = row.ContractorName;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

            }
        }

        private void Block_Add_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            StringBuilder sb = new StringBuilder();
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                 case 11:     // Team Holidays //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one Team record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContractorID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Team_Holiday_Edit fChildForm2 = new frm_GC_Team_Holiday_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                 case 12:     // Team Snow Clearance Rates //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one Team Snow Clearance Rate record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContractorID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Team_Rate_Edit fChildForm2 = new frm_GC_Snow_Team_Rate_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                 case 14:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContractorID")) + ',';
                        }

                        var fChildForm = new frm_OM_Visit_Type_Person_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intLinkedtoPersonTypeID = 1;  // 0 = Staff, 1 = Contractor //
                        fChildForm.intRecordTypeID = 1;  // 1 = People (Staff \ contractor), 2 = Master Job //
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                 case 15:
                    {
                        if (!iBool_AllowAddTraining) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContractorID")) + ',';
                        }

                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 1;  // 1 = Team, 2 = Team Member //
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                 case 16:
                    {
                        if (!iBool_AllowAddTraining) return;
                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TeamMemberID")) + ',';
                        }

                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 2;  // 1 = Team, 2 = Team Member //
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                 case 17:
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one Team record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb2 = new StringBuilder();
                        sb2.Append("Block Adding To: ");
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "ContractorID").ToString() + ",");
                            sb2.Append(view.GetRowCellValue(intRowHandle, "ContractorName").ToString() + ", ");
                        }
                        if (sb.Length > 2)
                        {
                            sb2.Length--;  // Remove last space //
                            sb2.Length--;  // Remove last comma //
                        }

                        var fChildForm = new frm_Core_Contractor_Insurance_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strLinkedToRecordDesc = sb2.ToString();
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 18:
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one Team record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        StringBuilder sb2 = new StringBuilder();
                        sb2.Append("Block Adding To: ");
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "ContractorID").ToString() + ",");
                            sb2.Append(view.GetRowCellValue(intRowHandle, "ContractorName").ToString() + ", ");
                        }
                        if (sb.Length > 2)
                        {
                            sb2.Length--;  // Remove last space //
                            sb2.Length--;  // Remove last comma //
                        }

                        var fChildForm = new frm_Core_Contractor_Insurance_Category_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strLinkedToRecordDesc = sb2.ToString();
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 25:     // Related Documents Link //
                    {
                        if (!iBool_AllowAddLinkedDocument) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContractorID")) + ',';
                        }
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 101;  // Team //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 30:  // Person Responsibility //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellDisplayText(intRowHandle, "ContractorID").ToString() + ",");
                        }

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "51,52,53,54,55,56,57,58,59";
                        fChildForm.i_intPassedInRecordTypeID = 4;  // Team //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                 default:
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            if (!iBool_AllowEdit) return;
            StringBuilder sb = new StringBuilder();
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            frmProgress fProgress = null;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Records", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContractorID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Edit fChildForm = new frm_Core_Contractor_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContactID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Contact_Edit fChildForm2 = new frm_Core_Contractor_Contact_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 3:
                    {
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CertificateID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Certificate_Edit fChildForm3 = new frm_Core_Contractor_Certificate_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = strRecordIDs;
                        fChildForm3.strFormMode = "blockedit";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = intCount;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 4:
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TeamMemberID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Team_Member_Edit fChildForm3 = new frm_Core_Contractor_Team_Member_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = strRecordIDs;
                        fChildForm3.strFormMode = "blockedit";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = intCount;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 5:
                    {
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AllocatedPdaID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Allocate_PDA_Edit fChildForm3 = new frm_Core_Contractor_Allocate_PDA_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = strRecordIDs;
                        fChildForm3.strFormMode = "blockedit";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = intCount;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 6:
                    {
                        view = (GridView)gridControl8.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SubContractorGritInformationID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Team_Gritting_Information_Edit fChildForm3 = new frm_GC_Team_Gritting_Information_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = strRecordIDs;
                        fChildForm3.strFormMode = "blockedit";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = intCount;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 11:  // Team Holiday //
                    {
                        view = (GridView)gridControl11.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SubContractorHolidayID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Team_Holiday_Edit fChildForm2 = new frm_GC_Team_Holiday_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 12:  // Team Snow Clearance Rate //
                    {
                        view = (GridView)gridControl12.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more then one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSubContractorRateRule")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();    // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Team_Rate_Edit fChildForm2 = new frm_GC_Snow_Team_Rate_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 14:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitTypePersonID")) + ',';
                        }

                        var fChildForm = new frm_OM_Visit_Type_Person_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intLinkedtoPersonTypeID = 1;  // 0 = Staff, 1 = Contractor //
                        fChildForm.intRecordTypeID = 1;  // 1 = People (Staff \ contractor), 2 = Master Job //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 15:
                    {
                        if (!iBool_AllowEditTraining) return;
                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }

                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 1;  // 1 = Team, 2 = Team Member //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 16:
                    {
                        if (!iBool_AllowEditTraining) return;
                        view = (GridView)gridControlTrainingTeam.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }

                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 2;  // 1 = Team, 2 = Team Member //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 17:
                    {
                        view = (GridView)gridControl17.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "InsuranceID").ToString() + ",");
                        }

                        var fChildForm = new frm_Core_Contractor_Insurance_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 18:
                    {
                        view = (GridView)gridControlInsuranceCategory.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "TeamInsuranceCategoryID").ToString() + ",");
                        }

                        var fChildForm = new frm_Core_Contractor_Insurance_Category_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 25:     // Linked Documents //
                    {
                        if (!iBool_AllowEditLinkedDocument) return;
                        view = (GridView)gridControl25.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 101;  // Team //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 30:  // Person Responsibility //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellDisplayText(intRowHandle, "PersonResponsibilityID").ToString() + ",");
                        }

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "51,52,53,54,55,56,57,58,59";
                        fChildForm.i_intPassedInRecordTypeID = 4;  // Team //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }

        private void Edit_Record()
        {
            if (!iBool_AllowEdit) return;
            StringBuilder sb = new StringBuilder();
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            frmProgress fProgress = null;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContractorID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Edit fChildForm = new frm_Core_Contractor_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 2:
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContactID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Contact_Edit fChildForm2 = new frm_Core_Contractor_Contact_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 3:
                    {
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CertificateID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Certificate_Edit fChildForm3 = new frm_Core_Contractor_Certificate_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = strRecordIDs;
                        fChildForm3.strFormMode = "edit";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = intCount;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 4:
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TeamMemberID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Team_Member_Edit fChildForm3 = new frm_Core_Contractor_Team_Member_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = strRecordIDs;
                        fChildForm3.strFormMode = "edit";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = intCount;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 5:
                    {
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AllocatedPdaID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Allocate_PDA_Edit fChildForm3 = new frm_Core_Contractor_Allocate_PDA_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = strRecordIDs;
                        fChildForm3.strFormMode = "edit";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = intCount;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 6:
                    {
                        view = (GridView)gridControl8.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SubContractorGritInformationID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Team_Gritting_Information_Edit fChildForm3 = new frm_GC_Team_Gritting_Information_Edit();
                        fChildForm3.MdiParent = this.MdiParent;
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.strRecordIDs = strRecordIDs;
                        fChildForm3.strFormMode = "edit";
                        fChildForm3.strCaller = this.Name;
                        fChildForm3.intRecordCount = intCount;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm3, new object[] { null });
                        break;
                    }
                case 11:  // Team Holiday //
                    {
                        view = (GridView)gridControl11.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SubContractorHolidayID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Team_Holiday_Edit fChildForm2 = new frm_GC_Team_Holiday_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 12:  // Team Snow Clearance Rate //
                    {
                        view = (GridView)gridControl12.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSubContractorRateRule")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State - Set to GridView 1 [parent view] as it will reload it's children //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_GC_Snow_Team_Rate_Edit fChildForm2 = new frm_GC_Snow_Team_Rate_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                        break;
                    }
                case 14:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitTypePersonID")) + ',';
                        }

                        var fChildForm = new frm_OM_Visit_Type_Person_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intLinkedtoPersonTypeID = 1;  // 0 = Staff, 1 = Contractor //
                        fChildForm.intRecordTypeID = 1;  // 1 = People (Staff \ contractor), 2 = Master Job //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 15:
                    {
                        if (!iBool_AllowEditTraining) return;
                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }

                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 1;  // 1 = Team, 2 = Team Member //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 16:
                    {
                        if (!iBool_AllowEditTraining) return;
                        view = (GridView)gridControlTrainingTeam.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ",");
                        }

                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 2;  // 1 = Team, 2 = Team Member //
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 17:
                    {
                        view = (GridView)gridControl17.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "InsuranceID").ToString() + ",");
                        }

                        var fChildForm = new frm_Core_Contractor_Insurance_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 18:
                    {
                        view = (GridView)gridControlInsuranceCategory.MainView ;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "TeamInsuranceCategoryID").ToString() + ",");
                        }

                        var fChildForm = new frm_Core_Contractor_Insurance_Category_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 25:     // Linked Documents //
                    {
                        if (!iBool_AllowEditLinkedDocument) return;
                        view = (GridView)gridControl25.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 101;  // Team //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 30:  // Person Responsibility //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellDisplayText(intRowHandle, "PersonResponsibilityID").ToString() + ",");
                        }

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "51,52,53,54,55,56,57,58,59";
                        fChildForm.i_intPassedInRecordTypeID = 4;  // Team //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }

        private void Delete_Record()
        {
            if (!iBool_AllowDelete) return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            string strRecordIDs = "";
            StringBuilder sb = new StringBuilder();
            switch (i_int_FocusedGrid)
            {
                case 1:
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Teams to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Team" : Convert.ToString(intRowHandles.Length) + " Teams") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this team" : "these teams") + " will no longer be available for selection and any linked records will also be deleted!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContractorID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        WoodPlanDataSetTableAdapters.QueriesTableAdapter RemoveRecords = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        RemoveRecords.sp00185_Contractor_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        Load_Data();

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 2:
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Contacts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Team Contact" : Convert.ToString(intRowHandles.Length) + " Team Contacts") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Team Contact" : "these Team Contacts") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContactID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        WoodPlanDataSetTableAdapters.QueriesTableAdapter RemoveRecords = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                        RemoveRecords.sp00189_Contractor_Contact_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        LoadLinkedRecords(GetSelectedParentIDs());

                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:
                    {
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Certificates to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team Certificate" : Convert.ToString(intRowHandles.Length) + " Team Certificates") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Team Certificate" : "these Team Certificates") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CertificateID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            WoodPlanDataSetTableAdapters.QueriesTableAdapter RemoveRecords = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp00193_Contractor_Certificate_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 4:
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Members to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team Member" : Convert.ToString(intRowHandles.Length) + " Team Members") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Team Member" : "these Team Members") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TeamMemberID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp03002_EP_Client_Delete("gc_sub_contractor_team_member", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 5:
                    {
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Allocated PDAs to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Allocated Device" : Convert.ToString(intRowHandles.Length) + " Allocated Devices") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Allocated Device" : "these Allocated Devices") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AllocatedPdaID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp03002_EP_Client_Delete("gc_sub_contractor_allocated_pda", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                /*case 6:
                    {
                        view = (GridView)gridControl8.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Gritting Information Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team Gritting Information Record" : Convert.ToString(intRowHandles.Length) + " Team Gritting Information Records") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Team Gritting Information Record" : "these Team Gritting Information Records") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SubContractorGritInformationID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp03002_EP_Client_Delete("gc_sub_contractor_allocated_pda", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }*/
                case 7:  // Sent Text Message //
                    {
                        view = (GridView)gridControl9.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Sent Text Messages to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Sent Text Message" : Convert.ToString(intRowHandles.Length) + " Sent Text Messages") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Sent Text Message" : "these Sent Text Messages") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SentTextMessageID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp03002_EP_Client_Delete("gc_sent_text_message", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 8:
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl10.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Self-Billing Invoices to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Self-Billing Invoice" : Convert.ToString(intRowHandles.Length) + " Self-Billing Invoices") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Self-Billing Invoice" : "these Self-Billing Invoices") + " will no longer be available for selection!\n\nAny Gritting Callouts on the invoice will be unlinked and available for inclusion on a new invoice.\n\nNote that any linked PDF files will not be deleted - these must be manually deleted.";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GrittingInvoiceID")) + ",";
                        }
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //


                        DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState8.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        RemoveRecords.sp03002_EP_Client_Delete("gc_gritting_self_billing_invoice", strRecordIDs);  // Remove the records from the DB in one go //
                        if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                        Load_Invoices();
                        if (fProgress != null)
                        {
                            fProgress.UpdateProgress(20); // Update Progress Bar //
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 11:  // Team Holidays //
                    {
                        view = (GridView)gridControl11.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Holidays to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team Holiday" : Convert.ToString(intRowHandles.Length) + " Team Holidays") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Team Holiday" : "these Team Holidays") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SubContractorHolidayID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp03002_EP_Client_Delete("gc_sub_contractor_holiday", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 12:  // Team Snow Clearance Rates //
                    {
                        view = (GridView)gridControl12.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Snow Clearance Rates to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team Snow Clearance Rate" : Convert.ToString(intRowHandles.Length) + " Team Snow Clearance Rates") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Team Snow Clearance Rate" : "these Team Snow Clearance Rates") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SnowClearanceSubContractorRateRule")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp03002_EP_Client_Delete("gc_snow_clearance_subcontractor_rate_rule", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case 13:  // Team Members Linked to Allocated Device //
                    {
                        view = (GridView)gridControl13.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Members Linked to an Allocated Device to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team Member Linked to an Allocated Device" : Convert.ToString(intRowHandles.Length) + " Team Members Linked to an Allocated Device") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Team Member Linked to an Allocated Device" : "these Team Members Linked to an Allocated Device") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AllocatedPdaLinkedTeamMemberID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_EPTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_EPTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp03002_EP_Client_Delete("team_member_linked_to_allocated_pda", strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords2();

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }

                case 14:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl14.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Visit Types to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Visit Type" : Convert.ToString(intRowHandles.Length) + " Visit Types") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Visit Type" : "these Linked Visit Types") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitTypePersonID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("person_visit_type", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 15:
                    {
                        if (!iBool_AllowDeleteTraining) return;
                        view = (GridView)gridControlTraining.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Qualifications to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team Qualification" : Convert.ToString(intRowHandles.Length) + " Team Qualifications") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Team Qualification" : "these Team Qualifications") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "QualificationID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("qualification", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 16:
                    {
                        if (!iBool_AllowDeleteTraining) return;
                        view = (GridView)gridControlTrainingTeam.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Member Qualifications to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team Member Qualification" : Convert.ToString(intRowHandles.Length) + " Team Member Qualifications") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Team Member Qualification" : "these Team Member Qualifications") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "QualificationID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("qualification", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data_Team_Member_Training();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 17:
                    {
                        view = (GridView)gridControl17.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Insurance records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team Insurance record" : Convert.ToString(intRowHandles.Length) + " Team Insurance records") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Team Insurance record" : "these Team Insurance records") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                sb.Append(view.GetRowCellDisplayText(intRowHandle, "InsuranceID").ToString() + ",");
                            }

                            using (var RemoveRecords = new WoodPlanDataSetTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp00246_Team_Insurance_Delete(sb.ToString());  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 18:
                    {
                        view = (GridView)gridControlInsuranceCategory.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Insurance Category records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Team Insurance Category record" : Convert.ToString(intRowHandles.Length) + " Team Insurance Category records") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Team Insurance Category record" : "these Team Insurance Category records") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                sb.Append(view.GetRowCellDisplayText(intRowHandle, "TeamInsuranceCategoryID").ToString() + ",");
                            }

                            using (var RemoveRecords = new WoodPlanDataSetTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp00256_Core_Team_Insurance_Category_Delete(sb.ToString());  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 25:  // Linked Documents //
                    {
                        if (!iBool_AllowDeleteLinkedDocument) return;
                        view = (GridView)gridControl25.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Deleting...");
                            fProgress.Show();
                            Application.DoEvents();

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                            LoadLinkedRecords(GetSelectedParentIDs());

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 30:  // Person Responsibility //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Person Responsibilities to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Person Responsibility" : Convert.ToString(intRowHandles.Length) + " Linked Person Responsibilities") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Person Responsibility" : "these Linked Person Responsibilities") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                sb.Append(view.GetRowCellDisplayText(intRowHandle, "PersonResponsibilityID").ToString() + ",");
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("person_responsibility", sb.ToString());  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords_Person_Responsibilities(GetSelectedParentIDs());

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            StringBuilder sb = new StringBuilder();
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ContractorID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_Core_Contractor_Edit fChildForm = new frm_Core_Contractor_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 14:
                    {
                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VisitTypePersonID")) + ',';
                        }
                        var fChildForm = new frm_OM_Visit_Type_Person_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intLinkedtoPersonTypeID = 1;  // 0 = Staff, 1 = Contractor //
                        fChildForm.intRecordTypeID = 1;  // 1 = People (Staff \ contractor), 2 = Master Job //
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 15:
                    {
                        if (!iBool_AllowVisibleTraining) return;
                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }
                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 1;  // 1 = Team, 2 = Team Member //
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 16:
                    {
                        if (!iBool_AllowVisibleTraining) return;
                        view = (GridView)gridControlTrainingTeam.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }
                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 2;  // 1 = Team, 2 = Team Member //
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 17:
                    {
                        view = (GridView)gridControl17.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "InsuranceID").ToString() + ",");
                        }
                        var fChildForm = new frm_Core_Contractor_Insurance_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 18:  //Team insurance Category
                    {
                        view = (GridView)gridControlInsuranceCategory.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "TeamInsuranceCategoryID").ToString() + ",");
                        }
                        var fChildForm = new frm_Core_Contractor_Insurance_Category_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 30:  // Person Responsibility //
                    {
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellDisplayText(intRowHandle, "PersonResponsibilityID").ToString() + ",");
                        }
                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = sb.ToString();
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "51,52,53,54,55,56,57,58,59";
                        fChildForm.i_intPassedInRecordTypeID = 4;  // Team //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }


        private void comboBoxEditLinkedRecordType_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBoxEdit cbe = (ComboBoxEdit)sender;
            switch (cbe.Text)
            {
                case "No Linked Records":
                    intLinkedRecordType = 1;
                    popupContainerEdit2.Properties.ReadOnly = true;
                    break;
                case "Gritting Callouts":
                    intLinkedRecordType = 2;
                    popupContainerEdit2.Properties.ReadOnly = false;
                    break;
                case "Snow Clearance Callouts":
                    intLinkedRecordType = 3;
                    popupContainerEdit2.Properties.ReadOnly = false;
                    break;
                case "Maintenance Visits":
                    intLinkedRecordType = 4;
                    popupContainerEdit2.Properties.ReadOnly = false;
                    break;
                case "Maintenance Jobs":
                    intLinkedRecordType = 5;
                    popupContainerEdit2.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
            strLinkedRecordType = cbe.Text;
            try
            {
                sp00249_Core_Record_Statuses_For_FilteringTableAdapter.Fill(woodPlanDataSet.sp00249_Core_Record_Statuses_For_Filtering, intLinkedRecordType);
            }
            catch (Exception) { }
            selection2.ClearSelection();
            popupContainerEdit2.Text = PopupContainerEdit2_Get_Selected();
        }

        private void dateEdit1_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Invoice Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEdit2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nWARNING: Loading Invoice Data without a date filter may return a lot of data!\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        private void btnReloadInvoices_Click(object sender, EventArgs e)
        {
            Load_Invoices();
        }

        private void Load_Invoices()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ContractorID"])) + ',';
            }
            
            DateTime dtFromDate = dateEdit1.DateTime;
            DateTime dtToDate = dateEdit2.DateTime;
            view = (GridView)gridControl10.MainView;
            this.RefreshGridViewState8.SaveViewInfo();  // Store expanded groups and selected rows //
            view.BeginUpdate();
            try
            {
                sp04131_GC_Team_Self_Billing_InvoicesTableAdapter.Fill(dataSet_GC_Core.sp04131_GC_Team_Self_Billing_Invoices, strSelectedIDs, dtFromDate, dtToDate);
                this.RefreshGridViewState8.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
                view.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Self-Billing Invoice list.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            view.EndUpdate();
        }

        private void bbiRecalculateStock_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (i_int_FocusedGrid != 6) return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            view = (GridView)gridControl8.MainView;
            intRowHandles = view.GetSelectedRows();
            string strMessage = "";
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Gritting Info record to recalculate stock levels for by clicking on them then try again.", "Recalculate Stock Levels", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Team Gritting Info record" : Convert.ToString(intRowHandles.Length) + " Team Gritting Info records") + " selected for recalculating the stock levels.\n\nProceed?";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Recalculating...");
                fProgress.Show();
                Application.DoEvents();

                int intRecordID = 0;
                int intUpdateProgressThreshhold = intRowHandles.Length / 10;
                int intUpdateProgressTempCount = 0;
                DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter UpdateRecords = new DataSet_GC_DataEntryTableAdapters.QueriesTableAdapter();
                UpdateRecords.ChangeConnectionString(strConnectionString);
                foreach (int intRowHandle in intRowHandles)
                {
                    intRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SubContractorID"));
                    UpdateRecords.sp04232_GC_Stock_Update_All_Totals(intRecordID, 1);  // 0 = Depot, 1 = Team //

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                LoadLinkedRecords(GetSelectedParentIDs());

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) recalculated.", "Recalculate Stock Levels", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }






    }
}


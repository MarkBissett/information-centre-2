namespace WoodPlan5
{
    partial class frm_Core_Contractor_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Contractor_Manager));
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.popupContainerEdit1 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlShowTabPages = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp04022CoreDummyTabPageListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTabPageName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.btnOK2 = new DevExpress.XtraEditors.SimpleButton();
            this.LoadContractorsBtn = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlLinkedGrittingCalloutsFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp00249CoreRecordStatusesForFilteringBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnGritCalloutFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEditLinkedRecordType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp00180ContractorManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultContactID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultContactEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUser3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsSpecialist = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsGritter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colIsWoodPlanVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedRecordCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colIsSnowClearer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsVatRegistered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsUtilityArbTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsOperationsTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSupplierCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLegalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUniqueTaxRef = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsConstruction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsFencing = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsPestControl = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsRailArb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsRoofing = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsWindowCleaning = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsPotholeTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingPublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceEmployersInsuranceValid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaintenancePublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaintenanceEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConstructionPublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConstructionEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmenityArbPublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmenityArbEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityArbPublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityArbEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRailArbPublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRailArbEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPestControlPublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPestControlEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFencingPublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFencingEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoofingPublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoofingEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWindowCleaningPublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWindowCleaningEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPotholePublicInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPotholeEmployersInsuranceValid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActiveTeamMembers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colOMPaidDiscountedVisitRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJoinedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApprovedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTerminationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoSelfBilling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp00181ContractorManagerContactsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colContactID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRecordDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditBoolean = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDefaultContact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditNumeric = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colIsSelfBillingEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsSnowMobileTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsActiveTotalViewUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalViewUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsOMSelfBillingEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp00182ContractorManagerCertificatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCertificateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCertificateTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCertificateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCertificatePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTabPage11 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer11 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlTraining = new DevExpress.XtraGrid.GridControl();
            this.sp09119HRQualificationsForContractorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridViewTraining = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQualificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationFromID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostToCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime12 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSummerMaintenance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colWinterMaintenance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityArb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityRail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysUntilExpiry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedDocumentCount7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colQualificationSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArchived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssessmentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControlTeamMembers = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp04023GCSubContractorTeamMembersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTeamMemberID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFax1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colGender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGenderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDAUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDAPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDALoginToken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritJobAllocateByDefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNatInsuranceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer12 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlTrainingTeam = new DevExpress.XtraGrid.GridControl();
            this.sp09120HRQualificationsForTeamMemberBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewTrainingTeam = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPagePersonResponsibilities = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlResponsibility = new DevExpress.XtraGrid.GridControl();
            this.sp01020CoreTeamManagerLinkedResponsiblePeopleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewResponsibility = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPersonResponsibilityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colResponsibilityType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibilityTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleForRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleForRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditHTML16 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPage8 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer5 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl11 = new DevExpress.XtraGrid.GridControl();
            this.sp04152GCHolidaysLinkedToTeamBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSubContractorHolidayID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamNotAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTeamName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colHolidayTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageInsurance = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl17 = new DevExpress.XtraGrid.GridControl();
            this.sp00245CoreTeamInsuranceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView17 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInsuranceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedCertificate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditInsurance = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colGrittingPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colGrittingEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearancePublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaintenancePublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaintenanceEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConstructionPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConstructionEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmenityArbPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmenityArbEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityArbPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityArbEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRailArbPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRailArbEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPestControlPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPestControlEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFencingPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFencingEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoofingPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoofingEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWindowCleaningPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWindowCleaningEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPotholePublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPotholeEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinGrittingPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinGrittingEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinSnowClearancePublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinSnowClearanceEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinMaintenancePublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinMaintenanceEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinConstructionPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinConstructionEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinAmenityArbPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinAmenityArbEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinUtilityArbPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinUtilityArbEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinRailArbPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinRailArbEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinPestControlPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinPestControlEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinFencingPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinFencingEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinRoofingPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinRoofingEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinWindowCleaningPublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinWindowCleaningEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinPotholePublicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinPotholeEmployersValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageInsuranceCategories = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlInsuranceCategory = new DevExpress.XtraGrid.GridControl();
            this.sp00250CoreTeamInsuranceCategoryListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewInsuranceCategory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTeamInsuranceCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInsuranceCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategoryStateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCategoryState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer6 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp04027GCSubContractorAllocatedPDAsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAllocatedPdaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaMake = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaFirmwareVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaMobileTelNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaDatePurchased = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoftwareVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAllocated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPDAUserName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDAPassword1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDALoginToken1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDACode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDAVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl13 = new DevExpress.XtraGrid.GridControl();
            this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAllocatedPdaLinkedTeamMemberID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocatedPDAID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamMemberID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAllocated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaMake1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaModel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaSerialNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaFirmwareVersion1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaMobileTelNo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaDatePurchased1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoftwareVersion1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDACode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubcontractorName5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer7 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp04032GCTeamGrittingInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSubContractorGritInformationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colHoldsStock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit25kgBags = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colWarningGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUrgentGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStockSuppliedByDepotID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellBillingInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingFrequencyDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsDirectLabour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSubContractorName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotFax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colGritDepotWebSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotCurrentGrittingLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotWarningGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritDepotUrgentGritLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingFrequencyDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHourlyProactiveRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHourlyReactiveRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritJobTransferMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGritMobileTelephoneNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartingGritAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferredInGritAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferredOutGritAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderedInGritAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalloutUsedGritAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMissingJobSheetDeliveryMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMissingJobSheetDeliveryMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAutoAllocateGritJobs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.btnReloadInvoices = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridSplitContainer8 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp04131GCTeamSelfBillingInvoicesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGrittingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedCalloutCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer9 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.sp04082GCSubContractorSentTextMessagesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSentTextMessageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeSent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextMessageTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextMessageTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSentToNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage9 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer10 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl12 = new DevExpress.XtraGrid.GridControl();
            this.sp04156GCSnowClearanceTeamRatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Snow_Core = new WoodPlan5.DataSet_GC_Snow_Core();
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSnowClearanceSubContractorRateRule = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDayTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDayTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDayTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMinimumHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineRate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumHours2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineRate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumHours3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMachineRate4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumHours4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage10 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl14 = new DevExpress.XtraGrid.GridControl();
            this.sp06309PersonVisitTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Core = new WoodPlan5.DataSet_OM_Core();
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisitTypePersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageLinkedDocuments = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl25 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView25 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDocumentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp00180_Contractor_ManagerTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00180_Contractor_ManagerTableAdapter();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00181_Contractor_Manager_ContactsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00181_Contractor_Manager_ContactsTableAdapter();
            this.sp00182_Contractor_Manager_CertificatesTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00182_Contractor_Manager_CertificatesTableAdapter();
            this.sp04022_Core_Dummy_TabPageListTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04022_Core_Dummy_TabPageListTableAdapter();
            this.sp04023_GC_SubContractor_Team_MembersTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04023_GC_SubContractor_Team_MembersTableAdapter();
            this.sp04027_GC_SubContractor_Allocated_PDAsTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04027_GC_SubContractor_Allocated_PDAsTableAdapter();
            this.sp04032_GC_Team_Gritting_InfoTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04032_GC_Team_Gritting_InfoTableAdapter();
            this.sp04082_GC_SubContractor_Sent_Text_MessagesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04082_GC_SubContractor_Sent_Text_MessagesTableAdapter();
            this.sp04131_GC_Team_Self_Billing_InvoicesTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04131_GC_Team_Self_Billing_InvoicesTableAdapter();
            this.sp04152_GC_Holidays_Linked_To_TeamTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04152_GC_Holidays_Linked_To_TeamTableAdapter();
            this.sp04156_GC_Snow_Clearance_Team_RatesTableAdapter = new WoodPlan5.DataSet_GC_Snow_CoreTableAdapters.sp04156_GC_Snow_Clearance_Team_RatesTableAdapter();
            this.bbiRecalculateStock = new DevExpress.XtraBars.BarButtonItem();
            this.sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter();
            this.sp06309_Person_Visit_TypesTableAdapter = new WoodPlan5.DataSet_OM_CoreTableAdapters.sp06309_Person_Visit_TypesTableAdapter();
            this.sp09119_HR_Qualifications_For_ContractorTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp09119_HR_Qualifications_For_ContractorTableAdapter();
            this.sp09120_HR_Qualifications_For_Team_MemberTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp09120_HR_Qualifications_For_Team_MemberTableAdapter();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp00245_Core_Team_InsuranceTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00245_Core_Team_InsuranceTableAdapter();
            this.sp00249_Core_Record_Statuses_For_FilteringTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00249_Core_Record_Statuses_For_FilteringTableAdapter();
            this.sp01020_Core_Team_Manager_Linked_Responsible_PeopleTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01020_Core_Team_Manager_Linked_Responsible_PeopleTableAdapter();
            this.dataSet_OM_Visit = new WoodPlan5.DataSet_OM_Visit();
            this.sp00250_Core_Team_Insurance_Category_ListTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00250_Core_Team_Insurance_Category_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlShowTabPages)).BeginInit();
            this.popupContainerControlShowTabPages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04022CoreDummyTabPageListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).BeginInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00249CoreRecordStatusesForFilteringBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditLinkedRecordType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00180ContractorManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00181ContractorManagerContactsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditBoolean)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditNumeric)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00182ContractorManagerCertificatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.xtraTabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer11)).BeginInit();
            this.gridSplitContainer11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTraining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09119HRQualificationsForContractorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTraining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsTraining)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlTeamMembers)).BeginInit();
            this.splitContainerControlTeamMembers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04023GCSubContractorTeamMembersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer12)).BeginInit();
            this.gridSplitContainer12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTrainingTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09120HRQualificationsForTeamMemberBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTrainingTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit7)).BeginInit();
            this.xtraTabPagePersonResponsibilities.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlResponsibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01020CoreTeamManagerLinkedResponsiblePeopleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewResponsibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML16)).BeginInit();
            this.xtraTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).BeginInit();
            this.gridSplitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04152GCHolidaysLinkedToTeamBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            this.xtraTabPageInsurance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00245CoreTeamInsuranceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditInsurance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency1)).BeginInit();
            this.xtraTabPageInsuranceCategories.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInsuranceCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00250CoreTeamInsuranceCategoryListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInsuranceCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit9)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).BeginInit();
            this.gridSplitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04027GCSubContractorAllocatedPDAsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).BeginInit();
            this.gridSplitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04032GCTeamGrittingInfoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25kgBags)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer8)).BeginInit();
            this.gridSplitContainer8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04131GCTeamSelfBillingInvoicesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).BeginInit();
            this.xtraTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer9)).BeginInit();
            this.gridSplitContainer9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04082GCSubContractorSentTextMessagesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            this.xtraTabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer10)).BeginInit();
            this.gridSplitContainer10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04156GCSnowClearanceTeamRatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            this.xtraTabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06309PersonVisitTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            this.xtraTabPageLinkedDocuments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRecalculateStock, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1270, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 691);
            this.barDockControlBottom.Size = new System.Drawing.Size(1270, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 691);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1270, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 691);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRecalculateStock});
            this.barManager1.MaxItemId = 28;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit14;
            this.colActive1.FieldName = "Active";
            this.colActive1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 2;
            this.colActive1.Width = 49;
            // 
            // repositoryItemCheckEdit14
            // 
            this.repositoryItemCheckEdit14.AutoHeight = false;
            this.repositoryItemCheckEdit14.Name = "repositoryItemCheckEdit14";
            this.repositoryItemCheckEdit14.ValueChecked = 1;
            this.repositoryItemCheckEdit14.ValueUnchecked = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Teams";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1270, 691);
            this.splitContainerControl1.SplitterPosition = 286;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControlShowTabPages);
            this.splitContainerControl2.Panel2.Controls.Add(this.popupContainerControlLinkedGrittingCalloutsFilter);
            this.splitContainerControl2.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1266, 375);
            this.splitContainerControl2.SplitterPosition = 30;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.popupContainerEdit1);
            this.layoutControl1.Controls.Add(this.LoadContractorsBtn);
            this.layoutControl1.Controls.Add(this.popupContainerEdit2);
            this.layoutControl1.Controls.Add(this.comboBoxEditLinkedRecordType);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(454, 215, 250, 350);
            this.layoutControl1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1266, 30);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.EditValue = "All Related Data";
            this.popupContainerEdit1.Location = new System.Drawing.Point(118, 4);
            this.popupContainerEdit1.MenuManager = this.barManager1;
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            this.popupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit1.Properties.PopupControl = this.popupContainerControlShowTabPages;
            this.popupContainerEdit1.Size = new System.Drawing.Size(341, 20);
            this.popupContainerEdit1.StyleController = this.layoutControl1;
            this.popupContainerEdit1.TabIndex = 0;
            this.popupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit1_QueryResultValue);
            // 
            // popupContainerControlShowTabPages
            // 
            this.popupContainerControlShowTabPages.Controls.Add(this.gridControl4);
            this.popupContainerControlShowTabPages.Controls.Add(this.btnOK2);
            this.popupContainerControlShowTabPages.Location = new System.Drawing.Point(58, 47);
            this.popupContainerControlShowTabPages.Name = "popupContainerControlShowTabPages";
            this.popupContainerControlShowTabPages.Size = new System.Drawing.Size(217, 245);
            this.popupContainerControlShowTabPages.TabIndex = 6;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp04022CoreDummyTabPageListBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 3);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit7});
            this.gridControl4.Size = new System.Drawing.Size(211, 215);
            this.gridControl4.TabIndex = 1;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp04022CoreDummyTabPageListBindingSource
            // 
            this.sp04022CoreDummyTabPageListBindingSource.DataMember = "sp04022_Core_Dummy_TabPageList";
            this.sp04022CoreDummyTabPageListBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTabPageName});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colTabPageName
            // 
            this.colTabPageName.Caption = "Linked Records";
            this.colTabPageName.FieldName = "TabPageName";
            this.colTabPageName.Name = "colTabPageName";
            this.colTabPageName.OptionsColumn.AllowEdit = false;
            this.colTabPageName.OptionsColumn.AllowFocus = false;
            this.colTabPageName.OptionsColumn.ReadOnly = true;
            this.colTabPageName.Visible = true;
            this.colTabPageName.VisibleIndex = 0;
            this.colTabPageName.Width = 179;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Caption = "Check";
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            // 
            // btnOK2
            // 
            this.btnOK2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK2.Location = new System.Drawing.Point(3, 220);
            this.btnOK2.Name = "btnOK2";
            this.btnOK2.Size = new System.Drawing.Size(75, 23);
            this.btnOK2.TabIndex = 0;
            this.btnOK2.Text = "OK";
            this.btnOK2.Click += new System.EventHandler(this.btnOK2_Click);
            // 
            // LoadContractorsBtn
            // 
            this.LoadContractorsBtn.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.LoadContractorsBtn.Location = new System.Drawing.Point(1183, 4);
            this.LoadContractorsBtn.Name = "LoadContractorsBtn";
            this.LoadContractorsBtn.Size = new System.Drawing.Size(79, 22);
            this.LoadContractorsBtn.StyleController = this.layoutControl1;
            this.LoadContractorsBtn.TabIndex = 4;
            this.LoadContractorsBtn.Text = "Refresh";
            this.LoadContractorsBtn.Click += new System.EventHandler(this.LoadContractorsBtn_Click);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.EditValue = "No Callout Status Filter";
            this.popupContainerEdit2.Location = new System.Drawing.Point(745, 4);
            this.popupContainerEdit2.MenuManager = this.barManager1;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            this.popupContainerEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit2.Properties.PopupControl = this.popupContainerControlLinkedGrittingCalloutsFilter;
            this.popupContainerEdit2.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit2.Size = new System.Drawing.Size(434, 20);
            this.popupContainerEdit2.StyleController = this.layoutControl1;
            this.popupContainerEdit2.TabIndex = 3;
            this.popupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit2_QueryResultValue);
            // 
            // popupContainerControlLinkedGrittingCalloutsFilter
            // 
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.layoutControl2);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Controls.Add(this.btnGritCalloutFilterOK);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Location = new System.Drawing.Point(322, 47);
            this.popupContainerControlLinkedGrittingCalloutsFilter.Name = "popupContainerControlLinkedGrittingCalloutsFilter";
            this.popupContainerControlLinkedGrittingCalloutsFilter.Size = new System.Drawing.Size(340, 245);
            this.popupContainerControlLinkedGrittingCalloutsFilter.TabIndex = 5;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl2.Controls.Add(this.gridControl5);
            this.layoutControl2.Controls.Add(this.dateEditToDate);
            this.layoutControl2.Controls.Add(this.dateEditFromDate);
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1185, 135, 250, 350);
            this.layoutControl2.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignMode.AutoSize;
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(340, 220);
            this.layoutControl2.TabIndex = 12;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp00249CoreRecordStatusesForFilteringBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(12, 32);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(316, 152);
            this.gridControl5.TabIndex = 4;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp00249CoreRecordStatusesForFilteringBindingSource
            // 
            this.sp00249CoreRecordStatusesForFilteringBindingSource.DataMember = "sp00249_Core_Record_Statuses_For_Filtering";
            this.sp00249CoreRecordStatusesForFilteringBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colValue,
            this.colOrder});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Callout Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 263;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(216, 188);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all ca" +
    "llouts will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(112, 20);
            this.dateEditToDate.StyleController = this.layoutControl2;
            this.dateEditToDate.TabIndex = 11;
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(69, 188);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all ca" +
    "llouts will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(98, 20);
            this.dateEditFromDate.StyleController = this.layoutControl2;
            this.dateEditFromDate.TabIndex = 10;
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(340, 220);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Linked Gritting Callouts - Data Filter";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(336, 216);
            this.layoutControlGroup3.Text = "Linked Callouts - Data Filter";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl5;
            this.layoutControlItem3.CustomizationFormText = "Callout Job Status Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(320, 156);
            this.layoutControlItem3.Text = "Callout Job Status Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateEditFromDate;
            this.layoutControlItem4.CustomizationFormText = "From Date:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(159, 24);
            this.layoutControlItem4.Text = "From Date:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dateEditToDate;
            this.layoutControlItem5.CustomizationFormText = "To Date:";
            this.layoutControlItem5.Location = new System.Drawing.Point(159, 156);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem5.Text = "To Date:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(42, 13);
            // 
            // btnGritCalloutFilterOK
            // 
            this.btnGritCalloutFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGritCalloutFilterOK.Location = new System.Drawing.Point(3, 220);
            this.btnGritCalloutFilterOK.Name = "btnGritCalloutFilterOK";
            this.btnGritCalloutFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnGritCalloutFilterOK.TabIndex = 12;
            this.btnGritCalloutFilterOK.Text = "OK";
            this.btnGritCalloutFilterOK.Click += new System.EventHandler(this.btnGritCalloutFilterOK_Click);
            // 
            // comboBoxEditLinkedRecordType
            // 
            this.comboBoxEditLinkedRecordType.Location = new System.Drawing.Point(564, 4);
            this.comboBoxEditLinkedRecordType.MenuManager = this.barManager1;
            this.comboBoxEditLinkedRecordType.Name = "comboBoxEditLinkedRecordType";
            this.comboBoxEditLinkedRecordType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditLinkedRecordType.Properties.Items.AddRange(new object[] {
            "No Linked Records",
            "Gritting Callouts",
            "Snow Clearance Callouts",
            "Maintenance Visits",
            "Maintenance Jobs"});
            this.comboBoxEditLinkedRecordType.Size = new System.Drawing.Size(139, 20);
            this.comboBoxEditLinkedRecordType.StyleController = this.layoutControl1;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem3.Text = "Linked Record Type - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I control the values calculated and shown in the Linked Records hyperlink column " +
    "of the Site grid.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.comboBoxEditLinkedRecordType.SuperTip = superToolTip3;
            this.comboBoxEditLinkedRecordType.TabIndex = 2;
            this.comboBoxEditLinkedRecordType.SelectedValueChanged += new System.EventHandler(this.comboBoxEditLinkedRecordType_SelectedValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1266, 30);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.comboBoxEditLinkedRecordType;
            this.layoutControlItem1.CustomizationFormText = "Linked Record Type:";
            this.layoutControlItem1.Location = new System.Drawing.Point(459, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(244, 26);
            this.layoutControlItem1.Text = "Linked Record Type:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.popupContainerEdit2;
            this.layoutControlItem2.CustomizationFormText = "Callout Status:";
            this.layoutControlItem2.Location = new System.Drawing.Point(703, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(476, 26);
            this.layoutControlItem2.Text = "Status:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(35, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.LoadContractorsBtn;
            this.layoutControlItem6.CustomizationFormText = "Load Contractors Button";
            this.layoutControlItem6.Location = new System.Drawing.Point(1179, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(83, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(83, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.popupContainerEdit1;
            this.layoutControlItem7.CustomizationFormText = "Linked Data To Show:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(459, 26);
            this.layoutControlItem7.Text = "Related Data To Show:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(111, 13);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1266, 339);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp00180ContractorManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Send Push Notification", "push_notification")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemTextEditInteger,
            this.repositoryItemDateEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1266, 339);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp00180ContractorManagerBindingSource
            // 
            this.sp00180ContractorManagerBindingSource.DataMember = "sp00180_Contractor_Manager";
            this.sp00180ContractorManagerBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("grid_16x16.png", "images/grid/grid_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/grid/grid_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "grid_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.linked_documents_16_16, "linked_documents_16_16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "linked_documents_16_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            this.imageCollection1.InsertGalleryImage("previouscomment_16x16.png", "images/comments/previouscomment_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/comments/previouscomment_16x16.png"), 7);
            this.imageCollection1.Images.SetKeyName(7, "previouscomment_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colContractorID,
            this.colContractorCode,
            this.colContractorTypeID,
            this.colContractorType,
            this.colContractorName,
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colPostcode,
            this.colTelephone1,
            this.colTelephone2,
            this.colMobile,
            this.colEmailPassword,
            this.colWebsite,
            this.colDefaultContactID,
            this.colDefaultContactEmail,
            this.colVatReg,
            this.colUser1,
            this.colUser2,
            this.colUser3,
            this.colRemarks,
            this.colDisabled,
            this.colInternalContractor,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.colIsSpecialist,
            this.colIsGritter,
            this.colIsWoodPlanVisible,
            this.colLatitude,
            this.colLongitude,
            this.colTextNumber,
            this.colLinkedRecordCount,
            this.colIsSnowClearer,
            this.colIsVatRegistered,
            this.colIsUtilityArbTeam,
            this.colIsOperationsTeam,
            this.colSupplierCode,
            this.colBusinessAreaName,
            this.colLegalStatus,
            this.colUniqueTaxRef,
            this.colIsConstruction,
            this.colIsFencing,
            this.colIsPestControl,
            this.colIsRailArb,
            this.colIsRoofing,
            this.colIsWindowCleaning,
            this.colIsPotholeTeam,
            this.colGrittingPublicInsuranceValid,
            this.colGrittingEmployersInsuranceValid,
            this.colSnowClearanceEmployersInsuranceValid1,
            this.colSnowClearanceEmployersInsuranceValid,
            this.colMaintenancePublicInsuranceValid,
            this.colMaintenanceEmployersInsuranceValid,
            this.colConstructionPublicInsuranceValid,
            this.colConstructionEmployersInsuranceValid,
            this.colAmenityArbPublicInsuranceValid,
            this.colAmenityArbEmployersInsuranceValid,
            this.colUtilityArbPublicInsuranceValid,
            this.colUtilityArbEmployersInsuranceValid,
            this.colRailArbPublicInsuranceValid,
            this.colRailArbEmployersInsuranceValid,
            this.colPestControlPublicInsuranceValid,
            this.colPestControlEmployersInsuranceValid,
            this.colFencingPublicInsuranceValid,
            this.colFencingEmployersInsuranceValid,
            this.colRoofingPublicInsuranceValid,
            this.colRoofingEmployersInsuranceValid,
            this.colWindowCleaningPublicInsuranceValid,
            this.colWindowCleaningEmployersInsuranceValid,
            this.colPotholePublicInsuranceValid,
            this.colPotholeEmployersInsuranceValid,
            this.colRegionID,
            this.colRegionName,
            this.colActiveTeamMembers,
            this.colOMPaidDiscountedVisitRate,
            this.colIsTest,
            this.colJoinedDate,
            this.colApprovedDate,
            this.colTerminationDate,
            this.colNoSelfBilling});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIsRoofing, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Visible = true;
            this.colContractorCode.VisibleIndex = 1;
            this.colContractorCode.Width = 111;
            // 
            // colContractorTypeID
            // 
            this.colContractorTypeID.Caption = "Type ID";
            this.colContractorTypeID.FieldName = "ContractorTypeID";
            this.colContractorTypeID.Name = "colContractorTypeID";
            this.colContractorTypeID.OptionsColumn.AllowEdit = false;
            this.colContractorTypeID.OptionsColumn.AllowFocus = false;
            this.colContractorTypeID.OptionsColumn.ReadOnly = true;
            this.colContractorTypeID.Width = 49;
            // 
            // colContractorType
            // 
            this.colContractorType.Caption = "Type";
            this.colContractorType.FieldName = "ContractorType";
            this.colContractorType.Name = "colContractorType";
            this.colContractorType.OptionsColumn.AllowEdit = false;
            this.colContractorType.OptionsColumn.AllowFocus = false;
            this.colContractorType.OptionsColumn.ReadOnly = true;
            this.colContractorType.Visible = true;
            this.colContractorType.VisibleIndex = 5;
            this.colContractorType.Width = 100;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 280;
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 27;
            this.colAddressLine1.Width = 181;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 180;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 181;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 180;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 188;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Visible = true;
            this.colTelephone1.VisibleIndex = 28;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Visible = true;
            this.colTelephone2.VisibleIndex = 29;
            this.colTelephone2.Width = 80;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Visible = true;
            this.colMobile.VisibleIndex = 30;
            this.colMobile.Width = 82;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Width = 94;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Visible = true;
            this.colWebsite.VisibleIndex = 33;
            this.colWebsite.Width = 133;
            // 
            // colDefaultContactID
            // 
            this.colDefaultContactID.FieldName = "DefaultContactID";
            this.colDefaultContactID.Name = "colDefaultContactID";
            this.colDefaultContactID.OptionsColumn.AllowEdit = false;
            this.colDefaultContactID.OptionsColumn.AllowFocus = false;
            this.colDefaultContactID.OptionsColumn.ReadOnly = true;
            // 
            // colDefaultContactEmail
            // 
            this.colDefaultContactEmail.Caption = "Default Contact Email";
            this.colDefaultContactEmail.FieldName = "DefaultContactEmail";
            this.colDefaultContactEmail.Name = "colDefaultContactEmail";
            this.colDefaultContactEmail.OptionsColumn.AllowEdit = false;
            this.colDefaultContactEmail.OptionsColumn.AllowFocus = false;
            this.colDefaultContactEmail.OptionsColumn.ReadOnly = true;
            this.colDefaultContactEmail.Visible = true;
            this.colDefaultContactEmail.VisibleIndex = 32;
            this.colDefaultContactEmail.Width = 116;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "Vat Reg No";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Visible = true;
            this.colVatReg.VisibleIndex = 25;
            // 
            // colUser1
            // 
            this.colUser1.Caption = "User Defined 1";
            this.colUser1.FieldName = "User1";
            this.colUser1.Name = "colUser1";
            this.colUser1.OptionsColumn.AllowEdit = false;
            this.colUser1.OptionsColumn.AllowFocus = false;
            this.colUser1.OptionsColumn.ReadOnly = true;
            this.colUser1.Visible = true;
            this.colUser1.VisibleIndex = 34;
            this.colUser1.Width = 92;
            // 
            // colUser2
            // 
            this.colUser2.Caption = "User Defined 2";
            this.colUser2.FieldName = "User2";
            this.colUser2.Name = "colUser2";
            this.colUser2.OptionsColumn.AllowEdit = false;
            this.colUser2.OptionsColumn.AllowFocus = false;
            this.colUser2.OptionsColumn.ReadOnly = true;
            this.colUser2.Visible = true;
            this.colUser2.VisibleIndex = 35;
            this.colUser2.Width = 92;
            // 
            // colUser3
            // 
            this.colUser3.Caption = "User Defined 3";
            this.colUser3.FieldName = "User3";
            this.colUser3.Name = "colUser3";
            this.colUser3.OptionsColumn.AllowEdit = false;
            this.colUser3.OptionsColumn.AllowFocus = false;
            this.colUser3.OptionsColumn.ReadOnly = true;
            this.colUser3.Visible = true;
            this.colUser3.VisibleIndex = 36;
            this.colUser3.Width = 92;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 37;
            this.colRemarks.Width = 138;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor";
            this.colInternalContractor.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Visible = true;
            this.colInternalContractor.VisibleIndex = 2;
            this.colInternalContractor.Width = 114;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "<b>Calculated 1</b>";
            this.gridColumn1.FieldName = "Calculated1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.ShowUnboundExpressionMenu = true;
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 38;
            this.gridColumn1.Width = 90;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "<b>Calculated 2</b>";
            this.gridColumn2.FieldName = "gridColumn2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.ShowUnboundExpressionMenu = true;
            this.gridColumn2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 39;
            this.gridColumn2.Width = 90;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "<b>Calculated 3</b>";
            this.gridColumn3.FieldName = "gridColumn3";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.ShowUnboundExpressionMenu = true;
            this.gridColumn3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 40;
            this.gridColumn3.Width = 90;
            // 
            // colIsSpecialist
            // 
            this.colIsSpecialist.Caption = "<b>Specialist Contractor</b>";
            this.colIsSpecialist.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsSpecialist.FieldName = "IsSpecialist";
            this.colIsSpecialist.Name = "colIsSpecialist";
            this.colIsSpecialist.OptionsColumn.AllowEdit = false;
            this.colIsSpecialist.OptionsColumn.AllowFocus = false;
            this.colIsSpecialist.OptionsColumn.ReadOnly = true;
            this.colIsSpecialist.Visible = true;
            this.colIsSpecialist.VisibleIndex = 9;
            this.colIsSpecialist.Width = 137;
            // 
            // colIsGritter
            // 
            this.colIsGritter.Caption = "Gritting";
            this.colIsGritter.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colIsGritter.FieldName = "IsGritter";
            this.colIsGritter.Name = "colIsGritter";
            this.colIsGritter.OptionsColumn.AllowEdit = false;
            this.colIsGritter.OptionsColumn.AllowFocus = false;
            this.colIsGritter.OptionsColumn.ReadOnly = true;
            this.colIsGritter.Visible = true;
            this.colIsGritter.VisibleIndex = 10;
            this.colIsGritter.Width = 54;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // colIsWoodPlanVisible
            // 
            this.colIsWoodPlanVisible.Caption = "Amenity Arb";
            this.colIsWoodPlanVisible.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colIsWoodPlanVisible.FieldName = "IsWoodPlanVisible";
            this.colIsWoodPlanVisible.Name = "colIsWoodPlanVisible";
            this.colIsWoodPlanVisible.OptionsColumn.AllowEdit = false;
            this.colIsWoodPlanVisible.OptionsColumn.AllowFocus = false;
            this.colIsWoodPlanVisible.OptionsColumn.ReadOnly = true;
            this.colIsWoodPlanVisible.Visible = true;
            this.colIsWoodPlanVisible.VisibleIndex = 14;
            this.colIsWoodPlanVisible.Width = 78;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Caption = "Check";
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            // 
            // colTextNumber
            // 
            this.colTextNumber.Caption = "Text Number";
            this.colTextNumber.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colTextNumber.FieldName = "TextNumber";
            this.colTextNumber.Name = "colTextNumber";
            this.colTextNumber.OptionsColumn.ReadOnly = true;
            this.colTextNumber.Visible = true;
            this.colTextNumber.VisibleIndex = 31;
            this.colTextNumber.Width = 113;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colLinkedRecordCount
            // 
            this.colLinkedRecordCount.Caption = "Linked Records";
            this.colLinkedRecordCount.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colLinkedRecordCount.FieldName = "LinkedRecordCount";
            this.colLinkedRecordCount.Name = "colLinkedRecordCount";
            this.colLinkedRecordCount.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordCount.Visible = true;
            this.colLinkedRecordCount.VisibleIndex = 43;
            this.colLinkedRecordCount.Width = 93;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colIsSnowClearer
            // 
            this.colIsSnowClearer.Caption = "Snow";
            this.colIsSnowClearer.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colIsSnowClearer.FieldName = "IsSnowClearer";
            this.colIsSnowClearer.Name = "colIsSnowClearer";
            this.colIsSnowClearer.OptionsColumn.AllowEdit = false;
            this.colIsSnowClearer.OptionsColumn.AllowFocus = false;
            this.colIsSnowClearer.OptionsColumn.ReadOnly = true;
            this.colIsSnowClearer.Visible = true;
            this.colIsSnowClearer.VisibleIndex = 11;
            this.colIsSnowClearer.Width = 45;
            // 
            // colIsVatRegistered
            // 
            this.colIsVatRegistered.Caption = "Is VAT Registered";
            this.colIsVatRegistered.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colIsVatRegistered.FieldName = "IsVatRegistered";
            this.colIsVatRegistered.Name = "colIsVatRegistered";
            this.colIsVatRegistered.OptionsColumn.AllowEdit = false;
            this.colIsVatRegistered.OptionsColumn.AllowFocus = false;
            this.colIsVatRegistered.OptionsColumn.ReadOnly = true;
            this.colIsVatRegistered.Visible = true;
            this.colIsVatRegistered.VisibleIndex = 24;
            this.colIsVatRegistered.Width = 107;
            // 
            // colIsUtilityArbTeam
            // 
            this.colIsUtilityArbTeam.Caption = "Utility Arb";
            this.colIsUtilityArbTeam.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsUtilityArbTeam.FieldName = "IsUtilityArbTeam";
            this.colIsUtilityArbTeam.Name = "colIsUtilityArbTeam";
            this.colIsUtilityArbTeam.OptionsColumn.AllowEdit = false;
            this.colIsUtilityArbTeam.OptionsColumn.AllowFocus = false;
            this.colIsUtilityArbTeam.OptionsColumn.ReadOnly = true;
            this.colIsUtilityArbTeam.Visible = true;
            this.colIsUtilityArbTeam.VisibleIndex = 15;
            this.colIsUtilityArbTeam.Width = 66;
            // 
            // colIsOperationsTeam
            // 
            this.colIsOperationsTeam.Caption = "Maintenance";
            this.colIsOperationsTeam.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsOperationsTeam.FieldName = "IsOperationsTeam";
            this.colIsOperationsTeam.Name = "colIsOperationsTeam";
            this.colIsOperationsTeam.OptionsColumn.AllowEdit = false;
            this.colIsOperationsTeam.OptionsColumn.AllowFocus = false;
            this.colIsOperationsTeam.OptionsColumn.ReadOnly = true;
            this.colIsOperationsTeam.Visible = true;
            this.colIsOperationsTeam.VisibleIndex = 12;
            this.colIsOperationsTeam.Width = 80;
            // 
            // colSupplierCode
            // 
            this.colSupplierCode.Caption = "Supplier Code";
            this.colSupplierCode.FieldName = "SupplierCode";
            this.colSupplierCode.Name = "colSupplierCode";
            this.colSupplierCode.OptionsColumn.AllowEdit = false;
            this.colSupplierCode.OptionsColumn.AllowFocus = false;
            this.colSupplierCode.OptionsColumn.ReadOnly = true;
            this.colSupplierCode.Visible = true;
            this.colSupplierCode.VisibleIndex = 23;
            this.colSupplierCode.Width = 85;
            // 
            // colBusinessAreaName
            // 
            this.colBusinessAreaName.Caption = "Business Area";
            this.colBusinessAreaName.FieldName = "BusinessAreaName";
            this.colBusinessAreaName.Name = "colBusinessAreaName";
            this.colBusinessAreaName.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName.Visible = true;
            this.colBusinessAreaName.VisibleIndex = 6;
            this.colBusinessAreaName.Width = 109;
            // 
            // colLegalStatus
            // 
            this.colLegalStatus.Caption = "Legal Status";
            this.colLegalStatus.FieldName = "LegalStatus";
            this.colLegalStatus.Name = "colLegalStatus";
            this.colLegalStatus.OptionsColumn.AllowEdit = false;
            this.colLegalStatus.OptionsColumn.AllowFocus = false;
            this.colLegalStatus.OptionsColumn.ReadOnly = true;
            this.colLegalStatus.Visible = true;
            this.colLegalStatus.VisibleIndex = 22;
            this.colLegalStatus.Width = 94;
            // 
            // colUniqueTaxRef
            // 
            this.colUniqueTaxRef.Caption = "Unique Tax Ref";
            this.colUniqueTaxRef.FieldName = "UniqueTaxRef";
            this.colUniqueTaxRef.Name = "colUniqueTaxRef";
            this.colUniqueTaxRef.OptionsColumn.AllowEdit = false;
            this.colUniqueTaxRef.OptionsColumn.AllowFocus = false;
            this.colUniqueTaxRef.OptionsColumn.ReadOnly = true;
            this.colUniqueTaxRef.Visible = true;
            this.colUniqueTaxRef.VisibleIndex = 26;
            this.colUniqueTaxRef.Width = 94;
            // 
            // colIsConstruction
            // 
            this.colIsConstruction.Caption = "Construction";
            this.colIsConstruction.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsConstruction.FieldName = "IsConstruction";
            this.colIsConstruction.Name = "colIsConstruction";
            this.colIsConstruction.OptionsColumn.AllowEdit = false;
            this.colIsConstruction.OptionsColumn.AllowFocus = false;
            this.colIsConstruction.OptionsColumn.ReadOnly = true;
            this.colIsConstruction.Visible = true;
            this.colIsConstruction.VisibleIndex = 13;
            this.colIsConstruction.Width = 80;
            // 
            // colIsFencing
            // 
            this.colIsFencing.Caption = "Fencing";
            this.colIsFencing.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsFencing.FieldName = "IsFencing";
            this.colIsFencing.Name = "colIsFencing";
            this.colIsFencing.OptionsColumn.AllowEdit = false;
            this.colIsFencing.OptionsColumn.AllowFocus = false;
            this.colIsFencing.OptionsColumn.ReadOnly = true;
            this.colIsFencing.Visible = true;
            this.colIsFencing.VisibleIndex = 18;
            this.colIsFencing.Width = 56;
            // 
            // colIsPestControl
            // 
            this.colIsPestControl.Caption = "Pest Control";
            this.colIsPestControl.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsPestControl.FieldName = "IsPestControl";
            this.colIsPestControl.Name = "colIsPestControl";
            this.colIsPestControl.OptionsColumn.AllowEdit = false;
            this.colIsPestControl.OptionsColumn.AllowFocus = false;
            this.colIsPestControl.OptionsColumn.ReadOnly = true;
            this.colIsPestControl.Visible = true;
            this.colIsPestControl.VisibleIndex = 17;
            this.colIsPestControl.Width = 78;
            // 
            // colIsRailArb
            // 
            this.colIsRailArb.Caption = "Rail Arb";
            this.colIsRailArb.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsRailArb.FieldName = "IsRailArb";
            this.colIsRailArb.Name = "colIsRailArb";
            this.colIsRailArb.OptionsColumn.AllowEdit = false;
            this.colIsRailArb.OptionsColumn.AllowFocus = false;
            this.colIsRailArb.OptionsColumn.ReadOnly = true;
            this.colIsRailArb.Visible = true;
            this.colIsRailArb.VisibleIndex = 16;
            this.colIsRailArb.Width = 56;
            // 
            // colIsRoofing
            // 
            this.colIsRoofing.Caption = "Roofing";
            this.colIsRoofing.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsRoofing.FieldName = "IsRoofing";
            this.colIsRoofing.Name = "colIsRoofing";
            this.colIsRoofing.OptionsColumn.AllowEdit = false;
            this.colIsRoofing.OptionsColumn.AllowFocus = false;
            this.colIsRoofing.OptionsColumn.ReadOnly = true;
            this.colIsRoofing.Visible = true;
            this.colIsRoofing.VisibleIndex = 19;
            this.colIsRoofing.Width = 69;
            // 
            // colIsWindowCleaning
            // 
            this.colIsWindowCleaning.Caption = "Window Cleaning";
            this.colIsWindowCleaning.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsWindowCleaning.FieldName = "IsWindowCleaning";
            this.colIsWindowCleaning.Name = "colIsWindowCleaning";
            this.colIsWindowCleaning.OptionsColumn.AllowEdit = false;
            this.colIsWindowCleaning.OptionsColumn.AllowFocus = false;
            this.colIsWindowCleaning.OptionsColumn.ReadOnly = true;
            this.colIsWindowCleaning.Visible = true;
            this.colIsWindowCleaning.VisibleIndex = 20;
            this.colIsWindowCleaning.Width = 101;
            // 
            // colIsPotholeTeam
            // 
            this.colIsPotholeTeam.Caption = "Pothole";
            this.colIsPotholeTeam.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsPotholeTeam.FieldName = "IsPotholeTeam";
            this.colIsPotholeTeam.Name = "colIsPotholeTeam";
            this.colIsPotholeTeam.OptionsColumn.AllowEdit = false;
            this.colIsPotholeTeam.OptionsColumn.AllowFocus = false;
            this.colIsPotholeTeam.OptionsColumn.ReadOnly = true;
            this.colIsPotholeTeam.Visible = true;
            this.colIsPotholeTeam.VisibleIndex = 21;
            // 
            // colGrittingPublicInsuranceValid
            // 
            this.colGrittingPublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colGrittingPublicInsuranceValid.FieldName = "GrittingPublicInsuranceValid";
            this.colGrittingPublicInsuranceValid.Name = "colGrittingPublicInsuranceValid";
            this.colGrittingPublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colGrittingPublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colGrittingPublicInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colGrittingPublicInsuranceValid.Width = 160;
            // 
            // colGrittingEmployersInsuranceValid
            // 
            this.colGrittingEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colGrittingEmployersInsuranceValid.FieldName = "GrittingEmployersInsuranceValid";
            this.colGrittingEmployersInsuranceValid.Name = "colGrittingEmployersInsuranceValid";
            this.colGrittingEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colGrittingEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colGrittingEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colGrittingEmployersInsuranceValid.Width = 182;
            // 
            // colSnowClearanceEmployersInsuranceValid1
            // 
            this.colSnowClearanceEmployersInsuranceValid1.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colSnowClearanceEmployersInsuranceValid1.FieldName = "SnowClearanceEmployersInsuranceValid1";
            this.colSnowClearanceEmployersInsuranceValid1.Name = "colSnowClearanceEmployersInsuranceValid1";
            this.colSnowClearanceEmployersInsuranceValid1.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceEmployersInsuranceValid1.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceEmployersInsuranceValid1.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceEmployersInsuranceValid1.Width = 230;
            // 
            // colSnowClearanceEmployersInsuranceValid
            // 
            this.colSnowClearanceEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colSnowClearanceEmployersInsuranceValid.FieldName = "SnowClearanceEmployersInsuranceValid";
            this.colSnowClearanceEmployersInsuranceValid.Name = "colSnowClearanceEmployersInsuranceValid";
            this.colSnowClearanceEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceEmployersInsuranceValid.Width = 224;
            // 
            // colMaintenancePublicInsuranceValid
            // 
            this.colMaintenancePublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colMaintenancePublicInsuranceValid.FieldName = "MaintenancePublicInsuranceValid";
            this.colMaintenancePublicInsuranceValid.Name = "colMaintenancePublicInsuranceValid";
            this.colMaintenancePublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colMaintenancePublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colMaintenancePublicInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colMaintenancePublicInsuranceValid.Width = 186;
            // 
            // colMaintenanceEmployersInsuranceValid
            // 
            this.colMaintenanceEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colMaintenanceEmployersInsuranceValid.FieldName = "MaintenanceEmployersInsuranceValid";
            this.colMaintenanceEmployersInsuranceValid.Name = "colMaintenanceEmployersInsuranceValid";
            this.colMaintenanceEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colMaintenanceEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colMaintenanceEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colMaintenanceEmployersInsuranceValid.Width = 208;
            // 
            // colConstructionPublicInsuranceValid
            // 
            this.colConstructionPublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colConstructionPublicInsuranceValid.FieldName = "ConstructionPublicInsuranceValid";
            this.colConstructionPublicInsuranceValid.Name = "colConstructionPublicInsuranceValid";
            this.colConstructionPublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colConstructionPublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colConstructionPublicInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colConstructionPublicInsuranceValid.Width = 186;
            // 
            // colConstructionEmployersInsuranceValid
            // 
            this.colConstructionEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colConstructionEmployersInsuranceValid.FieldName = "ConstructionEmployersInsuranceValid";
            this.colConstructionEmployersInsuranceValid.Name = "colConstructionEmployersInsuranceValid";
            this.colConstructionEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colConstructionEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colConstructionEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colConstructionEmployersInsuranceValid.Width = 208;
            // 
            // colAmenityArbPublicInsuranceValid
            // 
            this.colAmenityArbPublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colAmenityArbPublicInsuranceValid.FieldName = "AmenityArbPublicInsuranceValid";
            this.colAmenityArbPublicInsuranceValid.Name = "colAmenityArbPublicInsuranceValid";
            this.colAmenityArbPublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colAmenityArbPublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colAmenityArbPublicInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colAmenityArbPublicInsuranceValid.Width = 184;
            // 
            // colAmenityArbEmployersInsuranceValid
            // 
            this.colAmenityArbEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colAmenityArbEmployersInsuranceValid.FieldName = "AmenityArbEmployersInsuranceValid";
            this.colAmenityArbEmployersInsuranceValid.Name = "colAmenityArbEmployersInsuranceValid";
            this.colAmenityArbEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colAmenityArbEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colAmenityArbEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colAmenityArbEmployersInsuranceValid.Width = 206;
            // 
            // colUtilityArbPublicInsuranceValid
            // 
            this.colUtilityArbPublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colUtilityArbPublicInsuranceValid.FieldName = "UtilityArbPublicInsuranceValid";
            this.colUtilityArbPublicInsuranceValid.Name = "colUtilityArbPublicInsuranceValid";
            this.colUtilityArbPublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colUtilityArbPublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colUtilityArbPublicInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colUtilityArbPublicInsuranceValid.Width = 172;
            // 
            // colUtilityArbEmployersInsuranceValid
            // 
            this.colUtilityArbEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colUtilityArbEmployersInsuranceValid.FieldName = "UtilityArbEmployersInsuranceValid";
            this.colUtilityArbEmployersInsuranceValid.Name = "colUtilityArbEmployersInsuranceValid";
            this.colUtilityArbEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colUtilityArbEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colUtilityArbEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colUtilityArbEmployersInsuranceValid.Width = 194;
            // 
            // colRailArbPublicInsuranceValid
            // 
            this.colRailArbPublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colRailArbPublicInsuranceValid.FieldName = "RailArbPublicInsuranceValid";
            this.colRailArbPublicInsuranceValid.Name = "colRailArbPublicInsuranceValid";
            this.colRailArbPublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colRailArbPublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colRailArbPublicInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colRailArbPublicInsuranceValid.Width = 162;
            // 
            // colRailArbEmployersInsuranceValid
            // 
            this.colRailArbEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colRailArbEmployersInsuranceValid.FieldName = "RailArbEmployersInsuranceValid";
            this.colRailArbEmployersInsuranceValid.Name = "colRailArbEmployersInsuranceValid";
            this.colRailArbEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colRailArbEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colRailArbEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colRailArbEmployersInsuranceValid.Width = 184;
            // 
            // colPestControlPublicInsuranceValid
            // 
            this.colPestControlPublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colPestControlPublicInsuranceValid.FieldName = "PestControlPublicInsuranceValid";
            this.colPestControlPublicInsuranceValid.Name = "colPestControlPublicInsuranceValid";
            this.colPestControlPublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colPestControlPublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colPestControlPublicInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colPestControlPublicInsuranceValid.Width = 184;
            // 
            // colPestControlEmployersInsuranceValid
            // 
            this.colPestControlEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colPestControlEmployersInsuranceValid.FieldName = "PestControlEmployersInsuranceValid";
            this.colPestControlEmployersInsuranceValid.Name = "colPestControlEmployersInsuranceValid";
            this.colPestControlEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colPestControlEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colPestControlEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colPestControlEmployersInsuranceValid.Width = 206;
            // 
            // colFencingPublicInsuranceValid
            // 
            this.colFencingPublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colFencingPublicInsuranceValid.FieldName = "FencingPublicInsuranceValid";
            this.colFencingPublicInsuranceValid.Name = "colFencingPublicInsuranceValid";
            this.colFencingPublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colFencingPublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colFencingPublicInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colFencingPublicInsuranceValid.Width = 162;
            // 
            // colFencingEmployersInsuranceValid
            // 
            this.colFencingEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colFencingEmployersInsuranceValid.FieldName = "FencingEmployersInsuranceValid";
            this.colFencingEmployersInsuranceValid.Name = "colFencingEmployersInsuranceValid";
            this.colFencingEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colFencingEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colFencingEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colFencingEmployersInsuranceValid.Width = 184;
            // 
            // colRoofingPublicInsuranceValid
            // 
            this.colRoofingPublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colRoofingPublicInsuranceValid.FieldName = "RoofingPublicInsuranceValid";
            this.colRoofingPublicInsuranceValid.Name = "colRoofingPublicInsuranceValid";
            this.colRoofingPublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colRoofingPublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colRoofingPublicInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colRoofingPublicInsuranceValid.Width = 162;
            // 
            // colRoofingEmployersInsuranceValid
            // 
            this.colRoofingEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colRoofingEmployersInsuranceValid.FieldName = "RoofingEmployersInsuranceValid";
            this.colRoofingEmployersInsuranceValid.Name = "colRoofingEmployersInsuranceValid";
            this.colRoofingEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colRoofingEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colRoofingEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colRoofingEmployersInsuranceValid.Width = 184;
            // 
            // colWindowCleaningPublicInsuranceValid
            // 
            this.colWindowCleaningPublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colWindowCleaningPublicInsuranceValid.FieldName = "WindowCleaningPublicInsuranceValid";
            this.colWindowCleaningPublicInsuranceValid.Name = "colWindowCleaningPublicInsuranceValid";
            this.colWindowCleaningPublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colWindowCleaningPublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colWindowCleaningPublicInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colWindowCleaningPublicInsuranceValid.Width = 207;
            // 
            // colWindowCleaningEmployersInsuranceValid
            // 
            this.colWindowCleaningEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colWindowCleaningEmployersInsuranceValid.FieldName = "WindowCleaningEmployersInsuranceValid";
            this.colWindowCleaningEmployersInsuranceValid.Name = "colWindowCleaningEmployersInsuranceValid";
            this.colWindowCleaningEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colWindowCleaningEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colWindowCleaningEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            this.colWindowCleaningEmployersInsuranceValid.Width = 229;
            // 
            // colPotholePublicInsuranceValid
            // 
            this.colPotholePublicInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colPotholePublicInsuranceValid.FieldName = "PotholePublicInsuranceValid";
            this.colPotholePublicInsuranceValid.Name = "colPotholePublicInsuranceValid";
            this.colPotholePublicInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colPotholePublicInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colPotholePublicInsuranceValid.OptionsColumn.ReadOnly = true;
            // 
            // colPotholeEmployersInsuranceValid
            // 
            this.colPotholeEmployersInsuranceValid.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colPotholeEmployersInsuranceValid.FieldName = "PotholeEmployersInsuranceValid";
            this.colPotholeEmployersInsuranceValid.Name = "colPotholeEmployersInsuranceValid";
            this.colPotholeEmployersInsuranceValid.OptionsColumn.AllowEdit = false;
            this.colPotholeEmployersInsuranceValid.OptionsColumn.AllowFocus = false;
            this.colPotholeEmployersInsuranceValid.OptionsColumn.ReadOnly = true;
            // 
            // colRegionID
            // 
            this.colRegionID.Caption = "Region ID";
            this.colRegionID.FieldName = "RegionID";
            this.colRegionID.Name = "colRegionID";
            this.colRegionID.OptionsColumn.AllowEdit = false;
            this.colRegionID.OptionsColumn.AllowFocus = false;
            this.colRegionID.OptionsColumn.ReadOnly = true;
            this.colRegionID.Width = 66;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 7;
            this.colRegionName.Width = 115;
            // 
            // colActiveTeamMembers
            // 
            this.colActiveTeamMembers.Caption = "Active Team Members";
            this.colActiveTeamMembers.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colActiveTeamMembers.FieldName = "ActiveTeamMembers";
            this.colActiveTeamMembers.Name = "colActiveTeamMembers";
            this.colActiveTeamMembers.OptionsColumn.AllowEdit = false;
            this.colActiveTeamMembers.OptionsColumn.AllowFocus = false;
            this.colActiveTeamMembers.OptionsColumn.ReadOnly = true;
            this.colActiveTeamMembers.Visible = true;
            this.colActiveTeamMembers.VisibleIndex = 8;
            this.colActiveTeamMembers.Width = 124;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colOMPaidDiscountedVisitRate
            // 
            this.colOMPaidDiscountedVisitRate.Caption = "OM - Paid Discounted Visit Rate";
            this.colOMPaidDiscountedVisitRate.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colOMPaidDiscountedVisitRate.FieldName = "OMPaidDiscountedVisitRate";
            this.colOMPaidDiscountedVisitRate.Name = "colOMPaidDiscountedVisitRate";
            this.colOMPaidDiscountedVisitRate.OptionsColumn.AllowEdit = false;
            this.colOMPaidDiscountedVisitRate.OptionsColumn.AllowFocus = false;
            this.colOMPaidDiscountedVisitRate.OptionsColumn.ReadOnly = true;
            this.colOMPaidDiscountedVisitRate.Visible = true;
            this.colOMPaidDiscountedVisitRate.VisibleIndex = 41;
            this.colOMPaidDiscountedVisitRate.Width = 169;
            // 
            // colIsTest
            // 
            this.colIsTest.Caption = "Test Team";
            this.colIsTest.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colIsTest.FieldName = "IsTest";
            this.colIsTest.Name = "colIsTest";
            this.colIsTest.OptionsColumn.AllowEdit = false;
            this.colIsTest.OptionsColumn.AllowFocus = false;
            this.colIsTest.OptionsColumn.ReadOnly = true;
            this.colIsTest.Visible = true;
            this.colIsTest.VisibleIndex = 4;
            this.colIsTest.Width = 69;
            // 
            // colJoinedDate
            // 
            this.colJoinedDate.Caption = "Joined ";
            this.colJoinedDate.FieldName = "JoinedDate";
            this.colJoinedDate.Name = "colJoinedDate";
            this.colJoinedDate.OptionsColumn.AllowEdit = false;
            this.colJoinedDate.OptionsColumn.AllowFocus = false;
            this.colJoinedDate.OptionsColumn.ReadOnly = true;
            this.colJoinedDate.Visible = true;
            this.colJoinedDate.VisibleIndex = 44;
            // 
            // colApprovedDate
            // 
            this.colApprovedDate.Caption = "Approved";
            this.colApprovedDate.FieldName = "ApprovedDate";
            this.colApprovedDate.Name = "colApprovedDate";
            this.colApprovedDate.OptionsColumn.AllowEdit = false;
            this.colApprovedDate.OptionsColumn.AllowFocus = false;
            this.colApprovedDate.OptionsColumn.ReadOnly = true;
            this.colApprovedDate.Visible = true;
            this.colApprovedDate.VisibleIndex = 45;
            // 
            // colTerminationDate
            // 
            this.colTerminationDate.Caption = "Termination";
            this.colTerminationDate.FieldName = "TerminationDate";
            this.colTerminationDate.Name = "colTerminationDate";
            this.colTerminationDate.OptionsColumn.AllowEdit = false;
            this.colTerminationDate.OptionsColumn.AllowFocus = false;
            this.colTerminationDate.OptionsColumn.ReadOnly = true;
            this.colTerminationDate.Visible = true;
            this.colTerminationDate.VisibleIndex = 46;
            // 
            // colNoSelfBilling
            // 
            this.colNoSelfBilling.Caption = "OM - No Self-Billing";
            this.colNoSelfBilling.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colNoSelfBilling.FieldName = "NoSelfBilling";
            this.colNoSelfBilling.Name = "colNoSelfBilling";
            this.colNoSelfBilling.OptionsColumn.AllowEdit = false;
            this.colNoSelfBilling.OptionsColumn.AllowFocus = false;
            this.colNoSelfBilling.OptionsColumn.ReadOnly = true;
            this.colNoSelfBilling.Visible = true;
            this.colNoSelfBilling.VisibleIndex = 42;
            this.colNoSelfBilling.Width = 109;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1270, 286);
            this.xtraTabControl1.TabIndex = 1;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage11,
            this.xtraTabPage3,
            this.xtraTabPagePersonResponsibilities,
            this.xtraTabPage8,
            this.xtraTabPageInsurance,
            this.xtraTabPageInsuranceCategories,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6,
            this.xtraTabPage7,
            this.xtraTabPage9,
            this.xtraTabPage10,
            this.xtraTabPageLinkedDocuments});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage1.Text = "Contacts";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1265, 260);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp00181ContractorManagerContactsBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEditBoolean,
            this.repositoryItemHyperLinkEdit4,
            this.repositoryItemCheckEditNumeric});
            this.gridControl2.Size = new System.Drawing.Size(1265, 260);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp00181ContractorManagerContactsBindingSource
            // 
            this.sp00181ContractorManagerContactsBindingSource.DataMember = "sp00181_Contractor_Manager_Contacts";
            this.sp00181ContractorManagerContactsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colContactID,
            this.colContractorID1,
            this.colContactName,
            this.colContractorName1,
            this.colTelephone,
            this.colMobile1,
            this.colFax,
            this.colEmail,
            this.colRemarks1,
            this.colRecordDisabled,
            this.colDefaultContact,
            this.colCreatedBy,
            this.colGrittingEmail,
            this.colIsSelfBillingEmail,
            this.colIsSnowMobileTel,
            this.colIsActiveTotalViewUser,
            this.colTotalViewUserID,
            this.colIsOMSelfBillingEmail});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContactName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView2_CustomRowCellEdit);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView2_ShowingEditor);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colContactID
            // 
            this.colContactID.Caption = "Contact ID";
            this.colContactID.FieldName = "ContactID";
            this.colContactID.Name = "colContactID";
            this.colContactID.OptionsColumn.AllowEdit = false;
            this.colContactID.OptionsColumn.AllowFocus = false;
            this.colContactID.OptionsColumn.FixedWidth = true;
            this.colContactID.OptionsColumn.ReadOnly = true;
            // 
            // colContractorID1
            // 
            this.colContractorID1.Caption = "Team ID";
            this.colContractorID1.FieldName = "ContractorID";
            this.colContractorID1.Name = "colContractorID1";
            this.colContractorID1.OptionsColumn.AllowEdit = false;
            this.colContractorID1.OptionsColumn.AllowFocus = false;
            this.colContractorID1.OptionsColumn.FixedWidth = true;
            // 
            // colContactName
            // 
            this.colContactName.Caption = "Contact Name";
            this.colContactName.FieldName = "ContactName";
            this.colContactName.Name = "colContactName";
            this.colContactName.OptionsColumn.AllowEdit = false;
            this.colContactName.OptionsColumn.AllowFocus = false;
            this.colContactName.OptionsColumn.FixedWidth = true;
            this.colContactName.Visible = true;
            this.colContactName.VisibleIndex = 0;
            this.colContactName.Width = 280;
            // 
            // colContractorName1
            // 
            this.colContractorName1.Caption = "Team Name";
            this.colContractorName1.FieldName = "ContractorName";
            this.colContractorName1.Name = "colContractorName1";
            this.colContractorName1.OptionsColumn.AllowEdit = false;
            this.colContractorName1.OptionsColumn.AllowFocus = false;
            this.colContractorName1.OptionsColumn.FixedWidth = true;
            this.colContractorName1.OptionsColumn.ReadOnly = true;
            this.colContractorName1.Width = 199;
            // 
            // colTelephone
            // 
            this.colTelephone.Caption = "Telephone";
            this.colTelephone.FieldName = "Telephone";
            this.colTelephone.Name = "colTelephone";
            this.colTelephone.OptionsColumn.AllowEdit = false;
            this.colTelephone.OptionsColumn.AllowFocus = false;
            this.colTelephone.OptionsColumn.FixedWidth = true;
            this.colTelephone.Visible = true;
            this.colTelephone.VisibleIndex = 3;
            this.colTelephone.Width = 95;
            // 
            // colMobile1
            // 
            this.colMobile1.Caption = "Mobile";
            this.colMobile1.FieldName = "Mobile";
            this.colMobile1.Name = "colMobile1";
            this.colMobile1.OptionsColumn.AllowEdit = false;
            this.colMobile1.OptionsColumn.AllowFocus = false;
            this.colMobile1.OptionsColumn.FixedWidth = true;
            this.colMobile1.Visible = true;
            this.colMobile1.VisibleIndex = 4;
            this.colMobile1.Width = 95;
            // 
            // colFax
            // 
            this.colFax.Caption = "Fax";
            this.colFax.FieldName = "Fax";
            this.colFax.Name = "colFax";
            this.colFax.OptionsColumn.AllowEdit = false;
            this.colFax.OptionsColumn.AllowFocus = false;
            this.colFax.OptionsColumn.FixedWidth = true;
            this.colFax.Visible = true;
            this.colFax.VisibleIndex = 6;
            this.colFax.Width = 91;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.ColumnEdit = this.repositoryItemHyperLinkEdit4;
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.FixedWidth = true;
            this.colEmail.Visible = true;
            this.colEmail.VisibleIndex = 7;
            this.colEmail.Width = 161;
            // 
            // repositoryItemHyperLinkEdit4
            // 
            this.repositoryItemHyperLinkEdit4.AutoHeight = false;
            this.repositoryItemHyperLinkEdit4.Name = "repositoryItemHyperLinkEdit4";
            this.repositoryItemHyperLinkEdit4.SingleClick = true;
            this.repositoryItemHyperLinkEdit4.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit4_OpenLink);
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.FixedWidth = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 13;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colRecordDisabled
            // 
            this.colRecordDisabled.Caption = "Disabled";
            this.colRecordDisabled.ColumnEdit = this.repositoryItemCheckEditBoolean;
            this.colRecordDisabled.FieldName = "RecordDisabled";
            this.colRecordDisabled.Name = "colRecordDisabled";
            this.colRecordDisabled.OptionsColumn.AllowEdit = false;
            this.colRecordDisabled.OptionsColumn.AllowFocus = false;
            this.colRecordDisabled.OptionsColumn.FixedWidth = true;
            this.colRecordDisabled.Visible = true;
            this.colRecordDisabled.VisibleIndex = 2;
            this.colRecordDisabled.Width = 60;
            // 
            // repositoryItemCheckEditBoolean
            // 
            this.repositoryItemCheckEditBoolean.AutoHeight = false;
            this.repositoryItemCheckEditBoolean.Caption = "Check";
            this.repositoryItemCheckEditBoolean.Name = "repositoryItemCheckEditBoolean";
            // 
            // colDefaultContact
            // 
            this.colDefaultContact.Caption = "Default Contact";
            this.colDefaultContact.ColumnEdit = this.repositoryItemCheckEditBoolean;
            this.colDefaultContact.FieldName = "DefaultContact";
            this.colDefaultContact.Name = "colDefaultContact";
            this.colDefaultContact.OptionsColumn.AllowEdit = false;
            this.colDefaultContact.OptionsColumn.AllowFocus = false;
            this.colDefaultContact.OptionsColumn.FixedWidth = true;
            this.colDefaultContact.Visible = true;
            this.colDefaultContact.VisibleIndex = 1;
            this.colDefaultContact.Width = 98;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Created By ID";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.AllowEdit = false;
            this.colCreatedBy.OptionsColumn.AllowFocus = false;
            this.colCreatedBy.OptionsColumn.FixedWidth = true;
            // 
            // colGrittingEmail
            // 
            this.colGrittingEmail.Caption = "Gritting Email";
            this.colGrittingEmail.ColumnEdit = this.repositoryItemCheckEditNumeric;
            this.colGrittingEmail.FieldName = "GrittingEmail";
            this.colGrittingEmail.Name = "colGrittingEmail";
            this.colGrittingEmail.OptionsColumn.AllowEdit = false;
            this.colGrittingEmail.OptionsColumn.AllowFocus = false;
            this.colGrittingEmail.OptionsColumn.ReadOnly = true;
            this.colGrittingEmail.Visible = true;
            this.colGrittingEmail.VisibleIndex = 9;
            this.colGrittingEmail.Width = 133;
            // 
            // repositoryItemCheckEditNumeric
            // 
            this.repositoryItemCheckEditNumeric.AutoHeight = false;
            this.repositoryItemCheckEditNumeric.Caption = "Check";
            this.repositoryItemCheckEditNumeric.Name = "repositoryItemCheckEditNumeric";
            this.repositoryItemCheckEditNumeric.ValueChecked = 1;
            this.repositoryItemCheckEditNumeric.ValueUnchecked = 0;
            // 
            // colIsSelfBillingEmail
            // 
            this.colIsSelfBillingEmail.Caption = "WM Self-Billing Email";
            this.colIsSelfBillingEmail.ColumnEdit = this.repositoryItemCheckEditNumeric;
            this.colIsSelfBillingEmail.FieldName = "IsSelfBillingEmail";
            this.colIsSelfBillingEmail.Name = "colIsSelfBillingEmail";
            this.colIsSelfBillingEmail.OptionsColumn.AllowEdit = false;
            this.colIsSelfBillingEmail.OptionsColumn.AllowFocus = false;
            this.colIsSelfBillingEmail.OptionsColumn.ReadOnly = true;
            this.colIsSelfBillingEmail.Visible = true;
            this.colIsSelfBillingEmail.VisibleIndex = 10;
            this.colIsSelfBillingEmail.Width = 131;
            // 
            // colIsSnowMobileTel
            // 
            this.colIsSnowMobileTel.Caption = "Snow Clearance Mobile Tel";
            this.colIsSnowMobileTel.ColumnEdit = this.repositoryItemCheckEditNumeric;
            this.colIsSnowMobileTel.FieldName = "IsSnowMobileTel";
            this.colIsSnowMobileTel.Name = "colIsSnowMobileTel";
            this.colIsSnowMobileTel.OptionsColumn.AllowEdit = false;
            this.colIsSnowMobileTel.OptionsColumn.AllowFocus = false;
            this.colIsSnowMobileTel.OptionsColumn.ReadOnly = true;
            this.colIsSnowMobileTel.Visible = true;
            this.colIsSnowMobileTel.VisibleIndex = 5;
            this.colIsSnowMobileTel.Width = 146;
            // 
            // colIsActiveTotalViewUser
            // 
            this.colIsActiveTotalViewUser.Caption = "Total View Active";
            this.colIsActiveTotalViewUser.ColumnEdit = this.repositoryItemCheckEditNumeric;
            this.colIsActiveTotalViewUser.FieldName = "IsActiveTotalViewUser";
            this.colIsActiveTotalViewUser.Name = "colIsActiveTotalViewUser";
            this.colIsActiveTotalViewUser.OptionsColumn.AllowEdit = false;
            this.colIsActiveTotalViewUser.OptionsColumn.AllowFocus = false;
            this.colIsActiveTotalViewUser.OptionsColumn.ReadOnly = true;
            this.colIsActiveTotalViewUser.Visible = true;
            this.colIsActiveTotalViewUser.VisibleIndex = 11;
            this.colIsActiveTotalViewUser.Width = 101;
            // 
            // colTotalViewUserID
            // 
            this.colTotalViewUserID.Caption = "Total View User ID";
            this.colTotalViewUserID.FieldName = "TotalViewUserID";
            this.colTotalViewUserID.Name = "colTotalViewUserID";
            this.colTotalViewUserID.OptionsColumn.AllowEdit = false;
            this.colTotalViewUserID.OptionsColumn.AllowFocus = false;
            this.colTotalViewUserID.OptionsColumn.ReadOnly = true;
            this.colTotalViewUserID.Visible = true;
            this.colTotalViewUserID.VisibleIndex = 12;
            this.colTotalViewUserID.Width = 107;
            // 
            // colIsOMSelfBillingEmail
            // 
            this.colIsOMSelfBillingEmail.Caption = "OM Self-Billing Invoice Email";
            this.colIsOMSelfBillingEmail.ColumnEdit = this.repositoryItemCheckEditNumeric;
            this.colIsOMSelfBillingEmail.FieldName = "IsOMSelfBillingEmail";
            this.colIsOMSelfBillingEmail.Name = "colIsOMSelfBillingEmail";
            this.colIsOMSelfBillingEmail.OptionsColumn.AllowEdit = false;
            this.colIsOMSelfBillingEmail.OptionsColumn.AllowFocus = false;
            this.colIsOMSelfBillingEmail.OptionsColumn.ReadOnly = true;
            this.colIsOMSelfBillingEmail.Visible = true;
            this.colIsOMSelfBillingEmail.VisibleIndex = 8;
            this.colIsOMSelfBillingEmail.Width = 151;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridSplitContainer3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage2.Text = "Certificates";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl3;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer3.Size = new System.Drawing.Size(1265, 260);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp00182ContractorManagerCertificatesBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl3.Size = new System.Drawing.Size(1265, 260);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp00182ContractorManagerCertificatesBindingSource
            // 
            this.sp00182ContractorManagerCertificatesBindingSource.DataMember = "sp00182_Contractor_Manager_Certificates";
            this.sp00182ContractorManagerCertificatesBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCertificateID,
            this.colContractorID2,
            this.colContractorName2,
            this.colCertificateTypeID,
            this.colCertificateDescription,
            this.colStartDate,
            this.colEndDate,
            this.colRemarks2,
            this.colCertificatePath});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCertificateDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEdit);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView3_ShowingEditor);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colCertificateID
            // 
            this.colCertificateID.Caption = "Certificate ID";
            this.colCertificateID.FieldName = "CertificateID";
            this.colCertificateID.Name = "colCertificateID";
            this.colCertificateID.OptionsColumn.AllowEdit = false;
            this.colCertificateID.OptionsColumn.AllowFocus = false;
            this.colCertificateID.OptionsColumn.ReadOnly = true;
            // 
            // colContractorID2
            // 
            this.colContractorID2.Caption = "Team ID";
            this.colContractorID2.FieldName = "ContractorID";
            this.colContractorID2.Name = "colContractorID2";
            this.colContractorID2.OptionsColumn.AllowEdit = false;
            this.colContractorID2.OptionsColumn.AllowFocus = false;
            this.colContractorID2.OptionsColumn.ReadOnly = true;
            this.colContractorID2.Width = 77;
            // 
            // colContractorName2
            // 
            this.colContractorName2.Caption = "Team Name";
            this.colContractorName2.FieldName = "ContractorName";
            this.colContractorName2.Name = "colContractorName2";
            this.colContractorName2.OptionsColumn.AllowEdit = false;
            this.colContractorName2.OptionsColumn.AllowFocus = false;
            this.colContractorName2.OptionsColumn.ReadOnly = true;
            this.colContractorName2.Width = 203;
            // 
            // colCertificateTypeID
            // 
            this.colCertificateTypeID.Caption = "Certificate Type ID";
            this.colCertificateTypeID.FieldName = "CertificateTypeID";
            this.colCertificateTypeID.Name = "colCertificateTypeID";
            this.colCertificateTypeID.OptionsColumn.AllowEdit = false;
            this.colCertificateTypeID.OptionsColumn.AllowFocus = false;
            this.colCertificateTypeID.OptionsColumn.ReadOnly = true;
            this.colCertificateTypeID.Width = 102;
            // 
            // colCertificateDescription
            // 
            this.colCertificateDescription.Caption = "Certificate Name";
            this.colCertificateDescription.FieldName = "CertificateDescription";
            this.colCertificateDescription.Name = "colCertificateDescription";
            this.colCertificateDescription.OptionsColumn.AllowEdit = false;
            this.colCertificateDescription.OptionsColumn.AllowFocus = false;
            this.colCertificateDescription.OptionsColumn.ReadOnly = true;
            this.colCertificateDescription.Visible = true;
            this.colCertificateDescription.VisibleIndex = 0;
            this.colCertificateDescription.Width = 323;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 1;
            this.colStartDate.Width = 71;
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 2;
            this.colEndDate.Width = 65;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 4;
            this.colRemarks2.Width = 106;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colCertificatePath
            // 
            this.colCertificatePath.Caption = "Certificate File";
            this.colCertificatePath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colCertificatePath.FieldName = "CertificatePath";
            this.colCertificatePath.Name = "colCertificatePath";
            this.colCertificatePath.OptionsColumn.ReadOnly = true;
            this.colCertificatePath.Visible = true;
            this.colCertificatePath.VisibleIndex = 3;
            this.colCertificatePath.Width = 268;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // xtraTabPage11
            // 
            this.xtraTabPage11.Controls.Add(this.gridSplitContainer11);
            this.xtraTabPage11.Name = "xtraTabPage11";
            this.xtraTabPage11.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage11.Text = "Training";
            // 
            // gridSplitContainer11
            // 
            this.gridSplitContainer11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer11.Grid = this.gridControlTraining;
            this.gridSplitContainer11.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer11.Name = "gridSplitContainer11";
            this.gridSplitContainer11.Panel1.Controls.Add(this.gridControlTraining);
            this.gridSplitContainer11.Size = new System.Drawing.Size(1265, 260);
            this.gridSplitContainer11.TabIndex = 0;
            // 
            // gridControlTraining
            // 
            this.gridControlTraining.DataSource = this.sp09119HRQualificationsForContractorBindingSource;
            this.gridControlTraining.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlTraining.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Linked Documents", "linked_document")});
            this.gridControlTraining.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlTraining_EmbeddedNavigator_ButtonClick);
            this.gridControlTraining.Location = new System.Drawing.Point(0, 0);
            this.gridControlTraining.MainView = this.gridViewTraining;
            this.gridControlTraining.MenuManager = this.barManager1;
            this.gridControlTraining.Name = "gridControlTraining";
            this.gridControlTraining.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining,
            this.repositoryItemTextEditDateTime12,
            this.repositoryItemMemoExEdit14,
            this.repositoryItemCheckEdit13,
            this.repositoryItemTextEdit2});
            this.gridControlTraining.Size = new System.Drawing.Size(1265, 260);
            this.gridControlTraining.TabIndex = 1;
            this.gridControlTraining.UseEmbeddedNavigator = true;
            this.gridControlTraining.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTraining});
            // 
            // sp09119HRQualificationsForContractorBindingSource
            // 
            this.sp09119HRQualificationsForContractorBindingSource.DataMember = "sp09119_HR_Qualifications_For_Contractor";
            this.sp09119HRQualificationsForContractorBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewTraining
            // 
            this.gridViewTraining.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQualificationID,
            this.gridColumn5,
            this.gridColumn6,
            this.colQualificationTypeID,
            this.colQualificationFromID,
            this.colCourseNumber,
            this.colCourseName,
            this.colCostToCompany,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn7,
            this.colSummerMaintenance,
            this.colWinterMaintenance,
            this.colUtilityArb,
            this.colUtilityRail,
            this.colWoodPlan,
            this.colGUID,
            this.gridColumn11,
            this.gridColumn12,
            this.colQualificationType,
            this.colQualificationFrom,
            this.colDaysUntilExpiry,
            this.colLinkedDocumentCount7,
            this.colQualificationSubType,
            this.colQualificationSubTypeID,
            this.colArchived,
            this.colAssessmentDate,
            this.colCourseEndDate,
            this.colCourseStartDate});
            this.gridViewTraining.GridControl = this.gridControlTraining;
            this.gridViewTraining.GroupCount = 1;
            this.gridViewTraining.Name = "gridViewTraining";
            this.gridViewTraining.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewTraining.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewTraining.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewTraining.OptionsLayout.StoreAppearance = true;
            this.gridViewTraining.OptionsLayout.StoreFormatRules = true;
            this.gridViewTraining.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewTraining.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewTraining.OptionsSelection.MultiSelect = true;
            this.gridViewTraining.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewTraining.OptionsView.ColumnAutoWidth = false;
            this.gridViewTraining.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewTraining.OptionsView.ShowGroupPanel = false;
            this.gridViewTraining.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDaysUntilExpiry, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn11, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewTraining.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewTraining_CustomDrawCell);
            this.gridViewTraining.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewTraining_CustomRowCellEdit);
            this.gridViewTraining.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewTraining.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewTraining.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewTraining.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewTraining_ShowingEditor);
            this.gridViewTraining.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewTraining.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewTraining.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewTraining.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewTraining_MouseUp);
            this.gridViewTraining.DoubleClick += new System.EventHandler(this.gridViewTraining_DoubleClick);
            this.gridViewTraining.GotFocus += new System.EventHandler(this.gridViewTraining_GotFocus);
            // 
            // colQualificationID
            // 
            this.colQualificationID.Caption = "Qualification ID";
            this.colQualificationID.FieldName = "QualificationID";
            this.colQualificationID.Name = "colQualificationID";
            this.colQualificationID.OptionsColumn.AllowEdit = false;
            this.colQualificationID.OptionsColumn.AllowFocus = false;
            this.colQualificationID.OptionsColumn.ReadOnly = true;
            this.colQualificationID.Width = 94;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Linked To Person ID";
            this.gridColumn5.FieldName = "LinkedToPersonID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 116;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Linked To Person Type ID";
            this.gridColumn6.FieldName = "LinkedToPersonTypeID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 143;
            // 
            // colQualificationTypeID
            // 
            this.colQualificationTypeID.Caption = "Qualification Type ID";
            this.colQualificationTypeID.FieldName = "QualificationTypeID";
            this.colQualificationTypeID.Name = "colQualificationTypeID";
            this.colQualificationTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeID.Width = 121;
            // 
            // colQualificationFromID
            // 
            this.colQualificationFromID.Caption = "Qualification From ID";
            this.colQualificationFromID.FieldName = "QualificationFromID";
            this.colQualificationFromID.Name = "colQualificationFromID";
            this.colQualificationFromID.OptionsColumn.AllowEdit = false;
            this.colQualificationFromID.OptionsColumn.AllowFocus = false;
            this.colQualificationFromID.OptionsColumn.ReadOnly = true;
            this.colQualificationFromID.Width = 121;
            // 
            // colCourseNumber
            // 
            this.colCourseNumber.Caption = "Course #";
            this.colCourseNumber.FieldName = "CourseNumber";
            this.colCourseNumber.Name = "colCourseNumber";
            this.colCourseNumber.OptionsColumn.AllowEdit = false;
            this.colCourseNumber.OptionsColumn.AllowFocus = false;
            this.colCourseNumber.OptionsColumn.ReadOnly = true;
            this.colCourseNumber.Visible = true;
            this.colCourseNumber.VisibleIndex = 11;
            // 
            // colCourseName
            // 
            this.colCourseName.Caption = "Course Name";
            this.colCourseName.FieldName = "CourseName";
            this.colCourseName.Name = "colCourseName";
            this.colCourseName.OptionsColumn.AllowEdit = false;
            this.colCourseName.OptionsColumn.AllowFocus = false;
            this.colCourseName.OptionsColumn.ReadOnly = true;
            this.colCourseName.Visible = true;
            this.colCourseName.VisibleIndex = 10;
            this.colCourseName.Width = 120;
            // 
            // colCostToCompany
            // 
            this.colCostToCompany.Caption = "Cost to Company";
            this.colCostToCompany.FieldName = "CostToCompany";
            this.colCostToCompany.Name = "colCostToCompany";
            this.colCostToCompany.OptionsColumn.AllowEdit = false;
            this.colCostToCompany.OptionsColumn.AllowFocus = false;
            this.colCostToCompany.OptionsColumn.ReadOnly = true;
            this.colCostToCompany.Visible = true;
            this.colCostToCompany.VisibleIndex = 13;
            this.colCostToCompany.Width = 104;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Qualification Start";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEditDateTime12;
            this.gridColumn8.FieldName = "StartDate";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            this.gridColumn8.Width = 107;
            // 
            // repositoryItemTextEditDateTime12
            // 
            this.repositoryItemTextEditDateTime12.AutoHeight = false;
            this.repositoryItemTextEditDateTime12.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime12.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime12.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime12.Name = "repositoryItemTextEditDateTime12";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Qualification End";
            this.gridColumn9.ColumnEdit = this.repositoryItemTextEditDateTime12;
            this.gridColumn9.FieldName = "EndDate";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 6;
            this.gridColumn9.Width = 101;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Qualification Expiry";
            this.gridColumn10.ColumnEdit = this.repositoryItemTextEditDateTime12;
            this.gridColumn10.FieldName = "ExpiryDate";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            this.gridColumn10.Width = 113;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Remarks";
            this.gridColumn7.ColumnEdit = this.repositoryItemMemoExEdit14;
            this.gridColumn7.FieldName = "Remarks";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit14
            // 
            this.repositoryItemMemoExEdit14.AutoHeight = false;
            this.repositoryItemMemoExEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit14.Name = "repositoryItemMemoExEdit14";
            this.repositoryItemMemoExEdit14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit14.ShowIcon = false;
            // 
            // colSummerMaintenance
            // 
            this.colSummerMaintenance.Caption = "Summer Maintenance";
            this.colSummerMaintenance.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colSummerMaintenance.FieldName = "SummerMaintenance";
            this.colSummerMaintenance.Name = "colSummerMaintenance";
            this.colSummerMaintenance.OptionsColumn.AllowEdit = false;
            this.colSummerMaintenance.OptionsColumn.AllowFocus = false;
            this.colSummerMaintenance.OptionsColumn.ReadOnly = true;
            this.colSummerMaintenance.Visible = true;
            this.colSummerMaintenance.VisibleIndex = 15;
            this.colSummerMaintenance.Width = 123;
            // 
            // repositoryItemCheckEdit13
            // 
            this.repositoryItemCheckEdit13.AutoHeight = false;
            this.repositoryItemCheckEdit13.Caption = "Check";
            this.repositoryItemCheckEdit13.Name = "repositoryItemCheckEdit13";
            this.repositoryItemCheckEdit13.ValueChecked = 1;
            this.repositoryItemCheckEdit13.ValueUnchecked = 0;
            // 
            // colWinterMaintenance
            // 
            this.colWinterMaintenance.Caption = "Winter Maintenance";
            this.colWinterMaintenance.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colWinterMaintenance.FieldName = "WinterMaintenance";
            this.colWinterMaintenance.Name = "colWinterMaintenance";
            this.colWinterMaintenance.OptionsColumn.AllowEdit = false;
            this.colWinterMaintenance.OptionsColumn.AllowFocus = false;
            this.colWinterMaintenance.OptionsColumn.ReadOnly = true;
            this.colWinterMaintenance.Visible = true;
            this.colWinterMaintenance.VisibleIndex = 16;
            this.colWinterMaintenance.Width = 117;
            // 
            // colUtilityArb
            // 
            this.colUtilityArb.Caption = "Utility ARB";
            this.colUtilityArb.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colUtilityArb.FieldName = "UtilityArb";
            this.colUtilityArb.Name = "colUtilityArb";
            this.colUtilityArb.OptionsColumn.AllowEdit = false;
            this.colUtilityArb.OptionsColumn.AllowFocus = false;
            this.colUtilityArb.OptionsColumn.ReadOnly = true;
            this.colUtilityArb.Visible = true;
            this.colUtilityArb.VisibleIndex = 17;
            // 
            // colUtilityRail
            // 
            this.colUtilityRail.Caption = "Utility Rail";
            this.colUtilityRail.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colUtilityRail.FieldName = "UtilityRail";
            this.colUtilityRail.Name = "colUtilityRail";
            this.colUtilityRail.OptionsColumn.AllowEdit = false;
            this.colUtilityRail.OptionsColumn.AllowFocus = false;
            this.colUtilityRail.OptionsColumn.ReadOnly = true;
            this.colUtilityRail.Visible = true;
            this.colUtilityRail.VisibleIndex = 18;
            // 
            // colWoodPlan
            // 
            this.colWoodPlan.Caption = "WoodPlan";
            this.colWoodPlan.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colWoodPlan.FieldName = "WoodPlan";
            this.colWoodPlan.Name = "colWoodPlan";
            this.colWoodPlan.OptionsColumn.AllowEdit = false;
            this.colWoodPlan.OptionsColumn.AllowFocus = false;
            this.colWoodPlan.OptionsColumn.ReadOnly = true;
            this.colWoodPlan.Visible = true;
            this.colWoodPlan.VisibleIndex = 19;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Owner Type";
            this.gridColumn11.FieldName = "LinkedToPersonType";
            this.gridColumn11.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 102;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Owner";
            this.gridColumn12.FieldName = "LinkedToPersonName";
            this.gridColumn12.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 1;
            this.gridColumn12.Width = 151;
            // 
            // colQualificationType
            // 
            this.colQualificationType.Caption = "Qualification Type";
            this.colQualificationType.FieldName = "QualificationType";
            this.colQualificationType.Name = "colQualificationType";
            this.colQualificationType.OptionsColumn.AllowEdit = false;
            this.colQualificationType.OptionsColumn.AllowFocus = false;
            this.colQualificationType.OptionsColumn.ReadOnly = true;
            this.colQualificationType.Visible = true;
            this.colQualificationType.VisibleIndex = 1;
            this.colQualificationType.Width = 133;
            // 
            // colQualificationFrom
            // 
            this.colQualificationFrom.Caption = "Qualification Source";
            this.colQualificationFrom.FieldName = "QualificationFrom";
            this.colQualificationFrom.Name = "colQualificationFrom";
            this.colQualificationFrom.OptionsColumn.AllowEdit = false;
            this.colQualificationFrom.OptionsColumn.AllowFocus = false;
            this.colQualificationFrom.OptionsColumn.ReadOnly = true;
            this.colQualificationFrom.Visible = true;
            this.colQualificationFrom.VisibleIndex = 12;
            this.colQualificationFrom.Width = 147;
            // 
            // colDaysUntilExpiry
            // 
            this.colDaysUntilExpiry.Caption = "Expires In";
            this.colDaysUntilExpiry.ColumnEdit = this.repositoryItemTextEdit2;
            this.colDaysUntilExpiry.FieldName = "DaysUntilExpiry";
            this.colDaysUntilExpiry.Name = "colDaysUntilExpiry";
            this.colDaysUntilExpiry.OptionsColumn.AllowEdit = false;
            this.colDaysUntilExpiry.OptionsColumn.AllowFocus = false;
            this.colDaysUntilExpiry.OptionsColumn.ReadOnly = true;
            this.colDaysUntilExpiry.Visible = true;
            this.colDaysUntilExpiry.VisibleIndex = 3;
            this.colDaysUntilExpiry.Width = 82;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "#####0 Days";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colLinkedDocumentCount7
            // 
            this.colLinkedDocumentCount7.Caption = "Linked Documents";
            this.colLinkedDocumentCount7.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsTraining;
            this.colLinkedDocumentCount7.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount7.Name = "colLinkedDocumentCount7";
            this.colLinkedDocumentCount7.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount7.Visible = true;
            this.colLinkedDocumentCount7.VisibleIndex = 20;
            this.colLinkedDocumentCount7.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsTraining
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining.Name = "repositoryItemHyperLinkEditLinkedDocumentsTraining";
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsTraining_OpenLink);
            // 
            // colQualificationSubType
            // 
            this.colQualificationSubType.Caption = "Qualification Sub Type";
            this.colQualificationSubType.FieldName = "QualificationSubType";
            this.colQualificationSubType.Name = "colQualificationSubType";
            this.colQualificationSubType.OptionsColumn.AllowEdit = false;
            this.colQualificationSubType.OptionsColumn.AllowFocus = false;
            this.colQualificationSubType.OptionsColumn.ReadOnly = true;
            this.colQualificationSubType.Visible = true;
            this.colQualificationSubType.VisibleIndex = 2;
            this.colQualificationSubType.Width = 128;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.Caption = "Qualification Sub Type ID";
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationSubTypeID.Width = 142;
            // 
            // colArchived
            // 
            this.colArchived.Caption = "Archived";
            this.colArchived.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colArchived.FieldName = "Archived";
            this.colArchived.Name = "colArchived";
            this.colArchived.OptionsColumn.AllowEdit = false;
            this.colArchived.OptionsColumn.AllowFocus = false;
            this.colArchived.OptionsColumn.ReadOnly = true;
            this.colArchived.Visible = true;
            this.colArchived.VisibleIndex = 21;
            // 
            // colAssessmentDate
            // 
            this.colAssessmentDate.Caption = "Assessment Date";
            this.colAssessmentDate.ColumnEdit = this.repositoryItemTextEditDateTime12;
            this.colAssessmentDate.FieldName = "AssessmentDate";
            this.colAssessmentDate.Name = "colAssessmentDate";
            this.colAssessmentDate.OptionsColumn.AllowEdit = false;
            this.colAssessmentDate.OptionsColumn.AllowFocus = false;
            this.colAssessmentDate.OptionsColumn.ReadOnly = true;
            this.colAssessmentDate.Visible = true;
            this.colAssessmentDate.VisibleIndex = 9;
            this.colAssessmentDate.Width = 104;
            // 
            // colCourseEndDate
            // 
            this.colCourseEndDate.Caption = "Course End";
            this.colCourseEndDate.ColumnEdit = this.repositoryItemTextEditDateTime12;
            this.colCourseEndDate.FieldName = "CourseEndDate";
            this.colCourseEndDate.Name = "colCourseEndDate";
            this.colCourseEndDate.OptionsColumn.AllowEdit = false;
            this.colCourseEndDate.OptionsColumn.AllowFocus = false;
            this.colCourseEndDate.OptionsColumn.ReadOnly = true;
            this.colCourseEndDate.Visible = true;
            this.colCourseEndDate.VisibleIndex = 8;
            this.colCourseEndDate.Width = 100;
            // 
            // colCourseStartDate
            // 
            this.colCourseStartDate.Caption = "Course Start";
            this.colCourseStartDate.ColumnEdit = this.repositoryItemTextEditDateTime12;
            this.colCourseStartDate.FieldName = "CourseStartDate";
            this.colCourseStartDate.Name = "colCourseStartDate";
            this.colCourseStartDate.OptionsColumn.AllowEdit = false;
            this.colCourseStartDate.OptionsColumn.AllowFocus = false;
            this.colCourseStartDate.OptionsColumn.ReadOnly = true;
            this.colCourseStartDate.Visible = true;
            this.colCourseStartDate.VisibleIndex = 7;
            this.colCourseStartDate.Width = 100;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.splitContainerControlTeamMembers);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage3.Text = "Team Members";
            // 
            // splitContainerControlTeamMembers
            // 
            this.splitContainerControlTeamMembers.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControlTeamMembers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControlTeamMembers.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControlTeamMembers.Name = "splitContainerControlTeamMembers";
            this.splitContainerControlTeamMembers.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControlTeamMembers.Panel1.Controls.Add(this.gridSplitContainer4);
            this.splitContainerControlTeamMembers.Panel1.ShowCaption = true;
            this.splitContainerControlTeamMembers.Panel1.Text = "Team Members ";
            this.splitContainerControlTeamMembers.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControlTeamMembers.Panel2.CaptionLocation = DevExpress.Utils.Locations.Top;
            this.splitContainerControlTeamMembers.Panel2.Controls.Add(this.gridSplitContainer12);
            this.splitContainerControlTeamMembers.Panel2.ShowCaption = true;
            this.splitContainerControlTeamMembers.Panel2.Text = "Linked Training ";
            this.splitContainerControlTeamMembers.Size = new System.Drawing.Size(1265, 260);
            this.splitContainerControlTeamMembers.SplitterPosition = 540;
            this.splitContainerControlTeamMembers.TabIndex = 1;
            this.splitContainerControlTeamMembers.Text = "splitContainerControl5";
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.gridControl6;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControl6);
            this.gridSplitContainer4.Size = new System.Drawing.Size(536, 236);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp04023GCSubContractorTeamMembersBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit8,
            this.repositoryItemMemoExEdit5,
            this.repositoryItemHyperLinkEdit3});
            this.gridControl6.Size = new System.Drawing.Size(536, 236);
            this.gridControl6.TabIndex = 0;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp04023GCSubContractorTeamMembersBindingSource
            // 
            this.sp04023GCSubContractorTeamMembersBindingSource.DataMember = "sp04023_GC_SubContractor_Team_Members";
            this.sp04023GCSubContractorTeamMembersBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTeamMemberID,
            this.colSubContractorID,
            this.colName,
            this.colSubContractorName,
            this.colAddressLine11,
            this.colAddressLine21,
            this.colAddressLine31,
            this.colAddressLine41,
            this.colAddressLine51,
            this.colPostcode1,
            this.colTelephone3,
            this.colMobile2,
            this.colFax1,
            this.colEmail1,
            this.colRemarks3,
            this.colActive,
            this.colGender,
            this.colGenderID,
            this.colRole,
            this.colRoleID,
            this.colPDAUserName,
            this.colPDAPassword,
            this.colPDALoginToken,
            this.colGritJobAllocateByDefault,
            this.colDateAdded2,
            this.colNatInsuranceNumber});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 1;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView6_CustomRowCellEdit);
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView6_ShowingEditor);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colTeamMemberID
            // 
            this.colTeamMemberID.Caption = "Team Member ID";
            this.colTeamMemberID.FieldName = "TeamMemberID";
            this.colTeamMemberID.Name = "colTeamMemberID";
            this.colTeamMemberID.OptionsColumn.AllowEdit = false;
            this.colTeamMemberID.OptionsColumn.AllowFocus = false;
            this.colTeamMemberID.OptionsColumn.ReadOnly = true;
            this.colTeamMemberID.Width = 102;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Team ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            this.colSubContractorID.Width = 109;
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowFocus = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 218;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Team Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.Width = 177;
            // 
            // colAddressLine11
            // 
            this.colAddressLine11.Caption = "Address Line 1";
            this.colAddressLine11.FieldName = "AddressLine1";
            this.colAddressLine11.Name = "colAddressLine11";
            this.colAddressLine11.OptionsColumn.AllowEdit = false;
            this.colAddressLine11.OptionsColumn.AllowFocus = false;
            this.colAddressLine11.OptionsColumn.ReadOnly = true;
            this.colAddressLine11.Visible = true;
            this.colAddressLine11.VisibleIndex = 3;
            this.colAddressLine11.Width = 155;
            // 
            // colAddressLine21
            // 
            this.colAddressLine21.Caption = "Address Line 2";
            this.colAddressLine21.FieldName = "AddressLine2";
            this.colAddressLine21.Name = "colAddressLine21";
            this.colAddressLine21.OptionsColumn.AllowEdit = false;
            this.colAddressLine21.OptionsColumn.AllowFocus = false;
            this.colAddressLine21.OptionsColumn.ReadOnly = true;
            this.colAddressLine21.Width = 128;
            // 
            // colAddressLine31
            // 
            this.colAddressLine31.Caption = "Address Line 3";
            this.colAddressLine31.FieldName = "AddressLine3";
            this.colAddressLine31.Name = "colAddressLine31";
            this.colAddressLine31.OptionsColumn.AllowEdit = false;
            this.colAddressLine31.OptionsColumn.AllowFocus = false;
            this.colAddressLine31.OptionsColumn.ReadOnly = true;
            this.colAddressLine31.Width = 122;
            // 
            // colAddressLine41
            // 
            this.colAddressLine41.Caption = "Address Line 4";
            this.colAddressLine41.FieldName = "AddressLine4";
            this.colAddressLine41.Name = "colAddressLine41";
            this.colAddressLine41.OptionsColumn.AllowEdit = false;
            this.colAddressLine41.OptionsColumn.AllowFocus = false;
            this.colAddressLine41.OptionsColumn.ReadOnly = true;
            this.colAddressLine41.Width = 123;
            // 
            // colAddressLine51
            // 
            this.colAddressLine51.Caption = "Address Line 5";
            this.colAddressLine51.FieldName = "AddressLine5";
            this.colAddressLine51.Name = "colAddressLine51";
            this.colAddressLine51.OptionsColumn.AllowEdit = false;
            this.colAddressLine51.OptionsColumn.AllowFocus = false;
            this.colAddressLine51.OptionsColumn.ReadOnly = true;
            this.colAddressLine51.Width = 125;
            // 
            // colPostcode1
            // 
            this.colPostcode1.Caption = "Postcode";
            this.colPostcode1.FieldName = "Postcode";
            this.colPostcode1.Name = "colPostcode1";
            this.colPostcode1.OptionsColumn.AllowEdit = false;
            this.colPostcode1.OptionsColumn.AllowFocus = false;
            this.colPostcode1.OptionsColumn.ReadOnly = true;
            // 
            // colTelephone3
            // 
            this.colTelephone3.Caption = "Telephone";
            this.colTelephone3.FieldName = "Telephone";
            this.colTelephone3.Name = "colTelephone3";
            this.colTelephone3.OptionsColumn.AllowEdit = false;
            this.colTelephone3.OptionsColumn.AllowFocus = false;
            this.colTelephone3.OptionsColumn.ReadOnly = true;
            this.colTelephone3.Visible = true;
            this.colTelephone3.VisibleIndex = 4;
            this.colTelephone3.Width = 92;
            // 
            // colMobile2
            // 
            this.colMobile2.Caption = "Mobile";
            this.colMobile2.FieldName = "Mobile";
            this.colMobile2.Name = "colMobile2";
            this.colMobile2.OptionsColumn.AllowEdit = false;
            this.colMobile2.OptionsColumn.AllowFocus = false;
            this.colMobile2.OptionsColumn.ReadOnly = true;
            this.colMobile2.Visible = true;
            this.colMobile2.VisibleIndex = 5;
            this.colMobile2.Width = 88;
            // 
            // colFax1
            // 
            this.colFax1.Caption = "Fax";
            this.colFax1.FieldName = "Fax";
            this.colFax1.Name = "colFax1";
            this.colFax1.OptionsColumn.AllowEdit = false;
            this.colFax1.OptionsColumn.AllowFocus = false;
            this.colFax1.OptionsColumn.ReadOnly = true;
            this.colFax1.Visible = true;
            this.colFax1.VisibleIndex = 6;
            // 
            // colEmail1
            // 
            this.colEmail1.Caption = "Email";
            this.colEmail1.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.colEmail1.FieldName = "Email";
            this.colEmail1.Name = "colEmail1";
            this.colEmail1.OptionsColumn.ReadOnly = true;
            this.colEmail1.Visible = true;
            this.colEmail1.VisibleIndex = 7;
            this.colEmail1.Width = 193;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 10;
            this.colRemarks3.Width = 104;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit8;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Caption = "Check";
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = 1;
            this.repositoryItemCheckEdit8.ValueUnchecked = 0;
            // 
            // colGender
            // 
            this.colGender.Caption = "Gender";
            this.colGender.FieldName = "Gender";
            this.colGender.Name = "colGender";
            this.colGender.OptionsColumn.AllowEdit = false;
            this.colGender.OptionsColumn.AllowFocus = false;
            this.colGender.OptionsColumn.ReadOnly = true;
            this.colGender.Visible = true;
            this.colGender.VisibleIndex = 8;
            this.colGender.Width = 56;
            // 
            // colGenderID
            // 
            this.colGenderID.Caption = "Gender ID";
            this.colGenderID.FieldName = "GenderID";
            this.colGenderID.Name = "colGenderID";
            this.colGenderID.OptionsColumn.AllowEdit = false;
            this.colGenderID.OptionsColumn.AllowFocus = false;
            this.colGenderID.OptionsColumn.ReadOnly = true;
            // 
            // colRole
            // 
            this.colRole.Caption = "Role";
            this.colRole.FieldName = "Role";
            this.colRole.Name = "colRole";
            this.colRole.OptionsColumn.AllowEdit = false;
            this.colRole.OptionsColumn.AllowFocus = false;
            this.colRole.OptionsColumn.ReadOnly = true;
            this.colRole.Visible = true;
            this.colRole.VisibleIndex = 9;
            this.colRole.Width = 101;
            // 
            // colRoleID
            // 
            this.colRoleID.Caption = "Role ID";
            this.colRoleID.FieldName = "RoleID";
            this.colRoleID.Name = "colRoleID";
            this.colRoleID.OptionsColumn.AllowEdit = false;
            this.colRoleID.OptionsColumn.AllowFocus = false;
            this.colRoleID.OptionsColumn.ReadOnly = true;
            // 
            // colPDAUserName
            // 
            this.colPDAUserName.Caption = "Device Username";
            this.colPDAUserName.FieldName = "PDAUserName";
            this.colPDAUserName.Name = "colPDAUserName";
            this.colPDAUserName.OptionsColumn.AllowEdit = false;
            this.colPDAUserName.OptionsColumn.AllowFocus = false;
            this.colPDAUserName.OptionsColumn.ReadOnly = true;
            this.colPDAUserName.Visible = true;
            this.colPDAUserName.VisibleIndex = 11;
            this.colPDAUserName.Width = 92;
            // 
            // colPDAPassword
            // 
            this.colPDAPassword.Caption = "Device Password";
            this.colPDAPassword.FieldName = "PDAPassword";
            this.colPDAPassword.Name = "colPDAPassword";
            this.colPDAPassword.OptionsColumn.AllowEdit = false;
            this.colPDAPassword.OptionsColumn.AllowFocus = false;
            this.colPDAPassword.OptionsColumn.ReadOnly = true;
            this.colPDAPassword.Visible = true;
            this.colPDAPassword.VisibleIndex = 12;
            this.colPDAPassword.Width = 90;
            // 
            // colPDALoginToken
            // 
            this.colPDALoginToken.Caption = "Device Login Token";
            this.colPDALoginToken.FieldName = "PDALoginToken";
            this.colPDALoginToken.Name = "colPDALoginToken";
            this.colPDALoginToken.OptionsColumn.AllowEdit = false;
            this.colPDALoginToken.OptionsColumn.AllowFocus = false;
            this.colPDALoginToken.OptionsColumn.ReadOnly = true;
            this.colPDALoginToken.Visible = true;
            this.colPDALoginToken.VisibleIndex = 13;
            this.colPDALoginToken.Width = 178;
            // 
            // colGritJobAllocateByDefault
            // 
            this.colGritJobAllocateByDefault.Caption = "Default Jobs Allocated";
            this.colGritJobAllocateByDefault.ColumnEdit = this.repositoryItemCheckEdit8;
            this.colGritJobAllocateByDefault.FieldName = "GritJobAllocateByDefault";
            this.colGritJobAllocateByDefault.Name = "colGritJobAllocateByDefault";
            this.colGritJobAllocateByDefault.OptionsColumn.AllowEdit = false;
            this.colGritJobAllocateByDefault.OptionsColumn.AllowFocus = false;
            this.colGritJobAllocateByDefault.OptionsColumn.ReadOnly = true;
            this.colGritJobAllocateByDefault.Visible = true;
            this.colGritJobAllocateByDefault.VisibleIndex = 2;
            this.colGritJobAllocateByDefault.Width = 128;
            // 
            // colDateAdded2
            // 
            this.colDateAdded2.FieldName = "DateAdded";
            this.colDateAdded2.Name = "colDateAdded2";
            this.colDateAdded2.OptionsColumn.AllowEdit = false;
            this.colDateAdded2.OptionsColumn.AllowFocus = false;
            this.colDateAdded2.OptionsColumn.ReadOnly = true;
            this.colDateAdded2.Visible = true;
            this.colDateAdded2.VisibleIndex = 14;
            this.colDateAdded2.Width = 49;
            // 
            // colNatInsuranceNumber
            // 
            this.colNatInsuranceNumber.Caption = "NI Number";
            this.colNatInsuranceNumber.FieldName = "NatInsuranceNumber";
            this.colNatInsuranceNumber.Name = "colNatInsuranceNumber";
            this.colNatInsuranceNumber.OptionsColumn.AllowEdit = false;
            this.colNatInsuranceNumber.OptionsColumn.AllowFocus = false;
            this.colNatInsuranceNumber.OptionsColumn.ReadOnly = true;
            this.colNatInsuranceNumber.Visible = true;
            this.colNatInsuranceNumber.VisibleIndex = 15;
            // 
            // gridSplitContainer12
            // 
            this.gridSplitContainer12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer12.Grid = this.gridControlTrainingTeam;
            this.gridSplitContainer12.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer12.Name = "gridSplitContainer12";
            this.gridSplitContainer12.Panel1.Controls.Add(this.gridControlTrainingTeam);
            this.gridSplitContainer12.Size = new System.Drawing.Size(715, 236);
            this.gridSplitContainer12.TabIndex = 0;
            // 
            // gridControlTrainingTeam
            // 
            this.gridControlTrainingTeam.DataSource = this.sp09120HRQualificationsForTeamMemberBindingSource;
            this.gridControlTrainingTeam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlTrainingTeam.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlTrainingTeam.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Linked Documents", "linked_document")});
            this.gridControlTrainingTeam.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlTrainingTeam_EmbeddedNavigator_ButtonClick);
            this.gridControlTrainingTeam.Location = new System.Drawing.Point(0, 0);
            this.gridControlTrainingTeam.MainView = this.gridViewTrainingTeam;
            this.gridControlTrainingTeam.MenuManager = this.barManager1;
            this.gridControlTrainingTeam.Name = "gridControlTrainingTeam";
            this.gridControlTrainingTeam.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit7,
            this.repositoryItemTextEdit4,
            this.repositoryItemMemoExEdit12,
            this.repositoryItemCheckEdit12,
            this.repositoryItemTextEdit5});
            this.gridControlTrainingTeam.Size = new System.Drawing.Size(715, 236);
            this.gridControlTrainingTeam.TabIndex = 2;
            this.gridControlTrainingTeam.UseEmbeddedNavigator = true;
            this.gridControlTrainingTeam.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTrainingTeam});
            // 
            // sp09120HRQualificationsForTeamMemberBindingSource
            // 
            this.sp09120HRQualificationsForTeamMemberBindingSource.DataMember = "sp09120_HR_Qualifications_For_Team_Member";
            this.sp09120HRQualificationsForTeamMemberBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridViewTrainingTeam
            // 
            this.gridViewTrainingTeam.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42});
            this.gridViewTrainingTeam.GridControl = this.gridControlTrainingTeam;
            this.gridViewTrainingTeam.GroupCount = 1;
            this.gridViewTrainingTeam.Name = "gridViewTrainingTeam";
            this.gridViewTrainingTeam.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewTrainingTeam.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewTrainingTeam.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewTrainingTeam.OptionsLayout.StoreAppearance = true;
            this.gridViewTrainingTeam.OptionsLayout.StoreFormatRules = true;
            this.gridViewTrainingTeam.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewTrainingTeam.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewTrainingTeam.OptionsSelection.MultiSelect = true;
            this.gridViewTrainingTeam.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewTrainingTeam.OptionsView.ColumnAutoWidth = false;
            this.gridViewTrainingTeam.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewTrainingTeam.OptionsView.ShowGroupPanel = false;
            this.gridViewTrainingTeam.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn32, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn35, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn31, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewTrainingTeam.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewTrainingTeam_CustomDrawCell);
            this.gridViewTrainingTeam.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewTrainingTeam_CustomRowCellEdit);
            this.gridViewTrainingTeam.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewTrainingTeam.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewTrainingTeam.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewTrainingTeam.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewTrainingTeam_ShowingEditor);
            this.gridViewTrainingTeam.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewTrainingTeam.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewTrainingTeam.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewTrainingTeam.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewTrainingTeam_MouseUp);
            this.gridViewTrainingTeam.DoubleClick += new System.EventHandler(this.gridViewTrainingTeam_DoubleClick);
            this.gridViewTrainingTeam.GotFocus += new System.EventHandler(this.gridViewTrainingTeam_GotFocus);
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Qualification ID";
            this.gridColumn13.FieldName = "QualificationID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 94;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Linked To Person ID";
            this.gridColumn14.FieldName = "LinkedToPersonID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 116;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Linked To Person Type ID";
            this.gridColumn15.FieldName = "LinkedToPersonTypeID";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 143;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Qualification Type ID";
            this.gridColumn16.FieldName = "QualificationTypeID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 121;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Qualification From ID";
            this.gridColumn17.FieldName = "QualificationFromID";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 121;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Course #";
            this.gridColumn18.FieldName = "CourseNumber";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 11;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Course Name";
            this.gridColumn19.FieldName = "CourseName";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 10;
            this.gridColumn19.Width = 120;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Cost to Company";
            this.gridColumn20.FieldName = "CostToCompany";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 13;
            this.gridColumn20.Width = 104;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Qualification Start";
            this.gridColumn21.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn21.FieldName = "StartDate";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 5;
            this.gridColumn21.Width = 107;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "g";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Qualification End";
            this.gridColumn22.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn22.FieldName = "EndDate";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 8;
            this.gridColumn22.Width = 101;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Qualification Expiry";
            this.gridColumn23.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn23.FieldName = "ExpiryDate";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 4;
            this.gridColumn23.Width = 113;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Remarks";
            this.gridColumn24.ColumnEdit = this.repositoryItemMemoExEdit12;
            this.gridColumn24.FieldName = "Remarks";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Summer Maintenance";
            this.gridColumn25.ColumnEdit = this.repositoryItemCheckEdit12;
            this.gridColumn25.FieldName = "SummerMaintenance";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 15;
            this.gridColumn25.Width = 123;
            // 
            // repositoryItemCheckEdit12
            // 
            this.repositoryItemCheckEdit12.AutoHeight = false;
            this.repositoryItemCheckEdit12.Caption = "Check";
            this.repositoryItemCheckEdit12.Name = "repositoryItemCheckEdit12";
            this.repositoryItemCheckEdit12.ValueChecked = 1;
            this.repositoryItemCheckEdit12.ValueUnchecked = 0;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Winter Maintenance";
            this.gridColumn26.ColumnEdit = this.repositoryItemCheckEdit12;
            this.gridColumn26.FieldName = "WinterMaintenance";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 16;
            this.gridColumn26.Width = 117;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Utility ARB";
            this.gridColumn27.ColumnEdit = this.repositoryItemCheckEdit12;
            this.gridColumn27.FieldName = "UtilityArb";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 17;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Utility Rail";
            this.gridColumn28.ColumnEdit = this.repositoryItemCheckEdit12;
            this.gridColumn28.FieldName = "UtilityRail";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 18;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "WoodPlan";
            this.gridColumn29.ColumnEdit = this.repositoryItemCheckEdit12;
            this.gridColumn29.FieldName = "WoodPlan";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 19;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "GUID";
            this.gridColumn30.FieldName = "GUID";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Owner Type";
            this.gridColumn31.FieldName = "LinkedToPersonType";
            this.gridColumn31.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 0;
            this.gridColumn31.Width = 102;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Owner";
            this.gridColumn32.FieldName = "LinkedToPersonName";
            this.gridColumn32.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 1;
            this.gridColumn32.Width = 151;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Qualification Type";
            this.gridColumn33.FieldName = "QualificationType";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 1;
            this.gridColumn33.Width = 133;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Qualification Source";
            this.gridColumn34.FieldName = "QualificationFrom";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 12;
            this.gridColumn34.Width = 147;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Expires In";
            this.gridColumn35.ColumnEdit = this.repositoryItemTextEdit5;
            this.gridColumn35.FieldName = "DaysUntilExpiry";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 3;
            this.gridColumn35.Width = 82;
            // 
            // repositoryItemTextEdit5
            // 
            this.repositoryItemTextEdit5.AutoHeight = false;
            this.repositoryItemTextEdit5.Mask.EditMask = "#####0 Days";
            this.repositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit5.Name = "repositoryItemTextEdit5";
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Linked Documents";
            this.gridColumn36.ColumnEdit = this.repositoryItemHyperLinkEdit7;
            this.gridColumn36.FieldName = "LinkedDocumentCount";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 20;
            this.gridColumn36.Width = 107;
            // 
            // repositoryItemHyperLinkEdit7
            // 
            this.repositoryItemHyperLinkEdit7.AutoHeight = false;
            this.repositoryItemHyperLinkEdit7.Name = "repositoryItemHyperLinkEdit7";
            this.repositoryItemHyperLinkEdit7.SingleClick = true;
            this.repositoryItemHyperLinkEdit7.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit7_OpenLink);
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Qualification Sub Type";
            this.gridColumn37.FieldName = "QualificationSubType";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 2;
            this.gridColumn37.Width = 128;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Qualification Sub Type ID";
            this.gridColumn38.FieldName = "QualificationSubTypeID";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 142;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Archived";
            this.gridColumn39.ColumnEdit = this.repositoryItemCheckEdit12;
            this.gridColumn39.FieldName = "Archived";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 21;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Assessment Date";
            this.gridColumn40.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn40.FieldName = "AssessmentDate";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 9;
            this.gridColumn40.Width = 104;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Course End";
            this.gridColumn41.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn41.FieldName = "CourseEndDate";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 7;
            this.gridColumn41.Width = 100;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Course Start";
            this.gridColumn42.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn42.FieldName = "CourseStartDate";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 6;
            this.gridColumn42.Width = 100;
            // 
            // xtraTabPagePersonResponsibilities
            // 
            this.xtraTabPagePersonResponsibilities.Controls.Add(this.gridControlResponsibility);
            this.xtraTabPagePersonResponsibilities.Name = "xtraTabPagePersonResponsibilities";
            this.xtraTabPagePersonResponsibilities.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPagePersonResponsibilities.Text = "Responsible People";
            // 
            // gridControlResponsibility
            // 
            this.gridControlResponsibility.DataSource = this.sp01020CoreTeamManagerLinkedResponsiblePeopleBindingSource;
            this.gridControlResponsibility.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlResponsibility.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlResponsibility.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "reload")});
            this.gridControlResponsibility.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlResponsibility_EmbeddedNavigator_ButtonClick);
            this.gridControlResponsibility.Location = new System.Drawing.Point(0, 0);
            this.gridControlResponsibility.MainView = this.gridViewResponsibility;
            this.gridControlResponsibility.MenuManager = this.barManager1;
            this.gridControlResponsibility.Name = "gridControlResponsibility";
            this.gridControlResponsibility.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit16,
            this.repositoryItemTextEdit9,
            this.repositoryItemTextEditHTML16});
            this.gridControlResponsibility.Size = new System.Drawing.Size(1265, 260);
            this.gridControlResponsibility.TabIndex = 8;
            this.gridControlResponsibility.UseEmbeddedNavigator = true;
            this.gridControlResponsibility.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewResponsibility});
            // 
            // sp01020CoreTeamManagerLinkedResponsiblePeopleBindingSource
            // 
            this.sp01020CoreTeamManagerLinkedResponsiblePeopleBindingSource.DataMember = "sp01020_Core_Team_Manager_Linked_Responsible_People";
            this.sp01020CoreTeamManagerLinkedResponsiblePeopleBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridViewResponsibility
            // 
            this.gridViewResponsibility.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPersonResponsibilityID,
            this.colPersonTypeDescription,
            this.colPersonTypeID,
            this.gridColumn43,
            this.colResponsibilityType,
            this.colResponsibilityTypeID,
            this.colResponsibleForRecordID,
            this.colResponsibleForRecordTypeID,
            this.colStaffID,
            this.colStaffName,
            this.colTeamName2});
            this.gridViewResponsibility.GridControl = this.gridControlResponsibility;
            this.gridViewResponsibility.GroupCount = 1;
            this.gridViewResponsibility.Name = "gridViewResponsibility";
            this.gridViewResponsibility.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewResponsibility.OptionsFind.FindDelay = 2000;
            this.gridViewResponsibility.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewResponsibility.OptionsLayout.StoreAppearance = true;
            this.gridViewResponsibility.OptionsLayout.StoreFormatRules = true;
            this.gridViewResponsibility.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewResponsibility.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewResponsibility.OptionsSelection.MultiSelect = true;
            this.gridViewResponsibility.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewResponsibility.OptionsView.ColumnAutoWidth = false;
            this.gridViewResponsibility.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewResponsibility.OptionsView.ShowGroupPanel = false;
            this.gridViewResponsibility.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colResponsibilityType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPersonTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStaffName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewResponsibility.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewResponsibility.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewResponsibility.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewResponsibility.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewResponsibility.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewResponsibility.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewResponsibility.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewResponsibility_MouseUp);
            this.gridViewResponsibility.DoubleClick += new System.EventHandler(this.gridViewResponsibility_DoubleClick);
            this.gridViewResponsibility.GotFocus += new System.EventHandler(this.gridViewResponsibility_GotFocus);
            // 
            // colPersonResponsibilityID
            // 
            this.colPersonResponsibilityID.Caption = "Person Responsibility ID";
            this.colPersonResponsibilityID.FieldName = "PersonResponsibilityID";
            this.colPersonResponsibilityID.Name = "colPersonResponsibilityID";
            this.colPersonResponsibilityID.OptionsColumn.AllowEdit = false;
            this.colPersonResponsibilityID.OptionsColumn.AllowFocus = false;
            this.colPersonResponsibilityID.OptionsColumn.ReadOnly = true;
            this.colPersonResponsibilityID.Width = 136;
            // 
            // colPersonTypeDescription
            // 
            this.colPersonTypeDescription.Caption = "Person Type";
            this.colPersonTypeDescription.FieldName = "PersonTypeDescription";
            this.colPersonTypeDescription.Name = "colPersonTypeDescription";
            this.colPersonTypeDescription.OptionsColumn.AllowEdit = false;
            this.colPersonTypeDescription.OptionsColumn.AllowFocus = false;
            this.colPersonTypeDescription.OptionsColumn.ReadOnly = true;
            this.colPersonTypeDescription.Visible = true;
            this.colPersonTypeDescription.VisibleIndex = 1;
            this.colPersonTypeDescription.Width = 106;
            // 
            // colPersonTypeID
            // 
            this.colPersonTypeID.Caption = "Person Type ID";
            this.colPersonTypeID.FieldName = "PersonTypeID";
            this.colPersonTypeID.Name = "colPersonTypeID";
            this.colPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colPersonTypeID.Width = 93;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Remarks";
            this.gridColumn43.ColumnEdit = this.repositoryItemMemoExEdit16;
            this.gridColumn43.FieldName = "Remarks";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 3;
            this.gridColumn43.Width = 179;
            // 
            // repositoryItemMemoExEdit16
            // 
            this.repositoryItemMemoExEdit16.AutoHeight = false;
            this.repositoryItemMemoExEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit16.Name = "repositoryItemMemoExEdit16";
            this.repositoryItemMemoExEdit16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit16.ShowIcon = false;
            // 
            // colResponsibilityType
            // 
            this.colResponsibilityType.Caption = "Responsibility Type";
            this.colResponsibilityType.FieldName = "ResponsibilityType";
            this.colResponsibilityType.Name = "colResponsibilityType";
            this.colResponsibilityType.OptionsColumn.AllowEdit = false;
            this.colResponsibilityType.OptionsColumn.AllowFocus = false;
            this.colResponsibilityType.OptionsColumn.ReadOnly = true;
            this.colResponsibilityType.Visible = true;
            this.colResponsibilityType.VisibleIndex = 0;
            this.colResponsibilityType.Width = 210;
            // 
            // colResponsibilityTypeID
            // 
            this.colResponsibilityTypeID.Caption = "Responsibility Type ID";
            this.colResponsibilityTypeID.FieldName = "ResponsibilityTypeID";
            this.colResponsibilityTypeID.Name = "colResponsibilityTypeID";
            this.colResponsibilityTypeID.OptionsColumn.AllowEdit = false;
            this.colResponsibilityTypeID.OptionsColumn.AllowFocus = false;
            this.colResponsibilityTypeID.OptionsColumn.ReadOnly = true;
            this.colResponsibilityTypeID.Width = 127;
            // 
            // colResponsibleForRecordID
            // 
            this.colResponsibleForRecordID.Caption = "Responsible For Record ID";
            this.colResponsibleForRecordID.FieldName = "ResponsibleForRecordID";
            this.colResponsibleForRecordID.Name = "colResponsibleForRecordID";
            this.colResponsibleForRecordID.OptionsColumn.AllowEdit = false;
            this.colResponsibleForRecordID.OptionsColumn.AllowFocus = false;
            this.colResponsibleForRecordID.OptionsColumn.ReadOnly = true;
            this.colResponsibleForRecordID.Width = 148;
            // 
            // colResponsibleForRecordTypeID
            // 
            this.colResponsibleForRecordTypeID.Caption = "Responsible For Record Type ID";
            this.colResponsibleForRecordTypeID.FieldName = "ResponsibleForRecordTypeID";
            this.colResponsibleForRecordTypeID.Name = "colResponsibleForRecordTypeID";
            this.colResponsibleForRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colResponsibleForRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colResponsibleForRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colResponsibleForRecordTypeID.Width = 175;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Person Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Visible = true;
            this.colStaffName.VisibleIndex = 2;
            this.colStaffName.Width = 205;
            // 
            // colTeamName2
            // 
            this.colTeamName2.Caption = "Team Name";
            this.colTeamName2.FieldName = "TeamName";
            this.colTeamName2.Name = "colTeamName2";
            this.colTeamName2.OptionsColumn.AllowEdit = false;
            this.colTeamName2.OptionsColumn.AllowFocus = false;
            this.colTeamName2.OptionsColumn.ReadOnly = true;
            this.colTeamName2.Visible = true;
            this.colTeamName2.VisibleIndex = 2;
            this.colTeamName2.Width = 203;
            // 
            // repositoryItemTextEdit9
            // 
            this.repositoryItemTextEdit9.AutoHeight = false;
            this.repositoryItemTextEdit9.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEdit9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit9.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit9.Name = "repositoryItemTextEdit9";
            // 
            // repositoryItemTextEditHTML16
            // 
            this.repositoryItemTextEditHTML16.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTextEditHTML16.AutoHeight = false;
            this.repositoryItemTextEditHTML16.Name = "repositoryItemTextEditHTML16";
            // 
            // xtraTabPage8
            // 
            this.xtraTabPage8.Controls.Add(this.gridSplitContainer5);
            this.xtraTabPage8.Name = "xtraTabPage8";
            this.xtraTabPage8.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage8.Text = "Team Holidays";
            // 
            // gridSplitContainer5
            // 
            this.gridSplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer5.Grid = this.gridControl11;
            this.gridSplitContainer5.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer5.Name = "gridSplitContainer5";
            this.gridSplitContainer5.Panel1.Controls.Add(this.gridControl11);
            this.gridSplitContainer5.Size = new System.Drawing.Size(1265, 260);
            this.gridSplitContainer5.TabIndex = 0;
            // 
            // gridControl11
            // 
            this.gridControl11.DataSource = this.sp04152GCHolidaysLinkedToTeamBindingSource;
            this.gridControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl11.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl11.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl11_EmbeddedNavigator_ButtonClick);
            this.gridControl11.Location = new System.Drawing.Point(0, 0);
            this.gridControl11.MainView = this.gridView11;
            this.gridControl11.MenuManager = this.barManager1;
            this.gridControl11.Name = "gridControl11";
            this.gridControl11.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit9,
            this.repositoryItemCheckEdit11});
            this.gridControl11.Size = new System.Drawing.Size(1265, 260);
            this.gridControl11.TabIndex = 0;
            this.gridControl11.UseEmbeddedNavigator = true;
            this.gridControl11.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView11});
            // 
            // sp04152GCHolidaysLinkedToTeamBindingSource
            // 
            this.sp04152GCHolidaysLinkedToTeamBindingSource.DataMember = "sp04152_GC_Holidays_Linked_To_Team";
            this.sp04152GCHolidaysLinkedToTeamBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSubContractorHolidayID,
            this.colSubContractorID4,
            this.colHolidayDate,
            this.colTeamNotAvailable,
            this.colTeamName1,
            this.colRemarks6,
            this.colHolidayTypeID,
            this.colHolidayType});
            this.gridView11.GridControl = this.gridControl11;
            this.gridView11.GroupCount = 1;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsSelection.MultiSelect = true;
            this.gridView11.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHolidayDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView11.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView11.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView11.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView11.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView11.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView11_MouseUp);
            this.gridView11.DoubleClick += new System.EventHandler(this.gridView11_DoubleClick);
            this.gridView11.GotFocus += new System.EventHandler(this.gridView11_GotFocus);
            // 
            // colSubContractorHolidayID
            // 
            this.colSubContractorHolidayID.Caption = "Team Holiday ID";
            this.colSubContractorHolidayID.FieldName = "SubContractorHolidayID";
            this.colSubContractorHolidayID.Name = "colSubContractorHolidayID";
            this.colSubContractorHolidayID.OptionsColumn.AllowEdit = false;
            this.colSubContractorHolidayID.OptionsColumn.AllowFocus = false;
            this.colSubContractorHolidayID.OptionsColumn.ReadOnly = true;
            this.colSubContractorHolidayID.Width = 99;
            // 
            // colSubContractorID4
            // 
            this.colSubContractorID4.Caption = "Team ID";
            this.colSubContractorID4.FieldName = "SubContractorID";
            this.colSubContractorID4.Name = "colSubContractorID4";
            this.colSubContractorID4.OptionsColumn.AllowEdit = false;
            this.colSubContractorID4.OptionsColumn.AllowFocus = false;
            this.colSubContractorID4.OptionsColumn.ReadOnly = true;
            // 
            // colHolidayDate
            // 
            this.colHolidayDate.Caption = "Holiday Date";
            this.colHolidayDate.FieldName = "HolidayDate";
            this.colHolidayDate.Name = "colHolidayDate";
            this.colHolidayDate.OptionsColumn.AllowEdit = false;
            this.colHolidayDate.OptionsColumn.AllowFocus = false;
            this.colHolidayDate.OptionsColumn.ReadOnly = true;
            this.colHolidayDate.Visible = true;
            this.colHolidayDate.VisibleIndex = 0;
            this.colHolidayDate.Width = 102;
            // 
            // colTeamNotAvailable
            // 
            this.colTeamNotAvailable.Caption = "Team Not Available";
            this.colTeamNotAvailable.ColumnEdit = this.repositoryItemCheckEdit11;
            this.colTeamNotAvailable.FieldName = "TeamNotAvailable";
            this.colTeamNotAvailable.Name = "colTeamNotAvailable";
            this.colTeamNotAvailable.OptionsColumn.AllowEdit = false;
            this.colTeamNotAvailable.OptionsColumn.AllowFocus = false;
            this.colTeamNotAvailable.OptionsColumn.ReadOnly = true;
            this.colTeamNotAvailable.Visible = true;
            this.colTeamNotAvailable.VisibleIndex = 2;
            this.colTeamNotAvailable.Width = 113;
            // 
            // repositoryItemCheckEdit11
            // 
            this.repositoryItemCheckEdit11.AutoHeight = false;
            this.repositoryItemCheckEdit11.Caption = "Check";
            this.repositoryItemCheckEdit11.Name = "repositoryItemCheckEdit11";
            this.repositoryItemCheckEdit11.ValueChecked = 1;
            this.repositoryItemCheckEdit11.ValueUnchecked = 0;
            // 
            // colTeamName1
            // 
            this.colTeamName1.Caption = "Team Name";
            this.colTeamName1.FieldName = "TeamName";
            this.colTeamName1.Name = "colTeamName1";
            this.colTeamName1.OptionsColumn.AllowEdit = false;
            this.colTeamName1.OptionsColumn.AllowFocus = false;
            this.colTeamName1.OptionsColumn.ReadOnly = true;
            this.colTeamName1.Width = 200;
            // 
            // colRemarks6
            // 
            this.colRemarks6.Caption = "Remarks";
            this.colRemarks6.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colRemarks6.FieldName = "Remarks";
            this.colRemarks6.Name = "colRemarks6";
            this.colRemarks6.OptionsColumn.ReadOnly = true;
            this.colRemarks6.Visible = true;
            this.colRemarks6.VisibleIndex = 3;
            this.colRemarks6.Width = 204;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // colHolidayTypeID
            // 
            this.colHolidayTypeID.Caption = "Holiday Type ID";
            this.colHolidayTypeID.FieldName = "HolidayTypeID";
            this.colHolidayTypeID.Name = "colHolidayTypeID";
            this.colHolidayTypeID.OptionsColumn.AllowEdit = false;
            this.colHolidayTypeID.OptionsColumn.AllowFocus = false;
            this.colHolidayTypeID.OptionsColumn.ReadOnly = true;
            this.colHolidayTypeID.Width = 97;
            // 
            // colHolidayType
            // 
            this.colHolidayType.Caption = "Holiday Type";
            this.colHolidayType.FieldName = "HolidayType";
            this.colHolidayType.Name = "colHolidayType";
            this.colHolidayType.OptionsColumn.AllowEdit = false;
            this.colHolidayType.OptionsColumn.AllowFocus = false;
            this.colHolidayType.OptionsColumn.ReadOnly = true;
            this.colHolidayType.Visible = true;
            this.colHolidayType.VisibleIndex = 1;
            this.colHolidayType.Width = 247;
            // 
            // xtraTabPageInsurance
            // 
            this.xtraTabPageInsurance.Controls.Add(this.gridControl17);
            this.xtraTabPageInsurance.Name = "xtraTabPageInsurance";
            this.xtraTabPageInsurance.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPageInsurance.Text = "Insurance";
            // 
            // gridControl17
            // 
            this.gridControl17.DataSource = this.sp00245CoreTeamInsuranceBindingSource;
            this.gridControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl17.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl17.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl17.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl17.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl17.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view")});
            this.gridControl17.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl17_EmbeddedNavigator_ButtonClick);
            this.gridControl17.Location = new System.Drawing.Point(0, 0);
            this.gridControl17.MainView = this.gridView17;
            this.gridControl17.MenuManager = this.barManager1;
            this.gridControl17.Name = "gridControl17";
            this.gridControl17.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit15,
            this.repositoryItemTextEditCurrency1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemHyperLinkEditInsurance,
            this.repositoryItemCheckEdit14});
            this.gridControl17.Size = new System.Drawing.Size(1265, 260);
            this.gridControl17.TabIndex = 1;
            this.gridControl17.UseEmbeddedNavigator = true;
            this.gridControl17.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView17});
            // 
            // sp00245CoreTeamInsuranceBindingSource
            // 
            this.sp00245CoreTeamInsuranceBindingSource.DataMember = "sp00245_Core_Team_Insurance";
            this.sp00245CoreTeamInsuranceBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView17
            // 
            this.gridView17.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView17.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView17.ColumnPanelRowHeight = 30;
            this.gridView17.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInsuranceID,
            this.colTeamID,
            this.colStartDate1,
            this.colEndDate1,
            this.colRemarks9,
            this.colLinkedCertificate,
            this.colGrittingPublicValue,
            this.colGrittingEmployersValue,
            this.colSnowClearancePublicValue,
            this.colSnowClearanceEmployersValue,
            this.colMaintenancePublicValue,
            this.colMaintenanceEmployersValue,
            this.colConstructionPublicValue,
            this.colConstructionEmployersValue,
            this.colAmenityArbPublicValue,
            this.colAmenityArbEmployersValue,
            this.colUtilityArbPublicValue,
            this.colUtilityArbEmployersValue,
            this.colRailArbPublicValue,
            this.colRailArbEmployersValue,
            this.colPestControlPublicValue,
            this.colPestControlEmployersValue,
            this.colFencingPublicValue,
            this.colFencingEmployersValue,
            this.colRoofingPublicValue,
            this.colRoofingEmployersValue,
            this.colWindowCleaningPublicValue,
            this.colWindowCleaningEmployersValue,
            this.colPotholePublicValue,
            this.colPotholeEmployersValue,
            this.colSubContractorName6,
            this.colActive1,
            this.colMinGrittingPublicValue,
            this.colMinGrittingEmployersValue,
            this.colMinSnowClearancePublicValue,
            this.colMinSnowClearanceEmployersValue,
            this.colMinMaintenancePublicValue,
            this.colMinMaintenanceEmployersValue,
            this.colMinConstructionPublicValue,
            this.colMinConstructionEmployersValue,
            this.colMinAmenityArbPublicValue,
            this.colMinAmenityArbEmployersValue,
            this.colMinUtilityArbPublicValue,
            this.colMinUtilityArbEmployersValue,
            this.colMinRailArbPublicValue,
            this.colMinRailArbEmployersValue,
            this.colMinPestControlPublicValue,
            this.colMinPestControlEmployersValue,
            this.colMinFencingPublicValue,
            this.colMinFencingEmployersValue,
            this.colMinRoofingPublicValue,
            this.colMinRoofingEmployersValue,
            this.colMinWindowCleaningPublicValue,
            this.colMinWindowCleaningEmployersValue,
            this.colMinPotholePublicValue,
            this.colMinPotholeEmployersValue});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive1;
            gridFormatRule1.Name = "Format_Grey_InActive";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView17.FormatRules.Add(gridFormatRule1);
            this.gridView17.GridControl = this.gridControl17;
            this.gridView17.GroupCount = 1;
            this.gridView17.Name = "gridView17";
            this.gridView17.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView17.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView17.OptionsLayout.StoreAppearance = true;
            this.gridView17.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView17.OptionsSelection.MultiSelect = true;
            this.gridView17.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView17.OptionsView.ColumnAutoWidth = false;
            this.gridView17.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView17.OptionsView.ShowGroupPanel = false;
            this.gridView17.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName6, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate1, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEndDate1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView17.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView17_CustomDrawCell);
            this.gridView17.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView17.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView17.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView17.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView17.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView17.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView17.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView17_MouseUp);
            this.gridView17.DoubleClick += new System.EventHandler(this.gridView17_DoubleClick);
            this.gridView17.GotFocus += new System.EventHandler(this.gridView17_GotFocus);
            // 
            // colInsuranceID
            // 
            this.colInsuranceID.Caption = "Insurance ID";
            this.colInsuranceID.FieldName = "InsuranceID";
            this.colInsuranceID.Name = "colInsuranceID";
            this.colInsuranceID.OptionsColumn.AllowEdit = false;
            this.colInsuranceID.OptionsColumn.AllowFocus = false;
            this.colInsuranceID.OptionsColumn.ReadOnly = true;
            this.colInsuranceID.Width = 81;
            // 
            // colTeamID
            // 
            this.colTeamID.Caption = "Team ID";
            this.colTeamID.FieldName = "TeamID";
            this.colTeamID.Name = "colTeamID";
            this.colTeamID.OptionsColumn.AllowEdit = false;
            this.colTeamID.OptionsColumn.AllowFocus = false;
            this.colTeamID.OptionsColumn.ReadOnly = true;
            // 
            // colStartDate1
            // 
            this.colStartDate1.Caption = "Start Date";
            this.colStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 0;
            this.colStartDate1.Width = 127;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colEndDate1
            // 
            this.colEndDate1.Caption = "End Date";
            this.colEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 1;
            this.colEndDate1.Width = 106;
            // 
            // colRemarks9
            // 
            this.colRemarks9.Caption = "Remarks";
            this.colRemarks9.ColumnEdit = this.repositoryItemMemoExEdit15;
            this.colRemarks9.FieldName = "Remarks";
            this.colRemarks9.Name = "colRemarks9";
            this.colRemarks9.OptionsColumn.ReadOnly = true;
            this.colRemarks9.Visible = true;
            this.colRemarks9.VisibleIndex = 28;
            // 
            // repositoryItemMemoExEdit15
            // 
            this.repositoryItemMemoExEdit15.AutoHeight = false;
            this.repositoryItemMemoExEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit15.Name = "repositoryItemMemoExEdit15";
            this.repositoryItemMemoExEdit15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit15.ShowIcon = false;
            // 
            // colLinkedCertificate
            // 
            this.colLinkedCertificate.Caption = "Linked Certificate";
            this.colLinkedCertificate.ColumnEdit = this.repositoryItemHyperLinkEditInsurance;
            this.colLinkedCertificate.FieldName = "LinkedCertificate";
            this.colLinkedCertificate.Name = "colLinkedCertificate";
            this.colLinkedCertificate.OptionsColumn.ReadOnly = true;
            this.colLinkedCertificate.Visible = true;
            this.colLinkedCertificate.VisibleIndex = 27;
            this.colLinkedCertificate.Width = 277;
            // 
            // repositoryItemHyperLinkEditInsurance
            // 
            this.repositoryItemHyperLinkEditInsurance.AutoHeight = false;
            this.repositoryItemHyperLinkEditInsurance.Name = "repositoryItemHyperLinkEditInsurance";
            this.repositoryItemHyperLinkEditInsurance.SingleClick = true;
            this.repositoryItemHyperLinkEditInsurance.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditInsurance_OpenLink);
            // 
            // colGrittingPublicValue
            // 
            this.colGrittingPublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colGrittingPublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colGrittingPublicValue.Caption = "Gritting<br>Public";
            this.colGrittingPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colGrittingPublicValue.FieldName = "GrittingPublicValue";
            this.colGrittingPublicValue.Name = "colGrittingPublicValue";
            this.colGrittingPublicValue.OptionsColumn.AllowEdit = false;
            this.colGrittingPublicValue.OptionsColumn.AllowFocus = false;
            this.colGrittingPublicValue.OptionsColumn.ReadOnly = true;
            this.colGrittingPublicValue.Visible = true;
            this.colGrittingPublicValue.VisibleIndex = 3;
            // 
            // repositoryItemTextEditCurrency1
            // 
            this.repositoryItemTextEditCurrency1.AutoHeight = false;
            this.repositoryItemTextEditCurrency1.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency1.Name = "repositoryItemTextEditCurrency1";
            // 
            // colGrittingEmployersValue
            // 
            this.colGrittingEmployersValue.Caption = "Gritting<br>Employers";
            this.colGrittingEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colGrittingEmployersValue.FieldName = "GrittingEmployersValue";
            this.colGrittingEmployersValue.Name = "colGrittingEmployersValue";
            this.colGrittingEmployersValue.OptionsColumn.AllowEdit = false;
            this.colGrittingEmployersValue.OptionsColumn.AllowFocus = false;
            this.colGrittingEmployersValue.OptionsColumn.ReadOnly = true;
            this.colGrittingEmployersValue.Visible = true;
            this.colGrittingEmployersValue.VisibleIndex = 4;
            // 
            // colSnowClearancePublicValue
            // 
            this.colSnowClearancePublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colSnowClearancePublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colSnowClearancePublicValue.Caption = "Snow Clearance<br>Public";
            this.colSnowClearancePublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colSnowClearancePublicValue.FieldName = "SnowClearancePublicValue";
            this.colSnowClearancePublicValue.Name = "colSnowClearancePublicValue";
            this.colSnowClearancePublicValue.OptionsColumn.AllowEdit = false;
            this.colSnowClearancePublicValue.OptionsColumn.AllowFocus = false;
            this.colSnowClearancePublicValue.OptionsColumn.ReadOnly = true;
            this.colSnowClearancePublicValue.Visible = true;
            this.colSnowClearancePublicValue.VisibleIndex = 5;
            this.colSnowClearancePublicValue.Width = 96;
            // 
            // colSnowClearanceEmployersValue
            // 
            this.colSnowClearanceEmployersValue.Caption = "Snow Clearance<br>Employers";
            this.colSnowClearanceEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colSnowClearanceEmployersValue.FieldName = "SnowClearanceEmployersValue";
            this.colSnowClearanceEmployersValue.Name = "colSnowClearanceEmployersValue";
            this.colSnowClearanceEmployersValue.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceEmployersValue.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceEmployersValue.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceEmployersValue.Visible = true;
            this.colSnowClearanceEmployersValue.VisibleIndex = 6;
            this.colSnowClearanceEmployersValue.Width = 96;
            // 
            // colMaintenancePublicValue
            // 
            this.colMaintenancePublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colMaintenancePublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colMaintenancePublicValue.Caption = "Maintenance<br>Public";
            this.colMaintenancePublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMaintenancePublicValue.FieldName = "MaintenancePublicValue";
            this.colMaintenancePublicValue.Name = "colMaintenancePublicValue";
            this.colMaintenancePublicValue.OptionsColumn.AllowEdit = false;
            this.colMaintenancePublicValue.OptionsColumn.AllowFocus = false;
            this.colMaintenancePublicValue.OptionsColumn.ReadOnly = true;
            this.colMaintenancePublicValue.Visible = true;
            this.colMaintenancePublicValue.VisibleIndex = 7;
            this.colMaintenancePublicValue.Width = 80;
            // 
            // colMaintenanceEmployersValue
            // 
            this.colMaintenanceEmployersValue.Caption = "Maintenance<br>Employers";
            this.colMaintenanceEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMaintenanceEmployersValue.FieldName = "MaintenanceEmployersValue";
            this.colMaintenanceEmployersValue.Name = "colMaintenanceEmployersValue";
            this.colMaintenanceEmployersValue.OptionsColumn.AllowEdit = false;
            this.colMaintenanceEmployersValue.OptionsColumn.AllowFocus = false;
            this.colMaintenanceEmployersValue.OptionsColumn.ReadOnly = true;
            this.colMaintenanceEmployersValue.Visible = true;
            this.colMaintenanceEmployersValue.VisibleIndex = 8;
            this.colMaintenanceEmployersValue.Width = 80;
            // 
            // colConstructionPublicValue
            // 
            this.colConstructionPublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colConstructionPublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colConstructionPublicValue.Caption = "Construction<br>Public";
            this.colConstructionPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colConstructionPublicValue.FieldName = "ConstructionPublicValue";
            this.colConstructionPublicValue.Name = "colConstructionPublicValue";
            this.colConstructionPublicValue.OptionsColumn.AllowEdit = false;
            this.colConstructionPublicValue.OptionsColumn.AllowFocus = false;
            this.colConstructionPublicValue.OptionsColumn.ReadOnly = true;
            this.colConstructionPublicValue.Visible = true;
            this.colConstructionPublicValue.VisibleIndex = 9;
            this.colConstructionPublicValue.Width = 80;
            // 
            // colConstructionEmployersValue
            // 
            this.colConstructionEmployersValue.Caption = "Construction<br>Employers";
            this.colConstructionEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colConstructionEmployersValue.FieldName = "ConstructionEmployersValue";
            this.colConstructionEmployersValue.Name = "colConstructionEmployersValue";
            this.colConstructionEmployersValue.OptionsColumn.AllowEdit = false;
            this.colConstructionEmployersValue.OptionsColumn.AllowFocus = false;
            this.colConstructionEmployersValue.OptionsColumn.ReadOnly = true;
            this.colConstructionEmployersValue.Visible = true;
            this.colConstructionEmployersValue.VisibleIndex = 10;
            this.colConstructionEmployersValue.Width = 80;
            // 
            // colAmenityArbPublicValue
            // 
            this.colAmenityArbPublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colAmenityArbPublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colAmenityArbPublicValue.Caption = "Amenity Arb<br>Public";
            this.colAmenityArbPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colAmenityArbPublicValue.FieldName = "AmenityArbPublicValue";
            this.colAmenityArbPublicValue.Name = "colAmenityArbPublicValue";
            this.colAmenityArbPublicValue.OptionsColumn.AllowEdit = false;
            this.colAmenityArbPublicValue.OptionsColumn.AllowFocus = false;
            this.colAmenityArbPublicValue.OptionsColumn.ReadOnly = true;
            this.colAmenityArbPublicValue.Visible = true;
            this.colAmenityArbPublicValue.VisibleIndex = 11;
            this.colAmenityArbPublicValue.Width = 78;
            // 
            // colAmenityArbEmployersValue
            // 
            this.colAmenityArbEmployersValue.Caption = "Amenity Arb<br>Employers";
            this.colAmenityArbEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colAmenityArbEmployersValue.FieldName = "AmenityArbEmployersValue";
            this.colAmenityArbEmployersValue.Name = "colAmenityArbEmployersValue";
            this.colAmenityArbEmployersValue.OptionsColumn.AllowEdit = false;
            this.colAmenityArbEmployersValue.OptionsColumn.AllowFocus = false;
            this.colAmenityArbEmployersValue.OptionsColumn.ReadOnly = true;
            this.colAmenityArbEmployersValue.Visible = true;
            this.colAmenityArbEmployersValue.VisibleIndex = 12;
            this.colAmenityArbEmployersValue.Width = 78;
            // 
            // colUtilityArbPublicValue
            // 
            this.colUtilityArbPublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colUtilityArbPublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colUtilityArbPublicValue.Caption = "Utility Arb<br>Public";
            this.colUtilityArbPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colUtilityArbPublicValue.FieldName = "UtilityArbPublicValue";
            this.colUtilityArbPublicValue.Name = "colUtilityArbPublicValue";
            this.colUtilityArbPublicValue.OptionsColumn.AllowEdit = false;
            this.colUtilityArbPublicValue.OptionsColumn.AllowFocus = false;
            this.colUtilityArbPublicValue.OptionsColumn.ReadOnly = true;
            this.colUtilityArbPublicValue.Visible = true;
            this.colUtilityArbPublicValue.VisibleIndex = 13;
            // 
            // colUtilityArbEmployersValue
            // 
            this.colUtilityArbEmployersValue.Caption = "Utility Arb<br>Employers";
            this.colUtilityArbEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colUtilityArbEmployersValue.FieldName = "UtilityArbEmployersValue";
            this.colUtilityArbEmployersValue.Name = "colUtilityArbEmployersValue";
            this.colUtilityArbEmployersValue.OptionsColumn.AllowEdit = false;
            this.colUtilityArbEmployersValue.OptionsColumn.AllowFocus = false;
            this.colUtilityArbEmployersValue.OptionsColumn.ReadOnly = true;
            this.colUtilityArbEmployersValue.Visible = true;
            this.colUtilityArbEmployersValue.VisibleIndex = 14;
            // 
            // colRailArbPublicValue
            // 
            this.colRailArbPublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colRailArbPublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colRailArbPublicValue.Caption = "Rail Arb<br>Public";
            this.colRailArbPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colRailArbPublicValue.FieldName = "RailArbPublicValue";
            this.colRailArbPublicValue.Name = "colRailArbPublicValue";
            this.colRailArbPublicValue.OptionsColumn.AllowEdit = false;
            this.colRailArbPublicValue.OptionsColumn.AllowFocus = false;
            this.colRailArbPublicValue.OptionsColumn.ReadOnly = true;
            this.colRailArbPublicValue.Visible = true;
            this.colRailArbPublicValue.VisibleIndex = 15;
            // 
            // colRailArbEmployersValue
            // 
            this.colRailArbEmployersValue.Caption = "Rail Arb<br>Employers";
            this.colRailArbEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colRailArbEmployersValue.FieldName = "RailArbEmployersValue";
            this.colRailArbEmployersValue.Name = "colRailArbEmployersValue";
            this.colRailArbEmployersValue.OptionsColumn.AllowEdit = false;
            this.colRailArbEmployersValue.OptionsColumn.AllowFocus = false;
            this.colRailArbEmployersValue.OptionsColumn.ReadOnly = true;
            this.colRailArbEmployersValue.Visible = true;
            this.colRailArbEmployersValue.VisibleIndex = 16;
            // 
            // colPestControlPublicValue
            // 
            this.colPestControlPublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colPestControlPublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colPestControlPublicValue.Caption = "Pest Control<br>Public";
            this.colPestControlPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colPestControlPublicValue.FieldName = "PestControlPublicValue";
            this.colPestControlPublicValue.Name = "colPestControlPublicValue";
            this.colPestControlPublicValue.OptionsColumn.AllowEdit = false;
            this.colPestControlPublicValue.OptionsColumn.AllowFocus = false;
            this.colPestControlPublicValue.OptionsColumn.ReadOnly = true;
            this.colPestControlPublicValue.Visible = true;
            this.colPestControlPublicValue.VisibleIndex = 17;
            this.colPestControlPublicValue.Width = 78;
            // 
            // colPestControlEmployersValue
            // 
            this.colPestControlEmployersValue.Caption = "Pest Control<br>Employers";
            this.colPestControlEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colPestControlEmployersValue.FieldName = "PestControlEmployersValue";
            this.colPestControlEmployersValue.Name = "colPestControlEmployersValue";
            this.colPestControlEmployersValue.OptionsColumn.AllowEdit = false;
            this.colPestControlEmployersValue.OptionsColumn.AllowFocus = false;
            this.colPestControlEmployersValue.OptionsColumn.ReadOnly = true;
            this.colPestControlEmployersValue.Visible = true;
            this.colPestControlEmployersValue.VisibleIndex = 18;
            this.colPestControlEmployersValue.Width = 78;
            // 
            // colFencingPublicValue
            // 
            this.colFencingPublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colFencingPublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colFencingPublicValue.Caption = "Fencing<br>Public";
            this.colFencingPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colFencingPublicValue.FieldName = "FencingPublicValue";
            this.colFencingPublicValue.Name = "colFencingPublicValue";
            this.colFencingPublicValue.OptionsColumn.AllowEdit = false;
            this.colFencingPublicValue.OptionsColumn.AllowFocus = false;
            this.colFencingPublicValue.OptionsColumn.ReadOnly = true;
            this.colFencingPublicValue.Visible = true;
            this.colFencingPublicValue.VisibleIndex = 19;
            // 
            // colFencingEmployersValue
            // 
            this.colFencingEmployersValue.Caption = "Fencing<br>Employers";
            this.colFencingEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colFencingEmployersValue.FieldName = "FencingEmployersValue";
            this.colFencingEmployersValue.Name = "colFencingEmployersValue";
            this.colFencingEmployersValue.OptionsColumn.AllowEdit = false;
            this.colFencingEmployersValue.OptionsColumn.AllowFocus = false;
            this.colFencingEmployersValue.OptionsColumn.ReadOnly = true;
            this.colFencingEmployersValue.Visible = true;
            this.colFencingEmployersValue.VisibleIndex = 20;
            // 
            // colRoofingPublicValue
            // 
            this.colRoofingPublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colRoofingPublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colRoofingPublicValue.Caption = "Roofing<br>Public";
            this.colRoofingPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colRoofingPublicValue.FieldName = "RoofingPublicValue";
            this.colRoofingPublicValue.Name = "colRoofingPublicValue";
            this.colRoofingPublicValue.OptionsColumn.AllowEdit = false;
            this.colRoofingPublicValue.OptionsColumn.AllowFocus = false;
            this.colRoofingPublicValue.OptionsColumn.ReadOnly = true;
            this.colRoofingPublicValue.Visible = true;
            this.colRoofingPublicValue.VisibleIndex = 21;
            // 
            // colRoofingEmployersValue
            // 
            this.colRoofingEmployersValue.Caption = "Roofing<br>Employers";
            this.colRoofingEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colRoofingEmployersValue.FieldName = "RoofingEmployersValue";
            this.colRoofingEmployersValue.Name = "colRoofingEmployersValue";
            this.colRoofingEmployersValue.OptionsColumn.AllowEdit = false;
            this.colRoofingEmployersValue.OptionsColumn.AllowFocus = false;
            this.colRoofingEmployersValue.OptionsColumn.ReadOnly = true;
            this.colRoofingEmployersValue.Visible = true;
            this.colRoofingEmployersValue.VisibleIndex = 22;
            // 
            // colWindowCleaningPublicValue
            // 
            this.colWindowCleaningPublicValue.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colWindowCleaningPublicValue.AppearanceCell.Options.UseBackColor = true;
            this.colWindowCleaningPublicValue.Caption = "Window Cleaning<br>Public";
            this.colWindowCleaningPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colWindowCleaningPublicValue.FieldName = "WindowCleaningPublicValue";
            this.colWindowCleaningPublicValue.Name = "colWindowCleaningPublicValue";
            this.colWindowCleaningPublicValue.OptionsColumn.AllowEdit = false;
            this.colWindowCleaningPublicValue.OptionsColumn.AllowFocus = false;
            this.colWindowCleaningPublicValue.OptionsColumn.ReadOnly = true;
            this.colWindowCleaningPublicValue.Visible = true;
            this.colWindowCleaningPublicValue.VisibleIndex = 23;
            this.colWindowCleaningPublicValue.Width = 101;
            // 
            // colWindowCleaningEmployersValue
            // 
            this.colWindowCleaningEmployersValue.Caption = "Window Cleaning<br>Employers";
            this.colWindowCleaningEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colWindowCleaningEmployersValue.FieldName = "WindowCleaningEmployersValue";
            this.colWindowCleaningEmployersValue.Name = "colWindowCleaningEmployersValue";
            this.colWindowCleaningEmployersValue.OptionsColumn.AllowEdit = false;
            this.colWindowCleaningEmployersValue.OptionsColumn.AllowFocus = false;
            this.colWindowCleaningEmployersValue.OptionsColumn.ReadOnly = true;
            this.colWindowCleaningEmployersValue.Visible = true;
            this.colWindowCleaningEmployersValue.VisibleIndex = 24;
            this.colWindowCleaningEmployersValue.Width = 101;
            // 
            // colPotholePublicValue
            // 
            this.colPotholePublicValue.Caption = "Pothole<br>Public";
            this.colPotholePublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colPotholePublicValue.FieldName = "PotholePublicValue";
            this.colPotholePublicValue.Name = "colPotholePublicValue";
            this.colPotholePublicValue.OptionsColumn.AllowEdit = false;
            this.colPotholePublicValue.OptionsColumn.AllowFocus = false;
            this.colPotholePublicValue.OptionsColumn.ReadOnly = true;
            this.colPotholePublicValue.Visible = true;
            this.colPotholePublicValue.VisibleIndex = 25;
            // 
            // colPotholeEmployersValue
            // 
            this.colPotholeEmployersValue.Caption = "Pothole<br>Employers";
            this.colPotholeEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colPotholeEmployersValue.FieldName = "PotholeEmployersValue";
            this.colPotholeEmployersValue.Name = "colPotholeEmployersValue";
            this.colPotholeEmployersValue.OptionsColumn.AllowEdit = false;
            this.colPotholeEmployersValue.OptionsColumn.AllowFocus = false;
            this.colPotholeEmployersValue.OptionsColumn.ReadOnly = true;
            this.colPotholeEmployersValue.Visible = true;
            this.colPotholeEmployersValue.VisibleIndex = 26;
            // 
            // colSubContractorName6
            // 
            this.colSubContractorName6.Caption = "Team Name";
            this.colSubContractorName6.FieldName = "SubContractorName";
            this.colSubContractorName6.Name = "colSubContractorName6";
            this.colSubContractorName6.OptionsColumn.AllowEdit = false;
            this.colSubContractorName6.OptionsColumn.AllowFocus = false;
            this.colSubContractorName6.OptionsColumn.ReadOnly = true;
            this.colSubContractorName6.Visible = true;
            this.colSubContractorName6.VisibleIndex = 27;
            this.colSubContractorName6.Width = 388;
            // 
            // colMinGrittingPublicValue
            // 
            this.colMinGrittingPublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinGrittingPublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinGrittingPublicValue.Caption = "Gritting<br>Public Min";
            this.colMinGrittingPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinGrittingPublicValue.FieldName = "MinGrittingPublicValue";
            this.colMinGrittingPublicValue.Name = "colMinGrittingPublicValue";
            // 
            // colMinGrittingEmployersValue
            // 
            this.colMinGrittingEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinGrittingEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinGrittingEmployersValue.Caption = "Gritting<br>Employers Min";
            this.colMinGrittingEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinGrittingEmployersValue.FieldName = "MinGrittingEmployersValue";
            this.colMinGrittingEmployersValue.Name = "colMinGrittingEmployersValue";
            this.colMinGrittingEmployersValue.Width = 88;
            // 
            // colMinSnowClearancePublicValue
            // 
            this.colMinSnowClearancePublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinSnowClearancePublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinSnowClearancePublicValue.Caption = "Snow Clearance<br>Public Min";
            this.colMinSnowClearancePublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinSnowClearancePublicValue.FieldName = "MinSnowClearancePublicValue";
            this.colMinSnowClearancePublicValue.Name = "colMinSnowClearancePublicValue";
            this.colMinSnowClearancePublicValue.Width = 97;
            // 
            // colMinSnowClearanceEmployersValue
            // 
            this.colMinSnowClearanceEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinSnowClearanceEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinSnowClearanceEmployersValue.Caption = "Snow Clearance<br>Employers Min";
            this.colMinSnowClearanceEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinSnowClearanceEmployersValue.FieldName = "MinSnowClearanceEmployersValue";
            this.colMinSnowClearanceEmployersValue.Name = "colMinSnowClearanceEmployersValue";
            this.colMinSnowClearanceEmployersValue.Width = 97;
            // 
            // colMinMaintenancePublicValue
            // 
            this.colMinMaintenancePublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinMaintenancePublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinMaintenancePublicValue.Caption = "Maintenance<br>Public Min";
            this.colMinMaintenancePublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinMaintenancePublicValue.FieldName = "MinMaintenancePublicValue";
            this.colMinMaintenancePublicValue.Name = "colMinMaintenancePublicValue";
            this.colMinMaintenancePublicValue.Width = 81;
            // 
            // colMinMaintenanceEmployersValue
            // 
            this.colMinMaintenanceEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinMaintenanceEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinMaintenanceEmployersValue.Caption = "Maintenance<br>Employers Min";
            this.colMinMaintenanceEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinMaintenanceEmployersValue.FieldName = "MinMaintenanceEmployersValue";
            this.colMinMaintenanceEmployersValue.Name = "colMinMaintenanceEmployersValue";
            this.colMinMaintenanceEmployersValue.Width = 88;
            // 
            // colMinConstructionPublicValue
            // 
            this.colMinConstructionPublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinConstructionPublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinConstructionPublicValue.Caption = "Construction<br>Public Min";
            this.colMinConstructionPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinConstructionPublicValue.FieldName = "MinConstructionPublicValue";
            this.colMinConstructionPublicValue.Name = "colMinConstructionPublicValue";
            this.colMinConstructionPublicValue.Width = 81;
            // 
            // colMinConstructionEmployersValue
            // 
            this.colMinConstructionEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinConstructionEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinConstructionEmployersValue.Caption = "Construction<br>Employers Min";
            this.colMinConstructionEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinConstructionEmployersValue.FieldName = "MinConstructionEmployersValue";
            this.colMinConstructionEmployersValue.Name = "colMinConstructionEmployersValue";
            this.colMinConstructionEmployersValue.Width = 88;
            // 
            // colMinAmenityArbPublicValue
            // 
            this.colMinAmenityArbPublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinAmenityArbPublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinAmenityArbPublicValue.Caption = "Amenity Arb<br>Public Min";
            this.colMinAmenityArbPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinAmenityArbPublicValue.FieldName = "MinAmenityArbPublicValue";
            this.colMinAmenityArbPublicValue.Name = "colMinAmenityArbPublicValue";
            this.colMinAmenityArbPublicValue.Width = 79;
            // 
            // colMinAmenityArbEmployersValue
            // 
            this.colMinAmenityArbEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinAmenityArbEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinAmenityArbEmployersValue.Caption = "Amenity Arb<br>Employers Min";
            this.colMinAmenityArbEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinAmenityArbEmployersValue.FieldName = "MinAmenityArbEmployersValue";
            this.colMinAmenityArbEmployersValue.Name = "colMinAmenityArbEmployersValue";
            this.colMinAmenityArbEmployersValue.Width = 88;
            // 
            // colMinUtilityArbPublicValue
            // 
            this.colMinUtilityArbPublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinUtilityArbPublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinUtilityArbPublicValue.Caption = "Utility Arb<br>Public Min";
            this.colMinUtilityArbPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinUtilityArbPublicValue.FieldName = "MinUtilityArbPublicValue";
            this.colMinUtilityArbPublicValue.Name = "colMinUtilityArbPublicValue";
            // 
            // colMinUtilityArbEmployersValue
            // 
            this.colMinUtilityArbEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinUtilityArbEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinUtilityArbEmployersValue.Caption = "Utility Arb<br>Employers Min";
            this.colMinUtilityArbEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinUtilityArbEmployersValue.FieldName = "MinUtilityArbEmployersValue";
            this.colMinUtilityArbEmployersValue.Name = "colMinUtilityArbEmployersValue";
            this.colMinUtilityArbEmployersValue.Width = 88;
            // 
            // colMinRailArbPublicValue
            // 
            this.colMinRailArbPublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinRailArbPublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinRailArbPublicValue.Caption = "Rail Arb<br>Public Min";
            this.colMinRailArbPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinRailArbPublicValue.FieldName = "MinRailArbPublicValue";
            this.colMinRailArbPublicValue.Name = "colMinRailArbPublicValue";
            // 
            // colMinRailArbEmployersValue
            // 
            this.colMinRailArbEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinRailArbEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinRailArbEmployersValue.Caption = "Rail Arb<br>Employers Min";
            this.colMinRailArbEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinRailArbEmployersValue.FieldName = "MinRailArbEmployersValue";
            this.colMinRailArbEmployersValue.Name = "colMinRailArbEmployersValue";
            this.colMinRailArbEmployersValue.Width = 88;
            // 
            // colMinPestControlPublicValue
            // 
            this.colMinPestControlPublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinPestControlPublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinPestControlPublicValue.Caption = "Pest Control<br>Public Min";
            this.colMinPestControlPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinPestControlPublicValue.FieldName = "MinPestControlPublicValue";
            this.colMinPestControlPublicValue.Name = "colMinPestControlPublicValue";
            this.colMinPestControlPublicValue.Width = 80;
            // 
            // colMinPestControlEmployersValue
            // 
            this.colMinPestControlEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinPestControlEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinPestControlEmployersValue.Caption = "Pest Control<br>Employers Min";
            this.colMinPestControlEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinPestControlEmployersValue.FieldName = "MinPestControlEmployersValue";
            this.colMinPestControlEmployersValue.Name = "colMinPestControlEmployersValue";
            this.colMinPestControlEmployersValue.Width = 88;
            // 
            // colMinFencingPublicValue
            // 
            this.colMinFencingPublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinFencingPublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinFencingPublicValue.Caption = "Fencing<br>Public Min";
            this.colMinFencingPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinFencingPublicValue.FieldName = "MinFencingPublicValue";
            this.colMinFencingPublicValue.Name = "colMinFencingPublicValue";
            // 
            // colMinFencingEmployersValue
            // 
            this.colMinFencingEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinFencingEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinFencingEmployersValue.Caption = "Fencing<br>Employers Min";
            this.colMinFencingEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinFencingEmployersValue.FieldName = "MinFencingEmployersValue";
            this.colMinFencingEmployersValue.Name = "colMinFencingEmployersValue";
            this.colMinFencingEmployersValue.Width = 88;
            // 
            // colMinRoofingPublicValue
            // 
            this.colMinRoofingPublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinRoofingPublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinRoofingPublicValue.Caption = "Roofing<br>Public Min";
            this.colMinRoofingPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinRoofingPublicValue.FieldName = "MinRoofingPublicValue";
            this.colMinRoofingPublicValue.Name = "colMinRoofingPublicValue";
            // 
            // colMinRoofingEmployersValue
            // 
            this.colMinRoofingEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinRoofingEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinRoofingEmployersValue.Caption = "Roofing<br>Employers Min";
            this.colMinRoofingEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinRoofingEmployersValue.FieldName = "MinRoofingEmployersValue";
            this.colMinRoofingEmployersValue.Name = "colMinRoofingEmployersValue";
            this.colMinRoofingEmployersValue.Width = 88;
            // 
            // colMinWindowCleaningPublicValue
            // 
            this.colMinWindowCleaningPublicValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinWindowCleaningPublicValue.AppearanceHeader.Options.UseFont = true;
            this.colMinWindowCleaningPublicValue.Caption = "Window Cleaning<br>Public Min";
            this.colMinWindowCleaningPublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinWindowCleaningPublicValue.FieldName = "MinWindowCleaningPublicValue";
            this.colMinWindowCleaningPublicValue.Name = "colMinWindowCleaningPublicValue";
            this.colMinWindowCleaningPublicValue.Width = 102;
            // 
            // colMinWindowCleaningEmployersValue
            // 
            this.colMinWindowCleaningEmployersValue.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.colMinWindowCleaningEmployersValue.AppearanceHeader.Options.UseFont = true;
            this.colMinWindowCleaningEmployersValue.Caption = "Window Cleaning<br>Employers Min";
            this.colMinWindowCleaningEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinWindowCleaningEmployersValue.FieldName = "MinWindowCleaningEmployersValue";
            this.colMinWindowCleaningEmployersValue.Name = "colMinWindowCleaningEmployersValue";
            this.colMinWindowCleaningEmployersValue.Width = 102;
            // 
            // colMinPotholePublicValue
            // 
            this.colMinPotholePublicValue.Caption = "Pothole<br>Public Min";
            this.colMinPotholePublicValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinPotholePublicValue.FieldName = "MinPotholePublicValue";
            this.colMinPotholePublicValue.Name = "colMinPotholePublicValue";
            // 
            // colMinPotholeEmployersValue
            // 
            this.colMinPotholeEmployersValue.Caption = "Pothole<br>Employers Min";
            this.colMinPotholeEmployersValue.ColumnEdit = this.repositoryItemTextEditCurrency1;
            this.colMinPotholeEmployersValue.FieldName = "MinPotholeEmployersValue";
            this.colMinPotholeEmployersValue.Name = "colMinPotholeEmployersValue";
            // 
            // xtraTabPageInsuranceCategories
            // 
            this.xtraTabPageInsuranceCategories.Controls.Add(this.gridControlInsuranceCategory);
            this.xtraTabPageInsuranceCategories.Name = "xtraTabPageInsuranceCategories";
            this.xtraTabPageInsuranceCategories.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPageInsuranceCategories.Text = "Insurance Categories";
            // 
            // gridControlInsuranceCategory
            // 
            this.gridControlInsuranceCategory.DataSource = this.sp00250CoreTeamInsuranceCategoryListBindingSource;
            this.gridControlInsuranceCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlInsuranceCategory.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlInsuranceCategory.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view")});
            this.gridControlInsuranceCategory.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl18_EmbeddedNavigator_ButtonClick);
            this.gridControlInsuranceCategory.Location = new System.Drawing.Point(0, 0);
            this.gridControlInsuranceCategory.MainView = this.gridViewInsuranceCategory;
            this.gridControlInsuranceCategory.MenuManager = this.barManager1;
            this.gridControlInsuranceCategory.Name = "gridControlInsuranceCategory";
            this.gridControlInsuranceCategory.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit17,
            this.repositoryItemTextEdit7,
            this.repositoryItemTextEdit6,
            this.repositoryItemHyperLinkEdit9,
            this.repositoryItemCheckEdit3});
            this.gridControlInsuranceCategory.Size = new System.Drawing.Size(1265, 260);
            this.gridControlInsuranceCategory.TabIndex = 2;
            this.gridControlInsuranceCategory.UseEmbeddedNavigator = true;
            this.gridControlInsuranceCategory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInsuranceCategory});
            // 
            // sp00250CoreTeamInsuranceCategoryListBindingSource
            // 
            this.sp00250CoreTeamInsuranceCategoryListBindingSource.DataMember = "sp00250_Core_Team_Insurance_Category_List";
            this.sp00250CoreTeamInsuranceCategoryListBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridViewInsuranceCategory
            // 
            this.gridViewInsuranceCategory.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridViewInsuranceCategory.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridViewInsuranceCategory.ColumnPanelRowHeight = 30;
            this.gridViewInsuranceCategory.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTeamInsuranceCategoryID,
            this.colTeamID1,
            this.colTeamName3,
            this.colInsuranceCategoryID,
            this.colCategory,
            this.colCategoryStateID,
            this.colCategoryState,
            this.colActive2,
            this.colStartDate2,
            this.colEndDate2});
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Name = "Format_Grey_InActive";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridViewInsuranceCategory.FormatRules.Add(gridFormatRule2);
            this.gridViewInsuranceCategory.GridControl = this.gridControlInsuranceCategory;
            this.gridViewInsuranceCategory.GroupCount = 1;
            this.gridViewInsuranceCategory.Name = "gridViewInsuranceCategory";
            this.gridViewInsuranceCategory.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewInsuranceCategory.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewInsuranceCategory.OptionsLayout.StoreAppearance = true;
            this.gridViewInsuranceCategory.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewInsuranceCategory.OptionsSelection.MultiSelect = true;
            this.gridViewInsuranceCategory.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewInsuranceCategory.OptionsView.ColumnAutoWidth = false;
            this.gridViewInsuranceCategory.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewInsuranceCategory.OptionsView.ShowGroupPanel = false;
            this.gridViewInsuranceCategory.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamName3, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewInsuranceCategory.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView18_CustomDrawCell);
            this.gridViewInsuranceCategory.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewInsuranceCategory.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewInsuranceCategory.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewInsuranceCategory.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewInsuranceCategory.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewInsuranceCategory.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewInsuranceCategory.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView18_MouseUp);
            this.gridViewInsuranceCategory.DoubleClick += new System.EventHandler(this.gridView18_DoubleClick);
            this.gridViewInsuranceCategory.GotFocus += new System.EventHandler(this.gridView18_GotFocus);
            // 
            // colTeamInsuranceCategoryID
            // 
            this.colTeamInsuranceCategoryID.FieldName = "TeamInsuranceCategoryID";
            this.colTeamInsuranceCategoryID.Name = "colTeamInsuranceCategoryID";
            this.colTeamInsuranceCategoryID.OptionsColumn.AllowEdit = false;
            this.colTeamInsuranceCategoryID.OptionsColumn.AllowFocus = false;
            this.colTeamInsuranceCategoryID.OptionsColumn.ReadOnly = true;
            // 
            // colTeamID1
            // 
            this.colTeamID1.FieldName = "TeamID";
            this.colTeamID1.Name = "colTeamID1";
            this.colTeamID1.OptionsColumn.AllowEdit = false;
            this.colTeamID1.OptionsColumn.AllowFocus = false;
            this.colTeamID1.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName3
            // 
            this.colTeamName3.FieldName = "TeamName";
            this.colTeamName3.Name = "colTeamName3";
            this.colTeamName3.OptionsColumn.AllowEdit = false;
            this.colTeamName3.OptionsColumn.AllowFocus = false;
            this.colTeamName3.OptionsColumn.ReadOnly = true;
            this.colTeamName3.Visible = true;
            this.colTeamName3.VisibleIndex = 1;
            // 
            // colInsuranceCategoryID
            // 
            this.colInsuranceCategoryID.FieldName = "InsuranceCategoryID";
            this.colInsuranceCategoryID.Name = "colInsuranceCategoryID";
            this.colInsuranceCategoryID.OptionsColumn.AllowEdit = false;
            this.colInsuranceCategoryID.OptionsColumn.AllowFocus = false;
            this.colInsuranceCategoryID.OptionsColumn.ReadOnly = true;
            // 
            // colCategory
            // 
            this.colCategory.Caption = "Insurance Category";
            this.colCategory.FieldName = "Category";
            this.colCategory.Name = "colCategory";
            this.colCategory.OptionsColumn.AllowEdit = false;
            this.colCategory.OptionsColumn.AllowFocus = false;
            this.colCategory.OptionsColumn.ReadOnly = true;
            this.colCategory.Visible = true;
            this.colCategory.VisibleIndex = 3;
            this.colCategory.Width = 246;
            // 
            // colCategoryStateID
            // 
            this.colCategoryStateID.FieldName = "CategoryStateID";
            this.colCategoryStateID.Name = "colCategoryStateID";
            this.colCategoryStateID.OptionsColumn.AllowEdit = false;
            this.colCategoryStateID.OptionsColumn.AllowFocus = false;
            this.colCategoryStateID.OptionsColumn.ReadOnly = true;
            // 
            // colCategoryState
            // 
            this.colCategoryState.Caption = "Excluded <br> Included";
            this.colCategoryState.FieldName = "CategoryState";
            this.colCategoryState.Name = "colCategoryState";
            this.colCategoryState.OptionsColumn.AllowEdit = false;
            this.colCategoryState.OptionsColumn.AllowFocus = false;
            this.colCategoryState.OptionsColumn.ReadOnly = true;
            this.colCategoryState.Visible = true;
            this.colCategoryState.VisibleIndex = 4;
            // 
            // colActive2
            // 
            this.colActive2.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colActive2.FieldName = "Active";
            this.colActive2.Name = "colActive2";
            this.colActive2.Visible = true;
            this.colActive2.VisibleIndex = 2;
            this.colActive2.Width = 49;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colStartDate2
            // 
            this.colStartDate2.FieldName = "StartDate";
            this.colStartDate2.Name = "colStartDate2";
            this.colStartDate2.OptionsColumn.AllowEdit = false;
            this.colStartDate2.OptionsColumn.AllowFocus = false;
            this.colStartDate2.OptionsColumn.ReadOnly = true;
            this.colStartDate2.Visible = true;
            this.colStartDate2.VisibleIndex = 0;
            // 
            // colEndDate2
            // 
            this.colEndDate2.FieldName = "EndDate";
            this.colEndDate2.Name = "colEndDate2";
            this.colEndDate2.OptionsColumn.AllowEdit = false;
            this.colEndDate2.OptionsColumn.AllowFocus = false;
            this.colEndDate2.OptionsColumn.ReadOnly = true;
            this.colEndDate2.Visible = true;
            this.colEndDate2.VisibleIndex = 1;
            // 
            // repositoryItemMemoExEdit17
            // 
            this.repositoryItemMemoExEdit17.AutoHeight = false;
            this.repositoryItemMemoExEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit17.Name = "repositoryItemMemoExEdit17";
            this.repositoryItemMemoExEdit17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit17.ShowIcon = false;
            // 
            // repositoryItemTextEdit7
            // 
            this.repositoryItemTextEdit7.AutoHeight = false;
            this.repositoryItemTextEdit7.Mask.EditMask = "c";
            this.repositoryItemTextEdit7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit7.Name = "repositoryItemTextEdit7";
            // 
            // repositoryItemTextEdit6
            // 
            this.repositoryItemTextEdit6.AutoHeight = false;
            this.repositoryItemTextEdit6.Mask.EditMask = "g";
            this.repositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit6.Name = "repositoryItemTextEdit6";
            // 
            // repositoryItemHyperLinkEdit9
            // 
            this.repositoryItemHyperLinkEdit9.AutoHeight = false;
            this.repositoryItemHyperLinkEdit9.Name = "repositoryItemHyperLinkEdit9";
            this.repositoryItemHyperLinkEdit9.SingleClick = true;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.splitContainerControl4);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage4.Text = "Allocated Devices";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel1.Controls.Add(this.gridSplitContainer6);
            this.splitContainerControl4.Panel1.ShowCaption = true;
            this.splitContainerControl4.Panel1.Text = "Devices";
            this.splitContainerControl4.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel2.Controls.Add(this.gridControl13);
            this.splitContainerControl4.Panel2.ShowCaption = true;
            this.splitContainerControl4.Panel2.Text = "Team Members Assigned To Device";
            this.splitContainerControl4.Size = new System.Drawing.Size(1265, 260);
            this.splitContainerControl4.SplitterPosition = 491;
            this.splitContainerControl4.TabIndex = 1;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // gridSplitContainer6
            // 
            this.gridSplitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer6.Grid = this.gridControl7;
            this.gridSplitContainer6.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer6.Name = "gridSplitContainer6";
            this.gridSplitContainer6.Panel1.Controls.Add(this.gridControl7);
            this.gridSplitContainer6.Size = new System.Drawing.Size(764, 236);
            this.gridSplitContainer6.TabIndex = 0;
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp04027GCSubContractorAllocatedPDAsBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Open Master Device Manager", "pda_manager")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6});
            this.gridControl7.Size = new System.Drawing.Size(764, 236);
            this.gridControl7.TabIndex = 0;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp04027GCSubContractorAllocatedPDAsBindingSource
            // 
            this.sp04027GCSubContractorAllocatedPDAsBindingSource.DataMember = "sp04027_GC_SubContractor_Allocated_PDAs";
            this.sp04027GCSubContractorAllocatedPDAsBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAllocatedPdaID,
            this.colSubContractorID1,
            this.colSubContractorName1,
            this.colPdaID,
            this.colPdaMake,
            this.colPdaModel,
            this.colPdaSerialNumber,
            this.colPdaFirmwareVersion,
            this.colPdaMobileTelNo,
            this.colPdaDatePurchased,
            this.colSoftwareVersion,
            this.colIMEI,
            this.colDateAllocated,
            this.colRemarks4,
            this.colPDAUserName1,
            this.colPDAPassword1,
            this.colPDALoginToken1,
            this.colPDACode,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID1,
            this.colPDAVersion});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIMEI, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView7.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.DoubleClick += new System.EventHandler(this.gridView7_DoubleClick);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colAllocatedPdaID
            // 
            this.colAllocatedPdaID.Caption = "Allocated Device ID";
            this.colAllocatedPdaID.FieldName = "AllocatedPdaID";
            this.colAllocatedPdaID.Name = "colAllocatedPdaID";
            this.colAllocatedPdaID.OptionsColumn.AllowEdit = false;
            this.colAllocatedPdaID.OptionsColumn.AllowFocus = false;
            this.colAllocatedPdaID.OptionsColumn.ReadOnly = true;
            this.colAllocatedPdaID.Width = 112;
            // 
            // colSubContractorID1
            // 
            this.colSubContractorID1.Caption = "Team ID";
            this.colSubContractorID1.FieldName = "SubContractorID";
            this.colSubContractorID1.Name = "colSubContractorID1";
            this.colSubContractorID1.OptionsColumn.AllowEdit = false;
            this.colSubContractorID1.OptionsColumn.AllowFocus = false;
            this.colSubContractorID1.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName1
            // 
            this.colSubContractorName1.Caption = "Team Name";
            this.colSubContractorName1.FieldName = "SubContractorName";
            this.colSubContractorName1.Name = "colSubContractorName1";
            this.colSubContractorName1.OptionsColumn.AllowEdit = false;
            this.colSubContractorName1.OptionsColumn.AllowFocus = false;
            this.colSubContractorName1.OptionsColumn.ReadOnly = true;
            this.colSubContractorName1.Width = 195;
            // 
            // colPdaID
            // 
            this.colPdaID.Caption = "Device ID";
            this.colPdaID.FieldName = "PdaID";
            this.colPdaID.Name = "colPdaID";
            this.colPdaID.OptionsColumn.AllowEdit = false;
            this.colPdaID.OptionsColumn.AllowFocus = false;
            this.colPdaID.OptionsColumn.ReadOnly = true;
            // 
            // colPdaMake
            // 
            this.colPdaMake.Caption = "Make";
            this.colPdaMake.FieldName = "PdaMake";
            this.colPdaMake.Name = "colPdaMake";
            this.colPdaMake.OptionsColumn.AllowEdit = false;
            this.colPdaMake.OptionsColumn.AllowFocus = false;
            this.colPdaMake.OptionsColumn.ReadOnly = true;
            this.colPdaMake.Visible = true;
            this.colPdaMake.VisibleIndex = 4;
            this.colPdaMake.Width = 93;
            // 
            // colPdaModel
            // 
            this.colPdaModel.Caption = "Model";
            this.colPdaModel.FieldName = "PdaModel";
            this.colPdaModel.Name = "colPdaModel";
            this.colPdaModel.OptionsColumn.AllowEdit = false;
            this.colPdaModel.OptionsColumn.AllowFocus = false;
            this.colPdaModel.OptionsColumn.ReadOnly = true;
            this.colPdaModel.Visible = true;
            this.colPdaModel.VisibleIndex = 5;
            this.colPdaModel.Width = 95;
            // 
            // colPdaSerialNumber
            // 
            this.colPdaSerialNumber.Caption = "Serial No";
            this.colPdaSerialNumber.FieldName = "PdaSerialNumber";
            this.colPdaSerialNumber.Name = "colPdaSerialNumber";
            this.colPdaSerialNumber.OptionsColumn.AllowEdit = false;
            this.colPdaSerialNumber.OptionsColumn.AllowFocus = false;
            this.colPdaSerialNumber.OptionsColumn.ReadOnly = true;
            this.colPdaSerialNumber.Visible = true;
            this.colPdaSerialNumber.VisibleIndex = 6;
            this.colPdaSerialNumber.Width = 111;
            // 
            // colPdaFirmwareVersion
            // 
            this.colPdaFirmwareVersion.Caption = "Firmware";
            this.colPdaFirmwareVersion.FieldName = "PdaFirmwareVersion";
            this.colPdaFirmwareVersion.Name = "colPdaFirmwareVersion";
            this.colPdaFirmwareVersion.OptionsColumn.AllowEdit = false;
            this.colPdaFirmwareVersion.OptionsColumn.AllowFocus = false;
            this.colPdaFirmwareVersion.OptionsColumn.ReadOnly = true;
            this.colPdaFirmwareVersion.Visible = true;
            this.colPdaFirmwareVersion.VisibleIndex = 7;
            this.colPdaFirmwareVersion.Width = 99;
            // 
            // colPdaMobileTelNo
            // 
            this.colPdaMobileTelNo.Caption = "Device Mobile Tel";
            this.colPdaMobileTelNo.FieldName = "PdaMobileTelNo";
            this.colPdaMobileTelNo.Name = "colPdaMobileTelNo";
            this.colPdaMobileTelNo.OptionsColumn.AllowEdit = false;
            this.colPdaMobileTelNo.OptionsColumn.AllowFocus = false;
            this.colPdaMobileTelNo.OptionsColumn.ReadOnly = true;
            this.colPdaMobileTelNo.Visible = true;
            this.colPdaMobileTelNo.VisibleIndex = 2;
            this.colPdaMobileTelNo.Width = 101;
            // 
            // colPdaDatePurchased
            // 
            this.colPdaDatePurchased.Caption = "Purchase Date";
            this.colPdaDatePurchased.FieldName = "PdaDatePurchased";
            this.colPdaDatePurchased.Name = "colPdaDatePurchased";
            this.colPdaDatePurchased.OptionsColumn.AllowEdit = false;
            this.colPdaDatePurchased.OptionsColumn.AllowFocus = false;
            this.colPdaDatePurchased.OptionsColumn.ReadOnly = true;
            this.colPdaDatePurchased.Visible = true;
            this.colPdaDatePurchased.VisibleIndex = 9;
            this.colPdaDatePurchased.Width = 114;
            // 
            // colSoftwareVersion
            // 
            this.colSoftwareVersion.Caption = "Software Version";
            this.colSoftwareVersion.FieldName = "SoftwareVersion";
            this.colSoftwareVersion.Name = "colSoftwareVersion";
            this.colSoftwareVersion.OptionsColumn.AllowEdit = false;
            this.colSoftwareVersion.OptionsColumn.AllowFocus = false;
            this.colSoftwareVersion.OptionsColumn.ReadOnly = true;
            this.colSoftwareVersion.Visible = true;
            this.colSoftwareVersion.VisibleIndex = 8;
            this.colSoftwareVersion.Width = 126;
            // 
            // colIMEI
            // 
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsColumn.AllowFocus = false;
            this.colIMEI.OptionsColumn.ReadOnly = true;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 3;
            this.colIMEI.Width = 98;
            // 
            // colDateAllocated
            // 
            this.colDateAllocated.Caption = "Date Allocated";
            this.colDateAllocated.FieldName = "DateAllocated";
            this.colDateAllocated.Name = "colDateAllocated";
            this.colDateAllocated.OptionsColumn.AllowEdit = false;
            this.colDateAllocated.OptionsColumn.AllowFocus = false;
            this.colDateAllocated.OptionsColumn.ReadOnly = true;
            this.colDateAllocated.Visible = true;
            this.colDateAllocated.VisibleIndex = 0;
            this.colDateAllocated.Width = 91;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colPDAUserName1
            // 
            this.colPDAUserName1.Caption = "Device Username";
            this.colPDAUserName1.FieldName = "PDAUserName";
            this.colPDAUserName1.Name = "colPDAUserName1";
            this.colPDAUserName1.OptionsColumn.AllowEdit = false;
            this.colPDAUserName1.OptionsColumn.AllowFocus = false;
            this.colPDAUserName1.OptionsColumn.ReadOnly = true;
            this.colPDAUserName1.Visible = true;
            this.colPDAUserName1.VisibleIndex = 11;
            this.colPDAUserName1.Width = 92;
            // 
            // colPDAPassword1
            // 
            this.colPDAPassword1.Caption = "Device Password";
            this.colPDAPassword1.FieldName = "PDAPassword";
            this.colPDAPassword1.Name = "colPDAPassword1";
            this.colPDAPassword1.OptionsColumn.AllowEdit = false;
            this.colPDAPassword1.OptionsColumn.AllowFocus = false;
            this.colPDAPassword1.OptionsColumn.ReadOnly = true;
            this.colPDAPassword1.Visible = true;
            this.colPDAPassword1.VisibleIndex = 12;
            this.colPDAPassword1.Width = 90;
            // 
            // colPDALoginToken1
            // 
            this.colPDALoginToken1.Caption = "Device Token";
            this.colPDALoginToken1.FieldName = "PDALoginToken";
            this.colPDALoginToken1.Name = "colPDALoginToken1";
            this.colPDALoginToken1.OptionsColumn.AllowEdit = false;
            this.colPDALoginToken1.OptionsColumn.AllowFocus = false;
            this.colPDALoginToken1.OptionsColumn.ReadOnly = true;
            this.colPDALoginToken1.Visible = true;
            this.colPDALoginToken1.VisibleIndex = 13;
            this.colPDALoginToken1.Width = 231;
            // 
            // colPDACode
            // 
            this.colPDACode.Caption = "Device Code";
            this.colPDACode.FieldName = "PDACode";
            this.colPDACode.Name = "colPDACode";
            this.colPDACode.OptionsColumn.AllowEdit = false;
            this.colPDACode.OptionsColumn.AllowFocus = false;
            this.colPDACode.OptionsColumn.ReadOnly = true;
            this.colPDACode.Visible = true;
            this.colPDACode.VisibleIndex = 1;
            this.colPDACode.Width = 79;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Linked To Person Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Width = 127;
            // 
            // colLinkedToPersonTypeID1
            // 
            this.colLinkedToPersonTypeID1.Caption = "Linked To Person Type ID";
            this.colLinkedToPersonTypeID1.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID1.Name = "colLinkedToPersonTypeID1";
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID1.Width = 141;
            // 
            // colPDAVersion
            // 
            this.colPDAVersion.Caption = "Summer App Version";
            this.colPDAVersion.FieldName = "PDAVersion";
            this.colPDAVersion.Name = "colPDAVersion";
            this.colPDAVersion.OptionsColumn.AllowEdit = false;
            this.colPDAVersion.OptionsColumn.AllowFocus = false;
            this.colPDAVersion.OptionsColumn.ReadOnly = true;
            this.colPDAVersion.Visible = true;
            this.colPDAVersion.VisibleIndex = 14;
            this.colPDAVersion.Width = 117;
            // 
            // gridControl13
            // 
            this.gridControl13.DataSource = this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource;
            this.gridControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl13.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl13.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl13.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl13.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl13_EmbeddedNavigator_ButtonClick);
            this.gridControl13.Location = new System.Drawing.Point(0, 0);
            this.gridControl13.MainView = this.gridView13;
            this.gridControl13.MenuManager = this.barManager1;
            this.gridControl13.Name = "gridControl13";
            this.gridControl13.Size = new System.Drawing.Size(487, 236);
            this.gridControl13.TabIndex = 0;
            this.gridControl13.UseEmbeddedNavigator = true;
            this.gridControl13.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView13});
            // 
            // sp04344GCAllocatedPDALinkedTeamMembersBindingSource
            // 
            this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource.DataMember = "sp04344_GC_Allocated_PDA_Linked_Team_Members";
            this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAllocatedPdaLinkedTeamMemberID,
            this.colAllocatedPDAID1,
            this.colTeamMemberID1,
            this.colPdaID1,
            this.colDateAllocated1,
            this.colPdaMake1,
            this.colPdaModel1,
            this.colPdaSerialNumber1,
            this.colPdaFirmwareVersion1,
            this.colPdaMobileTelNo1,
            this.colPdaDatePurchased1,
            this.colSoftwareVersion1,
            this.colIMEI1,
            this.colPDACode1,
            this.colSubcontractorName5});
            this.gridView13.GridControl = this.gridControl13;
            this.gridView13.GroupCount = 1;
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView13.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView13.OptionsLayout.StoreAppearance = true;
            this.gridView13.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView13.OptionsSelection.MultiSelect = true;
            this.gridView13.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView13.OptionsView.ColumnAutoWidth = false;
            this.gridView13.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            this.gridView13.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPdaSerialNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubcontractorName5, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView13.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView13.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView13.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView13.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView13.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView13.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView13_MouseUp);
            this.gridView13.DoubleClick += new System.EventHandler(this.gridView13_DoubleClick);
            this.gridView13.GotFocus += new System.EventHandler(this.gridView13_GotFocus);
            // 
            // colAllocatedPdaLinkedTeamMemberID
            // 
            this.colAllocatedPdaLinkedTeamMemberID.Caption = "Allocated Device Linked Team Member ID";
            this.colAllocatedPdaLinkedTeamMemberID.FieldName = "AllocatedPdaLinkedTeamMemberID";
            this.colAllocatedPdaLinkedTeamMemberID.Name = "colAllocatedPdaLinkedTeamMemberID";
            this.colAllocatedPdaLinkedTeamMemberID.OptionsColumn.AllowEdit = false;
            this.colAllocatedPdaLinkedTeamMemberID.OptionsColumn.AllowFocus = false;
            this.colAllocatedPdaLinkedTeamMemberID.OptionsColumn.ReadOnly = true;
            this.colAllocatedPdaLinkedTeamMemberID.Width = 205;
            // 
            // colAllocatedPDAID1
            // 
            this.colAllocatedPDAID1.Caption = "Allocated Device ID";
            this.colAllocatedPDAID1.FieldName = "AllocatedPDAID";
            this.colAllocatedPDAID1.Name = "colAllocatedPDAID1";
            this.colAllocatedPDAID1.OptionsColumn.AllowEdit = false;
            this.colAllocatedPDAID1.OptionsColumn.AllowFocus = false;
            this.colAllocatedPDAID1.OptionsColumn.ReadOnly = true;
            this.colAllocatedPDAID1.Width = 102;
            // 
            // colTeamMemberID1
            // 
            this.colTeamMemberID1.Caption = "Team Member ID";
            this.colTeamMemberID1.FieldName = "TeamMemberID";
            this.colTeamMemberID1.Name = "colTeamMemberID1";
            this.colTeamMemberID1.OptionsColumn.AllowEdit = false;
            this.colTeamMemberID1.OptionsColumn.AllowFocus = false;
            this.colTeamMemberID1.OptionsColumn.ReadOnly = true;
            this.colTeamMemberID1.Width = 102;
            // 
            // colPdaID1
            // 
            this.colPdaID1.Caption = "Device ID";
            this.colPdaID1.FieldName = "PdaID";
            this.colPdaID1.Name = "colPdaID1";
            this.colPdaID1.OptionsColumn.AllowEdit = false;
            this.colPdaID1.OptionsColumn.AllowFocus = false;
            this.colPdaID1.OptionsColumn.ReadOnly = true;
            // 
            // colDateAllocated1
            // 
            this.colDateAllocated1.Caption = "Date Allocated";
            this.colDateAllocated1.FieldName = "DateAllocated";
            this.colDateAllocated1.Name = "colDateAllocated1";
            this.colDateAllocated1.OptionsColumn.AllowEdit = false;
            this.colDateAllocated1.OptionsColumn.AllowFocus = false;
            this.colDateAllocated1.OptionsColumn.ReadOnly = true;
            this.colDateAllocated1.Width = 91;
            // 
            // colPdaMake1
            // 
            this.colPdaMake1.Caption = "Make";
            this.colPdaMake1.FieldName = "PdaMake";
            this.colPdaMake1.Name = "colPdaMake1";
            this.colPdaMake1.OptionsColumn.AllowEdit = false;
            this.colPdaMake1.OptionsColumn.AllowFocus = false;
            this.colPdaMake1.OptionsColumn.ReadOnly = true;
            this.colPdaMake1.Visible = true;
            this.colPdaMake1.VisibleIndex = 2;
            // 
            // colPdaModel1
            // 
            this.colPdaModel1.Caption = "Model";
            this.colPdaModel1.FieldName = "PdaModel";
            this.colPdaModel1.Name = "colPdaModel1";
            this.colPdaModel1.OptionsColumn.AllowEdit = false;
            this.colPdaModel1.OptionsColumn.AllowFocus = false;
            this.colPdaModel1.OptionsColumn.ReadOnly = true;
            this.colPdaModel1.Visible = true;
            this.colPdaModel1.VisibleIndex = 3;
            // 
            // colPdaSerialNumber1
            // 
            this.colPdaSerialNumber1.Caption = "Serial Number";
            this.colPdaSerialNumber1.FieldName = "PdaSerialNumber";
            this.colPdaSerialNumber1.Name = "colPdaSerialNumber1";
            this.colPdaSerialNumber1.OptionsColumn.AllowEdit = false;
            this.colPdaSerialNumber1.OptionsColumn.AllowFocus = false;
            this.colPdaSerialNumber1.OptionsColumn.ReadOnly = true;
            this.colPdaSerialNumber1.Visible = true;
            this.colPdaSerialNumber1.VisibleIndex = 4;
            this.colPdaSerialNumber1.Width = 87;
            // 
            // colPdaFirmwareVersion1
            // 
            this.colPdaFirmwareVersion1.Caption = "Firmware Version";
            this.colPdaFirmwareVersion1.FieldName = "PdaFirmwareVersion";
            this.colPdaFirmwareVersion1.Name = "colPdaFirmwareVersion1";
            this.colPdaFirmwareVersion1.OptionsColumn.AllowEdit = false;
            this.colPdaFirmwareVersion1.OptionsColumn.AllowFocus = false;
            this.colPdaFirmwareVersion1.OptionsColumn.ReadOnly = true;
            this.colPdaFirmwareVersion1.Visible = true;
            this.colPdaFirmwareVersion1.VisibleIndex = 6;
            this.colPdaFirmwareVersion1.Width = 103;
            // 
            // colPdaMobileTelNo1
            // 
            this.colPdaMobileTelNo1.Caption = "Mobile Tel No";
            this.colPdaMobileTelNo1.FieldName = "PdaMobileTelNo";
            this.colPdaMobileTelNo1.Name = "colPdaMobileTelNo1";
            this.colPdaMobileTelNo1.OptionsColumn.AllowEdit = false;
            this.colPdaMobileTelNo1.OptionsColumn.AllowFocus = false;
            this.colPdaMobileTelNo1.OptionsColumn.ReadOnly = true;
            this.colPdaMobileTelNo1.Visible = true;
            this.colPdaMobileTelNo1.VisibleIndex = 4;
            this.colPdaMobileTelNo1.Width = 84;
            // 
            // colPdaDatePurchased1
            // 
            this.colPdaDatePurchased1.Caption = "Date Purchased";
            this.colPdaDatePurchased1.FieldName = "PdaDatePurchased";
            this.colPdaDatePurchased1.Name = "colPdaDatePurchased1";
            this.colPdaDatePurchased1.OptionsColumn.AllowEdit = false;
            this.colPdaDatePurchased1.OptionsColumn.AllowFocus = false;
            this.colPdaDatePurchased1.OptionsColumn.ReadOnly = true;
            this.colPdaDatePurchased1.Width = 97;
            // 
            // colSoftwareVersion1
            // 
            this.colSoftwareVersion1.Caption = "Software Version";
            this.colSoftwareVersion1.FieldName = "SoftwareVersion";
            this.colSoftwareVersion1.Name = "colSoftwareVersion1";
            this.colSoftwareVersion1.OptionsColumn.AllowEdit = false;
            this.colSoftwareVersion1.OptionsColumn.AllowFocus = false;
            this.colSoftwareVersion1.OptionsColumn.ReadOnly = true;
            this.colSoftwareVersion1.Width = 103;
            // 
            // colIMEI1
            // 
            this.colIMEI1.Caption = "IMEI";
            this.colIMEI1.FieldName = "IMEI";
            this.colIMEI1.Name = "colIMEI1";
            this.colIMEI1.OptionsColumn.AllowEdit = false;
            this.colIMEI1.OptionsColumn.AllowFocus = false;
            this.colIMEI1.OptionsColumn.ReadOnly = true;
            this.colIMEI1.Visible = true;
            this.colIMEI1.VisibleIndex = 5;
            // 
            // colPDACode1
            // 
            this.colPDACode1.Caption = "Device Code";
            this.colPDACode1.FieldName = "PDACode";
            this.colPDACode1.Name = "colPDACode1";
            this.colPDACode1.OptionsColumn.AllowEdit = false;
            this.colPDACode1.OptionsColumn.AllowFocus = false;
            this.colPDACode1.OptionsColumn.ReadOnly = true;
            this.colPDACode1.Visible = true;
            this.colPDACode1.VisibleIndex = 1;
            this.colPDACode1.Width = 79;
            // 
            // colSubcontractorName5
            // 
            this.colSubcontractorName5.Caption = "Team Member Name";
            this.colSubcontractorName5.FieldName = "SubcontractorName";
            this.colSubcontractorName5.Name = "colSubcontractorName5";
            this.colSubcontractorName5.OptionsColumn.AllowEdit = false;
            this.colSubcontractorName5.OptionsColumn.AllowFocus = false;
            this.colSubcontractorName5.OptionsColumn.ReadOnly = true;
            this.colSubcontractorName5.Visible = true;
            this.colSubcontractorName5.VisibleIndex = 0;
            this.colSubcontractorName5.Width = 132;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.gridSplitContainer7);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage5.Text = "Gritting Info";
            // 
            // gridSplitContainer7
            // 
            this.gridSplitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer7.Grid = this.gridControl8;
            this.gridSplitContainer7.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer7.Name = "gridSplitContainer7";
            this.gridSplitContainer7.Panel1.Controls.Add(this.gridControl8);
            this.gridSplitContainer7.Size = new System.Drawing.Size(1265, 260);
            this.gridSplitContainer7.TabIndex = 0;
            // 
            // gridControl8
            // 
            this.gridControl8.DataSource = this.sp04032GCTeamGrittingInfoBindingSource;
            this.gridControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit")});
            this.gridControl8.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl8_EmbeddedNavigator_ButtonClick);
            this.gridControl8.Location = new System.Drawing.Point(0, 0);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit9,
            this.repositoryItemMemoExEdit7,
            this.repositoryItemHyperLinkEdit5,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit25kgBags});
            this.gridControl8.Size = new System.Drawing.Size(1265, 260);
            this.gridControl8.TabIndex = 0;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp04032GCTeamGrittingInfoBindingSource
            // 
            this.sp04032GCTeamGrittingInfoBindingSource.DataMember = "sp04032_GC_Team_Gritting_Info";
            this.sp04032GCTeamGrittingInfoBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSubContractorGritInformationID,
            this.colSubContractorID2,
            this.colGrittingActive,
            this.colHoldsStock,
            this.colCurrentGritLevel,
            this.colWarningGritLevel,
            this.colUrgentGritLevel,
            this.colStockSuppliedByDepotID,
            this.colSellBillingInvoice,
            this.colSelfBillingFrequency,
            this.colSelfBillingFrequencyDescriptorID,
            this.colIsDirectLabour,
            this.colRemarks5,
            this.colSubContractorName2,
            this.colGritDepotName,
            this.colGritDepotAddressLine1,
            this.colGritDepotTelephone,
            this.colGritDepotMobile,
            this.colGritDepotFax,
            this.colGritDepotText,
            this.colGritDepotEmail,
            this.colGritDepotWebSite,
            this.colLatitude1,
            this.colLongitude1,
            this.colGritDepotCurrentGrittingLevel,
            this.colGritDepotWarningGritLevel,
            this.colGritDepotUrgentGritLevel,
            this.colSelfBillingFrequencyDescription,
            this.colHourlyProactiveRate,
            this.colHourlyReactiveRate,
            this.colGritJobTransferMethod,
            this.colGritMobileTelephoneNumber,
            this.colStartingGritAmount,
            this.colTransferredInGritAmount,
            this.colTransferredOutGritAmount,
            this.colOrderedInGritAmount,
            this.colCalloutUsedGritAmount,
            this.colMissingJobSheetDeliveryMethodID,
            this.colMissingJobSheetDeliveryMethod,
            this.colAutoAllocateGritJobs});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView8_CustomDrawCell);
            this.gridView8.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView8_CustomRowCellEdit);
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView8_ShowingEditor);
            this.gridView8.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView8.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.DoubleClick += new System.EventHandler(this.gridView8_DoubleClick);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colSubContractorGritInformationID
            // 
            this.colSubContractorGritInformationID.Caption = "Gritting Info ID";
            this.colSubContractorGritInformationID.FieldName = "SubContractorGritInformationID";
            this.colSubContractorGritInformationID.Name = "colSubContractorGritInformationID";
            this.colSubContractorGritInformationID.OptionsColumn.AllowEdit = false;
            this.colSubContractorGritInformationID.OptionsColumn.AllowFocus = false;
            this.colSubContractorGritInformationID.OptionsColumn.ReadOnly = true;
            this.colSubContractorGritInformationID.Width = 93;
            // 
            // colSubContractorID2
            // 
            this.colSubContractorID2.Caption = "Team ID";
            this.colSubContractorID2.FieldName = "SubContractorID";
            this.colSubContractorID2.Name = "colSubContractorID2";
            this.colSubContractorID2.OptionsColumn.AllowEdit = false;
            this.colSubContractorID2.OptionsColumn.AllowFocus = false;
            this.colSubContractorID2.OptionsColumn.ReadOnly = true;
            // 
            // colGrittingActive
            // 
            this.colGrittingActive.Caption = "Gritting Active";
            this.colGrittingActive.ColumnEdit = this.repositoryItemCheckEdit9;
            this.colGrittingActive.FieldName = "GrittingActive";
            this.colGrittingActive.Name = "colGrittingActive";
            this.colGrittingActive.OptionsColumn.AllowEdit = false;
            this.colGrittingActive.OptionsColumn.AllowFocus = false;
            this.colGrittingActive.OptionsColumn.ReadOnly = true;
            this.colGrittingActive.Visible = true;
            this.colGrittingActive.VisibleIndex = 1;
            this.colGrittingActive.Width = 89;
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Caption = "Check";
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            this.repositoryItemCheckEdit9.ValueChecked = 1;
            this.repositoryItemCheckEdit9.ValueUnchecked = 0;
            // 
            // colHoldsStock
            // 
            this.colHoldsStock.Caption = "Holds Stock";
            this.colHoldsStock.ColumnEdit = this.repositoryItemCheckEdit9;
            this.colHoldsStock.FieldName = "HoldsStock";
            this.colHoldsStock.Name = "colHoldsStock";
            this.colHoldsStock.OptionsColumn.AllowEdit = false;
            this.colHoldsStock.OptionsColumn.AllowFocus = false;
            this.colHoldsStock.OptionsColumn.ReadOnly = true;
            this.colHoldsStock.Visible = true;
            this.colHoldsStock.VisibleIndex = 5;
            this.colHoldsStock.Width = 76;
            // 
            // colCurrentGritLevel
            // 
            this.colCurrentGritLevel.Caption = "Current Grit Level";
            this.colCurrentGritLevel.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colCurrentGritLevel.FieldName = "CurrentGritLevel";
            this.colCurrentGritLevel.Name = "colCurrentGritLevel";
            this.colCurrentGritLevel.OptionsColumn.AllowEdit = false;
            this.colCurrentGritLevel.OptionsColumn.AllowFocus = false;
            this.colCurrentGritLevel.OptionsColumn.ReadOnly = true;
            this.colCurrentGritLevel.Visible = true;
            this.colCurrentGritLevel.VisibleIndex = 6;
            this.colCurrentGritLevel.Width = 106;
            // 
            // repositoryItemTextEdit25kgBags
            // 
            this.repositoryItemTextEdit25kgBags.AutoHeight = false;
            this.repositoryItemTextEdit25kgBags.Mask.EditMask = "######0.00 25 Kg Bags";
            this.repositoryItemTextEdit25kgBags.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit25kgBags.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit25kgBags.Name = "repositoryItemTextEdit25kgBags";
            // 
            // colWarningGritLevel
            // 
            this.colWarningGritLevel.Caption = "Warning Grit Level";
            this.colWarningGritLevel.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colWarningGritLevel.FieldName = "WarningGritLevel";
            this.colWarningGritLevel.Name = "colWarningGritLevel";
            this.colWarningGritLevel.OptionsColumn.AllowEdit = false;
            this.colWarningGritLevel.OptionsColumn.AllowFocus = false;
            this.colWarningGritLevel.OptionsColumn.ReadOnly = true;
            this.colWarningGritLevel.Visible = true;
            this.colWarningGritLevel.VisibleIndex = 7;
            this.colWarningGritLevel.Width = 109;
            // 
            // colUrgentGritLevel
            // 
            this.colUrgentGritLevel.Caption = "Urgent Grit Level";
            this.colUrgentGritLevel.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colUrgentGritLevel.FieldName = "UrgentGritLevel";
            this.colUrgentGritLevel.Name = "colUrgentGritLevel";
            this.colUrgentGritLevel.OptionsColumn.AllowEdit = false;
            this.colUrgentGritLevel.OptionsColumn.AllowFocus = false;
            this.colUrgentGritLevel.OptionsColumn.ReadOnly = true;
            this.colUrgentGritLevel.Visible = true;
            this.colUrgentGritLevel.VisibleIndex = 8;
            this.colUrgentGritLevel.Width = 102;
            // 
            // colStockSuppliedByDepotID
            // 
            this.colStockSuppliedByDepotID.Caption = "Suppling Depot ID";
            this.colStockSuppliedByDepotID.FieldName = "StockSuppliedByDepotID";
            this.colStockSuppliedByDepotID.Name = "colStockSuppliedByDepotID";
            this.colStockSuppliedByDepotID.OptionsColumn.AllowEdit = false;
            this.colStockSuppliedByDepotID.OptionsColumn.AllowFocus = false;
            this.colStockSuppliedByDepotID.OptionsColumn.ReadOnly = true;
            this.colStockSuppliedByDepotID.Width = 107;
            // 
            // colSellBillingInvoice
            // 
            this.colSellBillingInvoice.Caption = "Self Billing Invoice";
            this.colSellBillingInvoice.ColumnEdit = this.repositoryItemCheckEdit9;
            this.colSellBillingInvoice.FieldName = "SellBillingInvoice";
            this.colSellBillingInvoice.Name = "colSellBillingInvoice";
            this.colSellBillingInvoice.OptionsColumn.AllowEdit = false;
            this.colSellBillingInvoice.OptionsColumn.AllowFocus = false;
            this.colSellBillingInvoice.OptionsColumn.ReadOnly = true;
            this.colSellBillingInvoice.Visible = true;
            this.colSellBillingInvoice.VisibleIndex = 14;
            this.colSellBillingInvoice.Width = 106;
            // 
            // colSelfBillingFrequency
            // 
            this.colSelfBillingFrequency.Caption = "Self Billing Frequency";
            this.colSelfBillingFrequency.FieldName = "SelfBillingFrequency";
            this.colSelfBillingFrequency.Name = "colSelfBillingFrequency";
            this.colSelfBillingFrequency.OptionsColumn.AllowEdit = false;
            this.colSelfBillingFrequency.OptionsColumn.AllowFocus = false;
            this.colSelfBillingFrequency.OptionsColumn.ReadOnly = true;
            this.colSelfBillingFrequency.Visible = true;
            this.colSelfBillingFrequency.VisibleIndex = 15;
            this.colSelfBillingFrequency.Width = 122;
            // 
            // colSelfBillingFrequencyDescriptorID
            // 
            this.colSelfBillingFrequencyDescriptorID.Caption = "Billing Frequency Descriptor ID";
            this.colSelfBillingFrequencyDescriptorID.FieldName = "SelfBillingFrequencyDescriptorID";
            this.colSelfBillingFrequencyDescriptorID.Name = "colSelfBillingFrequencyDescriptorID";
            this.colSelfBillingFrequencyDescriptorID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingFrequencyDescriptorID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingFrequencyDescriptorID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingFrequencyDescriptorID.Width = 164;
            // 
            // colIsDirectLabour
            // 
            this.colIsDirectLabour.Caption = "Is Direct Labour";
            this.colIsDirectLabour.ColumnEdit = this.repositoryItemCheckEdit9;
            this.colIsDirectLabour.FieldName = "IsDirectLabour";
            this.colIsDirectLabour.Name = "colIsDirectLabour";
            this.colIsDirectLabour.OptionsColumn.AllowEdit = false;
            this.colIsDirectLabour.OptionsColumn.AllowFocus = false;
            this.colIsDirectLabour.OptionsColumn.ReadOnly = true;
            this.colIsDirectLabour.Visible = true;
            this.colIsDirectLabour.VisibleIndex = 2;
            this.colIsDirectLabour.Width = 97;
            // 
            // colRemarks5
            // 
            this.colRemarks5.Caption = "Remarks";
            this.colRemarks5.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colRemarks5.FieldName = "Remarks";
            this.colRemarks5.Name = "colRemarks5";
            this.colRemarks5.OptionsColumn.ReadOnly = true;
            this.colRemarks5.Visible = true;
            this.colRemarks5.VisibleIndex = 19;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // colSubContractorName2
            // 
            this.colSubContractorName2.Caption = "Team Name";
            this.colSubContractorName2.FieldName = "SubContractorName";
            this.colSubContractorName2.Name = "colSubContractorName2";
            this.colSubContractorName2.OptionsColumn.AllowEdit = false;
            this.colSubContractorName2.OptionsColumn.AllowFocus = false;
            this.colSubContractorName2.OptionsColumn.ReadOnly = true;
            this.colSubContractorName2.Visible = true;
            this.colSubContractorName2.VisibleIndex = 0;
            this.colSubContractorName2.Width = 168;
            // 
            // colGritDepotName
            // 
            this.colGritDepotName.Caption = "Supplying Depot Name";
            this.colGritDepotName.FieldName = "GritDepotName";
            this.colGritDepotName.Name = "colGritDepotName";
            this.colGritDepotName.OptionsColumn.AllowEdit = false;
            this.colGritDepotName.OptionsColumn.AllowFocus = false;
            this.colGritDepotName.OptionsColumn.ReadOnly = true;
            this.colGritDepotName.Visible = true;
            this.colGritDepotName.VisibleIndex = 22;
            this.colGritDepotName.Width = 129;
            // 
            // colGritDepotAddressLine1
            // 
            this.colGritDepotAddressLine1.Caption = "Depot Address";
            this.colGritDepotAddressLine1.FieldName = "GritDepotAddressLine1";
            this.colGritDepotAddressLine1.Name = "colGritDepotAddressLine1";
            this.colGritDepotAddressLine1.OptionsColumn.AllowEdit = false;
            this.colGritDepotAddressLine1.OptionsColumn.AllowFocus = false;
            this.colGritDepotAddressLine1.OptionsColumn.ReadOnly = true;
            this.colGritDepotAddressLine1.Visible = true;
            this.colGritDepotAddressLine1.VisibleIndex = 23;
            this.colGritDepotAddressLine1.Width = 92;
            // 
            // colGritDepotTelephone
            // 
            this.colGritDepotTelephone.Caption = "Depot Telephone";
            this.colGritDepotTelephone.FieldName = "GritDepotTelephone";
            this.colGritDepotTelephone.Name = "colGritDepotTelephone";
            this.colGritDepotTelephone.OptionsColumn.AllowEdit = false;
            this.colGritDepotTelephone.OptionsColumn.AllowFocus = false;
            this.colGritDepotTelephone.OptionsColumn.ReadOnly = true;
            this.colGritDepotTelephone.Visible = true;
            this.colGritDepotTelephone.VisibleIndex = 24;
            this.colGritDepotTelephone.Width = 103;
            // 
            // colGritDepotMobile
            // 
            this.colGritDepotMobile.Caption = "Depot Mobile";
            this.colGritDepotMobile.FieldName = "GritDepotMobile";
            this.colGritDepotMobile.Name = "colGritDepotMobile";
            this.colGritDepotMobile.OptionsColumn.AllowEdit = false;
            this.colGritDepotMobile.OptionsColumn.AllowFocus = false;
            this.colGritDepotMobile.OptionsColumn.ReadOnly = true;
            this.colGritDepotMobile.Visible = true;
            this.colGritDepotMobile.VisibleIndex = 25;
            this.colGritDepotMobile.Width = 83;
            // 
            // colGritDepotFax
            // 
            this.colGritDepotFax.Caption = "Depot Fax";
            this.colGritDepotFax.FieldName = "GritDepotFax";
            this.colGritDepotFax.Name = "colGritDepotFax";
            this.colGritDepotFax.OptionsColumn.AllowEdit = false;
            this.colGritDepotFax.OptionsColumn.AllowFocus = false;
            this.colGritDepotFax.OptionsColumn.ReadOnly = true;
            this.colGritDepotFax.Visible = true;
            this.colGritDepotFax.VisibleIndex = 27;
            // 
            // colGritDepotText
            // 
            this.colGritDepotText.Caption = "Depot Text";
            this.colGritDepotText.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colGritDepotText.FieldName = "GritDepotText";
            this.colGritDepotText.Name = "colGritDepotText";
            this.colGritDepotText.OptionsColumn.AllowEdit = false;
            this.colGritDepotText.OptionsColumn.AllowFocus = false;
            this.colGritDepotText.OptionsColumn.ReadOnly = true;
            this.colGritDepotText.Visible = true;
            this.colGritDepotText.VisibleIndex = 28;
            // 
            // colGritDepotEmail
            // 
            this.colGritDepotEmail.Caption = "Depot Email";
            this.colGritDepotEmail.ColumnEdit = this.repositoryItemHyperLinkEdit5;
            this.colGritDepotEmail.FieldName = "GritDepotEmail";
            this.colGritDepotEmail.Name = "colGritDepotEmail";
            this.colGritDepotEmail.OptionsColumn.ReadOnly = true;
            this.colGritDepotEmail.Visible = true;
            this.colGritDepotEmail.VisibleIndex = 26;
            this.colGritDepotEmail.Width = 77;
            // 
            // repositoryItemHyperLinkEdit5
            // 
            this.repositoryItemHyperLinkEdit5.AutoHeight = false;
            this.repositoryItemHyperLinkEdit5.Name = "repositoryItemHyperLinkEdit5";
            this.repositoryItemHyperLinkEdit5.SingleClick = true;
            this.repositoryItemHyperLinkEdit5.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit5_OpenLink);
            // 
            // colGritDepotWebSite
            // 
            this.colGritDepotWebSite.Caption = "Depot Website";
            this.colGritDepotWebSite.ColumnEdit = this.repositoryItemHyperLinkEdit5;
            this.colGritDepotWebSite.FieldName = "GritDepotWebSite";
            this.colGritDepotWebSite.Name = "colGritDepotWebSite";
            this.colGritDepotWebSite.OptionsColumn.ReadOnly = true;
            this.colGritDepotWebSite.Visible = true;
            this.colGritDepotWebSite.VisibleIndex = 29;
            this.colGritDepotWebSite.Width = 92;
            // 
            // colLatitude1
            // 
            this.colLatitude1.Caption = "Depot Latitude";
            this.colLatitude1.FieldName = "Latitude";
            this.colLatitude1.Name = "colLatitude1";
            this.colLatitude1.OptionsColumn.AllowEdit = false;
            this.colLatitude1.OptionsColumn.AllowFocus = false;
            this.colLatitude1.OptionsColumn.ReadOnly = true;
            this.colLatitude1.Width = 92;
            // 
            // colLongitude1
            // 
            this.colLongitude1.Caption = "Depot Longitude";
            this.colLongitude1.FieldName = "Longitude";
            this.colLongitude1.Name = "colLongitude1";
            this.colLongitude1.OptionsColumn.AllowEdit = false;
            this.colLongitude1.OptionsColumn.AllowFocus = false;
            this.colLongitude1.OptionsColumn.ReadOnly = true;
            this.colLongitude1.Width = 100;
            // 
            // colGritDepotCurrentGrittingLevel
            // 
            this.colGritDepotCurrentGrittingLevel.Caption = "Depot Current Grit Level";
            this.colGritDepotCurrentGrittingLevel.FieldName = "GritDepotCurrentGrittingLevel";
            this.colGritDepotCurrentGrittingLevel.Name = "colGritDepotCurrentGrittingLevel";
            this.colGritDepotCurrentGrittingLevel.OptionsColumn.AllowEdit = false;
            this.colGritDepotCurrentGrittingLevel.OptionsColumn.AllowFocus = false;
            this.colGritDepotCurrentGrittingLevel.OptionsColumn.ReadOnly = true;
            this.colGritDepotCurrentGrittingLevel.Visible = true;
            this.colGritDepotCurrentGrittingLevel.VisibleIndex = 30;
            this.colGritDepotCurrentGrittingLevel.Width = 138;
            // 
            // colGritDepotWarningGritLevel
            // 
            this.colGritDepotWarningGritLevel.Caption = "Depot Warning Grit Level";
            this.colGritDepotWarningGritLevel.FieldName = "GritDepotWarningGritLevel";
            this.colGritDepotWarningGritLevel.Name = "colGritDepotWarningGritLevel";
            this.colGritDepotWarningGritLevel.OptionsColumn.AllowEdit = false;
            this.colGritDepotWarningGritLevel.OptionsColumn.AllowFocus = false;
            this.colGritDepotWarningGritLevel.OptionsColumn.ReadOnly = true;
            this.colGritDepotWarningGritLevel.Visible = true;
            this.colGritDepotWarningGritLevel.VisibleIndex = 31;
            this.colGritDepotWarningGritLevel.Width = 141;
            // 
            // colGritDepotUrgentGritLevel
            // 
            this.colGritDepotUrgentGritLevel.Caption = "Depot Urgent Grit Level";
            this.colGritDepotUrgentGritLevel.FieldName = "GritDepotUrgentGritLevel";
            this.colGritDepotUrgentGritLevel.Name = "colGritDepotUrgentGritLevel";
            this.colGritDepotUrgentGritLevel.OptionsColumn.AllowEdit = false;
            this.colGritDepotUrgentGritLevel.OptionsColumn.AllowFocus = false;
            this.colGritDepotUrgentGritLevel.OptionsColumn.ReadOnly = true;
            this.colGritDepotUrgentGritLevel.Visible = true;
            this.colGritDepotUrgentGritLevel.VisibleIndex = 32;
            this.colGritDepotUrgentGritLevel.Width = 134;
            // 
            // colSelfBillingFrequencyDescription
            // 
            this.colSelfBillingFrequencyDescription.Caption = "Billing Frequency Descriptor";
            this.colSelfBillingFrequencyDescription.FieldName = "SelfBillingFrequencyDescription";
            this.colSelfBillingFrequencyDescription.Name = "colSelfBillingFrequencyDescription";
            this.colSelfBillingFrequencyDescription.OptionsColumn.AllowEdit = false;
            this.colSelfBillingFrequencyDescription.OptionsColumn.AllowFocus = false;
            this.colSelfBillingFrequencyDescription.OptionsColumn.ReadOnly = true;
            this.colSelfBillingFrequencyDescription.Visible = true;
            this.colSelfBillingFrequencyDescription.VisibleIndex = 16;
            this.colSelfBillingFrequencyDescription.Width = 154;
            // 
            // colHourlyProactiveRate
            // 
            this.colHourlyProactiveRate.Caption = "Hourly Proactive Rate";
            this.colHourlyProactiveRate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colHourlyProactiveRate.FieldName = "HourlyProactiveRate";
            this.colHourlyProactiveRate.Name = "colHourlyProactiveRate";
            this.colHourlyProactiveRate.OptionsColumn.AllowEdit = false;
            this.colHourlyProactiveRate.OptionsColumn.AllowFocus = false;
            this.colHourlyProactiveRate.OptionsColumn.ReadOnly = true;
            this.colHourlyProactiveRate.Visible = true;
            this.colHourlyProactiveRate.VisibleIndex = 3;
            this.colHourlyProactiveRate.Width = 126;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colHourlyReactiveRate
            // 
            this.colHourlyReactiveRate.Caption = "Hourly Reactive Rate";
            this.colHourlyReactiveRate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colHourlyReactiveRate.FieldName = "HourlyReactiveRate";
            this.colHourlyReactiveRate.Name = "colHourlyReactiveRate";
            this.colHourlyReactiveRate.OptionsColumn.AllowEdit = false;
            this.colHourlyReactiveRate.OptionsColumn.AllowFocus = false;
            this.colHourlyReactiveRate.OptionsColumn.ReadOnly = true;
            this.colHourlyReactiveRate.Visible = true;
            this.colHourlyReactiveRate.VisibleIndex = 4;
            this.colHourlyReactiveRate.Width = 136;
            // 
            // colGritJobTransferMethod
            // 
            this.colGritJobTransferMethod.Caption = "Device Used";
            this.colGritJobTransferMethod.ColumnEdit = this.repositoryItemCheckEdit9;
            this.colGritJobTransferMethod.FieldName = "GritJobTransferMethod";
            this.colGritJobTransferMethod.Name = "colGritJobTransferMethod";
            this.colGritJobTransferMethod.OptionsColumn.AllowEdit = false;
            this.colGritJobTransferMethod.OptionsColumn.AllowFocus = false;
            this.colGritJobTransferMethod.OptionsColumn.ReadOnly = true;
            this.colGritJobTransferMethod.Visible = true;
            this.colGritJobTransferMethod.VisibleIndex = 17;
            this.colGritJobTransferMethod.Width = 70;
            // 
            // colGritMobileTelephoneNumber
            // 
            this.colGritMobileTelephoneNumber.Caption = "Gritting Mobile Tel";
            this.colGritMobileTelephoneNumber.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colGritMobileTelephoneNumber.FieldName = "GritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.Name = "colGritMobileTelephoneNumber";
            this.colGritMobileTelephoneNumber.OptionsColumn.ReadOnly = true;
            this.colGritMobileTelephoneNumber.Visible = true;
            this.colGritMobileTelephoneNumber.VisibleIndex = 20;
            this.colGritMobileTelephoneNumber.Width = 106;
            // 
            // colStartingGritAmount
            // 
            this.colStartingGritAmount.Caption = "Start Grit Amount";
            this.colStartingGritAmount.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colStartingGritAmount.FieldName = "StartingGritAmount";
            this.colStartingGritAmount.Name = "colStartingGritAmount";
            this.colStartingGritAmount.OptionsColumn.AllowEdit = false;
            this.colStartingGritAmount.OptionsColumn.AllowFocus = false;
            this.colStartingGritAmount.OptionsColumn.ReadOnly = true;
            this.colStartingGritAmount.Visible = true;
            this.colStartingGritAmount.VisibleIndex = 9;
            this.colStartingGritAmount.Width = 105;
            // 
            // colTransferredInGritAmount
            // 
            this.colTransferredInGritAmount.Caption = "Transferred In Grit";
            this.colTransferredInGritAmount.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colTransferredInGritAmount.FieldName = "TransferredInGritAmount";
            this.colTransferredInGritAmount.Name = "colTransferredInGritAmount";
            this.colTransferredInGritAmount.OptionsColumn.AllowEdit = false;
            this.colTransferredInGritAmount.OptionsColumn.AllowFocus = false;
            this.colTransferredInGritAmount.OptionsColumn.ReadOnly = true;
            this.colTransferredInGritAmount.Visible = true;
            this.colTransferredInGritAmount.VisibleIndex = 10;
            this.colTransferredInGritAmount.Width = 111;
            // 
            // colTransferredOutGritAmount
            // 
            this.colTransferredOutGritAmount.Caption = "Transferred Out Grit";
            this.colTransferredOutGritAmount.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colTransferredOutGritAmount.FieldName = "TransferredOutGritAmount";
            this.colTransferredOutGritAmount.Name = "colTransferredOutGritAmount";
            this.colTransferredOutGritAmount.OptionsColumn.AllowEdit = false;
            this.colTransferredOutGritAmount.OptionsColumn.AllowFocus = false;
            this.colTransferredOutGritAmount.OptionsColumn.ReadOnly = true;
            this.colTransferredOutGritAmount.Visible = true;
            this.colTransferredOutGritAmount.VisibleIndex = 12;
            this.colTransferredOutGritAmount.Width = 119;
            // 
            // colOrderedInGritAmount
            // 
            this.colOrderedInGritAmount.Caption = "Ordered In Grit";
            this.colOrderedInGritAmount.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colOrderedInGritAmount.FieldName = "OrderedInGritAmount";
            this.colOrderedInGritAmount.Name = "colOrderedInGritAmount";
            this.colOrderedInGritAmount.OptionsColumn.AllowEdit = false;
            this.colOrderedInGritAmount.OptionsColumn.AllowFocus = false;
            this.colOrderedInGritAmount.OptionsColumn.ReadOnly = true;
            this.colOrderedInGritAmount.Visible = true;
            this.colOrderedInGritAmount.VisibleIndex = 11;
            this.colOrderedInGritAmount.Width = 94;
            // 
            // colCalloutUsedGritAmount
            // 
            this.colCalloutUsedGritAmount.Caption = "Grit Used on Callouts";
            this.colCalloutUsedGritAmount.ColumnEdit = this.repositoryItemTextEdit25kgBags;
            this.colCalloutUsedGritAmount.FieldName = "CalloutUsedGritAmount";
            this.colCalloutUsedGritAmount.Name = "colCalloutUsedGritAmount";
            this.colCalloutUsedGritAmount.OptionsColumn.AllowEdit = false;
            this.colCalloutUsedGritAmount.OptionsColumn.AllowFocus = false;
            this.colCalloutUsedGritAmount.OptionsColumn.ReadOnly = true;
            this.colCalloutUsedGritAmount.Visible = true;
            this.colCalloutUsedGritAmount.VisibleIndex = 13;
            this.colCalloutUsedGritAmount.Width = 121;
            // 
            // colMissingJobSheetDeliveryMethodID
            // 
            this.colMissingJobSheetDeliveryMethodID.Caption = "Missing Job Sheet Delivery Method ID";
            this.colMissingJobSheetDeliveryMethodID.FieldName = "MissingJobSheetDeliveryMethodID";
            this.colMissingJobSheetDeliveryMethodID.Name = "colMissingJobSheetDeliveryMethodID";
            this.colMissingJobSheetDeliveryMethodID.OptionsColumn.AllowEdit = false;
            this.colMissingJobSheetDeliveryMethodID.OptionsColumn.AllowFocus = false;
            this.colMissingJobSheetDeliveryMethodID.OptionsColumn.ReadOnly = true;
            this.colMissingJobSheetDeliveryMethodID.Width = 201;
            // 
            // colMissingJobSheetDeliveryMethod
            // 
            this.colMissingJobSheetDeliveryMethod.Caption = "Missing Job Sheet Delivery Method";
            this.colMissingJobSheetDeliveryMethod.FieldName = "MissingJobSheetDeliveryMethod";
            this.colMissingJobSheetDeliveryMethod.Name = "colMissingJobSheetDeliveryMethod";
            this.colMissingJobSheetDeliveryMethod.OptionsColumn.AllowEdit = false;
            this.colMissingJobSheetDeliveryMethod.OptionsColumn.AllowFocus = false;
            this.colMissingJobSheetDeliveryMethod.OptionsColumn.ReadOnly = true;
            this.colMissingJobSheetDeliveryMethod.Visible = true;
            this.colMissingJobSheetDeliveryMethod.VisibleIndex = 21;
            this.colMissingJobSheetDeliveryMethod.Width = 187;
            // 
            // colAutoAllocateGritJobs
            // 
            this.colAutoAllocateGritJobs.Caption = "Auto Allocate Grit Jobs";
            this.colAutoAllocateGritJobs.ColumnEdit = this.repositoryItemCheckEdit9;
            this.colAutoAllocateGritJobs.FieldName = "AutoAllocateGritJobs";
            this.colAutoAllocateGritJobs.Name = "colAutoAllocateGritJobs";
            this.colAutoAllocateGritJobs.OptionsColumn.AllowEdit = false;
            this.colAutoAllocateGritJobs.OptionsColumn.AllowFocus = false;
            this.colAutoAllocateGritJobs.OptionsColumn.ReadOnly = true;
            this.colAutoAllocateGritJobs.Visible = true;
            this.colAutoAllocateGritJobs.VisibleIndex = 18;
            this.colAutoAllocateGritJobs.Width = 130;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.splitContainerControl3);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage6.Text = "WM Self-Billing Invoices";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.layoutControl3);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.gridSplitContainer8);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1265, 260);
            this.splitContainerControl3.SplitterPosition = 31;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.btnReloadInvoices);
            this.layoutControl3.Controls.Add(this.dateEdit2);
            this.layoutControl3.Controls.Add(this.dateEdit1);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1447, 124, 250, 350);
            this.layoutControl3.Root = this.layoutControlGroup4;
            this.layoutControl3.Size = new System.Drawing.Size(1265, 31);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // btnReloadInvoices
            // 
            this.btnReloadInvoices.ImageOptions.ImageIndex = 6;
            this.btnReloadInvoices.ImageOptions.ImageList = this.imageCollection1;
            this.btnReloadInvoices.Location = new System.Drawing.Point(401, 4);
            this.btnReloadInvoices.Name = "btnReloadInvoices";
            this.btnReloadInvoices.Size = new System.Drawing.Size(78, 22);
            this.btnReloadInvoices.StyleController = this.layoutControl3;
            this.btnReloadInvoices.TabIndex = 13;
            this.btnReloadInvoices.Text = "Refresh";
            this.btnReloadInvoices.Click += new System.EventHandler(this.btnReloadInvoices_Click);
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = null;
            this.dateEdit2.Location = new System.Drawing.Point(259, 4);
            this.dateEdit2.MenuManager = this.barManager1;
            this.dateEdit2.Name = "dateEdit2";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem4.Text = "Clear Date - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all in" +
    "voices will be loaded.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, superToolTip4, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit2.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dateEdit2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit2.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEdit2.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEdit2.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEdit2.Properties.NullValuePrompt = "No Date";
            this.dateEdit2.Size = new System.Drawing.Size(138, 20);
            this.dateEdit2.StyleController = this.layoutControl3;
            this.dateEdit2.TabIndex = 12;
            this.dateEdit2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEdit2_ButtonClick);
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(61, 4);
            this.dateEdit1.MenuManager = this.barManager1;
            this.dateEdit1.Name = "dateEdit1";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Clear Date - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all in" +
    "voices will be loaded.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.dateEdit1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit1.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEdit1.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEdit1.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEdit1.Properties.NullValuePrompt = "No Date";
            this.dateEdit1.Size = new System.Drawing.Size(137, 20);
            this.dateEdit1.StyleController = this.layoutControl3;
            this.dateEdit1.TabIndex = 11;
            this.dateEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEdit1_ButtonClick);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Root";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem1,
            this.layoutControlItem10});
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup4.Size = new System.Drawing.Size(1265, 31);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.dateEdit1;
            this.layoutControlItem8.CustomizationFormText = "From Date:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(198, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(198, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(198, 27);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "From Date:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.dateEdit2;
            this.layoutControlItem9.CustomizationFormText = "To Date:";
            this.layoutControlItem9.Location = new System.Drawing.Point(198, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(199, 27);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "To Date:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(54, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(479, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(782, 27);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnReloadInvoices;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(397, 0);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(82, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(82, 27);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // gridSplitContainer8
            // 
            this.gridSplitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer8.Grid = this.gridControl10;
            this.gridSplitContainer8.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer8.Name = "gridSplitContainer8";
            this.gridSplitContainer8.Panel1.Controls.Add(this.gridControl10);
            this.gridSplitContainer8.Size = new System.Drawing.Size(1265, 223);
            this.gridSplitContainer8.TabIndex = 0;
            // 
            // gridControl10
            // 
            this.gridControl10.DataSource = this.sp04131GCTeamSelfBillingInvoicesBindingSource;
            this.gridControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl10.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl10.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl10_EmbeddedNavigator_ButtonClick);
            this.gridControl10.Location = new System.Drawing.Point(0, 0);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit6});
            this.gridControl10.Size = new System.Drawing.Size(1265, 223);
            this.gridControl10.TabIndex = 0;
            this.gridControl10.UseEmbeddedNavigator = true;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp04131GCTeamSelfBillingInvoicesBindingSource
            // 
            this.sp04131GCTeamSelfBillingInvoicesBindingSource.DataMember = "sp04131_GC_Team_Self_Billing_Invoices";
            this.sp04131GCTeamSelfBillingInvoicesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGrittingInvoiceID,
            this.gridColumn4,
            this.colInvoiceDate,
            this.colLinkedFileName,
            this.colTeamName,
            this.colLinkedCalloutCount});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.GroupCount = 1;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.MultiSelect = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView10.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView10_CustomRowCellEdit);
            this.gridView10.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView10.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView10.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView10.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView10_ShowingEditor);
            this.gridView10.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView10.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView10_MouseUp);
            this.gridView10.DoubleClick += new System.EventHandler(this.gridView10_DoubleClick);
            this.gridView10.GotFocus += new System.EventHandler(this.gridView10_GotFocus);
            // 
            // colGrittingInvoiceID
            // 
            this.colGrittingInvoiceID.Caption = "Invoice ID";
            this.colGrittingInvoiceID.FieldName = "GrittingInvoiceID";
            this.colGrittingInvoiceID.Name = "colGrittingInvoiceID";
            this.colGrittingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colGrittingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colGrittingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colGrittingInvoiceID.Visible = true;
            this.colGrittingInvoiceID.VisibleIndex = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Team ID";
            this.gridColumn4.FieldName = "SubContractorID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // colInvoiceDate
            // 
            this.colInvoiceDate.Caption = "Invoice Date";
            this.colInvoiceDate.FieldName = "InvoiceDate";
            this.colInvoiceDate.Name = "colInvoiceDate";
            this.colInvoiceDate.OptionsColumn.AllowEdit = false;
            this.colInvoiceDate.OptionsColumn.AllowFocus = false;
            this.colInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colInvoiceDate.Visible = true;
            this.colInvoiceDate.VisibleIndex = 1;
            this.colInvoiceDate.Width = 102;
            // 
            // colLinkedFileName
            // 
            this.colLinkedFileName.Caption = "Linked File Name";
            this.colLinkedFileName.ColumnEdit = this.repositoryItemHyperLinkEdit6;
            this.colLinkedFileName.FieldName = "LinkedFileName";
            this.colLinkedFileName.Name = "colLinkedFileName";
            this.colLinkedFileName.OptionsColumn.ReadOnly = true;
            this.colLinkedFileName.Visible = true;
            this.colLinkedFileName.VisibleIndex = 3;
            this.colLinkedFileName.Width = 267;
            // 
            // repositoryItemHyperLinkEdit6
            // 
            this.repositoryItemHyperLinkEdit6.AutoHeight = false;
            this.repositoryItemHyperLinkEdit6.Name = "repositoryItemHyperLinkEdit6";
            this.repositoryItemHyperLinkEdit6.SingleClick = true;
            this.repositoryItemHyperLinkEdit6.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit6_OpenLink);
            // 
            // colTeamName
            // 
            this.colTeamName.Caption = "Team Name";
            this.colTeamName.FieldName = "TeamName";
            this.colTeamName.Name = "colTeamName";
            this.colTeamName.OptionsColumn.AllowEdit = false;
            this.colTeamName.OptionsColumn.AllowFocus = false;
            this.colTeamName.OptionsColumn.ReadOnly = true;
            this.colTeamName.Width = 234;
            // 
            // colLinkedCalloutCount
            // 
            this.colLinkedCalloutCount.Caption = "Linked Callouts";
            this.colLinkedCalloutCount.FieldName = "LinkedCalloutCount";
            this.colLinkedCalloutCount.Name = "colLinkedCalloutCount";
            this.colLinkedCalloutCount.OptionsColumn.AllowEdit = false;
            this.colLinkedCalloutCount.OptionsColumn.AllowFocus = false;
            this.colLinkedCalloutCount.OptionsColumn.ReadOnly = true;
            this.colLinkedCalloutCount.Visible = true;
            this.colLinkedCalloutCount.VisibleIndex = 2;
            this.colLinkedCalloutCount.Width = 92;
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.gridSplitContainer9);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage7.Text = "Sent Text Messages";
            // 
            // gridSplitContainer9
            // 
            this.gridSplitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer9.Grid = this.gridControl9;
            this.gridSplitContainer9.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer9.Name = "gridSplitContainer9";
            this.gridSplitContainer9.Panel1.Controls.Add(this.gridControl9);
            this.gridSplitContainer9.Size = new System.Drawing.Size(1265, 260);
            this.gridSplitContainer9.TabIndex = 0;
            // 
            // gridControl9
            // 
            this.gridControl9.DataSource = this.sp04082GCSubContractorSentTextMessagesBindingSource;
            this.gridControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl9.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl9.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl9_EmbeddedNavigator_ButtonClick);
            this.gridControl9.Location = new System.Drawing.Point(0, 0);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit8});
            this.gridControl9.Size = new System.Drawing.Size(1265, 260);
            this.gridControl9.TabIndex = 0;
            this.gridControl9.UseEmbeddedNavigator = true;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // sp04082GCSubContractorSentTextMessagesBindingSource
            // 
            this.sp04082GCSubContractorSentTextMessagesBindingSource.DataMember = "sp04082_GC_SubContractor_Sent_Text_Messages";
            this.sp04082GCSubContractorSentTextMessagesBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSentTextMessageID,
            this.colSubContractorID3,
            this.colSubContractorName3,
            this.colDateTimeSent,
            this.colTextMessageTypeID,
            this.colTextMessageTypeDescription,
            this.colTextMessage,
            this.colSentToNumber});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.GroupCount = 1;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.MultiSelect = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateTimeSent, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView9.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView9.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView9.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView9.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView9.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView9_MouseUp);
            this.gridView9.DoubleClick += new System.EventHandler(this.gridView9_DoubleClick);
            this.gridView9.GotFocus += new System.EventHandler(this.gridView9_GotFocus);
            // 
            // colSentTextMessageID
            // 
            this.colSentTextMessageID.Caption = "Sent Text Message ID";
            this.colSentTextMessageID.FieldName = "SentTextMessageID";
            this.colSentTextMessageID.Name = "colSentTextMessageID";
            this.colSentTextMessageID.OptionsColumn.AllowEdit = false;
            this.colSentTextMessageID.OptionsColumn.AllowFocus = false;
            this.colSentTextMessageID.OptionsColumn.ReadOnly = true;
            this.colSentTextMessageID.Width = 127;
            // 
            // colSubContractorID3
            // 
            this.colSubContractorID3.Caption = "Team ID";
            this.colSubContractorID3.FieldName = "SubContractorID";
            this.colSubContractorID3.Name = "colSubContractorID3";
            this.colSubContractorID3.OptionsColumn.AllowEdit = false;
            this.colSubContractorID3.OptionsColumn.AllowFocus = false;
            this.colSubContractorID3.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName3
            // 
            this.colSubContractorName3.Caption = "Team Name";
            this.colSubContractorName3.FieldName = "SubContractorName";
            this.colSubContractorName3.Name = "colSubContractorName3";
            this.colSubContractorName3.OptionsColumn.AllowEdit = false;
            this.colSubContractorName3.OptionsColumn.AllowFocus = false;
            this.colSubContractorName3.OptionsColumn.ReadOnly = true;
            this.colSubContractorName3.Visible = true;
            this.colSubContractorName3.VisibleIndex = 0;
            this.colSubContractorName3.Width = 252;
            // 
            // colDateTimeSent
            // 
            this.colDateTimeSent.Caption = "Date\\Time Sent";
            this.colDateTimeSent.FieldName = "DateTimeSent";
            this.colDateTimeSent.Name = "colDateTimeSent";
            this.colDateTimeSent.OptionsColumn.AllowEdit = false;
            this.colDateTimeSent.OptionsColumn.AllowFocus = false;
            this.colDateTimeSent.OptionsColumn.ReadOnly = true;
            this.colDateTimeSent.Visible = true;
            this.colDateTimeSent.VisibleIndex = 0;
            this.colDateTimeSent.Width = 108;
            // 
            // colTextMessageTypeID
            // 
            this.colTextMessageTypeID.Caption = "Text Message Type ID";
            this.colTextMessageTypeID.FieldName = "TextMessageTypeID";
            this.colTextMessageTypeID.Name = "colTextMessageTypeID";
            this.colTextMessageTypeID.OptionsColumn.AllowEdit = false;
            this.colTextMessageTypeID.OptionsColumn.AllowFocus = false;
            this.colTextMessageTypeID.OptionsColumn.ReadOnly = true;
            this.colTextMessageTypeID.Width = 129;
            // 
            // colTextMessageTypeDescription
            // 
            this.colTextMessageTypeDescription.Caption = "Text Message Type";
            this.colTextMessageTypeDescription.FieldName = "TextMessageTypeDescription";
            this.colTextMessageTypeDescription.Name = "colTextMessageTypeDescription";
            this.colTextMessageTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTextMessageTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTextMessageTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTextMessageTypeDescription.Visible = true;
            this.colTextMessageTypeDescription.VisibleIndex = 1;
            this.colTextMessageTypeDescription.Width = 178;
            // 
            // colTextMessage
            // 
            this.colTextMessage.Caption = "Text Message";
            this.colTextMessage.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.colTextMessage.FieldName = "TextMessage";
            this.colTextMessage.Name = "colTextMessage";
            this.colTextMessage.OptionsColumn.ReadOnly = true;
            this.colTextMessage.Visible = true;
            this.colTextMessage.VisibleIndex = 2;
            this.colTextMessage.Width = 238;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // colSentToNumber
            // 
            this.colSentToNumber.Caption = "Sent To Number";
            this.colSentToNumber.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.colSentToNumber.FieldName = "SentToNumber";
            this.colSentToNumber.Name = "colSentToNumber";
            this.colSentToNumber.OptionsColumn.ReadOnly = true;
            this.colSentToNumber.Visible = true;
            this.colSentToNumber.VisibleIndex = 3;
            this.colSentToNumber.Width = 186;
            // 
            // xtraTabPage9
            // 
            this.xtraTabPage9.Controls.Add(this.gridSplitContainer10);
            this.xtraTabPage9.Name = "xtraTabPage9";
            this.xtraTabPage9.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage9.Text = "Snow Clearance Rates";
            // 
            // gridSplitContainer10
            // 
            this.gridSplitContainer10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer10.Grid = this.gridControl12;
            this.gridSplitContainer10.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer10.Name = "gridSplitContainer10";
            this.gridSplitContainer10.Panel1.Controls.Add(this.gridControl12);
            this.gridSplitContainer10.Size = new System.Drawing.Size(1265, 260);
            this.gridSplitContainer10.TabIndex = 0;
            // 
            // gridControl12
            // 
            this.gridControl12.DataSource = this.sp04156GCSnowClearanceTeamRatesBindingSource;
            this.gridControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl12.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl12.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl12.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl12.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl12_EmbeddedNavigator_ButtonClick);
            this.gridControl12.Location = new System.Drawing.Point(0, 0);
            this.gridControl12.MainView = this.gridView12;
            this.gridControl12.MenuManager = this.barManager1;
            this.gridControl12.Name = "gridControl12";
            this.gridControl12.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit10,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit2DP});
            this.gridControl12.Size = new System.Drawing.Size(1265, 260);
            this.gridControl12.TabIndex = 0;
            this.gridControl12.UseEmbeddedNavigator = true;
            this.gridControl12.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView12});
            this.gridControl12.Paint += new System.Windows.Forms.PaintEventHandler(this.gridControl12_Paint);
            // 
            // sp04156GCSnowClearanceTeamRatesBindingSource
            // 
            this.sp04156GCSnowClearanceTeamRatesBindingSource.DataMember = "sp04156_GC_Snow_Clearance_Team_Rates";
            this.sp04156GCSnowClearanceTeamRatesBindingSource.DataSource = this.dataSet_GC_Snow_Core;
            // 
            // dataSet_GC_Snow_Core
            // 
            this.dataSet_GC_Snow_Core.DataSetName = "DataSet_GC_Snow_Core";
            this.dataSet_GC_Snow_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSnowClearanceSubContractorRateRule,
            this.colSubContractorID5,
            this.colSubContractorName4,
            this.colDayTypeID,
            this.colDayTypeDescription,
            this.colDayTypeOrder,
            this.colStartTime,
            this.colEndTime,
            this.colRemarks7,
            this.colHours,
            this.colRate,
            this.colMinimumHours,
            this.colMachineRate2,
            this.colMinimumHours2,
            this.colMachineRate3,
            this.colMinimumHours3,
            this.colMachineRate4,
            this.colMinimumHours4});
            this.gridView12.GridControl = this.gridControl12;
            this.gridView12.GroupCount = 1;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsSelection.MultiSelect = true;
            this.gridView12.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDayTypeOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartTime, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView12.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView12.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView12.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView12.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView12.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView12_MouseUp);
            this.gridView12.DoubleClick += new System.EventHandler(this.gridView12_DoubleClick);
            this.gridView12.GotFocus += new System.EventHandler(this.gridView12_GotFocus);
            // 
            // colSnowClearanceSubContractorRateRule
            // 
            this.colSnowClearanceSubContractorRateRule.Caption = "Rate Rule ID";
            this.colSnowClearanceSubContractorRateRule.FieldName = "SnowClearanceSubContractorRateRule";
            this.colSnowClearanceSubContractorRateRule.Name = "colSnowClearanceSubContractorRateRule";
            this.colSnowClearanceSubContractorRateRule.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceSubContractorRateRule.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceSubContractorRateRule.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceSubContractorRateRule.Width = 82;
            // 
            // colSubContractorID5
            // 
            this.colSubContractorID5.Caption = "Team ID";
            this.colSubContractorID5.FieldName = "SubContractorID";
            this.colSubContractorID5.Name = "colSubContractorID5";
            this.colSubContractorID5.OptionsColumn.AllowEdit = false;
            this.colSubContractorID5.OptionsColumn.AllowFocus = false;
            this.colSubContractorID5.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName4
            // 
            this.colSubContractorName4.Caption = "Team Name";
            this.colSubContractorName4.FieldName = "SubContractorName";
            this.colSubContractorName4.Name = "colSubContractorName4";
            this.colSubContractorName4.OptionsColumn.AllowEdit = false;
            this.colSubContractorName4.OptionsColumn.AllowFocus = false;
            this.colSubContractorName4.OptionsColumn.ReadOnly = true;
            this.colSubContractorName4.Visible = true;
            this.colSubContractorName4.VisibleIndex = 0;
            this.colSubContractorName4.Width = 222;
            // 
            // colDayTypeID
            // 
            this.colDayTypeID.Caption = "Day Type ID";
            this.colDayTypeID.FieldName = "DayTypeID";
            this.colDayTypeID.Name = "colDayTypeID";
            this.colDayTypeID.OptionsColumn.AllowEdit = false;
            this.colDayTypeID.OptionsColumn.AllowFocus = false;
            this.colDayTypeID.OptionsColumn.ReadOnly = true;
            this.colDayTypeID.Width = 81;
            // 
            // colDayTypeDescription
            // 
            this.colDayTypeDescription.Caption = "Day Type Description";
            this.colDayTypeDescription.FieldName = "DayTypeDescription";
            this.colDayTypeDescription.Name = "colDayTypeDescription";
            this.colDayTypeDescription.OptionsColumn.AllowEdit = false;
            this.colDayTypeDescription.OptionsColumn.AllowFocus = false;
            this.colDayTypeDescription.OptionsColumn.ReadOnly = true;
            this.colDayTypeDescription.Visible = true;
            this.colDayTypeDescription.VisibleIndex = 0;
            this.colDayTypeDescription.Width = 135;
            // 
            // colDayTypeOrder
            // 
            this.colDayTypeOrder.Caption = "Day Type Order";
            this.colDayTypeOrder.FieldName = "DayTypeOrder";
            this.colDayTypeOrder.Name = "colDayTypeOrder";
            this.colDayTypeOrder.OptionsColumn.AllowEdit = false;
            this.colDayTypeOrder.OptionsColumn.AllowFocus = false;
            this.colDayTypeOrder.OptionsColumn.ReadOnly = true;
            this.colDayTypeOrder.Width = 115;
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start Time";
            this.colStartTime.ColumnEdit = this.repositoryItemTextEdit3;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 1;
            this.colStartTime.Width = 90;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "T";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colEndTime
            // 
            this.colEndTime.Caption = "End Time";
            this.colEndTime.ColumnEdit = this.repositoryItemTextEdit3;
            this.colEndTime.FieldName = "EndTime";
            this.colEndTime.Name = "colEndTime";
            this.colEndTime.OptionsColumn.AllowEdit = false;
            this.colEndTime.OptionsColumn.AllowFocus = false;
            this.colEndTime.OptionsColumn.ReadOnly = true;
            this.colEndTime.Visible = true;
            this.colEndTime.VisibleIndex = 2;
            this.colEndTime.Width = 78;
            // 
            // colRemarks7
            // 
            this.colRemarks7.Caption = "Remarks";
            this.colRemarks7.ColumnEdit = this.repositoryItemMemoExEdit10;
            this.colRemarks7.FieldName = "Remarks";
            this.colRemarks7.Name = "colRemarks7";
            this.colRemarks7.OptionsColumn.ReadOnly = true;
            this.colRemarks7.Visible = true;
            this.colRemarks7.VisibleIndex = 12;
            this.colRemarks7.Width = 168;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // colHours
            // 
            this.colHours.Caption = "Elapsed Hours";
            this.colHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colHours.FieldName = "Hours";
            this.colHours.Name = "colHours";
            this.colHours.OptionsColumn.AllowEdit = false;
            this.colHours.OptionsColumn.AllowFocus = false;
            this.colHours.OptionsColumn.ReadOnly = true;
            this.colHours.Visible = true;
            this.colHours.VisibleIndex = 3;
            this.colHours.Width = 89;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colRate
            // 
            this.colRate.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colRate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colRate.AppearanceCell.Options.UseBackColor = true;
            this.colRate.AppearanceCell.Options.UseForeColor = true;
            this.colRate.Caption = "Machine Rate 1";
            this.colRate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colRate.FieldName = "Rate";
            this.colRate.Name = "colRate";
            this.colRate.OptionsColumn.AllowEdit = false;
            this.colRate.OptionsColumn.AllowFocus = false;
            this.colRate.OptionsColumn.ReadOnly = true;
            this.colRate.Visible = true;
            this.colRate.VisibleIndex = 4;
            this.colRate.Width = 95;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colMinimumHours
            // 
            this.colMinimumHours.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colMinimumHours.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMinimumHours.AppearanceCell.Options.UseBackColor = true;
            this.colMinimumHours.AppearanceCell.Options.UseForeColor = true;
            this.colMinimumHours.Caption = "Minimum Hours 1";
            this.colMinimumHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colMinimumHours.FieldName = "MinimumHours";
            this.colMinimumHours.Name = "colMinimumHours";
            this.colMinimumHours.OptionsColumn.AllowEdit = false;
            this.colMinimumHours.OptionsColumn.AllowFocus = false;
            this.colMinimumHours.OptionsColumn.ReadOnly = true;
            this.colMinimumHours.Visible = true;
            this.colMinimumHours.VisibleIndex = 5;
            this.colMinimumHours.Width = 101;
            // 
            // colMachineRate2
            // 
            this.colMachineRate2.Caption = "Machine Rate 2";
            this.colMachineRate2.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colMachineRate2.FieldName = "MachineRate2";
            this.colMachineRate2.Name = "colMachineRate2";
            this.colMachineRate2.OptionsColumn.AllowEdit = false;
            this.colMachineRate2.OptionsColumn.AllowFocus = false;
            this.colMachineRate2.OptionsColumn.ReadOnly = true;
            this.colMachineRate2.Visible = true;
            this.colMachineRate2.VisibleIndex = 6;
            this.colMachineRate2.Width = 95;
            // 
            // colMinimumHours2
            // 
            this.colMinimumHours2.Caption = "Minimum Hours 2";
            this.colMinimumHours2.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colMinimumHours2.FieldName = "MinimumHours2";
            this.colMinimumHours2.Name = "colMinimumHours2";
            this.colMinimumHours2.OptionsColumn.AllowEdit = false;
            this.colMinimumHours2.OptionsColumn.AllowFocus = false;
            this.colMinimumHours2.OptionsColumn.ReadOnly = true;
            this.colMinimumHours2.Visible = true;
            this.colMinimumHours2.VisibleIndex = 7;
            this.colMinimumHours2.Width = 101;
            // 
            // colMachineRate3
            // 
            this.colMachineRate3.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colMachineRate3.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMachineRate3.AppearanceCell.Options.UseBackColor = true;
            this.colMachineRate3.AppearanceCell.Options.UseForeColor = true;
            this.colMachineRate3.Caption = "Machine Rate 3";
            this.colMachineRate3.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colMachineRate3.FieldName = "MachineRate3";
            this.colMachineRate3.Name = "colMachineRate3";
            this.colMachineRate3.OptionsColumn.AllowEdit = false;
            this.colMachineRate3.OptionsColumn.AllowFocus = false;
            this.colMachineRate3.OptionsColumn.ReadOnly = true;
            this.colMachineRate3.Visible = true;
            this.colMachineRate3.VisibleIndex = 8;
            this.colMachineRate3.Width = 95;
            // 
            // colMinimumHours3
            // 
            this.colMinimumHours3.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colMinimumHours3.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colMinimumHours3.AppearanceCell.Options.UseBackColor = true;
            this.colMinimumHours3.AppearanceCell.Options.UseForeColor = true;
            this.colMinimumHours3.Caption = "Minimum Hours 3";
            this.colMinimumHours3.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colMinimumHours3.FieldName = "MinimumHours3";
            this.colMinimumHours3.Name = "colMinimumHours3";
            this.colMinimumHours3.OptionsColumn.AllowEdit = false;
            this.colMinimumHours3.OptionsColumn.AllowFocus = false;
            this.colMinimumHours3.OptionsColumn.ReadOnly = true;
            this.colMinimumHours3.Visible = true;
            this.colMinimumHours3.VisibleIndex = 9;
            this.colMinimumHours3.Width = 101;
            // 
            // colMachineRate4
            // 
            this.colMachineRate4.Caption = "Machine Rate 4";
            this.colMachineRate4.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colMachineRate4.FieldName = "MachineRate4";
            this.colMachineRate4.Name = "colMachineRate4";
            this.colMachineRate4.OptionsColumn.AllowEdit = false;
            this.colMachineRate4.OptionsColumn.AllowFocus = false;
            this.colMachineRate4.OptionsColumn.ReadOnly = true;
            this.colMachineRate4.Visible = true;
            this.colMachineRate4.VisibleIndex = 10;
            this.colMachineRate4.Width = 95;
            // 
            // colMinimumHours4
            // 
            this.colMinimumHours4.Caption = "Minimum Hours 4";
            this.colMinimumHours4.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colMinimumHours4.FieldName = "MinimumHours4";
            this.colMinimumHours4.Name = "colMinimumHours4";
            this.colMinimumHours4.OptionsColumn.AllowEdit = false;
            this.colMinimumHours4.OptionsColumn.AllowFocus = false;
            this.colMinimumHours4.OptionsColumn.ReadOnly = true;
            this.colMinimumHours4.Visible = true;
            this.colMinimumHours4.VisibleIndex = 11;
            this.colMinimumHours4.Width = 101;
            // 
            // xtraTabPage10
            // 
            this.xtraTabPage10.Controls.Add(this.gridControl14);
            this.xtraTabPage10.Name = "xtraTabPage10";
            this.xtraTabPage10.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPage10.Text = "Visit Types";
            // 
            // gridControl14
            // 
            this.gridControl14.DataSource = this.sp06309PersonVisitTypesBindingSource;
            this.gridControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl14.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl14.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl14.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view")});
            this.gridControl14.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl14_EmbeddedNavigator_ButtonClick);
            this.gridControl14.Location = new System.Drawing.Point(0, 0);
            this.gridControl14.MainView = this.gridView14;
            this.gridControl14.MenuManager = this.barManager1;
            this.gridControl14.Name = "gridControl14";
            this.gridControl14.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit11});
            this.gridControl14.Size = new System.Drawing.Size(1265, 260);
            this.gridControl14.TabIndex = 1;
            this.gridControl14.UseEmbeddedNavigator = true;
            this.gridControl14.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView14});
            // 
            // sp06309PersonVisitTypesBindingSource
            // 
            this.sp06309PersonVisitTypesBindingSource.DataMember = "sp06309_Person_Visit_Types";
            this.sp06309PersonVisitTypesBindingSource.DataSource = this.dataSet_OM_Core;
            // 
            // dataSet_OM_Core
            // 
            this.dataSet_OM_Core.DataSetName = "DataSet_OM_Core";
            this.dataSet_OM_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisitTypePersonID,
            this.colLinkedToPersonID,
            this.colLinkedToPersonTypeID,
            this.colVisitTypeID,
            this.colRemarks8,
            this.colLinkedToPersonName,
            this.colVisitTypeDescription});
            this.gridView14.GridControl = this.gridControl14;
            this.gridView14.GroupCount = 1;
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView14.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsSelection.MultiSelect = true;
            this.gridView14.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView14.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView14.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView14.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView14.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView14.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView14.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView14.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView14_MouseUp);
            this.gridView14.DoubleClick += new System.EventHandler(this.gridView14_DoubleClick);
            this.gridView14.GotFocus += new System.EventHandler(this.gridView14_GotFocus);
            // 
            // colVisitTypePersonID
            // 
            this.colVisitTypePersonID.Caption = "Visit Type Person ID";
            this.colVisitTypePersonID.FieldName = "VisitTypePersonID";
            this.colVisitTypePersonID.Name = "colVisitTypePersonID";
            this.colVisitTypePersonID.OptionsColumn.AllowEdit = false;
            this.colVisitTypePersonID.OptionsColumn.AllowFocus = false;
            this.colVisitTypePersonID.OptionsColumn.ReadOnly = true;
            this.colVisitTypePersonID.Width = 115;
            // 
            // colLinkedToPersonID
            // 
            this.colLinkedToPersonID.Caption = "Linked To Person ID";
            this.colLinkedToPersonID.FieldName = "LinkedToPersonID";
            this.colLinkedToPersonID.Name = "colLinkedToPersonID";
            this.colLinkedToPersonID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonID.Width = 114;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Linked To Person Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 141;
            // 
            // colVisitTypeID
            // 
            this.colVisitTypeID.Caption = "Visit Type ID";
            this.colVisitTypeID.FieldName = "VisitTypeID";
            this.colVisitTypeID.Name = "colVisitTypeID";
            this.colVisitTypeID.OptionsColumn.AllowEdit = false;
            this.colVisitTypeID.OptionsColumn.AllowFocus = false;
            this.colVisitTypeID.OptionsColumn.ReadOnly = true;
            this.colVisitTypeID.Width = 79;
            // 
            // colRemarks8
            // 
            this.colRemarks8.Caption = "Remarks";
            this.colRemarks8.ColumnEdit = this.repositoryItemMemoExEdit11;
            this.colRemarks8.FieldName = "Remarks";
            this.colRemarks8.Name = "colRemarks8";
            this.colRemarks8.OptionsColumn.ReadOnly = true;
            this.colRemarks8.Visible = true;
            this.colRemarks8.VisibleIndex = 1;
            this.colRemarks8.Width = 382;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // colLinkedToPersonName
            // 
            this.colLinkedToPersonName.Caption = "Linked To Team";
            this.colLinkedToPersonName.FieldName = "LinkedToPersonName";
            this.colLinkedToPersonName.Name = "colLinkedToPersonName";
            this.colLinkedToPersonName.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonName.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonName.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonName.Visible = true;
            this.colLinkedToPersonName.VisibleIndex = 2;
            this.colLinkedToPersonName.Width = 283;
            // 
            // colVisitTypeDescription
            // 
            this.colVisitTypeDescription.Caption = "Visit Type";
            this.colVisitTypeDescription.FieldName = "VisitTypeDescription";
            this.colVisitTypeDescription.Name = "colVisitTypeDescription";
            this.colVisitTypeDescription.OptionsColumn.AllowEdit = false;
            this.colVisitTypeDescription.OptionsColumn.AllowFocus = false;
            this.colVisitTypeDescription.OptionsColumn.ReadOnly = true;
            this.colVisitTypeDescription.Visible = true;
            this.colVisitTypeDescription.VisibleIndex = 0;
            this.colVisitTypeDescription.Width = 224;
            // 
            // xtraTabPageLinkedDocuments
            // 
            this.xtraTabPageLinkedDocuments.Controls.Add(this.gridControl25);
            this.xtraTabPageLinkedDocuments.Name = "xtraTabPageLinkedDocuments";
            this.xtraTabPageLinkedDocuments.Size = new System.Drawing.Size(1265, 260);
            this.xtraTabPageLinkedDocuments.Text = "Linked Documents";
            // 
            // gridControl25
            // 
            this.gridControl25.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl25.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl25.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl25.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl25.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl25.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl25.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl25.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl25.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl25.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl25.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl25.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl25.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl25.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl25_EmbeddedNavigator_ButtonClick);
            this.gridControl25.Location = new System.Drawing.Point(0, 0);
            this.gridControl25.MainView = this.gridView25;
            this.gridControl25.MenuManager = this.barManager1;
            this.gridControl25.Name = "gridControl25";
            this.gridControl25.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit13,
            this.repositoryItemHyperLinkEdit8});
            this.gridControl25.Size = new System.Drawing.Size(1265, 260);
            this.gridControl25.TabIndex = 2;
            this.gridControl25.UseEmbeddedNavigator = true;
            this.gridControl25.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView25});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView25
            // 
            this.gridView25.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks,
            this.colDocumentType});
            this.gridView25.GridControl = this.gridControl25;
            this.gridView25.GroupCount = 1;
            this.gridView25.Name = "gridView25";
            this.gridView25.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView25.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView25.OptionsLayout.StoreAppearance = true;
            this.gridView25.OptionsLayout.StoreFormatRules = true;
            this.gridView25.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView25.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView25.OptionsSelection.MultiSelect = true;
            this.gridView25.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView25.OptionsView.ColumnAutoWidth = false;
            this.gridView25.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView25.OptionsView.ShowGroupPanel = false;
            this.gridView25.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView25.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView25_CustomRowCellEdit);
            this.gridView25.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView25.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView25.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView25.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView25_ShowingEditor);
            this.gridView25.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView25.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView25.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView25.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView25_MouseUp);
            this.gridView25.DoubleClick += new System.EventHandler(this.gridView25_DoubleClick);
            this.gridView25.GotFocus += new System.EventHandler(this.gridView25_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit8;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit8
            // 
            this.repositoryItemHyperLinkEdit8.AutoHeight = false;
            this.repositoryItemHyperLinkEdit8.Name = "repositoryItemHyperLinkEdit8";
            this.repositoryItemHyperLinkEdit8.SingleClick = true;
            this.repositoryItemHyperLinkEdit8.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit8_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "File Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 5;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit13;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 6;
            // 
            // repositoryItemMemoExEdit13
            // 
            this.repositoryItemMemoExEdit13.AutoHeight = false;
            this.repositoryItemMemoExEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit13.Name = "repositoryItemMemoExEdit13";
            this.repositoryItemMemoExEdit13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit13.ShowIcon = false;
            // 
            // colDocumentType
            // 
            this.colDocumentType.Caption = "Document Type";
            this.colDocumentType.FieldName = "DocumentType";
            this.colDocumentType.Name = "colDocumentType";
            this.colDocumentType.OptionsColumn.AllowEdit = false;
            this.colDocumentType.OptionsColumn.AllowFocus = false;
            this.colDocumentType.OptionsColumn.ReadOnly = true;
            this.colDocumentType.Visible = true;
            this.colDocumentType.VisibleIndex = 4;
            this.colDocumentType.Width = 150;
            // 
            // sp00180_Contractor_ManagerTableAdapter
            // 
            this.sp00180_Contractor_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            // 
            // sp00181_Contractor_Manager_ContactsTableAdapter
            // 
            this.sp00181_Contractor_Manager_ContactsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00182_Contractor_Manager_CertificatesTableAdapter
            // 
            this.sp00182_Contractor_Manager_CertificatesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04022_Core_Dummy_TabPageListTableAdapter
            // 
            this.sp04022_Core_Dummy_TabPageListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04023_GC_SubContractor_Team_MembersTableAdapter
            // 
            this.sp04023_GC_SubContractor_Team_MembersTableAdapter.ClearBeforeFill = true;
            // 
            // sp04027_GC_SubContractor_Allocated_PDAsTableAdapter
            // 
            this.sp04027_GC_SubContractor_Allocated_PDAsTableAdapter.ClearBeforeFill = true;
            // 
            // sp04032_GC_Team_Gritting_InfoTableAdapter
            // 
            this.sp04032_GC_Team_Gritting_InfoTableAdapter.ClearBeforeFill = true;
            // 
            // sp04082_GC_SubContractor_Sent_Text_MessagesTableAdapter
            // 
            this.sp04082_GC_SubContractor_Sent_Text_MessagesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04131_GC_Team_Self_Billing_InvoicesTableAdapter
            // 
            this.sp04131_GC_Team_Self_Billing_InvoicesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04152_GC_Holidays_Linked_To_TeamTableAdapter
            // 
            this.sp04152_GC_Holidays_Linked_To_TeamTableAdapter.ClearBeforeFill = true;
            // 
            // sp04156_GC_Snow_Clearance_Team_RatesTableAdapter
            // 
            this.sp04156_GC_Snow_Clearance_Team_RatesTableAdapter.ClearBeforeFill = true;
            // 
            // bbiRecalculateStock
            // 
            this.bbiRecalculateStock.Caption = "Recalculate Stock Level";
            this.bbiRecalculateStock.Id = 27;
            this.bbiRecalculateStock.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRecalculateStock.ImageOptions.Image")));
            this.bbiRecalculateStock.Name = "bbiRecalculateStock";
            this.bbiRecalculateStock.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRecalculateStock_ItemClick);
            // 
            // sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter
            // 
            this.sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter.ClearBeforeFill = true;
            // 
            // sp06309_Person_Visit_TypesTableAdapter
            // 
            this.sp06309_Person_Visit_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp09119_HR_Qualifications_For_ContractorTableAdapter
            // 
            this.sp09119_HR_Qualifications_For_ContractorTableAdapter.ClearBeforeFill = true;
            // 
            // sp09120_HR_Qualifications_For_Team_MemberTableAdapter
            // 
            this.sp09120_HR_Qualifications_For_Team_MemberTableAdapter.ClearBeforeFill = true;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp00245_Core_Team_InsuranceTableAdapter
            // 
            this.sp00245_Core_Team_InsuranceTableAdapter.ClearBeforeFill = true;
            // 
            // sp00249_Core_Record_Statuses_For_FilteringTableAdapter
            // 
            this.sp00249_Core_Record_Statuses_For_FilteringTableAdapter.ClearBeforeFill = true;
            // 
            // sp01020_Core_Team_Manager_Linked_Responsible_PeopleTableAdapter
            // 
            this.sp01020_Core_Team_Manager_Linked_Responsible_PeopleTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_OM_Visit
            // 
            this.dataSet_OM_Visit.DataSetName = "DataSet_OM_Visit";
            this.dataSet_OM_Visit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00250_Core_Team_Insurance_Category_ListTableAdapter
            // 
            this.sp00250_Core_Team_Insurance_Category_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Contractor_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1270, 691);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Contractor_Manager";
            this.Text = "Team Manager";
            this.Activated += new System.EventHandler(this.frm_Core_Contractor_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Contractor_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Contractor_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlShowTabPages)).EndInit();
            this.popupContainerControlShowTabPages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04022CoreDummyTabPageListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlLinkedGrittingCalloutsFilter)).EndInit();
            this.popupContainerControlLinkedGrittingCalloutsFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00249CoreRecordStatusesForFilteringBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditLinkedRecordType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00180ContractorManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00181ContractorManagerContactsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditBoolean)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditNumeric)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00182ContractorManagerCertificatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.xtraTabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer11)).EndInit();
            this.gridSplitContainer11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTraining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09119HRQualificationsForContractorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTraining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsTraining)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControlTeamMembers)).EndInit();
            this.splitContainerControlTeamMembers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04023GCSubContractorTeamMembersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer12)).EndInit();
            this.gridSplitContainer12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTrainingTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09120HRQualificationsForTeamMemberBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTrainingTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit7)).EndInit();
            this.xtraTabPagePersonResponsibilities.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlResponsibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01020CoreTeamManagerLinkedResponsiblePeopleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewResponsibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHTML16)).EndInit();
            this.xtraTabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).EndInit();
            this.gridSplitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04152GCHolidaysLinkedToTeamBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            this.xtraTabPageInsurance.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00245CoreTeamInsuranceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditInsurance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency1)).EndInit();
            this.xtraTabPageInsuranceCategories.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInsuranceCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00250CoreTeamInsuranceCategoryListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInsuranceCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit9)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).EndInit();
            this.gridSplitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04027GCSubContractorAllocatedPDAsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).EndInit();
            this.gridSplitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04032GCTeamGrittingInfoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit25kgBags)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer8)).EndInit();
            this.gridSplitContainer8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04131GCTeamSelfBillingInvoicesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).EndInit();
            this.xtraTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer9)).EndInit();
            this.gridSplitContainer9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04082GCSubContractorSentTextMessagesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            this.xtraTabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer10)).EndInit();
            this.gridSplitContainer10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04156GCSnowClearanceTeamRatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Snow_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            this.xtraTabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06309PersonVisitTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            this.xtraTabPageLinkedDocuments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Visit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private WoodPlanDataSet woodPlanDataSet;
        private System.Windows.Forms.BindingSource sp00180ContractorManagerBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00180_Contractor_ManagerTableAdapter sp00180_Contractor_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colUser1;
        private DevExpress.XtraGrid.Columns.GridColumn colUser2;
        private DevExpress.XtraGrid.Columns.GridColumn colUser3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DataSet_AT dataSet_AT;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private System.Windows.Forms.BindingSource sp00181ContractorManagerContactsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colContactID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile1;
        private DevExpress.XtraGrid.Columns.GridColumn colFax;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultContact;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00181_Contractor_Manager_ContactsTableAdapter sp00181_Contractor_Manager_ContactsTableAdapter;
        private System.Windows.Forms.BindingSource sp00182ContractorManagerCertificatesBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00182_Contractor_Manager_CertificatesTableAdapter sp00182_Contractor_Manager_CertificatesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditBoolean;
        private DevExpress.XtraGrid.Columns.GridColumn colCertificateID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID2;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName2;
        private DevExpress.XtraGrid.Columns.GridColumn colCertificateTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colCertificateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Columns.GridColumn colIsGritter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colIsWoodPlanVisible;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colTextNumber;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditLinkedRecordType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlLinkedGrittingCalloutsFilter;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SimpleButton btnGritCalloutFilterOK;
        private DevExpress.XtraEditors.SimpleButton LoadContractorsBtn;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlShowTabPages;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.SimpleButton btnOK2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DataSet_GC_Core dataSet_GC_Core;
        private System.Windows.Forms.BindingSource sp04022CoreDummyTabPageListBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04022_Core_Dummy_TabPageListTableAdapter sp04022_Core_Dummy_TabPageListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTabPageName;
        private DevExpress.XtraGrid.Columns.GridColumn colCertificatePath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private System.Windows.Forms.BindingSource sp04023GCSubContractorTeamMembersBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMemberID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine11;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine21;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine31;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine41;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine51;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone3;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile2;
        private DevExpress.XtraGrid.Columns.GridColumn colFax1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DataSet_GC_CoreTableAdapters.sp04023_GC_SubContractor_Team_MembersTableAdapter sp04023_GC_SubContractor_Team_MembersTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit4;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private System.Windows.Forms.BindingSource sp04027GCSubContractorAllocatedPDAsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocatedPdaID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaMake;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaModel;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaFirmwareVersion;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaMobileTelNo;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaDatePurchased;
        private DevExpress.XtraGrid.Columns.GridColumn colSoftwareVersion;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAllocated;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DataSet_GC_CoreTableAdapters.sp04027_GC_SubContractor_Allocated_PDAsTableAdapter sp04027_GC_SubContractor_Allocated_PDAsTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private System.Windows.Forms.BindingSource sp04032GCTeamGrittingInfoBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorGritInformationID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID2;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn colHoldsStock;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colWarningGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colUrgentGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colStockSuppliedByDepotID;
        private DevExpress.XtraGrid.Columns.GridColumn colSellBillingInvoice;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingFrequencyDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colIsDirectLabour;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName2;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotName;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotFax;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotText;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotEmail;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotWebSite;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotCurrentGrittingLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotWarningGritLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colGritDepotUrgentGritLevel;
        private DataSet_GC_CoreTableAdapters.sp04032_GC_Team_Gritting_InfoTableAdapter sp04032_GC_Team_Gritting_InfoTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingFrequencyDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colHourlyProactiveRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colHourlyReactiveRate;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn colSentTextMessageID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID3;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName3;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeSent;
        private DevExpress.XtraGrid.Columns.GridColumn colTextMessageTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTextMessageTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTextMessage;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn colSentToNumber;
        private System.Windows.Forms.BindingSource sp04082GCSubContractorSentTextMessagesBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04082_GC_SubContractor_Sent_Text_MessagesTableAdapter sp04082_GC_SubContractor_Sent_Text_MessagesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colGritJobTransferMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSnowClearer;
        private DevExpress.XtraGrid.Columns.GridColumn colIsVatRegistered;
        private DevExpress.XtraGrid.Columns.GridColumn colGritMobileTelephoneNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingEmail;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditNumeric;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSelfBillingEmail;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.SimpleButton btnReloadInvoices;
        private DevExpress.XtraEditors.DateEdit dateEdit2;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private System.Windows.Forms.BindingSource sp04131GCTeamSelfBillingInvoicesBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedFileName;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedCalloutCount;
        private DataSet_GC_CoreTableAdapters.sp04131_GC_Team_Self_Billing_InvoicesTableAdapter sp04131_GC_Team_Self_Billing_InvoicesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage8;
        private DevExpress.XtraGrid.GridControl gridControl11;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private System.Windows.Forms.BindingSource sp04152GCHolidaysLinkedToTeamBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorHolidayID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID4;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamNotAvailable;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayType;
        private DataSet_GC_CoreTableAdapters.sp04152_GC_Holidays_Linked_To_TeamTableAdapter sp04152_GC_Holidays_Linked_To_TeamTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage9;
        private DevExpress.XtraGrid.GridControl gridControl12;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private System.Windows.Forms.BindingSource sp04156GCSnowClearanceTeamRatesBindingSource;
        private DataSet_GC_Snow_Core dataSet_GC_Snow_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceSubContractorRateRule;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID5;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName4;
        private DevExpress.XtraGrid.Columns.GridColumn colDayTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDayTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDayTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn colHours;
        private DevExpress.XtraGrid.Columns.GridColumn colRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private DataSet_GC_Snow_CoreTableAdapters.sp04156_GC_Snow_Clearance_Team_RatesTableAdapter sp04156_GC_Snow_Clearance_Team_RatesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit25kgBags;
        private DevExpress.XtraGrid.Columns.GridColumn colStartingGritAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferredInGritAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferredOutGritAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderedInGritAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colCalloutUsedGritAmount;
        private DevExpress.XtraBars.BarButtonItem bbiRecalculateStock;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSnowMobileTel;
        private DevExpress.XtraGrid.Columns.GridColumn colMissingJobSheetDeliveryMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colMissingJobSheetDeliveryMethod;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer5;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer7;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer8;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer9;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer10;
        private DevExpress.XtraGrid.Columns.GridColumn colGender;
        private DevExpress.XtraGrid.Columns.GridColumn colGenderID;
        private DevExpress.XtraGrid.Columns.GridColumn colRole;
        private DevExpress.XtraGrid.Columns.GridColumn colRoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineRate2;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineRate3;
        private DevExpress.XtraGrid.Columns.GridColumn colMachineRate4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours2;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours3;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumHours4;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colPDALoginToken;
        private DevExpress.XtraGrid.Columns.GridColumn colAutoAllocateGritJobs;
        private DevExpress.XtraGrid.Columns.GridColumn colGritJobAllocateByDefault;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAUserName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAPassword1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDALoginToken1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer6;
        private DevExpress.XtraGrid.GridControl gridControl13;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraGrid.Columns.GridColumn colPDACode;
        private System.Windows.Forms.BindingSource sp04344GCAllocatedPDALinkedTeamMembersBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocatedPdaLinkedTeamMemberID;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocatedPDAID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMemberID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAllocated1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaMake1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaModel1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaSerialNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaFirmwareVersion1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaMobileTelNo1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaDatePurchased1;
        private DevExpress.XtraGrid.Columns.GridColumn colSoftwareVersion1;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDACode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubcontractorName5;
        private DataSet_GC_CoreTableAdapters.sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colIsUtilityArbTeam;
        private DevExpress.XtraGrid.Columns.GridColumn colIsOperationsTeam;
        private DevExpress.XtraGrid.Columns.GridColumn colSupplierCode;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage10;
        private DevExpress.XtraGrid.GridControl gridControl14;
        private System.Windows.Forms.BindingSource sp06309PersonVisitTypesBindingSource;
        private DataSet_OM_Core dataSet_OM_Core;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypePersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeDescription;
        private DataSet_OM_CoreTableAdapters.sp06309_Person_Visit_TypesTableAdapter sp06309_Person_Visit_TypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAVersion;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage11;
        private DevExpress.XtraGrid.GridControl gridControlTraining;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTraining;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationFromID;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseName;
        private DevExpress.XtraGrid.Columns.GridColumn colCostToCompany;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit14;
        private DevExpress.XtraGrid.Columns.GridColumn colSummerMaintenance;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit13;
        private DevExpress.XtraGrid.Columns.GridColumn colWinterMaintenance;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArb;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityRail;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodPlan;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationType;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysUntilExpiry;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount7;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsTraining;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colArchived;
        private DevExpress.XtraGrid.Columns.GridColumn colAssessmentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseStartDate;
        private System.Windows.Forms.BindingSource sp09119HRQualificationsForContractorBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DataSet_Common_FunctionalityTableAdapters.sp09119_HR_Qualifications_For_ContractorTableAdapter sp09119_HR_Qualifications_For_ContractorTableAdapter;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer11;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControlTeamMembers;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer12;
        private DevExpress.XtraGrid.GridControl gridControlTrainingTeam;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTrainingTeam;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private System.Windows.Forms.BindingSource sp09120HRQualificationsForTeamMemberBindingSource;
        private DataSet_Common_FunctionalityTableAdapters.sp09120_HR_Qualifications_For_Team_MemberTableAdapter sp09120_HR_Qualifications_For_Team_MemberTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedDocuments;
        private DevExpress.XtraGrid.GridControl gridControl25;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView25;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit13;
        private DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentType;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colLegalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colUniqueTaxRef;
        private DevExpress.XtraGrid.Columns.GridColumn colIsConstruction;
        private DevExpress.XtraGrid.Columns.GridColumn colIsFencing;
        private DevExpress.XtraGrid.Columns.GridColumn colIsPestControl;
        private DevExpress.XtraGrid.Columns.GridColumn colIsRailArb;
        private DevExpress.XtraGrid.Columns.GridColumn colIsRoofing;
        private DevExpress.XtraGrid.Columns.GridColumn colIsWindowCleaning;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageInsurance;
        private DevExpress.XtraGrid.GridControl gridControl17;
        private System.Windows.Forms.BindingSource sp00245CoreTeamInsuranceBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView17;
        private DevExpress.XtraGrid.Columns.GridColumn colInsuranceID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks9;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit15;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedCertificate;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditInsurance;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingPublicValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency1;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearancePublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMaintenancePublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMaintenanceEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colConstructionPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colConstructionEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colAmenityArbPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colAmenityArbEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArbPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArbEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRailArbPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRailArbEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPestControlPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPestControlEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colFencingPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colFencingEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRoofingPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRoofingEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colWindowCleaningPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colWindowCleaningEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName6;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit14;
        private WoodPlanDataSetTableAdapters.sp00245_Core_Team_InsuranceTableAdapter sp00245_Core_Team_InsuranceTableAdapter;
        private System.Windows.Forms.BindingSource sp00249CoreRecordStatusesForFilteringBindingSource;
        private WoodPlanDataSetTableAdapters.sp00249_Core_Record_Statuses_For_FilteringTableAdapter sp00249_Core_Record_Statuses_For_FilteringTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colMinGrittingPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinGrittingEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinSnowClearancePublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinSnowClearanceEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinMaintenancePublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinMaintenanceEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinConstructionPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinConstructionEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinAmenityArbPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinAmenityArbEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinUtilityArbPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinUtilityArbEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinRailArbPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinRailArbEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinPestControlPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinPestControlEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinFencingPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinFencingEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinRoofingPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinRoofingEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinWindowCleaningPublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinWindowCleaningEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingPublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceEmployersInsuranceValid1;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colMaintenancePublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colMaintenanceEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colConstructionPublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colConstructionEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colAmenityArbPublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colAmenityArbEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArbPublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArbEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colRailArbPublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colRailArbEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colPestControlPublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colPestControlEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colFencingPublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colFencingEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colRoofingPublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colRoofingEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colWindowCleaningPublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colWindowCleaningEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveTeamMembers;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePersonResponsibilities;
        private DevExpress.XtraGrid.GridControl gridControlResponsibility;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewResponsibility;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonResponsibilityID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleForRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleForRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibilityTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit16;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHTML16;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibilityType;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonTypeID;
        private System.Windows.Forms.BindingSource sp01020CoreTeamManagerLinkedResponsiblePeopleBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName2;
        private DataSet_Common_FunctionalityTableAdapters.sp01020_Core_Team_Manager_Linked_Responsible_PeopleTableAdapter sp01020_Core_Team_Manager_Linked_Responsible_PeopleTableAdapter;
        private DataSet_OM_Visit dataSet_OM_Visit;
        private DevExpress.XtraGrid.Columns.GridColumn colOMPaidDiscountedVisitRate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsActiveTotalViewUser;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalViewUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTest;
        private DevExpress.XtraGrid.Columns.GridColumn colIsOMSelfBillingEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colIsPotholeTeam;
        private DevExpress.XtraGrid.Columns.GridColumn colPotholePublicInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colPotholeEmployersInsuranceValid;
        private DevExpress.XtraGrid.Columns.GridColumn colPotholePublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPotholeEmployersValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinPotholePublicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colMinPotholeEmployersValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colJoinedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTerminationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded2;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSpecialist;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultContactEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultContactID;
        private DevExpress.XtraGrid.Columns.GridColumn colNatInsuranceNumber;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageInsuranceCategories;
        private DevExpress.XtraGrid.GridControl gridControlInsuranceCategory;
        private System.Windows.Forms.BindingSource sp00250CoreTeamInsuranceCategoryListBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInsuranceCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamInsuranceCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInsuranceCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colActive2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit17;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private WoodPlanDataSetTableAdapters.sp00250_Core_Team_Insurance_Category_ListTableAdapter sp00250_Core_Team_Insurance_Category_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName3;
        private DevExpress.XtraGrid.Columns.GridColumn colCategoryStateID;
        private DevExpress.XtraGrid.Columns.GridColumn colCategoryState;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colNoSelfBilling;
    }
}

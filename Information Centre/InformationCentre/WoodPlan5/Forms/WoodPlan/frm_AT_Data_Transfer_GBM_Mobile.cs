using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Repository;

using System.IO;
//using System.Text;  // StringBuilder Class //

using MapInfo.Engine;
using MapInfo.Geometry;
using MapInfo.Mapping;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;


namespace WoodPlan5
{
    public partial class frm_AT_Data_Transfer_GBM_Mobile : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        private ExtendedGridViewFunctions extGridViewFunction;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;
        private int i_int_FocusedGrid = 1;

        private int i_intLoadedTemplateID = 0;
        private int i_intLoadedTemplateCreatedByID = 0;

        private string i_str_selected_locality_ids = "";
        private string i_str_selected_locality_names = "";

        private string i_str_selected_map_file_ids = "";
        private string i_str_selected_map_file_names = "";

        private string i_str_selected_workorder_ids = "";
        private string i_str_selected_workorder_names = "";

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        BaseObjects.GridCheckMarksSelection selection4;
        BaseObjects.GridCheckMarksSelection selection5;

        /// <summary>
        /// 
        /// </summary>
        public RefreshGridState RefreshGridViewState;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the checkbox editor //

        private double doubleImportXMin = 0;
        private double doubleImportYMin = 0;
        private double doubleImportXMax = 0;
        private double doubleImportYMax = 0;

        const string NotFoundText = "???";  // Used to display ??? in GridLookUpEdits when a matching value is not present //

        AutoScrollHelper autoScrollHelper;

        private string strDefaultMappingProjection = "";

        #endregion

        public frm_AT_Data_Transfer_GBM_Mobile()
        {
            InitializeComponent();
        }

        private void frm_AT_Data_Transfer_GBM_Mobile_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet_AT_DataTransfer.sp01323_AT_import_from_GBM_workorder_checking' table. You can move, or remove it, as needed.
            this.sp01323_AT_import_from_GBM_workorder_checkingTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp01323_AT_import_from_GBM_workorder_checking);
            this.LockThisWindow();  // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            this.FormID = 2002;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            this.sp00100_export_to_GBM_locality_filter_listTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp00100_export_to_GBM_locality_filter_listTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00100_export_to_GBM_locality_filter_list);

            this.sp00112_export_to_GBM_saved_templates_listTableAdapter.Connection.ConnectionString = strConnectionString;
            GridView view = (GridView)gridControl1.MainView;
            RefreshGridViewState = new RefreshGridState(view, "TemplateID");
            Load_Templates_List(view, false);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            this.sp00099_export_to_GBM_default_group_listTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp00099_export_to_GBM_default_group_listTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00099_export_to_GBM_default_group_list, 0);

            this.sp00102_export_to_GBM_map_scale_listTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp00102_export_to_GBM_map_scale_listTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00102_export_to_GBM_map_scale_list);
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            this.sp00098_export_to_GBM_field_listTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp00098_export_to_GBM_field_listTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00098_export_to_GBM_field_list);
            
            this.sp00101_export_to_GBM_map_centre_locality_listTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp00135_export_to_GBM_map_background_filesTableAdapter.Connection.ConnectionString = strConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            this.sp00103_export_to_GBM_picklist_itemsTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp00103_export_to_GBM_picklist_itemsTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00103_export_to_GBM_picklist_items);
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
           
            gridLookUpEdit2.EditValue = 2500;  // Default value for Map Scale//


            dateEditWorkOrderCheckingFromDate.DateTime = this.GlobalSettings.ViewedStartDate;
            dateEditWorkOrderCheckingToDate.DateTime = this.GlobalSettings.ViewedEndDate;
            this.sp01321_AT_export_to_GBM_available_workordersTableAdapter.Connection.ConnectionString = strConnectionString;
            Refresh_WorkOrder_List();
            // Force Grid to load including their views and data - Needed because these grid are on PopupContainerControls so they are normally only loaded on first Popup //
            gridControl9.ForceInitialize();


            // Add record selection checkboxes to grid and popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl8.MainView);
            selection4.CheckMarkColumn.VisibleIndex = 0;
            selection4.CheckMarkColumn.Width = 30;

            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl9.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;

            Load_Centre_Map_Locality_List();          
            Load_Map_File_List();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            Set_Required_Columns();

            // Force Grid to load including their views and data - Needed because these grid are on PopupContainerControls so they are normally only loaded on first Popup //
            gridControl4.ForceInitialize();
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Force Grid to load including their views and data - Needed because these grid are on PopupContainerControls so they are normally only loaded on first Popup //
            gridControl5.ForceInitialize();
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            Set_PickListItem_Filter_Status(1);  // Pre-tick all filter items //

            Set_Picklist_Item_Text();  // Set Picklist text [x / x] for each picklist //

            emptyEditor = new RepositoryItem();
            gridControl2.RepositoryItems.Add(emptyEditor);
            gridControl3.RepositoryItems.Add(emptyEditor);

            // Style Fields //
            sp00131_export_to_GBM_style_fieldsTableAdapter.Connection.ConnectionString = strConnectionString;
            glueStyleField.EditValue = null;

            // Force Grid to load including their views and data - Needed because these grid are on PopupContainerControls so they are normally only loaded on first Popup //
            gridControl8.ForceInitialize();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            // Populate Import Grid - list of Species //
            sp00136_import_from_GBM_locality_species_list_with_blankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00136_import_from_GBM_locality_species_list_with_blankTableAdapter.Fill(dataSet_AT_DataTransfer.sp00136_import_from_GBM_locality_species_list_with_blank);

            // Populate Import Grid - list of Tree Statuses //
            sp00137_import_from_GBM_ltree_status_list_with_blankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00137_import_from_GBM_ltree_status_list_with_blankTableAdapter.Fill(dataSet_AT_DataTransfer.sp00137_import_from_GBM_ltree_status_list_with_blank);

            // Populate Import Grid - list of Districts //
            sp00138_import_from_GBM_districts_list_with_blankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00138_import_from_GBM_districts_list_with_blankTableAdapter.Fill(dataSet_AT_DataTransfer.sp00138_import_from_GBM_districts_list_with_blank);

            //Populate Import Grid - list of Localities //
            sp00139_import_from_GBM_locailities_list_with_blankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00139_import_from_GBM_locailities_list_with_blankTableAdapter.Fill(dataSet_AT_DataTransfer.sp00139_import_from_GBM_locailities_list_with_blank);

            // View Column lookupedit list on Import Page populated in PostOpen //

            new GridViewBookmarks(this.gridView7, "ID", "ID");  // Add BookMark Functionality to Grid //

            autoScrollHelper = new AutoScrollHelper(gridView3);  // Add Autoscroll functionality when dragging to adjust record order //
            gridControl3.AllowDrop = true;

            this.sp01323_AT_import_from_GBM_workorder_checkingTableAdapter.Connection.ConnectionString = strConnectionString;

            // Get Default Mapping Projection //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            strDefaultMappingProjection = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesDefaultMappingProjection").ToString();
            if (string.IsNullOrEmpty(strDefaultMappingProjection)) strDefaultMappingProjection = "CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000";

        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);

            Set_Controls_Enabled_Status(comboBoxEditExportType);  // Enabled / Disable controls depending on Export Type //
            
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        
            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
        }

        private void frm_AT_Data_Transfer_GBM_Mobile_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            switch (xtraTabControl1.SelectedTabPage.Name.ToString())
            {
                case "xtraTabPage1":
                    gridControl1.Focus();
                    {
                        break;
                    }
                case "xtraTabPage2":
                    gridControl7.Focus();
                    {
                        break;
                    }
            }
        }

        private void Load_Templates_List(GridView view, Boolean boolLoadGridState)
        {
            view.BeginUpdate();
            this.sp00112_export_to_GBM_saved_templates_listTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00112_export_to_GBM_saved_templates_list, GlobalSettings.UserID, GlobalSettings.PersonType);
            if (boolLoadGridState) this.RefreshGridViewState.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();
        }

        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            if (view.GridControl.MainView.Name != "gridView3") return;  // Only fire for appropriate view //
            if (row == GridControl.InvalidRowHandle) return;
            if (row < 0)  // Group or viewed header checkbox clicked //
            {
                Set_Required_Columns();
            }
            else  // Indivdual row checkbox clicked //
            {
                int intStyleField = 0;
                if (glueStyleField.EditValue != null) intStyleField = Convert.ToInt32(glueStyleField.EditValue);
                if (Convert.ToInt32(view.GetRowCellValue(row, "ColumnID")) == intStyleField && Convert.ToInt32(view.GetRowCellValue(row, "CheckMarkSelection")) == 0)
                {
                    if (DevExpress.XtraEditors.XtraMessageBox.Show("You have de-selected the column controlling the styles for export. If you proceed, the styles will be cleared!\n\nAre you sure you wish to proceed?", "De-Select Column For Export", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        view.SetRowCellValue(row, "CheckMarkSelection", 1);
                    }
                    else
                    {
                        glueStyleField.EditValue = null;
                        GridView styleView = (GridView)gridControl6.MainView;
                        styleView.BeginUpdate();
                        this.dataSet_AT_DataTransfer.sp00131_export_to_GBM_style_fields.Rows.Clear();
                        styleView.EndUpdate();
                    }
                }
                else if (Convert.ToInt32(view.GetRowCellValue(row, "Required")) == 1 && Convert.ToInt32(view.GetRowCellValue(row, "CheckMarkSelection")) == 0) view.SetRowCellValue(row, "CheckMarkSelection", 1);
            }
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }
        
        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            bbiDelete.Enabled = false;
            if (i_int_FocusedGrid == 1)
            {
                if (iBool_AllowDelete)
                {
                    GridView view1 = (GridView)gridControl1.MainView;
                    int[] intSelectedRows = view1.GetSelectedRows();
                    if (intSelectedRows.Length > 0)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                }
            }
            else if (i_int_FocusedGrid == 7) // Import Grid //
            {
                GridView view7 = (GridView)gridControl7.MainView;
                int[] intSelectedRows = view7.GetSelectedRows();
                if (intSelectedRows.Length > 0 && iBool_AllowDelete)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void Load_Centre_Map_Locality_List()
        {
            this.sp00101_export_to_GBM_map_centre_locality_listTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00101_export_to_GBM_map_centre_locality_list, i_str_selected_locality_ids);
            gridLookUpEdit1.EditValue = 0;
        }

        private void Load_Map_File_List()
        {
            this.sp00135_export_to_GBM_map_background_filesTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00135_export_to_GBM_map_background_files, i_str_selected_locality_ids);
            popupContainerEdit3.Text = "No Background Files Selected";
            i_str_selected_map_file_ids = "";
            i_str_selected_map_file_names = "";
            selection4.ClearSelection();
        }

        private void Set_Required_Columns()
        {
            // Make sure all required rows are set as checked //
            GridView view = (GridView)gridControl3.MainView;
            GridColumn column = view.Columns["Required"];
            int intRowHandle = 0;
            int intStartRow = 0;
            while (intRowHandle != GridControl.InvalidRowHandle)
            {
                intRowHandle = view.LocateByValue(intStartRow, column, 1);
                if (intRowHandle != GridControl.InvalidRowHandle)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);
                    intStartRow++;
                }
            }
        }

        private void Set_PickListItem_Filter_Status(int intValue)
        {
            if (intValue == 1)
            {
                selection3.SelectAll();
            }
            else
            {
                selection3.ClearSelection();
            }
        }

        private void Set_Picklist_Item_Text()
        {
            // Check if any other items are sharing this picklist and update their count [x / x] if yes //
            GridView view = (GridView)gridControl3.MainView;
            GridView view2 = (GridView)gridControl5.MainView;
            string strSelectedItems = "";
            int intSelectedCount = 0;
            int intTotalCount = 0;
            int intPicklistID = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                intPicklistID = Convert.ToInt32(view.GetRowCellValue(i, "PickList"));
                if (intPicklistID > 0 && Convert.ToString(view.GetRowCellValue(i, "PicklistSummery")) == "")
                {
                    if (view2.DataRowCount <= 0)
                    {
                        strSelectedItems = "0/0";
                    }
                    else
                    {
                        intTotalCount = 0;
                        intSelectedCount = 0;
                        for (int j = 0; j < view2.DataRowCount; j++)
                        {
                            if (Convert.ToInt32(view2.GetRowCellValue(j, "HeaderID")) == intPicklistID)
                            {
                                intTotalCount++;
                                if (Convert.ToInt32(view2.GetRowCellValue(j, "CheckMarkSelection")) == 1)
                                {
                                    intSelectedCount++;
                                }
                            }
                        }
                        strSelectedItems = Convert.ToInt32(intSelectedCount) + "/" + Convert.ToInt32(intTotalCount);
                    }
                    view.SetRowCellValue(i, "PicklistSummery", strSelectedItems);  // Update Current Row //
                    // Check if this picklist is shared - if yes then update these too //
                    for (int k = i; k < view.DataRowCount; k++)
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(k, "PickList")) == intPicklistID && Convert.ToString(view.GetRowCellValue(k, "PicklistSummery")) == "")
                        {
                            view.SetRowCellValue(k, "PicklistSummery", strSelectedItems);  // Update Matching Row //
                        }
                    }
                }
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No saved export templates available";
                    break;
                case "gridView2":
                    message = "No Groups Available";
                    break;
                case "gridView6":
                    message = "No styles defined - select a value from the Style Field to proceed";
                    break;
                case "gridView7":
                    message = "No data available for importing - click Load File button to get data";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView7":
                    int[] SelectedRows = view.GetSelectedRows();
                    btnBlockEdit.Enabled = (SelectedRows.Length > 0 ? true : false);
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region grid3

        #region Drag Drop Functionality

        // AutoScrollHelper autoScrollHelper declared under instance vars //
        // autoScrollHelper linked to grid in Form Load Event //
        // gridControl3.AllowDrop = true in form Load Event //

        int dropTargetRowHandle = -1;
        int DropTargetRowHandle 
        {
            get 
            { 
                return dropTargetRowHandle; 
            }
            set 
            {
                dropTargetRowHandle = value;
                gridControl3.Invalidate();
            }
        }
        const string OrderFieldName = "ColumnOrder";
        //string strCurrentWO = "";

        private void gridControl3_DragOver(object sender, DragEventArgs e)
        {
            GridControl grid = (GridControl)sender;
            System.Drawing.Point pt = new System.Drawing.Point(e.X, e.Y);
            pt = grid.PointToClient(pt);
            GridView view = grid.GetViewAt(pt) as GridView;
            if (view == null) return;
            GridHitInfo hitInfo = view.CalcHitInfo(pt);
            if (hitInfo.HitTest == GridHitTest.EmptyRow)
            {
                DropTargetRowHandle = view.DataRowCount;
            }
            else
            {
                DropTargetRowHandle = hitInfo.RowHandle;
            }

            e.Effect = DragDropEffects.None;

            GridHitInfo downHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            if (downHitInfo != null)
            {
                if (hitInfo.RowHandle != downHitInfo.RowHandle) e.Effect = DragDropEffects.Move;
            }

            autoScrollHelper.ScrollIfNeeded(hitInfo);
        }

        private void gridControl3_DragDrop(object sender, DragEventArgs e)
        {

            GridControl grid = sender as GridControl;
            GridView view = grid.MainView as GridView;
            GridHitInfo srcHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            GridHitInfo hitInfo = view.CalcHitInfo(grid.PointToClient(new System.Drawing.Point(e.X, e.Y)));
            int sourceRow = srcHitInfo.RowHandle;
            int targetRow = (hitInfo.RowHandle == 0 ? 0 : (hitInfo.RowHandle > 0 ? hitInfo.RowHandle - 1 : view.DataRowCount - 1));

            if (DropTargetRowHandle < 0) return;  // No Valid Place for Dropping //

            DropTargetRowHandle = -1;
            MoveRow(sourceRow, targetRow);
        }

        private void gridControl3_DragLeave(object sender, EventArgs e)
        {
            DropTargetRowHandle = -1;
        }

        private void gridControl3_Paint(object sender, PaintEventArgs e)
        {
            if (DropTargetRowHandle < 0) return;
            GridControl grid = (GridControl)sender;
            GridView view = (GridView)grid.MainView;

            bool isBottomLine = DropTargetRowHandle == view.DataRowCount;

            GridViewInfo viewInfo = view.GetViewInfo() as GridViewInfo;
            GridRowInfo rowInfo = viewInfo.GetGridRowInfo(isBottomLine ? DropTargetRowHandle - 1 : DropTargetRowHandle);

            if (rowInfo == null) return;

            System.Drawing.Point p1, p2;
            if (isBottomLine)
            {
                p1 = new System.Drawing.Point(rowInfo.Bounds.Left, rowInfo.Bounds.Bottom - 1);
                p2 = new System.Drawing.Point(rowInfo.Bounds.Right, rowInfo.Bounds.Bottom - 1);
            }
            else
            {
                p1 = new System.Drawing.Point(rowInfo.Bounds.Left, rowInfo.Bounds.Top - 1);
                p2 = new System.Drawing.Point(rowInfo.Bounds.Right, rowInfo.Bounds.Top - 1);
            }
            e.Graphics.DrawLine(Pens.Blue, p1, p2);
        }

        private void gridView3_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Button == MouseButtons.Left && downHitInfo != null)
            {
                Size dragSize = SystemInformation.DragSize;
                System.Drawing.Rectangle dragRect = new System.Drawing.Rectangle(new System.Drawing.Point(downHitInfo.HitPoint.X - dragSize.Width / 2, downHitInfo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new System.Drawing.Point(e.X, e.Y)))
                {
                    view.GridControl.DoDragDrop(downHitInfo, DragDropEffects.All);
                    downHitInfo = null;
                }
            }
        }

        private void MoveRow(int sourceRow, int targetRow)
        {
            if (sourceRow == targetRow || (sourceRow == targetRow + 1 && targetRow != 0)) return;

            GridView view = gridView3;
            DataRow row1 = view.GetDataRow(targetRow);
            DataRow row2 = view.GetDataRow(targetRow + 1);
            DataRow dragRow = view.GetDataRow(sourceRow);
            decimal val1 = (decimal)row1[OrderFieldName];

            if (row2 == null)
                dragRow[OrderFieldName] = val1 + 1;
            else
            {
                if (targetRow == 0)
                {
                    dragRow[OrderFieldName] = val1 / 2;
                }
                else
                {
                    decimal val2 = (decimal)row2[OrderFieldName];
                    dragRow[OrderFieldName] = (val1 + val2) / 2;
                }
            }
            // Update Parent Level Grouping if required //
            int intGroup = (int)row1["GroupOrder"];
            if ((int)dragRow["GroupOrder"] != intGroup) dragRow["GroupOrder"] = intGroup;
        }

        #endregion
       

        private void gridView3_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            // Custom sorting is enabled for the GroupName column (its GridColumn.SortMode property is set to ColumnSortMode.Custom). //
            GridView view = (GridView)sender;
            if (e.Column.FieldName == "GroupName")
            {
                int val1 = Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex1, "GroupOrder"));
                int val2 = Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex2, "GroupOrder"));
                e.Handled = true;
                e.Result = val1 > val2 ? 1 : val1 == val2 ? 0 : -1;
            }
        }

        private void gridView3_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "UseLastRecord":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "UseLastRecordAvailable")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "PicklistSummery":  // Don't show Picklist editor if no picklist linked or column = Tree Status //
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "PickList")) <= 0 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ColumnID")) == 8) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (i_intLoadedTemplateID == 0)
                {
                    bbiSaveTemplateAs.Enabled = false;
                }
                else
                {
                    bbiSaveTemplateAs.Enabled = true;
                }

                if (i_intLoadedTemplateCreatedByID != GlobalSettings.UserID && i_intLoadedTemplateID > 0 && GlobalSettings.PersonType != 0)
                {
                    bbiSaveTemplate.Enabled = false;
                }
                else
                {
                    bbiSaveTemplate.Enabled = true;
                }
                BarButtonItem bbiGroupItem;
                bsiGroup.ClearLinks();
                for (int i = 0; i < this.dataSet_AT_DataTransfer.sp00099_export_to_GBM_default_group_list.Rows.Count; i++)
                {
                    if (this.dataSet_AT_DataTransfer.sp00099_export_to_GBM_default_group_list.Rows[i]["GroupName"].ToString() != "")
                    {
                        bbiGroupItem = new BarButtonItem();
                        bbiGroupItem.Tag = Convert.ToInt32(this.dataSet_AT_DataTransfer.sp00099_export_to_GBM_default_group_list.Rows[i]["GroupOrder"]);
                        bbiGroupItem.Caption = this.dataSet_AT_DataTransfer.sp00099_export_to_GBM_default_group_list.Rows[i]["GroupName"].ToString();
                        bbiGroupItem.Name = "iLayoutItem";
                        bsiGroup.AddItem(bbiGroupItem);
                        bbiGroupItem.ItemClick += new ItemClickEventHandler(bbiGroupItem_ItemClick);
                    }
                }
                pmGrid3Menu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView3_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent editing of Filter Summary if row had no associated picklist [PickList column value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "PicklistSummery":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("PickList")) <= 0) e.Cancel = true;
                    else if (Convert.ToInt32(view.GetFocusedRowCellValue("ColumnID")) == 8) e.Cancel = true;  // Don't allow Status list to be filtered //
                    break;
                case "UseLastRecord":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("UseLastRecordAvailable")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridView3_ShownEditor(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            if ((view.FocusedColumn.FieldName == "PicklistSummery"))
            {
                int intHeaderID = Convert.ToInt32(view.GetFocusedRowCellValue("PickList"));
                if (intHeaderID != 0)
                {
                    view = (GridView)gridControl5.MainView;
                    view.BeginUpdate();

                    view.ActiveFilter.Clear();
                    view.ActiveFilter.NonColumnFilter = "[HeaderID] = " + Convert.ToString(intHeaderID);
                    /*
                    while (true)  // Locate all rows matching value //
                    {
                        intRowHandle = view.LocateByValue(intRowHandle + 1, view.Columns["HeaderID"], intHeaderID);
                        if (intRowHandle == GridControl.InvalidRowHandle)
                        {
                            break;  // exit the loop if no row is found //
                        }
                        if (intFirstFoundRow == -1)
                        {
                            intFirstFoundRow = intRowHandle;
                        }
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                    view.EndUpdate();
                    if (intFirstFoundRow > -1)
                    {
                        view.MakeRowVisible(intFirstFoundRow, true);
                        view.Invalidate();
                    }*/
                    view.MakeRowVisible(-1, true);
                    view.EndUpdate();
                }             
            }
        }

        private void gridView3_HiddenEditor(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            if ((view.FocusedColumn.FieldName == "PicklistSummery"))
            {              
                view = (GridView)gridControl5.MainView;
                view.ActiveFilter.Clear();
            }
        }

        private void gridView3_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedColumn.Name == "colColumnLabel")
            {
                string strNewValue;
                if (e.Value == DBNull.Value)
                {
                    strNewValue = null;
                }
                else
                {
                    strNewValue = Convert.ToString(e.Value).Trim();
                }
                DataRow row = view.GetDataRow(view.FocusedRowHandle);
                string strDescription;
                if (e.Value == DBNull.Value)
                {
                    strDescription = null;
                }
                else
                {
                    strDescription = Convert.ToString(e.Value).Trim();
                }
                if (strDescription == "" || strDescription == null)
                {
                    row.SetColumnError("ColumnLabel", view.Columns["ColumnLabel"].Caption + " requires a value.");
                }
                else
                {
                    row.SetColumnError("ColumnLabel", "");  // Clear Error Text //
                }
            }
            else if (view.FocusedColumn.Name == "colPicklistSummery")
            {
                // Check if any other items are sharing this picklist and update their count if yes //
                int intfocusedRow = view.FocusedRowHandle;
                string strValue = e.Value.ToString();
                int intPicklistID = Convert.ToInt32(view.GetFocusedRowCellValue("PickList"));
                if (intPicklistID <= 0) return;
                //view.BeginUpdate();
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "PickList")) == intPicklistID && i != intfocusedRow)
                    {
                        view.SetRowCellValue(i, "PicklistSummery", strValue);
                    }
                }
                //view.EndUpdate();
            }

        }
        
        private void bbiChecked_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);
                }
                view.EndUpdate();
            }
        }

        private void bbiUnchecked_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intStyleField = 0;
            if (glueStyleField.EditValue != null) intStyleField = Convert.ToInt32(glueStyleField.EditValue);
            
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ColumnID")) != intStyleField)
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Required")) == 0) view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 0);
                    }
                }
                view.EndUpdate();
            }
        }

        private void bbiGroupItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            BarButtonItem bbiGroupItem = (BarButtonItem)e.Item;
            int intGroupOrder = Convert.ToInt32(bbiGroupItem.Tag);
            String strGroupName = bbiGroupItem.Caption;

            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                view.BeginDataUpdate();  // Prevent grid from re-calculating group and sort positions until process is complete //
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "GroupName", strGroupName);
                    view.SetRowCellValue(intRowHandle, "GroupOrder", intGroupOrder);
                }
                view.EndDataUpdate();
                view.EndUpdate();
                view.MakeRowVisible(view.FocusedRowHandle, true);  // Scroll grid to show focused row //
            }
        }

        private void bbiSaveTemplate_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (i_intLoadedTemplateID == 0)  // Nothing saves so open screen to allow user to input a description etc //
            {
                SaveTemplateHeaderInfo();
            }
            else if (i_intLoadedTemplateCreatedByID != GlobalSettings.UserID && GlobalSettings.PersonType != 0)  // Template open but user didn't create it and they're not a super user so abort //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You can only save changes to Export Templates you own.\n\nYou did not create the currently loaded template so you cannot change it!\n\nSave Changes Cancelled.", "Save Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else  // Ok to save changes so update associated info //
            {
                // Update Header first //
                string strMapCentreID = gridLookUpEdit1.EditValue.ToString();
                string strMapScale = gridLookUpEdit2.EditValue.ToString();
                int? intStyleField = null;
                if (glueStyleField.EditValue != null) intStyleField = Convert.ToInt32(glueStyleField.EditValue);
                int intIncludeLastInspection = (IncludeLastInspectionDetailsCheckEdit.Checked ? 1 : 0);

                DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter SaveTemplate = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                SaveTemplate.ChangeConnectionString(strConnectionString);
                try
                {
                    SaveTemplate.sp00107_export_to_GBM_save_template_header_and_access("update", i_intLoadedTemplateID, 0, "", "", i_str_selected_locality_ids, strMapCentreID, strMapScale, 0, "", intStyleField, i_str_selected_map_file_ids, intIncludeLastInspection);
                }
                catch
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save template - an error occurred while saving the template header!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                this.fProgress = new frmProgress(10);  // Show Progress Window //

                // Clear associated info for this header first [it will be recreated in SaveTemplateAssociatedInfo] //
                DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter ClearTemplateAssociatedData = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                ClearTemplateAssociatedData.ChangeConnectionString(strConnectionString);
                try
                {
                    ClearTemplateAssociatedData.sp00111_export_to_GBM_save_template_clear_associated_data(i_intLoadedTemplateID);
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save template - an error occurred while clearing the previous version!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                
                this.fProgress = new frmProgress(10);  // Show Progress Window //
                this.ParentForm.AddOwnedForm(fProgress);
                fProgress.UpdateCaption("Saving Template, Please Wait...");
                fProgress.Show();
                Application.DoEvents();

                SaveTemplateAssociatedInfo(i_intLoadedTemplateID);
            }

        }

        private void bbiSaveTemplateAs_ItemClick(object sender, ItemClickEventArgs e)
        {           
            SaveTemplateHeaderInfo();
        }

        private void SaveTemplateHeaderInfo()
        {
            frm_AT_Data_Transfer_GBM_Mobile_Template_Save fChildForm = new frm_AT_Data_Transfer_GBM_Mobile_Template_Save();
            this.ParentForm.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;

            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                this.RefreshGridViewState.SaveViewInfo();  // Store Grid View State //

                this.fProgress = new frmProgress(10);  // Show Progress Window //
                this.ParentForm.AddOwnedForm(fProgress);
                fProgress.UpdateCaption("Saving Template, Please Wait...");
                fProgress.Show();
                Application.DoEvents();
                
                int intTemplateID = 0;
                int intTemplateCreatedBy = fChildForm.intTemplateCreatedByID;
                int intTemplateShareType = fChildForm.intTemplateShareType;
                string strTemplateDescription = fChildForm.strTemplateDescription;
                string strTemplateRemarks = fChildForm.strTemplateRemarks;
                string strTemplateShareGroups = fChildForm.strTemplateShareGroups;
                string strMapCentreID = gridLookUpEdit1.EditValue.ToString();
                string strMapScale = gridLookUpEdit2.EditValue.ToString();
                int? intStyleField = null;
                if (glueStyleField.EditValue != null) intStyleField = Convert.ToInt32(glueStyleField.EditValue);
                int intIncludeLastInspection = (IncludeLastInspectionDetailsCheckEdit.Checked ? 1 : 0);

                // Save Layout Header... //
                DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter SaveTemplate = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                SaveTemplate.ChangeConnectionString(strConnectionString);
                try
                {
                    intTemplateID = Convert.ToInt32(SaveTemplate.sp00107_export_to_GBM_save_template_header_and_access("insert", i_intLoadedTemplateID, intTemplateCreatedBy, strTemplateDescription, strTemplateRemarks, i_str_selected_locality_ids, strMapCentreID, strMapScale, intTemplateShareType, strTemplateShareGroups, intStyleField, i_str_selected_map_file_ids, intIncludeLastInspection));
                }
                catch
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save template [" + strTemplateDescription + "] - an error occurred while saving the template header!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                i_intLoadedTemplateID = intTemplateID;
                i_intLoadedTemplateCreatedByID = intTemplateCreatedBy;
                this.Text = "Amenity Trees - GBM Mobile Data Interchange [" + strTemplateDescription + "]";

                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                Load_Templates_List((GridView)gridControl1.MainView, false);  // Changes to contents so reload data //
                
                SaveTemplateAssociatedInfo(intTemplateID);
            }
        }

        private void SaveTemplateAssociatedInfo(int intTemplateID)
        {
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
       
            // Save Layout Items...// 
            DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter SaveTemplateItem = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            SaveTemplateItem.ChangeConnectionString(strConnectionString);

            GridView view = (GridView)gridControl3.MainView;
            view.BeginSort();
            //if (view.ActiveFilter.Criteria == null) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            
            int intColumnID = 0;
            int intSelected = 0;
            int intEditable = 0;
            int intSearchable = 0;
            int intUseLastRecord = 0;
            string ColumnLabel = "";
            string strGroupName = "";
            int intColumnOrder = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                intColumnID = Convert.ToInt32(view.GetRowCellValue(i, "ColumnID"));
                intSelected = Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection"));
                intEditable = Convert.ToInt32(view.GetRowCellValue(i, "Editable"));
                intSearchable = Convert.ToInt32(view.GetRowCellValue(i, "Searchable"));
                intUseLastRecord = Convert.ToInt32(view.GetRowCellValue(i, "UseLastRecord"));
                ColumnLabel = Convert.ToString(view.GetRowCellValue(i, "ColumnLabel"));
                strGroupName = Convert.ToString(view.GetRowCellValue(i, "GroupName"));
                view.SetRowCellValue(i, "ColumnOrder", i);  // Ensure Sort Order is Correct with no decimal places //
                intColumnOrder = i;
                try
                {
                    SaveTemplateItem.sp00108_export_to_GBM_save_template_item("insert", 0, intTemplateID, intColumnID, intSelected, ColumnLabel, strGroupName, intEditable, intSearchable, intUseLastRecord, intColumnOrder);
                }
                catch
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save template - an error occurred while saving the columns for export!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            view.EndSort();


            // Save Layout Styles...// 
            DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter SaveTemplateStyle = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            SaveTemplateStyle.ChangeConnectionString(strConnectionString);

            view = (GridView)gridControl6.MainView;
            if (ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //

            int intPicklistItemID = 0;
            int intPointSymbolID = 0;
            decimal decPointSize = 0;
            int intPointSymbolColour = 0;
            int intPolygonFillColour = 0;
            int intPolygonLineColour = 0;
            int intPolygonLineWidth = 0;
            int intPolygonFillPattern = 0;
            int intPolygonFillPatternColour = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                intPicklistItemID = Convert.ToInt32(view.GetRowCellValue(i, "ItemID"));
                intPointSymbolID = Convert.ToInt32(view.GetRowCellValue(i, "Symbol"));
                decPointSize = Convert.ToDecimal(view.GetRowCellValue(i, "Size"));
                intPointSymbolColour = Convert.ToInt32(view.GetRowCellValue(i, "SymbolColour"));
                intPolygonFillColour = Convert.ToInt32(view.GetRowCellValue(i, "PolygonFillColour"));
                intPolygonLineColour = Convert.ToInt32(view.GetRowCellValue(i, "PolygonLineColour"));
                intPolygonLineWidth = Convert.ToInt32(view.GetRowCellValue(i, "PolygonLineWidth"));
                intPolygonFillPattern = Convert.ToInt32(view.GetRowCellValue(i, "PolygonFillPattern"));
                intPolygonFillPatternColour = Convert.ToInt32(view.GetRowCellValue(i, "PolygonFillPatternColour"));
                try
                {
                    SaveTemplateStyle.sp00132_export_to_GBM_save_template_style("insert", 0, intTemplateID, intPicklistItemID, intPointSymbolID, decPointSize, intPointSymbolColour, intPolygonFillColour, intPolygonLineColour, intPolygonFillPattern, intPolygonFillPatternColour, intPolygonLineWidth);
                }
                catch
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save template - an error occurred while saving the styles for export!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Save Picklist Selected Items...// 
            DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter SaveTemplatePicklistItem = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            SaveTemplatePicklistItem.ChangeConnectionString(strConnectionString);

            view = (GridView)gridControl5.MainView;
            //if (view.ActiveFilter.Criteria != null) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            
            int intHeaderID = 0;
            int intItemID = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    intHeaderID = Convert.ToInt32(view.GetRowCellValue(i, "HeaderID"));
                    intItemID = Convert.ToInt32(view.GetRowCellValue(i, "ItemID"));
                    try
                    {
                        SaveTemplatePicklistItem.sp00109_export_to_GBM_save_template_selected_picklist_item(intTemplateID, intHeaderID, intItemID);
                    }
                    catch
                    {
                        if (fProgress != null)
                        {
                            fProgress.Close(); // Close Progress Window //
                            fProgress = null;
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save template - an error occurred while saving the selected picklist items!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //


            // Save Groups...// 
            DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter SaveGroup = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            SaveGroup.ChangeConnectionString(strConnectionString);

            view = (GridView)gridControl2.MainView;
            //if (view.ActiveFilter.Criteria != null) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            
            string strGroup = "";
            int intGroupOrder = 0;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                strGroup = Convert.ToString(view.GetRowCellValue(i, "GroupName"));
                intGroupOrder = Convert.ToInt32(view.GetRowCellValue(i, "GroupOrder"));
                try
                {
                    SaveGroup.sp00110_export_to_GBM_save_template_group(intTemplateID, strGroup, intGroupOrder);
                }
                catch
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save template - an error occurred while saving the groups!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;

                }
            }
            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();  // close Progress Window //
                fProgress = null;
            }
            if (this.GlobalSettings.ShowConfirmations == 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Template Saved.", "Save Template As", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion


        #region PopUpContainer1 Locality\District Filter

        private void popupContainerEdit1_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_locality_ids = "";    // Reset any prior values first //
            i_str_selected_locality_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_locality_ids = "";
                return "All Localities [No Filter]";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_locality_ids = "";
                return "All Localities [No Filter]";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_locality_ids += Convert.ToString(view.GetRowCellValue(i, "LocalityID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_locality_names = Convert.ToString(view.GetRowCellValue(i, "LocalityName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_locality_names += ", " + Convert.ToString(view.GetRowCellValue(i, "LocalityName"));
                        }
                        intCount++;
                    }
                }
            }
            Load_Centre_Map_Locality_List();
            Load_Map_File_List();
            return i_str_selected_locality_names;
        }

        private void btnLocalityFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        #endregion


        #region PopUpContainer2 FilterItems Filter

        private void btnFilterFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {         
            e.Value = PopupContainerEdit2_Get_Selected();  // Get value //
        }

        private string PopupContainerEdit2_Get_Selected()
        {           
            string strSelectedItems = "";    // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                strSelectedItems = "0/0";

            }
            else if (selection3.SelectedCount <= 0)
            {
                strSelectedItems = "0/" + view.DataRowCount.ToString();
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection"))) intCount++;
                }
                strSelectedItems = intCount.ToString() + '/' + view.DataRowCount.ToString();
            }
            return strSelectedItems;
        }

        #endregion


        #region PopupContainer3 MapFiles Filter
        
        private void popupContainerEdit3_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit3_Get_Selected();
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_map_file_ids = "";    // Reset any prior values first //
            i_str_selected_map_file_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl8.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_map_file_ids = "";
                return "No Background Files Selected";

            }
            else if (selection4.SelectedCount <= 0)
            {
                i_str_selected_map_file_ids = "";
                return "No Background Files Selected";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_map_file_ids += Convert.ToString(view.GetRowCellValue(i, "MapLinkID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_map_file_names = Convert.ToString(view.GetRowCellValue(i, "FileName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_map_file_names += ", " + Convert.ToString(view.GetRowCellValue(i, "FileName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_map_file_names;
        }

        private void btnMapFilesOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        #endregion


        #region grid2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }
        
        private void gridView2_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent editing of GroupName if value = "None" //
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle ) return;
            try
            {
                if ((view.FocusedColumn.FieldName == "GroupName") && view.GetFocusedRowCellValue("GroupName").ToString() == "None") e.Cancel = true;
            }
            catch (Exception)
            {
            }
        }

        private void gridView2_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedColumn.Name == "colGroupName1")
            {
                string strOldValue = view.ActiveEditor.OldEditValue.ToString();
                string strNewValue;
                if (e.Value == DBNull.Value)
                {
                    strNewValue = null;
                }
                else
                {
                    strNewValue = Convert.ToString(e.Value).Trim();
                }
                if (strNewValue != strOldValue)
                {
                    // Check value not = "None" as this value ius reserved for the None Group //
                    DataRow row = view.GetDataRow(view.FocusedRowHandle);
                    string strDescription;
                    if (e.Value == DBNull.Value) strDescription = null;
                    else strDescription = Convert.ToString(e.Value).Trim();
                    if (strDescription == "" || strDescription == null)
                    {
                        row.SetColumnError("GroupName", view.Columns["GroupName"].Caption + " requires a value.");
                        e.Valid = false;
                        return;
                    }
                    else if (strDescription == "None")
                    {
                        row.SetColumnError("GroupName", view.Columns["GroupName"].Caption + " cannot be named as None.");
                        e.Valid = false;
                        return;
                    }
                    else    
                    {
                        row.SetColumnError("GroupName", "");  // Clear Error Text //
                    }

                    view = (GridView)gridControl3.MainView;
                    //if (view.ActiveFilter.Criteria != null) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                    if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                    view.BeginUpdate();
                    view.BeginSort();
                    view.BeginDataUpdate();

                    GridColumn column = view.Columns["GroupName"];
                    int intRowHandle = 0;
                    int intStartRow = 0;
                    while (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        intRowHandle = view.LocateByValue(intStartRow, column, strOldValue);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intRowHandle, column, strNewValue);
                            intStartRow++;
                        }
                    }
                    view.EndDataUpdate();
                    view.EndSort();
                    view.EndUpdate();
                }
            }
            else if (view.FocusedColumn.Name == "colGroupOrder")
            {
                string strOldValue = view.ActiveEditor.OldEditValue.ToString();
                string strNewValue;
                if (e.Value == DBNull.Value)
                {
                    strNewValue = null;
                }
                else
                {
                    strNewValue = Convert.ToString(e.Value).Trim();
                }
                if (strNewValue != strOldValue)
                {
                    // Check value >= 0 //
                    DataRow row = view.GetDataRow(view.FocusedRowHandle);
                    int  intOrder;
                    if (e.Value == DBNull.Value) intOrder = -1;
                    else intOrder = Convert.ToInt32(e.Value);
                    if (intOrder < 0)
                    {
                        row.SetColumnError("GroupOrder", view.Columns["GroupOrder"].Caption + " requires a value >= 0.");
                        e.Valid = false;
                        return;
                    }
                    else
                    {
                        row.SetColumnError("GroupOrder", "");  // Clear Error Text //
                    }
                    
                    GridColumn column = view.Columns["GroupName"];
                    string strGroupName = Convert.ToString(view.GetFocusedRowCellValue(column));

                    view = (GridView)gridControl3.MainView;
                    //if (view.ActiveFilter.Criteria != null) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                    if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                    view.BeginUpdate();
                    view.BeginSort();
                    view.BeginDataUpdate();

                    GridColumn column1 = view.Columns["GroupOrder"];
                    GridColumn column2 = view.Columns["GroupName"];
                    for (int i = 0; i < view.DataRowCount; i++)
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(i, column1)) == Convert.ToInt32(strOldValue) && Convert.ToString(view.GetRowCellValue(i, column2)) == strGroupName)
                        {
                            view.SetRowCellValue(i, column1, Convert.ToInt32(strNewValue));
                        }
                    }
                    view.EndDataUpdate();
                    view.EndSort();
                    view.EndUpdate();
                }
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Append:
                    e.Handled = true;
                    AddRow();
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.CancelEdit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Edit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.EndEdit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.NextPage:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.PrevPage:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    e.Handled = true;
                    DeleteRow();
                    break;
                default:
                    break;
            }
        }            

        private void AddRow()
        {
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            view.BeginSort();
            view.BeginDataUpdate();

            DataRow dr = this.dataSet_AT_DataTransfer.sp00099_export_to_GBM_default_group_list.NewRow();
            dr["GroupName"] = "";
            dr["GroupOrder"] = 0;
            this.dataSet_AT_DataTransfer.sp00099_export_to_GBM_default_group_list.Rows.Add(dr);
            dr.SetColumnError("GroupName", view.Columns["GroupName"].Caption + " requires a value.");

            view.EndDataUpdate();
            view.EndSort();
            view.EndUpdate();
            view.RefreshData();
        }

        private void DeleteRow()
        {
            GridView view = (GridView)gridControl2.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the group to be deleted before proceeding then try again.", "Delete Group", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (view.GetRowCellValue(view.FocusedRowHandle, "GroupName").ToString() == "None")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Group selected [None] cannot be deleted - The group list muust always contain this item.", "Delete Group", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = "You are about to delete Group [" + view.GetRowCellValue(view.FocusedRowHandle, "GroupName").ToString() + "]!\n\nAre you sure you wish to proceed?\n\nIf you proceed, any export layout items linked to it will be moved to the None Group.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete Group", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                string strOldGroupName = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "GroupName"));
                int intOldGroupOrder = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "GroupOrder"));

                view.BeginUpdate();
                int intDataSetRow = view.GetDataSourceRowIndex(view.FocusedRowHandle);
                dataSet_AT_DataTransfer.sp00099_export_to_GBM_default_group_list.Rows[intDataSetRow].Delete();  // Delete record from underlying dataset (updates grid also) //
                dataSet_AT_DataTransfer.sp00099_export_to_GBM_default_group_list.AcceptChanges();
                //view.DeleteRow(view.FocusedRowHandle);  // Delete record from grid //
                view.EndUpdate();

                //Now move any linked records //
                GridColumn column = view.Columns["GroupName"];
                int intRowHandle = view.LocateByValue(0, column, "None");
                if (intRowHandle != GridControl.InvalidRowHandle)
                {
                    string strNewGroupName = Convert.ToString(view.GetRowCellValue(intRowHandle, "GroupName"));
                    int intNewGroupOrder = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "GroupOrder"));

                    view = (GridView)gridControl3.MainView;
                    view.BeginUpdate();
                    view.BeginSort();
                    view.BeginDataUpdate();
                    for (int i = 0; i < view.DataRowCount; i++)
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(i, "GroupOrder")) == intOldGroupOrder && Convert.ToString(view.GetRowCellValue(i, "GroupName")) == strOldGroupName)
                        {
                            view.SetRowCellValue(i, "GroupOrder", intNewGroupOrder);
                            view.SetRowCellValue(i, "GroupName", strNewGroupName);
                        }
                    }
                    view.EndDataUpdate();
                    view.EndSort();
                    view.EndUpdate();
                    if (this.GlobalSettings.ShowConfirmations == 1)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Group Deleted.\n\nAny linked items were moved to the [None] group.", "Delete Group", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        #endregion


        #region GridControl1


        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiLoadTemplate.Enabled = true;
                if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
                {
                    bbiDeleteTemplate.Enabled = false;
                    bbiEditTemplate.Enabled = false;
                    bbiLoadTemplate.Enabled = false;
                }
                else if (Convert.ToInt32(view.GetFocusedRowCellValue("CreatedByID")) != GlobalSettings.UserID && GlobalSettings.PersonType != 0)
                {
                    bbiDeleteTemplate.Enabled = false;
                    bbiEditTemplate.Enabled = false;
                }
                else
                {
                    bbiDeleteTemplate.Enabled = true;
                    bbiEditTemplate.Enabled = true;
                }
                pmGrid1Menu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }

            // ***** Stop Group Row Being Selected ***** //
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void bbiLoadTemplate_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the export template to be loaded before proceeding then try again.", "Load Export Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.fProgress = new frmProgress(10);  // Show Progress Window //
            this.ParentForm.AddOwnedForm(fProgress);
            fProgress.UpdateCaption("Loading Template, Please Wait...");
            fProgress.Show();
            Application.DoEvents();

            int intTemplateID = Convert.ToInt32(view.GetFocusedRowCellValue("TemplateID"));

            SqlDataAdapter sdaDataAdpter = new SqlDataAdapter();
            DataSet dsDataSet = new DataSet("NewDataSet");

            // ***** Load Template Header Info First ***** //
            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp00119_export_to_GBM_load_template_header", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@TemplateID", intTemplateID));
            sdaDataAdpter = new SqlDataAdapter(cmd);
            sdaDataAdpter.Fill(dsDataSet, "Table");
            conn.Close();
            conn.Dispose();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            string strTemplateDescription = "";
            string strLocalityIDs = "";
            string strMapScale = "";
            int intCreatedByID = 0;
            int intMapCentreID = 0;
            int? intStyleField = 0;
            string strMapLinkIDs = "";
            int intIncludeLastInspection = 0;
            foreach (DataRow dr in dsDataSet.Tables[0].Rows)
            {
                strTemplateDescription = Convert.ToString(dr["Description"]);
                strLocalityIDs = Convert.ToString(dr["LocalityIDs"]);
                strMapScale = Convert.ToString(dr["MapScale"]);
                intCreatedByID = Convert.ToInt32(dr["CreatedByID"]);
                intMapCentreID = Convert.ToInt32(dr["MapCentreID"]);
                strMapLinkIDs = Convert.ToString(dr["MapLinkIDs"]);
                if (dr["StyleField"] == DBNull.Value)
                {
                    intStyleField = null;
                }
                else
                {
                    intStyleField = Convert.ToInt32(dr["StyleField"]);
                }
                intIncludeLastInspection = Convert.ToInt32(dr["IncludeLastInspection"]);
            }

            // ***** Reselect any stored localities ***** //
            selection2.ClearSelection();  // Mark all localities as de-selected so the following foreach can select the saved items //
            Array arrayLocalities = strLocalityIDs.Split(',');  // Single quotes because char expected for delimeter //
            GridView viewLocalities = (GridView)gridControl4.MainView;
            if (!ReferenceEquals(viewLocalities.ActiveFilter.Criteria, null)) viewLocalities.ActiveFilter.Clear();  // Ensure there are no fiters switched on //

            viewLocalities.BeginUpdate();
            int intFoundRow = 0;
            foreach (string strElement in arrayLocalities)
            {
                if (strElement == "") break;
                intFoundRow = viewLocalities.LocateByValue(0, viewLocalities.Columns["LocalityID"], Convert.ToInt32(strElement));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    viewLocalities.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                    viewLocalities.MakeRowVisible(intFoundRow, false);
                }
            }
            viewLocalities.EndUpdate();
            i_str_selected_locality_ids = strLocalityIDs;
            i_str_selected_locality_names = PopupContainerEdit1_Get_Selected();  // Loads District Map Links //
            popupContainerEdit1.EditValue = i_str_selected_locality_names;
            dsDataSet = null;
            sdaDataAdpter = null;

            // ***** Reselect any stored District Map Links ***** //
            selection4.ClearSelection();  // Mark all District Map Links as de-selected so the following foreach can select the saved items //
            char[] delimiters = new char[] { ',' };
            Array arrayDistrictMapLinks = strMapLinkIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            GridView viewDistrictMapLinks = (GridView)gridControl8.MainView;
            if (!ReferenceEquals(viewDistrictMapLinks.ActiveFilter.Criteria, null)) viewDistrictMapLinks.ActiveFilter.Clear();  // Ensure there are no fiters switched on //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            viewDistrictMapLinks.BeginUpdate();
            foreach (string strElement in arrayDistrictMapLinks)
            {
                if (strElement == "") break;
                intFoundRow = viewDistrictMapLinks.LocateByValue(0, viewDistrictMapLinks.Columns["MapLinkID"], Convert.ToInt32(strElement));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    viewDistrictMapLinks.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                    viewDistrictMapLinks.MakeRowVisible(intFoundRow, false);
                }
            }
            viewDistrictMapLinks.EndUpdate();
            i_str_selected_map_file_ids = strMapLinkIDs;
            i_str_selected_map_file_names = PopupContainerEdit3_Get_Selected();
            popupContainerEdit3.EditValue = i_str_selected_map_file_names;

            gridLookUpEdit2.EditValue = strMapScale;
            gridLookUpEdit1.EditValue = intMapCentreID;
            i_intLoadedTemplateID = intTemplateID;
            i_intLoadedTemplateCreatedByID = intCreatedByID;
            this.Text = "Amenity Trees - GBM Mobile Data Interchange [" + strTemplateDescription + "]";

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //


            // ***** Load Groups ***** //
            GridView viewGroups = (GridView)gridControl2.MainView;
            //if (viewGroups.ActiveFilter.Criteria != null) viewGroups.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!ReferenceEquals(viewGroups.ActiveFilter.Criteria, null)) viewGroups.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            
            view.BeginUpdate();
            this.sp00099_export_to_GBM_default_group_listTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00099_export_to_GBM_default_group_list, intTemplateID);
            view.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //


            // ***** Load Picklist Items ***** //
            conn = new SqlConnection(strConnectionString);
            cmd = null;
            cmd = new SqlCommand("sp00118_export_to_GBM_load_template_picklist_items", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@TemplateID", intTemplateID));
            dsDataSet = new DataSet("NewDataSet");
            sdaDataAdpter = new SqlDataAdapter(cmd);
            sdaDataAdpter.Fill(dsDataSet, "Table");
            conn.Close();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            int intHeaderID = 0;
            int intItemID = 0;
            GridView viewPickListItems = (GridView)gridControl5.MainView;
            //if (viewPickListItems.ActiveFilter.Criteria != null) viewPickListItems.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!ReferenceEquals(viewPickListItems.ActiveFilter.Criteria, null)) viewPickListItems.ActiveFilter.Clear();  // Ensure there are no fiters switched on //

            viewPickListItems.BeginUpdate();
            viewPickListItems.BeginDataUpdate();
          
            GridColumn[] cols = new GridColumn[] { colHeaderID, colItemID };  // Array of columns to search for a match on //
            object[] values; // Array of objects [values] to look for //
            extGridViewFunction = new ExtendedGridViewFunctions();

            selection3.ClearSelection();  // Mark all items as de-selected so the following foreach can select the saved picklist items //

            foreach (DataRow dr in dsDataSet.Tables[0].Rows)
            {
                intHeaderID = Convert.ToInt32(dr["HeaderID"]);
                intItemID = Convert.ToInt32(dr["ItemID"]);

                values = new object[] { intHeaderID, intItemID };
                intFoundRow = extGridViewFunction.LocateRowByMultipleValues(viewPickListItems, cols, values, 0);  // Function declared in BaseObjects... Classes... ExtendedGridViewFunctions //

                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    viewPickListItems.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                    viewPickListItems.MakeRowVisible(intFoundRow, false);
                }
            }
            viewPickListItems.EndDataUpdate();
            viewPickListItems.EndUpdate();
            dsDataSet = null;
            sdaDataAdpter = null;
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //


            // ***** Load Template Items ***** //
            conn = new SqlConnection(strConnectionString);
            cmd = null;
            cmd = new SqlCommand("sp00120_export_to_GBM_load_template_items", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@TemplateID", intTemplateID));
            dsDataSet = new DataSet("NewDataSet");
            sdaDataAdpter = new SqlDataAdapter(cmd);
            sdaDataAdpter.Fill(dsDataSet, "Table");
            conn.Close();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            int intColumnID = 0;
            int intSelected = 0;
            int intEditable = 0;
            int intSearchable = 0;
            int intUseLastRecord = 0;
            string strGroupName = "";
            string strScreenLabel = "";
            int intColumnOrder = 0;
            GridView viewItems = (GridView)gridControl3.MainView;
            if (!ReferenceEquals(viewItems.ActiveFilter.Criteria, null)) viewItems.ActiveFilter.Clear();  // Ensure there are no fiters switched on //

            viewItems.BeginUpdate();

            foreach (DataRow dr in dsDataSet.Tables[0].Rows)
            {
                intColumnID = Convert.ToInt32(dr["ColumnID"]);
                intSelected = Convert.ToInt32(dr["Selected"]);
                intEditable = Convert.ToInt32(dr["Editable"]);
                intSearchable = Convert.ToInt32(dr["Searchable"]);
                intUseLastRecord = Convert.ToInt32(dr["UseLastRecord"]);
                strGroupName = Convert.ToString(dr["GroupName"]);
                strScreenLabel = Convert.ToString(dr["ScreenLabel"]);
                intColumnOrder = Convert.ToInt32(dr["ColumnOrder"]);
                intFoundRow = viewItems.LocateByValue(0, viewItems.Columns["ColumnID"], intColumnID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    viewItems.SetRowCellValue(intFoundRow, "CheckMarkSelection", intSelected);
                    viewItems.SetRowCellValue(intFoundRow, "GroupName", strGroupName);
                    viewItems.SetRowCellValue(intFoundRow, "ColumnLabel", strScreenLabel);
                    viewItems.SetRowCellValue(intFoundRow, "PicklistSummery", "");  // clear Picklist summary info so that following call to Set_Picklist_Item_Text() can set it //
                    viewItems.SetRowCellValue(intFoundRow, "Editable", intEditable);
                    viewItems.SetRowCellValue(intFoundRow, "Searchable", intSearchable);
                    viewItems.SetRowCellValue(intFoundRow, "UseLastRecord", intUseLastRecord);
                    if (intColumnOrder > 0)
                    {
                        viewItems.SetRowCellValue(intFoundRow, "ColumnOrder", intColumnOrder);
                    }
                }
            }

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            Set_Required_Columns();

            Set_Picklist_Item_Text();  // Set Picklist text [x / x] for each picklist //
            viewItems.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            
            // Load Styles Field List //
            GridView viewStyleItems = (GridView)gridControl6.MainView;
            viewStyleItems.BeginUpdate();

            Populate_glueStyles();
            glueStyleField.EditValue = intStyleField;
            
            if (intStyleField == null || intStyleField == 0)
            {
                this.dataSet_AT_DataTransfer.sp00131_export_to_GBM_style_fields.Rows.Clear();
            }
            else
            {
                // ***** Load Style Items ***** //
                conn = new SqlConnection(strConnectionString);
                cmd = null;
                cmd = new SqlCommand("sp00133_export_to_GBM_load_template_style", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TemplateID", intTemplateID));
                dsDataSet = new DataSet("NewDataSet");
                sdaDataAdpter = new SqlDataAdapter(cmd);
                sdaDataAdpter.Fill(dsDataSet, "Table");
                conn.Close();

                int intPicklistItemID = 0;
                int intPointSymbolID = 0;
                decimal decPointSize = 0;
                int intPointColour = 0;
                int intPolygonFillColour = 0;
                int intPolygonLineColour = 0;
                int intPolygonFillPattern = 0;
                int intPolygonFillPatternColour = 0;
                int intPolygonLineWidth = 0;

                if (!ReferenceEquals(viewStyleItems.ActiveFilter.Criteria, null)) viewStyleItems.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                foreach (DataRow dr in dsDataSet.Tables[0].Rows)
                {
                    intPicklistItemID = Convert.ToInt32(dr["PicklistItemID"]);
                    intPointSymbolID = Convert.ToInt32(dr["PointSymbolID"]);
                    decPointSize = Convert.ToDecimal(dr["PointSize"]);
                    intPointColour = Convert.ToInt32(dr["PointColour"]);
                    intPolygonFillColour = Convert.ToInt32(dr["PolygonFillColour"]);
                    intPolygonLineColour = Convert.ToInt32(dr["PolygonLineColour"]);
                    intPolygonFillPattern = Convert.ToInt32(dr["PolygonFillPattern"]);
                    intPolygonFillPatternColour = Convert.ToInt32(dr["PolygonFillPatternColour"]);
                    intPolygonLineWidth = Convert.ToInt32(dr["PolygonLineWidth"]);
                    intFoundRow = viewStyleItems.LocateByValue(0, viewStyleItems.Columns["ItemID"], intPicklistItemID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        viewStyleItems.SetRowCellValue(intFoundRow, "Symbol", intPointSymbolID);
                        viewStyleItems.SetRowCellValue(intFoundRow, "Size", decPointSize);
                        viewStyleItems.SetRowCellValue(intFoundRow, "SymbolColour", intPointColour);
                        viewStyleItems.SetRowCellValue(intFoundRow, "PolygonFillColour", intPolygonFillColour);
                        viewStyleItems.SetRowCellValue(intFoundRow, "PolygonLineColour", intPolygonLineColour);
                        viewStyleItems.SetRowCellValue(intFoundRow, "PolygonFillPattern", intPolygonFillPattern);
                        viewStyleItems.SetRowCellValue(intFoundRow, "PolygonFillPatternColour", intPolygonFillPatternColour);
                        viewStyleItems.SetRowCellValue(intFoundRow, "PolygonLineWidth", intPolygonLineWidth);
                    }
                }
            }
            viewStyleItems.EndUpdate();

            // Set IncludeLastInspectionDetails Checked Status // 
            IncludeLastInspectionDetailsCheckEdit.Checked = (intIncludeLastInspection == 1 ? true : false);

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show full progress //
                fProgress.Close();
                fProgress = null;
            }
            if (this.GlobalSettings.ShowConfirmations == 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Template Loaded.", "Load Template", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bbiEditTemplate_ItemClick(object sender, ItemClickEventArgs e)
        {
            EditTemplate();
        }

        private void bbiDeleteTemplate_ItemClick(object sender, ItemClickEventArgs e)
        {
            DeleteTemplate();
        }

        private void EditTemplate()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the export template to be edited before proceeding then try again.", "Edit Export Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CreatedByID")) != this.GlobalSettings.UserID && this.GlobalSettings.PersonType != 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You can only edit export templates which you created!\n\nThe currently selected export template was created by a different user - editing cancelled.", "Edit Export Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strRecordIDs = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "TemplateID")) + ',';

            this.RefreshGridViewState.SaveViewInfo();  // Store Grid View State //

            frm_AT_Data_Transfer_GBM_Mobile_Template_Edit fChildForm = new frm_AT_Data_Transfer_GBM_Mobile_Template_Edit();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = strRecordIDs;
            fChildForm.strFormMode = "edit";
            fChildForm.strCaller = "frm_AT_Data_Transfer_GBM_Mobile";
            fChildForm.intRecordCount = 1;
            fChildForm.boolAllowEdit = true;
            fChildForm.FormPermissions = this.FormPermissions;
            if (fChildForm.ShowDialog(this) == DialogResult.OK)
            {
                Load_Templates_List(view, false);  // Changes made on child screen so reload data //
            }
        }

        private void DeleteTemplate()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the export template to be deleted before proceeding then try again.", "Delete Export Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CreatedByID")) != this.GlobalSettings.UserID && this.GlobalSettings.PersonType != 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You can only delete export templates which you created!\n\nThe currently selected export template was created by a different user - deletion cancelled.", "Delete Export Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = "You are about to delete export template [" + view.GetRowCellValue(view.FocusedRowHandle, "TemplateDescription").ToString() + "]!\n\nAre you sure you wish to proceed?\n\nIf you proceed this template will no longer be available.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete Export Template", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                int intTemplateID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "TemplateID"));
                string strRecordIDs = intTemplateID.ToString() + ",";

                if (intTemplateID == i_intLoadedTemplateID)  // Currently loaded template is about to be deleted, so clear form setting so when user right-clicks to save template, the SaveAs menu Item is disabled */
                {
                    i_intLoadedTemplateID = 0;
                    i_intLoadedTemplateCreatedByID = 0;
                    this.Text = "Amenity Trees - GBM Mobile Data Interchange";
                }

                DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter DeleteTemplate = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                DeleteTemplate.ChangeConnectionString(strConnectionString);
                gridControl1.BeginUpdate();
                DeleteTemplate.sp00116_export_to_GBM_template_delete(strRecordIDs);  // Delete record from back end //
                view.DeleteRow(view.FocusedRowHandle);  // Delete record from grid //
                gridControl1.EndUpdate();
                if (this.GlobalSettings.ShowConfirmations == 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Export Template Deleted.", "Delete Export Template", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        #endregion


        #region Styles and GridControl6
        private void glueStyleField_QueryPopUp(object sender, CancelEventArgs e)
        {
            Populate_glueStyles();
        }

        private void Populate_glueStyles()
        {
            GridView view = (GridView)gridControl3.MainView;
            string strSelectedFilter = "";
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1 && Convert.ToInt32(view.GetRowCellValue(i, "PickList")) != 0)
                {
                    strSelectedFilter += " ColumnID = '" + Convert.ToString(view.GetRowCellValue(i, "ColumnID")) + "' OR ";
                }
            }
            if (strSelectedFilter == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view Styles, no picklist fields have been selected for export.\n\nSelect one or more picklist fields for export before proceeding then try again.", "Export - Set Styles", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strSelectedFilter = strSelectedFilter.Remove(strSelectedFilter.Length - 4, 4);  // Cut last " OR " from end //


            DataView dvFiltered = new DataView(this.dataSet_AT_DataTransfer.sp00098_export_to_GBM_field_list, strSelectedFilter, "", DataViewRowState.CurrentRows);
            glueStyleField.Properties.DataSource = dvFiltered;
        }

        private void glueStyleField_EditValueChanged(object sender, EventArgs e)
        {
            LoadStyleGrid(Convert.ToInt32(glueStyleField.EditValue));
        }

        private void LoadStyleGrid(int intColumnID)
        {
            GridView viewLookupParent = (GridView)gridControl3.MainView;
            int intPickListID = 0;
            int intFoundRow = viewLookupParent.LocateByValue(0, viewLookupParent.Columns["ColumnID"], intColumnID);
            if (intFoundRow < 0) return;
            intPickListID = Convert.ToInt32(viewLookupParent.GetRowCellValue(intFoundRow, "PickList"));
            GridView view = (GridView)gridControl6.MainView;
            view.BeginUpdate();
            view.BeginDataUpdate();
            this.sp00131_export_to_GBM_style_fieldsTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00131_export_to_GBM_style_fields, intPickListID);
            view.EndDataUpdate();
            view.EndUpdate();
        }

        private void glueStyleField_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear all styles!\n\nAre you sure you wish to proceed?", "Clear Styles", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    glueStyleField.EditValue = null;
                    GridView view = (GridView)gridControl6.MainView;
                    view.BeginUpdate();
                    view.BeginDataUpdate();
                    this.dataSet_AT_DataTransfer.sp00131_export_to_GBM_style_fields.Rows.Clear();
                    view.EndDataUpdate();
                    view.EndUpdate();
                }
            }
        }


        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (i_intLoadedTemplateID == 0)
                {
                    bbiSaveTemplateAs.Enabled = false;
                }
                else
                {
                    bbiSaveTemplateAs.Enabled = true;
                }

                if (i_intLoadedTemplateCreatedByID != GlobalSettings.UserID && i_intLoadedTemplateID > 0 && GlobalSettings.PersonType != 0)
                {
                    bbiSaveTemplate.Enabled = false;
                }
                else
                {
                    bbiSaveTemplate.Enabled = true;
                }

                int[] intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length > 0)
                {
                    bbiBlockEditStyles.Enabled = true;
                }
                else
                {
                    bbiBlockEditStyles.Enabled = false;
                }
                pmGrid6Menu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void bbiBlockEditStyles_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more styles before proceeding.", "Block Edit Styles", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strRecordIDs = "";
            int intCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ItemID")) + ",";
                intCount++;
            }
            frm_AT_Data_Transfer_GBM_Mobile_Styles frm_styles = new frm_AT_Data_Transfer_GBM_Mobile_Styles();
            frm_styles.intCount = intCount;
            if (frm_styles.ShowDialog() == DialogResult.OK)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (frm_styles.intPointSymbol != null) view.SetRowCellValue(intRowHandle, "Symbol", frm_styles.intPointSymbol);
                    if (frm_styles.decPointSize != null) view.SetRowCellValue(intRowHandle, "Size", frm_styles.decPointSize);
                    if (frm_styles.intPointColour != null) view.SetRowCellValue(intRowHandle, "SymbolColour", frm_styles.intPointColour);
                    if (frm_styles.intPolygonFillColour != null) view.SetRowCellValue(intRowHandle, "PolygonFillColour", frm_styles.intPolygonFillColour);
                    if (frm_styles.intPolygonLineColour != null) view.SetRowCellValue(intRowHandle, "PolygonLineColour", frm_styles.intPolygonLineColour);
                    if (frm_styles.intPolygonFillPattern != null) view.SetRowCellValue(intRowHandle, "PolygonFillPattern", frm_styles.intPolygonFillPattern);
                    if (frm_styles.intPolygonFillPatternColour != null) view.SetRowCellValue(intRowHandle, "PolygonFillPatternColour", frm_styles.intPolygonFillPatternColour);
                    if (frm_styles.intPolygonLineWidth != null) view.SetRowCellValue(intRowHandle, "PolygonLineWidth", frm_styles.intPolygonLineWidth);
                }
                view.EndUpdate();
            }
        }

        #endregion


        #region PopupContainer Work Order Checking

        private void btnWorkOrderCheckingRefresh_Click(object sender, EventArgs e)
        {
            Refresh_WorkOrder_List();
        }

        private void Refresh_WorkOrder_List()
        {
            DateTime dtFromDate = dateEditWorkOrderCheckingFromDate.DateTime;
            DateTime dtToDate = dateEditWorkOrderCheckingToDate.DateTime;
            int intIncludeComplete = (checkEditWorkOrderCheckingIncludeCompleted.Checked ? 1 : 0);

            GridView view = (GridView)gridControl9.MainView;
            view.BeginUpdate();
            this.sp01321_AT_export_to_GBM_available_workordersTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp01321_AT_export_to_GBM_available_workorders, dtFromDate, dtToDate, intIncludeComplete);
            view.EndUpdate();
        }

        private void popupContainerEdit2_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditWorkOrders_Get_Selected();  // Get value //

        }

        private string PopupContainerEditWorkOrders_Get_Selected()
        {
            i_str_selected_workorder_ids = "";    // Reset any prior values first //
            i_str_selected_workorder_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl9.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_workorder_ids = "";
                return "No Work Orders Selected [No Work Order Checking]";

            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_selected_workorder_ids = "";
                return "No Work Orders Selected [No Work Order Checking]";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_workorder_ids += Convert.ToString(view.GetRowCellValue(i, "WorkOrderID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_workorder_names = Convert.ToString(view.GetRowCellValue(i, "PaddedWorkOrderID"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_workorder_names += ", " + Convert.ToString(view.GetRowCellValue(i, "PaddedWorkOrderID"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_workorder_names;
        }

        private void btnWorkOrderCheckingOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent.Parent as PopupContainerControl).OwnerEdit.ClosePopup();  // Extra ".Parent" as button resides inside a Panel control within the Popup //
        }

        #endregion


        private void comboBoxEditExportType_EditValueChanged(object sender, EventArgs e)
        {
            ComboBoxEdit cbe = (ComboBoxEdit)sender;
            Set_Controls_Enabled_Status(cbe);
        }

        private void Set_Controls_Enabled_Status(ComboBoxEdit cbe)
        {
            switch (cbe.EditValue.ToString())
            {
                case "Survey":
                    popupContainerEdit2.Enabled = false;
                    gridControl1.Enabled = true;
                    gridControl2.Enabled = true;
                    gridControl3.Enabled = true;
                    gridControl6.Enabled = true;
                    glueStyleField.Enabled = true;
                    IncludeLastInspectionDetailsCheckEdit.Enabled = true;
                    break;
                case "Work Order Checking":
                    popupContainerEdit2.Enabled = true;
                    gridControl1.Enabled = false;
                    gridControl2.Enabled = false;
                    gridControl3.Enabled = false;
                    gridControl6.Enabled = false;
                    glueStyleField.Enabled = false;
                    IncludeLastInspectionDetailsCheckEdit.Enabled = false;
                    break;
                case "Survey and Work Order Checking":
                    popupContainerEdit2.Enabled = true;
                    gridControl1.Enabled = true;
                    gridControl2.Enabled = true;
                    gridControl3.Enabled = true;
                    gridControl6.Enabled = true;
                    glueStyleField.Enabled = true;
                    IncludeLastInspectionDetailsCheckEdit.Enabled = true;
                    break;
            }
        }


        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            switch (i_int_FocusedGrid)
            {
                case 1:
                    DeleteTemplate();
                    break;
                case 7:  // Delete one or more rows from Import Grid //
                    if (!iBool_AllowDelete) return;
                    int[] intRowHandles;
                    int intCount = 0;
                    GridView view = (GridView)gridControl7.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or import rows to delete by clicking on them then try again.", "Delete Import Rows", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    string strMessage = (intCount == 1 ? "You have 1 import row selected for delete!\n\nProceed?" : "You have " + Convert.ToString(intRowHandles.Length) + " import rows selected for delete!\n\nProceed?");
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        frmProgress fProgress = new frmProgress(20);
                        fProgress.UpdateCaption("Deleting...");
                        fProgress.Show();
                        Application.DoEvents();
                        int intUpdateProgressThreshhold = intCount / 10;
                        int intUpdateProgressTempCount = 0;

                        view.BeginUpdate();
                        view.BeginDataUpdate();
                        view.BeginSort();
                        for (int i = intRowHandles.Length - 1; i >= 0; i--)
                        {
                            view.DeleteRow(i);
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            } 
                        }
                        view.EndSort();
                        view.EndDataUpdate();
                        view.EndUpdate();

                        if (fProgress != null)
                        {
                            fProgress.Close();
                            fProgress = null;
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.\n\nImportant Information: The row(s) just deleted have only been deleted from the screen - they are still present within the original file.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                default:
                    break;
            }
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            EditTemplate();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (comboBoxEditExportType.EditValue.ToString().Contains("Work Order Checking") && popupContainerEdit2.EditValue.ToString() == "No Work Orders Selected [No Work Order Checking]")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You have selected " + comboBoxEditExportType.EditValue.ToString() + " as the Export type but no Work Orders have been selected for export.\n\nSelect one or more Work Orders from the Work Orders drop down list or select a different type of export before proceeding.", "Export", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            bool boolCreateSurvey = false;
            bool boolCreateWorkOrders = false;
            switch (comboBoxEditExportType.EditValue.ToString())
            {
                case "Survey":
                    boolCreateSurvey = true;
                    break;
                case "Work Order Checking":
                    boolCreateWorkOrders = true;
                    break;
                default:  // Survey and Work Order Checking //
                    boolCreateSurvey = true;
                    boolCreateWorkOrders = true;
                    break;
            }

            #region Generate XML File
 
            this.fProgress = new frmProgress(10);  // Show Progress Window //
            this.ParentForm.AddOwnedForm(fProgress);
            fProgress.UpdateCaption("Exporting Data, Please Wait...");
            fProgress.Show();
            Application.DoEvents();

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);

            // Get GBM_Mobile.XML Output Location //
            string strXMLFileLocation = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesExportToGBM_GBM_Mobile_XML_Folder"));
            if (strXMLFileLocation == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - No GbmMobile.xml File Location has been specified in the System Settings table.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                return;
            }
            strXMLFileLocation += @"\GbmMobile.xml";

            XmlTextWriter textWriter = new XmlTextWriter(strXMLFileLocation, null);
            textWriter.WriteStartDocument();  // Open the document //
            textWriter.WriteComment("Generated by WoodPlan 5");  // Write comments //

            textWriter.WriteStartElement("xml");  // Write first element  - Not closed so subsequent items are indented inside it //
            {
                if (boolCreateSurvey)
                {
                    ExportData("Point", textWriter);
                    if (fProgress != null) fProgress.UpdateProgress((boolCreateWorkOrders ? 4 : 10)); // Update Progress Bar //

                    ExportData("Polygon", textWriter);
                    if (fProgress != null) fProgress.UpdateProgress((boolCreateWorkOrders ? 3 : 5)); // Update Progress Bar //

                    ExportData("Polyline", textWriter);
                    if (fProgress != null) fProgress.UpdateProgress((boolCreateWorkOrders ? 3 : 5)); // Update Progress Bar //
                }
                if (boolCreateWorkOrders)
                {
                    ExportDataWorkOrder("Point", textWriter);
                    if (fProgress != null) fProgress.UpdateProgress((boolCreateSurvey ? 4 : 10)); // Update Progress Bar //

                    ExportDataWorkOrder("Polygon", textWriter);
                    if (fProgress != null) fProgress.UpdateProgress((boolCreateSurvey ? 3 : 5)); // Update Progress Bar //

                    ExportDataWorkOrder("Polyline", textWriter);
                    if (fProgress != null) fProgress.UpdateProgress((boolCreateSurvey ? 3 : 5)); // Update Progress Bar //
                }
                AddGazetteerFormToXML(textWriter);
            }
            textWriter.WriteEndElement();  // End of xml //

            textWriter.WriteEndDocument();  // Ends the document //
            textWriter.Close();  // Close writer //
            # endregion

            // Get Centre Points of Centre Locality and pass to SP [these are used when no trees are found and a dummy tree is inserted]//
            double doubleX = 0.00;
            double doubleY = 0.00;
            int intCentreLocality = (gridLookUpEdit1.EditValue == null ? 0 : Convert.ToInt32(gridLookUpEdit1.EditValue));
            if (intCentreLocality != 0)
            {
                GridView childView = (GridView)gridLookUpEdit1.Properties.View;
                int intFoundRow = childView.LocateByValue(0, childView.Columns["LocalityID"], Convert.ToInt32(intCentreLocality));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    doubleX = Convert.ToDouble(childView.GetRowCellValue(intFoundRow, "Xcoordinate"));
                    doubleY = Convert.ToDouble(childView.GetRowCellValue(intFoundRow, "Ycoordinate"));
                }
            }
            // Get MID / MIF Output Location //
            string strMidMifFileLocation = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesExportToGBM_Mid_Mif_Folder"));
            if (strMidMifFileLocation == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - No MID / MIF File Location has been specified in the System Settings table.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                return;
            }
            // Get TAB file Output Location //
            string strTabFileLocation = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesExportToGBM_Tab_Folder"));  // Get TAB File Output Location //
            if (strTabFileLocation == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - No TAB File Location has been specified in the System Settings table.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                return;
            }

            double doubleTotalX = (double)0.00;  // The following 3 vars are used to derive averages for the centre point on the CreateGeosetFile //
            double doubleTotalY = (double)0.00;
            int intValuesCounted = 0;
            int intReturnValue = 0;
            
            if (boolCreateSurvey)
            {
                #region Get Tree Data

                SqlDataAdapter sdaDataAdpter = new SqlDataAdapter();
                DataSet dsDataSet = new DataSet("NewDataSet");
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp00134_export_to_GBM_export_data", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@LocalityIDs", i_str_selected_locality_ids));
                cmd.Parameters.Add(new SqlParameter("@XCoordinate", doubleX));
                cmd.Parameters.Add(new SqlParameter("@YCoordinate", doubleY));
                sdaDataAdpter = new SqlDataAdapter(cmd);

                // Ensure columns are of correct datatype as they have Nulls in the StoredProc so VS will map them to Int32 //
                sdaDataAdpter.FillSchema(dsDataSet, SchemaType.Mapped);
                dsDataSet.Tables[0].Columns["dtLastInspectionDate"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtNextInspectionDate"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtPlantDate"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtSurveyDate"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtInspectDate"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDueDate_1"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDoneDate_1"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDueDate_2"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDoneDate_2"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDueDate_3"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDoneDate_3"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDueDate_4"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDoneDate_4"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDueDate_5"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDoneDate_5"].DataType = System.Type.GetType("System.DateTime");

                dsDataSet.Tables[0].Columns["TreePhotograph"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["InspectionPhotograph"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strInspectRef"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser1_Inspection"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser2_Inspection"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser3_Inspection"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strRemarks_Inspection"].DataType = System.Type.GetType("System.String");
                
                dsDataSet.Tables[0].Columns["Last_strInspectRef"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["Last_dtInspectDate"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["Last_strUser1_Inspection"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["Last_strUser2_Inspection"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["Last_strUser3_Inspection"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["Last_strRemarks_Inspection"].DataType = System.Type.GetType("System.String");

                dsDataSet.Tables[0].Columns["strJobNumber_1"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser1_Action_1"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser2_Action_1"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser3_Action_1"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strRemarks_Action_1"].DataType = System.Type.GetType("System.String");

                dsDataSet.Tables[0].Columns["strJobNumber_2"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser1_Action_2"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser2_Action_2"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser3_Action_2"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strRemarks_Action_2"].DataType = System.Type.GetType("System.String");

                dsDataSet.Tables[0].Columns["strJobNumber_3"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser1_Action_3"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser2_Action_3"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser3_Action_3"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strRemarks_Action_3"].DataType = System.Type.GetType("System.String");

                dsDataSet.Tables[0].Columns["strJobNumber_4"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser1_Action_4"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser2_Action_4"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser3_Action_4"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strRemarks_Action_4"].DataType = System.Type.GetType("System.String");

                dsDataSet.Tables[0].Columns["strJobNumber_5"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser1_Action_5"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser2_Action_5"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser3_Action_5"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strRemarks_Action_5"].DataType = System.Type.GetType("System.String");

                sdaDataAdpter.Fill(dsDataSet, "Table");
                conn.Close();
                conn.Dispose();

                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

                // Strip out unwanted columns first //
                ArrayList alExportColumns = new ArrayList();
                GridView view = (GridView)gridControl3.MainView;
                int intCounter = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    dsDataSet.Tables[0].Columns[view.GetRowCellValue(i, "InternalColumnName").ToString()].SetOrdinal(intCounter);  // Alter dataset column order to match order of exported fields //
                    
                    // Include Last Inspection Values if Required //
                    if (IncludeLastInspectionDetailsCheckEdit.Checked)
                    {
                        if (view.GetRowCellValue(i, "TableName").ToString().ToLower() == "inspection" && view.GetRowCellValue(i, "InternalColumnName").ToString().ToLower() != "inspectionphotograph")
                        {
                            intCounter++;
                            dsDataSet.Tables[0].Columns["Last_" + view.GetRowCellValue(i, "InternalColumnName").ToString()].SetOrdinal(intCounter);  // Alter dataset column order to match order of exported fields //
                        }
                    }
                    intCounter++;

                    if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        alExportColumns.Add(view.GetRowCellValue(i, "InternalColumnName").ToString());
                        
                        // Include Last Inspection Values if Required //
                        if (IncludeLastInspectionDetailsCheckEdit.Checked)
                        {
                            if (view.GetRowCellValue(i, "TableName").ToString().ToLower() == "inspection" && view.GetRowCellValue(i, "InternalColumnName").ToString().ToLower() != "inspectionphotograph")
                            {
                                alExportColumns.Add("Last_" + view.GetRowCellValue(i, "InternalColumnName").ToString());
                            }
                        }
                    }
                }
                alExportColumns.Add("ObjectType");  // Make sure dummy column isn't removed later...//
                for (int i = dsDataSet.Tables[0].Columns.Count - 1; i >= 0; i--)
                {
                    if (!alExportColumns.Contains(dsDataSet.Tables[0].Columns[i].ColumnName)) dsDataSet.Tables[0].Columns.Remove(dsDataSet.Tables[0].Columns[i].ColumnName);
                }

                // Update ObjectType on each record with a non-blank strPolygonXY (need to set either a polygon [1] if first and last coord set match or polyline [2] if they don't match)//

                #endregion

                #region Generate Mid Mif Files
                // Create MIF FIle //
                intReturnValue = CreateMifFile(view, dsDataSet.Tables[0], strMidMifFileLocation + @"\Tree.mif", ref doubleTotalX, ref doubleTotalY, ref intValuesCounted);  // Last 3 values are returned by called procedure as out variables [passing by Ref] //
                if (intReturnValue == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the Tree MIF file.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

                // Create MID File //
                intReturnValue = CreateMidFile(dsDataSet.Tables[0], strMidMifFileLocation + @"\Tree.mid", false);
                if (intReturnValue == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the Tree MID file.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
                #endregion

                #region Generate Tab File
                // Generate TAB file using Tab2Tab.Exe //
                intReturnValue = CreateMapInfoTabFile("\"" + strMidMifFileLocation + @"\Tree.mif" + "\" \"" + strTabFileLocation + @"\Tree.tab" + "\"");
                if (intReturnValue == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the Tree TAB file.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
                #endregion
            }
            if (boolCreateWorkOrders)
            {
                #region Get Work Order Tree Data

                SqlDataAdapter sdaDataAdpter = new SqlDataAdapter();
                DataSet dsDataSet = new DataSet("NewDataSet");
                SqlConnection conn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp01322_AT_export_to_GBM_workorder_jobs_for_export", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@WorkOrderIDs", i_str_selected_workorder_ids));
                sdaDataAdpter = new SqlDataAdapter(cmd);

                // Ensure columns are of correct datatype as they have Nulls in the StoredProc so VS will map them to Int32 //
                sdaDataAdpter.FillSchema(dsDataSet, SchemaType.Mapped);
                dsDataSet.Tables[0].Columns["dtDueDate"].DataType = System.Type.GetType("System.DateTime");
                dsDataSet.Tables[0].Columns["dtDoneDate"].DataType = System.Type.GetType("System.DateTime");

                dsDataSet.Tables[0].Columns["strDescription"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strJobNumber"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strActionDescription"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strTreeRef"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser1"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser2"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strUser3"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strRemarks"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strWorkOrder"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strPriority"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strSpecies"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strSize"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strNearestHouse"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strActionBy"].DataType = System.Type.GetType("System.String");
                dsDataSet.Tables[0].Columns["strSupervisor"].DataType = System.Type.GetType("System.String");

                sdaDataAdpter.Fill(dsDataSet, "Table");
                conn.Close();
                conn.Dispose();

                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

                #endregion

                #region Generate Work Order Mid Mif Files
                // Create MIF FIle //
                intReturnValue = CreateWorkOrderMifFile(dsDataSet.Tables[0], strMidMifFileLocation + @"\WorkOrder.mif", ref doubleTotalX, ref doubleTotalY, ref intValuesCounted);  // Last 3 values are returned by called procedure as out variables [passing by Ref] //
                if (intReturnValue == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the Work Order MIF file.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

                // Create MID File //
                intReturnValue = CreateMidFile(dsDataSet.Tables[0], strMidMifFileLocation + @"\WorkOrder.mid", false);
                if (intReturnValue == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the Work Order MID file.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
                #endregion

                #region Generate Tab File
                // Generate TAB file using Tab2Tab.Exe //
                intReturnValue = CreateMapInfoTabFile("\"" + strMidMifFileLocation + @"\WorkOrder.mif" + "\" \"" + strTabFileLocation + @"\WorkOrder.tab" + "\"");
                if (intReturnValue == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the WorkOrder TAB file.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress = null;
                    }
                    return;
                }
                if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
                #endregion
            }
            string strTreeTabFile = "";
            string strWorkOrderTabFile = "";
            if (boolCreateSurvey)
            {
                strTreeTabFile = strTabFileLocation + @"\Tree.tab";
            }
            if (boolCreateWorkOrders)
            {
                strWorkOrderTabFile = strTabFileLocation + @"\WorkOrder.tab";
            }

            #region Generate Geoset File
            // Generate Geoset File //
            string strGeosetLocation = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesExportToGBM_Geoset_Folder"));  // Get Geoset File Output Location //
            if (strGeosetLocation == "")
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - No Geoset File Location has been specified in the System Settings table.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strBackgroundMappingLocation = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesExportToGBM_Background_Mapping_Folder"));
            if (strBackgroundMappingLocation == "")
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - No Backgroun Mapping Location has been specified in the System Settings table.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            intReturnValue = CreateGeosetFile(strGeosetLocation + @"\WoodPlan_Survey.gst", strTreeTabFile, strWorkOrderTabFile, strBackgroundMappingLocation, doubleTotalX, doubleTotalY, intValuesCounted, strTabFileLocation + @"\Gazetteer.tab");
            if (intReturnValue != 1)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                if (intReturnValue == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the Geoset file.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else if (intReturnValue == -1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the Geoset file [Missing Map Centre Points].", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            #endregion

            #region Generate Profile File
            // Generate Profile File //
            string strProfileLocation = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesExportToGBM_Profile_Folder"));  // Get Profile File Output Location //
            if (strProfileLocation == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - No Profile File Location has been specified in the System Settings table.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                return;
            }
            string strGBMReferencePathLocation = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesExportToGBM_Pda_Reference_Data_Folder"));
            if (strGBMReferencePathLocation == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - No PDA Working Directory Location has been specified in the System Settings table.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                return;
            }

            intReturnValue = CreateProfileFile(strProfileLocation + @"\WoodPlan_Survey.gbmp", strGeosetLocation + @"\WoodPlan_Survey.gst", strTreeTabFile, strWorkOrderTabFile, strGBMReferencePathLocation, strBackgroundMappingLocation, strTabFileLocation + @"\Gazetteer.tab");
            if (intReturnValue == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the Profile file.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            #endregion

            #region Generate Gazetteer

            SqlDataAdapter sdaDataAdpter3 = new SqlDataAdapter();
            DataSet dsDataSet3 = new DataSet("NewDataSet");
            SqlConnection conn3 = new SqlConnection(strConnectionString);
            SqlCommand cmd3 = new SqlCommand("sp00146_export_to_GBM_gazetteer_localities", conn3);
            cmd3.CommandType = CommandType.StoredProcedure;
            cmd3.Parameters.Add(new SqlParameter("@LocalityIDs", i_str_selected_locality_ids));
            sdaDataAdpter3 = new SqlDataAdapter(cmd3);
            sdaDataAdpter3.Fill(dsDataSet3, "Table");
            conn3.Close();
            conn3.Dispose();

            #region Gazetteer MIF File
            double doubleMinX = (double)999999999999.999;
            double doubleMinY = (double)999999999999.999;
            double doubleMaxX = (double)(-999999999999.999);
            double doubleMaxY = (double)(-999999999999.999);

            System.Text.StringBuilder sbText = new System.Text.StringBuilder("");
            sbText.Append("Columns 1\r\n");
            sbText.Append("  LocalityName Char(50)\r\n");
            sbText.Append("Data\r\n\r\n");

            // ***** Write Localities ***** //
            DataTable dt = dsDataSet3.Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToDouble(dr["X"]) < doubleMinX) doubleMinX = Convert.ToDouble(dr["X"]);
                if (Convert.ToDouble(dr["Y"]) < doubleMinY) doubleMinY = Convert.ToDouble(dr["Y"]);
                if (Convert.ToDouble(dr["X"]) > doubleMaxX) doubleMaxX = Convert.ToDouble(dr["X"]);
                if (Convert.ToDouble(dr["Y"]) > doubleMaxY) doubleMaxY = Convert.ToDouble(dr["Y"]);
                sbText.Append("Point " + dr["X"].ToString() + " " + dr["Y"].ToString() + "\r\n");
                sbText.Append("    Symbol (34,255,10)\r\n");
                doubleTotalX += Convert.ToDouble(dr["X"]);
                doubleTotalY += Convert.ToDouble(dr["Y"]);
                intValuesCounted++;
            }
            // Add extra safety margins to bounds //
            doubleMinX += -5000;
            doubleMinY += -5000;
            doubleMaxX += 5000;
            doubleMaxY += 5000;

            string strText = "";
            strText = "Version 450\r\n";
            strText += "Charset \"WindowsLatin1\"\r\n";
            strText += "Delimiter \",\"\r\n";

            

            strText += strDefaultMappingProjection + " Bounds (" + doubleMinX.ToString() + ", " + doubleMinY.ToString() + ") (" + doubleMaxX.ToString() + ", " + doubleMaxY.ToString() + ")\r\n";
            //strText += "CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000 Bounds (" + doubleMinX.ToString() + ", " + doubleMinY.ToString() + ") (" + doubleMaxX.ToString() + ", " + doubleMaxY.ToString() + ")\r\n";
            sbText.Insert(0, strText);
            StreamWriter sw = new StreamWriter(strMidMifFileLocation + @"\Gazetteer.mif", false);
            sw.Write(sbText);
            sw.Close();         
            #endregion

            #region Gazetteer MID File
            // Strip out unneccessary columns [these were needed earlier for MIF File]... //
            dsDataSet3.Tables[0].Columns.Remove("LocalityID");
            dsDataSet3.Tables[0].Columns.Remove("X");
            dsDataSet3.Tables[0].Columns.Remove("Y");
            intReturnValue = CreateMidFile(dsDataSet3.Tables[0], strMidMifFileLocation + @"\Gazetteer.mid", false);
            if (intReturnValue == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the Gazetteer MID file.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                return;
            }
            //if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            #endregion

            #region Gazetteer TAB File
            intReturnValue = CreateMapInfoTabFile("\"" + strMidMifFileLocation + @"\Gazetteer.mif" + "\" \"" + strTabFileLocation + @"\Gazetteer.tab" + "\"");
            if (intReturnValue == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export - An error occurred while generating the Gazetteer TAB file.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                return;
            }
            #endregion

            #endregion

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show full progress //
                fProgress.Close();
                fProgress = null;
            }
            DevExpress.XtraEditors.XtraMessageBox.Show("Data Export Completed.", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private int CreateMidFile(DataTable dt, string strFilePath, Boolean boolColumnHeaders)
        {
            StreamWriter sw = new StreamWriter(strFilePath, false);
            int iColCount = dt.Columns.Count;
            if (boolColumnHeaders)  // First write the headers [Optional] //
            {
                for (int i = 0; i < iColCount; i++)
                {
                    sw.Write(dt.Columns[i]);
                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            // Now write all the rows //
            
            //char tab = '\u0009';
            //string.Replace(tab.ToString(), "")

            foreach (DataRow dr in dt.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    //if (dt.Columns[i].ColumnName == "dtDueDate_1")
                    //{
                    //    i = i;
                    //}       
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        if (dt.Columns[i].DataType == System.Type.GetType("System.Int32") || dt.Columns[i].DataType == System.Type.GetType("System.Decimal"))
                        //if (CheckingFunctions.IsNumeric(dr[i].ToString()))
                        {
                            sw.Write(dr[i].ToString());  // Numeric so write value without double quotes around it //
                        }
                        //else if (DateTime.TryParse(dr[i].ToString(), out dateTime))
                        else if (dt.Columns[i].DataType == System.Type.GetType("System.DateTime"))
                        {
                            sw.Write(Convert.ToDateTime(dr[i]).ToString("yyyyMMdd"));  // Date so write value without double quotes around it and format to correct structure for MapInfo //
                        }
                        else
                        {
                            sw.Write("\"" + dr[i].ToString().Replace('"', '\'').Replace(System.Environment.NewLine, " ") + "\"");  // Text so surround value in double quotes but replace any double quotes with single quotes and any carriage returns with single spaces first //
                        }
                    }
                    else
                    {
                        if (dt.Columns[i].DataType == System.Type.GetType("System.Int32") || dt.Columns[i].DataType == System.Type.GetType("System.Decimal"))
                        {
                            sw.Write("0");  // No value in numeric column so write 0 value without double quotes around it //
                        }
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();
            return 1;
        }

        private int CreateMifFile(GridView view, DataTable dt, string strFilePath, ref double doubleTotalX, ref double doubleTotalY, ref int intValuesCounted)    // Use "ref" to return values back to original variables [pass by reference] //
        {
            double doubleMinX = (double)999999999999.999;
            double doubleMinY = (double)999999999999.999;
            double doubleMaxX = (double)(-999999999999.999);
            double doubleMaxY = (double)(-999999999999.999);

            System.Text.StringBuilder sbText = new System.Text.StringBuilder("");
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    sbText.Append("  " + view.GetRowCellValue(i, "InternalColumnName").ToString().ToUpper() + " " + System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(view.GetRowCellValue(i, "ColumnType").ToString()));  // Force First Letter of column type to uppercase //
                    switch (view.GetRowCellValue(i, "ColumnType").ToString().ToLower())
                    {
                        case "char":
                            sbText.Append("(" + view.GetRowCellValue(i, "ColumnLength").ToString() + ")");
                            break;
                        case "decimal":
                            sbText.Append("(18, " + view.GetRowCellValue(i, "ColumnLength").ToString() + ")");
                            break;
                        default:
                            break;
                    }
                    sbText.Append("\r\n");

                    // Include Last Inspection Values if Required //
                    if (IncludeLastInspectionDetailsCheckEdit.Checked)
                    {
                        if (view.GetRowCellValue(i, "TableName").ToString().ToLower() == "inspection" && view.GetRowCellValue(i, "InternalColumnName").ToString().ToLower() != "inspectionphotograph")
                        {
                            sbText.Append("  " + "LAST_" + view.GetRowCellValue(i, "InternalColumnName").ToString().ToUpper() + " " + System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(view.GetRowCellValue(i, "ColumnType").ToString()));  // Force First Letter of column type to uppercase //
                            switch (view.GetRowCellValue(i, "ColumnType").ToString().ToLower())
                            {
                                case "char":
                                    sbText.Append("(" + view.GetRowCellValue(i, "ColumnLength").ToString() + ")");
                                    break;
                                case "decimal":
                                    sbText.Append("(18, " + view.GetRowCellValue(i, "ColumnLength").ToString() + ")");
                                    break;
                                default:
                                    break;
                            }
                            sbText.Append("\r\n");
                        }
                    }

                }
            }
            sbText.Append("  ObjectType Integer\r\n");  // Add dummy column to end //

            sbText.Append("Data\r\n\r\n");

            // ***** Write Points and Polygons ***** //
            string[] strArray;
            char[] delimiters = new char[] { ';' };

            string strLocalitiesIncludedForAverageXY = gridLookUpEdit1.EditValue.ToString();
            if (strLocalitiesIncludedForAverageXY != null && strLocalitiesIncludedForAverageXY != "0") strLocalitiesIncludedForAverageXY = "," + strLocalitiesIncludedForAverageXY + ",";  // Wrap with commas to make comparisons more accurate //

            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToInt32(dr["ObjectType"]) == 0)  // Point //
                {
                    if (Convert.ToDouble(dr["intX"]) < doubleMinX) doubleMinX = Convert.ToDouble(dr["intX"]);
                    if (Convert.ToDouble(dr["intY"]) < doubleMinY) doubleMinY = Convert.ToDouble(dr["intY"]);
                    if (Convert.ToDouble(dr["intX"]) > doubleMaxX) doubleMaxX = Convert.ToDouble(dr["intX"]);
                    if (Convert.ToDouble(dr["intY"]) > doubleMaxY) doubleMaxY = Convert.ToDouble(dr["intY"]);
                    sbText.Append("Point " + dr["intX"].ToString() + " " + dr["intY"].ToString() + "\r\n");
                    sbText.Append("    Symbol (34,255,10)\r\n");
                    if (strLocalitiesIncludedForAverageXY == "0" || strLocalitiesIncludedForAverageXY.Contains("," + dr["intLocalityID"].ToString() + ","))
                    {
                        doubleTotalX += Convert.ToDouble(dr["intX"]);
                        doubleTotalY += Convert.ToDouble(dr["intY"]);
                        intValuesCounted++;
                    }
                }
                else if (Convert.ToInt32(dr["ObjectType"]) == 1) // Polygon //
                {
                    sbText.Append("Region 1\r\n");
                    strArray = dr["strPolygonXY"].ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    sbText.Append("  " + Convert.ToInt32(strArray.Length / 2) + "\r\n");
                    for (int i = 0; i < strArray.Length - 1; i++)
                    {
                        if (Convert.ToDouble(strArray[i]) < doubleMinX) doubleMinX = Convert.ToDouble(strArray[i]);
                        if (Convert.ToDouble(strArray[i + 1]) < doubleMinY) doubleMinY = Convert.ToDouble(strArray[i + 1]);
                        if (Convert.ToDouble(strArray[i]) > doubleMaxX) doubleMaxX = Convert.ToDouble(strArray[i]);
                        if (Convert.ToDouble(strArray[i + 1]) > doubleMaxY) doubleMaxY = Convert.ToDouble(strArray[i + 1]);
                        sbText.Append(strArray[i] + " " + strArray[i + 1] + "\r\n");
                        if (strLocalitiesIncludedForAverageXY == "0" || strLocalitiesIncludedForAverageXY.Contains("," + dr["intLocalityID"].ToString() + ","))
                        {
                            doubleTotalX += Convert.ToDouble(strArray[i]);
                            doubleTotalY += Convert.ToDouble(strArray[i + 1]);
                            intValuesCounted++;
                        }
                        i++;  // Jump ahead so that we are always processing odd numbered array items //
                    }
                    sbText.Append("    Pen (1,2,0)\r\n");
                    sbText.Append("    Brush (2,12632256,12632256)\r\n");
                }
                else if (Convert.ToInt32(dr["ObjectType"]) == 2) // Polyline //
                {
                    sbText.Append("Pline\r\n");
                    strArray = dr["strPolygonXY"].ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    sbText.Append("  " + Convert.ToInt32(strArray.Length / 2) + "\r\n");
                    for (int i = 0; i < strArray.Length - 1; i++)
                    {
                        if (Convert.ToDouble(strArray[i]) < doubleMinX) doubleMinX = Convert.ToDouble(strArray[i]);
                        if (Convert.ToDouble(strArray[i + 1]) < doubleMinY) doubleMinY = Convert.ToDouble(strArray[i + 1]);
                        if (Convert.ToDouble(strArray[i]) > doubleMaxX) doubleMaxX = Convert.ToDouble(strArray[i]);
                        if (Convert.ToDouble(strArray[i + 1]) > doubleMaxY) doubleMaxY = Convert.ToDouble(strArray[i + 1]);
                        sbText.Append(strArray[i] + " " + strArray[i + 1] + "\r\n");
                        if (strLocalitiesIncludedForAverageXY == "0" || strLocalitiesIncludedForAverageXY.Contains("," + dr["intLocalityID"].ToString() + ","))
                        {
                            doubleTotalX += Convert.ToDouble(strArray[i]);
                            doubleTotalY += Convert.ToDouble(strArray[i + 1]);
                            intValuesCounted++;
                        }    
                        i++;  // Jump ahead so that we are always processing odd numbered array items //
                    }
                    sbText.Append("    Pen (1,2,0)\r\n");
                    sbText.Append("    Brush (2,12632256,12632256)\r\n");
                }
            }

            // Add extra safety margins to bounds //
            doubleMinX += -5000;
            doubleMinY += -5000;
            doubleMaxX += 5000;
            doubleMaxY += 5000;

            string strText = "";
            strText = "Version 450\r\n";
            strText+="Charset \"WindowsLatin1\"\r\n";
            strText+="Delimiter \",\"\r\n";
            strText+=strDefaultMappingProjection + " Bounds (" + doubleMinX.ToString() + ", " + doubleMinY.ToString() + ") (" + doubleMaxX.ToString() + ", " + doubleMaxY.ToString() + ")\r\n";
            //strText+="CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000 Bounds (" + doubleMinX.ToString() + ", " + doubleMinY.ToString() + ") (" + doubleMaxX.ToString() + ", " + doubleMaxY.ToString() + ")\r\n";
            strText+="Columns " + dt.Columns.Count.ToString() + "\r\n";

            sbText.Insert(0, strText);

            StreamWriter sw = new StreamWriter(strFilePath, false);
            sw.Write(sbText);

            sw.Close();
            return 1;
        }

        private int CreateMapInfoTabFile(string strArguments)
        {
            string strAppToRun = "\"" + Application.StartupPath + "\\Plugin_Apps\\tab2tab.exe" + "\"";
            string error = "";
            try
            {
                System.Diagnostics.Process Proc = new System.Diagnostics.Process();
                Proc.StartInfo.CreateNoWindow = true;  // Hide DOS Window [This line and next]//
                Proc.StartInfo.UseShellExecute = false;
                Proc.StartInfo.RedirectStandardError = true;
                Proc.StartInfo.FileName = strAppToRun;
                Proc.StartInfo.Arguments = strArguments;

                Proc.Start();
                error = Proc.StandardError.ReadToEnd();
                Proc.WaitForExit(300000);
                Proc.Close();
                Proc = null;
                GC.GetTotalMemory(true);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to export data - an error occurred with Tab2Tab.EXE - contact Technical Support.", "Export To GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }
            if (!string.IsNullOrEmpty(error)) return 0;  // An error occurred //
            return 1;
        }

        private int CreateGeosetFile(string strGeosetFilePath, string strTreeTabFilePath, string strWorkOrderTabFilePath, string strBackgroundMappingLocation, double doubleTotalX, double doubleTotalY, int intValuesCounted, string strGazetteerFilePath)
        {
            bool boolErrorWithMapCentre = false;

            StreamWriter sw = new StreamWriter(strGeosetFilePath, false);
            sw.Write("!GEOSET"); sw.Write(sw.NewLine);
            sw.Write("!VERSION 100"); sw.Write(sw.NewLine);
            sw.Write("begin_metadata"); sw.Write(sw.NewLine);
            sw.Write("\"\\GEOSET\\NAME\" = \"WoodPlan_Survey\""); sw.Write(sw.NewLine);
            sw.Write("\"\\GEOSET\\PROJECTION\" = \"8, 79, 7, -2, 49, 0.9996012717, 400000, -100000\""); sw.Write(sw.NewLine);
            string strCentre = "";
            // Get Centre points - first check Centre Locality on Drop down for a set of coordinates. If none then use calculated value from trees
            int intCentreLocality = (gridLookUpEdit1.EditValue == null ? 0 : Convert.ToInt32(gridLookUpEdit1.EditValue));
            if (intCentreLocality != 0)
            {
                GridView childView = (GridView)gridLookUpEdit1.Properties.View;
                int intFoundRow = childView.LocateByValue(0, childView.Columns["LocalityID"], Convert.ToInt32(intCentreLocality));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    double doubleX = Convert.ToDouble(childView.GetRowCellValue(intFoundRow, "Xcoordinate"));
                    double doubleY = Convert.ToDouble(childView.GetRowCellValue(intFoundRow, "Ycoordinate"));
                    if (doubleX != 0.00 || doubleY != 0.00)
                    {
                        strCentre = doubleX.ToString() + "," + doubleY.ToString();
                    }
                }
            }
            if (strCentre == "")  // No Locality Coordinates so get from tree data //
            {
                try
                {
                    strCentre = Convert.ToDecimal(doubleTotalX / intValuesCounted).ToString() + "," + Convert.ToDouble(doubleTotalY / intValuesCounted).ToString();
                }
                catch (Exception)
                {
                    strCentre = "0,0";
                }
            }
            if (strCentre == "0,0") boolErrorWithMapCentre = true;

            sw.Write("\"\\GEOSET\\CENTER\" = \"" + strCentre + "\""); sw.Write(sw.NewLine);
            sw.Write("\"\\GEOSET\\CENTER_USER\" = \"" + strCentre + "\""); sw.Write(sw.NewLine);
            double doubleZoomLevel = (double)0.00;
            try 
	        {
                doubleZoomLevel = Convert.ToDouble(gridLookUpEdit2.EditValue) * 0.00019163;
	        }
	        catch (Exception)
	        {
                doubleZoomLevel = (double)0.25;
		   	}
            sw.Write("\"\\GEOSET\\ZOOMLEVEL\" = \"" + doubleZoomLevel.ToString() + "\""); sw.Write(sw.NewLine);
            sw.Write("\"\\GEOSET\\AUTOLAYER\" = \"FALSE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\GEOSET\\MAPUNIT\" = \"1\""); sw.Write(sw.NewLine);

            int intCounter = 1;
            // Write Gazetteer file //
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\FILE\" = \"" + strGazetteerFilePath + "\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\ISVISIBLE\" = \"FALSE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\AUTOLABEL\" = \"FALSE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWLINEDIRECTION\" = \"FALSE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWNODES\" = \"FALSE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWCENTROIDS\" = \"FALSE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SELECTABLE\" = \"TRUE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\EDITABLE\" = \"FALSE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\DUPLICATE\" = \"TRUE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\LINETYPE\" = \"2\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\OFFSET\" = \"2\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\OVERLAP\" = \"FALSE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\PARALLEL\" = \"TRUE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\POSITION\" = \"5\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\PARTIALSEGMENTS\" = \"FALSE\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\STYLE\" = \"0\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\EXTSTYLE\" = \"0\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\DESCRIPTION\" = \"Arial\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\SIZE\" = \"9\""); sw.Write(sw.NewLine);
            sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\FORECOLOR\" = \"0\""); sw.Write(sw.NewLine);
            intCounter++;
            if (!string.IsNullOrEmpty(strTreeTabFilePath))
            {
                // Write Tree File Layer //
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\FILE\" = \"" + strTreeTabFilePath + "\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\ISVISIBLE\" = \"TRUE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\AUTOLABEL\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWLINEDIRECTION\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWNODES\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWCENTROIDS\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SELECTABLE\" = \"TRUE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\EDITABLE\" = \"TRUE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\DUPLICATE\" = \"TRUE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\LINETYPE\" = \"2\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\OFFSET\" = \"2\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\OVERLAP\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\PARALLEL\" = \"TRUE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\POSITION\" = \"5\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\PARTIALSEGMENTS\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\STYLE\" = \"0\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\EXTSTYLE\" = \"0\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\DESCRIPTION\" = \"Arial\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\SIZE\" = \"9\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\FORECOLOR\" = \"0\""); sw.Write(sw.NewLine);
                intCounter++;
            }
            if (!string.IsNullOrEmpty(strWorkOrderTabFilePath))
            {
                // Write Tree File Layer //
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\FILE\" = \"" + strWorkOrderTabFilePath + "\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\ISVISIBLE\" = \"TRUE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\AUTOLABEL\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWLINEDIRECTION\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWNODES\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWCENTROIDS\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SELECTABLE\" = \"TRUE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\EDITABLE\" = \"TRUE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\DUPLICATE\" = \"TRUE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\LINETYPE\" = \"2\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\OFFSET\" = \"2\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\OVERLAP\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\PARALLEL\" = \"TRUE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\POSITION\" = \"5\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\PARTIALSEGMENTS\" = \"FALSE\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\STYLE\" = \"0\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\EXTSTYLE\" = \"0\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\DESCRIPTION\" = \"Arial\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\SIZE\" = \"9\""); sw.Write(sw.NewLine);
                sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\FORECOLOR\" = \"0\""); sw.Write(sw.NewLine);
                intCounter++;
            }

            // Write Each Map Background Layer //
            GridView view = (GridView)gridControl8.MainView;
            char[] delimiters = new char[] { ',' };
            Array arrayDistrictMapLinks = null;
            ArrayList arrayListDistrictMapLinksParsed = new ArrayList();
            arrayDistrictMapLinks = i_str_selected_map_file_names.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            foreach (string strElement in arrayDistrictMapLinks)
            {
                if (!arrayListDistrictMapLinksParsed.Contains(strElement))
                {
                    arrayListDistrictMapLinksParsed.Add(strElement);
                }
            }
            string strFileText = "";
            foreach (string strElement in arrayListDistrictMapLinksParsed)
            {
                // Determin if the file is Raster or Vector //
                try
                {
                    strFileText = System.IO.File.ReadAllText(strBackgroundMappingLocation + strElement.Trim());
                }
                catch (Exception)
                {
                    // Ignore current file and proceed to next iteration of loop //
                    continue;
                }
                if (strFileText.Contains("Type \"RASTER\""))
                {
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\FILE\" = \"" + strBackgroundMappingLocation + strElement.Trim() + "\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\ISVISIBLE\" = \"TRUE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\ZOOM\\MIN\" = \"0.25\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\ZOOM\\MAX\" = \"100\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\ZOOM\\UNIT\" = \"1\""); sw.Write(sw.NewLine);
                }
                else  // Vector //
                {
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\FILE\" = \"" + strBackgroundMappingLocation + strElement.Trim() + "\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\ISVISIBLE\" = \"TRUE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\AUTOLABEL\" = \"FALSE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWLINEDIRECTION\" = \"FALSE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWNODES\" = \"FALSE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SHOWCENTROIDS\" = \"FALSE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\SELECTABLE\" = \"FALSE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\EDITABLE\" = \"FALSE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\DUPLICATE\" = \"FALSE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\LINETYPE\" = \"2\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\OFFSET\" = \"2\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\OVERLAP\" = \"FALSE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\PARALLEL\" = \"TRUE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\POSITION\" = \"2\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\PARTIALSEGMENTS\" = \"FALSE\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\STYLE\" = \"0\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\EXTSTYLE\" = \"0\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\DESCRIPTION\" = \"Arial\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\SIZE\" = \"9\""); sw.Write(sw.NewLine);
                    sw.Write("\"\\TABLE\\" + intCounter.ToString() + "\\LABEL\\FONT\\FORECOLOR\" = \"0\""); sw.Write(sw.NewLine);
                }
                intCounter++;
            }
            sw.Write("end_metadata"); sw.Write(sw.NewLine);
            sw.Close();
            if (boolErrorWithMapCentre)
            {
                return -1;  // Error With Centre point (0,0) //
            }
            else
            {
                return 1;
            }
        }

        private int CreateProfileFile(string stProfileFilePath, string strGeosetFilePath, string strTreeTabFilePath, string strWorkOrderTabFilePath, string strGBMReferencePath, string strBackgroundMappingLocation, string strGazetteerFilePath)
        {
            XmlTextWriter textWriter = new XmlTextWriter(stProfileFilePath, System.Text.Encoding.UTF8);
            textWriter.WriteStartDocument();  // Open the document //
            textWriter.WriteComment("Generated by WoodPlan 5");  // Write comments //

            textWriter.WriteStartElement("profile");  // Write first element  - Not closed so subsequent items are indented inside it //
            {
                textWriter.WriteStartElement("DataSet", ""); textWriter.WriteString(strGeosetFilePath); textWriter.WriteEndElement();
                textWriter.WriteStartElement("Description", ""); textWriter.WriteString("WoodPlan_Survey"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("ProfileName", ""); textWriter.WriteString("WoodPlan_Survey"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("ProfileFileName", ""); textWriter.WriteString(stProfileFilePath); textWriter.WriteEndElement();
                textWriter.WriteStartElement("Files");
                {
                    if (!string.IsNullOrEmpty(strTreeTabFilePath))
                    {
                        textWriter.WriteStartElement("File");  // Gazetteer File //
                        {
                            textWriter.WriteStartElement("Name", ""); textWriter.WriteString(strGazetteerFilePath); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("IsRefData", ""); textWriter.WriteString("True"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("RefPath", ""); textWriter.WriteString(strGBMReferencePath); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("HasRaster", ""); textWriter.WriteString("False"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("RasterPath", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("IsSeamless", ""); textWriter.WriteString("False"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("SeamlessReferencedFiles", ""); textWriter.WriteEndElement(); // Node is Blank //
                        }
                        textWriter.WriteEndElement();  // End of File //
                        textWriter.WriteStartElement("File");  // Tree.Tab file //
                        {
                            textWriter.WriteStartElement("Name", ""); textWriter.WriteString(strTreeTabFilePath); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("IsRefData", ""); textWriter.WriteString("False"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("RefPath", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("HasRaster", ""); textWriter.WriteString("False"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("RasterPath", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("IsSeamless", ""); textWriter.WriteString("False"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("SeamlessReferencedFiles", ""); textWriter.WriteEndElement(); // Node is Blank //
                        }
                        textWriter.WriteEndElement();  // End of File //
                    }
                    if (!string.IsNullOrEmpty(strWorkOrderTabFilePath))
                    {
                        textWriter.WriteStartElement("File");  // WorkOrder.Tab file //
                        {
                            textWriter.WriteStartElement("Name", ""); textWriter.WriteString(strWorkOrderTabFilePath); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("IsRefData", ""); textWriter.WriteString("False"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("RefPath", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("HasRaster", ""); textWriter.WriteString("False"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("RasterPath", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("IsSeamless", ""); textWriter.WriteString("False"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("SeamlessReferencedFiles", ""); textWriter.WriteEndElement(); // Node is Blank //
                        }
                        textWriter.WriteEndElement();  // End of File //
                    }

                    
                    GridView view = (GridView)gridControl8.MainView;
                    char[] delimiters = new char[] { ',' };
                    Array arrayDistrictMapLinks = null;
                    ArrayList arrayListDistrictMapLinksParsed = new ArrayList();
                    arrayDistrictMapLinks = i_str_selected_map_file_names.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string strElement in arrayDistrictMapLinks)
                    {
                        if (!arrayListDistrictMapLinksParsed.Contains(strElement))
                        {
                            arrayListDistrictMapLinksParsed.Add(strElement);
                        }
                    }
                    string strFileText = "";
                    foreach (string strElement in arrayListDistrictMapLinksParsed)
                    {
                        // Determin if the file is Raster or Vector //
                        try
                        {
                            strFileText = System.IO.File.ReadAllText(strBackgroundMappingLocation + strElement.Trim());
                        }
                        catch (Exception)
                        {
                            // Ignore current file and proceed to next iteration of loop //
                            continue;
                        }
                        textWriter.WriteStartElement("File", "");
                        {
                            textWriter.WriteStartElement("Name", ""); textWriter.WriteString(strBackgroundMappingLocation + strElement.Trim()); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("IsRefData", ""); textWriter.WriteString("True"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("RefPath", ""); textWriter.WriteString(strGBMReferencePath); textWriter.WriteEndElement();
                            if (strFileText.Contains("Type \"RASTER\""))
                            {
                                textWriter.WriteStartElement("HasRaster", ""); textWriter.WriteString("True"); textWriter.WriteEndElement();
                                // Extract the raster filename from the tab file //
                                char chSep = '"';
                                string[] arrStr = strFileText.Split(chSep);
                                if (arrStr.Length >= 1)  // Filename found and stored in second element of array (Element 1) //
                                {
                                    textWriter.WriteStartElement("RasterPath", ""); textWriter.WriteString(arrStr[1]); textWriter.WriteEndElement();

                                }
                                else  // Unable to find filename so write a blank in instead //
                                {
                                    textWriter.WriteStartElement("RasterPath", ""); textWriter.WriteEndElement(); // Node is Blank //
                                }
                            }
                            else  // Vector //
                            {
                                textWriter.WriteStartElement("HasRaster", ""); textWriter.WriteString("False"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("RasterPath", ""); textWriter.WriteEndElement(); // Node is Blank //
                            }
                            textWriter.WriteStartElement("IsSeamless", ""); textWriter.WriteString("False"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("SeamlessReferencedFiles", ""); textWriter.WriteEndElement(); // Node is Blank //
                        }
                        textWriter.WriteEndElement();  // End of File //
                    }
                }
                textWriter.WriteEndElement();  // End of Files //

                textWriter.WriteStartElement("Forms");
                {
                    if (!string.IsNullOrEmpty(strTreeTabFilePath))
                    {
                        textWriter.WriteStartElement("Form");
                        {
                            textWriter.WriteStartElement("Fields", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("Name", ""); textWriter.WriteString("Single Tree"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("Layer", ""); textWriter.WriteString("Tree"); textWriter.WriteEndElement();

                        }
                        textWriter.WriteEndElement();  // End of Forms //

                        textWriter.WriteStartElement("Form");
                        {
                            textWriter.WriteStartElement("Fields", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("Name", ""); textWriter.WriteString("Tree Group"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("Layer", ""); textWriter.WriteString("Tree"); textWriter.WriteEndElement();

                        }
                        textWriter.WriteEndElement();  // End of Forms //

                        textWriter.WriteStartElement("Form");
                        {
                            textWriter.WriteStartElement("Fields", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("Name", ""); textWriter.WriteString("Tree Line"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("Layer", ""); textWriter.WriteString("Tree"); textWriter.WriteEndElement();

                        }
                        textWriter.WriteEndElement();  // End of Forms //
                    }

                    if (!string.IsNullOrEmpty(strWorkOrderTabFilePath))
                    {
                        textWriter.WriteStartElement("Form");
                        {
                            textWriter.WriteStartElement("Fields", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("Name", ""); textWriter.WriteString("WorkOrder Tree"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("Layer", ""); textWriter.WriteString("WorkOrder"); textWriter.WriteEndElement();

                        }
                        textWriter.WriteEndElement();  // End of Forms //

                        textWriter.WriteStartElement("Form");
                        {
                            textWriter.WriteStartElement("Fields", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("Name", ""); textWriter.WriteString("WorkOrder Group"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("Layer", ""); textWriter.WriteString("WorkOrder"); textWriter.WriteEndElement();

                        }
                        textWriter.WriteEndElement();  // End of Forms //

                        textWriter.WriteStartElement("Form");
                        {
                            textWriter.WriteStartElement("Fields", ""); textWriter.WriteEndElement(); // Node is Blank //
                            textWriter.WriteStartElement("Name", ""); textWriter.WriteString("WorkOrder Line"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("Layer", ""); textWriter.WriteString("WorkOrder"); textWriter.WriteEndElement();

                        }
                        textWriter.WriteEndElement();  // End of Forms //
                    }

                    textWriter.WriteStartElement("Form");
                    {
                        textWriter.WriteStartElement("Fields", ""); textWriter.WriteEndElement(); // Node is Blank //
                        textWriter.WriteStartElement("Name", ""); textWriter.WriteString("Gazetteer"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("Layer", ""); textWriter.WriteString("Gazetteer"); textWriter.WriteEndElement();

                    }
                    textWriter.WriteEndElement();  // End of Forms //
                }
                textWriter.WriteEndElement();  // End of Forms //
            }
            textWriter.WriteEndElement();  // End of xml //

            textWriter.WriteEndDocument();  // Ends the document //
            textWriter.Close();  // Close writer //           
            return 1;
        }

        private void ExportData(string strFormType, XmlTextWriter textWriter)
        {
            textWriter.WriteStartElement("object_def");  // Write second element - Not closed so subsequent items are indented inside it //
            {
                textWriter.WriteStartElement("date_modified", ""); textWriter.WriteString(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")); textWriter.WriteEndElement();

                switch (strFormType)
                {
                    case "Point":
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("Single Tree");
                        {
                            textWriter.WriteStartElement("plural", ""); textWriter.WriteString("Single Tree"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();
                        break;
                    case "Polygon":
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("Tree Group");
                        {
                            textWriter.WriteStartElement("plural", ""); textWriter.WriteString("Tree Group"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();
                        break;
                    default:  // Polyline //
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("Tree Line");
                        {
                            textWriter.WriteStartElement("plural", ""); textWriter.WriteString("Tree Line"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();
                        break;
                }

                textWriter.WriteStartElement("description", ""); textWriter.WriteString("Test"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("can_edit", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("can_draw", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("can_gps", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("auto_form", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();

                int intStyleField = 0;
                if (glueStyleField.EditValue != null) intStyleField = Convert.ToInt32(glueStyleField.EditValue);
                if (intStyleField != 0)
                {
                    textWriter.WriteStartElement("use_value_styles", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                }
                else
                {
                    textWriter.WriteStartElement("use_value_styles", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                }
                textWriter.WriteStartElement("use_find", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_attributes", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_parented_lists", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_edit_tracking", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_printing", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();

                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                string strPath = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesExportToGBM_Tab_Folder"));
                if (strPath == "")
                {
                    textWriter.WriteEndElement();
                    textWriter.WriteEndDocument();  // Ends the document //
                    textWriter.Close();  // close writer //
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export, no Default Save Folder has been specified for the export.\n\nUse the System Configuration screen to adjust before trying again.", "Export", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!strPath.EndsWith("\\")) strPath += "\\";

                textWriter.WriteStartElement("spatial_def", "");
                {
                    textWriter.WriteStartElement("file", ""); textWriter.WriteString(strPath + "Tree.tab"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("layer", ""); textWriter.WriteString("Tree"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("ObjectType");
                    {
                        switch (strFormType)
                        {
                            case "Point":
                                textWriter.WriteStartElement("value", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                break;
                            case "Polygon":
                                textWriter.WriteStartElement("value", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                break;
                            default:  // Polyline //
                                textWriter.WriteStartElement("value", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                break;
                        }
                    }
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("list_field", ""); textWriter.WriteString("strTreeRef"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("list_field", ""); textWriter.WriteEndElement();
                    if (strFormType == "Point")
                    {
                        textWriter.WriteStartElement("spatial_type", ""); textWriter.WriteString("point"); textWriter.WriteEndElement();
                    }
                    else if (strFormType == "Polygon")
                    {
                        textWriter.WriteStartElement("spatial_type", ""); textWriter.WriteString("region"); textWriter.WriteEndElement();
                    }
                    else  // Polyline //
                    {
                        textWriter.WriteStartElement("spatial_type", ""); textWriter.WriteString("pline"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteStartElement("file_fields", "");
                    {
                        // Process all rows to go out from the exporter //
                        string strType = "";
                        GridView view = (GridView)gridControl3.MainView;
                        //if (view.ActiveFilter.Criteria != null) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                        if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                            {
                                textWriter.WriteStartElement("file_field", "");
                                {
                                    textWriter.WriteStartElement("name", ""); textWriter.WriteString(view.GetRowCellValue(i, "InternalColumnName").ToString()); textWriter.WriteEndElement();
                                    strType = view.GetRowCellValue(i, "ColumnType").ToString().ToLower();
                                    switch (strType)
                                    {
                                        case "char":
                                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                            textWriter.WriteStartElement("width", ""); textWriter.WriteString(view.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                            break;
                                        case "integer":
                                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                                            break;
                                        case "date":
                                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                            break;
                                        case "decimal":
                                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                            textWriter.WriteStartElement("precision", ""); textWriter.WriteString("18"); textWriter.WriteEndElement();
                                            textWriter.WriteStartElement("decimals", ""); textWriter.WriteString(view.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                            break;
                                        default:
                                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                            textWriter.WriteStartElement("width", ""); textWriter.WriteString(view.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                            break;
                                    }
                                }
                                textWriter.WriteEndElement();

                                // Write Last Inspection Values if Required //
                                if (IncludeLastInspectionDetailsCheckEdit.Checked)
                                {
                                    if (view.GetRowCellValue(i, "TableName").ToString().ToLower() == "inspection" && view.GetRowCellValue(i, "InternalColumnName").ToString().ToLower() != "inspectionphotograph")
                                    {
                                        textWriter.WriteStartElement("file_field", "");
                                        {
                                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("Last_" + view.GetRowCellValue(i, "InternalColumnName").ToString()); textWriter.WriteEndElement();
                                            strType = view.GetRowCellValue(i, "ColumnType").ToString().ToLower();
                                            switch (strType)
                                            {
                                                case "char":
                                                    textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                    textWriter.WriteStartElement("width", ""); textWriter.WriteString(view.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                                    break;
                                                case "integer":
                                                    textWriter.WriteStartElement("type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                                                    break;
                                                case "date":
                                                    textWriter.WriteStartElement("type", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                                    break;
                                                case "decimal":
                                                    textWriter.WriteStartElement("type", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                                    textWriter.WriteStartElement("precision", ""); textWriter.WriteString("18"); textWriter.WriteEndElement();
                                                    textWriter.WriteStartElement("decimals", ""); textWriter.WriteString(view.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                                    break;
                                                default:
                                                    textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                    textWriter.WriteStartElement("width", ""); textWriter.WriteString(view.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                                    break;
                                            }
                                        }
                                        textWriter.WriteEndElement();
                                    }
                                }
                                // End of Write Last Inspection Values //
                            }
                        }

                        textWriter.WriteStartElement("file_field", "");  // Add in Dummy field //
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("ObjectType"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of Dummy field //
                    }
                    textWriter.WriteEndElement();  // End of File fields //

                    textWriter.WriteStartElement("style", "");
                    {
                        if (strFormType == "Point")  // Similar code fired later for Polygons //
                        {
                            textWriter.WriteStartElement("symbol", "");
                            {
                                textWriter.WriteStartElement("font", ""); textWriter.WriteString("Map Symbols"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("size", ""); textWriter.WriteString("9.75"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bold", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("italic", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("underline", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("strikethrough", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("weight", ""); textWriter.WriteString("400"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("charset", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bitmap_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bitmap_name", ""); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bitmap_override_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bitmap_size", ""); textWriter.WriteString("14"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bitmap_transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("character", ""); textWriter.WriteString("33"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString("16777215"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("color", ""); textWriter.WriteString("5672960"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("halo", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("opaque", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("rotation", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("shadow", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("vector_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("vector_size", ""); textWriter.WriteString("14"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // end of symbol //
                        }
                        else if (strFormType == "Polygon")
                        {
                            textWriter.WriteStartElement("region", "");
                            {
                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString("16777215"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_style", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_width", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_width_unit", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("color", ""); textWriter.WriteString("16734240"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("pattern", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // end of region //
                        }
                        else // Polyline //
                        {
                            textWriter.WriteStartElement("pline", "");
                            {
                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString("16777215"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_style", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_width", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_width_unit", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("color", ""); textWriter.WriteString("16734240"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("pattern", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // end of region //
                        }
                    }
                    textWriter.WriteEndElement();  // End of style //
                    textWriter.WriteStartElement("db", "");
                    {
                        textWriter.WriteStartElement("db_fields_sql", ""); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("db_con_name", ""); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("db_con_file", ""); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("db_con", ""); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of db //

                }
                textWriter.WriteEndElement();  // End of spatial_def //

                // ********** Style Definitions Section ********** //
                int intRed = 0;
                int intGreen = 0;
                int intBlue = 0;
                int intCalculatedColour = 0;
                Color tempColour;

                if (intStyleField != 0)
                {
                    string strStyleFieldName = "";
                    GridView view = (GridView)gridControl3.MainView;
                    int intFoundRow = view.LocateByValue(0, view.Columns["ColumnID"], intStyleField);
                    if (intFoundRow >= 0) strStyleFieldName = Convert.ToString(view.GetRowCellValue(intFoundRow, "InternalColumnName"));
                    if (strStyleFieldName != "")
                    {
                        textWriter.WriteStartElement("value_style_defs", "");
                        {
                            textWriter.WriteStartElement("field", ""); textWriter.WriteString(strStyleFieldName); textWriter.WriteEndElement();

                            GridView viewStyles = (GridView)gridControl6.MainView;
                            for (int i = 0; i < viewStyles.DataRowCount; i++)
                            {
                                textWriter.WriteStartElement("value_style_def", "");
                                {
                                    textWriter.WriteStartElement("value", ""); textWriter.WriteString(Convert.ToString(viewStyles.GetRowCellValue(i, "ItemID"))); textWriter.WriteEndElement();
                                    textWriter.WriteStartElement("style", "");
                                    {
                                        if (strFormType == "Point")
                                        {
                                            textWriter.WriteStartElement("symbol", "");
                                            {
                                                textWriter.WriteStartElement("font", ""); textWriter.WriteString("Map Symbols"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("size", ""); textWriter.WriteString(Convert.ToString(viewStyles.GetRowCellValue(i, "Size"))); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bold", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("italic", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("underline", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("strikethrough", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("weight", ""); textWriter.WriteString("400"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("charset", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bitmap_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bitmap_name", ""); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bitmap_override_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bitmap_size", ""); textWriter.WriteString("14"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bitmap_transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("character", ""); textWriter.WriteString(Convert.ToString(viewStyles.GetRowCellValue(i, "Symbol"))); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString("16777215"); textWriter.WriteEndElement();

                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "SymbolColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                textWriter.WriteStartElement("halo", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("opaque", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("rotation", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("shadow", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("vector_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("vector_size", ""); textWriter.WriteString("14"); textWriter.WriteEndElement();
                                            }
                                            textWriter.WriteEndElement();  // End of symbol //  
                                        }
                                        else if (strFormType == "Polygon")
                                        {
                                            textWriter.WriteStartElement("region", "");
                                            {
                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonFillPatternColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonLineColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("border_color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                textWriter.WriteStartElement("border_style", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("border_width", ""); textWriter.WriteString(viewStyles.GetRowCellValue(i, "PolygonLineWidth").ToString()); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("border_width_unit", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();

                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonFillColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                textWriter.WriteStartElement("pattern", ""); textWriter.WriteString(viewStyles.GetRowCellValue(i, "PolygonFillPattern").ToString()); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                            }
                                            textWriter.WriteEndElement();  // End of region //  
                                        }
                                        else  // Polyline //
                                        {
                                            textWriter.WriteStartElement("pline", "");
                                            {
                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonFillPatternColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonLineColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("border_color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                textWriter.WriteStartElement("border_style", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("border_width", ""); textWriter.WriteString(viewStyles.GetRowCellValue(i, "PolygonLineWidth").ToString()); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("border_width_unit", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();

                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonFillColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                textWriter.WriteStartElement("pattern", ""); textWriter.WriteString(viewStyles.GetRowCellValue(i, "PolygonFillPattern").ToString()); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                            }
                                            textWriter.WriteEndElement();  // End of region //  
                                        }
                                    }
                                    textWriter.WriteEndElement();  // End of style //
                                }
                                textWriter.WriteEndElement();  // End of value_style_def //
                            }
                        }
                        textWriter.WriteEndElement();  // End of value_style_defs //
                    }
                }

                textWriter.WriteStartElement("field_def", "");  // Dummy field to control if Point or Polygon //
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("ObjectType"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Object Type:"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Object Type:"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("number"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    if (strFormType == "Point")
                    {
                        textWriter.WriteStartElement("default", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    }
                    else if (strFormType == "Polygon")
                    {
                        textWriter.WriteStartElement("default", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                    }
                    else  // Polyline //
                    {
                        textWriter.WriteStartElement("default", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteStartElement("min", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();

                }
                textWriter.WriteEndElement();  // End of field_def //

                GridView view3 = (GridView)gridControl3.MainView;
                GridView view4 = (GridView)gridControl4.MainView;
                //if (view4.ActiveFilter.Criteria != null) view4.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                if (!ReferenceEquals(view4.ActiveFilter.Criteria, null)) view4.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                GridView view5 = (GridView)gridControl5.MainView;
                //if (view5.ActiveFilter.Criteria != null) view5.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                if (!ReferenceEquals(view5.ActiveFilter.Criteria, null)) view5.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                string strType2;
                int intLastDistrictID = 0;
                int intThisDistrictID = 0;

                for (int i = 0; i < view3.DataRowCount; i++)
                {
                    string strColumnName = view3.GetRowCellValue(i, "InternalColumnName").ToString().ToLower();
                    if (Convert.ToInt32(view3.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        textWriter.WriteStartElement("field_def", "");
                        {
                            textWriter.WriteStartElement("field", ""); textWriter.WriteString(view3.GetRowCellValue(i, "InternalColumnName").ToString()); textWriter.WriteEndElement();
                            strType2 = view3.GetRowCellValue(i, "ColumnType").ToString().ToLower();
                            switch (strType2)
                            {
                                case "char":
                                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                    break;
                                case "integer":
                                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                                    break;
                                case "date":
                                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                    break;
                                case "decimal":
                                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                    break;
                                default:
                                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                    break;
                            }
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLabel").ToString()); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("prompt", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLabel").ToString()); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("visible", ""); textWriter.WriteString(Convert.ToString(Convert.ToInt32(view3.GetRowCellValue(i, "CheckMarkSelection")) * -1)); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("editable", ""); textWriter.WriteString(Convert.ToString(Convert.ToInt32(view3.GetRowCellValue(i, "Editable")) * -1)); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("searchable", ""); textWriter.WriteString(Convert.ToString(Convert.ToInt32(view3.GetRowCellValue(i, "Searchable")) * -1)); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString(Convert.ToString(Convert.ToInt32(view3.GetRowCellValue(i, "Manditory")) * -1)); textWriter.WriteEndElement();
                            string strEditType = "";
                            if (Convert.ToInt32(view3.GetRowCellValue(i, "PickList")) != 0)
                            {
                                strEditType = "list";
                            }
                            else
                            {
                                switch (strType2)
                                {
                                    case "char":
                                        strEditType = "text";
                                        break;
                                    case "decimal":
                                    case "integer":
                                        strEditType = "number";
                                        break;
                                    case "date":
                                        strEditType = "date";
                                        break;
                                    default:
                                        strEditType = "";
                                        break;
                                }
                            }
                            textWriter.WriteStartElement("edit", ""); textWriter.WriteString(strEditType); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                            if (strColumnName == "intstatus")  // Default to 1 [Actual] //
                            {
                                textWriter.WriteStartElement("default", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                            }
                            else
                            {
                                textWriter.WriteStartElement("default", ""); textWriter.WriteEndElement();
                            }
                            switch (strType2)
                            {
                                case "char":
                                    textWriter.WriteStartElement("max", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                    break;
                                case "decimal":
                                    textWriter.WriteStartElement("max", ""); textWriter.WriteEndElement();
                                    textWriter.WriteStartElement("decimals", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                    break;
                                case "integer":
                                    textWriter.WriteStartElement("min", ""); textWriter.WriteEndElement();
                                    textWriter.WriteStartElement("max", ""); textWriter.WriteEndElement();
                                    break;
                                case "date":
                                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("8"); textWriter.WriteEndElement();
                                    break;
                                default:
                                    textWriter.WriteStartElement("max", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                    break;
                            }

                            switch (strColumnName)
                            {
                                case "intlocalityid":
                                    textWriter.WriteStartElement("parent_field", ""); textWriter.WriteString("intDistrictID"); textWriter.WriteEndElement();

                                    textWriter.WriteStartElement("parented_list", "");  // Dummy parent list for 0 [-- None --] district //
                                    {
                                        textWriter.WriteStartElement("parent_value", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                        textWriter.WriteStartElement("default", ""); textWriter.WriteString(""); textWriter.WriteEndElement();

                                        textWriter.WriteStartElement("list", ""); textWriter.WriteString("0");  // Dummy picklist item for none //
                                        {
                                            textWriter.WriteStartElement("desc", ""); textWriter.WriteString("--None--"); textWriter.WriteEndElement();
                                        }
                                        textWriter.WriteEndElement();  // End of dummy picklist item for none //
                                    }
                                    textWriter.WriteEndElement();  // End of Dummy parented list //
                                    intLastDistrictID = 0;
                                    for (int k = 0; k < view4.DataRowCount; k++)
                                    {
                                        if (Convert.ToInt32(view4.GetRowCellValue(k, "CheckMarkSelection")) == 1 || popupContainerEdit1.EditValue.ToString() == "All Localities [No Filter]")
                                        {
                                            intThisDistrictID = Convert.ToInt32(view4.GetRowCellValue(k, "DistrictID"));
                                            if (intThisDistrictID != intLastDistrictID)
                                            {
                                                if (intLastDistrictID > 0) textWriter.WriteEndElement(); // Write End Element for last group processed //

                                                textWriter.WriteStartElement("parented_list", "");
                                                {
                                                    textWriter.WriteStartElement("parent_value", ""); textWriter.WriteString(intThisDistrictID.ToString()); textWriter.WriteEndElement();
                                                    textWriter.WriteStartElement("default", ""); textWriter.WriteString(""); textWriter.WriteEndElement();

                                                    textWriter.WriteStartElement("list", ""); textWriter.WriteString("0");  // Dummy picklist item for none //
                                                    {
                                                        textWriter.WriteStartElement("desc", ""); textWriter.WriteString("--None--"); textWriter.WriteEndElement();
                                                    }
                                                    textWriter.WriteEndElement();  // End of dummy picklist item for none //

                                                    textWriter.WriteStartElement("list", ""); textWriter.WriteString(view4.GetRowCellValue(k, "LocalityID").ToString());
                                                    {
                                                        textWriter.WriteStartElement("desc", ""); textWriter.WriteString(view4.GetRowCellValue(k, "LocalityName").ToString()); textWriter.WriteEndElement();
                                                    }
                                                    textWriter.WriteEndElement();  // End of list //
                                                }
                                                intLastDistrictID = intThisDistrictID;
                                            }
                                            else
                                            {
                                                textWriter.WriteStartElement("list", ""); textWriter.WriteString(view4.GetRowCellValue(k, "LocalityID").ToString());
                                                {
                                                    textWriter.WriteStartElement("desc", ""); textWriter.WriteString(view4.GetRowCellValue(k, "LocalityName").ToString()); textWriter.WriteEndElement();
                                                }
                                                textWriter.WriteEndElement();  // End of list //
                                            }
                                        }
                                    }
                                    if (intLastDistrictID > 0) textWriter.WriteEndElement(); // Write End Element for last group processed //
                                    break;
                                case "intdistrictid":
                                    textWriter.WriteStartElement("list", ""); textWriter.WriteString("0");  // Dummy picklist item for none //
                                    {
                                        textWriter.WriteStartElement("desc", ""); textWriter.WriteString("--None--"); textWriter.WriteEndElement();
                                    }
                                    textWriter.WriteEndElement();  // End of dummy picklist item for none //

                                    intLastDistrictID = 0;
                                    for (int k = 0; k < view4.DataRowCount; k++)
                                    {
                                        if (Convert.ToInt32(view4.GetRowCellValue(k, "CheckMarkSelection")) == 1 || popupContainerEdit1.EditValue.ToString() == "All Localities [No Filter]")
                                        {
                                            intThisDistrictID = Convert.ToInt32(view4.GetRowCellValue(k, "DistrictID"));
                                            if (intThisDistrictID != intLastDistrictID)
                                            {
                                                textWriter.WriteStartElement("list", ""); textWriter.WriteString(view4.GetRowCellValue(k, "DistrictID").ToString());
                                                {
                                                    textWriter.WriteStartElement("desc", ""); textWriter.WriteString(view4.GetRowCellValue(k, "DistrictName").ToString()); textWriter.WriteEndElement();
                                                }
                                                textWriter.WriteEndElement();  // End of list //
                                                intLastDistrictID = intThisDistrictID;
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    if (strEditType == "list")
                                    {
                                        if (strColumnName != "intstatus")  // Add dummy --None-- picklist item if not the Tree Status field //
                                        {
                                            textWriter.WriteStartElement("list", ""); textWriter.WriteString("0");  // Dummy picklist item for none //
                                            {
                                                textWriter.WriteStartElement("desc", ""); textWriter.WriteString("--None--"); textWriter.WriteEndElement();
                                            }
                                            textWriter.WriteEndElement();  // End of dummy picklist item for none //
                                        }

                                        view5.ActiveFilter.NonColumnFilter = "[HeaderID] = " + view3.GetRowCellValue(i, "PickList").ToString();

                                        for (int k = 0; k < view5.DataRowCount; k++)
                                        {
                                            if (Convert.ToInt32(view5.GetRowCellValue(k, "CheckMarkSelection")) == 1)
                                            {
                                                textWriter.WriteStartElement("list", ""); textWriter.WriteString(view5.GetRowCellValue(k, "ItemID").ToString());
                                                {
                                                    textWriter.WriteStartElement("desc", ""); textWriter.WriteString(view5.GetRowCellValue(k, "ItemDescription").ToString()); textWriter.WriteEndElement();
                                                }
                                                textWriter.WriteEndElement();  // End of list //
                                            }
                                        }
                                        view5.ActiveFilter.Clear();
                                    }
                                    break;
                            }

                        }
                        textWriter.WriteEndElement();  // End of field_def //

                        // Write Last Inspection Values if Required //
                        if (IncludeLastInspectionDetailsCheckEdit.Checked)
                        {
                            if (view3.GetRowCellValue(i, "TableName").ToString().ToLower() == "inspection" && view3.GetRowCellValue(i, "InternalColumnName").ToString().ToLower() != "inspectionphotograph")
                            {
                                textWriter.WriteStartElement("field_def", "");
                                {
                                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("Last_" + view3.GetRowCellValue(i, "InternalColumnName").ToString()); textWriter.WriteEndElement();
                                    strType2 = view3.GetRowCellValue(i, "ColumnType").ToString().ToLower();
                                    switch (strType2)
                                    {
                                        case "char":
                                            textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                            break;
                                        case "integer":
                                            textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                                            break;
                                        case "date":
                                            textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                            break;
                                        case "decimal":
                                            textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                            break;
                                        default:
                                            textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                            break;
                                    }
                                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Last " + view3.GetRowCellValue(i, "ColumnLabel").ToString()); textWriter.WriteEndElement();
                                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Last " + view3.GetRowCellValue(i, "ColumnLabel").ToString()); textWriter.WriteEndElement();
                                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString(Convert.ToString(Convert.ToInt32(view3.GetRowCellValue(i, "CheckMarkSelection")) * -1)); textWriter.WriteEndElement();
                                    if (view3.GetRowCellValue(i, "InternalColumnName").ToString().ToLower() == "strremarks_inspection")  // Allow this field to be editable so it is scrollable [changes made won't be re-imported] //
                                    {
                                        textWriter.WriteStartElement("editable", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                    }
                                    else
                                    {
                                        textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                    }
                                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString(Convert.ToString(Convert.ToInt32(view3.GetRowCellValue(i, "Searchable")) * -1)); textWriter.WriteEndElement();
                                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                    string strEditType = "";
                                    if (Convert.ToInt32(view3.GetRowCellValue(i, "PickList")) != 0)
                                    {
                                        strEditType = "list";
                                    }
                                    else
                                    {
                                        switch (strType2)
                                        {
                                            case "char":
                                                strEditType = "text";
                                                break;
                                            case "decimal":
                                            case "integer":
                                                strEditType = "number";
                                                break;
                                            case "date":
                                                strEditType = "date";
                                                break;
                                            default:
                                                strEditType = "";
                                                break;
                                        }
                                    }
                                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString(strEditType); textWriter.WriteEndElement();
                                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                                    if (strColumnName == "intstatus")  // Default to 1 [Actual] //
                                    {
                                        textWriter.WriteStartElement("default", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                    }
                                    else
                                    {
                                        textWriter.WriteStartElement("default", ""); textWriter.WriteEndElement();
                                    }
                                    switch (strType2)
                                    {
                                        case "char":
                                            textWriter.WriteStartElement("max", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                            break;
                                        case "decimal":
                                            textWriter.WriteStartElement("max", ""); textWriter.WriteEndElement();
                                            textWriter.WriteStartElement("decimals", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                            break;
                                        case "integer":
                                            textWriter.WriteStartElement("min", ""); textWriter.WriteEndElement();
                                            textWriter.WriteStartElement("max", ""); textWriter.WriteEndElement();
                                            break;
                                        case "date":
                                            textWriter.WriteStartElement("max", ""); textWriter.WriteString("8"); textWriter.WriteEndElement();
                                            break;
                                        default:
                                            textWriter.WriteStartElement("max", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                            break;
                                    }

                                    switch (strColumnName)
                                    {
                                        default:
                                            if (strEditType == "list")
                                            {
                                                if (strColumnName != "intstatus")  // Add dummy --None-- picklist item if not the Tree Status field //
                                                {
                                                    textWriter.WriteStartElement("list", ""); textWriter.WriteString("0");  // Dummy picklist item for none //
                                                    {
                                                        textWriter.WriteStartElement("desc", ""); textWriter.WriteString("--None--"); textWriter.WriteEndElement();
                                                    }
                                                    textWriter.WriteEndElement();  // End of dummy picklist item for none //
                                                }

                                                view5.ActiveFilter.NonColumnFilter = "[HeaderID] = " + view3.GetRowCellValue(i, "PickList").ToString();

                                                for (int k = 0; k < view5.DataRowCount; k++)
                                                {
                                                    if (Convert.ToInt32(view5.GetRowCellValue(k, "CheckMarkSelection")) == 1)
                                                    {
                                                        textWriter.WriteStartElement("list", ""); textWriter.WriteString(view5.GetRowCellValue(k, "ItemID").ToString());
                                                        {
                                                            textWriter.WriteStartElement("desc", ""); textWriter.WriteString(view5.GetRowCellValue(k, "ItemDescription").ToString()); textWriter.WriteEndElement();
                                                        }
                                                        textWriter.WriteEndElement();  // End of list //
                                                    }
                                                }
                                                view5.ActiveFilter.Clear();
                                            }
                                            break;
                                    }

                                }
                                textWriter.WriteEndElement();  // End of field_def //

                            }
                        }
                        // End of Write Last Inspection Values //
                    }
                }
                textWriter.WriteStartElement("auto_fields", "");
                {
                    //if (strFormType == "Point")  // *** REMOVED SO THAT CENTROID INFO GOES INTO POLYGONS\POLYLINES ***//
                    //{
                        textWriter.WriteStartElement("auto_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("intX"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("x"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // End of auto_field //

                        textWriter.WriteStartElement("auto_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("intY"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("y"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // End of auto_field //
                    //}

                    //textWriter.WriteStartElement("auto_field", "");
                    //{
                    //    textWriter.WriteStartElement("name", ""); textWriter.WriteString("strPolygonXY"); textWriter.WriteEndElement();
                    //    textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("perimeter"); textWriter.WriteEndElement();
                    //}
                    //textWriter.WriteEndElement();  // End of auto_field //

                    textWriter.WriteStartElement("auto_field", "");
                    {
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("ID"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("unique"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of auto_field //

                    textWriter.WriteStartElement("auto_field", "");
                    {
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("DECAREAHA"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("area"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of auto_field //

                    textWriter.WriteStartElement("auto_field", "");
                    {
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("dtInspectDate"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("CREATION_DATE"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of auto_field //

                    textWriter.WriteStartElement("auto_field", "");
                    {
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("TreePhotograph"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("photo"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of auto_field //

                    textWriter.WriteStartElement("auto_field", "");
                    {
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("InspectionPhotograph"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("photo"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of auto_field //


                    // Use Last Value Fields //
                    for (int i = 0; i < view3.DataRowCount; i++)
                    {
                        if (Convert.ToInt32(view3.GetRowCellValue(i, "UseLastRecord")) == 1)
                        {
                            textWriter.WriteStartElement("auto_field", "");
                            {
                                textWriter.WriteStartElement("name", ""); textWriter.WriteString(view3.GetRowCellValue(i, "InternalColumnName").ToString()); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("last_created"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();
                        }
                    }
                }
                textWriter.WriteEndElement();  // End of auto_fields //

                textWriter.WriteStartElement("edit_tracking", "");
                {
                    textWriter.WriteStartElement("id_field", ""); textWriter.WriteString("ID"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("datetime_field", ""); textWriter.WriteString("GBM_EDIT_TIME"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edittype_field", ""); textWriter.WriteString("GBM_EDIT_FLAG"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of edit_tracking //


                textWriter.WriteStartElement("printing", "");
                {
                    textWriter.WriteStartElement("exe", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("template", ""); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of printing //

                textWriter.WriteStartElement("grid", ""); textWriter.WriteEndElement();

                textWriter.WriteStartElement("group_defs", "");
                {
                    GridView view2 = (GridView)gridControl2.MainView;
                    //if (view2.ActiveFilter.Criteria != null) view2.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                    if (!ReferenceEquals(view2.ActiveFilter.Criteria, null)) view2.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                    if (view2.DataRowCount <= 1)  // If only 1 Then it's the "None" group left so there is no grouping //
                    {
                        textWriter.WriteStartElement("use_groups", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    }
                    else
                    {
                        textWriter.WriteStartElement("use_groups", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                        for (int i = 0; i < view2.DataRowCount; i++)
                        {
                            //view3.ActiveFilter.NonColumnFilter = "[GroupName] = '" + view2.GetRowCellValue(i, "GroupName").ToString() + "'";
                            view3.ActiveFilter.NonColumnFilter = "[GroupName] = '" + view2.GetRowCellValue(i, "GroupName").ToString() + "' and [CheckMarkSelection] = 1";
                            if (view3.DataRowCount > 0)  // Group has contents so write it //
                            {
                                textWriter.WriteStartElement("group_def", "");
                                {
                                    textWriter.WriteStartElement("name", ""); textWriter.WriteString(view2.GetRowCellValue(i, "GroupName").ToString()); textWriter.WriteEndElement();
                                    for (int j = 0; j < view3.DataRowCount; j++)
                                    {
                                        textWriter.WriteStartElement("field", ""); textWriter.WriteString(view3.GetRowCellValue(j, "InternalColumnName").ToString()); textWriter.WriteEndElement();
                                        
                                        // Write Last Inspection Values if Required //
                                        if (IncludeLastInspectionDetailsCheckEdit.Checked)
                                        {
                                            if (view3.GetRowCellValue(i, "TableName") != null)
                                            {
                                                if (view3.GetRowCellValue(i, "TableName").ToString().ToLower() == "inspection" && view3.GetRowCellValue(i, "InternalColumnName").ToString().ToLower() != "inspectionphotograph")
                                                {
                                                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("Last_" + view3.GetRowCellValue(j, "InternalColumnName").ToString()); textWriter.WriteEndElement();
                                                }
                                            }
                                        }
                                        // End of Write Last Inspection Values //
                                    }
                                }
                                textWriter.WriteEndElement();  // End of group_def //
                            }
                            view3.ActiveFilter.Clear();
                        }
                    }
                }
                textWriter.WriteEndElement();  // End of group_defs //

                textWriter.WriteStartElement("listing", ""); textWriter.WriteEndElement();
            }
            textWriter.WriteEndElement();  // End of object_def //
        }

        private void ExportDataWorkOrder(string strFormType, XmlTextWriter textWriter)
        {
            textWriter.WriteStartElement("object_def");  // Write second element - Not closed so subsequent items are indented inside it //
            {
                textWriter.WriteStartElement("date_modified", ""); textWriter.WriteString(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")); textWriter.WriteEndElement();

                switch (strFormType)
                {
                    case "Point":
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("WorkOrder Tree");
                        {
                            textWriter.WriteStartElement("plural", ""); textWriter.WriteString("WorkOrder Tree"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();
                        break;
                    case "Polygon":
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("WorkOrder Group");
                        {
                            textWriter.WriteStartElement("plural", ""); textWriter.WriteString("WorkOrder Group"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();
                        break;
                    default:  // Polyline //
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("WorkOrder Line");
                        {
                            textWriter.WriteStartElement("plural", ""); textWriter.WriteString("WorkOrder Line"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();
                        break;
                }

                textWriter.WriteStartElement("description", ""); textWriter.WriteString("Test"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("can_edit", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("can_draw", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // No creation of new map objects //
                textWriter.WriteStartElement("can_gps", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("auto_form", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();

                int intStyleField = 0;
                if (glueStyleField.EditValue != null) intStyleField = Convert.ToInt32(glueStyleField.EditValue);
                if (intStyleField != 0)
                {
                    textWriter.WriteStartElement("use_value_styles", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                }
                else
                {
                    textWriter.WriteStartElement("use_value_styles", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                }
                textWriter.WriteStartElement("use_find", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_attributes", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_parented_lists", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_edit_tracking", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_printing", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();

                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                string strPath = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesExportToGBM_Tab_Folder"));
                if (strPath == "")
                {
                    textWriter.WriteEndElement();
                    textWriter.WriteEndDocument();  // Ends the document //
                    textWriter.Close();  // close writer //
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Export, no Default Save Folder has been specified for the export.\n\nUse the System Configuration screen to adjust before trying again.", "Export", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!strPath.EndsWith("\\")) strPath += "\\";

                textWriter.WriteStartElement("spatial_def", "");
                {
                    textWriter.WriteStartElement("file", ""); textWriter.WriteString(strPath + "WorkOrder.tab"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("layer", ""); textWriter.WriteString("WorkOrder"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("ObjectType");
                    {
                        switch (strFormType)
                        {
                            case "Point":
                                textWriter.WriteStartElement("value", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                                break;
                            case "Polygon":
                                textWriter.WriteStartElement("value", ""); textWriter.WriteString("4"); textWriter.WriteEndElement();
                                break;
                            default:  // Polyline //
                                textWriter.WriteStartElement("value", ""); textWriter.WriteString("5"); textWriter.WriteEndElement();
                                break;
                        }
                    }
                    textWriter.WriteEndElement();
                    textWriter.WriteStartElement("list_field", ""); textWriter.WriteString("strDescription"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("list_field", ""); textWriter.WriteEndElement();
                    if (strFormType == "Point")
                    {
                        textWriter.WriteStartElement("spatial_type", ""); textWriter.WriteString("point"); textWriter.WriteEndElement();
                    }
                    else if (strFormType == "Polygon")
                    {
                        textWriter.WriteStartElement("spatial_type", ""); textWriter.WriteString("region"); textWriter.WriteEndElement();
                    }
                    else  // Polyline //
                    {
                        textWriter.WriteStartElement("spatial_type", ""); textWriter.WriteString("pline"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteStartElement("file_fields", "");
                    {
                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strDescription"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("200"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strTreeRef"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("50"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("intActionID"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();  // Integer //
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strJobNumber"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strActionDescription"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("dtDueDate"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();  // Date //
                        }
                        textWriter.WriteEndElement();  // end of file field //
                        
                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("dtDoneDate"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();  // Date //
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strRemarks"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("250"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strUser1"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("10"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strUser2"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("10"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strUser3"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("10"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strWorkOrder"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("50"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strPriority"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strSpecies"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strSize"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strNearestHouse"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //
                                             
                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strActionBy"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("150"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //
                        
                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("strSupervisor"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();  // Char //
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("150"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of file field //

                        textWriter.WriteStartElement("file_field", "");  // Add in Dummy field //
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("ObjectType"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of Dummy field //
                    }
                    textWriter.WriteEndElement();  // End of File fields //

                    textWriter.WriteStartElement("style", "");
                    {
                        if (strFormType == "Point")  // Similar code fired later for Polygons //
                        {
                            textWriter.WriteStartElement("symbol", "");
                            {
                                textWriter.WriteStartElement("font", ""); textWriter.WriteString("Map Symbols"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("size", ""); textWriter.WriteString("9.75"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bold", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("italic", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("underline", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("strikethrough", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("weight", ""); textWriter.WriteString("400"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("charset", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bitmap_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bitmap_name", ""); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bitmap_override_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bitmap_size", ""); textWriter.WriteString("14"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("bitmap_transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("character", ""); textWriter.WriteString("33"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString("16777215"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("color", ""); textWriter.WriteString("5672960"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("halo", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("opaque", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("rotation", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("shadow", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("vector_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("vector_size", ""); textWriter.WriteString("14"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // end of symbol //
                        }
                        else if (strFormType == "Polygon")
                        {
                            textWriter.WriteStartElement("region", "");
                            {
                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString("16777215"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_style", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_width", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_width_unit", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("color", ""); textWriter.WriteString("16734240"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("pattern", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // end of region //
                        }
                        else // Polyline //
                        {
                            textWriter.WriteStartElement("pline", "");
                            {
                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString("16777215"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_style", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_width", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("border_width_unit", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("color", ""); textWriter.WriteString("16734240"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("pattern", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();  // end of region //
                        }
                    }
                    textWriter.WriteEndElement();  // End of style //
                    textWriter.WriteStartElement("db", "");
                    {
                        textWriter.WriteStartElement("db_fields_sql", ""); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("db_con_name", ""); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("db_con_file", ""); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("db_con", ""); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of db //

                }
                textWriter.WriteEndElement();  // End of spatial_def //
/*
                // ********** Style Definitions Section ********** //
                int intRed = 0;
                int intGreen = 0;
                int intBlue = 0;
                int intCalculatedColour = 0;
                Color tempColour;

                if (intStyleField != 0)
                {
                    string strStyleFieldName = "";
                    GridView view = (GridView)gridControl3.MainView;
                    int intFoundRow = view.LocateByValue(0, view.Columns["ColumnID"], intStyleField);
                    if (intFoundRow >= 0) strStyleFieldName = Convert.ToString(view.GetRowCellValue(intFoundRow, "InternalColumnName"));
                    if (strStyleFieldName != "")
                    {
                        textWriter.WriteStartElement("value_style_defs", "");
                        {
                            textWriter.WriteStartElement("field", ""); textWriter.WriteString(strStyleFieldName); textWriter.WriteEndElement();

                            GridView viewStyles = (GridView)gridControl6.MainView;
                            for (int i = 0; i < viewStyles.DataRowCount; i++)
                            {
                                textWriter.WriteStartElement("value_style_def", "");
                                {
                                    textWriter.WriteStartElement("value", ""); textWriter.WriteString(Convert.ToString(viewStyles.GetRowCellValue(i, "ItemID"))); textWriter.WriteEndElement();
                                    textWriter.WriteStartElement("style", "");
                                    {
                                        if (strFormType == "Point")
                                        {
                                            textWriter.WriteStartElement("symbol", "");
                                            {
                                                textWriter.WriteStartElement("font", ""); textWriter.WriteString("Map Symbols"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("size", ""); textWriter.WriteString(Convert.ToString(viewStyles.GetRowCellValue(i, "Size"))); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bold", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("italic", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("underline", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("strikethrough", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("weight", ""); textWriter.WriteString("400"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("charset", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bitmap_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bitmap_name", ""); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bitmap_override_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bitmap_size", ""); textWriter.WriteString("14"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("bitmap_transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("character", ""); textWriter.WriteString(Convert.ToString(viewStyles.GetRowCellValue(i, "Symbol"))); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString("16777215"); textWriter.WriteEndElement();

                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "SymbolColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                textWriter.WriteStartElement("halo", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("opaque", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("rotation", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("shadow", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("vector_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("vector_size", ""); textWriter.WriteString("14"); textWriter.WriteEndElement();
                                            }
                                            textWriter.WriteEndElement();  // End of symbol //  
                                        }
                                        else if (strFormType == "Polygon")
                                        {
                                            textWriter.WriteStartElement("region", "");
                                            {
                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonFillPatternColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonLineColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("border_color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                textWriter.WriteStartElement("border_style", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("border_width", ""); textWriter.WriteString(viewStyles.GetRowCellValue(i, "PolygonLineWidth").ToString()); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("border_width_unit", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();

                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonFillColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                textWriter.WriteStartElement("pattern", ""); textWriter.WriteString(viewStyles.GetRowCellValue(i, "PolygonFillPattern").ToString()); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                            }
                                            textWriter.WriteEndElement();  // End of region //  
                                        }
                                        else  // Polyline //
                                        {
                                            textWriter.WriteStartElement("pline", "");
                                            {
                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonFillPatternColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("back_color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonLineColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("border_color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                textWriter.WriteStartElement("border_style", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("border_width", ""); textWriter.WriteString(viewStyles.GetRowCellValue(i, "PolygonLineWidth").ToString()); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("border_width_unit", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();

                                                tempColour = Color.FromArgb(Convert.ToInt32(viewStyles.GetRowCellValue(i, "PolygonFillColour")));
                                                intRed = tempColour.R;
                                                intGreen = tempColour.G;
                                                intBlue = tempColour.B;
                                                //intCalculatedColour = (256 * 256 * intRed) + (256 * intGreen) + (intBlue); // This is the correct formula for conversion but GBM uses a reversed formula! //
                                                intCalculatedColour = (intRed) + (256 * intGreen) + (256 * 256 * intBlue);
                                                textWriter.WriteStartElement("color", ""); textWriter.WriteString(intCalculatedColour.ToString()); textWriter.WriteEndElement();

                                                textWriter.WriteStartElement("pattern", ""); textWriter.WriteString(viewStyles.GetRowCellValue(i, "PolygonFillPattern").ToString()); textWriter.WriteEndElement();
                                                textWriter.WriteStartElement("transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                            }
                                            textWriter.WriteEndElement();  // End of region //  
                                        }
                                    }
                                    textWriter.WriteEndElement();  // End of style //
                                }
                                textWriter.WriteEndElement();  // End of value_style_def //
                            }
                        }
                        textWriter.WriteEndElement();  // End of value_style_defs //
                    }
                }*/

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strDescription"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Description"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Description"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("200"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("intActionID"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Action ID"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Action ID"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("number"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("min", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strJobNumber"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Job Number"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Job Number"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strActionDescription"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Action"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Action"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strTreeRef"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Tree Reference"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Tree Reference"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("200"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("dtDueDate"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Due Date"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Due Date"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("date"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("8"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("dtDoneDate"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Done Date"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Done Date"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("date"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("8"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strRemarks"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Remarks"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Remarks"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("250"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strUser1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("User Defined 1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("User Defined 1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("10"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strUser2"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("User Defined 2"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("User Defined 2"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("10"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strUser3"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("User Defined 3"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("User Defined 3"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("10"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strWorkOrder"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Work Order"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Work Order"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strPriority"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Priority"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Priority"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strSpecies"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Species"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Species"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strSize"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Size"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Size"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strNearestHouse"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Nearest House"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Nearest House"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("200"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strActionBy"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Carried Out By"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Carried Out By"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("150"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("strSupervisor"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Supervisor"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Supervisor"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("100"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //

                textWriter.WriteStartElement("field_def", "");  // Dummy field to control if Point or Polygon //
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("ObjectType"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("Object Type:"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Object Type:"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("number"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    if (strFormType == "Point")
                    {
                        textWriter.WriteStartElement("default", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    }
                    else if (strFormType == "Polygon")
                    {
                        textWriter.WriteStartElement("default", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                    }
                    else  // Polyline //
                    {
                        textWriter.WriteStartElement("default", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteStartElement("min", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();

                }
                textWriter.WriteEndElement();  // End of field_def //


                /*
                GridView view3 = (GridView)gridControl3.MainView;
                GridView view4 = (GridView)gridControl4.MainView;
                //if (view4.ActiveFilter.Criteria != null) view4.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                if (!ReferenceEquals(view4.ActiveFilter.Criteria, null)) view4.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                GridView view5 = (GridView)gridControl5.MainView;
                //if (view5.ActiveFilter.Criteria != null) view5.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                if (!ReferenceEquals(view5.ActiveFilter.Criteria, null)) view5.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
                string strType2;
                int intLastDistrictID = 0;
                int intThisDistrictID = 0;

                for (int i = 0; i < view3.DataRowCount; i++)
                {
                    string strColumnName = view3.GetRowCellValue(i, "InternalColumnName").ToString().ToLower();
                    if (Convert.ToInt32(view3.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        textWriter.WriteStartElement("field_def", "");
                        {
                            textWriter.WriteStartElement("field", ""); textWriter.WriteString(view3.GetRowCellValue(i, "InternalColumnName").ToString()); textWriter.WriteEndElement();
                            strType2 = view3.GetRowCellValue(i, "ColumnType").ToString().ToLower();
                            switch (strType2)
                            {
                                case "char":
                                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                    break;
                                case "integer":
                                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("3"); textWriter.WriteEndElement();
                                    break;
                                case "date":
                                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                                    break;
                                case "decimal":
                                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                                    break;
                                default:
                                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                    break;
                            }
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLabel").ToString()); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("prompt", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLabel").ToString()); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("visible", ""); textWriter.WriteString(Convert.ToString(Convert.ToInt32(view3.GetRowCellValue(i, "CheckMarkSelection")) * -1)); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("editable", ""); textWriter.WriteString(Convert.ToString(Convert.ToInt32(view3.GetRowCellValue(i, "Editable")) * -1)); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("searchable", ""); textWriter.WriteString(Convert.ToString(Convert.ToInt32(view3.GetRowCellValue(i, "Searchable")) * -1)); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString(Convert.ToString(Convert.ToInt32(view3.GetRowCellValue(i, "Manditory")) * -1)); textWriter.WriteEndElement();
                            string strEditType = "";
                            if (Convert.ToInt32(view3.GetRowCellValue(i, "PickList")) != 0)
                            {
                                strEditType = "list";
                            }
                            else
                            {
                                switch (strType2)
                                {
                                    case "char":
                                        strEditType = "text";
                                        break;
                                    case "decimal":
                                    case "integer":
                                        strEditType = "number";
                                        break;
                                    case "date":
                                        strEditType = "date";
                                        break;
                                    default:
                                        strEditType = "";
                                        break;
                                }
                            }
                            textWriter.WriteStartElement("edit", ""); textWriter.WriteString(strEditType); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                            if (strColumnName == "intstatus")  // Default to 1 [Actual] //
                            {
                                textWriter.WriteStartElement("default", ""); textWriter.WriteString("1"); textWriter.WriteEndElement();
                            }
                            else
                            {
                                textWriter.WriteStartElement("default", ""); textWriter.WriteEndElement();
                            }
                            switch (strType2)
                            {
                                case "char":
                                    textWriter.WriteStartElement("max", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                    break;
                                case "decimal":
                                    textWriter.WriteStartElement("max", ""); textWriter.WriteEndElement();
                                    textWriter.WriteStartElement("decimals", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                    break;
                                case "integer":
                                    textWriter.WriteStartElement("min", ""); textWriter.WriteEndElement();
                                    textWriter.WriteStartElement("max", ""); textWriter.WriteEndElement();
                                    break;
                                case "date":
                                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("8"); textWriter.WriteEndElement();
                                    break;
                                default:
                                    textWriter.WriteStartElement("max", ""); textWriter.WriteString(view3.GetRowCellValue(i, "ColumnLength").ToString()); textWriter.WriteEndElement();
                                    break;
                            }

                            switch (strColumnName)
                            {
                                case "intlocalityid":
                                    textWriter.WriteStartElement("parent_field", ""); textWriter.WriteString("intDistrictID"); textWriter.WriteEndElement();

                                    textWriter.WriteStartElement("parented_list", "");  // Dummy parent list for 0 [-- None --] district //
                                    {
                                        textWriter.WriteStartElement("parent_value", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                                        textWriter.WriteStartElement("default", ""); textWriter.WriteString(""); textWriter.WriteEndElement();

                                        textWriter.WriteStartElement("list", ""); textWriter.WriteString("0");  // Dummy picklist item for none //
                                        {
                                            textWriter.WriteStartElement("desc", ""); textWriter.WriteString("--None--"); textWriter.WriteEndElement();
                                        }
                                        textWriter.WriteEndElement();  // End of dummy picklist item for none //
                                    }
                                    textWriter.WriteEndElement();  // End of Dummy parented list //
                                    intLastDistrictID = 0;
                                    for (int k = 0; k < view4.DataRowCount; k++)
                                    {
                                        if (Convert.ToInt32(view4.GetRowCellValue(k, "CheckMarkSelection")) == 1 || popupContainerEdit1.EditValue.ToString() == "All Localities [No Filter]")
                                        {
                                            intThisDistrictID = Convert.ToInt32(view4.GetRowCellValue(k, "DistrictID"));
                                            if (intThisDistrictID != intLastDistrictID)
                                            {
                                                if (intLastDistrictID > 0) textWriter.WriteEndElement(); // Write End Element for last group processed //

                                                textWriter.WriteStartElement("parented_list", "");
                                                {
                                                    textWriter.WriteStartElement("parent_value", ""); textWriter.WriteString(intThisDistrictID.ToString()); textWriter.WriteEndElement();
                                                    textWriter.WriteStartElement("default", ""); textWriter.WriteString(""); textWriter.WriteEndElement();

                                                    textWriter.WriteStartElement("list", ""); textWriter.WriteString("0");  // Dummy picklist item for none //
                                                    {
                                                        textWriter.WriteStartElement("desc", ""); textWriter.WriteString("--None--"); textWriter.WriteEndElement();
                                                    }
                                                    textWriter.WriteEndElement();  // End of dummy picklist item for none //

                                                    textWriter.WriteStartElement("list", ""); textWriter.WriteString(view4.GetRowCellValue(k, "LocalityID").ToString());
                                                    {
                                                        textWriter.WriteStartElement("desc", ""); textWriter.WriteString(view4.GetRowCellValue(k, "LocalityName").ToString()); textWriter.WriteEndElement();
                                                    }
                                                    textWriter.WriteEndElement();  // End of list //
                                                }
                                                intLastDistrictID = intThisDistrictID;
                                            }
                                            else
                                            {
                                                textWriter.WriteStartElement("list", ""); textWriter.WriteString(view4.GetRowCellValue(k, "LocalityID").ToString());
                                                {
                                                    textWriter.WriteStartElement("desc", ""); textWriter.WriteString(view4.GetRowCellValue(k, "LocalityName").ToString()); textWriter.WriteEndElement();
                                                }
                                                textWriter.WriteEndElement();  // End of list //
                                            }
                                        }
                                    }
                                    if (intLastDistrictID > 0) textWriter.WriteEndElement(); // Write End Element for last group processed //
                                    break;
                                case "intdistrictid":
                                    textWriter.WriteStartElement("list", ""); textWriter.WriteString("0");  // Dummy picklist item for none //
                                    {
                                        textWriter.WriteStartElement("desc", ""); textWriter.WriteString("--None--"); textWriter.WriteEndElement();
                                    }
                                    textWriter.WriteEndElement();  // End of dummy picklist item for none //

                                    intLastDistrictID = 0;
                                    for (int k = 0; k < view4.DataRowCount; k++)
                                    {
                                        if (Convert.ToInt32(view4.GetRowCellValue(k, "CheckMarkSelection")) == 1 || popupContainerEdit1.EditValue.ToString() == "All Localities [No Filter]")
                                        {
                                            intThisDistrictID = Convert.ToInt32(view4.GetRowCellValue(k, "DistrictID"));
                                            if (intThisDistrictID != intLastDistrictID)
                                            {
                                                textWriter.WriteStartElement("list", ""); textWriter.WriteString(view4.GetRowCellValue(k, "DistrictID").ToString());
                                                {
                                                    textWriter.WriteStartElement("desc", ""); textWriter.WriteString(view4.GetRowCellValue(k, "DistrictName").ToString()); textWriter.WriteEndElement();
                                                }
                                                textWriter.WriteEndElement();  // End of list //
                                                intLastDistrictID = intThisDistrictID;
                                            }
                                        }
                                    }
                                    break;
                                default:
                                    if (strEditType == "list")
                                    {
                                        if (strColumnName != "intstatus")  // Add dummy --None-- picklist item if not the Tree Status field //
                                        {
                                            textWriter.WriteStartElement("list", ""); textWriter.WriteString("0");  // Dummy picklist item for none //
                                            {
                                                textWriter.WriteStartElement("desc", ""); textWriter.WriteString("--None--"); textWriter.WriteEndElement();
                                            }
                                            textWriter.WriteEndElement();  // End of dummy picklist item for none //
                                        }

                                        view5.ActiveFilter.NonColumnFilter = "[HeaderID] = " + view3.GetRowCellValue(i, "PickList").ToString();

                                        for (int k = 0; k < view5.DataRowCount; k++)
                                        {
                                            if (Convert.ToInt32(view5.GetRowCellValue(k, "CheckMarkSelection")) == 1)
                                            {
                                                textWriter.WriteStartElement("list", ""); textWriter.WriteString(view5.GetRowCellValue(k, "ItemID").ToString());
                                                {
                                                    textWriter.WriteStartElement("desc", ""); textWriter.WriteString(view5.GetRowCellValue(k, "ItemDescription").ToString()); textWriter.WriteEndElement();
                                                }
                                                textWriter.WriteEndElement();  // End of list //
                                            }
                                        }
                                        view5.ActiveFilter.Clear();
                                    }
                                    break;
                            }

                        }
                        textWriter.WriteEndElement();  // End of field_def //
                    }
                }*/

                /*
                textWriter.WriteStartElement("auto_fields", "");
                {
                    textWriter.WriteStartElement("auto_field", "");
                    {
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("intX"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("x"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of auto_field //

                    textWriter.WriteStartElement("auto_field", "");
                    {
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("intY"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("y"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of auto_field //

                    //textWriter.WriteStartElement("auto_field", "");
                    //{
                    //    textWriter.WriteStartElement("name", ""); textWriter.WriteString("strPolygonXY"); textWriter.WriteEndElement();
                    //    textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("perimeter"); textWriter.WriteEndElement();
                    //}
                    //textWriter.WriteEndElement();  // End of auto_field //

                    textWriter.WriteStartElement("auto_field", "");
                    {
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("ID"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("unique"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of auto_field //

                    textWriter.WriteStartElement("auto_field", "");
                    {
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("DECAREAHA"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("area"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of auto_field //

                    textWriter.WriteStartElement("auto_field", "");
                    {
                        textWriter.WriteStartElement("name", ""); textWriter.WriteString("dtInspectDate"); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("CREATION_DATE"); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of auto_field //

                    // Use Last Value Fields //
                    for (int i = 0; i < view3.DataRowCount; i++)
                    {
                        if (Convert.ToInt32(view3.GetRowCellValue(i, "UseLastRecord")) == 1)
                        {
                            textWriter.WriteStartElement("auto_field", "");
                            {
                                textWriter.WriteStartElement("name", ""); textWriter.WriteString(view3.GetRowCellValue(i, "InternalColumnName").ToString()); textWriter.WriteEndElement();
                                textWriter.WriteStartElement("auto_value", ""); textWriter.WriteString("last_created"); textWriter.WriteEndElement();
                            }
                            textWriter.WriteEndElement();
                        }
                    }
                }
                textWriter.WriteEndElement();  // End of auto_fields //

                textWriter.WriteStartElement("edit_tracking", "");
                {
                    textWriter.WriteStartElement("id_field", ""); textWriter.WriteString("ID"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("datetime_field", ""); textWriter.WriteString("GBM_EDIT_TIME"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edittype_field", ""); textWriter.WriteString("GBM_EDIT_FLAG"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of edit_tracking //
                */

                textWriter.WriteStartElement("printing", "");
                {
                    textWriter.WriteStartElement("exe", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("template", ""); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of printing //

                textWriter.WriteStartElement("grid", ""); textWriter.WriteEndElement();
                
                textWriter.WriteStartElement("group_defs", "");
                {

                    textWriter.WriteStartElement("use_groups", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of group_defs //

                textWriter.WriteStartElement("listing", ""); textWriter.WriteEndElement();
            }
            textWriter.WriteEndElement();  // End of object_def //
        }

        private void AddGazetteerFormToXML(XmlTextWriter textWriter)
        {
            // Get MID / MIF Output Location [Gazetteer file is stored here] //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strMidMifFileLocation = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesExportToGBM_Mid_Mif_Folder"));

            textWriter.WriteStartElement("object_def");  // Write third element - Not closed so subsequent items are indented inside it //
            {
                textWriter.WriteStartElement("date_modified", ""); textWriter.WriteString(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")); textWriter.WriteEndElement();

                textWriter.WriteStartElement("name", ""); textWriter.WriteString("Gazetteer");
                {
                    textWriter.WriteStartElement("plural", ""); textWriter.WriteString("Gazetteers"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();

                textWriter.WriteStartElement("description", ""); textWriter.WriteString("Street Finder"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("can_edit", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("can_draw", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("can_gps", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("auto_form", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_value_styles", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_find", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_attributes", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_parented_lists", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_edit_tracking", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                textWriter.WriteStartElement("use_printing", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();

                textWriter.WriteStartElement("spatial_def", "");
                {
                    textWriter.WriteStartElement("file", ""); textWriter.WriteString(strMidMifFileLocation + @"\Gazetteer.tab"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("layer", ""); textWriter.WriteString("gazetteer"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("list_field", ""); textWriter.WriteString("LocalityName"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("list_field", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("spatial_type", ""); textWriter.WriteString("point"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("file_fields", "");
                    {
                        textWriter.WriteStartElement("file_field", "");
                        {
                            textWriter.WriteStartElement("name", ""); textWriter.WriteString("LocalityName"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("width", ""); textWriter.WriteString("50"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // End of file_field //
                    }
                    textWriter.WriteEndElement(); // End of file_fields //

                    textWriter.WriteStartElement("style", "");
                    {
                        textWriter.WriteStartElement("symbol", "");
                        {
                            textWriter.WriteStartElement("font", ""); textWriter.WriteString("Map Symbols"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("size", ""); textWriter.WriteString("14.25"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("bold", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("italic", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("underline", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("strikethrough", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("weight", ""); textWriter.WriteString("400"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("charset", ""); textWriter.WriteString("2"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("bitmap_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("bitmap_name", ""); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("bitmap_override_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("bitmap_size", ""); textWriter.WriteString("14"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("bitmap_transparent", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("character", ""); textWriter.WriteString("55"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("back_color", ""); textWriter.WriteString("16777215"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("halo", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("opaque", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("rotation", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("shadow", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("vector_color", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                            textWriter.WriteStartElement("vector_size", ""); textWriter.WriteString("14"); textWriter.WriteEndElement();
                        }
                        textWriter.WriteEndElement();  // end of symbol //
                    }
                    textWriter.WriteEndElement();  // End of style //
                    
                    textWriter.WriteStartElement("db", "");
                    {
                        textWriter.WriteStartElement("db_fields_sql", ""); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("db_con_name", ""); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("db_con_file", ""); textWriter.WriteEndElement();
                        textWriter.WriteStartElement("db_con", ""); textWriter.WriteEndElement();
                    }
                    textWriter.WriteEndElement();  // End of db //
                }
                textWriter.WriteEndElement();  // End of spatial_def //

                textWriter.WriteStartElement("field_def", "");
                {
                    textWriter.WriteStartElement("field", ""); textWriter.WriteString("LocalityName"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("field_type", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("name", ""); textWriter.WriteString("LocalityName"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("prompt", ""); textWriter.WriteString("Street:"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("visible", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("editable", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("searchable", ""); textWriter.WriteString("-1"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("mandatory", ""); textWriter.WriteString("0"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit", ""); textWriter.WriteString("text"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edit_ex", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("default", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("max", ""); textWriter.WriteString("50"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of field_def //
                
                textWriter.WriteStartElement("auto_fields", ""); textWriter.WriteEndElement();

                textWriter.WriteStartElement("edit_tracking", "");
                {
                    textWriter.WriteStartElement("id_field", ""); textWriter.WriteString("ID"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("datetime_field", ""); textWriter.WriteString("GBM_EDIT_TIME"); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("edittype_field", ""); textWriter.WriteString("GBM_EDIT_FLAG"); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of edit_tracking //

                textWriter.WriteStartElement("printing", "");
                {
                    textWriter.WriteStartElement("exe", ""); textWriter.WriteEndElement();
                    textWriter.WriteStartElement("template", ""); textWriter.WriteEndElement();
                }
                textWriter.WriteEndElement();  // End of printing //

                textWriter.WriteStartElement("grid", ""); textWriter.WriteEndElement();
                textWriter.WriteStartElement("group_defs", ""); textWriter.WriteEndElement();
                textWriter.WriteStartElement("listing", ""); textWriter.WriteEndElement();     
            }
            textWriter.WriteEndElement();  // End of object_def //
        }

        private int CreateWorkOrderMifFile(DataTable dt, string strFilePath, ref double doubleTotalX, ref double doubleTotalY, ref int intValuesCounted)    // Use "ref" to return values back to original variables [pass by reference] //
        {
            double doubleMinX = (double)999999999999.999;
            double doubleMinY = (double)999999999999.999;
            double doubleMaxX = (double)(-999999999999.999);
            double doubleMaxY = (double)(-999999999999.999);

            System.Text.StringBuilder sbText = new System.Text.StringBuilder("");

            sbText.Append("  STRDESCRIPTION Char(200)\r\n");
            sbText.Append("  INTACTIONID Integer\r\n");
            sbText.Append("  STRJOBNUMBER Char(100)\r\n");
            sbText.Append("  STRACTIONDESCRIPTION Char(100)\r\n");
            sbText.Append("  STRTREEREF Char(50)\r\n");
            sbText.Append("  DTDUEDATE Date\r\n");
            sbText.Append("  DTDONEDATE Date\r\n");
            sbText.Append("  STRREMARKS Char(250)\r\n");
            sbText.Append("  STRUSER1 Char(10)\r\n");
            sbText.Append("  STRUSER2 Char(10)\r\n");
            sbText.Append("  STRUSER3 Char(10)\r\n");
            sbText.Append("  STRWORKORDER Char(50)\r\n");
            sbText.Append("  STRPRIORITY Char(100)\r\n");
            sbText.Append("  STRSPECIES Char(100)\r\n");
            sbText.Append("  STRSIZE Char(100)\r\n");
            sbText.Append("  STRNEARESTHOUSE Char(100)\r\n");
            sbText.Append("  STRACTIONBY Char(150)\r\n");
            sbText.Append("  STRSUPERVISOR Char(150)\r\n");
            sbText.Append("  ObjectType Integer\r\n");
            sbText.Append("  intX Integer\r\n");
            sbText.Append("  intY Integer\r\n");
            sbText.Append("  strPolygonXY Char(254)\r\n");

            /*
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    sbText.Append("  " + view.GetRowCellValue(i, "InternalColumnName").ToString().ToUpper() + " " + System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(view.GetRowCellValue(i, "ColumnType").ToString()));  // Force First Letter of column type to uppercase //
                    switch (view.GetRowCellValue(i, "ColumnType").ToString().ToLower())
                    {
                        case "char":
                            sbText.Append("(" + view.GetRowCellValue(i, "ColumnLength").ToString() + ")");
                            break;
                        case "decimal":
                            sbText.Append("(18, " + view.GetRowCellValue(i, "ColumnLength").ToString() + ")");
                            break;
                        default:
                            break;
                    }
                    sbText.Append("\r\n");
                }
            }
            sbText.Append("  ObjectType Integer\r\n");  // Add dummy column to end //
            */
            sbText.Append("Data\r\n\r\n");

            // ***** Write Points and Polygons ***** //
            string[] strArray;
            char[] delimiters = new char[] { ';' };

            string strLocalitiesIncludedForAverageXY = gridLookUpEdit1.EditValue.ToString();
            if (strLocalitiesIncludedForAverageXY != null && strLocalitiesIncludedForAverageXY != "0") strLocalitiesIncludedForAverageXY = "," + strLocalitiesIncludedForAverageXY + ",";  // Wrap with commas to make comparisons more accurate //

            bool boolBoundsAlreadyCalculated = false;
            if (doubleTotalX > 0) boolBoundsAlreadyCalculated = true;

            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToInt32(dr["ObjectType"]) == 0 || Convert.ToInt32(dr["ObjectType"]) == 3)  // Point //
                {
                    if (Convert.ToDouble(dr["intX"]) < doubleMinX) doubleMinX = Convert.ToDouble(dr["intX"]);
                    if (Convert.ToDouble(dr["intY"]) < doubleMinY) doubleMinY = Convert.ToDouble(dr["intY"]);
                    if (Convert.ToDouble(dr["intX"]) > doubleMaxX) doubleMaxX = Convert.ToDouble(dr["intX"]);
                    if (Convert.ToDouble(dr["intY"]) > doubleMaxY) doubleMaxY = Convert.ToDouble(dr["intY"]);
                    sbText.Append("Point " + dr["intX"].ToString() + " " + dr["intY"].ToString() + "\r\n");
                    sbText.Append("    Symbol (34,255,10)\r\n");
                    if (strLocalitiesIncludedForAverageXY == "0" && !boolBoundsAlreadyCalculated)
                    {
                        doubleTotalX += Convert.ToDouble(dr["intX"]);
                        doubleTotalY += Convert.ToDouble(dr["intY"]);
                        intValuesCounted++;
                    }
                }
                else if (Convert.ToInt32(dr["ObjectType"]) == 1 || Convert.ToInt32(dr["ObjectType"]) == 4) // Polygon //
                {
                    sbText.Append("Region 1\r\n");
                    strArray = dr["strPolygonXY"].ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    sbText.Append("  " + Convert.ToInt32(strArray.Length / 2) + "\r\n");
                    for (int i = 0; i < strArray.Length - 1; i++)
                    {
                        if (Convert.ToDouble(strArray[i]) < doubleMinX) doubleMinX = Convert.ToDouble(strArray[i]);
                        if (Convert.ToDouble(strArray[i + 1]) < doubleMinY) doubleMinY = Convert.ToDouble(strArray[i + 1]);
                        if (Convert.ToDouble(strArray[i]) > doubleMaxX) doubleMaxX = Convert.ToDouble(strArray[i]);
                        if (Convert.ToDouble(strArray[i + 1]) > doubleMaxY) doubleMaxY = Convert.ToDouble(strArray[i + 1]);
                        sbText.Append(strArray[i] + " " + strArray[i + 1] + "\r\n");
                        if (strLocalitiesIncludedForAverageXY == "0" && !boolBoundsAlreadyCalculated)
                        {
                            doubleTotalX += Convert.ToDouble(strArray[i]);
                            doubleTotalY += Convert.ToDouble(strArray[i + 1]);
                            intValuesCounted++;
                        }
                        i++;  // Jump ahead so that we are always processing odd numbered array items //
                    }
                    sbText.Append("    Pen (1,2,0)\r\n");
                    sbText.Append("    Brush (2,12632256,12632256)\r\n");
                }
                else if (Convert.ToInt32(dr["ObjectType"]) == 2 || Convert.ToInt32(dr["ObjectType"]) == 5) // Polyline //
                {
                    sbText.Append("Pline\r\n");
                    strArray = dr["strPolygonXY"].ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    sbText.Append("  " + Convert.ToInt32(strArray.Length / 2) + "\r\n");
                    for (int i = 0; i < strArray.Length - 1; i++)
                    {
                        if (Convert.ToDouble(strArray[i]) < doubleMinX) doubleMinX = Convert.ToDouble(strArray[i]);
                        if (Convert.ToDouble(strArray[i + 1]) < doubleMinY) doubleMinY = Convert.ToDouble(strArray[i + 1]);
                        if (Convert.ToDouble(strArray[i]) > doubleMaxX) doubleMaxX = Convert.ToDouble(strArray[i]);
                        if (Convert.ToDouble(strArray[i + 1]) > doubleMaxY) doubleMaxY = Convert.ToDouble(strArray[i + 1]);
                        sbText.Append(strArray[i] + " " + strArray[i + 1] + "\r\n");
                        if (strLocalitiesIncludedForAverageXY == "0" && !boolBoundsAlreadyCalculated)
                        {
                            doubleTotalX += Convert.ToDouble(strArray[i]);
                            doubleTotalY += Convert.ToDouble(strArray[i + 1]);
                            intValuesCounted++;
                        }
                        i++;  // Jump ahead so that we are always processing odd numbered array items //
                    }
                    sbText.Append("    Pen (1,2,0)\r\n");
                    sbText.Append("    Brush (2,12632256,12632256)\r\n");
                }
            }

            // Add extra safety margins to bounds //
            doubleMinX += -5000;
            doubleMinY += -5000;
            doubleMaxX += 5000;
            doubleMaxY += 5000;

            string strText = "";
            strText = "Version 450\r\n";
            strText += "Charset \"WindowsLatin1\"\r\n";
            strText += "Delimiter \",\"\r\n";
            
            strText += strDefaultMappingProjection + " Bounds (" + doubleMinX.ToString() + ", " + doubleMinY.ToString() + ") (" + doubleMaxX.ToString() + ", " + doubleMaxY.ToString() + ")\r\n";
            //strText += "CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000 Bounds (" + doubleMinX.ToString() + ", " + doubleMinY.ToString() + ") (" + doubleMaxX.ToString() + ", " + doubleMaxY.ToString() + ")\r\n";
            strText += "Columns " + dt.Columns.Count.ToString() + "\r\n";

            sbText.Insert(0, strText);

            StreamWriter sw = new StreamWriter(strFilePath, false);
            sw.Write(sbText);

            sw.Close();
            return 1;
        }



        #region Import Grid

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 7;
            SetMenuStatus();
        }

        private void gridView7_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView7_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {

        }

        private void gridView7_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            
            //int intActualErrorCount = 0;
            //int intPossibleErrorCount = 0;
            //CheckData(view, view.FocusedRowHandle, ref intActualErrorCount, ref intPossibleErrorCount);
            
             double doubleNewValue;
             int intFocusedRow = view.FocusedRowHandle;
             DataRow dr = view.GetDataRow(intFocusedRow);
             dr.RowError = "";
             switch (view.FocusedColumn.Name)
             {
                 case "colintX":
                     dr.SetColumnError("intX", "");
                     if (e.Value == DBNull.Value)
                     {
                         doubleNewValue = (double)0.00;
                     }
                     else
                     {
                         doubleNewValue = Convert.ToDouble(e.Value);
                     }
                     //if (view.GetRowCellValue(intFocusedRow, "strPolygonXY") == null || view.GetRowCellValue(intFocusedRow, "strPolygonXY").ToString() == "")
                     //{
                         if (doubleNewValue == (double)0.00)
                         {
                             dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Missing value!");
                             dr.RowError = "Error(s) Present";
                         }
                         else if (doubleNewValue < doubleImportXMin || doubleNewValue > doubleImportXMax)
                         {
                             dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Value is outwith the bounds.");
                         }
                     //}
                     //else  // strPolygonXY has a value //   *** NO LONGER REQUIRED AS POLYGONS\POLYLINES HAVE THEIR CENTROID STORED IN X\Y FIELDS *** \\
                     //{
                     //    if (doubleNewValue != (double)0.00)
                     //    {
                     //        dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Cannot have a value when " + view.Columns["strPolygonXY"].Caption + " has a value!");
                     //    }
                     //}
                     break;
                 case "colintY":
                     dr.SetColumnError("intY", "");
                     if (e.Value == DBNull.Value)
                     {
                         doubleNewValue = (double)0.00;
                     }
                     else
                     {
                         doubleNewValue = Convert.ToDouble(e.Value);
                     }
                     //if (view.GetRowCellValue(intFocusedRow, "strPolygonXY") == null || view.GetRowCellValue(intFocusedRow, "strPolygonXY").ToString() == "")
                     //{
                         if (doubleNewValue == (double)0.00)
                         {
                             dr.SetColumnError("intY", view.Columns["intY"].Caption + ": Missing value!");
                         }
                         else if (doubleNewValue < doubleImportYMin || doubleNewValue > doubleImportYMax)
                         {
                             dr.SetColumnError("intY", view.Columns["intY"].Caption + ": Value is outwith the bounds.");
                         }
                     //}
                     //else  // strPolygonXY has a value //   *** NO LONGER REQUIRED AS POLYGONS\POLYLINES HAVE THEIR CENTROID STORED IN X\Y FIELDS *** \\
                     //{
                     //    if (doubleNewValue != (double)0.00)
                     //    {
                     //        dr.SetColumnError("intY", view.Columns["intY"].Caption + ": Cannot have a value when " + view.Columns["strPolygonXY"].Caption + " has a value!");
                     //    }
                     //}
                     break;
                 case "colstrPolygonXY":
                     // Check all points within polygon to ensure that are within the bounds //
                     dr.SetColumnError("strPolygonXY", "");
                     string[] strArray;
                     string strErrorMessage = "";
                     int intPolygonBoundaryXErrors = 0;
                     int intPolygonBoundaryYErrors = 0;
                     char[] delimiters = new char[] { ';' };
                     strArray = e.Value.ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                     intPolygonBoundaryXErrors = 0;
                     intPolygonBoundaryYErrors = 0;
                     if (strArray.Length > 0)
                     {
                         if (strArray.Length % 2 != 0)  // Modulus (%) //
                         {
                             dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Invalid number of coordinates - must be an even number!");
                         }
                         else if (strArray.Length < 6)  // was 8 but changed to 6 as polyline only needs 3 sets of coords (polygon needs at least 4 sets) //
                         {
                             dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Invalid number of coordinates - must be an least 6!");
                         }
                         //else if (strArray[0] != strArray[strArray.Length - 2] || strArray[1] != strArray[strArray.Length - 1])  // Value 1 lower than expected as array starts at 0 //
                         //{
                         //    dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Invalid coordinates - first pair and last pair must match to close polygon!");
                         //}  // *** NO LONGR REQUIRED AS IF POLYLINE, START AND END COORDS WON'T MATCH *** //
                         else
                         {
                             try
                             {
                                 for (int j = 0; j < strArray.Length - 1; j++)
                                 {
                                     if (Convert.ToDouble(strArray[j]) < doubleImportXMin || Convert.ToDouble(strArray[j]) > doubleImportXMax) intPolygonBoundaryXErrors++;
                                     if (Convert.ToDouble(strArray[j + 1]) < doubleImportYMin || Convert.ToDouble(strArray[j + 1]) > doubleImportYMax) intPolygonBoundaryYErrors++;
                                     j++;  // Jump ahead so that we are always processing odd numbered array items //
                                 }
                                 if (intPolygonBoundaryXErrors > 0 || intPolygonBoundaryYErrors > 0)
                                 {
                                     strErrorMessage = (intPolygonBoundaryXErrors > 0 ? view.Columns["intX"].Caption + ": " + intPolygonBoundaryXErrors.ToString() + " outwith the Bounds!" : "");
                                     strErrorMessage += (strErrorMessage == "" ? (intPolygonBoundaryYErrors > 0 ? view.Columns["intY"].Caption + ": " + intPolygonBoundaryYErrors.ToString() + " outwith the Bounds!" : "") : "\n" + (intPolygonBoundaryYErrors > 0 ? view.Columns["intY"].Caption + ": " + intPolygonBoundaryYErrors.ToString() + " outwith the Bounds!" : ""));
                                     dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": " + strErrorMessage);
                                 }
                             }
                             catch (Exception)
                             {
                                 dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Problem with entered points [must be numeric and seperated by ';']!");
                             }
                         }
                     }
                     if (e.Value.ToString() != "")
                     {
                         // *** FOLLOWING NO LONGER REQUIRED AS POLYGONS\POLYLINES HAVE THEIR CENTROID STORED IN X\Y FIELDS *** //
                         /*if (view.GetRowCellValue(intFocusedRow, "intX") != DBNull.Value)
                         {
                             if (Convert.ToDouble(view.GetRowCellValue(intFocusedRow, "intX")) != (double)0.00)
                             {
                                 dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Cannot have a value when " + view.Columns["strPolygonXY"].Caption + " has a value!");
                             }
                         }
                         if (view.GetRowCellValue(intFocusedRow, "intY") != DBNull.Value)
                         {
                             if (Convert.ToDouble(view.GetRowCellValue(intFocusedRow, "intY")) != (double)0.00)
                             {
                                 dr.SetColumnError("intY", view.Columns["intY"].Caption + ": Cannot have a value when " + view.Columns["strPolygonXY"].Caption + " has a value!");
                             }
                         }
                         */
                     }
                     else  // strPolygonXY is blank //
                     {
                         //if (view.GetRowCellValue(intFocusedRow, "intX") == null || Convert.ToDouble(view.GetRowCellValue(intFocusedRow, "intX")) == (double)0.00)
                         if (view.GetRowCellValue(intFocusedRow, "intX") == DBNull.Value)
                         {
                             dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Missing value!");
                         }
                         else if (Convert.ToDouble(view.GetRowCellValue(intFocusedRow, "intX")) == (double)0.00)
                         {
                             dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Missing value!");
                         }
                         else if (Convert.ToDouble(view.GetRowCellValue(intFocusedRow, "intX")) < doubleImportXMin || Convert.ToDouble(view.GetRowCellValue(intFocusedRow, "intX")) > doubleImportXMax)
                         {
                             dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Value is outwith the bounds.");
                         }
                         else
                         {
                             dr.SetColumnError("intX", "");
                         }

                         //if (view.GetRowCellValue(intFocusedRow, "intY") == null || Convert.ToDouble(view.GetRowCellValue(intFocusedRow, "intY")) == (double)0.00)
                         if (view.GetRowCellValue(intFocusedRow, "intY") == DBNull.Value)
                         {
                             dr.SetColumnError("intY", view.Columns["intY"].Caption + ": Missing value!");
                         }
                         else if (Convert.ToDouble(view.GetRowCellValue(intFocusedRow, "intY")) == (double)0.00)
                         {
                             dr.SetColumnError("intY", view.Columns["intY"].Caption + ": Missing value!");
                         }
                         else if (Convert.ToDouble(view.GetRowCellValue(intFocusedRow, "intY")) < doubleImportYMin || Convert.ToDouble(view.GetRowCellValue(intFocusedRow, "intY")) > doubleImportYMax)
                         {
                             dr.SetColumnError("intY", view.Columns["intY"].Caption + ": Value is outwith the bounds.");
                         }
                         else
                         {
                             dr.SetColumnError("intY", "");
                         }
                     }
                     break;
                 case "colintDistrictID":
                     dr.SetColumnError("intDistrictID", "");
                     int intDistrictID = 0;
                     if (e.Value != DBNull.Value)
                     {
                         intDistrictID = Convert.ToInt32(e.Value);
                     }
                     if (intDistrictID == 0)
                     {
                         dr.SetColumnError("intDistrictID", view.Columns["intDistrictID"].Caption + ": Must have a value!");
                     }
                     dr.SetColumnError("intLocalityID", "");
                     if ((view.GetRowCellValue(intFocusedRow, "intLocalityID") == null ? 0 : Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "intLocalityID"))) == 0) dr.SetColumnError("intLocalityID", view.Columns["intLocalityID"].Caption + ": Must have a value!");
                     break;
                 case "colintLocalityID":
                     dr.SetColumnError("intLocalityID", "");
                     int intLocalityID = 0;
                     if (e.Value != DBNull.Value)
                     {
                         intLocalityID = Convert.ToInt32(e.Value);
                     }
                     if (intLocalityID == 0)
                     {
                         dr.SetColumnError("intLocalityID", view.Columns["intLocalityID"].Caption + ": Must have a value!");
                     }
                     if ((view.GetRowCellValue(intFocusedRow, "intDistrictID") == null ? 0 : Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "intDistrictID"))) == 0) dr.SetColumnError("intDistrictID", view.Columns["intDistrictID"].Caption + ": Must have a value!");
                     break;
                 case "colintStatus":
                     dr.SetColumnError("intStatus", "");
                     int intStatus = 0;
                     if (e.Value != DBNull.Value)
                     {
                         intStatus = Convert.ToInt32(e.Value);
                     }
                     if (intStatus == 0)
                     {
                         dr.SetColumnError("intStatus", view.Columns["intStatus"].Caption + ": Must have a value!");
                     }
                     break;
                 default:
                     break;
             }

             // Critical Errors are denoted with an "!" at the end of them - if no critical erors then don't display RowError Warning icon in row selector column //
             string strCriticalErrors = "";
             DataColumn[] ErrorColumns = dr.GetColumnsInError();
             foreach (DataColumn dc in ErrorColumns)
             {
                 if (dr.GetColumnError(dc).ToString().EndsWith("!"))
                 {
                     strCriticalErrors += dr.GetColumnError(dc) + "\n";
                 }
             }
             if (strCriticalErrors == "")
             {
                 dr.RowError = "";
             }
             else
             {
                 dr.RowError = "Error(s) Present...\n" + strCriticalErrors;
             }

        }

        private void repositoryItemGridLookUpEditDistrict_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            RepositoryItemLookUpEdit props;
            if (sender is LookUpEdit)
            {
                props = (sender as LookUpEdit).Properties;
            }
            else
            {
                props = sender as RepositoryItemLookUpEdit;
            }
            if (props != null && (e.Value is int))
            {
                object row = props.GetDataSourceRowByKeyValue(e.Value);
                if (row == null)
                {
                    e.DisplayText = NotFoundText;
                }
            }
        }

        private void repositoryItemGridLookUpEditLocality_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            RepositoryItemLookUpEdit props;
            if (sender is LookUpEdit)
            {
                props = (sender as LookUpEdit).Properties;
            }
            else
            {
                props = sender as RepositoryItemLookUpEdit;
            }
            if (props != null && (e.Value is int))
            {
                object row = props.GetDataSourceRowByKeyValue(e.Value);
                if (row == null)
                {
                    e.DisplayText = NotFoundText;
                }
            }
        }

        private void gridView7_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            // colour Modified Cells Red //
            GridView view = (GridView)sender;
            DataRow row = view.GetDataRow(e.RowHandle);
            if (row.RowState != DataRowState.Unchanged)
            {
                if (e.Column.UnboundType == DevExpress.Data.UnboundColumnType.Bound && row.HasVersion(DataRowVersion.Original))
                {
                    if (!Equals(row[e.Column.FieldName, DataRowVersion.Current], row[e.Column.FieldName, DataRowVersion.Original]))
                    {
                        e.Appearance.BackColor = Color.Tomato;
                    }
                }
            }
        }

        #endregion
        
        private void ImportFileButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

            #region GetFileToImport
            string strDefaultPath = "";
            string strImportPath = "";
            string strFileName = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesImportFromGBM_Default_Tab_Folder").ToString().ToLower();
            }
            catch (Exception)
            {
            }
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.DefaultExt = "tab";
                dlg.Filter = "TAB Files (*.tab)|*.tab"; ;
                if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                dlg.Multiselect = false;
                if (dlg.ShowDialog() != DialogResult.OK) return;

                FileInfo file = new FileInfo(dlg.FileName);
                strImportPath = file.DirectoryName;
                string strPathAndFileName = file.FullName;
                strFileName = file.Name.Substring(0, file.Name.Length - 4);

                if (strFileName.ToLower() != "tree")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("The file selected for a Survey Import must be named 'Tree.Tab'.\n\nTry selecting the correct file before proceeding.", "Import Survey Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                ImportFileButtonEdit.EditValue = strPathAndFileName;

                btnCheckData.Enabled = false;
                btnImport.Enabled = false;

                // Check if an import has occurred from the selected directory //
                try
                {
                    DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter CheckImportFolder = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                    CheckImportFolder.ChangeConnectionString(strConnectionString);
                    int intImportCount = Convert.ToInt32(CheckImportFolder.sp00143_import_from_GBM_check_if_already_imported(strImportPath, 0));  // 0 = Survey, 1 = WorkOrders //
                    if (intImportCount > 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("WARNING...  WARNING...  WARNING...  WARNING...  WARNING...\n=========================================\n\nYou have already imported from the selected folder [" + strImportPath + "]!\n\nIf you proceed and import this data you may create duplicate Inspections and Actions!", "Import Data   *** IMPORTANT INFORMATION ***", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while checking if an import has already been made from the selected folder!\n\nYou can still import from the folder however you should manually check you have not already imported the file.", "Import Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }


            }
            #endregion

            #region ConvertTabToMidMif

            string strAppToRun = "\"" + Application.StartupPath + "\\Plugin_Apps\\tab2tab.exe" + "\"";
            string strArguments = "\"" + ImportFileButtonEdit.EditValue + "\" \"" + strImportPath + "\\" + strFileName + ".mif" + "\"";
            string error = "";
            try
            {
                System.Diagnostics.Process Proc = new System.Diagnostics.Process();
                Proc.StartInfo.CreateNoWindow = true;  // Hide DOS Window [This line and next]//
                Proc.StartInfo.UseShellExecute = false;
                Proc.StartInfo.RedirectStandardError = true;
                Proc.StartInfo.FileName = strAppToRun;
                Proc.StartInfo.Arguments = strArguments;

                Proc.Start();
                error = Proc.StandardError.ReadToEnd();
                Proc.WaitForExit(300000);
                Proc.Close();
                Proc = null;
                GC.GetTotalMemory(true);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to import data - an error occurred with Tab2Tab.EXE [" + ex.Message + "] - contact Technical Support.", "Import From GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!string.IsNullOrEmpty(error))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to import data - an error occurred with Tab2Tab.EXE [" + error + "] - contact Technical Support.", "Import From GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            #endregion

            #region ImportDataInToGrid
            GridView view = (GridView)gridControl7.MainView;
            view.BeginUpdate();
            view.BeginDataUpdate();
            this.dataSet_AT_DataTransfer.sp00134_export_to_GBM_export_data.Rows.Clear();
            view.EndDataUpdate();
            view.EndUpdate();
            Application.DoEvents();  // Allow Form time to repaint itself //

            string[] linesMIF = File.ReadAllLines(strImportPath + "\\" + strFileName + ".mif");

            Boolean boolColumns = false;
            char[] delimiters = new char[] { ' ', '(', ')', ',' };
            ArrayList arrayImportStructure = new ArrayList();

            doubleImportXMin = (double)0.00;
            doubleImportYMin = (double)0.00;
            doubleImportXMax = (double)0.00;
            doubleImportYMax = (double)0.00;

            int intCurrentMidFileLineNumber = 0;
            try
            {
                foreach (string line in linesMIF)
                {
                    intCurrentMidFileLineNumber++; // Store current position in the file for later //
                    if (line.StartsWith("CoordSys"))
                    {
                        string strBounds = line.Substring(line.IndexOf("Bounds"));
                        doubleImportXMin = Convert.ToDouble(strBounds.Substring(strBounds.IndexOf('(') + 1, strBounds.IndexOf(',') - (strBounds.IndexOf('(') + 1)));
                        doubleImportYMin = Convert.ToDouble(strBounds.Substring(strBounds.IndexOf(',') + 2, strBounds.IndexOf(')') - (strBounds.IndexOf(',') + 2)));
                        strBounds = strBounds.Substring(strBounds.IndexOf(')') + 2);
                        doubleImportXMax = Convert.ToDouble(strBounds.Substring(1, strBounds.IndexOf(',') - 1));
                        doubleImportYMax = Convert.ToDouble(strBounds.Substring(strBounds.IndexOf(',') + 2, strBounds.IndexOf(')') - (strBounds.IndexOf(',') + 2)));
                        continue;  // Go to next iteration of loop //
                    }
                    if (line.StartsWith("Columns"))
                    {
                        boolColumns = true;
                        continue;  // Go to next iteration of loop //
                    }
                    if (line.StartsWith("Data")) break; // All columns processed so Exit Loop //
                    if (!boolColumns) continue;  // Go to next iteration of loop //

                    // If we are here, we are processing a field definition line //
                    string[] items = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);  // StringSplitOptions.RemoveEmptyEntries: Stops double space from start of line from being picked up and inserted as elements of the array. //
                    //arrayListStructure.Add(items);
                    arrayImportStructure.Add(items[0]);
                }
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to import data - an error occurred while parsing the " + strFileName + ".mif file  - contact Technical Support.", "Import From GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intColumnPosition = 0;
            int intLinePosition = 0;

            string[] linesMID = File.ReadAllLines(strImportPath + "\\" + strFileName + ".mid");
            System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
            view.BeginUpdate();
            view.BeginDataUpdate();
            DataRow drNewRow;

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Processing...");
            fProgress.Show();
            Application.DoEvents();
            int intUpdateProgressThreshhold = linesMID.Length / 10;
            int intUpdateProgressTempCount = 0;
            
            ArrayList ArrayListColumnNamesUpperCase = new ArrayList();
            foreach (GridColumn col in view.Columns)
	        {
                ArrayListColumnNamesUpperCase.Add(col.Name.ToUpper());
	        }

            string strCurrentMifFileLine = "";
            string strX = "";
            string strY = "";
            string strPolygonXY = "";
            string[] strTempArray = null;
            int intNumberOfPolygonCoordinates = 0;
            int intRecordOrder = 1;
            string strNumber = "";
            try
            {
                foreach (string line in linesMID)
                {
                    drNewRow = this.dataSet_AT_DataTransfer.sp00134_export_to_GBM_export_data.NewRow();
                    intColumnPosition = 0;
                    string[] items = r.Split(line);
                    foreach (object item in items)
                    {                       
                        if (ArrayListColumnNamesUpperCase.Contains("COL" + arrayImportStructure[intColumnPosition].ToString().ToUpper()))
                        {
                            if ((arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtlastinspectiondate" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtnextinspectiondate" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtsurveydate" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtplantdate" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtinspectdate" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate_1" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate_1" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate_2" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate_2" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate_3" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate_3" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate_4" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate_4" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate_5" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate_5")
                                   && (item.ToString() == "" || item.ToString().Length < 8))
                            {
                                drNewRow[arrayImportStructure[intColumnPosition].ToString()] = DBNull.Value;
                            }
                            else if (arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtlastinspectiondate" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtnextinspectiondate" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtsurveydate" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtplantdate" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtinspectdate" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate_1" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate_1" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate_2" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate_2" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate_3" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate_3" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate_4" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate_4" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate_5" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate_5")
                            {
                                if (item.ToString().StartsWith("\""))  // Remove quotes while converting to datetime //
                                {
                                    drNewRow[arrayImportStructure[intColumnPosition].ToString()] = DateTime.ParseExact(Convert.ToString(item).Substring(1, item.ToString().Length - 2), "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo);  // Non null date so convert string into a proper date //
                                }
                                else  // No quotes to remove while converting to datetime //
                                {
                                    drNewRow[arrayImportStructure[intColumnPosition].ToString()] = DateTime.ParseExact(Convert.ToString(item), "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo);  // Non null date so convert string into a proper date //
                                }
                            }
                            else
                            {
                                if (item.ToString().StartsWith("\""))
                                {
                                    drNewRow[arrayImportStructure[intColumnPosition].ToString()] = item.ToString().Substring(1, item.ToString().Length - 2);  // Remove surrounding quotes //
                                }
                                else
                                {
                                    drNewRow[arrayImportStructure[intColumnPosition].ToString()] = item;
                                }
                            }
                        }
                        intColumnPosition++;
                    }
                    intLinePosition++;

                    do
                    {
                        strCurrentMifFileLine = linesMIF[intCurrentMidFileLineNumber];
                        intCurrentMidFileLineNumber++;  // Move to next line //
                        if (strCurrentMifFileLine.StartsWith("Point") || strCurrentMifFileLine.StartsWith("Region") || strCurrentMifFileLine.StartsWith("Pline") || strCurrentMifFileLine.StartsWith("Line"))
                        {
                            if (strCurrentMifFileLine.StartsWith("Point"))
                            {
                                strTempArray = strCurrentMifFileLine.Split(' ');
                                strX = strTempArray[1];
                                strY = strTempArray[2];
                                strPolygonXY = "";
                                intCurrentMidFileLineNumber++;  // Move past the subsequent symbol styling line //
                            }
                            else if (strCurrentMifFileLine.StartsWith("Pline"))
                            {
                                intCurrentMidFileLineNumber--;  // Move to previous line as the number of coord pairs is displayed right after the 'pline ' *** //
                                //strX = "0.00";
                                //strY = "0.00";
                                strPolygonXY = "";
                                strNumber = strCurrentMifFileLine.Trim().Substring(6).Trim();
                                if (!string.IsNullOrEmpty(strNumber))  // Use number of coord pairs found //
                                {
                                    intNumberOfPolygonCoordinates = Convert.ToInt32(strNumber);
                                }
                                else  // the number of coords did not follow 'pline' clause so move down a line and try next (the way Polygons are processed) // 
                                {
                                    intCurrentMidFileLineNumber++;  // Move to next line //
                                    strCurrentMifFileLine = linesMIF[intCurrentMidFileLineNumber];
                                    intNumberOfPolygonCoordinates = Convert.ToInt32(strCurrentMifFileLine.Trim());
                                }
                                for (int i = 0; i < intNumberOfPolygonCoordinates; i++)
                                {
                                    intCurrentMidFileLineNumber++;  // Move to next line //
                                    strCurrentMifFileLine = linesMIF[intCurrentMidFileLineNumber];
                                    strTempArray = strCurrentMifFileLine.Split(' ');
                                    strPolygonXY += Convert.ToString(Math.Round(Convert.ToDecimal(strTempArray[0]), 2)) + ";" + Convert.ToString(Math.Round(Convert.ToDecimal(strTempArray[1]), 2)) + ";";
                                }
                                intCurrentMidFileLineNumber++;  // Move past the subsequent symbol styling line //
                            }
                            else if (strCurrentMifFileLine.StartsWith("Line"))  // These shouldn't really be present within the file but you never know - if not handled, data can get out of sync when pulling from mid and mif files // 
                            {
                                //strX = "0.00";
                                //strY = "0.00";
                                strPolygonXY = strCurrentMifFileLine.Trim().Substring(5).Trim();
                                strTempArray = strPolygonXY.Split(' ');
                                strPolygonXY = "";
                                for (int j = 0; j < strTempArray.Length; j++)
                                {
                                    strPolygonXY += Convert.ToString(Math.Round(Convert.ToDecimal(strTempArray[j]), 2)) + ";";                                       
                                }
                                intCurrentMidFileLineNumber++;  // Move past the subsequent symbol styling line //
                            }
                            else if (strCurrentMifFileLine.StartsWith("Region"))
                            {
                                //strX = "0.00";
                                //strY = "0.00";
                                strPolygonXY = "";
                                strCurrentMifFileLine = linesMIF[intCurrentMidFileLineNumber];
                                intNumberOfPolygonCoordinates = Convert.ToInt32(strCurrentMifFileLine.Trim());
                                for (int i = 0; i < intNumberOfPolygonCoordinates; i++)
                                {
                                    intCurrentMidFileLineNumber++;  // Move to next line //
                                    strCurrentMifFileLine = linesMIF[intCurrentMidFileLineNumber];
                                    strTempArray = strCurrentMifFileLine.Split(' ');
                                    strPolygonXY += Convert.ToString(Math.Round(Convert.ToDecimal(strTempArray[0]), 2)) + ";" + Convert.ToString(Math.Round(Convert.ToDecimal(strTempArray[1]), 2)) + ";";
                                }
                                intCurrentMidFileLineNumber++;  // Move past the subsequent symbol styling line //
                            }
                            break;
                        }
                        else
                        {
                            //if (intCurrentMidFileLineNumber >= linesMIF.Length) break;
                        }
                    } while (true);

                    if (!string.IsNullOrEmpty(drNewRow["ID"].ToString()) && drNewRow["ID"].ToString() != "**********")  // Ignore any PolyLines plotted [PLine has no Map ID] and any dummy tree created as the exported localities had no trees in them //
                    {
                        //drNewRow["intX"] = Convert.ToDouble(strX);
                        //drNewRow["intY"] = Convert.ToDouble(strY);
                        drNewRow["intX"] = (string.IsNullOrEmpty(strX) ? (double)0.00 : Convert.ToDouble(strX));
                        drNewRow["intY"] = (string.IsNullOrEmpty(strY) ? (double)0.00 : Convert.ToDouble(strY));
                        drNewRow["strPolygonXY"] = strPolygonXY;
                        drNewRow["RecordOrder"] = intRecordOrder;
                        if (!string.IsNullOrEmpty(drNewRow["decareaha"].ToString())) drNewRow["decareaha"] = Convert.ToDecimal(drNewRow["decareaha"]) * (decimal)1000000;  // Convert GBM Mobile's area from KMs to Metres //
                        this.dataSet_AT_DataTransfer.sp00134_export_to_GBM_export_data.Rows.Add(drNewRow);
                        intRecordOrder++;
                    }

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                this.dataSet_AT_DataTransfer.AcceptChanges();  // Commit changes to underlying dataset so Cell Colouring for change tracking can occur //
            }
            catch (Exception Ex)
            {
                view.EndDataUpdate();
                view.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to import data - an error occurred while parsing the " + strFileName + ".mid file [" + Ex.Message + "] - contact Technical Support.", "Import From GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress.Dispose();
            }
            view.EndDataUpdate();
            view.EndUpdate();
 
            #endregion

            #region PerformInitialChecksOnData


            btnCheckData.Enabled = true;
            btnImport.Enabled = true;

            #endregion
        }

        private void btnCheckData_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl7.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow()) return;

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Checking Data...");
            fProgress.Show();
            Application.DoEvents();
            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            int intActualErrorCount = 0;
            int intPossibleErrorCount = 0;
            int intPictureErrorCount = 0;
            string strMissingPictures = "";
            string strDefaultPicturePath = "";

            /*try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPicturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesImportFromGBM_Default_Picture_Folder").ToString().ToLower();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + ex.Message + "] while retrieving the Default Picture File Folder Location from the WoodPlan Settings Table!\n\nOpen the System Settings screen and check the value before trying again. If the problem persists contact Technical Support.\n\nChecking Aborted!", "Check Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            }*/
            char[] delimiters = new char[] { '\\' };
            string[] strArray = ImportFileButtonEdit.EditValue.ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length >= 4)
            {
                strArray[strArray.Length - 3] = "Photos";  // Extra 1 as Array starts at element 0 //
                for (int i = 0; i <= strArray.Length - 2; i++)  // Extra 1 as Array starts at element 0 and 1 to ignore filename //
                {
                    if (i < strArray.Length - 2)
                    {
                        strDefaultPicturePath += strArray[i] + "\\";
                    }
                    else
                    {
                        strDefaultPicturePath += strArray[i].Substring(5) + "\\";  // Last element so strip off "tree_" from start of folder name //
                    }
                }
            }
            else
            {
                strDefaultPicturePath = "C:\\";
            }


            for (int i = 0; i < view.DataRowCount; i++)
            {
                CheckData(view, i, ref intActualErrorCount, ref intPossibleErrorCount, ref intPictureErrorCount, ref strMissingPictures, strDefaultPicturePath);

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress.Dispose();
            }
            if (intActualErrorCount > 0 || intPossibleErrorCount > 0 || intPictureErrorCount > 0)
            {
                frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors frm_child = new frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors();
                frm_child.GlobalSettings = this.GlobalSettings;
                frm_child.intActualErrorCount = intActualErrorCount;
                frm_child.intPossibleErrorCount = intPossibleErrorCount;
                frm_child.intPictureErrorCount = intPictureErrorCount;
                frm_child.strMissingPictures = strMissingPictures;
                this.ParentForm.AddOwnedForm(frm_child);
                switch (frm_child.ShowDialog())
                {
                    case DialogResult.Cancel:
                        break;
                    case DialogResult.OK:  // Clear Missing Photo Links //
                        Clear_Missing_Picture_Links(view, strDefaultPicturePath);
                        break;
                    default:
                        break;
                }
            }
        }

        private void Clear_Missing_Picture_Links(GridView view, string strDefaultPicturePath)
        {
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (!string.IsNullOrEmpty(view.GetRowCellValue(i, "TreePhotograph").ToString()))
                {
                    if (!File.Exists(strDefaultPicturePath + "\\" + view.GetRowCellValue(i, "TreePhotograph").ToString())) view.SetRowCellValue(i, "TreePhotograph", null);
                }
                if (!string.IsNullOrEmpty(view.GetRowCellValue(i, "InspectionPhotograph").ToString()))
                {
                    if (!File.Exists(strDefaultPicturePath + "\\" + view.GetRowCellValue(i, "InspectionPhotograph").ToString())) view.SetRowCellValue(i, "InspectionPhotograph", null);
                }
                DataRow dr = view.GetDataRow(i);
                if (dr.HasErrors) dr.ClearErrors();
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl7.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow()) return;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no filters switched on //
            sp00134exporttoGBMexportdataBindingSource.Filter = "";  // Clear any Prior Filter //

            if (view.DataRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Data To Import - Import Aborted!", "Check Import Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Checking Data...");
            fProgress.Show();
            Application.DoEvents();
            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            int intActualErrorCount = 0;
            int intPossibleErrorCount = 0;
            int intPictureErrorCount = 0;
            string strMissingPictures = "";
            string strDefaultPicturePath = "";
            /*try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPicturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesImportFromGBM_Default_Picture_Folder").ToString().ToLower();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + ex.Message + "] while retrieving the Default Picture File Folder Location from the System Settings Table!\n\nOpen the WoodPlan Settings screen and check the value before trying again. If the problem persists contact Technical Support.\n\nImport Aborted!", "Import Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            }*/
            char[] delimiters = new char[] { '\\' };
            string[] strArray = ImportFileButtonEdit.EditValue.ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length >= 4)
            {
                strArray[strArray.Length - 3] = "Photos";  // Extra 1 as Array starts at element 0 //
                for (int i = 0; i <= strArray.Length - 2; i++)  // Extra 1 as Array starts at element 0 and 1 to ignore filename //
                {
                    if (i < strArray.Length - 2)
                    {
                        strDefaultPicturePath += strArray[i] + "\\";
                    }
                    else
                    {
                        strDefaultPicturePath += strArray[i].Substring(5) + "\\";  // Last element so strip off "tree_" from start of folder name //
                    }
                }
            }
            else
            {
                strDefaultPicturePath = "C:\\";
            }

            string strDestinationPicturePath = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                //strDestinationPicturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder").ToString().ToLower();
                strDestinationPicturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString().ToLower();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + ex.Message + "] while retrieving the Picture Files Folder from the System Settings Table!\n\nOpen the WoodPlan Settings screen and check the value before trying again. If the problem persists contact Technical Support.\n\nImport Aborted!", "Import Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            }

            for (int i = 0; i < view.DataRowCount; i++)
            {
                CheckData(view, i, ref intActualErrorCount, ref intPossibleErrorCount, ref intPictureErrorCount, ref strMissingPictures, strDefaultPicturePath);

                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress.Dispose();
            }
            bool boolProceed = false;
            if (intActualErrorCount > 0 || intPossibleErrorCount > 0 || intPictureErrorCount > 0)
            {
                frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors frm_child = new frm_AT_Data_Transfer_GBM_Mobile_Check_Data_Errors();
                frm_child.GlobalSettings = this.GlobalSettings;
                frm_child.intActualErrorCount = intActualErrorCount;
                frm_child.intPossibleErrorCount = intPossibleErrorCount;
                frm_child.intPictureErrorCount = intPictureErrorCount;
                frm_child.strMissingPictures = strMissingPictures;
                this.ParentForm.AddOwnedForm(frm_child);
                switch (frm_child.ShowDialog())
                {
                    case DialogResult.Cancel:
                        if (intActualErrorCount > 0 || intPictureErrorCount > 0)
                        {
                            return;  // Missing Picture Links or Critical Errors so abort //
                        }
                        else  // OK to proceed becuase only potential errors were found //
                        {
                            break;
                        }
                    case DialogResult.OK:  // Clear Missing Photo Links //
                        Clear_Missing_Picture_Links(view, strDefaultPicturePath);
                        break;
                    default:
                        return;
                }
            }
            //string strMessage = "You are about to import your data; before you proceed have you checked to ensure that any new Inspections and Actions have been allocated a reference number?\n\nYou can do this using the Select Records - Missing: and then block edit to choose a sequence and allocate the correct numbers.\n\nIf you are unsure on how to proceed contact Technical Support.\n\nClick Cancel to halt the process and allocate the numbers or OK to continue.";
            //if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Import Data", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.OK)
            frm_AT_Data_Transfer_GBM_Mobile_Confirm_Import frm_confirm = new frm_AT_Data_Transfer_GBM_Mobile_Confirm_Import();
            if (frm_confirm.ShowDialog() == DialogResult.OK)    
            {
                boolProceed = true;
            }
            if (!boolProceed) return;

            // Checks Passed so import data... //
            fProgress = new frmProgress(5);
            fProgress.UpdateCaption("Importing Data [Copying Photos]...");
            fProgress.Show();
            Application.DoEvents();
            intUpdateProgressThreshhold = view.DataRowCount / 10;
            intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginDataUpdate();
            view.BeginSort();

            // Copy any Pictures First... //
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (!string.IsNullOrEmpty(view.GetRowCellValue(i, "TreePhotograph").ToString()))
                {
                    try
                    {
                        File.Copy(strDefaultPicturePath + view.GetRowCellValue(i, "TreePhotograph").ToString(), strDestinationPicturePath + "\\" + view.GetRowCellValue(i, "TreePhotograph").ToString());
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + ex.Message + "] while copying Tree Photograph: " + strDefaultPicturePath + view.GetRowCellValue(i, "TreePhotograph").ToString() + " - try importing again. If the problem persists contact Technical Support.\n\nImport Aborted!", "Import Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    }
                }
                if (!string.IsNullOrEmpty(view.GetRowCellValue(i, "InspectionPhotograph").ToString()))
                {
                    try
                    {
                        File.Copy(strDefaultPicturePath + view.GetRowCellValue(i, "InspectionPhotograph").ToString(), strDestinationPicturePath + "\\" + view.GetRowCellValue(i, "InspectionPhotograph").ToString());
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + ex.Message + "] while copying Inspection Photograph: " + strDefaultPicturePath + view.GetRowCellValue(i, "InspectionPhotograph").ToString() + " - try importing again. If the problem persists contact Technical Support.\n\nImport Aborted!", "Import Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    }
                }
            }
            fProgress.UpdateProgress(5);
            fProgress.UpdateCaption("Importing Data...");

            // Import Data... //
            DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter ImportData = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            ImportData.ChangeConnectionString(strConnectionString);
            DateTime dtImportStartTime = DateTime.Now;
            int intCurrentRow = 0;

            DataRow dr = null;

            string ID;
            int intLocalityID;
            string strTreeRef;
            int intX;	
            int intY;
            string strPolygonXY;
            int intStatus;
            int intAgeClass;
            int intDistance;
            string strHouseName;
            int intSpeciesID;
            int intAccess;
            int intVisibility;
            string strContext;
            string strManagement;
            int intLegalStatus;
            DateTime dtLastInspectionDate;
            int intInspectionCycle;
            string strInspectionUnit;
            DateTime dtNextInspectionDate;
            int intGroundType;
            decimal decHeight;
            int intHeightRange;
            int intDBH;
            int intDBHRange;
            int intProtectionType;
            int intCrownDiameter;
            int intCrownDepth;
            DateTime dtPlantDate;
            int intGroupNo;
            int intSafetyPriority;
            int intSiteHazardClass;
            int intPlantSize;
            int intPlantSource;
            int intPlantMethod;
            string strPostcode;
            string strMapLabel;
            int intSize;
            int intNearbyObject;
            int intNearbyObject2;
            int intNearbyObject3;
            string strRemarks_Tree;
            string strUser1_Tree;
            string strUser2_Tree;
            string strUser3_Tree;
            decimal decAreaHa;
            int intType;
            string strHedgeOwner;
            int intLength;
            decimal decGardenSize;
            decimal decPropertyProximity;
            decimal decSiteLevel;
            int intReplantCount;
            DateTime dtSurveyDate;
            int intTreeOwnership;
            string strTreePhotograph;
            decimal Cavat;
            int StemCount;
            decimal CrownNorth;
            decimal CrownSouth;
            decimal CrownEast;
            decimal CrownWest;
            int RetentionCategory;
            int SULE;
            int UserPicklist1_Tree;
            int UserPicklist2_Tree;
            int UserPicklist3_Tree;
            int SpeciesVariety;
            decimal ObjectLength;
            decimal ObjectWidth;
            float LocationX = (float)0.00;
            float LocationY = (float)0.00;
            string LocationPolyXY = "";

            string strInspectionPhotograph;
            string strInspectRef;
            int intInspector;
            DateTime dtInspectDate;
            int intIncidentID;
            int intStemPhysical;
            int intStemPhysical2;
            int intStemPhysical3;
            int intStemDisease;
            int intStemDisease2;
            int intStemDisease3;
            int intAngleToVertical;
            int intCrownPhysical;
            int intCrownPhysical2;
            int intCrownPhysical3;
            int intCrownDisease;
            int intCrownDisease2;
            int intCrownDisease3;
            int intCrownFoliation;
            int intCrownFoliation2;
            int intCrownFoliation3;
            int intSafety;
            int intGeneralCondition;
            int intRootHeave;
            int intRootHeave2;
            int intRootHeave3;
            string strUser1_Inspection;
            string strUser2_Inspection;
            string strUser3_Inspection;
            string strRemarks_Inspection;
            int intVitality;
            int UserPicklist1_Inspection;
            int UserPicklist2_Inspection;
            int UserPicklist3_Inspection;

            DateTime dtDueDate_1;
            DateTime dtDoneDate_1;
            int intAction_1;
            int intActionBy_1;
            int intSupervisor_1;
            string strJobNumber_1;
            string strUser1_Action_1;
            string strUser2_Action_1;
            string strUser3_Action_1;
            string strRemarks_Action_1;
            int intWorkOrderID_1;
            int intPriorityID_1;
            int intScheduleOfRates_1;
            decimal monJobWorkUnits_1;
            int intOwnership_1;
            int UserPicklist1_Action_1;
            int UserPicklist2_Action_1;
            int UserPicklist3_Action_1;

            DateTime dtDueDate_2;
            DateTime dtDoneDate_2;
            int intAction_2;
            int intActionBy_2;
            int intSupervisor_2;
            string strJobNumber_2;
            string strUser1_Action_2;
            string strUser2_Action_2;
            string strUser3_Action_2;
            string strRemarks_Action_2;
            int intWorkOrderID_2;
            int intPriorityID_2;
            int intScheduleOfRates_2;
            decimal monJobWorkUnits_2;
            int intOwnership_2;
            int UserPicklist1_Action_2;
            int UserPicklist2_Action_2;
            int UserPicklist3_Action_2;

            DateTime dtDueDate_3;
            DateTime dtDoneDate_3;
            int intAction_3;
            int intActionBy_3;
            int intSupervisor_3;
            string strJobNumber_3;
            string strUser1_Action_3;
            string strUser2_Action_3;
            string strUser3_Action_3;
            string strRemarks_Action_3;
            int intWorkOrderID_3;
            int intPriorityID_3;
            int intScheduleOfRates_3;
            decimal monJobWorkUnits_3;
            int intOwnership_3;
            int UserPicklist1_Action_3;
            int UserPicklist2_Action_3;
            int UserPicklist3_Action_3;

            DateTime dtDueDate_4;
            DateTime dtDoneDate_4;
            int intAction_4;
            int intActionBy_4;
            int intSupervisor_4;
            string strJobNumber_4;
            string strUser1_Action_4;
            string strUser2_Action_4;
            string strUser3_Action_4;
            string strRemarks_Action_4;
            int intWorkOrderID_4;
            int intPriorityID_4;
            int intScheduleOfRates_4;
            decimal monJobWorkUnits_4;
            int intOwnership_4;
            int UserPicklist1_Action_4;
            int UserPicklist2_Action_4;
            int UserPicklist3_Action_4;

            DateTime dtDueDate_5;
            DateTime dtDoneDate_5;
            int intAction_5;
            int intActionBy_5;
            int intSupervisor_5;
            string strJobNumber_5;
            string strUser1_Action_5;
            string strUser2_Action_5;
            string strUser3_Action_5;
            string strRemarks_Action_5;
            int intWorkOrderID_5;
            int intPriorityID_5;
            int intScheduleOfRates_5;
            decimal monJobWorkUnits_5;
            int intOwnership_5;
            int UserPicklist1_Action_5;
            int UserPicklist2_Action_5;
            int UserPicklist3_Action_5;

            Mapping_Functions MapFunctions = new Mapping_Functions(); 
            try
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    intCurrentRow = i;  // Make current row availble to Catch statement just in case an error occurs //
                    dr = view.GetDataRow(intCurrentRow);

                    ID = Convert.ToString(dr["ID"] ?? "");
                    intLocalityID = Convert.ToInt32(dr["intLocalityID"] ?? 0);
                    strTreeRef = Convert.ToString(dr["strTreeRef"] ?? "");
                    intX = (dr["intX"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intX"]));
                    intY = (dr["intY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intY"]));
                    strPolygonXY = Convert.ToString(dr["strPolygonXY"] ?? "");

                    // Calculate Lat Long Values //
                    MapInfo.Geometry.FeatureGeometry ftr = MapFunctions.CreateLatLongFeatureFromNonLatLongCoords((float)intX, (float)intY, strPolygonXY);
                    if (ftr == null)
                    {
                        LocationX = (float)0.00;
                        LocationY = (float)0.00;
                        LocationPolyXY = "";
                    }
                    else
                    {
                        MapFunctions.GetLatLongCoordsFromLatLongFeature(ftr, ref LocationX, ref LocationY, ref LocationPolyXY);  // Passed by Ref so original values are updated //
                    }

                    intStatus = (dr["intStatus"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intStatus"]));
                    intAgeClass = (dr["intAgeClass"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intAgeClass"]));
                    intDistance = (dr["intDistance"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intDistance"]));
                    strHouseName = Convert.ToString(dr["strHouseName"] ?? "");
                    intSpeciesID = (dr["intSpeciesID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intSpeciesID"]));
                    intAccess = (dr["intAccess"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intAccess"]));
                    intVisibility = (dr["intVisibility"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intVisibility"]));
                    strContext = Convert.ToString(dr["strContext"] ?? "");
                    strManagement = Convert.ToString(dr["strManagement"] ?? "");
                    intLegalStatus = (dr["intLegalStatus"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intLegalStatus"]));
                    dtLastInspectionDate = (dr["dtLastInspectionDate"] == DBNull.Value ? Convert.ToDateTime("01/01/1900 00:00:00.000") : Convert.ToDateTime(dr["dtLastInspectionDate"]));
                    intInspectionCycle = (dr["intInspectionCycle"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intInspectionCycle"]));
                    strInspectionUnit = Convert.ToString(dr["strInspectionUnit"] ?? 0);
                    dtNextInspectionDate = (dr["dtNextInspectionDate"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtNextInspectionDate"]));
                    intGroundType = (dr["intGroundType"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intGroundType"]));
                    decHeight = (dr["decHeight"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["decHeight"]));
                    intHeightRange = (dr["intHeightRange"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intHeightRange"]));
                    intDBH = (dr["intDBH"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intDBH"]));
                    intDBHRange = (dr["intDBHRange"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intDBHRange"]));
                    intProtectionType = (dr["intProtectionType"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intProtectionType"]));
                    intCrownDiameter = (dr["intCrownDiameter"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownDiameter"]));
                    intCrownDepth = (dr["intCrownDepth"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownDepth"]));
                    dtPlantDate = (dr["dtPlantDate"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtPlantDate"]));
                    intGroupNo = (dr["intGroupNo"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intGroupNo"]));
                    intSafetyPriority = (dr["intSafetyPriority"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intSafetyPriority"]));
                    intSiteHazardClass = (dr["intSiteHazardClass"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intSiteHazardClass"]));
                    intPlantSize = (dr["intPlantSize"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intPlantSize"]));
                    intPlantSource = (dr["intPlantSource"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intPlantSource"]));
                    intPlantMethod = (dr["intPlantMethod"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intPlantMethod"]));
                    strPostcode = Convert.ToString(dr["strPostcode"] ?? "");
                    strMapLabel = Convert.ToString(dr["strMapLabel"] ?? "");
                    intSize = (dr["intSize"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intSize"]));
                    intNearbyObject = (dr["intNearbyObject"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intNearbyObject"]));
                    intNearbyObject2 = (dr["intNearbyObject2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intNearbyObject2"]));
                    intNearbyObject3 = (dr["intNearbyObject3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intNearbyObject3"]));
                    strRemarks_Tree = Convert.ToString(dr["strRemarks_Tree"] ?? "");
                    strUser1_Tree = Convert.ToString(dr["strUser1_Tree"] ?? "");
                    strUser2_Tree = Convert.ToString(dr["strUser2_Tree"] ?? "");
                    strUser3_Tree = Convert.ToString(dr["strUser3_Tree"] ?? "");
                    decAreaHa = (dr["decAreaHa"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["decAreaHa"]));
                    intType = (dr["intType"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intType"]));
                    strHedgeOwner = Convert.ToString(dr["strHedgeOwner"] ?? "");
                    intLength = (dr["intLength"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intLength"]));
                    decGardenSize = (dr["decGardenSize"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["decGardenSize"]));
                    decPropertyProximity = (dr["decPropertyProximity"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["decPropertyProximity"]));
                    decSiteLevel = (dr["decSiteLevel"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["decSiteLevel"]));
                    intReplantCount = (dr["intReplantCount"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intReplantCount"]));
                    dtSurveyDate = (dr["dtSurveyDate"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtSurveyDate"]));
                    intTreeOwnership = (dr["intTreeOwnership"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intTreeOwnership"]));
                    strTreePhotograph = Convert.ToString(dr["TreePhotograph"] ?? "");
                    Cavat = (dr["Cavat"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Cavat"]));
                    StemCount = (dr["StemCount"] == DBNull.Value ? 0 : Convert.ToInt32(dr["StemCount"]));
                    CrownNorth = (dr["CrownNorth"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["CrownNorth"]));
                    CrownSouth = (dr["CrownSouth"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["CrownSouth"]));
                    CrownEast = (dr["CrownEast"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["CrownEast"]));
                    CrownWest = (dr["CrownWest"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["CrownWest"]));
                    RetentionCategory = (dr["RetentionCategory"] == DBNull.Value ? 0 : Convert.ToInt32(dr["RetentionCategory"]));
                    SULE = (dr["SULE"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SULE"]));
                    UserPicklist1_Tree = (dr["UserPicklist1_Tree"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist1_Tree"]));
                    UserPicklist2_Tree = (dr["UserPicklist2_Tree"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist2_Tree"]));
                    UserPicklist3_Tree = (dr["UserPicklist3_Tree"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist3_Tree"]));
                    SpeciesVariety = (dr["SpeciesVariety"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SpeciesVariety"]));
                    ObjectLength = (dr["ObjectLength"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["ObjectLength"]));
                    ObjectWidth = (dr["ObjectWidth"] == DBNull.Value ? (Decimal)0.00 : Convert.ToDecimal(dr["ObjectWidth"]));

                    strInspectionPhotograph = Convert.ToString(dr["InspectionPhotograph"] ?? "");
                    strInspectRef = Convert.ToString(dr["strInspectRef"] ?? "");
                    intInspector = (dr["intInspector"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intInspector"]));
                    dtInspectDate = (dr["dtInspectDate"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtInspectDate"]));
                    intIncidentID = (dr["intIncidentID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intIncidentID"]));
                    intStemPhysical = (dr["intStemPhysical"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intStemPhysical"]));
                    intStemPhysical2 = (dr["intStemPhysical2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intStemPhysical2"]));
                    intStemPhysical3 = (dr["intStemPhysical3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intStemPhysical3"]));
                    intStemDisease = (dr["intStemDisease"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intStemDisease"]));
                    intStemDisease2 = (dr["intStemDisease2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intStemDisease2"]));
                    intStemDisease3 = (dr["intStemDisease3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intStemDisease3"]));
                    intAngleToVertical = (dr["intAngleToVertical"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intAngleToVertical"]));
                    intCrownPhysical = (dr["intCrownPhysical"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownPhysical"]));
                    intCrownPhysical2 = (dr["intCrownPhysical2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownPhysical2"]));
                    intCrownPhysical3 = (dr["intCrownPhysical3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownPhysical3"]));
                    intCrownDisease = (dr["intCrownDisease"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownDisease"]));
                    intCrownDisease2 = (dr["intCrownDisease2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownDisease2"]));
                    intCrownDisease3 = (dr["intCrownDisease3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownDisease3"]));
                    intCrownFoliation = (dr["intCrownFoliation"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownFoliation"]));
                    intCrownFoliation2 = (dr["intCrownFoliation2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownFoliation2"]));
                    intCrownFoliation3 = (dr["intCrownFoliation3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intCrownFoliation3"]));
                    intSafety = (dr["intSafety"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intSafety"]));
                    intGeneralCondition = (dr["intGeneralCondition"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intGeneralCondition"]));
                    intRootHeave = (dr["intRootHeave"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intRootHeave"]));
                    intRootHeave2 = (dr["intRootHeave2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intRootHeave2"]));
                    intRootHeave3 = (dr["intRootHeave3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intRootHeave3"]));
                    strUser1_Inspection = Convert.ToString(dr["strUser1_Inspection"] ?? 0);
                    strUser2_Inspection = Convert.ToString(dr["strUser2_Inspection"] ?? 0);
                    strUser3_Inspection = Convert.ToString(dr["strUser3_Inspection"] ?? 0);
                    strRemarks_Inspection = Convert.ToString(dr["strRemarks_Inspection"] ?? 0);
                    intVitality = (dr["intVitality"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intVitality"]));
                    UserPicklist1_Inspection = (dr["UserPicklist1_Inspection"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist1_Inspection"]));
                    UserPicklist2_Inspection = (dr["UserPicklist2_Inspection"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist2_Inspection"]));
                    UserPicklist3_Inspection = (dr["UserPicklist3_Inspection"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist3_Inspection"]));

                    dtDueDate_1 = (dr["dtDueDate_1"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtDueDate_1"]));
                    dtDoneDate_1 = (dr["dtDoneDate_1"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtDoneDate_1"]));
                    intAction_1 = (dr["intAction_1"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intAction_1"]));
                    intActionBy_1 = (dr["intActionBy_1"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intActionBy_1"]));
                    intSupervisor_1 = (dr["intSupervisor_1"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intSupervisor_1"]));
                    strJobNumber_1 = Convert.ToString(dr["strJobNumber_1"] ?? "");
                    strUser1_Action_1 = Convert.ToString(dr["strUser1_Action_1"] ?? "");
                    strUser2_Action_1 = Convert.ToString(dr["strUser2_Action_1"] ?? "");
                    strUser3_Action_1 = Convert.ToString(dr["strUser3_Action_1"] ?? "");
                    strRemarks_Action_1 = Convert.ToString(dr["strRemarks_Action_1"] ?? "");
                    intWorkOrderID_1 = (dr["intWorkOrderID_1"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intWorkOrderID_1"]));
                    intPriorityID_1 = (dr["intPriorityID_1"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intPriorityID_1"]));
                    intScheduleOfRates_1 = (dr["intScheduleOfRates_1"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intScheduleOfRates_1"]));
                    monJobWorkUnits_1 = (dr["monJobWorkUnits_1"] == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(dr["monJobWorkUnits_1"]));                   
                    intOwnership_1 = (dr["intOwnership_1"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intOwnership_1"]));
                    UserPicklist1_Action_1 = (dr["UserPicklist1_Action_1"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist1_Action_1"]));
                    UserPicklist2_Action_1 = (dr["UserPicklist2_Action_1"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist2_Action_1"]));
                    UserPicklist3_Action_1 = (dr["UserPicklist3_Action_1"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist3_Action_1"]));

                    dtDueDate_2 = (dr["dtDueDate_2"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtDueDate_2"]));
                    dtDoneDate_2 = (dr["dtDoneDate_2"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtDoneDate_2"]));
                    intAction_2 = (dr["intAction_2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intAction_2"]));
                    intActionBy_2 = (dr["intActionBy_2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intActionBy_2"]));
                    intSupervisor_2 = (dr["intSupervisor_2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intSupervisor_2"]));
                    strJobNumber_2 = Convert.ToString(dr["strJobNumber_2"] ?? "");
                    strUser1_Action_2 = Convert.ToString(dr["strUser1_Action_2"] ?? "");
                    strUser2_Action_2 = Convert.ToString(dr["strUser2_Action_2"] ?? "");
                    strUser3_Action_2 = Convert.ToString(dr["strUser3_Action_2"] ?? "");
                    strRemarks_Action_2 = Convert.ToString(dr["strRemarks_Action_2"] ?? "");
                    intWorkOrderID_2 = (dr["intWorkOrderID_2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intWorkOrderID_2"]));
                    intPriorityID_2 = (dr["intPriorityID_2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intPriorityID_2"]));
                    intScheduleOfRates_2 = (dr["intScheduleOfRates_2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intScheduleOfRates_2"]));
                    monJobWorkUnits_2 = (dr["monJobWorkUnits_2"] == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(dr["monJobWorkUnits_2"]));
                    intOwnership_2 = (dr["intOwnership_2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intOwnership_2"]));
                    UserPicklist1_Action_2 = (dr["UserPicklist1_Action_2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist1_Action_2"]));
                    UserPicklist2_Action_2 = (dr["UserPicklist2_Action_2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist2_Action_2"]));
                    UserPicklist3_Action_2 = (dr["UserPicklist3_Action_2"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist3_Action_2"]));

                    dtDueDate_3 = (dr["dtDueDate_3"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtDueDate_3"]));
                    dtDoneDate_3 = (dr["dtDoneDate_3"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtDoneDate_3"]));
                    intAction_3 = (dr["intAction_3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intAction_3"]));
                    intActionBy_3 = (dr["intActionBy_3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intActionBy_3"]));
                    intSupervisor_3 = (dr["intSupervisor_3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intSupervisor_3"]));
                    strJobNumber_3 = Convert.ToString(dr["strJobNumber_3"] ?? "");
                    strUser1_Action_3 = Convert.ToString(dr["strUser1_Action_3"] ?? "");
                    strUser2_Action_3 = Convert.ToString(dr["strUser2_Action_3"] ?? "");
                    strUser3_Action_3 = Convert.ToString(dr["strUser3_Action_3"] ?? "");
                    strRemarks_Action_3 = Convert.ToString(dr["strRemarks_Action_3"] ?? "");
                    intWorkOrderID_3 = (dr["intWorkOrderID_3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intWorkOrderID_3"]));
                    intPriorityID_3 = (dr["intPriorityID_3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intPriorityID_3"]));
                    intScheduleOfRates_3 = (dr["intScheduleOfRates_3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intScheduleOfRates_3"]));
                    monJobWorkUnits_3 = (dr["monJobWorkUnits_3"] == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(dr["monJobWorkUnits_3"]));
                    intOwnership_3 = (dr["intOwnership_3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intOwnership_3"]));
                    UserPicklist1_Action_3 = (dr["UserPicklist1_Action_3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist1_Action_3"]));
                    UserPicklist2_Action_3 = (dr["UserPicklist2_Action_3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist2_Action_3"]));
                    UserPicklist3_Action_3 = (dr["UserPicklist3_Action_3"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist3_Action_3"]));

                    dtDueDate_4 = (dr["dtDueDate_4"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtDueDate_4"]));
                    dtDoneDate_4 = (dr["dtDoneDate_4"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtDoneDate_4"]));
                    intAction_4 = (dr["intAction_4"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intAction_4"]));
                    intActionBy_4 = (dr["intActionBy_4"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intActionBy_4"]));
                    intSupervisor_4 = (dr["intSupervisor_4"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intSupervisor_4"]));
                    strJobNumber_4 = Convert.ToString(dr["strJobNumber_4"] ?? "");
                    strUser1_Action_4 = Convert.ToString(dr["strUser1_Action_4"] ?? "");
                    strUser2_Action_4 = Convert.ToString(dr["strUser2_Action_4"] ?? "");
                    strUser3_Action_4 = Convert.ToString(dr["strUser3_Action_4"] ?? "");
                    strRemarks_Action_4 = Convert.ToString(dr["strRemarks_Action_4"] ?? "");
                    intWorkOrderID_4 = (dr["intWorkOrderID_4"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intWorkOrderID_4"]));
                    intPriorityID_4 = (dr["intPriorityID_4"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intPriorityID_4"]));
                    intScheduleOfRates_4 = (dr["intScheduleOfRates_4"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intScheduleOfRates_4"]));
                    monJobWorkUnits_4 = (dr["monJobWorkUnits_4"] == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(dr["monJobWorkUnits_4"]));
                    intOwnership_4 = (dr["intOwnership_4"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intOwnership_4"]));
                    UserPicklist1_Action_4 = (dr["UserPicklist1_Action_4"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist1_Action_4"]));
                    UserPicklist2_Action_4 = (dr["UserPicklist2_Action_4"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist2_Action_4"]));
                    UserPicklist3_Action_4 = (dr["UserPicklist3_Action_4"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist3_Action_4"]));

                    dtDueDate_5 = (dr["dtDueDate_5"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtDueDate_%"]));
                    dtDoneDate_5 = (dr["dtDoneDate_5"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(dr["dtDoneDate_5"]));
                    intAction_5 = (dr["intAction_5"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intAction_5"]));
                    intActionBy_5 = (dr["intActionBy_5"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intActionBy_5"]));
                    intSupervisor_5 = (dr["intSupervisor_5"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intSupervisor_5"]));
                    strJobNumber_5 = Convert.ToString(dr["strJobNumber_5"] ?? "");
                    strUser1_Action_5 = Convert.ToString(dr["strUser1_Action_5"] ?? "");
                    strUser2_Action_5 = Convert.ToString(dr["strUser2_Action_5"] ?? "");
                    strUser3_Action_5 = Convert.ToString(dr["strUser3_Action_5"] ?? "");
                    strRemarks_Action_5 = Convert.ToString(dr["strRemarks_Action_5"] ?? "");
                    intWorkOrderID_5 = (dr["intWorkOrderID_5"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intWorkOrderID_5"]));
                    intPriorityID_5 = (dr["intPriorityID_5"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intPriorityID_5"]));
                    intScheduleOfRates_5 = (dr["intScheduleOfRates_5"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intScheduleOfRates_5"]));
                    monJobWorkUnits_5 = (dr["monJobWorkUnits_5"] == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(dr["monJobWorkUnits_5"]));
                    intOwnership_5 = (dr["intOwnership_5"] == DBNull.Value ? 0 : Convert.ToInt32(dr["intOwnership_5"]));
                    UserPicklist1_Action_5 = (dr["UserPicklist1_Action_5"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist1_Action_5"]));
                    UserPicklist2_Action_5 = (dr["UserPicklist2_Action_5"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist2_Action_5"]));
                    UserPicklist3_Action_5 = (dr["UserPicklist3_Action_5"] == DBNull.Value ? 0 : Convert.ToInt32(dr["UserPicklist3_Action_5"]));

                    ImportData.sp00140_import_from_GBM_merge_data(dtImportStartTime, ID, intLocalityID, strTreeRef, intX, intY, strPolygonXY, intStatus, intAgeClass,
                                                                    intDistance, strHouseName, intSpeciesID, intAccess, intVisibility, strContext, strManagement,
                                                                    intLegalStatus, dtLastInspectionDate, intInspectionCycle, strInspectionUnit, dtNextInspectionDate,
                                                                    intGroundType, decHeight, intHeightRange, intDBH, intDBHRange, intProtectionType, intCrownDiameter,
                                                                    intCrownDepth, dtPlantDate, intGroupNo, intSafetyPriority, intSiteHazardClass, intPlantSize,
                                                                    intPlantSource, intPlantMethod, strPostcode, strMapLabel, intSize, intNearbyObject, intNearbyObject2,
                                                                    intNearbyObject3, strRemarks_Tree, strUser1_Tree, strUser2_Tree, strUser3_Tree, decAreaHa,
                                                                    intType, strHedgeOwner, intLength, decGardenSize, decPropertyProximity, decSiteLevel, intReplantCount,
                                                                    dtSurveyDate, intTreeOwnership, Cavat, StemCount, CrownNorth, CrownSouth, CrownEast, CrownWest, RetentionCategory,
                                                                    SULE, UserPicklist1_Tree, UserPicklist2_Tree, UserPicklist3_Tree, SpeciesVariety, ObjectLength, ObjectWidth,
                                                                    LocationX, LocationY, LocationPolyXY,
                                                                    
                                                                    strInspectRef, intInspector, dtInspectDate, intIncidentID,
                                                                    intStemPhysical, intStemPhysical2, intStemPhysical3, intStemDisease, intStemDisease2, intStemDisease3,
                                                                    intAngleToVertical, intCrownPhysical, intCrownPhysical2, intCrownPhysical3, intCrownDisease,
                                                                    intCrownDisease2, intCrownDisease3, intCrownFoliation, intCrownFoliation2, intCrownFoliation3,
                                                                    intSafety, intGeneralCondition, intRootHeave, intRootHeave2, intRootHeave3, strUser1_Inspection,
                                                                    strUser2_Inspection, strUser3_Inspection, strRemarks_Inspection, intVitality, UserPicklist1_Inspection,
                                                                    UserPicklist2_Inspection, UserPicklist3_Inspection,

                                                                    dtDueDate_1, dtDoneDate_1, intAction_1, intActionBy_1, intSupervisor_1, strJobNumber_1,
                                                                    strUser1_Action_1, strUser2_Action_1, strUser3_Action_1, strRemarks_Action_1, intWorkOrderID_1,
                                                                    intPriorityID_1, intScheduleOfRates_1, monJobWorkUnits_1,intOwnership_1, UserPicklist1_Action_1, UserPicklist2_Action_1,
                                                                    UserPicklist3_Action_1,

                                                                    dtDueDate_2, dtDoneDate_2, intAction_2, intActionBy_2, intSupervisor_2, strJobNumber_2,
                                                                    strUser1_Action_2, strUser2_Action_2, strUser3_Action_2, strRemarks_Action_2, intWorkOrderID_2,
                                                                    intPriorityID_2, intScheduleOfRates_2, monJobWorkUnits_2, intOwnership_2, UserPicklist1_Action_2, UserPicklist2_Action_2,
                                                                    UserPicklist3_Action_2,

                                                                    dtDueDate_3, dtDoneDate_3, intAction_3, intActionBy_3, intSupervisor_3, strJobNumber_3,
                                                                    strUser1_Action_3, strUser2_Action_3, strUser3_Action_3, strRemarks_Action_3, intWorkOrderID_3,
                                                                    intPriorityID_3, intScheduleOfRates_3, monJobWorkUnits_3, intOwnership_3, UserPicklist1_Action_3, UserPicklist2_Action_3,
                                                                    UserPicklist3_Action_3,

                                                                    dtDueDate_4, dtDoneDate_4, intAction_4, intActionBy_4, intSupervisor_4, strJobNumber_4,
                                                                    strUser1_Action_4, strUser2_Action_4, strUser3_Action_4, strRemarks_Action_4, intWorkOrderID_4,
                                                                    intPriorityID_4, intScheduleOfRates_4, monJobWorkUnits_4, intOwnership_4, UserPicklist1_Action_4, UserPicklist2_Action_4,
                                                                    UserPicklist3_Action_4,

                                                                    dtDueDate_5, dtDoneDate_5, intAction_5, intActionBy_5, intSupervisor_5, strJobNumber_5,
                                                                    strUser1_Action_5, strUser2_Action_5, strUser3_Action_5, strRemarks_Action_5, intWorkOrderID_5,
                                                                    intPriorityID_5, intScheduleOfRates_5, monJobWorkUnits_5, intOwnership_5, UserPicklist1_Action_5, UserPicklist2_Action_5,
                                                                    UserPicklist3_Action_5,

                                                                    strTreePhotograph, strInspectionPhotograph);
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }

            }
            catch(Exception Ex)
            {
                // Error occurred, so Roll Back the whole process [delete any added inspections\actions so data can be re-imported with duplicated being created] //
                dr.RowError = "Error(s) Present...\nWarning: Invalid Data Within Row - Unable To Import!";
                view.ClearSelection();
                view.SelectRow(intCurrentRow);
                view.MakeRowVisible(intCurrentRow, false);

                DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter RollbackImportData = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                RollbackImportData.ChangeConnectionString(strConnectionString);
                try
                {
                    RollbackImportData.sp00141_import_from_GBM_merge_data_Rollback(dtImportStartTime);
                }
                catch (Exception Ex2)
                {
                    view.EndSort();
                    view.EndDataUpdate();
                    view.EndUpdate();
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred during the import process [Error Triggering Rollback:" + Ex.Message + "] [Error During Rollback: " + Ex2.Message + "]!\n\nThe system attempted to roll back any new records created but it was unable to complete the process!\n\nAny records within the Action, Inspection and Tree tables with a [DateAdded] date of " + dtImportStartTime.ToString() + " should be deleted manually before any subsequent attempt is made to re-import the data. Use Utilities... Rollback Import button on this screen or Contact Technical support for further help.", "Import Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                view.EndSort();
                view.EndDataUpdate();
                view.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close(); // Close Progress Window //
                    fProgress = null;
                }

                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred during the import process [" + Ex.Message + "]!\n\nThe system successfully removed any new records added.\n\nThe selected import file should be re-imported once you have corrected the problem. The row containing the problem has been highlighted and an error icon has been shown to the left of it.", "Import Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            view.EndSort();
            view.EndDataUpdate();
            view.EndUpdate();

            // Write to Import Log //
            try
            {
                DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter WriteToLog = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                WriteToLog.ChangeConnectionString(strConnectionString);
                FileInfo file = new FileInfo(ImportFileButtonEdit.EditValue.ToString());
                string strImportPath = file.DirectoryName;
                WriteToLog.sp00142_import_from_GBM_log_import(strImportPath, GlobalSettings.Username, dtImportStartTime, 0);  // 0 = Survey, 1 = WorkOrders //
            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while writing to the Import Log [" + Ex.Message + "]!\n\nYou will not be able to roll back this import!", "Import Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }


            if (fProgress != null)
            {
                fProgress.Close();
                fProgress.Dispose();
            }

            this.dataSet_AT_DataTransfer.sp00134_export_to_GBM_export_data.Rows.Clear();  // Clear all the rows so the user doesn't accidentally import them again! //

            DevExpress.XtraEditors.XtraMessageBox.Show("Data Imported Successfully", "Import Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
       }

        private void CheckData(GridView view, int intRow, ref int intActualErrorCount, ref int intPossibleErrorCount, ref int intPictureErrorCount, ref string strMissingPictures, string strDefaultPicturePath)
        {
            /*System.Collections.Generic.List<double> listXvalues = new System.Collections.Generic.List<double>();
            System.Collections.Generic.List<double> listYvalues = new System.Collections.Generic.List<double>();      
            double xStdDev = 0;
            double yStdDev = 0;
            GridView view = (GridView)gridControl7.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (view.GetRowCellValue(i, "intX") != null) listXvalues.Add(Convert.ToDouble(view.GetRowCellValue(i, "intX")));
                if (view.GetRowCellValue(i, "intY") != null) listYvalues.Add(Convert.ToDouble(view.GetRowCellValue(i, "intY")));
            }
            xStdDev = Mathematical_Functions.StandardDeviation(listXvalues.ToArray());
            yStdDev = Mathematical_Functions.StandardDeviation(listYvalues.ToArray());*/

            /*FileInfo file  = new FileInfo(ImportFileButtonEdit.EditValue.ToString());
            string filePath = file.DirectoryName;
            string fileName = file.Name.Substring(0, file.Name.Length - 4);

            string[] lines = File.ReadAllLines(filePath + "\\" + fileName + ".mif");
            foreach (string line in lines)
            {
                if (!line.StartsWith("CoordSys")) continue;  // Go to next iteration of loop //
                try
                {
                    string strBounds = line.Substring(line.IndexOf("Bounds"));
                    doubleImportXMin = Convert.ToDouble(strBounds.Substring(strBounds.IndexOf('(') + 1, strBounds.IndexOf(',') - (strBounds.IndexOf('(') + 1)));
                    doubleImportYMin = Convert.ToDouble(strBounds.Substring(strBounds.IndexOf(',') + 2, strBounds.IndexOf(')') - (strBounds.IndexOf(',') + 2)));
                    strBounds = strBounds.Substring(strBounds.IndexOf(')') + 2);
                    doubleImportXMax = Convert.ToDouble(strBounds.Substring(1, strBounds.IndexOf(',') - 1));
                    doubleImportYMax = Convert.ToDouble(strBounds.Substring(strBounds.IndexOf(',') + 2, strBounds.IndexOf(')') - (strBounds.IndexOf(',') + 2)));
                }
                catch(Exception)
                {
                }
                break;
            }*/
            DataRow dr = view.GetDataRow(intRow);
            dr.ClearErrors();
            string[] strArray;
            string strErrorMessage = "";
            int intPolygonBoundaryXErrors = 0;
            int intPolygonBoundaryYErrors = 0;
            char[] delimiters = new char[] { ';' };
           /* 
            // *** Check strPolygonXY and intX and intY *** //
            if (view.GetRowCellValue(intRow, "strPolygonXY") == null || view.GetRowCellValue(intRow, "strPolygonXY").ToString() == "")
                {
                if (view.GetRowCellValue(intRow, "intX") == null  || Convert.ToDouble(view.GetRowCellValue(intRow, "intX")) == (double)0.00)
                {
                    dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Missing value!");
                    intActualErrorCount++;
                }
                else if (Convert.ToDouble(view.GetRowCellValue(intRow, "intX")) < doubleImportXMin || Convert.ToDouble(view.GetRowCellValue(intRow, "intX")) > doubleImportXMax)
                {
                    dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Value is outwith the bounds.");
                    intPossibleErrorCount++;
                }

                if (view.GetRowCellValue(intRow, "intY") == null || Convert.ToDouble(view.GetRowCellValue(intRow, "intY")) == (double)0.00)
                {
                    dr.SetColumnError("intY", String.Format("{0}: Missing value!", view.Columns["intY"].Caption));
                    intActualErrorCount++;
                }
                else if (Convert.ToDouble(view.GetRowCellValue(intRow, "intY")) < doubleImportYMin || Convert.ToDouble(view.GetRowCellValue(intRow, "intY")) > doubleImportYMax)
                {
                    dr.SetColumnError("intY", view.Columns["intY"].Caption + ": Value is outwith the bounds.");
                    intPossibleErrorCount++;
                }
            }
            else  // strPolygonXY contains a value //
            {
                //if (view.GetRowCellValue(intRow, "intX") != null && Convert.ToDouble(view.GetRowCellValue(intRow, "intX")) != (double)0.00)
                if (view.GetRowCellValue(intRow, "intX") != DBNull.Value)
                {
                    if (Convert.ToDouble(view.GetRowCellValue(intRow, "intX")) != (double)0.00)
                    {
                        dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Cannot have a value when " + view.Columns["strPolygonXY"].Caption + " has a value!");
                        intActualErrorCount++;
                    }
                }
                //if (view.GetRowCellValue(intRow, "intY") != null && Convert.ToDouble(view.GetRowCellValue(intRow, "intY")) != (double)0.00)
                if (view.GetRowCellValue(intRow, "intY") != DBNull.Value)
                {
                    if (Convert.ToDouble(view.GetRowCellValue(intRow, "intY")) != (double)0.00)
                    {
                        dr.SetColumnError("intY", String.Format("{0}: Cannot have a value when " + String.Format("{0} has a value!", view.Columns["strPolygonXY"].Caption), view.Columns["intY"].Caption));
                        intActualErrorCount++;
                    }
                }
                // Check all points within polygon to ensure that are within the bounds //
                strArray = view.GetRowCellValue(intRow, "strPolygonXY").ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intPolygonBoundaryXErrors = 0;
                intPolygonBoundaryYErrors = 0;
                if (strArray.Length % 2 != 0)  // Modulus (%) //
                {
                    dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Invalid number of coordinates - must be an even number!");
                    intActualErrorCount++;
                }
                else if (strArray.Length < 8)
                {
                    dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Invalid number of coordinates - must be an least 8!");
                    intActualErrorCount++;
                }
                else if (strArray[0] != strArray[strArray.Length - 2] || strArray[1] != strArray[strArray.Length - 1])  // Value 1 lower than expected as array starts at 0 //
                {
                    dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Invalid coordinates - first pair and last pair must match to close polygon!");
                    intActualErrorCount++;
                }
                else
                {
                    try
                    {
                        for (int j = 0; j < strArray.Length - 1; j++)
                        {
                            if (Convert.ToDouble(strArray[j]) < doubleImportXMin || Convert.ToDouble(strArray[j]) > doubleImportXMax) intPolygonBoundaryXErrors++;
                            if (Convert.ToDouble(strArray[j + 1]) < doubleImportYMin || Convert.ToDouble(strArray[j + 1]) > doubleImportYMax) intPolygonBoundaryYErrors++;
                            j++;  // Jump ahead so that we are always processing odd numbered array items //
                        }
                        if (intPolygonBoundaryXErrors > 0 || intPolygonBoundaryYErrors > 0)
                        {
                            strErrorMessage = (intPolygonBoundaryXErrors > 0 ? view.Columns["intX"].Caption + ": " + intPolygonBoundaryXErrors.ToString() + " outwith the Bounds!" : "");
                            strErrorMessage += (strErrorMessage == "" ? (intPolygonBoundaryYErrors > 0 ? view.Columns["intY"].Caption + ": " + intPolygonBoundaryYErrors.ToString() + " outwith the Bounds!" : "") : "\n" + (intPolygonBoundaryYErrors > 0 ? view.Columns["intY"].Caption + ": " + intPolygonBoundaryYErrors.ToString() + " outwith the Bounds!" : ""));
                            dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": " + strErrorMessage);
                            intPossibleErrorCount++;
                        }
                    }
                    catch (Exception)
                    {
                        dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Problem with entered points [must be numeric and seperated by ';']!");
                        intActualErrorCount++;
                    }
                }
            }
            */
            // *** Check strPolygonXY and intX and intY *** //
            if (!string.IsNullOrEmpty(view.GetRowCellValue(intRow, "strPolygonXY").ToString()))
            {
                // Check all points within polygon to ensure that are within the bounds //
                strArray = view.GetRowCellValue(intRow, "strPolygonXY").ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intPolygonBoundaryXErrors = 0;
                intPolygonBoundaryYErrors = 0;
                if (strArray.Length % 2 != 0)  // Modulus (%) //
                {
                    dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Invalid number of coordinates - must be an even number!");
                    intActualErrorCount++;
                }
                else if (strArray.Length < 6)  // was 8 but changed to 6 as polyline only needs 3 sets of coords (polygon needs at least 4 sets) //
                {
                    dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Invalid number of coordinates - must be an least 6!");
                    intActualErrorCount++;
                }
                //else if (strArray[0] != strArray[strArray.Length - 2] || strArray[1] != strArray[strArray.Length - 1])  // Value 1 lower than expected as array starts at 0 //
                //{
                //    dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Invalid coordinates - first pair and last pair must match to close polygon!");
                //    intActualErrorCount++;
                //}
                else
                {
                    try
                    {
                        for (int j = 0; j < strArray.Length - 1; j++)
                        {
                            if (Convert.ToDouble(strArray[j]) < doubleImportXMin || Convert.ToDouble(strArray[j]) > doubleImportXMax) intPolygonBoundaryXErrors++;
                            if (Convert.ToDouble(strArray[j + 1]) < doubleImportYMin || Convert.ToDouble(strArray[j + 1]) > doubleImportYMax) intPolygonBoundaryYErrors++;
                            j++;  // Jump ahead so that we are always processing odd numbered array items //
                        }
                        if (intPolygonBoundaryXErrors > 0 || intPolygonBoundaryYErrors > 0)
                        {
                            strErrorMessage = (intPolygonBoundaryXErrors > 0 ? view.Columns["intX"].Caption + ": " + intPolygonBoundaryXErrors.ToString() + " outwith the Bounds!" : "");
                            strErrorMessage += (strErrorMessage == "" ? (intPolygonBoundaryYErrors > 0 ? view.Columns["intY"].Caption + ": " + intPolygonBoundaryYErrors.ToString() + " outwith the Bounds!" : "") : "\n" + (intPolygonBoundaryYErrors > 0 ? view.Columns["intY"].Caption + ": " + intPolygonBoundaryYErrors.ToString() + " outwith the Bounds!" : ""));
                            dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": " + strErrorMessage);
                            intPossibleErrorCount++;
                        }
                    }
                    catch (Exception)
                    {
                        dr.SetColumnError("strPolygonXY", view.Columns["strPolygonXY"].Caption + ": Problem with entered points [must be numeric and seperated by ';']!");
                        intActualErrorCount++;
                    }
                }
            }
            
            if (view.GetRowCellValue(intRow, "intX") == DBNull.Value)
            {
                dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Missing value!");
                intActualErrorCount++;
            }
            else if (Convert.ToDouble(view.GetRowCellValue(intRow, "intX")) == (double)0.00)
            {
              dr.SetColumnError("intX", view.Columns["intX"].Caption + ": Missing value!");
              intActualErrorCount++;
            }

            if (view.GetRowCellValue(intRow, "intY") == DBNull.Value)
            {
                dr.SetColumnError("intY", view.Columns["intY"].Caption + ": Missing value!");
                intActualErrorCount++;
            }
            else if (Convert.ToDouble(view.GetRowCellValue(intRow, "intY")) == (double)0.00)
            {
                dr.SetColumnError("intY", view.Columns["intY"].Caption + ": Missing value!");
                intActualErrorCount++;
            }

            if (view.GetRowCellValue(intRow, "intDistrictID") == null || Convert.ToInt32(view.GetRowCellValue(intRow, "intDistrictID")) == 0)
            {
                dr.SetColumnError("intDistrictID", view.Columns["intDistrictID"].Caption + ": Must have a value!");
                intActualErrorCount++;
            }
            if (view.GetRowCellValue(intRow, "intLocalityID") == null || Convert.ToInt32(view.GetRowCellValue(intRow, "intLocalityID")) == 0)
            {
                dr.SetColumnError("intLocalityID", view.Columns["intLocalityID"].Caption + ": Must have a value!");
                intActualErrorCount++;
            }
            if (view.GetRowCellValue(intRow, "intStatus") == null || Convert.ToInt32(view.GetRowCellValue(intRow, "intStatus")) == 0)
            {
                dr.SetColumnError("intStatus", view.Columns["intStatus"].Caption + ": Must have a value!");
                intActualErrorCount++;
                //view.SetColumnError(view.Columns["intStatus"], String.Format("{0}: Must have a value.", view.Columns["intStatus"].Caption), DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning);
            }

            if (!string.IsNullOrEmpty(view.GetRowCellValue(intRow, "TreePhotograph").ToString()))
            {
                if (!File.Exists(strDefaultPicturePath + view.GetRowCellValue(intRow, "TreePhotograph").ToString()))
                {
                    dr.SetColumnError("TreePhotograph", view.Columns["TreePhotograph"].Caption + ": Missing!");
                    intPictureErrorCount++;
                    strMissingPictures += strDefaultPicturePath + view.GetRowCellValue(intRow, "TreePhotograph").ToString() + "\r\n";
                }
            }

            if (!string.IsNullOrEmpty(view.GetRowCellValue(intRow, "InspectionPhotograph").ToString()))
            {
                if (!File.Exists(strDefaultPicturePath + view.GetRowCellValue(intRow, "InspectionPhotograph").ToString()))
                {
                    dr.SetColumnError("InspectionPhotograph", view.Columns["InspectionPhotograph"].Caption + ": Missing!");
                    intPictureErrorCount++;
                    strMissingPictures += strDefaultPicturePath + view.GetRowCellValue(intRow, "InspectionPhotograph").ToString() + "\r\n";
                }
            }

            // Critical Errors are denoted with an "!" at the end of them - if no critical erors then don't display RowError Warning icon in row selector column //
            string strCriticalErrors = "";
            DataColumn[] ErrorColumns = dr.GetColumnsInError();
            foreach (DataColumn dc in ErrorColumns)
            {
                if (dr.GetColumnError(dc).ToString().EndsWith("!"))
                {
                    strCriticalErrors+= dr.GetColumnError(dc) + "\n";
                }
            }
            if (strCriticalErrors == "")
            {
                dr.RowError = "";
            }
            else
            {
                dr.RowError = "Error(s) Present...\n" + strCriticalErrors;
            }
        }

        private void repositoryItemGridLookUpEditLocality_Popup(object sender, EventArgs e)
        {
            // Autosize Grid lookup and its columns to show all columns //
            /*GridLookUpEdit view = sender as GridLookUpEdit;
            view.Properties.View.BestFitColumns();

            GridViewInfo vinfo = view.Properties.View.GetViewInfo() as GridViewInfo;
            Size clientSize = view.Properties.View.GridControl.FindForm().ClientSize;
            clientSize.Width = Math.Min(SystemInformation.WorkingArea.Width - 10, vinfo.ViewRects.ColumnTotalWidth + vinfo.ViewRects.IndicatorWidth + SystemInformation.VerticalScrollBarWidth + 10);
            view.Properties.View.GridControl.FindForm().ClientSize = clientSize;
            */
        }

        private void repositoryItemGridLookUpEditDistrict_EditValueChanged(object sender, EventArgs e)
        {

            GridView localityLookupView = (GridView)repositoryItemGridLookUpEditLocality.View;
            int intLocalityDistrict = Convert.ToInt32(localityLookupView.GetRowCellValue(localityLookupView.FocusedRowHandle, "DistrictID"));

            GridView districtLookupView = (GridView)repositoryItemGridLookUpEditDistrict.View;
            int intDistrict = Convert.ToInt32(districtLookupView.GetRowCellValue(districtLookupView.FocusedRowHandle, "DistrictID"));

            GridView view = (GridView)gridControl7.MainView;
            int intFocusedRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intFocusedRow);
            dr.SetColumnError("intLocalityID", "");
            if (intLocalityDistrict != intDistrict)
            {
                view.SetRowCellValue(intFocusedRow, "intLocalityID", 0);
                dr.SetColumnError("intLocalityID", String.Format("{0}: Must have a value", view.Columns["intLocalityID"].Caption));
            }
        }

        private void repositoryItemGridLookUpEditLocality_EditValueChanged(object sender, EventArgs e)
        {
            GridView localityLookupView = (GridView)repositoryItemGridLookUpEditLocality.View;
            int intDistrict = Convert.ToInt32(localityLookupView.GetRowCellValue(localityLookupView.FocusedRowHandle, "DistrictID"));
            GridView view = (GridView)gridControl7.MainView;
            int intFocusedRow = view.FocusedRowHandle;
            DataRow dr = view.GetDataRow(intFocusedRow);
            dr.SetColumnError("intDistrictID", "");
            if (Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "intDistrictID")) != intDistrict)
            {
                view.SetRowCellValue(intFocusedRow, "intDistrictID", intDistrict);
                if (intDistrict <= 0)
                {
                    dr.SetColumnError("intDistrictID", String.Format("{0}: Must have a value", view.Columns["intDistrictID"].Caption));
                }
            }
        }

        private void btnSelectRecords_Click(object sender, EventArgs e)
        {
            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Processing...");
            fProgress.Show();
            Application.DoEvents();

            GridView view = (GridView) gridControl7.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow()) return;

            int intUpdateProgressThreshhold = view.DataRowCount / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
            int intCount = 0;

            if (checkEditClearPriorSelection.Checked)
            {
                view.ClearSelection();
            }

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (checkEditSelectTreeRef.Checked)
                {
                    if (string.IsNullOrEmpty(view.GetRowCellValue(i, "strTreeRef").ToString().Trim()))
                        {
                        view.SelectRow(i);
                        view.MakeRowVisible(i, false);
                        intCount++;
                    }
                }
                else if (checkEditSelectInspRef.Checked)
                {
                    if (string.IsNullOrEmpty(view.GetRowCellValue(i, "strInspectRef").ToString().Trim())
                            && ((view.GetRowCellValue(i, "intInspector") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intInspector")) != 0)
                                || (view.GetRowCellValue(i, "dtInspectDate") != DBNull.Value && view.GetRowCellValue(i, "dtInspectDate").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intIncidentID") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intIncidentID")) != 0)
                                || (view.GetRowCellValue(i, "intStemPhysical") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intStemPhysical")) != 0)
                                || (view.GetRowCellValue(i, "intStemPhysical2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intStemPhysical2")) != 0)
                                || (view.GetRowCellValue(i, "intStemPhysical3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intStemPhysical3")) != 0)
                                || (view.GetRowCellValue(i, "intStemDisease") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intStemDisease")) != 0)
                                || (view.GetRowCellValue(i, "intStemDisease2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intStemDisease2")) != 0)
                                || (view.GetRowCellValue(i, "intStemDisease3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intStemDisease3")) != 0)
                                || (view.GetRowCellValue(i, "intAngleToVertical") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intAngleToVertical")) != 0)
                                || (view.GetRowCellValue(i, "intCrownPhysical") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intCrownPhysical")) != 0)
                                || (view.GetRowCellValue(i, "intCrownPhysical2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intCrownPhysical2")) != 0)
                                || (view.GetRowCellValue(i, "intCrownPhysical3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intCrownPhysical3")) != 0)
                                || (view.GetRowCellValue(i, "intCrownDisease") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intCrownDisease")) != 0)
                                || (view.GetRowCellValue(i, "intCrownDisease2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intCrownDisease2")) != 0)
                                || (view.GetRowCellValue(i, "intCrownDisease3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intCrownDisease3")) != 0)
                                || (view.GetRowCellValue(i, "intCrownFoliation") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intCrownFoliation")) != 0)
                                || (view.GetRowCellValue(i, "intCrownFoliation2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intCrownFoliation2")) != 0)
                                || (view.GetRowCellValue(i, "intCrownFoliation3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intCrownFoliation3")) != 0)
                                || (view.GetRowCellValue(i, "intSafety") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intSafety")) != 0)
                                || (view.GetRowCellValue(i, "intGeneralCondition") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intGeneralCondition")) != 0)
                                || (view.GetRowCellValue(i, "strUser1_Inspection") != DBNull.Value && view.GetRowCellValue(i, "strUser1_Inspection").ToString().Trim() != "")
                                || (view.GetRowCellValue(i, "strUser2_Inspection") != DBNull.Value && view.GetRowCellValue(i, "strUser2_Inspection").ToString().Trim() != "")
                                || (view.GetRowCellValue(i, "strUser3_Inspection") != DBNull.Value && view.GetRowCellValue(i, "strUser3_Inspection").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strRemarks_Inspection") != null && view.GetRowCellValue(i, "strRemarks_Inspection").ToString().Trim() != "")
                                || (view.GetRowCellValue(i, "intVitality") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intVitality")) != 0)
                                || (view.GetRowCellValue(i, "Cavat") != DBNull.Value && Convert.ToDecimal(view.GetRowCellValue(i, "Cavat")) != (decimal)0.00)
                                || (view.GetRowCellValue(i, "StemCount") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "StemCount")) != 0)
                                || (view.GetRowCellValue(i, "CrownNorth") != DBNull.Value && Convert.ToDecimal(view.GetRowCellValue(i, "CrownNorth")) != (decimal)0.00)
                                || (view.GetRowCellValue(i, "CrownSouth") != DBNull.Value && Convert.ToDecimal(view.GetRowCellValue(i, "CrownSouth")) != (decimal)0.00)
                                || (view.GetRowCellValue(i, "CrownEast") != DBNull.Value && Convert.ToDecimal(view.GetRowCellValue(i, "CrownEast")) != (decimal)0.00)
                                || (view.GetRowCellValue(i, "CrownWest") != DBNull.Value && Convert.ToDecimal(view.GetRowCellValue(i, "CrownWest")) != (decimal)0.00)
                                || (view.GetRowCellValue(i, "RetentionCategory") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "RetentionCategory")) != 0)
                                || (view.GetRowCellValue(i, "SULE") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "SULE")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist1_Tree") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist1_Tree")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist2_Tree") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist2_Tree")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist3_Tree") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist3_Tree")) != 0)
                                || (view.GetRowCellValue(i, "SpeciesVariety") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "SpeciesVariety")) != 0)
                                || (view.GetRowCellValue(i, "ObjectLength") != DBNull.Value && Convert.ToDecimal(view.GetRowCellValue(i, "ObjectLength")) != (decimal)0.00)
                                || (view.GetRowCellValue(i, "ObjectWidth") != DBNull.Value && Convert.ToDecimal(view.GetRowCellValue(i, "ObjectWidth")) != (decimal)0.00) ))
                    {
                        view.SelectRow(i);
                        view.MakeRowVisible(i, false);
                        intCount++;
                    }
                }
                else if (checkEditSelectAct1Ref.Checked)
                {
                    if (string.IsNullOrEmpty(view.GetRowCellValue(i, "strJobNumber_1").ToString().Trim())
                            && ((view.GetRowCellValue(i, "dtDueDate_1") != DBNull.Value && view.GetRowCellValue(i, "dtDueDate_1").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "dtDoneDate_1") != DBNull.Value && view.GetRowCellValue(i, "dtDoneDate_1").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intAction_1") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intAction_1")) != 0)
                                || (view.GetRowCellValue(i, "intActionBy_1") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intActionBy_1")) != 0)
                                || (view.GetRowCellValue(i, "intSupervisor_1") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intSupervisor_1")) != 0)
                                || (view.GetRowCellValue(i, "strUser1_Action_1") != DBNull.Value && view.GetRowCellValue(i, "strUser1_Action_1").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strUser2_Action_1") != DBNull.Value && view.GetRowCellValue(i, "strUser2_Action_1").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strUser3_Action_1") != DBNull.Value && view.GetRowCellValue(i, "strUser3_Action_1").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strRemarks_Action_1") != null && view.GetRowCellValue(i, "strRemarks_Action_1").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intWorkOrderID_1") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intWorkOrderID_1")) != 0)
                                || (view.GetRowCellValue(i, "intPriorityID_1") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intPriorityID_1")) != 0)
                                || (view.GetRowCellValue(i, "intScheduleOfRates_1") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intScheduleOfRates_1")) != 0)
                                || (view.GetRowCellValue(i, "intOwnership_1") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intOwnership_1")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist1_Action_1") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist1_Action_1")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist2_Action_1") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist2_Action_1")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist3_Action_1") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist3_Action_1")) != 0) ))
                    {
                        view.SelectRow(i);
                        view.MakeRowVisible(i, false);
                        intCount++;
                    }
                }
                else if (checkEditSelectAct2Ref.Checked)
                {
                    if (string.IsNullOrEmpty(view.GetRowCellValue(i, "strJobNumber_2").ToString().Trim())
                            && ((view.GetRowCellValue(i, "dtDueDate_2") != DBNull.Value && view.GetRowCellValue(i, "dtDueDate_2").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "dtDoneDate_2") != DBNull.Value && view.GetRowCellValue(i, "dtDoneDate_2").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intAction_2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intAction_2")) != 0)
                                || (view.GetRowCellValue(i, "intActionBy_2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intActionBy_2")) != 0)
                                || (view.GetRowCellValue(i, "intSupervisor_2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intSupervisor_2")) != 0)
                                || (view.GetRowCellValue(i, "strUser1_Action_2") != DBNull.Value && view.GetRowCellValue(i, "strUser1_Action_2").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strUser2_Action_2") != DBNull.Value && view.GetRowCellValue(i, "strUser2_Action_2").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strUser3_Action_2") != DBNull.Value && view.GetRowCellValue(i, "strUser3_Action_2").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strRemarks_Action_2") != null && view.GetRowCellValue(i, "strRemarks_Action_2").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intWorkOrderID_2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intWorkOrderID_2")) != 0)
                                || (view.GetRowCellValue(i, "intPriorityID_2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intPriorityID_2")) != 0)
                                || (view.GetRowCellValue(i, "intScheduleOfRates_2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intScheduleOfRates_2")) != 0)
                                || (view.GetRowCellValue(i, "intOwnership_2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intOwnership_2")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist1_Action_2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist1_Action_2")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist2_Action_2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist2_Action_2")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist3_Action_2") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist3_Action_2")) != 0) ))
                    {
                        view.SelectRow(i);
                        view.MakeRowVisible(i, false);
                        intCount++;
                    }
                }
                else if (checkEditSelectAct3Ref.Checked)
                {
                    if (string.IsNullOrEmpty(view.GetRowCellValue(i, "strJobNumber_3").ToString().Trim())
                            && ((view.GetRowCellValue(i, "dtDueDate_3") != DBNull.Value && view.GetRowCellValue(i, "dtDueDate_3").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "dtDoneDate_3") != DBNull.Value && view.GetRowCellValue(i, "dtDoneDate_3").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intAction_3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intAction_3")) != 0)
                                || (view.GetRowCellValue(i, "intActionBy_3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intActionBy_3")) != 0)
                                || (view.GetRowCellValue(i, "intSupervisor_3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intSupervisor_3")) != 0)
                                || (view.GetRowCellValue(i, "strUser1_Action_3") != DBNull.Value && view.GetRowCellValue(i, "strUser1_Action_3").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strUser2_Action_3") != DBNull.Value && view.GetRowCellValue(i, "strUser2_Action_3").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strUser3_Action_3") != DBNull.Value && view.GetRowCellValue(i, "strUser3_Action_3").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strRemarks_Action_3") != null && view.GetRowCellValue(i, "strRemarks_Action_3").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intWorkOrderID_3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intWorkOrderID_3")) != 0)
                                || (view.GetRowCellValue(i, "intPriorityID_3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intPriorityID_3")) != 0)
                                || (view.GetRowCellValue(i, "intScheduleOfRates_3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intScheduleOfRates_3")) != 0)
                                || (view.GetRowCellValue(i, "intOwnership_3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intOwnership_3")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist1_Action_3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist1_Action_3")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist2_Action_3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist2_Action_3")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist3_Action_3") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist3_Action_3")) != 0) ))
                    {
                        view.SelectRow(i);
                        view.MakeRowVisible(i, false);
                        intCount++;
                    }
                }
                else if (checkEditSelectAct4Ref.Checked)
                {
                    if (string.IsNullOrEmpty(view.GetRowCellValue(i, "strJobNumber_4").ToString().Trim())
                            && ((view.GetRowCellValue(i, "dtDueDate_4") != DBNull.Value && view.GetRowCellValue(i, "dtDueDate_4").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "dtDoneDate_4") != DBNull.Value && view.GetRowCellValue(i, "dtDoneDate_4").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intAction_4") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intAction_4")) != 0)
                                || (view.GetRowCellValue(i, "intActionBy_4") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intActionBy_4")) != 0)
                                || (view.GetRowCellValue(i, "intSupervisor_4") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intSupervisor_4")) != 0)
                                || (view.GetRowCellValue(i, "strUser1_Action_4") != DBNull.Value && view.GetRowCellValue(i, "strUser1_Action_4").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strUser2_Action_4") != DBNull.Value && view.GetRowCellValue(i, "strUser2_Action_4").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strUser3_Action_4") != DBNull.Value && view.GetRowCellValue(i, "strUser3_Action_4").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strRemarks_Action_4") != null && view.GetRowCellValue(i, "strRemarks_Action_4").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intWorkOrderID_4") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intWorkOrderID_4")) != 0)
                                || (view.GetRowCellValue(i, "intPriorityID_4") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intPriorityID_4")) != 0)
                                || (view.GetRowCellValue(i, "intScheduleOfRates_4") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intScheduleOfRates_4")) != 0)
                                || (view.GetRowCellValue(i, "intOwnership_4") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intOwnership_4")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist1_Action_4") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist1_Action_4")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist2_Action_4") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist2_Action_4")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist3_Action_4") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist3_Action_4")) != 0) ))
                    {
                        view.SelectRow(i);
                        view.MakeRowVisible(i, false);
                        intCount++;
                    }
                }
                else if (checkEditSelectAct5Ref.Checked)
                {
                    if (string.IsNullOrEmpty(view.GetRowCellValue(i, "strJobNumber_5").ToString().Trim())
                            && ((view.GetRowCellValue(i, "dtDueDate_5") != DBNull.Value && view.GetRowCellValue(i, "dtDueDate_5").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "dtDoneDate_5") != DBNull.Value && view.GetRowCellValue(i, "dtDoneDate_5").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intAction_5") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intAction_5")) != 0)
                                || (view.GetRowCellValue(i, "intActionBy_5") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intActionBy_5")) != 0)
                                || (view.GetRowCellValue(i, "intSupervisor_5") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intSupervisor_5")) != 0)
                                || (view.GetRowCellValue(i, "strUser1_Action_5") != DBNull.Value && view.GetRowCellValue(i, "strUser1_Action_5").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strUser2_Action_5") != DBNull.Value && view.GetRowCellValue(i, "strUser2_Action_5").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strUser3_Action_5") != DBNull.Value && view.GetRowCellValue(i, "strUser3_Action_5").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "strRemarks_Action_5") != null && view.GetRowCellValue(i, "strRemarks_Action_5").ToString().Trim() != String.Empty)
                                || (view.GetRowCellValue(i, "intWorkOrderID_5") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intWorkOrderID_5")) != 0)
                                || (view.GetRowCellValue(i, "intPriorityID_5") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intPriorityID_5")) != 0)
                                || (view.GetRowCellValue(i, "intScheduleOfRates_5") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intScheduleOfRates_5")) != 0)
                                || (view.GetRowCellValue(i, "intOwnership_5") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "intOwnership_5")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist1_Action_5") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist1_Action_5")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist2_Action_5") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist2_Action_5")) != 0)
                                || (view.GetRowCellValue(i, "UserPicklist3_Action_5") != DBNull.Value && Convert.ToInt32(view.GetRowCellValue(i, "UserPicklist3_Action_5")) != 0) ))
                    {
                        view.SelectRow(i);
                        view.MakeRowVisible(i, false);
                        intCount++;
                    }
                }
                intUpdateProgressTempCount++;
                if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                {
                    intUpdateProgressTempCount = 0;
                    fProgress.UpdateProgress(10);
                }
            }
            view.EndSelection();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress.Dispose();
            }
            string strMessage;
            switch (intCount)
            {
                case 0:
                    strMessage = "No Records Selected.";
                    break;
                case 1:
                    strMessage = "1 Record Selected.";
                    break;
                default:
                    strMessage = intCount.ToString() + " Records Selected.";
                    break;
            }
            DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Select Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnBlockEdit_Click(object sender, EventArgs e)
        {
            int intFailedRecordCount = 0;
            GridView view = (GridView)gridControl7.MainView;
            view.CloseEditor();
            int[] intSelectedRows = view.GetSelectedRows();
            if (intSelectedRows.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records before attempting to block Edit.", "Block Edit", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check grid is sorted first by RecordOrder - if not, warn user then allow proceed //
            if (view.Columns["RecordOrder"].SortIndex != 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Warning: The grid is not ordered primarily by the Record Order column.\n\nIf you proceed the sequence numbers generated will not increment in the same order as the records were added.\n\nProceed Anyway?", "Block Edit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) == DialogResult.No)
                {
                    return;
                }
            }
            else if (view.Columns["RecordOrder"].SortOrder != ColumnSortOrder.Ascending)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Warning: The grid ordering for the Record Order column is set to Descending.\n\nIf you proceed the sequence numbers generated will not increment in the same order as the records were added [it will be reversed].\n\nProceed Anyway?", "Block Edit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) == DialogResult.No)
                {
                    return;
                }
            }

            string strFriendlyName = "";
            string strTableName = "";
            string strFieldName = "";
            string strActualGridColumnName = "";

            if (checkEditBlockEditTreeRef.Checked)
            {
                strTableName = "tblTree";
                strFieldName = "strTreeRef";
                strFriendlyName = view.Columns["strTreeRef"].Caption;
                strActualGridColumnName = "strTreeRef";
            }
            else if (checkEditBlockEditInspRef.Checked)
            {
                strTableName = "tblInspection";
                strFieldName = "strInspectRef";
                strFriendlyName = view.Columns["strInspectRef"].Caption;
                strActualGridColumnName = "strInspectRef";
            }
            else if (checkEditBlockEditAct1Ref.Checked)
            {
                strTableName = "tblAction";
                strFieldName = "strJobNumber";
                strFriendlyName = view.Columns["strJobNumber_1"].Caption;
                strActualGridColumnName = "strJobNumber_1";
            }
            else if (checkEditBlockEditAct2Ref.Checked)
            {
                strTableName = "tblAction";
                strFieldName = "strJobNumber";
                strFriendlyName = view.Columns["strJobNumber_2"].Caption;
                strActualGridColumnName = "strJobNumber_2";
            }
            else if (checkEditBlockEditAct3Ref.Checked)
            {
                strTableName = "tblAction";
                strFieldName = "strJobNumber";
                strFriendlyName = view.Columns["strJobNumber_3"].Caption;
                strActualGridColumnName = "strJobNumber_3";
            }
            else if (checkEditBlockEditAct4Ref.Checked)
            {
                strTableName = "tblAction";
                strFieldName = "strJobNumber";
                strFriendlyName = view.Columns["strJobNumber_4"].Caption;
                strActualGridColumnName = "strJobNumber_4";
            }
            else if (checkEditBlockEditAct5Ref.Checked)
            {
                strTableName = "tblAction";
                strFieldName = "strJobNumber";
                strFriendlyName = view.Columns["strJobNumber_5"].Caption;
                strActualGridColumnName = "strJobNumber_5";
            }
            frm_AT_Data_Transfer_GBM_Mobile_Block_Edit_Sequence_Data fChildForm = new frm_AT_Data_Transfer_GBM_Mobile_Block_Edit_Sequence_Data();
            fChildForm.intRecordCount = intSelectedRows.Length;
            this.ParentForm.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.PassedInTableName = strTableName;
            fChildForm.PassedInFieldName = strFieldName;
            fChildForm.UserFriendlyFieldName = strFriendlyName;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            string strSequence = fChildForm.SelectedSequence ?? "";

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Processing...");
            fProgress.Show();
            Application.DoEvents();
            int intUpdateProgressThreshhold = intSelectedRows.Length / 10;
            int intUpdateProgressTempCount = 0;

            view.BeginUpdate();
            view.BeginDataUpdate();
            view.BeginSort();

            if (fChildForm.SelectedSequenceID == -1)  // Block Edit using Locality Code //
            {
                int? intNumericLength = 7;
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    intNumericLength = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_Sequence_From_Locality_Length").ToString());
                    if (string.IsNullOrEmpty(intNumericLength.ToString())) intNumericLength = 7;
                }
                catch (Exception Ex)
                {
                    view.EndUpdate();
                    view.EndDataUpdate();
                    view.EndSort();
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress.Dispose();
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while retrieving the length of sequence numbers generated from Locality Codes.\nCheck Your System Settings table to ensure a value has been set.", "Get Sequence", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                string strSeperator = "";
                try
                {
                    strSeperator = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_Sequence_From_Locality_Seperator").ToString();
                    if (string.IsNullOrEmpty(strSeperator)) strSeperator = "";
                }
                catch (Exception Ex)
                {
                    view.EndUpdate();
                    view.EndDataUpdate();
                    view.EndSort();
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress.Dispose();
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while retrieving the sequence seperator.\nCheck Your System Settings table to ensure a value has been set.", "Get Sequence", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                int intAmountToTrim = 0;
                foreach (int i in intSelectedRows)
                {
                    if (strFieldName == "strTreeRef" && ! string.IsNullOrEmpty(view.GetRowCellValue(i, "strTreeRef").ToString())) continue;  // Ignore as a Tree Reference already exists //
                    object obj = view.GetRowCellValue(i, view.Columns["intLocalityID"]);
                    object obj2 = gridView11.GetRow(repositoryItemGridLookUpEditLocality.GetIndexByKeyValue(obj));
                    string strLocalityCode = (obj2 as DataRowView)["LocalityCode"].ToString();
                    if (String.IsNullOrEmpty(strLocalityCode) || strLocalityCode == "N\\A")
                    {
                        view.SetRowCellValue(i, strActualGridColumnName, String.Empty);  // Reset to blank as can't calculate a sequence //
                        intFailedRecordCount++;
                    }
                    else
                    {
                        intAmountToTrim = (strLocalityCode.Length + (int)intNumericLength) + strSeperator.Length - 20;
                        if (intAmountToTrim > 0) strLocalityCode = strLocalityCode.Substring(0, strLocalityCode.Length - intAmountToTrim);
                        strLocalityCode += strSeperator;
                        view.SetRowCellValue(i, strFieldName, "?" + intNumericLength.ToString() + strLocalityCode);  // Just First x Digits preceeding by a question mark and x numeric length so Get Sequence SP can treat it differently //
                    }
                }

                string strNextNumber = "";
                bool boolUniqueValueFound = false;
                foreach (int i in intSelectedRows)
                {
                    if (strFieldName == "strTreeRef" && !string.IsNullOrEmpty(view.GetRowCellValue(i, "strTreeRef").ToString()) && !view.GetRowCellValue(i, "strTreeRef").ToString().StartsWith("?")) continue;  // Ignore as a Tree Reference already exists //
                    strSequence = view.GetRowCellValue(i, strActualGridColumnName).ToString() ?? "";  // Get next value in sequence //
                    if (strSequence != "")
                    {
                        strNextNumber = GetNextNumberInSequence(strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //        
                        if (strNextNumber == "")
                        {
                            intFailedRecordCount++;
                            continue;
                        }
                        if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

                        int intRequiredLength = strNextNumber.Length;
                        int intNextNumber = Convert.ToInt32(strNextNumber);
                        int intIncrement = 0;
                        string strTempNumber = "";

                        // Check if value is already present within grid //
                        boolUniqueValueFound = false;
                        do
                        {
                            strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                            if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                            {
                                view.SetRowCellValue(i, strActualGridColumnName, String.Empty);  // Reset to blank as can't calculate a sequence [Number generated is too long for field]  //
                            }
                            else
                            {
                                strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                            }
                            switch (strFieldName)
                            {
                                case "strTreeRef":
                                case "strInspectRef":
                                    if (view.LocateByValue(0, view.Columns[strFieldName], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                    {
                                        intIncrement++;
                                    }
                                    else
                                    {
                                        boolUniqueValueFound = true;  // Exit Loop //
                                    }
                                    break;
                                case "strJobNumber":
                                    if (view.LocateByValue(0, view.Columns["strJobNumber_1"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                    {
                                        intIncrement++;
                                    }
                                    else if (view.LocateByValue(0, view.Columns["strJobNumber_2"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                    {
                                        intIncrement++;
                                    }
                                    else if (view.LocateByValue(0, view.Columns["strJobNumber_3"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                    {
                                        intIncrement++;
                                    }
                                    else if (view.LocateByValue(0, view.Columns["strJobNumber_4"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                    {
                                        intIncrement++;
                                    }
                                    else if (view.LocateByValue(0, view.Columns["strJobNumber_5"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                    {
                                        intIncrement++;
                                    }
                                    else
                                    {
                                        boolUniqueValueFound = true;  // Exit Loop //
                                    }
                                    break;
                            }
                        } while (!boolUniqueValueFound);
                        view.SetRowCellValue(i, strActualGridColumnName, strSequence + strTempNumber);
                        
                        intUpdateProgressTempCount++;  // Update Progress //
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                    }
                }
            }
            else  // Block Edit using passed sequence //
            {
                foreach (int i in intSelectedRows)
                {
                    if (strFieldName == "strTreeRef" && ! string.IsNullOrEmpty(view.GetRowCellValue(i, "strTreeRef").ToString())) continue;  // Ignore as a Tree Reference already exists //
                    view.SetRowCellValue(i, strActualGridColumnName, String.Empty);  // Reset all rows to be block-edited first //
                }

                string strNextNumber = GetNextNumberInSequence(strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //        
                if (strNextNumber == "")
                {
                    view.EndSort();
                    view.EndDataUpdate();
                    view.EndUpdate();
                    if (fProgress != null)
                    {
                        fProgress.Close();
                        fProgress.Dispose();
                    }
                    return;
                }

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";
                bool boolUniqueValueFound = false;
                foreach (int i in intSelectedRows)
                {
                    if (strFieldName == "strTreeRef" && ! string.IsNullOrEmpty(view.GetRowCellValue(i, "strTreeRef").ToString())) continue;  // Ignore as a Tree Reference already exists //
                    // Check if value is already present within grid //
                    boolUniqueValueFound = false;
                    do
                    {
                        strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                        if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                        {
                            view.EndSort();
                            view.EndDataUpdate();
                            view.EndUpdate();
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress.Dispose();
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        else
                        {
                            strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                        }
                        switch (strFieldName)
                        {
                            case "strTreeRef":
                            case "strInspectRef":
                                if (view.LocateByValue(0, view.Columns[strFieldName], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                {
                                    intIncrement++;
                                }
                                else
                                {
                                    boolUniqueValueFound = true;  // Exit Loop //
                                }
                                break;
                            case "strJobNumber":
                                if (view.LocateByValue(0, view.Columns["strJobNumber_1"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                {
                                    intIncrement++;
                                }
                                else if (view.LocateByValue(0, view.Columns["strJobNumber_2"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                {
                                    intIncrement++;
                                }
                                else if (view.LocateByValue(0, view.Columns["strJobNumber_3"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                {
                                    intIncrement++;
                                }
                                else if (view.LocateByValue(0, view.Columns["strJobNumber_4"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                {
                                    intIncrement++;
                                }
                                else if (view.LocateByValue(0, view.Columns["strJobNumber_5"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                                {
                                    intIncrement++;
                                }
                                else
                                {
                                    boolUniqueValueFound = true;  // Exit Loop //
                                }
                                break;
                        }
                    } while (!boolUniqueValueFound);
                    view.SetRowCellValue(i, strActualGridColumnName, strSequence + strTempNumber);

                    intUpdateProgressTempCount++;  // Update Progress //
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            view.EndSort();
            view.EndDataUpdate();
            view.EndUpdate();
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress.Dispose();
            }
            if (intFailedRecordCount > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Warning: Unable to calculate sequences for " + intFailedRecordCount.ToString() + (intFailedRecordCount == 1 ? " record" : " records") + "!", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void repositoryItemButtonEditSequenceField_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl7.MainView;
            string strTableName = "";
            string strFieldName = "";
            switch (view.FocusedColumn.Name)
            {
                case "colstrTreeRef":
                    strTableName = "tblTree";
                    strFieldName = "strTreeRef";
                    break;
                case "colstrInspectRef":
                    strTableName = "tblInspection";
                    strFieldName = "strInspectRef";
                    break;
                case "colstrJobNumber_1":
                case "colstrJobNumber_2":
                case "colstrJobNumber_3":
                case "colstrJobNumber_4":
                case "colstrJobNumber_5":
                    strTableName = "tblAction";
                    strFieldName = "strJobNumber";
                    break;
                default:
                    break;
            }
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {

                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                
                int? intNumericLength = 7;
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    intNumericLength = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_Sequence_From_Locality_Length").ToString());
                    if (string.IsNullOrEmpty(intNumericLength.ToString())) intNumericLength = 7;
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while retrieving the length of sequence numbers generated from Locality Codes.\nCheck Your System Settings table to ensure a value has been set.", "Get Sequence", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                string strSeperator = "";
                try
                {
                    strSeperator = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_Sequence_From_Locality_Seperator").ToString();
                    if (string.IsNullOrEmpty(strSeperator)) strSeperator = "";
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while retrieving the sequence seperator.\nCheck Your System Settings table to ensure a value has been set.", "Get Sequence", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                int intAmountToTrim = 0;
                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    if (fChildForm.SelectedSequenceID == -1) // Locality Code seed //
                    {
                       	object obj = view.GetRowCellValue(view.FocusedRowHandle,view.Columns["intLocalityID"]);
			            object obj2 = gridView11.GetRow(repositoryItemGridLookUpEditLocality.GetIndexByKeyValue(obj));
			            string strLocalityCode = (obj2 as DataRowView)["LocalityCode"].ToString();
                        if (String.IsNullOrEmpty(strLocalityCode) || strLocalityCode == "N\\A")
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to get a locality code for the current record\nTry selecting a Locality first.", "Get Sequence Seed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;  // Abort - No Locality Code found] //
                        }
                        intAmountToTrim = (strLocalityCode.Length + (int)intNumericLength) + strSeperator.Length - 20;
                        if (intAmountToTrim > 0) strLocalityCode = strLocalityCode.Substring(0, strLocalityCode.Length - intAmountToTrim);
                        strLocalityCode += strSeperator;
                        view.SetFocusedValue("?" + intNumericLength.ToString() + strLocalityCode);  // Just First x Digits preceeding by a question mark and x numeric length so Get Sequence SP can treat it differently //
                    }
                    else
                    {
                        view.SetFocusedValue(fChildForm.SelectedSequence);
                    }
                }

            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                view.CloseEditor();
                // Get next value in sequence //
                string strSequence = view.GetFocusedValue().ToString() ?? "";

                string strNextNumber = GetNextNumberInSequence(strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //        
                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";

                // Check if value is already present within grid //
                bool boolUniqueValueFound = false;
                do
                {
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    switch (strFieldName)
                    {
                        case "strTreeRef":
                        case "strInspectRef":
                            if (view.LocateByValue(0, view.Columns[strFieldName], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                            {
                                intIncrement++;
                            }
                            else
                            {
                                boolUniqueValueFound = true;  // Exit Loop //
                            }
                            break;
                        case "strJobNumber":
                            if (view.LocateByValue(0, view.Columns["strJobNumber_1"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                            {
                                intIncrement++;
                            }
                            else if (view.LocateByValue(0, view.Columns["strJobNumber_2"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                            {
                                intIncrement++;
                            }
                            else if (view.LocateByValue(0, view.Columns["strJobNumber_3"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                            {
                                intIncrement++;
                            }
                            else if (view.LocateByValue(0, view.Columns["strJobNumber_4"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                            {
                                intIncrement++;
                            }
                            else if (view.LocateByValue(0, view.Columns["strJobNumber_5"], strSequence + strTempNumber) != GridControl.InvalidRowHandle)
                            {
                                intIncrement++;
                            }
                            else
                            {
                                boolUniqueValueFound = true;  // Exit Loop //
                            }
                            break;
                    }
                } while (!boolUniqueValueFound);
                view.SetFocusedValue(strSequence + strTempNumber);
            }
        }

        private string GetNextNumberInSequence(string strFieldName, string strTableName, string strSequence)
        {
            DataSet_ATTableAdapters.QueriesTableAdapter GetValue = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetValue.ChangeConnectionString(strConnectionString);
            string strNextNumber = "";
            int intAdditionalIncrement = 0;
            try
            {
                strNextNumber = GetValue.sp01248_AT_Sequence_Get_Next_Sequence(strTableName, strFieldName, strSequence, intAdditionalIncrement).ToString();
                if (String.IsNullOrEmpty(strNextNumber))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number - the input sequence prefix is unrecognised!\n\nTry selecting a different sequence prefix first or clear the sequence prefix to calculate a sequence without a prefix.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return "";  // Abort - System was unable to return a sequence [may have had an invalid first part of the sequence passed through] //
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //
                if (strSequence.Length + strNextNumber.Length > 20)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return "";
                }
            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number - an error ocurred [" + Ex.Message + "]!\n\nYou may try again - if the error persists, contact Technical Support.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }
            return strNextNumber;
        }

        private void btnShowRecordsErrorsOnly_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl7.MainView;
            view.BeginUpdate();
            view.BeginSort();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no filters switched on //

            string strFilterString = "";
            sp00134exporttoGBMexportdataBindingSource.Filter = "";  // Clear any Prior Filter //
            foreach (DataRow dr in this.dataSet_AT_DataTransfer.sp00134_export_to_GBM_export_data.Rows)
            {
                if (dr.HasErrors) strFilterString += " or ID = '" + dr["ID"].ToString() + "'" ?? String.Empty;
            }
            if (strFilterString.Length > 4) strFilterString = strFilterString.Remove(0, 4);
            if (strFilterString == String.Empty)
            {
                view.EndSort();
                view.EndUpdate();
                DevExpress.XtraEditors.XtraMessageBox.Show("No Errors Present.", "Error Row Filter", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            try
            {
                sp00134exporttoGBMexportdataBindingSource.Filter = strFilterString;
            }
            catch (Exception Ex)
            {
                view.EndSort();
                view.EndUpdate();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to filter errors rows - an error ocurred [" + Ex.Message + "]!\n\nYou may try again - if the error persists, contact Technical Support.", "Error Row Filter", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            view.EndSort();
            view.EndUpdate();
        }

        private void btnShowRecordsAll_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl7.MainView;
            view.BeginUpdate();
            view.BeginSort();          
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no filters switched on //
            
            sp00134exporttoGBMexportdataBindingSource.Filter = "";  // Clear any Prior Filter //

            view.EndSort();
            view.EndUpdate();
        }

        private void btnRollBackImport_Click(object sender, EventArgs e)
        {
            frm_AT_Data_Transfer_GBM_Mobile_Rollback_Import fChildForm = new frm_AT_Data_Transfer_GBM_Mobile_Rollback_Import();
            this.ParentForm.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.ShowDialog();
        }

        private void repositoryItemGridLookUpEditSpecies_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            RepositoryItemLookUpEdit props;
            if (sender is LookUpEdit)
            {
                props = (sender as LookUpEdit).Properties;
            }
            else
            {
                props = sender as RepositoryItemLookUpEdit;
            }
            if (props != null && (e.Value is int))
            {
                object row = props.GetDataSourceRowByKeyValue(e.Value);
                if (row == null)
                {
                    e.DisplayText = NotFoundText;
                }
            }
        }

        private void repositoryItemGridLookUpEditStatus_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            RepositoryItemLookUpEdit props;
            if (sender is LookUpEdit)
            {
                props = (sender as LookUpEdit).Properties;
            }
            else
            {
                props = sender as RepositoryItemLookUpEdit;
            }
            if (props != null && (e.Value is int))
            {
                object row = props.GetDataSourceRowByKeyValue(e.Value);
                if (row == null)
                {
                    e.DisplayText = NotFoundText;
                }
            }
        }

        private void buttonEditImportWorkOrderChecking_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            #region GetFileToImport
            string strDefaultPath = "";
            string strImportPath = "";
            string strFileName = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesImportFromGBM_Default_Tab_Folder").ToString().ToLower();
            }
            catch (Exception)
            {
            }
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.DefaultExt = "tab";
                dlg.Filter = "TAB Files (*.tab)|*.tab"; ;
                if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                dlg.Multiselect = false;
                if (dlg.ShowDialog() != DialogResult.OK) return;

                FileInfo file = new FileInfo(dlg.FileName);
                strImportPath = file.DirectoryName;
                string strPathAndFileName = file.FullName;
                strFileName = file.Name.Substring(0, file.Name.Length - 4);

                if (strFileName.ToLower() != "workorder")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("The file selected for a Work Order Checking Import must be named 'WorkOrder.Tab'.\n\nTry selecting the correct file before proceeding.", "Import Work Order Checking Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                buttonEditImportWorkOrderChecking.EditValue = strPathAndFileName;

                // Check if an import has occurred from the selected directory //
                try
                {
                    DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter CheckImportFolder = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                    CheckImportFolder.ChangeConnectionString(strConnectionString);
                    int intImportCount = Convert.ToInt32(CheckImportFolder.sp00143_import_from_GBM_check_if_already_imported(strImportPath, 1));  // 0 = Survey, 1 = WorkOrders //
                    if (intImportCount > 0)
                    {
                        if (DevExpress.XtraEditors.XtraMessageBox.Show("WARNING...  WARNING...  WARNING...  WARNING...  WARNING...\n=========================================\n\nYou have already imported from the selected folder [" + strImportPath + "]!\n\nIf you proceed and import this data you may overwrite action completion dates!\n\nProceed with the Import?", "Import Data   *** IMPORTANT INFORMATION ***", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No) return;
                    }
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while checking if an import has already been made from the selected folder!\n\nYou can still import from the folder however you should manually check you have not already imported the file.", "Import Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            #endregion

            #region ConvertTabToMidMif

            string strAppToRun = "\"" + Application.StartupPath + "\\Plugin_Apps\\tab2tab.exe" + "\"";
            string strArguments = "\"" + buttonEditImportWorkOrderChecking.EditValue + "\" \"" + strImportPath + "\\" + strFileName + ".mif" + "\"";
            string error = "";
            try
            {
                System.Diagnostics.Process Proc = new System.Diagnostics.Process();
                Proc.StartInfo.CreateNoWindow = true;  // Hide DOS Window [This line and next]//
                Proc.StartInfo.UseShellExecute = false;
                Proc.StartInfo.RedirectStandardError = true;
                Proc.StartInfo.FileName = strAppToRun;
                Proc.StartInfo.Arguments = strArguments;

                Proc.Start();
                error = Proc.StandardError.ReadToEnd();
                Proc.WaitForExit(300000);
                Proc.Close();
                Proc = null;
                GC.GetTotalMemory(true);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to import data - an error occurred with Tab2Tab.EXE [" + ex.Message + "] - contact Technical Support.", "Import From GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (! string.IsNullOrEmpty(error))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to import data - an error occurred with Tab2Tab.EXE [" + error + "] - contact Technical Support.", "Import From GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            #endregion

            #region ImportDataInToDatabase
            this.dataSet_AT_DataTransfer.sp01323_AT_import_from_GBM_workorder_checking.Rows.Clear();
            Application.DoEvents();  // Allow Form time to repaint itself //

            string[] linesMIF = File.ReadAllLines(strImportPath + "\\" + strFileName + ".mif");

            Boolean boolColumns = false;
            char[] delimiters = new char[] { ' ', '(', ')', ',' };
            ArrayList arrayImportStructure = new ArrayList();

            doubleImportXMin = (double)0.00;
            doubleImportYMin = (double)0.00;
            doubleImportXMax = (double)0.00;
            doubleImportYMax = (double)0.00;

            int intCurrentMidFileLineNumber = 0;
            try
            {
                foreach (string line in linesMIF)
                {
                    intCurrentMidFileLineNumber++; // Store current position in the file for later //
                    if (line.StartsWith("CoordSys"))
                    {
                        string strBounds = line.Substring(line.IndexOf("Bounds"));
                        doubleImportXMin = Convert.ToDouble(strBounds.Substring(strBounds.IndexOf('(') + 1, strBounds.IndexOf(',') - (strBounds.IndexOf('(') + 1)));
                        doubleImportYMin = Convert.ToDouble(strBounds.Substring(strBounds.IndexOf(',') + 2, strBounds.IndexOf(')') - (strBounds.IndexOf(',') + 2)));
                        strBounds = strBounds.Substring(strBounds.IndexOf(')') + 2);
                        doubleImportXMax = Convert.ToDouble(strBounds.Substring(1, strBounds.IndexOf(',') - 1));
                        doubleImportYMax = Convert.ToDouble(strBounds.Substring(strBounds.IndexOf(',') + 2, strBounds.IndexOf(')') - (strBounds.IndexOf(',') + 2)));
                        continue;  // Go to next iteration of loop //
                    }
                    if (line.StartsWith("Columns"))
                    {
                        boolColumns = true;
                        continue;  // Go to next iteration of loop //
                    }
                    if (line.StartsWith("Data")) break; // All columns processed so Exit Loop //
                    if (!boolColumns) continue;  // Go to next iteration of loop //

                    // If we are here, we are processing a field definition line //
                    string[] items = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);  // StringSplitOptions.RemoveEmptyEntries: Stops double space from start of line from being picked up and inserted as elements of the array. //
                    arrayImportStructure.Add(items[0]);
                }
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to import data - an error occurred while parsing the " + strFileName + ".mif file  - contact Technical Support.", "Import From GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intColumnPosition = 0;
            int intLinePosition = 0;

            string[] linesMID = File.ReadAllLines(strImportPath + "\\" + strFileName + ".mid");
            System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
            DataRow drNewRow;

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Processing...");
            fProgress.Show();
            Application.DoEvents();
            int intUpdateProgressThreshhold = linesMID.Length / 10;
            int intUpdateProgressTempCount = 0;

            // Import Data... //
            DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter ImportData = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            ImportData.ChangeConnectionString(strConnectionString);
            DateTime dtImportStartTime = DateTime.Now;

            int intRecordOrder = 1;
            int intActionID;
            DateTime dtDoneDate;
            string strUser1;
            string strUser2;
            string strUser3;

            try
            {
                foreach (string line in linesMID)
                {
                    drNewRow = this.dataSet_AT_DataTransfer.sp01323_AT_import_from_GBM_workorder_checking.NewRow();
                    intColumnPosition = 0;
                    string[] items = r.Split(line);
                    foreach (object item in items)
                    {
                            if ((arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate" ||
                                 arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate")
                                   && (item.ToString() == "" || item.ToString().Length < 8))
                            {
                                drNewRow[arrayImportStructure[intColumnPosition].ToString()] = DBNull.Value;
                            }
                            else if (arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtduedate" ||
                                     arrayImportStructure[intColumnPosition].ToString().ToLower() == "dtdonedate")
                            {
                                if (item.ToString().StartsWith("\""))  // Remove quotes while converting to datetime //
                                {
                                    drNewRow[arrayImportStructure[intColumnPosition].ToString()] = DateTime.ParseExact(Convert.ToString(item).Substring(1, item.ToString().Length - 2), "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo);  // Non null date so convert string into a proper date //
                                }
                                else  // No quotes to remove while converting to datetime //
                                {
                                    drNewRow[arrayImportStructure[intColumnPosition].ToString()] = DateTime.ParseExact(Convert.ToString(item), "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo);  // Non null date so convert string into a proper date //
                                }
                            }
                            else
                            {
                                if (item.ToString().StartsWith("\""))
                                {
                                    drNewRow[arrayImportStructure[intColumnPosition].ToString()] = item.ToString().Substring(1, item.ToString().Length - 2);  // Remove surrounding quotes //
                                }
                                else
                                {
                                    drNewRow[arrayImportStructure[intColumnPosition].ToString()] = item;
                                }
                            }
                        intColumnPosition++;
                    }
                    intLinePosition++;
                    if (!string.IsNullOrEmpty(drNewRow["intActionID"].ToString()))
                    {
                        //this.dataSet_AT_DataTransfer.sp01323_AT_import_from_GBM_workorder_checking.Rows.Add(drNewRow);
                        intRecordOrder++;
                        
                        intActionID = (drNewRow["intActionID"] == DBNull.Value ? 0 : Convert.ToInt32(drNewRow["intActionID"]));
                        dtDoneDate = (drNewRow["dtDoneDate"] == DBNull.Value ? Convert.ToDateTime("01/01/1900 00:00:00.000") : Convert.ToDateTime(drNewRow["dtDoneDate"]));
                        strUser1 = Convert.ToString(drNewRow["strUser1"] ?? "");
                        strUser2 = Convert.ToString(drNewRow["strUser2"] ?? "");
                        strUser3 = Convert.ToString(drNewRow["strUser3"] ?? "");

                        ImportData.sp01324_AT_import_from_GBM_workorder_checking_import(intActionID, dtDoneDate, strUser1, strUser2, strUser3);
                    }

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                //this.dataSet_AT_DataTransfer.AcceptChanges();  // Commit changes to underlying dataset so Cell Colouring for change tracking can occur //
            }
            catch (Exception ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to import data - an error occurred during the import process [" + ex.Message + "] - contact Technical Support.", "Import From GBM Mobile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Write to Import Log //
            try
            {
                DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter WriteToLog = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                WriteToLog.ChangeConnectionString(strConnectionString);
                FileInfo file = new FileInfo(buttonEditImportWorkOrderChecking.EditValue.ToString());
                strImportPath = file.DirectoryName;
                WriteToLog.sp00142_import_from_GBM_log_import(strImportPath, GlobalSettings.Username, dtImportStartTime, 1);  // 0 = Survey, 1 = WorkOrders //
            }
            catch (Exception Ex)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while writing to the Import Log [" + Ex.Message + "]!\n\nYou will not be able to roll back this import!", "Import Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress.Dispose();
            }
            DevExpress.XtraEditors.XtraMessageBox.Show("Work Order Checking Imported Successfully.", "Import Data", MessageBoxButtons.OK, MessageBoxIcon.Information);

            #endregion
        }


        #region Import - Editors

        private void checkEditSelectTreeRef_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) checkEditBlockEditTreeRef.Checked = true;
        }

        private void checkEditSelectInspRef_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) checkEditBlockEditInspRef.Checked = true;
        }

        private void checkEditSelectAct1Ref_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) checkEditBlockEditAct1Ref.Checked = true;
        }

        private void checkEditSelectAct2Ref_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) checkEditBlockEditAct2Ref.Checked = true;
        }

        private void checkEditSelectAct3Ref_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) checkEditBlockEditAct3Ref.Checked = true;
        }

        private void checkEditSelectAct4Ref_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) checkEditBlockEditAct4Ref.Checked = true;
        }

        private void checkEditSelectAct5Ref_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) checkEditBlockEditAct5Ref.Checked = true;
        }

        #endregion



    }    
}

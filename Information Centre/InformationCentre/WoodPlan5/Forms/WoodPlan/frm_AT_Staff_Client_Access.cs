using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraGrid.Columns;
using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_AT_Staff_Client_Access : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;

        WaitDialogForm loadingForm;
        private int i_int_FocusedGrid = 1;
        /// <summary>
        /// 
        /// </summary>
        public bool UpdateRefreshStatus = false; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        /// <summary>
        /// 
        /// </summary>
        public RefreshGridState RefreshGridViewState;  // Used by Grid View State Facilities //

        //BaseObjects.GridCheckMarksSelection selection1;
        #endregion

        public frm_AT_Staff_Client_Access()
        {
            InitializeComponent();
        }

        private void frm_AT_Staff_Client_Access_Load(object sender, EventArgs e)
        {
            this.FormID = 2017;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow();  // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 1 = Staff //

            sp02059_AT_Staff_Client_Access_Staff_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            
            sp02061_AT_Staff_Client_Access_Selected_ClientsTableAdapter.Connection.ConnectionString = strConnectionString;
                      
            sp02060_AT_Staff_Client_Access_Available_ClientsTableAdapter.Connection.ConnectionString = strConnectionString;

            ProcessPermissionsForForm();

            RefreshGridViewState = new RefreshGridState(gridView1, "StaffID");

            gridControl1.BeginUpdate();
            LoadData();
            gridControl1.EndUpdate();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }


        public void UpdateFormRefreshStatus(bool status)
        {
            UpdateRefreshStatus = status;
        }

        private void LoadData()
        {
            if (UpdateRefreshStatus) UpdateRefreshStatus = false;
            try
            {
                sp02059_AT_Staff_Client_Access_Staff_ListTableAdapter.Fill(dataSet_AT.sp02059_AT_Staff_Client_Access_Staff_List);
            }
            catch (Exception) { }
            RefreshGridViewState.LoadViewInfo();  // Reload any expanded groups and selected rows //
        }

        private void frm_AT_Staff_Client_Access_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus)
            {
                gridControl1.BeginUpdate();
                LoadData();
                gridControl1.EndUpdate();
            }
            SetMenuStatus();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;


            alItems.AddRange(new string[] { "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
        }
        
        public override void OnEditEvent(object sender, EventArgs e)
        {
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Staff - Try clearing any applied filters";
                    break;
                case "gridView2":
                    message = "No Clients - Select one or more members of staff";
                    break;
                case "gridView3":
                    message = "No Clients - Select one or more members of staff";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadGroupMembers();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region Staff Grid

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }
       
        private void LoadGroupMembers()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedGroups = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedGroups += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["StaffID"])) + ',';
            }
            gridControl2.MainView.BeginUpdate();
            gridControl3.MainView.BeginUpdate();
            if (string.IsNullOrEmpty(strSelectedGroups))
            {
                dataSet_AT.sp02060_AT_Staff_Client_Access_Available_Clients.Rows.Clear();
                dataSet_AT.sp02061_AT_Staff_Client_Access_Selected_Clients.Rows.Clear();
            }
            else
            {
                try
                {
                    sp02060_AT_Staff_Client_Access_Available_ClientsTableAdapter.Fill(dataSet_AT.sp02060_AT_Staff_Client_Access_Available_Clients, strSelectedGroups);
                    sp02061_AT_Staff_Client_Access_Selected_ClientsTableAdapter.Fill(dataSet_AT.sp02061_AT_Staff_Client_Access_Selected_Clients, strSelectedGroups);
                }
                catch (Exception) { }
            }
            gridControl2.MainView.EndUpdate();
            gridControl3.MainView.EndUpdate();
        }

        #endregion


        #region Available Clients Grid

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            AddOne();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region Selected Clients Grid

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            RemoveOne();
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void btnAddOne1_Click(object sender, EventArgs e)
        {
            AddOne();
        }
 
        private void btnAddAll1_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int intCount = view.RowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("There are no clients to add to the selected staff.", "Add All Clients to Selected Staff" , MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedClient = "";
            for (int i = 0; i < intCount; i++)
            {
                strSelectedClient += Convert.ToString(view.GetRowCellDisplayText(i, view.Columns["ClientID"])) + ',';

            }
            view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more staff to add to before proceeding.", "Add All Clients to Selected Staff", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedStaff = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedStaff += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["StaffID"])) + ',';
            }
            AddGroupMembers(strSelectedClient, strSelectedStaff);
        }

        private void AddOne()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more clients to add to the selected staff before proceeding.", "Add Selected Clients to Selected Staff", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedClient = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedClient += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ClientID"])) + ',';
            }
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more staff to add to before proceeding.", "Add Selected Clients to Selected Staff", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedStaff = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedStaff += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["StaffID"])) + ',';
            }
            AddGroupMembers(strSelectedClient, strSelectedStaff);
        }
        
        private void AddGroupMembers(string strSelectedClient, string strSelectedStaff)
        {
            using (DataSet_ATTableAdapters.QueriesTableAdapter AddGroupMembers = new DataSet_ATTableAdapters.QueriesTableAdapter())
            {
                // Add the selected staff to the selected groups //
                AddGroupMembers.ChangeConnectionString(strConnectionString);
                try
                {
                    AddGroupMembers.sp02062_AT_Staff_Client_Access_Link_Clients(strSelectedStaff, strSelectedClient);
                }
                catch (Exception) { }
            }
            RefreshGridViewState.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            LoadData();
            gridControl1.EndUpdate();
        }

        private void btnRemoveOne1_Click(object sender, EventArgs e)
        {
            RemoveOne();
        }

        private void btnRemoveAll1_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            int intCount = view.RowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("There are no clients to remove from the selected staff.", "Remove All Clients from Selected Staff", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedIDs = "";
            for (int i = 0; i < intCount; i++)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(i, view.Columns["StaffClientAccessID"])) + ',';

            }
            RemoveGroupMembers(strSelectedIDs);
        }

        private void RemoveOne()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more clients to remove from the selected staff before proceeding.", "Remove Selected Clients from Selected Staff", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedRecordIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["StaffClientAccessID"])) + ',';
            }
            RemoveGroupMembers(strSelectedRecordIDs);
        }

        private void RemoveGroupMembers(string strSelectedGroupMembers)
        {
            using (DataSet_ATTableAdapters.QueriesTableAdapter RemoveGroupMembers = new DataSet_ATTableAdapters.QueriesTableAdapter())
            {
                // Add the selected staff to the selected groups //
                RemoveGroupMembers.ChangeConnectionString(strConnectionString);
                try
                {
                    RemoveGroupMembers.sp02063_AT_Staff_Client_Access_Link_Delete(strSelectedGroupMembers);
                }
                catch (Exception) { }
            }
            RefreshGridViewState.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            LoadData();
            gridControl1.EndUpdate();
        }






    }
}


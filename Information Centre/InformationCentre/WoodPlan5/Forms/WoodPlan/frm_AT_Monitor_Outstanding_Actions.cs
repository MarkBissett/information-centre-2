using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Wizard;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_AT_Monitor_Outstanding_Actions : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        //private DataSet_Selection DS_Selection1;
        //private DataSet_Selection DS_Selection2;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
  
        #endregion

        public frm_AT_Monitor_Outstanding_Actions()
        {
            InitializeComponent();
        }

        private void frm_AT_Monitor_Outstanding_Actions_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 2010;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();
            
            this.sp01457_AT_Site_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01457_AT_Site_Filter_ListTableAdapter.Fill(this.dataSet_AT.sp01457_AT_Site_Filter_List);
            gridControl1.ForceInitialize();
            // Add record selection checkboxes to grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            this.sp01335_AT_Job_Master_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01335_AT_Job_Master_ListTableAdapter.Fill(this.dataSet_AT.sp01335_AT_Job_Master_List);
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            this.sp01336_AT_Contractor_Master_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01336_AT_Contractor_Master_ListTableAdapter.Fill(this.dataSet_AT.sp01336_AT_Contractor_Master_List);
            gridControl3.ForceInitialize();
            // Add record selection checkboxes to grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            this.sp01337_Inspections_DueTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01338_AT_Actions_DueTableAdapter.Connection.ConnectionString = strConnectionString;

            dateEditFromDate.DateTime = this.GlobalSettings.ViewedStartDate;
            //dateEditToDate.DateTime = this.GlobalSettings.ViewedEndDate;
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

            LoadLastSavedUserScreenSettings();
        }

        public override void PostLoadView(object objParameter)
        {
            chartControl1.DataSource = sp01338ATActionsDueBindingSource;
            chartControl1.DataAdapter = sp01338_AT_Actions_DueTableAdapter;

            chartControl2.DataSource = sp01337InspectionsDueBindingSource;
            chartControl2.DataAdapter = sp01337_Inspections_DueTableAdapter;
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // From Date //
                string strFromDate = default_screen_settings.RetrieveSetting("FromDate");
                if (!string.IsNullOrEmpty(strFromDate)) dateEditFromDate.DateTime = Convert.ToDateTime(strFromDate);
                
                // Site Filter //
                int intFoundRow = 0;
                string strSiteFilter = default_screen_settings.RetrieveSetting("SiteFilter");
                if (!string.IsNullOrEmpty(strSiteFilter))
                {
                    Array arraySites = strSiteFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewSites = (GridView)gridControl1.MainView;
                    viewSites.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arraySites)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewSites.LocateByValue(0, viewSites.Columns["SiteID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewSites.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewSites.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewSites.EndUpdate();
                }
                
                // Job Type Filter //
                string strJobTypeFilter = default_screen_settings.RetrieveSetting("JobTypeFilter");
                if (!string.IsNullOrEmpty(strJobTypeFilter))
                {
                    Array arrayJobTypes = strJobTypeFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewJobTypes = (GridView)gridControl2.MainView;
                    viewJobTypes.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayJobTypes)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewJobTypes.LocateByValue(0, viewJobTypes.Columns["JobID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewJobTypes.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewJobTypes.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewJobTypes.EndUpdate();
                }
                
                // Contractor Filter //
                string strContractorFilter = default_screen_settings.RetrieveSetting("ContractorFilter");
                if (!string.IsNullOrEmpty(strContractorFilter))
                {
                    Array arrayContractors = strContractorFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewContractors = (GridView)gridControl3.MainView;
                    viewContractors.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayContractors)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewContractors.LocateByValue(0, viewContractors.Columns["ContractorID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewContractors.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewContractors.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewContractors.EndUpdate();
                }

                btnLoadData.PerformClick();
            }
        }

        private void frm_AT_Monitor_Outstanding_Actions_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_AT_Monitor_Outstanding_Actions_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SiteFilter", Get_Selected_Sites());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "JobTypeFilter", Get_Selected_JobTypes());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ContractorFilter", Get_Selected_Contractors());
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDate", dateEditFromDate.DateTime.ToString());
                default_screen_settings.SaveDefaultScreenSettings();
            }
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }
        
        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = true;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            switch (i_int_FocusedGrid)
            {
                case 4:  // Inspections Grid //
                    view = (GridView)gridControl4.MainView;
                    intRowHandles = view.GetSelectedRows();
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        view = (GridView)gridControl1.MainView;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                    }
                    bbiShowMap.Enabled = intRowHandles.Length > 0;
                    break;
                case 5:  // Actions Grid //
                    view = (GridView)gridControl5.MainView;
                    intRowHandles = view.GetSelectedRows();
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        view = (GridView)gridControl1.MainView;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                    }
                    bbiShowMap.Enabled = intRowHandles.Length > 0;
                    break;
                default:  // Client Grid //
                    break;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients \\ Sites Available";
                    break;
                case "gridView2":
                    message = "No Action Types Available";
                    break;
                case "gridView3":
                    message = "No Contractors Available";
                    break;
                case "gridView4":
                    message = "No Outstanding Inspections Available - Click Load Data button or Adjust Filters";
                    break;
                case "gridView5":
                    message = "No Outstanding Actions Available - Click Load Data button or Adjust Filters";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridControl 1 - Clients \ Sites Filter

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        #endregion


        #region GridControl 2 - Action Type Filter

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        #endregion


        #region GridControl 3 - Contractor Filter

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        #endregion


        #region GridControl 4 - Outstanding Inspections

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;
                bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridControl 5 - Outstanding Actions

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;
                bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void Chart_Preserve_User_Interaction(ChartControl chart)
        {
            // Ensure end-user rotation working if 3D chart //
            try
            {
                SimpleDiagram3D diagram = (SimpleDiagram3D)chart.Diagram;
                if (diagram != null)
                {
                    diagram.RuntimeRotation = true;
                    diagram.RuntimeScrolling = true;
                    diagram.RuntimeZooming = true;
                }
            }
            catch
            {
            }
        }

        private void bbiChartWizard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ChartControl chart = null;
            if (xtraTabControl1.SelectedTabPage == xtraTabPage1)
            {
                chart = chartControl2;
            }
            else
            {
                chart = chartControl1;
            }
            // Create a new Wizard.
            ChartWizard wizard = new ChartWizard(chart);

            // Obtain a Data page.
            //WizardDataPage page = wizard.DataPage;

            // Hide datasource-related tabs on the Data page.
            //page.HiddenPageTabs.Add(DataPageTab.AutoCreatedSeries);
            //page.HiddenPageTabs.Add(DataPageTab.SeriesBinding);

            // Invoke the Wizard window.
            wizard.ShowDialog();
            Chart_Preserve_User_Interaction(chart);  // Ensure end-user rotation still working if user converted from 2D to 3D chart //

        }


        #region ChartControl 1 - Outstanding Actions

        private void chartControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                pmChart.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region ChartControl 2 - Outstanding Actions

        private void chartControl2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                pmChart.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            GridView view = null;
            string strSelectedIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 4:  // Outstanding Inspections //

                    view = (GridView)gridControl4.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeID")) + ',';
                    }
                    try
                    {
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "tree");
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    break;
                case 5:  // Outstanding Actions //

                    view = (GridView)gridControl5.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                    }
                    try
                    {
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "action");
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    break;
            }
        }

        private void dateEditFromDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }

        private void btnLoadData_Click(object sender, EventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Outstanding Work...");
            }

            string strSiteIDs = Get_Selected_Sites();
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;
            GridView view = (GridView)gridControl4.MainView;
            view.BeginUpdate();
            sp01337_Inspections_DueTableAdapter.Fill(this.dataSet_AT.sp01337_Inspections_Due, dtFromDate, dtToDate, strSiteIDs);
            view.EndUpdate();

            string strJobTypeIDs = Get_Selected_JobTypes();
            string strContractorIDs = Get_Selected_Contractors();
            view = (GridView)gridControl5.MainView;
            view.BeginUpdate();
            sp01338_AT_Actions_DueTableAdapter.Fill(this.dataSet_AT.sp01338_AT_Actions_Due, dtFromDate, dtToDate, strSiteIDs, strJobTypeIDs, strContractorIDs);
            view.EndUpdate();

            //chartControl1.DataSource = gridControl5.DataSource;
            //chartControl1.SeriesDataMember = "DueWithinDays";
            //chartControl1.SeriesTemplate.ArgumentDataMember = "DueWithinDays";
            //chartControl1.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "DueWithinDays" });
            chartControl1.RefreshData();
            Chart_Preserve_User_Interaction(chartControl1);  // Ensure end-user rotation still working if user converted from 2D to 3D chart //
            chartControl2.RefreshData();
            Chart_Preserve_User_Interaction(chartControl2);  // Ensure end-user rotation still working if user converted from 2D to 3D chart //

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private string Get_Selected_Sites()
        {
            string strSiteIDs = "";
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0 || selection1.SelectedCount <= 0)
            {
                return "";
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSiteIDs += Convert.ToString(view.GetRowCellValue(i, "SiteID")) + ",";
                    }
                }
            }
            return strSiteIDs;
        }

        private string Get_Selected_JobTypes()
        {
            string strJobTypeIDs = "";
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0 || selection2.SelectedCount <= 0)
            {
                return "";
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strJobTypeIDs += Convert.ToString(view.GetRowCellValue(i, "JobID")) + ",";
                    }
                }
            }
            return strJobTypeIDs;
        }

        private string Get_Selected_Contractors()
        {
            string strContractorIDs = "";
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0 || selection3.SelectedCount <= 0)
            {
                return "";
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strContractorIDs += Convert.ToString(view.GetRowCellValue(i, "ContractorID")) + ",";
                    }
                }
            }
            return strContractorIDs;
        }




    }
}


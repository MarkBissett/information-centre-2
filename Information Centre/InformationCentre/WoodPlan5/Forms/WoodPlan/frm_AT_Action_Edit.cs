using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.Skins;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_AT_Action_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        public int intParentOwnershipID = 0;
        public string strParentNearestHouse = "";
        public string strParentTreeIDs = "";  // Only used when Block Adding actions with dummy inspections //

        public string strBlockAddedDummyInspectionIDs = "";

        public bool ibool_CalledByTreePicker = false;
        public string strID = "";
        public string strMapIDs = "";  // Only required when block editing from the map - used to tell the tree picker which objects to reload //

        ArrayList ArrayListFilteredPicklists;  // Holds all the picklists bound to sp01372 - used for iterating round to bind and unbind generic Enter, Leave and ButtonClick events //
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        private string i_strLastUsedSequencePrefix = "";

        private ArrayList arraylistFloatingPanels = new ArrayList();  // Holds which panels were floating when form was deactivated so the panels can be hidden then re-shown on form activate //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
   
        public RefreshGridState RefreshGridViewState39;  // Used by Grid View State Facilities //
        string i_str_AddedRecordIDs39 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string strDefaultPicturesPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        #endregion

        public frm_AT_Action_Edit()
        {
            InitializeComponent();
        }

        private void frm_AT_Action_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 20013;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            // Set form Permissions //
            stcFormPermissions sfpPermissions = (stcFormPermissions)this.FormPermissions[0];
            if (this.FormPermissions.Count > 0)
            {
                iBool_AllowAdd = sfpPermissions.blCreate;
                iBool_AllowEdit = sfpPermissions.blUpdate;
                iBool_AllowDelete = sfpPermissions.blDelete;
            }

            ArrayListFilteredPicklists = new ArrayList();  // Note: Each GridLookUpEdit within the array should have it's tag value set to it's value in the SQL PicklistHeader Table //
            ArrayListFilteredPicklists.Add(intPriorityIDGridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList1GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList3GridLookUpEdit);

            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);

            sp00190_Contractor_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00190_Contractor_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00190_Contractor_List_With_Blank);

            // Last Saved Record Details //
            sp01375_AT_User_Screen_SettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01375_AT_User_Screen_SettingsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings, this.GlobalSettings.UserID, FormID);

            sp01390_AT_Action_Edit_Previous_ActionsTableAdapter.Connection.ConnectionString = strConnectionString;

            sp01362_AT_Ownership_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01362_AT_Ownership_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01362_AT_Ownership_List_With_Blank);

            sp00214_Master_Job_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00214_Master_Job_List_With_BlankTableAdapter.Fill(this.dataSet_AT.sp00214_Master_Job_List_With_Blank);

            sp02066_AT_Action_Pictures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState39 = new RefreshGridState(gridView39, "SurveyPictureID");

            // Initialise Cost Centre Panel - Population of grid initiated by Dataset row position change or Ownership ID selection change //
            dateEditDateFrom.DateTime = GlobalSettings.LiveStartDate;
            dateEditDateTo.DateTime = GlobalSettings.LiveEndDate;
            sp01391_AT_Cost_Centres_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPicturesPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Pictures path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Pictures Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Populate Main Dataset //
            sp01388_AT_Action_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    int intDefaultContractor = 0;
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    try
                    {
                        intDefaultContractor = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesDefaultContractor"));
                    }
                    catch (Exception) { }
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["intInspectionID"] = intLinkedToRecordID;
                        drNewRow["InspectionReference"] = (string.IsNullOrEmpty(strLinkedToRecordDesc) && intLinkedToRecordID > 0 ? "[Unknown Inspection Reference]" : strLinkedToRecordDesc);
                        drNewRow["intActionBy"] = intDefaultContractor;
                        drNewRow["dtDueDate"] = DateTime.Today;
                        drNewRow["intOwnershipID"] = intParentOwnershipID;
                        drNewRow["strNearestHouse"] = strParentNearestHouse;
                        drNewRow["monJobWorkUnits"] = (decimal)1.00;
                        drNewRow["intScheduleOfRates"] = 1;
                        this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.Rows.Add(drNewRow);
                        //Get_Values_From_Current_Locality(intLocalityIDGridLookUpEdit);

                        // Get next sequence //
                        DataRow[] drFiltered = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = 'strJobNumberButtonEdit'");
                        if (drFiltered.Length != 0)  // Matching row found //
                        {
                            DataRow dr = drFiltered[0];
                            strJobNumberButtonEdit.EditValue = dr["ItemValue"].ToString();
                            // Calculate Next Sequence Number by triggering button click event of strTreeRefButtonEdit //
                            if (!string.IsNullOrEmpty(strJobNumberButtonEdit.EditValue.ToString())) strJobNumberButtonEdit_ButtonClick(strJobNumberButtonEdit, new DevExpress.XtraEditors.Controls.ButtonPressedEventArgs(strJobNumberButtonEdit.Properties.Buttons[1]));
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.NewRow();
                        drNewRow["strMode"] = "blockadd";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["intInspectionID"] = 0;
                        //drNewRow["intInspectionID"] = intLinkedToRecordID;
                        //drNewRow["InspectionReference"] = strLinkedToRecordDesc;
                        drNewRow["monJobWorkUnits"] = (decimal)1.00;
                        drNewRow["intScheduleOfRates"] = 1;
                        drNewRow["dtDueDate"] = DateTime.Today;
                        this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.Rows.Add(drNewRow);
                        // Get next sequence //
                        DataRow[] drFiltered = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = 'strJobNumberButtonEdit'");
                        if (drFiltered.Length != 0)  // Matching row found //
                        {
                            DataRow dr = drFiltered[0];
                            strJobNumberButtonEdit.EditValue = dr["ItemValue"].ToString();
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["intInspectionID"] = 0;
                        this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.Rows.Add(drNewRow);
                        this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp01388_AT_Action_EditTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    Load_Linked_Pictures();
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
            Attach_Enter_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach Enter Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //
            Attach_ButtonClick_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach ButtonClick Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //

            bbiPasteAllValues.Enabled = (this.strFormMode == "add" || ibool_CalledByTreePicker ? true : false);
            bbiPasteSelectedValues.Enabled = (this.strFormMode == "add" || ibool_CalledByTreePicker ? true : false);
            bbiReload.Enabled = (this.strFormMode == "add" || ibool_CalledByTreePicker ? true : false);
            bbiCopyDetails.Enabled = (this.strFormMode == "add" || this.strFormMode == "edit" || ibool_CalledByTreePicker ? true : false);
        }

        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).Enter += new EventHandler(Generic_Enter);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).Enter -= new EventHandler(Generic_Enter);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void Generic_Enter(object sender, EventArgs e)
        {
            if (!checkEditSychronise.Checked) return;

            // Focus VerticalGrid Row with name same as current Control //
            DevExpress.XtraEditors.BaseEdit be = (DevExpress.XtraEditors.BaseEdit)sender;
            string strName = "row" + be.Name;

            try
            {
                BaseRow br = vGridControl1.FocusedRow;
                if (br.Name != strName)
                {
                    BaseRow br2 = vGridControl1.Rows[strName];
                    if (br2 == null) return;
                    if (br2.Visible)
                    {
                        vGridControl1.FocusedRow = br2;
                    }
                    else
                    {
                        vGridControl1.FocusFirst();
                    }
                }
            }
            catch (Exception)
            {
            }
        }


        DataTable data;  // Used for holding filtered dataset for picklists //
        private void Attach_Enter_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter += new EventHandler(Filter_GridView);
            }
        }

        private void Detach_Enter_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter -= new EventHandler(Filter_GridView);
            }
        }

        private void Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            int intHeaderID = Convert.ToInt32(glue.Tag);
            data = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks;
            DataView newView = new DataView(data);
            newView.RowFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            glue.Properties.DataSource = newView;
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }


        private void Attach_Leave_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave += new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Detach_Leave_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave -= new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Clear_Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            glue.Properties.DataSource = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks.DefaultView;
            view.EndUpdate();
        }


        private void Attach_ButtonClick_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void Detach_ButtonClick_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick -= new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void ButtonClick_GridView(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    GridLookUpEdit glue = (GridLookUpEdit)sender;
                    int intHeaderID = Convert.ToInt32(glue.Tag);
                    String strPickListName = "";
                    switch (glue.Name)
                    {
                        case "intPriorityIDGridLookUpEdit":
                            strPickListName = "Job Priorities";
                            break;
                        case "UserPickList1GridLookUpEdit":
                            strPickListName = "Action User Defined 1";
                            break;
                        case "UserPickList2GridLookUpEdit":
                            strPickListName = "Action User Defined 2";
                            break;
                        case "UserPickList3GridLookUpEdit":
                            strPickListName = "Action User Defined 3";
                            break;
                        default:
                            break;
                    }
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, intHeaderID, strPickListName);
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);
                    this.ValidateChildren();
                }
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Action", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
            
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        intActionGridLookUpEdit.Focus();

                        InspectionReferenceButtonEdit.Properties.Buttons[0].Enabled = true;
                        InspectionReferenceButtonEdit.Properties.Buttons[1].Enabled = true;

                        strJobNumberButtonEdit.Properties.ReadOnly = false;
                        strJobNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        strJobNumberButtonEdit.Properties.Buttons[1].Enabled = true;

                        WorkOrderDescriptionTextEdit.Properties.Buttons[0].Enabled = false;
                        gridControl39.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        dtDoneDateDateEdit.Focus();

                        InspectionReferenceButtonEdit.Properties.Buttons[0].Enabled = false;
                        InspectionReferenceButtonEdit.Properties.Buttons[1].Enabled = false;

                        strJobNumberButtonEdit.Properties.ReadOnly = true;
                        strJobNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        strJobNumberButtonEdit.Properties.Buttons[1].Enabled = false;

                        WorkOrderDescriptionTextEdit.Properties.Buttons[0].Enabled = false;
                        gridControl39.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        dtDoneDateDateEdit.Focus();

                        InspectionReferenceButtonEdit.Properties.Buttons[0].Enabled = true;
                        InspectionReferenceButtonEdit.Properties.Buttons[1].Enabled = true;

                        strJobNumberButtonEdit.Properties.ReadOnly = false;
                        strJobNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        strJobNumberButtonEdit.Properties.Buttons[1].Enabled = true;

                        WorkOrderDescriptionTextEdit.Properties.Buttons[0].Enabled = true;
                        gridControl39.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        dtDoneDateDateEdit.Focus();

                        InspectionReferenceButtonEdit.Properties.Buttons[0].Enabled = true;
                        InspectionReferenceButtonEdit.Properties.Buttons[0].Enabled = true;

                        strJobNumberButtonEdit.Properties.ReadOnly = true;
                        strJobNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        strJobNumberButtonEdit.Properties.Buttons[1].Enabled = false;

                        WorkOrderDescriptionTextEdit.Properties.Buttons[0].Enabled = false;
                        gridControl39.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            ibool_FireLayoutUpdateCode = true;  // Refresh Vertical Grid Row Labels to match those of the Layout control //
            Refresh_Vertical_Grid_Row_Labels(dataLayoutControl1);

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            intActionGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intActionGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intActionGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intActionGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intActionByGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intActionByGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intActionByGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intActionByGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intSupervisorGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intSupervisorGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intSupervisorGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intSupervisorGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intPriorityIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intPriorityIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intPriorityIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intPriorityIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intOwnershipIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intOwnershipIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intOwnershipIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intOwnershipIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            CostCentreCodeButtonEdit.Properties.Buttons[1].Enabled = false;
            CostCentreCodeButtonEdit.Properties.Buttons[1].Visible = false;
            CostCentreCodeButtonEdit.Properties.Buttons[2].Enabled = false;
            CostCentreCodeButtonEdit.Properties.Buttons[2].Visible = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[2].Visible = false;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "2011,8001,8001,9026,2012,9047,9048,9049", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 2011:  // Action Type //    
                        {
                            intActionGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intActionGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intActionGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intActionGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 8001:  // Action By & SUpervisor //    
                        {
                            intActionByGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intActionByGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intActionByGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intActionByGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                            intSupervisorGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intSupervisorGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intSupervisorGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intSupervisorGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;              
                        }
                        break;
                    case 9026:  // Priority //    
                        {
                            intPriorityIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intPriorityIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intPriorityIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intPriorityIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 2012:  // Ownership \ Cost Centre //    
                        {
                            intOwnershipIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intOwnershipIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intOwnershipIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intOwnershipIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            CostCentreCodeButtonEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            CostCentreCodeButtonEdit.Properties.Buttons[1].Visible = boolUpdate;
                            CostCentreCodeButtonEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            CostCentreCodeButtonEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9047:  // User Define 1 //    
                        {
                            UserPickList1GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9048:  // User Define 2 //    
                        {
                            UserPickList2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9049:  // User Define 3 //    
                        {
                            UserPickList3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                }
            }
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_AT_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl39.MainView;

            intRowHandles = view.GetSelectedRows();
            int intActionID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
            if (currentRow != null) intActionID = (currentRow["intActionID"] == null ? 0 : Convert.ToInt32(currentRow["intActionID"]));

            if (i_int_FocusedGrid == 39)  // Linked Pictures //
            {
                view = (GridView)gridControl39.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intActionID > 0 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                if (iBool_AllowEdit && intRowHandles.Length == 1 && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView39 navigator custom buttons //
            view = (GridView)gridControl39.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intActionID > 0 && strFormMode != "view" ? true : false);
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && strFormMode != "view" && intRowHandles.Length == 1 ? true : false);
            gridControl39.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
        }


        private void frm_AT_Action_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in arraylistFloatingPanels)
            {
                dp.Show();
            }
        }

        private void frm_AT_Action_Edit_Deactivate(object sender, EventArgs e)
        {
            arraylistFloatingPanels = new ArrayList();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in dockManager1.Panels)
            {
                if (dp.Dock == DevExpress.XtraBars.Docking.DockingStyle.Float && dp.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible)
                {
                    arraylistFloatingPanels.Add(dp);
                    dp.Hide();
                }
            }
        }

        private void frm_AT_Action_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        Detach_Enter_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Enter Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_Leave_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach Leave Event from All Filtered GridLookUpEdits... Attached on Form Load Event //
                        Detach_ButtonClick_From_GridLookUpEdits(ArrayListFilteredPicklists);  // Detach ButtonClick Event from All Filtered GridLookUpEdits... Attached on Form Load Event //

                        if (!string.IsNullOrEmpty(strBlockAddedDummyInspectionIDs))  // BlockAdding, so remove any generated dummy inspections //
                        {
                            try
                            {
                                DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                RemoveRecords.sp01383_AT_Inspection_Manager_Delete("inspection", strBlockAddedDummyInspectionIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception ex)
                            {
                                XtraMessageBox.Show("An error occurred [" + ex.Message + "] while removing the block added Dummy Inspection(s)!\n\nYou should locate them and remove them manually. If the problem persists contact Technical Support!", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            this.sp01388ATActionEditBindingSource.EndEdit();
            try
            {
                this.sp01388_AT_Action_EditTableAdapter.Update(dataSet_AT_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["intActionID"]) + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Values returned from Update SP //

                if (!string.IsNullOrEmpty(strBlockAddedDummyInspectionIDs))
                {
                    strBlockAddedDummyInspectionIDs = strBlockAddedDummyInspectionIDs.Replace(',', ';');  // Swap commas for semi-colons //

                    // Notify any open forms which reference thie block added dummy inspections that they will need to refresh their data on activating //
                    Broadcast.Inspection_Refresh(this.ParentForm, "", strBlockAddedDummyInspectionIDs, strParentTreeIDs);   // Don't pass a form name (second parameter) so all form's including caller are refreshed //
                }
            }

            // Notify any open forms which reference this data that they will need to refresh their data on activating //
            Broadcast.Action_Refresh(this.ParentForm, this.Name, strNewIDs, "");

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            return "";  // No problems //
        }

        private void Store_Last_Saved_Record_Details(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit)
                {
                    string strName = ((DevExpress.XtraEditors.BaseEdit)item2).Name;
                    string strText = "";
                    if (strName == "strJobNumberButtonEdit")
                    {
                        if (i_strLastUsedSequencePrefix != "")
                        {
                            strText = i_strLastUsedSequencePrefix;
                        }
                        else
                        {
                            continue;  // Ignore Sequence if empty //
                        }
                    }
                    else if (strName == "intActionIDTextEdit" || strName == "intBudgetIDTextEdit" || strName == "intCostCentreIDTextEdit" || strName == "DateAddedDateEdit" ||
                             strName == "GUIDTextEdit" || strName == "intInspectionIDTextEdit" || strName == "InspectionReferenceButtonEdit" || strName == "intJobRateActualSpinEdit" ||
                             strName == "intJobRateBudgetedSpinEdit" || strName == "intPrevActionIDTextEdit" || strName == "intSortOrderSpinEdit" || strName == "intWorkOrderIDTextEdit" ||
                             strName == "JobRateDescButtonEdit" || strName == "monJobRateBudgetedSpinEdit" || strName == "monBudgetedCostSpinEdit" || strName == "ActualJobRateDescTextEdit" ||
                             strName == "monJobRateActualSpinEdit" || strName == "monDiscountRateSpinEdit" || strName == "monActualCostSpinEdit" || strName == "strNearestHouseTextEdit")
                    {
                        continue;  // Ignore Record ID and costing Fields //
                    }
                    else
                    {
                        strText = (((DevExpress.XtraEditors.BaseEdit)item2).EditValue == null ? null : ((DevExpress.XtraEditors.BaseEdit)item2).EditValue.ToString());
                    }
                    DataRow[] drFiltered;
                    drFiltered = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = '" + strName + "'");
                    if (drFiltered.Length != 0)  // Matching row found so Update it //
                    {
                        DataRow drExistingRow = drFiltered[0];
                        drExistingRow["ItemValue"] = strText;
                    }
                    else  // No Matching row so Add it to DataSet //
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.NewRow();
                        drNewRow["ScreenID"] = FormID;
                        drNewRow["UserID"] = this.GlobalSettings.UserID;
                        drNewRow["ItemName"] = strName;
                        drNewRow["ItemValue"] = strText;
                        this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Rows.Add(drNewRow);
                    }
                }
                if (item2 is ContainerControl) Store_Last_Saved_Record_Details(item.Controls);
            }
            // Dummy Row for Calculate Job Rates //
            DataRow[] drFiltered2;
            drFiltered2 = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Select("ItemName = 'Calculate Job Rate'");
            if (drFiltered2.Length != 0)  // Matching row found so Update it //
            {
                DataRow drExistingRow = drFiltered2[0];
                drExistingRow["ItemValue"] = "Calculated Value";
            }
            else  // No Matching row so Add it to DataSet //
            {
                DataRow drNewRow;
                drNewRow = this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.NewRow();
                drNewRow["ScreenID"] = FormID;
                drNewRow["UserID"] = this.GlobalSettings.UserID;
                drNewRow["ItemName"] = "Calculate Job Rate";
                drNewRow["ItemValue"] = "Calculated Value";
                this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Rows.Add(drNewRow);
            }

            try
            {
                this.sp01375_AT_User_Screen_SettingsTableAdapter.Update(dataSet_AT_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while copying the action details [" + ex.Message + "]!\n\nTry copying again - if the problem persists, contact Technical Support.", "Copy Action Details", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiPasteAllValues_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Control ctrlOnForm = null;
            string strName = "";
            string strText = "";
            bool boolSequenceUpdated = false;
            dataLayoutControl1.BeginUpdate();
            foreach (DataRow dr in dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings.Rows)
            {
                strName = dr["ItemName"].ToString();
                if (strName == "Calculate Job Rate") continue;  // Ignore this value [used later in process] //

                strText = dr["ItemValue"].ToString();
                Control[] controls = this.Controls.Find(strName, true);
                if (controls.Length == 1)  // Only proceed if just one control was found matching the name // 
                {
                    ctrlOnForm = controls[0] as Control;
                    if (ctrlOnForm is DevExpress.XtraEditors.BaseEdit)
                    {
                        if (strName == "strJobNumberButtonEdit")
                        {
                            boolSequenceUpdated = true;
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = "";  // Clear any sequence first //
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
                        }
                        if (string.IsNullOrEmpty(strText))
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = DBNull.Value;
                        }
                        else
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = strText;
                        }
                    }
                }
            }
            // Calculate Next Sequence Number by triggering button click event of strTreeRefButtonEdit //
            if (!string.IsNullOrEmpty(strJobNumberButtonEdit.EditValue.ToString()) && boolSequenceUpdated) strJobNumberButtonEdit_ButtonClick(strJobNumberButtonEdit, new DevExpress.XtraEditors.Controls.ButtonPressedEventArgs(strJobNumberButtonEdit.Properties.Buttons[1]));

            dataLayoutControl1.EndUpdate();
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
        }

        private void bbiPasteSelectedValues_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Loop round all objects in DataLayout Control and store the child objects and the Layout Captions in HashTable //
            Hashtable htObjects = new Hashtable();
            foreach (Control item in this.Controls)
            {
                if (item is DevExpress.XtraLayout.LayoutControl)
                {
                    DevExpress.XtraLayout.LayoutControl item2 = (DevExpress.XtraLayout.LayoutControl)item;
                    foreach (object obj in item2.Items)
                    {
                        if (obj is DevExpress.XtraLayout.LayoutControlItem)
                        {
                            DevExpress.XtraLayout.LayoutControlItem lci = (DevExpress.XtraLayout.LayoutControlItem)obj;
                            if (lci.Control != null)
                            {
                                if (lci.Control.Name == "GUIDTextEdit" || lci.Control.Name == "intActionIDTextEdit") continue;
                                htObjects.Add(lci.Control.Name, lci.Text);
                            }
                        }
                    }
                    htObjects.Add("Calculate Job Rate", "Calculate Job Rate");
                }
            }
            frm_Core_Recall_Selected_Values fChildForm = new frm_Core_Recall_Selected_Values();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intPassedInScreenID = this.FormID;
            fChildForm.htPassedInObjects = htObjects;
            if (fChildForm.ShowDialog() == DialogResult.OK)
            {
                htObjects = fChildForm.htPassedInObjects;
            }

            // Process the updated HashTable //
            Control ctrlOnForm = null;
            string strName = "";
            string strText = "";
            bool boolSequenceUpdated = false;
            dataLayoutControl1.BeginUpdate();
            IDictionaryEnumerator enumerator = htObjects.GetEnumerator();
            while (enumerator.MoveNext())
            {
                strName = enumerator.Key.ToString();
                if (strName == "intActionIDTextEdit") continue;  // Ignore this value [only stored when user clicks Copy Actions] //
                strText = enumerator.Value.ToString();
                Control[] controls = this.Controls.Find(strName, true);
                if (controls.Length == 1)  // Only proceed if just one control was found matching the name // 
                {
                    ctrlOnForm = controls[0] as Control;
                    if (ctrlOnForm is DevExpress.XtraEditors.BaseEdit)
                    {
                        if (strName == "strJobNumberButtonEdit")
                        {
                            boolSequenceUpdated = true;
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = "";  // Clear any sequence first //
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
                        }
                        if (string.IsNullOrEmpty(strText))
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = DBNull.Value;
                        }
                        else
                        {
                            ((DevExpress.XtraEditors.BaseEdit)ctrlOnForm).EditValue = strText;
                        }
                    }
                }
            }
            // Calculate Next Sequence Number by triggering button click event of strTreeRefButtonEdit //
            if (!string.IsNullOrEmpty(strJobNumberButtonEdit.EditValue.ToString()) && boolSequenceUpdated) strJobNumberButtonEdit_ButtonClick(strJobNumberButtonEdit, new DevExpress.XtraEditors.Controls.ButtonPressedEventArgs(strJobNumberButtonEdit.Properties.Buttons[1]));

            dataLayoutControl1.EndUpdate();
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //


        }

        private void bbiReload_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Reload Last Saved Record Details into memory//
            sp01375_AT_User_Screen_SettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01375_AT_User_Screen_SettingsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01375_AT_User_Screen_Settings, this.GlobalSettings.UserID, FormID);
        }

        private void bbiCopyDetails_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Save records attributes to User_Settings table for later recall //
            DevExpress.Utils.WaitDialogForm loading = new DevExpress.Utils.WaitDialogForm("Storing Details to Memory...", "Actions");
            loading.Show();
            Store_Last_Saved_Record_Details(this.Controls);
            XtraMessageBox.Show("Details Copied.", "Copy Action Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
            loading.Hide();
            loading.Dispose();
        }

        private void LoadPriorActionRecords()
        {
            string strInspectionID = "";
            string strCurrentActionID = "";
            DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
            if (currentRow != null)
            {
                strInspectionID = (currentRow["intInspectionID"] == null ? "" : currentRow["intInspectionID"].ToString() + ",");
                strCurrentActionID = (currentRow["intActionID"] == null ? "" : currentRow["intActionID"].ToString() + ",");
            }
            vGridControl1.BeginUpdate();
            sp01390_AT_Action_Edit_Previous_ActionsTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01390_AT_Action_Edit_Previous_Actions, strInspectionID, strCurrentActionID);
            vGridControl1.EndUpdate();
        }

        private void LoadCostCentresGrid()
        {
            string strOwnershipIDs = (string.IsNullOrEmpty(intOwnershipIDGridLookUpEdit.EditValue.ToString()) || Convert.ToInt32(intOwnershipIDGridLookUpEdit.EditValue) == 0 ? "" : intOwnershipIDGridLookUpEdit.EditValue.ToString() + ",");
            gridControl1.BeginUpdate();
            sp01391_AT_Cost_Centres_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01391_AT_Cost_Centres_With_Blank, strOwnershipIDs, dateEditDateFrom.DateTime, dateEditDateTo.DateTime);
            gridControl1.EndUpdate();

            DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
            if (currentRow != null)
            {
                if (string.IsNullOrEmpty(currentRow["intOwnershipID"].ToString()) || Convert.ToInt32(currentRow["intOwnershipID"]) == 0)
                {
                    CostCentreCodeButtonEdit.Enabled = false;
                }
                else
                {
                    CostCentreCodeButtonEdit.Enabled = true;
                }
            }
        }

        private void Load_Linked_Pictures()
        {
            string strActionID = "";
            DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
            if (currentRow != null)
            {
                strActionID = (currentRow["intActionID"] == null ? "" : currentRow["intActionID"].ToString() + ",");
            }
            gridControl39.BeginUpdate();
            this.RefreshGridViewState39.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp02066_AT_Action_Pictures_ListTableAdapter.Fill(dataSet_AT.sp02066_AT_Action_Pictures_List, strActionID, strDefaultPicturesPath);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState39.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl39.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs39 != "")
            {
                strArray = i_str_AddedRecordIDs39.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl39.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyPictureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs39 = "";
            }
        }

        private void Set_Choose_Rates_Button_Enabled_Status()
        {
            /*DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
            if (currentRow == null) return;
            if (string.IsNullOrEmpty(currentRow["intScheduleOfRates"].ToString()) || Convert.ToInt32(currentRow["intScheduleOfRates"]) == 0)
            {
                JobRateDescButtonEdit.Properties.Buttons[0].Enabled = false;
            }
            else
            {
                JobRateDescButtonEdit.Properties.Buttons[0].Enabled = true;
            }*/
        }

        private void Calculate_Total_Cost(string strType)
        {
            this.sp01388ATActionEditBindingSource.EndEdit();
            DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
            if (currentRow == null) return;
            if (strType == "Budgeted")
            {
                currentRow["monBudgetedCost"] = decimal.Round((string.IsNullOrEmpty(currentRow["monJobRateBudgeted"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["monJobRateBudgeted"])) * (string.IsNullOrEmpty(currentRow["monJobWorkUnits"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["monJobWorkUnits"])), 2);
                this.sp01388ATActionEditBindingSource.EndEdit();
            }
            else // Actual //
            {
                decimal decAmount = decimal.Round((string.IsNullOrEmpty(currentRow["monJobRateActual"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["monJobRateActual"])) * (string.IsNullOrEmpty(currentRow["monJobWorkUnits"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["monJobWorkUnits"])), 2);
                currentRow["monActualCost"] = decimal.Round(decAmount - (decAmount * (string.IsNullOrEmpty(currentRow["monDiscountRate"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["monDiscountRate"])) / 100), 2);
                this.sp01388ATActionEditBindingSource.EndEdit();
            }
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else  // Load in Past Actions... //
                {
                    if (this.strFormMode != "blockedit" && this.strFormMode != "blockadd") LoadPriorActionRecords();
                    LoadCostCentresGrid();
                    Set_Choose_Rates_Button_Enabled_Status();
                    
                    Load_Linked_Pictures();
                    GridView view = (GridView)gridControl39.MainView;
                    view.ExpandAllGroups();

                    DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
                    if (currentRow != null)
                    {
                        WorkOrderDescriptionTextEdit.Properties.Buttons[0].Enabled = (string.IsNullOrEmpty(currentRow["intWorkOrderID"].ToString()) || Convert.ToInt32(currentRow["intWorkOrderID"]) == 0 ? false : true);
                    }
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void InspectionReferenceButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                frm_AT_Select_Inspection fChildForm = new frm_AT_Select_Inspection();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intAmenityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                {
                    DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
                    if (currentRow != null)
                    {
                        currentRow["intInspectionID"] = fChildForm.intSelectedID;
                        currentRow["InspectionReference"] = fChildForm.strSelectedValue;
                        currentRow["intOwnershipID"] = fChildForm.intTreeOwnershipID;
                        currentRow["intBudgetID"] = 0;
                        currentRow["intCostCentreID"] = 0;
                        currentRow["CostCentreCode"] = "";
                        currentRow["strNearestHouse"] = fChildForm.strNearestHouseName;
                        if (this.strFormMode != "blockedit" && this.strFormMode != "blockadd") LoadPriorActionRecords();
                    }
                }
            }
            else if (e.Button.Tag.ToString() == "view")  // View Inspection Button //
            {
                DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
                if (currentRow != null)
                {
                    int intInspectionID = (string.IsNullOrEmpty(currentRow["intInspectionID"].ToString()) ? 0 : Convert.ToInt32(currentRow["intInspectionID"]));
                    if (intInspectionID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to display parent inspection - no inspection set as parent.", "View Parent Inspection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    var fChildForm = new frm_AT_Inspection_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = intInspectionID.ToString() + ",";
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = "frm_AT_Action_Edit";
                    fChildForm.intRecordCount = 1;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
            }
        }

        private void InspectionReferenceButtonEdit_EditValueChanged(object sender, EventArgs e)
        {
        }

        private void InspectionReferenceButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "edit" || this.strFormMode == "add") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(InspectionReferenceButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(InspectionReferenceButtonEdit, "");
            }
        }

        private void intActionGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(intActionGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(intActionGridLookUpEdit, "");
            }
        }

        private void strJobNumberButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            string strTableName = "tblAction";
            string strFieldName = "strJobNumber";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    strJobNumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    i_strLastUsedSequencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                // Get next value in sequence //
                string strSequence = strJobNumberButtonEdit.EditValue.ToString() ?? "";

                SequenceNumberGetNext sequence = new SequenceNumberGetNext();
                string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //

                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";


                // Check if value is already present within dataset //
                string strCurrentActionID = intActionIDTextEdit.EditValue.ToString();
                if (string.IsNullOrEmpty(strCurrentActionID)) strCurrentActionID = "";
                bool boolUniqueValueFound = false;
                bool boolDuplicateFound = false;
                do
                {
                    boolDuplicateFound = false;
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    foreach (DataRow dr in this.dataSet_AT_DataEntry.sp01388_AT_Action_Edit.Rows)
                    {
                        if (dr["strJobNumber"].ToString() == (strSequence + strTempNumber))// && dr["intInspectionID"].ToString() != strCurrentInspectionID)
                        {
                            intIncrement++;
                            boolDuplicateFound = true;
                            break;
                        }
                    }
                    if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
                } while (!boolUniqueValueFound);
                strJobNumberButtonEdit.EditValue = strSequence + strTempNumber;
            }
        }

        private void strJobNumberButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            /*ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(strJobNumberButtonEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(strJobNumberButtonEdit, "");
            }*/
        }

        private void intInspectorGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 1, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00190_Contractor_List_With_BlankTableAdapter.Fill(woodPlanDataSet.sp00190_Contractor_List_With_Blank);
                }
            }
        }

        private void dtDueDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            /*DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(dtDueDateDateEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(dtDueDateDateEdit, "");
            }*/
        }

        private void intActionGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 902, "Job Rates");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00214_Master_Job_List_With_BlankTableAdapter.Fill(this.dataSet_AT.sp00214_Master_Job_List_With_Blank);
                }
            }
        }

        private void intActionByGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 1, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00190_Contractor_List_With_BlankTableAdapter.Fill(woodPlanDataSet.sp00190_Contractor_List_With_Blank);
                }
            }
        }

        private void intSupervisorGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 1, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00190_Contractor_List_With_BlankTableAdapter.Fill(woodPlanDataSet.sp00190_Contractor_List_With_Blank);
                }
            }
        }

        private void intOwnershipIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 900, "Cost Centres");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01362_AT_Ownership_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01362_AT_Ownership_List_With_Blank);
                }
            }
        }

        private void intOwnershipIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Field //       
        }
        
        private void intOwnershipIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;
            ibool_ignoreValidation = true;
            
            LoadCostCentresGrid();

            DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
            if (currentRow != null)
            {
                if (string.IsNullOrEmpty(currentRow["intCostCentreID"].ToString()) || Convert.ToInt32(currentRow["intCostCentreID"]) != 0) currentRow["intCostCentreID"] = 0;
                if (string.IsNullOrEmpty(currentRow["CostCentreCode"].ToString()) || currentRow["CostCentreCode"].ToString() != "") currentRow["CostCentreCode"] = "";
                if (string.IsNullOrEmpty(currentRow["intBudgetID"].ToString()) || Convert.ToInt32(currentRow["intBudgetID"]) != 0) currentRow["intBudgetID"] = 0;
                this.sp01388ATActionEditBindingSource.EndEdit();
            }
            return;
        }

        private void CostCentreCodeButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 900, "Cost Centres");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    LoadCostCentresGrid();
                }
            }
        }

        private void JobRateDescButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                if (strFormMode == "blockedit" || strFormMode == "blockadd")
                {
                    this.sp01388ATActionEditBindingSource.EndEdit();
                    if (currentRow == null) return;
                    int intScheduleOfRates = (string.IsNullOrEmpty(currentRow["intScheduleOfRates"].ToString()) || Convert.ToInt32(currentRow["intScheduleOfRates"]) == 0 ? 0 : 1);
                    if (intScheduleOfRates == 1)  // Don't open the Select Rate screen, set dummy value in rate so in Save SP, system will resolve the rates //
                    {
                        currentRow["JobRateDesc"] = "Calculated on Save";
                        currentRow["monJobRateBudgeted"] = (decimal)0.00;
                        currentRow["intJobRateBudgeted"] = -1;
                        this.sp01388ATActionEditBindingSource.EndEdit();
                        return;
                    }
                }
                // At this point, so OK to open Select Rate screen //
                this.sp01388ATActionEditBindingSource.EndEdit();
                if (currentRow == null) return;

                int intOnScheduleOfRates = (string.IsNullOrEmpty(currentRow["intScheduleOfRates"].ToString()) ? 0 : Convert.ToInt32(currentRow["intScheduleOfRates"]));
                int intInspectionID = (string.IsNullOrEmpty(currentRow["intInspectionID"].ToString()) ? 0 : Convert.ToInt32(currentRow["intInspectionID"]));
                int intActionBy = (string.IsNullOrEmpty(currentRow["intActionBy"].ToString()) ? 0 : Convert.ToInt32(currentRow["intActionBy"]));
                int intAction = (string.IsNullOrEmpty(currentRow["intAction"].ToString()) ? 0 : Convert.ToInt32(currentRow["intAction"]));
                if (intOnScheduleOfRates == 1 && (intInspectionID == 0 || intAction == 0))
                {
                    XtraMessageBox.Show("No Parent Inspection and \\ or Action has been set for this action - the system will be unable to highlight best match rates for you.", "Select Job Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                frm_AT_Action_Edit_Select_Job_Rate fChildForm = new frm_AT_Action_Edit_Select_Job_Rate();
                fChildForm.intJobRate_RateID = (string.IsNullOrEmpty(currentRow["intJobRateBudgeted"].ToString()) ? 0 : Convert.ToInt32(currentRow["intJobRateBudgeted"]));
                fChildForm.intOnScheduleOfRates = intOnScheduleOfRates;
                fChildForm.intInspectionID = intInspectionID;
                fChildForm.intActionByID = intActionBy;
                fChildForm.intAction = intAction;

                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    currentRow["JobRateDesc"] = fChildForm.strSelectedRateDescription;
                    currentRow["monJobRateBudgeted"] = fChildForm.decSelectedRate;
                    currentRow["intJobRateBudgeted"] = fChildForm.intSelectedRateID;
                    this.sp01388ATActionEditBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered Job Rate.\n\nAre you sure you wish to proceed?", "Clear Job Rate", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    currentRow["JobRateDesc"] = "";
                    currentRow["monJobRateBudgeted"] = (decimal)0.00;
                    currentRow["intJobRateBudgeted"] = 0;
                    this.sp01388ATActionEditBindingSource.EndEdit();
                }
            }
            Calculate_Total_Cost("Budgeted");
            Calculate_Total_Cost("Actual");
        }

        private void intScheduleOfRatesCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            Set_Choose_Rates_Button_Enabled_Status();
        }

        private void monJobRateBudgetedSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Field //       
        }

        private void monJobRateBudgetedSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_ignoreValidation) return;
            ibool_ignoreValidation = true;
            
            Calculate_Total_Cost("Budgeted");
        }
        
        private void monJobWorkUnitsSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Field //       
        }

        private void monJobWorkUnitsSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_ignoreValidation) return;
            ibool_ignoreValidation = true;

            Calculate_Total_Cost("Budgeted");
            Calculate_Total_Cost("Actual");
        }

        private void WorkOrderDescriptionTextEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            ButtonEdit be = (ButtonEdit)sender;
            if (be.EditValue == null || string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The current Action is not linked to a Work Order.\n\nIf you need to add this action to a Work Order, use the Work Order Manager screen.", "View Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intWorkOrderID = Convert.ToInt32(be.EditValue);
            if (intWorkOrderID <= 0) return;

            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            frm_AT_WorkOrder_Edit fChildForm = new frm_AT_WorkOrder_Edit();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = intWorkOrderID.ToString() + ",";
            fChildForm.strFormMode = "edit";
            fChildForm.strCaller = "frm_AT_WorkOrder_Manager";
            fChildForm.intRecordCount = 1;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.fProgress = fProgress;
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void intActionByGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[Disabled] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void intActionByGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        private void intSupervisorGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[Disabled] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void intSupervisorGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        private void intActionGridLookUpEdit_Enter(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[JobDisabled] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void intActionGridLookUpEdit_Leave(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();
        }

        #endregion


        #region Toolbar

        private void bciShowPrevious_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciShowPrevious.Checked)
            {
                dockPanelLastAction.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelLastAction.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void dockPanelLastInspection_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciShowPrevious.Checked) bciShowPrevious.Checked = false;
            //this.dataSet_AT_DataEntry.sp01367_AT_Inspections_For_Trees.Clear();  // Empty Inspection grid to conserve memory //
        }


        #endregion


        #region DataLayoutControl1

        bool ibool_FireLayoutUpdateCode = false;  // Used to stop firing everytime a programatic change is made to the Layout Control //
        private void dataLayoutControl1_LayoutUpdate(object sender, EventArgs e)
        {
            if (!ibool_FireLayoutUpdateCode) return;
            DevExpress.XtraLayout.LayoutControl lc = (DevExpress.XtraLayout.LayoutControl)sender;
            Refresh_Vertical_Grid_Row_Labels(lc);
        }

        private void dataLayoutControl1_HideCustomization(object sender, EventArgs e)
        {
            ibool_FireLayoutUpdateCode = true;
        }

        private void Refresh_Vertical_Grid_Row_Labels(DevExpress.XtraLayout.LayoutControl lc)
        {
            vGridControl1.BeginUpdate();
            foreach (object obj in lc.Items)
            {
                if (obj is DevExpress.XtraLayout.LayoutControlItem)
                {
                    DevExpress.XtraLayout.LayoutControlItem lci = (DevExpress.XtraLayout.LayoutControlItem)obj;
                    if (lci.TypeName != "LayoutControlItem") continue;
                    string strName = "row" + lci.Control.Name;
                    BaseRow br2 = vGridControl1.Rows[strName];
                    if (br2 == null) continue;
                    if (br2.Properties.Caption != lci.Text) br2.Properties.Caption = lci.Text;
                }
            }
            vGridControl1.EndUpdate();
            ibool_FireLayoutUpdateCode = false;

        }

        #endregion


        #region vGridControl1

        private void vGridControl1_CustomDrawRowHeaderCell(object sender, DevExpress.XtraVerticalGrid.Events.CustomDrawRowHeaderCellEventArgs e)
        {
            // Paint readonly rows as if they were not read only //
            /*EditorRow row = e.Row as EditorRow;
            if (row == null) return;
            if (row.Properties.ReadOnly)
            {
                AppearanceObject app = vGridControl1.ViewInfo.PaintAppearance.GetAppearance(); //.RowHeaderPanel;
                if (vGridControl1.FocusedRow == row) app = vGridControl1.ViewInfo.PaintAppearance.FocusedRow;
                e.Appearance.Assign(app);
            }*/
        }

        #endregion


        #region PopupContainerControl1

        private void btnOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void CostCentreCodeButtonEdit_Popup(object sender, EventArgs e)
        {
            // Select correct row in Cost Centres grid //
            DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
            if (currentRow != null)
            {
                int intCostCentreID = 0;
                if (String.IsNullOrEmpty(currentRow["intCostCentreID"].ToString()) || currentRow["intCostCentreID"].ToString() == "0")
                {
                    intCostCentreID = 0;
                }
                else
                {
                    intCostCentreID = Convert.ToInt32(currentRow["intCostCentreID"]);
                }
                GridView view = gridView5;
                int intFoundRow = view.LocateByValue(0, view.Columns["CostCentreID"], intCostCentreID);
                if (intFoundRow != GridControl.InvalidRowHandle) view.FocusedRowHandle = intFoundRow;
            }
        }

        private void popupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            string strCostCentre;
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strCostCentre = view.GetFocusedRowCellValue("CostCentreCode").ToString() + (string.IsNullOrEmpty(view.GetFocusedRowCellValue("BudgetDesc").ToString()) ? "" :" in " + view.GetFocusedRowCellValue("BudgetDesc").ToString());
                DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
                if (currentRow != null)
                {

                    currentRow["intCostCentreID"] = Convert.ToInt32(view.GetFocusedRowCellValue("CostCentreID"));
                    currentRow["intBudgetID"] = Convert.ToInt32(view.GetFocusedRowCellValue("BudgetID"));

                    //int intOwnershipID = Convert.ToInt32(view.GetFocusedRowCellValue("OwnershipID"));
                    //if (intOwnershipID != 0) currentRow["intOwnershipID"] = intOwnershipID;
                    this.sp01388ATActionEditBindingSource.EndEdit();
                }
            }
            else
            {
                strCostCentre = "";
                 DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
                 if (currentRow != null)
                 {
                     if (string.IsNullOrEmpty(currentRow["intCostCentreID"].ToString()) || Convert.ToInt32(currentRow["intCostCentreID"]) != 0) currentRow["intCostCentreID"] = 0;
                     if (string.IsNullOrEmpty(currentRow["intBudgetID"].ToString()) || Convert.ToInt32(currentRow["intBudgetID"]) != 0) currentRow["intBudgetID"] = 0;
                     this.sp01388ATActionEditBindingSource.EndEdit();
                 }
            }
            return strCostCentre;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadCostCentresGrid();
        }

        private void dateEditDateFrom_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditDateFrom.EditValue = null;
                }
            }
        }

        private void dateEditDateTo_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditDateTo.EditValue = null;
                }
            }
        }

        #endregion


        #region GridView5

        private void gridView5_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Records Available For Selection");
        }

        bool internalRowFocusing;
        private void gridView5_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView5_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        #endregion


        #region Grid - Linked Pictures

        private void gridView39_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Linked Pictures Available");
        }

        private void gridView39_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            //if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView39_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView39_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView39_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView39_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 39;
            SetMenuStatus();
        }

        private void gridView39_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView39_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView39_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridControl39_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl39.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit3_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 39:  // Linked Pictures //
                    {
                        int intID = 0;
                        DataRowView currentRow = (DataRowView)sp01388ATActionEditBindingSource.Current;
                        if (currentRow != null)
                        {
                            intID = (currentRow["intActionID"] == null ? 0 : Convert.ToInt32(currentRow["intActionID"]));
                        }
                        if (intID == 0) return;
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "add";
                        fChildForm.intAddToRecordID = intID;
                        fChildForm.intAddToRecordTypeID = 103;  // 103 = AT Action //
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.strDefaultPath = strDefaultPicturesPath;
                        fChildForm._PassedInFileNamePrefix = "Action";
                        fChildForm.ShowDialog();
                        int intNewPictureID = fChildForm._AddedPolePictureID;
                        if (intNewPictureID != 0)  // At least 1 picture added so refresh picture grid //
                        {
                            i_str_AddedRecordIDs39 = intNewPictureID.ToString() + ";";
                            this.RefreshGridViewState39.SaveViewInfo();
                            Load_Linked_Pictures();
                            SetMenuStatus();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            int[] intRowHandles;
            switch (i_int_FocusedGrid)
            {
                 case 39:     // Picture //
                    {
                        view = (GridView)gridControl39.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Picture Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intPictureID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyPictureID"));
                        int intAddToRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordID"));
                        int intLinkedToRecordTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordTypeID"));
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "edit";
                        fChildForm._PassedInEditImageID = intPictureID;
                        fChildForm.intAddToRecordID = intAddToRecordID;
                        fChildForm.intAddToRecordTypeID = intLinkedToRecordTypeID;
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.strDefaultPath = strDefaultPicturesPath;
                        fChildForm._PassedInFileNamePrefix = "Action";
                        fChildForm.ShowDialog();
                        this.RefreshGridViewState39.SaveViewInfo();
                        Load_Linked_Pictures();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 39:  // Linked Action Pictures //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl39.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Action Picture" : Convert.ToString(intRowHandles.Length) + " Action Pictures") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Action Picture" : "these Action Pictures") + " will no longer be available for selection and any associated images will be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            string strImageName = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strImageName = view.GetRowCellValue(intRowHandle, "PicturePath").ToString();
                                try
                                {
                                    base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                                    GC.GetTotalMemory(true);
                                    System.IO.File.Delete(strImageName);
                                }
                                catch (Exception ex)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete a linked picture [" + ex.Message + "]. The record has not been deleted.\n\nTry deleting again. If the problem persists contact technical support.", "Delete Inspection Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    continue;
                                }
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyPictureID")) + ",";
                            }
                            DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_AT_DataEntryTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState39.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp01387_AT_Action_Manager_Delete("picture", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Linked_Pictures();
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }




    }
}


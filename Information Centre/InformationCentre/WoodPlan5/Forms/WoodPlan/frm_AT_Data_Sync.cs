﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Collections;

using System.Net;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Amenity_Trees_WebService;

namespace WoodPlan5
{
    public partial class frm_AT_Data_Sync : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        string _MachineID = "";
        string _SystemDataTransferMode = "";
        string _LastRetrievalOn = "";
        DateTime _LastRetrievalDateTime = new DateTime();
        DateTime _LastSentDateTime = new DateTime();
        private string strPermissionDocumentPath = "";
        private string strSignaturePath = "";
        private string strMapPath = "";
        private string strPicturePath = "";
        private string strLinkedDocumentsFilePath = "";
        private string strWorkOrderPath = "";
 
        Timer timer;

        #endregion

        public frm_AT_Data_Sync()
        {
            InitializeComponent();
        }

        private void frm_AT_Data_Sync_Load(object sender, EventArgs e)
        {
            splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            this.FormID = 2003;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);

            // Timer used to lock import for 5 minutes to stop crash on subsequent runs //
            timer = new Timer() { Interval = 10000 };  // 10000 = 10 seconds //
            timer.Tick += TimerEventProcessor;

            try
            {
                _MachineID = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "DataSynchronisationMachineID").ToString();
            }
            catch (Exception)
            {
            }
            if (string.IsNullOrEmpty(_MachineID))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Machine ID has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Machine ID is set in System Settings table manually - contact Technical Support for further info.", "Get Machine ID - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }

            try
            {
                _SystemDataTransferMode = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SystemDataTransferMode").ToString().ToLower();
            }
            catch (Exception)
            {
            }
            if (string.IsNullOrEmpty(_SystemDataTransferMode))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The System Data Transfer Mode has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe System Data Transfer Mode is set in System Settings table manually - contact Technical Support for further info.", "Get System Data Transfer Mode - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }      

            try
            {
                strPicturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesPictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strPicturePath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Picture Files path has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Picture Files path is set in the System Configuration Screen - contact Technical Support for further info.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
 
            try
            {
                strLinkedDocumentsFilePath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Document Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strLinkedDocumentsFilePath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Linked Document Files path has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Linked Document Files path is set in the System Configuration Screen - contact Technical Support for further info.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
/*
            try
            {
                strWorkOrderPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedWorkOrderDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Work Order Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strWorkOrderPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order Files path has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Work Order Files path is set in the System Configuration Screen - contact Technical Support for further info.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
*/
            try
            {
                _LastRetrievalOn = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AT_DataSynchronisationLastRetrieved").ToString();
                _LastRetrievalOn = (string.IsNullOrEmpty(_LastRetrievalOn) ? "None" : String.Format("{0:dd/MM/yyyy HH:mm:ss}", _LastRetrievalOn));
                _LastRetrievalDateTime = (_LastRetrievalOn == "None" ? DateTime.MinValue : Convert.ToDateTime(_LastRetrievalOn));
                if (_LastRetrievalDateTime == DateTime.MinValue) _LastRetrievalDateTime = new DateTime(1900, 1, 1);
            }
            catch (Exception)
            {
            }
            _LastSentDateTime = _LastRetrievalDateTime.AddSeconds(10);  // Make sure we don't pick up anything unless it was after last import (10 seconds is a safe margin) //

            labelLastImportDate.Text = "        Last Successfull Import: " + _LastRetrievalOn;
            
            sp02102_AT_WebService_Tablet_Get_Export_OverviewTableAdapter.Connection.ConnectionString = strConnectionString;
            sp02102_AT_WebService_Tablet_Get_Export_OverviewTableAdapter.Fill(dataSet_AT_DataTransfer.sp02102_AT_WebService_Tablet_Get_Export_Overview, _LastSentDateTime);
            gridControl1.ForceInitialize();

            // REMEMBER TO UNCOMMENT OUT THE FOLLOWING //
            if (this.GlobalSettings.SystemDataTransferMode.ToLower() == "desktop")
            {
                splashScreenManager.CloseWaitForm(); 
                DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process can only be run when the software is in Tablet Mode. The software on this computer is running in Server Mode - unable to proceed with Data Synchronisation.", "Check Software Mode - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }

            SetExportImportStatus(); 
            PostOpen();
        }

        private void PostOpen()
        {
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            Application.DoEvents();  // Allow Form time to repaint itself //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        private void frm_AT_Data_Sync_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Data to Export.", "Export Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                CleanUp();
                return;
            }

            if (!GetInternetConnectionStatus()) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - please try later or move to an area with a stronger signal.", "Check Internet Connection - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            ShowProgressBar(progressBarControlExport, view.DataRowCount);

            string strTableName = "";
            string strStoredProcNameGet = "";
            string strStoredProcNameSet = "";
            string strReturn = "";
            SqlConnection SQlConn;

            var UpdateMobileDeviceChangeLog = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            UpdateMobileDeviceChangeLog.ChangeConnectionString(strConnectionString);


            // Get Version Number for this database //
            int intCurrentVersionNumber = 0;
            try
            {
                intCurrentVersionNumber = Convert.ToInt32(UpdateMobileDeviceChangeLog.sp02100_AT_Get_Data_Transfer_Version_Number());
            }
            catch (Exception ex) { }
            if (intCurrentVersionNumber <= 0)
            {
                CleanUp();
                DevExpress.XtraEditors.XtraMessageBox.Show("Error - Unable to retrieve database version number.\n\nYou may need to install an update for this application - contact Technical Support for further info.", "Get Version Number - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
            Amenity_TreesSoapClient ws2 = new Amenity_TreesSoapClient();
            try  // Call Web Service and send data to export //
            {
                ws2.InnerChannel.OperationTimeout = new TimeSpan(0, 10, 0);  // Set Timeout to 10 minutes //

                SOAPHeaderContent header2 = new SOAPHeaderContent() { Password = "***Ch4r10tte***", UserName = "MarkBissett" };

                // Check version number against central version number //
                int intCentralVersionNumber = ws2.GetVersionNumber(header2);  // Call Web Service to sync file //
                if (intCentralVersionNumber != intCurrentVersionNumber)
                {
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The version of this database does not match the version of the central server.\n\nYou will need to install an update to this application - contact Technical Support.", "Export Data - Failed on Version Check", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                CleanUp();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to get the version number from the central server.\n\nError: " + ex.Message + "\n\nThis screen will now close. Try exporting again.", "Export Data - Failed on Version Check", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }


            DateTime dtCurrentStartTime = DateTime.Now;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                strTableName = view.GetRowCellValue(i, "RecordType").ToString();
                strStoredProcNameGet = view.GetRowCellValue(i, "StoredProcGet").ToString();
                strStoredProcNameSet = view.GetRowCellValue(i, "StoredProcSet").ToString();
                strReturn = "";  // Added 16/04/2014 //

                DataSet ds = new DataSet("NewDataSet");
                SqlDataAdapter sda = new SqlDataAdapter();
                SQlConn = new SqlConnection(strConnectionString);
                try  // Get Data To Export //
                {
                    SQlConn.Open();
                    SqlCommand cmd = new SqlCommand(strStoredProcNameGet, SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DeviceID", _MachineID));
                    cmd.Parameters.Add(new SqlParameter("@TableName", strTableName));
                    ds.Clear();
                    sda = new SqlDataAdapter(cmd);
                    sda.Fill(ds, "Table");
                }
                catch (Exception ex)
                {
                    SQlConn.Close();
                    SQlConn.Dispose();
                    sda.Dispose();
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show(String.Format("The Synchronisation Process failed trying to load {0} export data.\n\nError: {1}\n\nThis screen will now close. Try exporting again.", strTableName, ex.Message), "Export Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
                Amenity_TreesSoapClient ws = new Amenity_TreesSoapClient();
                try  // Call Web Service and send data to export //
                {
                    ws.InnerChannel.OperationTimeout = new TimeSpan(0, 10, 0);  // Set Timeout to 10 minutes //

                    SOAPHeaderContent header = new SOAPHeaderContent() { Password = "***Ch4r10tte***", UserName = "MarkBissett" };
                    if (strTableName == "UT_Survey_Picture" || strTableName == "Linked_Document")
                    {
                        string strColumnName = "";
                        string strDefaultPath = "";
                        string strDocumentTypePath = "";
                        int intLinkedToRecordTypeID = 0;
                        string strTempPath = "";
                        switch (strTableName)
                        {
                            case "UT_Survey_Picture":
                                strColumnName = "PicturePath";
                                strDefaultPath = strPicturePath;
                                break;
                            case "Linked_Document":
                                strColumnName = "DocumentPath";
                                strDefaultPath = strLinkedDocumentsFilePath;
                                break;
                            default:
                                break;
                        }
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string strFileName = dr[strColumnName].ToString();
                            if (string.IsNullOrEmpty(strFileName)) continue;
                            strDocumentTypePath = "";
                            intLinkedToRecordTypeID = 0;

                            if (strTableName == "Linked_Document")
                            {
                                intLinkedToRecordTypeID = Convert.ToInt32(dr["LinkedToRecordTypeID"]);
                                
                                // Get Linked Document Path based on Type of Linked Document //
                                SqlCommand cmd = new SqlCommand("sp02200_AT_WebService_LinkedDocument_Save_Path_For_Type", SQlConn) { CommandType = CommandType.StoredProcedure };
                                cmd.Parameters.Add(new SqlParameter("@LinkedToRecordTypeID", intLinkedToRecordTypeID));
                                try
                                {
                                    strDocumentTypePath = cmd.ExecuteScalar().ToString();
                                }
                                catch (Exception ex)
                                {
                                    SQlConn.Close();
                                    SQlConn.Dispose();
                                    ws.Close();
                                    ws = null;
                                    CleanUp();
                                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to export " + strTableName + " data.\n\nUnable To get linked Document Type Path. Try importing again.", "Export Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    GC.GetTotalMemory(true);
                                    GC.Collect();
                                    this.Close();
                                    return;
                                }
                                if (string.IsNullOrEmpty(strDocumentTypePath))
                                {
                                    SQlConn.Close();
                                    SQlConn.Dispose();
                                    ws.Close();
                                    ws = null;
                                    CleanUp();
                                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to export " + strTableName + " data.\n\nUnable To get linked Document Type Path.\n\n. Try importing again.", "Export Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    GC.GetTotalMemory(true);
                                    GC.Collect();
                                    this.Close();
                                    return;
                                }
                            }

                            if (!string.IsNullOrEmpty(strDocumentTypePath))
                            {
                                strTempPath = strDocumentTypePath;  // For Linked Docs, switch out the path if Linked Document Type specific path is available //
                            }
                            else
                            {
                                strTempPath = strDefaultPath;
                            }

                            if (!File.Exists((Path.Combine(strTempPath, strFileName)))) continue;
                            FileInfo fInfo = new FileInfo(Path.Combine(strTempPath, strFileName));  // Get the file information form the selected file //

                            // Get the length of the file to see if it is possible to upload it (with the standard 4 MB limit) //
                            long numBytes = fInfo.Length;
                            double dLen = Convert.ToDouble(fInfo.Length / 1000000);

                            // Default limit of 4 MB on web server. Have to change the web.config to allow larger uploads //
                            if (dLen < 4)
                            {
                                // Set up a file stream and binary reader for the selected file //                          
                                FileStream fStream = new FileStream(Path.Combine(strTempPath, strFileName), FileMode.Open, FileAccess.Read);
                                BinaryReader br = new BinaryReader(fStream);
                                byte[] data = br.ReadBytes((int)numBytes);  // Convert the file to a byte array //
                                br.Close();
                                br.Dispose();

                                // pass the byte array (file) and file name to the web service
                                strReturn = ws.FileSet(header, data, strFileName, strTableName, intLinkedToRecordTypeID);  // Call Web Service to sync file //
                                fStream.Close();
                                fStream.Dispose();
                            }

                        }
                    }
                    strReturn = ws.SetData(header, _MachineID, strTableName, strStoredProcNameSet, ds);  // Call Web Service to sync data //
                    if (!strReturn.ToLower().StartsWith("success"))  // Web Service was unsuccessfull //
                    {
                        SQlConn.Close();
                        SQlConn.Dispose();
                        sda.Dispose();
                        CleanUp();
                        DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to export " + strTableName + " data.\n\nError: " + strReturn + "\n\nThis screen will now close. Try exporting again. If the problem persists contact Technical Support.", "Export Data - Failed on Clear Change Log", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.Close();
                        return;
                    }
                }
                catch (Exception ex)
                {
                    ws.Close();
                    ws = null;
                    SQlConn.Close();
                    SQlConn.Dispose();
                    sda.Dispose();
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to export " + strTableName + " data.\n\nError: " + ex.Message + "\n\nThe Web Service may not be running.\n\nThis screen will now close. Try exporting again.", "Export Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
                ws.Close();
                ws = null;
                if (!strReturn.ToLower().StartsWith("success"))  // Web Service was unsuccessfull //
                {
                    if (string.IsNullOrEmpty(strReturn))
                    {
                        strReturn = "A problem occurred - the web service may not be running.";
                    }
                    SQlConn.Close();
                    SQlConn.Dispose();
                    sda.Dispose();
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to export " + strTableName + " data.\n\nError: " + strReturn + "\n\nThis screen will now close. Try exporting again. If the problem persists contact Technical Support.", "Export Data - Failed on Clear Change Log", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
                else  // Web service was successfull so remove records just exported from MobileDeviceChangeLog //
                {
                    try
                    {
                        strReturn = UpdateMobileDeviceChangeLog.sp02101_AT_WebService_Tablet_Remove_Exported_From_Change_Log(strTableName, dtCurrentStartTime).ToString();
                    }
                    catch (Exception ex)
                    {
                        SQlConn.Close();
                        SQlConn.Dispose();
                        sda.Dispose();
                        CleanUp();
                        DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to remove the exported " + strTableName + " data from the Change Log.\n\nError: " + ex.Message + "\n\nThis screen will now close. Try exporting again.", "Export Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.Close();
                        return;
                    }
                }
                SQlConn.Close();
                SQlConn.Dispose();
                sda.Dispose();

                progressBarControlExport.PerformStep();
                progressBarControlExport.Update();
                Application.DoEvents();   // Allow Form time to repaint itself //
            }
            _LastSentDateTime = DateTime.Now;
            CleanUp();
            DevExpress.XtraEditors.XtraMessageBox.Show("The Export Synchronisation Process completed Successfully.", "Export Data - Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (!GetInternetConnectionStatus()) // Alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - please try later or move to an area with a stronger signal.", "Check Internet Connection - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get Version Number for this database //
            int intCurrentVersionNumber = 0;
            try
            {
               var UpdateMobileDeviceChangeLog = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
                UpdateMobileDeviceChangeLog.ChangeConnectionString(strConnectionString);
                intCurrentVersionNumber = Convert.ToInt32(UpdateMobileDeviceChangeLog.sp02100_AT_Get_Data_Transfer_Version_Number());
            }
            catch (Exception ex)
            {
            }
            if (intCurrentVersionNumber <= 0)
            {
                CleanUp();
                DevExpress.XtraEditors.XtraMessageBox.Show("Error - Unable to retrieve database version number.\n\nYou may need to install an update for this application - contact Technical Support for further info.", "Get Version Number - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
            Amenity_TreesSoapClient ws2 = new Amenity_TreesSoapClient();
            try  // Call Web Service and send data to export //
            {
                ws2.InnerChannel.OperationTimeout = new TimeSpan(0, 10, 0);  // Set Timeout to 10 minutes //

                SOAPHeaderContent header2 = new SOAPHeaderContent() { Password = "***Ch4r10tte***", UserName = "MarkBissett" };

                // Check version number against central version number //
                int intCentralVersionNumber = ws2.GetVersionNumber(header2);  // Call Web Service to sync file //
                if (intCentralVersionNumber != intCurrentVersionNumber)
                {
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The version of this database does not match the version of the central server.\n\nYou will need to install an update to this application - contact Technical Support.", "Export Data - Failed on Version Check", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                CleanUp();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to get the version number from the central server.\n\nError: " + ex.Message + "\n\nThis screen will now close. Try exporting again.", "Export Data - Failed on Version Check", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }

             
            ArrayList arrayListTableNames = new ArrayList();
            arrayListTableNames.Add("tblStaff");
            arrayListTableNames.Add("tblContractor");
            arrayListTableNames.Add("tblPickListHeader");
            arrayListTableNames.Add("tblPickListItem");
            arrayListTableNames.Add("tblSpecies");
            arrayListTableNames.Add("Species_Variety");
            arrayListTableNames.Add("tblSequence");
            arrayListTableNames.Add("tblBudget");
            arrayListTableNames.Add("tblCompanyHeader");
            arrayListTableNames.Add("tblJobMaster");
            arrayListTableNames.Add("tblJobRate");
            arrayListTableNames.Add("tblJobRateDiscount");
            arrayListTableNames.Add("tblJobRateParameter");
            arrayListTableNames.Add("tblOwnership");
            arrayListTableNames.Add("tblCostCentre");
            arrayListTableNames.Add("tblLegalText");
            arrayListTableNames.Add("tblHSEMaster");
            arrayListTableNames.Add("tblMapAddress");
            arrayListTableNames.Add("EP_Client");
            arrayListTableNames.Add("EP_Site");
            arrayListTableNames.Add("tblTree");
            arrayListTableNames.Add("tblIncident");
            arrayListTableNames.Add("tblInspection");
            arrayListTableNames.Add("tblWorkOrder");
            arrayListTableNames.Add("tblAction");
            arrayListTableNames.Add("UT_Survey_Picture");
            arrayListTableNames.Add("Linked_Document");

            ArrayList arrayListTableFriendlyNames = new ArrayList();
            arrayListTableFriendlyNames.Add("Staff");
            arrayListTableFriendlyNames.Add("Contractor");
            arrayListTableFriendlyNames.Add("Pick List Header");
            arrayListTableFriendlyNames.Add("Pick List Item");
            arrayListTableFriendlyNames.Add("Species");
            arrayListTableFriendlyNames.Add("Species Variety");
            arrayListTableFriendlyNames.Add("Sequences");
            arrayListTableFriendlyNames.Add("Budget");
            arrayListTableFriendlyNames.Add("Company Header");
            arrayListTableFriendlyNames.Add("Job Master");
            arrayListTableFriendlyNames.Add("Job Rate");
            arrayListTableFriendlyNames.Add("Job Rate Discount");
            arrayListTableFriendlyNames.Add("Job Rate Parameter");
            arrayListTableFriendlyNames.Add("Ownership");
            arrayListTableFriendlyNames.Add("Cost Centre");
            arrayListTableFriendlyNames.Add("Legal Text");
            arrayListTableFriendlyNames.Add("HSE Master");
            arrayListTableFriendlyNames.Add("Map Address");
            arrayListTableFriendlyNames.Add("Client");
            arrayListTableFriendlyNames.Add("Site");
            arrayListTableFriendlyNames.Add("Tree");
            arrayListTableFriendlyNames.Add("Incident");
            arrayListTableFriendlyNames.Add("Inspection");
            arrayListTableFriendlyNames.Add("Work Order");
            arrayListTableFriendlyNames.Add("Action");
            arrayListTableFriendlyNames.Add("Picture");
            arrayListTableFriendlyNames.Add("Linked Document");
      
            ArrayList arrayListStoredProcNamesGet = new ArrayList();
            arrayListStoredProcNamesGet.Add("sp02103_AT_WebService_Staff_Get");
            arrayListStoredProcNamesGet.Add("sp02105_AT_WebService_Contractor_Get");
            arrayListStoredProcNamesGet.Add("sp02107_AT_WebService_PickListHeader_Get");
            arrayListStoredProcNamesGet.Add("sp02109_AT_WebService_PickListItem_Get");
            arrayListStoredProcNamesGet.Add("sp02111_AT_WebService_Species_Get");
            arrayListStoredProcNamesGet.Add("sp02113_AT_WebService_Species_Variety_Get");
            arrayListStoredProcNamesGet.Add("sp02115_AT_WebService_Sequence_Get");
            arrayListStoredProcNamesGet.Add("sp02117_AT_WebService_Budget_Get");
            arrayListStoredProcNamesGet.Add("sp02119_AT_WebService_CompanyHeader_Get");
            arrayListStoredProcNamesGet.Add("sp02121_AT_WebService_JobMaster_Get");
            arrayListStoredProcNamesGet.Add("sp02123_AT_WebService_JobRate_Get");
            arrayListStoredProcNamesGet.Add("sp02125_AT_WebService_JobRateDiscount_Get");
            arrayListStoredProcNamesGet.Add("sp02127_AT_WebService_JobRateParameter_Get");
            arrayListStoredProcNamesGet.Add("sp02129_AT_WebService_Ownership_Get");
            arrayListStoredProcNamesGet.Add("sp02131_AT_WebService_CostCentre_Get");
            arrayListStoredProcNamesGet.Add("sp02133_AT_WebService_LegalText_Get");
            arrayListStoredProcNamesGet.Add("sp02135_AT_WebService_HSEMaster_Get");
            arrayListStoredProcNamesGet.Add("sp02137_AT_WebService_MapAddress_Get");
            arrayListStoredProcNamesGet.Add("sp02139_AT_WebService_Client_Get");
            arrayListStoredProcNamesGet.Add("sp02141_AT_WebService_Site_Get");
            arrayListStoredProcNamesGet.Add("sp02143_AT_WebService_Tree_Get");
            arrayListStoredProcNamesGet.Add("sp02145_AT_WebService_Incident_Get");
            arrayListStoredProcNamesGet.Add("sp02147_AT_WebService_Inspection_Get");
            arrayListStoredProcNamesGet.Add("sp02149_AT_WebService_WorkOrder_Get");
            arrayListStoredProcNamesGet.Add("sp02151_AT_WebService_Action_Get");
            arrayListStoredProcNamesGet.Add("sp02153_AT_WebService_SurveyPicture_Get");
            arrayListStoredProcNamesGet.Add("sp02155_AT_WebService_LinkedDocument_Get");

            ArrayList arrayListStoredProcNamesSet = new ArrayList();
            arrayListStoredProcNamesSet.Add("sp02104_AT_WebService_Staff_Set");
            arrayListStoredProcNamesSet.Add("sp02106_AT_WebService_Contractor_Set");
            arrayListStoredProcNamesSet.Add("sp02108_AT_WebService_PickListHeader_Set");
            arrayListStoredProcNamesSet.Add("sp02110_AT_WebService_PickListItem_Set");
            arrayListStoredProcNamesSet.Add("sp02112_AT_WebService_Species_Set");
            arrayListStoredProcNamesSet.Add("sp02114_AT_WebService_Species_Variety_Set");
            arrayListStoredProcNamesSet.Add("sp02116_AT_WebService_Sequence_Set");
            arrayListStoredProcNamesSet.Add("sp02118_AT_WebService_Budget_Set");
            arrayListStoredProcNamesSet.Add("sp02120_AT_WebService_CompanyHeader_Set");
            arrayListStoredProcNamesSet.Add("sp02122_AT_WebService_JobMaster_Set");
            arrayListStoredProcNamesSet.Add("sp02124_AT_WebService_JobRate_Set");
            arrayListStoredProcNamesSet.Add("sp02126_AT_WebService_JobRateDiscount_Set");
            arrayListStoredProcNamesSet.Add("sp02128_AT_WebService_JobRateParameter_Set");
            arrayListStoredProcNamesSet.Add("sp02130_AT_WebService_Ownership_Set");
            arrayListStoredProcNamesSet.Add("sp02132_AT_WebService_CostCentre_Set");
            arrayListStoredProcNamesSet.Add("sp02134_AT_WebService_LegalText_Set");
            arrayListStoredProcNamesSet.Add("sp02136_AT_WebService_HSEMaster_Set");
            arrayListStoredProcNamesSet.Add("sp02138_AT_WebService_MapAddress_Set");
            arrayListStoredProcNamesSet.Add("sp02140_AT_WebService_Client_Set");
            arrayListStoredProcNamesSet.Add("sp02142_AT_WebService_Site_Set");
            arrayListStoredProcNamesSet.Add("sp02144_AT_WebService_Tree_Set");
            arrayListStoredProcNamesSet.Add("sp02146_AT_WebService_Incident_Set");
            arrayListStoredProcNamesSet.Add("sp02148_AT_WebService_Inspection_Set");
            arrayListStoredProcNamesSet.Add("sp02150_AT_WebService_WorkOrder_Set");
            arrayListStoredProcNamesSet.Add("sp02152_AT_WebService_Action_Set");
            arrayListStoredProcNamesSet.Add("sp02154_AT_WebService_SurveyPicture_Set");
            arrayListStoredProcNamesSet.Add("sp02156_AT_WebService_LinkedDocument_Set");

            ShowProgressBar(progressBarControlImport, arrayListTableNames.Count);

            DataSet dsTemp = new DataSet();
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SQlConn.Open();

            Amenity_TreesSoapClient ws = new Amenity_TreesSoapClient();
            ws.InnerChannel.OperationTimeout = new TimeSpan(0, 10, 0);  // Set Timeout to 10 minutes //          

            SOAPHeaderContent header = new SOAPHeaderContent() { Password = "***Ch4r10tte***", UserName = "MarkBissett" };

            int intCounter = -1;  // Start on -1 as first iteration of loop will move it to 0. Critical that intCounter increments at top of loop as there are various exist point in loop //
            int intStoredProcReturnValue = 0;
            foreach (string TableName in arrayListTableNames)
            {
                intCounter++;
                progressBarControlImport.PerformStep();
                progressBarControlImport.Update();
                Application.DoEvents();   // Allow Form time to repaint itself //

                string strFriendlyTableName = arrayListTableFriendlyNames[intCounter].ToString();
                string strStoredProcGet = arrayListStoredProcNamesGet[intCounter].ToString();
                string strStoredProcSet = arrayListStoredProcNamesSet[intCounter].ToString();
                try
                {
                    dsTemp = ws.GetData(header, _MachineID, TableName, strStoredProcGet);
                }
                catch (Exception ex)
                {
                    SQlConn.Close();
                    SQlConn.Dispose();
                    ws.Close();
                    ws = null;
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to import " + strFriendlyTableName + " data.\n\nError: " + ex.Message + ".\n\nThis screen will now close. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    GC.GetTotalMemory(true);
                    GC.Collect();
                    this.Close();
                    return;
                }
                if (dsTemp == null)
                {
                    SQlConn.Close();
                    SQlConn.Dispose();
                    ws.Close();
                    ws = null;
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to import " + strFriendlyTableName + " data.\n\nThe connection may have been lost.\n\nThis screen will now close. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    GC.GetTotalMemory(true);
                    GC.Collect();
                    this.Close();
                    return;
                }
                if (dsTemp.Tables.Count == 0) continue;   // Nothing to process //
                if (dsTemp.Tables[0].Rows.Count == 1)
                {
                    if (dsTemp.Tables[0].Columns.Count == 1)
                    {
                        string strError = dsTemp.Tables[0].Columns["Error"].ToString();
                        if (string.IsNullOrEmpty(strError) || strError == "Error") strError = "Unknown Error";
                        SQlConn.Close();
                        SQlConn.Dispose();
                        ws.Close();
                        ws = null;
                        CleanUp();
                        DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to import " + strFriendlyTableName + " data.\n\nError: " + strError + ". The Web Service may not be running.\n\nThis screen will now close. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.Close();
                        return;
                    }
                }
                if (dsTemp.Tables[0].Rows.Count == 0) continue;   // Nothing to process //
 
                // Error checks passed so process data - Check if there are images to import //
                if (TableName == "UT_Survey_Picture" || TableName == "Linked_Document")
                {
                    string strColumnName = "";
                    string strDefaultPath = "";
                    string strDocumentTypePath = "";
                    int intLinkedToRecordTypeID = 0;
                    string strTempPath = "";
                    switch (TableName)
                    {
                        case "UT_Survey_Picture":
                            strColumnName = "PicturePath";
                            strDefaultPath = strPicturePath;
                            break;
                        case "Linked_Document":
                            strColumnName = "DocumentPath";
                            strDefaultPath = strLinkedDocumentsFilePath;
                            break;
                        default:
                            break;
                    }
                    foreach (DataRow dr in dsTemp.Tables[0].Rows)
                    {
                        byte[] byteData = null;
                        string fileName = dr[strColumnName].ToString();
                        strDocumentTypePath = "";
                        intLinkedToRecordTypeID = 0;

                        if (TableName == "Linked_Document")
                        {
                            intLinkedToRecordTypeID = Convert.ToInt32(dr["LinkedToRecordTypeID"]);

                            // Get Linked Document Path based on Type of Linked Document //
                            SqlCommand cmd2 = new SqlCommand("sp02200_AT_WebService_LinkedDocument_Save_Path_For_Type", SQlConn) { CommandType = CommandType.StoredProcedure };
                            cmd2.Parameters.Add(new SqlParameter("@LinkedToRecordTypeID", intLinkedToRecordTypeID));
                            try
                            {
                                strDocumentTypePath = cmd2.ExecuteScalar().ToString();
                            }
                            catch (Exception ex)
                            {
                                SQlConn.Close();
                                SQlConn.Dispose();
                                ws.Close();
                                ws = null;
                                CleanUp();
                                DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to import " + strFriendlyTableName + " data.\n\nUnable To get linked Document Type Path. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                GC.GetTotalMemory(true);
                                GC.Collect();
                                this.Close();
                                return;
                            }
                            if (string.IsNullOrEmpty(strDocumentTypePath))
                            {
                                SQlConn.Close();
                                SQlConn.Dispose();
                                ws.Close();
                                ws = null;
                                CleanUp();
                                DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to import " + strFriendlyTableName + " data.\n\nUnable To get linked Document Type Path.\n\n. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                GC.GetTotalMemory(true);
                                GC.Collect();
                                this.Close();
                                return;
                            }

                        }
                        if (string.IsNullOrEmpty(fileName)) continue;
                        //if (TableName == "UT_Linked_Map") fileName += ".gif";  // Linked Maps don't have a file extension, so add it.
                        try
                        {
                            byteData = ws.FileGet(header, fileName, TableName, intLinkedToRecordTypeID);  // Get document as a stream from web service //
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                        if (byteData == null) continue;

                        if (!string.IsNullOrEmpty(strDocumentTypePath))
                        {
                            strTempPath = strDocumentTypePath;  // For Linked Docs, switch out the path if Linked Document Type specific path is available //
                        }
                        else
                        {
                            strTempPath = strDefaultPath;
                        }

                        MemoryStream ms;
                        try
                        {
                            // instance a memory stream and pass the byte array to its constructor //
                            ms = new MemoryStream(byteData);
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                        if (File.Exists(Path.Combine(strTempPath, fileName)))  // Attempt to delete old version and re-create.//
                        {
                            try
                            {
                                File.Delete(Path.Combine(strTempPath, fileName));
                            }
                            catch (Exception ex)
                            {
                                continue; // We can safely bomb out as the old version wasn't deleted so the new version doesn't really need to be created. //
                            }
                            try
                            {
                                FileStream fs = new FileStream(Path.Combine(strTempPath, fileName), FileMode.Create);  // instance a filestream pointing to the storage folder, use the original file name to name the resulting file //
                                ms.WriteTo(fs);  // write the memory stream containing the original file as a byte array to the filestream //
                                ms.Close();
                                ms.Dispose();
                                fs.Close();
                                fs.Dispose();
                            }
                            catch (Exception ex)
                            {
                                continue;
                            }
                        }
                        else  // File doesn't exist so create it //
                        {
                            try
                            {
                                FileStream fs = new FileStream(Path.Combine(strTempPath, fileName), FileMode.Create);  // instance a filestream pointing to the storage folder, use the original file name to name the resulting file //
                                ms.WriteTo(fs);  // write the memory stream containing the original file as a byte array to the filestream //
                                ms.Close();
                                ms.Dispose();
                                fs.Close();
                                fs.Dispose();
                            }
                            catch (Exception ex)
                            {
                                continue;
                            }
                        }
                    }
                }

                // Now Import Data //
                SqlCommand cmd = new SqlCommand(strStoredProcSet, SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                // Add paramaters to Stored Proc //
                foreach (DataColumn c in dsTemp.Tables[0].Columns)
                {
                    cmd.Parameters.Add(new SqlParameter("@" + c.ColumnName.Trim(), BaseObjects.ExtensionFunctions.ToSqlDbType(c.DataType), c.MaxLength));
                }
                cmd.Parameters.Add(new SqlParameter("@SystemDataTransferMode", _SystemDataTransferMode));  // Extra parameter to control if table trigger is fired on updating data in SP //

                // Merge Rows into Database... //
                foreach (DataRow r in dsTemp.Tables[0].Rows)
                {
                    foreach (DataColumn c in dsTemp.Tables[0].Columns)
                    {
                        cmd.Parameters["@" + c.ColumnName.Trim()].Value = r[c.ColumnName.Trim()];  // Populate parameters //
                    }
                    try
                    {
                        intStoredProcReturnValue = Convert.ToInt32(cmd.ExecuteScalar());  // Fire Stored Proc to merge data //
                    }
                    catch (Exception ex)
                    {
                        SQlConn.Close();
                        SQlConn.Dispose();
                        CleanUp();
                        DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to import " + arrayListTableFriendlyNames[intCounter] + " data.\n\nError: [" + ex.Message + "]!\n\nThis screen will now close. Try importing again. If the problem persists contact Technical Support.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.Close();
                        return;
                    }
                }

                // Call Web Service to update last successfull retrieval of this type of data on the server database //
                string strReturn = "";
                try
                {
                    strReturn = ws.SetLastRetrieved(header, _MachineID, TableName);
                }
                catch (Exception ex)
                {
                    SQlConn.Close();
                    SQlConn.Dispose();
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to update the Last Retrieved Date for " + strFriendlyTableName + " data on the Server.\n\nThe connection may have been lost..\n\nError: " + ex.Message + ".\n\nThis screen will now close. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
                if (!strReturn.ToLower().StartsWith("success"))
                {
                    SQlConn.Close();
                    SQlConn.Dispose();
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to update the Last Retrieved Date for " + strFriendlyTableName + " data on the Server.\n\nError: " + strReturn + ".\n\nThis screen will now close. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
            }
            SQlConn.Close();
            SQlConn.Dispose();
            ws.Close();
            ws = null;

            using (WoodPlanDataSetTableAdapters.QueriesTableAdapter SetSetting = new WoodPlanDataSetTableAdapters.QueriesTableAdapter())
            {
                SetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    // Update Last Imported date in System Settings table //
                    _LastRetrievalDateTime = DateTime.Now;
                    _LastRetrievalOn = _LastRetrievalDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                    SetSetting.sp00048_Update_System_Setting(1, "AT_DataSynchronisationLastRetrieved", _LastRetrievalOn);
                    labelLastImportDate.Text = "        Last Successfull Import: " + _LastRetrievalOn;
                    _LastSentDateTime = _LastRetrievalDateTime.AddSeconds(10);  // Make sure we don't pick up anything unless it was after last import (10 seconds is a safe margin) //

                }
                catch (Exception)
                {
                }
            }

            CleanUp();
            DevExpress.XtraEditors.XtraMessageBox.Show("The Import Synchronisation Process completed Successfully.", "Import Data - Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }


        private void ShowProgressBar(DevExpress.XtraEditors.ProgressBarControl pb, int intProgressCount)
        {
            pb.Visible = true;
            pb.Properties.Step = 1;
            pb.Properties.PercentView = true;
            pb.Properties.Maximum = intProgressCount;
            pb.Properties.Minimum = 0;
            Application.DoEvents();
        }

        private void CleanUp()
        {
            progressBarControlExport.Visible = false;
            progressBarControlExport.Position = 0;
            btnExport.Enabled = true;

            progressBarControlImport.Visible = false;
            progressBarControlImport.Position = 0;

            // Reload Export objects grid to reflect what has been exported //
            sp02102_AT_WebService_Tablet_Get_Export_OverviewTableAdapter.Fill(dataSet_AT_DataTransfer.sp02102_AT_WebService_Tablet_Get_Export_Overview, _LastSentDateTime);
            Application.DoEvents();
            SetExportImportStatus();
        }

        private void SetExportImportStatus()
        {
            // Check if 5 minutes has passed since last import - if not, prevent a re-import until it expires //
            TimeSpan span = DateTime.Now.Subtract(_LastRetrievalDateTime);
            if (span.TotalMinutes < (double)5)
            {
                if (!timer.Enabled)
                {
                    btnExport.Enabled = false;
                    btnImport.Enabled = false;
                    TimeoutLabel.Text = "        You cannot Import again until: " + String.Format("{0:dd/MM/yyyy HH:mm:ss}", _LastRetrievalDateTime.AddMinutes(5));
                    TimeoutLabel.Visible = true;
                    timer.Start();
                }
            }
            else
            {
                timer.Stop();
                TimeoutLabel.Visible = false;

                GridView view = (GridView)gridControl1.MainView;
                if (view.DataRowCount > 0)
                {
                    btnExport.Enabled = true;
                    btnImport.Enabled = false;
                }
                else
                {
                    btnExport.Enabled = false;
                    btnImport.Enabled = true;
                }
            }
        }

        private bool GetInternetConnectionStatus()
        {
            // Check for internet Connectivity //
            bool boolInternet = BaseObjects.PingTest.Ping("www.google.com");
            if (!boolInternet) boolInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
            if (!boolInternet) boolInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
            return boolInternet;
        }

        public bool PingTest(string strSite)
        {
            System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
            try
            {
                System.Net.NetworkInformation.PingReply pingStatus = ping.Send(strSite, 1000);
                return pingStatus.Status == System.Net.NetworkInformation.IPStatus.Success;
            }
            catch (Exception){ }
            return false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TimerEventProcessor(object sender, EventArgs e)
        {
            SetExportImportStatus();
        }

        private void frm_AT_Data_Sync_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer.Stop();
        }



    }
}

namespace WoodPlan5
{
    partial class frm_AT_Tree_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Tree_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions13 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions14 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject53 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject54 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject55 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject56 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions15 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject57 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject58 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject59 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject60 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions16 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject61 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject62 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject63 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject64 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions17 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject65 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject66 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject67 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject68 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions18 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject69 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject70 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject71 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject72 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions19 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject73 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject74 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject75 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject76 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions20 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject77 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject78 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject79 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject80 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions21 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject81 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject82 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject83 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject84 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions22 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject85 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject86 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject87 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject88 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition7 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions23 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject89 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject90 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject91 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject92 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions24 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject93 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject94 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject95 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject96 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition8 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions25 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject97 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject98 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject99 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject100 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions26 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject101 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject102 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject103 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject104 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition9 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions27 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject105 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject106 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject107 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject108 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions28 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject109 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject110 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject111 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject112 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition10 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions29 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject113 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject114 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject115 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject116 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions30 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject117 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject118 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject119 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject120 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition11 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions31 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject121 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject122 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject123 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject124 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions32 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject125 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject126 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject127 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject128 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition12 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions33 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject129 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject130 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject131 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject132 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions34 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject133 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject134 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject135 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject136 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition13 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions35 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject137 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject138 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject139 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject140 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions36 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject141 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject142 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject143 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject144 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition14 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions37 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject145 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject146 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject147 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject148 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions38 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject149 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject150 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject151 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject152 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition15 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions39 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject153 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject154 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject155 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject156 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions40 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject157 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject158 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject159 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject160 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition16 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions41 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject161 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject162 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject163 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject164 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions42 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject165 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject166 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject167 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject168 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition17 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions43 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject169 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject170 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject171 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject172 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions44 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject173 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject174 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject175 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject176 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition18 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions45 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject177 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject178 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject179 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject180 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions46 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject181 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject182 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject183 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject184 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition19 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions47 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject185 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject186 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject187 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject188 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions48 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject189 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject190 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject191 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject192 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition20 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions49 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject193 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject194 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject195 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject196 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions50 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject197 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject198 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject199 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject200 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition21 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions51 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject201 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject202 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject203 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject204 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions52 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject205 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject206 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject207 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject208 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition22 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions53 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject209 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject210 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject211 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject212 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions54 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject213 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject214 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject215 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject216 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition23 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions55 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject217 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject218 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject219 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject220 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions56 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject221 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject222 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject223 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject224 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition24 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions57 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject225 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject226 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject227 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject228 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions58 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject229 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject230 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject231 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject232 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition25 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions59 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject233 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject234 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject235 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject236 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions60 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject237 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject238 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject239 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject240 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colItemID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn94 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn112 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn118 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn124 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn130 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiRememberDetails = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReload = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopyAllValues = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopySelectedValues = new DevExpress.XtraBars.BarButtonItem();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bbiShowMapButton = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SiteCodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp01204ATEditTreeDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.SiteIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientEditTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.gridControl39 = new DevExpress.XtraGrid.GridControl();
            this.sp02064ATTreePicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView39 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn169 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn170 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn171 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn172 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn173 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn174 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn175 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn176 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn177 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn178 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn179 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShortLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LocationPolyXYMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.LocationYSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LocationXSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CavatSLEFactorGridLookUpEdit2 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01444ATCAVATListSLEFactor2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView38 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn165 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn166 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn167 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn168 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CavatFunctionalValueTextEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.CavatFunctionalValueFactorGridLookUpEdit2 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01443ATCAVATListFunctionalAdjustmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView37 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn161 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn162 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn163 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn164 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CavatCTIValueTextEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.CavatCTIFactorGridLookUpEdit2 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01442ATCAVATListCTIRatingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView36 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn157 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn158 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn159 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn160 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CavatUnitValueFactorSpinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.CavatStemDiameterSpinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.CavatSLEFactorGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01441ATCAVATListSLEFactors2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView35 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn141 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn142 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn143 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn144 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CavatAdjustedValueTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CavatAdjustedPercentSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CavatAppropriatenessGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01440ATCAVATListCTIAppropriatenessBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView34 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn145 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn146 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn147 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn148 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CavatFunctionalValueTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CavatAmenityFactorsGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01439ATCAVATListCTIAmenityFactorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView33 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn149 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn150 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn151 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn152 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CavatFunctionalValueFactorGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01438ATCAVATListCTIFunctionalValueFactorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView32 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn153 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn154 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn155 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn156 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CavatCTIValueTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CavatAccessibilityGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01437ATCAVATListCTIAccessibilityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn137 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn138 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn139 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn140 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CavatCTIFactorGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01436ATCAVATListCTIFactorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView31 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDisplayValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colListType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CavatBasicValueTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CavatUnitValueFactorSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CavatStemDiameterSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView30 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn136 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01380ATActionsLinkedToInspectionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView29 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionOwnership = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActionScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActionBudgetedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colActionUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostDifference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCompletionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01367ATInspectionsForTreesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView25 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn132 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreePolygonXY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionInspector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionIncidentReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionIncidentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionAngleToVertical = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionLastModified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colInspectionStemPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionStemDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownPhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownDisease3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCrownFoliation3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionGeneralCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionBasePhysical3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionVitality = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedOutstandingActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colNoFurtherActionRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn133 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn134 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn135 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SpeciesVarietyGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01374ATSpeciesVarietiesWithBlanksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView28 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ObjectWidthSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ObjectLengthSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.decHeightSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.intTreeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strGridRefTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateAddedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.idTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intDistanceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.strHouseNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dtLastInspectionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.intInspectionCycleSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dtNextInspectionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.intDBHSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.intCrownDiameterSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.intCrownDepthSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dtPlantDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.intGroupNoSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.strMapLabelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.strUser1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intXSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.intYSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.decAreaHaSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.intReplantCountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dtSurveyDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.decRiskFactorSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CavatSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.StemCountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CrownNorthSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CrownSouthSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CrownEastSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CrownWestSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SULESpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.intDBHRangeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBandStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBandEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intHeightRangeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBandStart1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBandEnd1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intSpeciesIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00179SpeciesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSpeciesCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSpeciesRiskFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesScientificName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmenityArbVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colUtilityArbVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intStatusGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01371ATObjectStatusesNoBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.strTreeRefButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.intLegalStatusGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intSafetyPriorityGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intSiteHazardClassGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.strPolygonXYTextEdit = new DevExpress.XtraEditors.MemoEdit();
            this.intPlantSourceGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intPlantMethodGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList1GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn108 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn109 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn110 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn114 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn115 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn116 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn117 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn119 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn120 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn121 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn122 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn123 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn125 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intAccessGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intVisibilityGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intGroundTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView16 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intProtectionTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView17 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn107 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intAgeClassGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView18 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.strSituationMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.intNearbyObjectGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView20 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intNearbyObject2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView21 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intNearbyObject3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView22 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intOwnershipIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01362ATOwnershipListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView23 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOwnershipCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnershipName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.RetentionCategoryGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView24 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01370ATObjectTypesNoBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView19 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intSizeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView26 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.strInspectionUnitComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.intPlantSizeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.strContextPopupContainerEdit = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControl1 = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnPopoupContainerOK1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView27 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn126 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn127 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn128 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn129 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn131 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClearAnyExistingValuesCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.strManagementPopupContainerEdit = new DevExpress.XtraEditors.PopupContainerEdit();
            this.ItemForintTreeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateAdded = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrGridRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintDBH = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintDBHRange = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintHeightRange = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintLegalStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordecHeight = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintGroupNo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSULE = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintAgeClass = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStemCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintSize = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrContext = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrManagement = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForintCrownDiameter = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCrownDepth = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCrownNorth = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCrownWest = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCrownSouth = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCrownEast = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForintSpeciesID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSpeciesVarietyGridLookUpEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRetentionCategory = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintSafetyPriority = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordecRiskFactor = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintSiteHazardClass = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintNearbyObject = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintNearbyObject3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintNearbyObject2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintDistance = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup17 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup18 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup19 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup20 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup21 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup22 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup23 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup24 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup25 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup26 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCavat = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrHouseName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintAccess = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrSituation = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForintVisibility = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordecAreaHa = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrMapLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForObjectLengthSpinEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForObjectWidthSpinEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrPolygonXY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintY = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemFordtNextInspectionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtLastInspectionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintInspectionCycle = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrInspectionUnit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtSurveyDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintPlantSource = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintReplantCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtPlantDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForintPlantSize = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintPlantMethod = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintGroundType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintProtectionType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForUserPickList1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup27 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPicturesGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrTreeRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintOwnershipID = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp01204_AT_Edit_Tree_DetailsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01204_AT_Edit_Tree_DetailsTableAdapter();
            this.sp01370_AT_Object_Types_No_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01370_AT_Object_Types_No_BlankTableAdapter();
            this.sp01371_AT_Object_Statuses_No_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01371_AT_Object_Statuses_No_BlankTableAdapter();
            this.sp01362_AT_Ownership_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01362_AT_Ownership_List_With_BlankTableAdapter();
            this.sp00179_Species_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00179_Species_List_With_BlankTableAdapter();
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter();
            this.sp01367_AT_Inspections_For_TreesTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01367_AT_Inspections_For_TreesTableAdapter();
            this.sp01374_AT_Species_Varieties_With_BlanksTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01374_AT_Species_Varieties_With_BlanksTableAdapter();
            this.sp01375_AT_User_Screen_SettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp01375_AT_User_Screen_SettingsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01375_AT_User_Screen_SettingsTableAdapter();
            this.bbiBlockAddAction = new DevExpress.XtraBars.BarButtonItem();
            this.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp01436_AT_CAVAT_List_CTI_FactorsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01436_AT_CAVAT_List_CTI_FactorsTableAdapter();
            this.sp01437_AT_CAVAT_List_CTI_AccessibilityTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01437_AT_CAVAT_List_CTI_AccessibilityTableAdapter();
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.dataSetATWorkOrdersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp01438_AT_CAVAT_List_CTI_FunctionalValueFactorsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01438_AT_CAVAT_List_CTI_FunctionalValueFactorsTableAdapter();
            this.sp01439_AT_CAVAT_List_CTI_AmenityFactorsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01439_AT_CAVAT_List_CTI_AmenityFactorsTableAdapter();
            this.sp01440_AT_CAVAT_List_CTI_AppropriatenessTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01440_AT_CAVAT_List_CTI_AppropriatenessTableAdapter();
            this.sp01441_AT_CAVAT_List_SLEFactors2TableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01441_AT_CAVAT_List_SLEFactors2TableAdapter();
            this.sp01442_AT_CAVAT_List_CTIRatingTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01442_AT_CAVAT_List_CTIRatingTableAdapter();
            this.sp01443_AT_CAVAT_List_FunctionalAdjustmentTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01443_AT_CAVAT_List_FunctionalAdjustmentTableAdapter();
            this.sp01444_AT_CAVAT_List_SLEFactor2TableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01444_AT_CAVAT_List_SLEFactor2TableAdapter();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp02064_AT_Tree_Pictures_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp02064_AT_Tree_Pictures_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending39 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SiteCodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01204ATEditTreeDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientEditTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp02064ATTreePicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationPolyXYMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationYSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationXSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatSLEFactorGridLookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01444ATCAVATListSLEFactor2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatFunctionalValueTextEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatFunctionalValueFactorGridLookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01443ATCAVATListFunctionalAdjustmentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatCTIValueTextEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatCTIFactorGridLookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01442ATCAVATListCTIRatingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatUnitValueFactorSpinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatStemDiameterSpinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatSLEFactorGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01441ATCAVATListSLEFactors2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatAdjustedValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatAdjustedPercentSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatAppropriatenessGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01440ATCAVATListCTIAppropriatenessBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatFunctionalValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatAmenityFactorsGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01439ATCAVATListCTIAmenityFactorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatFunctionalValueFactorGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01438ATCAVATListCTIFunctionalValueFactorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatCTIValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatAccessibilityGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01437ATCAVATListCTIAccessibilityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatCTIFactorGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01436ATCAVATListCTIFactorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatBasicValueTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatUnitValueFactorSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatStemDiameterSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01380ATActionsLinkedToInspectionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01367ATInspectionsForTreesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeciesVarietyGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01374ATSpeciesVarietiesWithBlanksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObjectWidthSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObjectLengthSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.decHeightSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTreeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strGridRefTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.idTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intDistanceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strHouseNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLastInspectionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLastInspectionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intInspectionCycleSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNextInspectionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNextInspectionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intDBHSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownDiameterSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownDepthSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPlantDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPlantDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intGroupNoSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strMapLabelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intXSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intYSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.decAreaHaSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intReplantCountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtSurveyDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtSurveyDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.decRiskFactorSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StemCountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrownNorthSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrownSouthSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrownEastSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrownWestSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SULESpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intDBHRangeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intHeightRangeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSpeciesIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00179SpeciesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStatusGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01371ATObjectStatusesNoBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTreeRefButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intLegalStatusGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSafetyPriorityGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSiteHazardClassGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strPolygonXYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPlantSourceGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPlantMethodGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList1GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intAccessGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intVisibilityGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intGroundTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intProtectionTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intAgeClassGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strSituationMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intNearbyObjectGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intNearbyObject2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intNearbyObject3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intOwnershipIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01362ATOwnershipListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetentionCategoryGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01370ATObjectTypesNoBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSizeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strInspectionUnitComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPlantSizeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strContextPopupContainerEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClearAnyExistingValuesCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strManagementPopupContainerEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintTreeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrGridRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintDBH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintDBHRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintHeightRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintLegalStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordecHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintGroupNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSULE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintAgeClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStemCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrContext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrManagement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownDiameter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownDepth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCrownNorth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCrownWest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCrownSouth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCrownEast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSpeciesID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpeciesVarietyGridLookUpEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRetentionCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSafetyPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordecRiskFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSiteHazardClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintNearbyObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintNearbyObject3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintNearbyObject2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCavat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrHouseName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintAccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrSituation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintVisibility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordecAreaHa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrMapLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForObjectLengthSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForObjectWidthSpinEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrPolygonXY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtNextInspectionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtLastInspectionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintInspectionCycle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrInspectionUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtSurveyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPlantSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintReplantCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtPlantDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPlantSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPlantMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintGroundType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintProtectionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPicturesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTreeRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintOwnershipID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01375_AT_User_Screen_SettingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetATWorkOrdersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            this.bsiAdd.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockAddAction, true)});
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1145, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 726);
            this.barDockControlBottom.Size = new System.Drawing.Size(1145, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 700);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1145, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 700);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiBlockAddAction});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colItemID1
            // 
            this.colItemID1.Caption = "Variety ID";
            this.colItemID1.FieldName = "ItemID";
            this.colItemID1.Name = "colItemID1";
            this.colItemID1.OptionsColumn.AllowEdit = false;
            this.colItemID1.OptionsColumn.AllowFocus = false;
            this.colItemID1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Item ID";
            this.gridColumn10.FieldName = "ItemID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 58;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Item ID";
            this.gridColumn16.FieldName = "ItemID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 58;
            // 
            // colSpeciesID
            // 
            this.colSpeciesID.Caption = "Species ID";
            this.colSpeciesID.FieldName = "SpeciesID";
            this.colSpeciesID.Name = "colSpeciesID";
            this.colSpeciesID.OptionsColumn.AllowEdit = false;
            this.colSpeciesID.OptionsColumn.AllowFocus = false;
            this.colSpeciesID.OptionsColumn.ReadOnly = true;
            this.colSpeciesID.Width = 71;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Item ID";
            this.gridColumn22.FieldName = "ItemID";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Width = 58;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Item ID";
            this.gridColumn40.FieldName = "ItemID";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Width = 58;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Item ID";
            this.gridColumn46.FieldName = "ItemID";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Width = 58;
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Item ID";
            this.gridColumn88.FieldName = "ItemID";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.AllowEdit = false;
            this.gridColumn88.OptionsColumn.AllowFocus = false;
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Width = 58;
            // 
            // gridColumn94
            // 
            this.gridColumn94.Caption = "Item ID";
            this.gridColumn94.FieldName = "ItemID";
            this.gridColumn94.Name = "gridColumn94";
            this.gridColumn94.OptionsColumn.AllowEdit = false;
            this.gridColumn94.OptionsColumn.AllowFocus = false;
            this.gridColumn94.OptionsColumn.ReadOnly = true;
            this.gridColumn94.Width = 58;
            // 
            // gridColumn112
            // 
            this.gridColumn112.Caption = "Item ID";
            this.gridColumn112.FieldName = "ItemID";
            this.gridColumn112.Name = "gridColumn112";
            this.gridColumn112.OptionsColumn.AllowEdit = false;
            this.gridColumn112.OptionsColumn.AllowFocus = false;
            this.gridColumn112.OptionsColumn.ReadOnly = true;
            this.gridColumn112.Width = 58;
            // 
            // gridColumn118
            // 
            this.gridColumn118.Caption = "Item ID";
            this.gridColumn118.FieldName = "ItemID";
            this.gridColumn118.Name = "gridColumn118";
            this.gridColumn118.OptionsColumn.AllowEdit = false;
            this.gridColumn118.OptionsColumn.AllowFocus = false;
            this.gridColumn118.OptionsColumn.ReadOnly = true;
            this.gridColumn118.Width = 58;
            // 
            // gridColumn124
            // 
            this.gridColumn124.Caption = "Item ID";
            this.gridColumn124.FieldName = "ItemID";
            this.gridColumn124.Name = "gridColumn124";
            this.gridColumn124.OptionsColumn.AllowEdit = false;
            this.gridColumn124.OptionsColumn.AllowFocus = false;
            this.gridColumn124.OptionsColumn.ReadOnly = true;
            this.gridColumn124.Width = 58;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Item ID";
            this.gridColumn70.FieldName = "ItemID";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            this.gridColumn70.Width = 58;
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Item ID";
            this.gridColumn76.FieldName = "ItemID";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.Width = 58;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Item ID";
            this.gridColumn100.FieldName = "ItemID";
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.OptionsColumn.AllowEdit = false;
            this.gridColumn100.OptionsColumn.AllowFocus = false;
            this.gridColumn100.OptionsColumn.ReadOnly = true;
            this.gridColumn100.Width = 58;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "Item ID";
            this.gridColumn106.FieldName = "ItemID";
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.OptionsColumn.AllowEdit = false;
            this.gridColumn106.OptionsColumn.AllowFocus = false;
            this.gridColumn106.OptionsColumn.ReadOnly = true;
            this.gridColumn106.Width = 58;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Item ID";
            this.gridColumn28.FieldName = "ItemID";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Width = 58;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Item ID";
            this.gridColumn52.FieldName = "ItemID";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Width = 58;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Item ID";
            this.gridColumn58.FieldName = "ItemID";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Width = 58;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Item ID";
            this.gridColumn64.FieldName = "ItemID";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.Width = 58;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Item ID";
            this.gridColumn34.FieldName = "ItemID";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Width = 58;
            // 
            // colItemID
            // 
            this.colItemID.Caption = "Item ID";
            this.colItemID.FieldName = "ItemID";
            this.colItemID.Name = "colItemID";
            this.colItemID.OptionsColumn.AllowEdit = false;
            this.colItemID.OptionsColumn.AllowFocus = false;
            this.colItemID.OptionsColumn.ReadOnly = true;
            this.colItemID.Width = 58;
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Item ID";
            this.gridColumn82.FieldName = "ItemID";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            this.gridColumn82.Width = 58;
            // 
            // gridColumn130
            // 
            this.gridColumn130.Caption = "Item ID";
            this.gridColumn130.FieldName = "ItemID";
            this.gridColumn130.Name = "gridColumn130";
            this.gridColumn130.OptionsColumn.AllowEdit = false;
            this.gridColumn130.OptionsColumn.AllowFocus = false;
            this.gridColumn130.OptionsColumn.ReadOnly = true;
            this.gridColumn130.Width = 58;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2,
            this.bar4});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiCopyAllValues,
            this.bbiCopySelectedValues,
            this.bbiShowMapButton,
            this.bbiRememberDetails,
            this.bbiReload});
            this.barManager2.MaxItemId = 23;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(591, 185);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRememberDetails),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReload, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopyAllValues, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopySelectedValues)});
            this.bar2.Text = "Copy Values";
            // 
            // bbiRememberDetails
            // 
            this.bbiRememberDetails.Caption = "Copy Tree";
            this.bbiRememberDetails.Id = 21;
            this.bbiRememberDetails.Name = "bbiRememberDetails";
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Copy Tree - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiRememberDetails.SuperTip = superToolTip4;
            this.bbiRememberDetails.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRememberDetails_ItemClick);
            // 
            // bbiReload
            // 
            this.bbiReload.Caption = "Reload";
            this.bbiReload.Id = 22;
            this.bbiReload.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiReload.ImageOptions.Image")));
            this.bbiReload.Name = "bbiReload";
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Reload - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to refresh the currently loaded last copied record.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiReload.SuperTip = superToolTip5;
            this.bbiReload.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReload_ItemClick);
            // 
            // bbiCopyAllValues
            // 
            this.bbiCopyAllValues.Caption = "Copy ALL Values";
            this.bbiCopyAllValues.Id = 15;
            this.bbiCopyAllValues.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyAllValues.ImageOptions.Image")));
            this.bbiCopyAllValues.Name = "bbiCopyAllValues";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Paste All Values - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to paste <b>all</b> values from the last copied record to this record.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bbiCopyAllValues.SuperTip = superToolTip6;
            this.bbiCopyAllValues.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopyAllValues_ItemClick);
            // 
            // bbiCopySelectedValues
            // 
            this.bbiCopySelectedValues.Caption = "Copy Selected Values";
            this.bbiCopySelectedValues.Id = 16;
            this.bbiCopySelectedValues.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopySelectedValues.ImageOptions.Image")));
            this.bbiCopySelectedValues.Name = "bbiCopySelectedValues";
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Paste Selected Values - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to open a screen to specify which values to paste from the last copied r" +
    "ecord to this record.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiCopySelectedValues.SuperTip = superToolTip7;
            this.bbiCopySelectedValues.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopySelectedValues_ItemClick);
            // 
            // bar4
            // 
            this.bar4.BarName = "Custom 5";
            this.bar4.DockCol = 2;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiShowMapButton)});
            this.bar4.Text = "Mapping";
            // 
            // bbiShowMapButton
            // 
            this.bbiShowMapButton.Caption = "Show Map";
            this.bbiShowMapButton.Id = 18;
            this.bbiShowMapButton.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMapButton.ImageOptions.Image")));
            this.bbiShowMapButton.Name = "bbiShowMapButton";
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "View On Map - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to view the current record on the map.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiShowMapButton.SuperTip = superToolTip8;
            this.bbiShowMapButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShowMapButton_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1145, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 726);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1145, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 700);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1145, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 700);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Refresh2_16x16, "Refresh2_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "Refresh2_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SiteCodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientEditTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl39);
            this.dataLayoutControl1.Controls.Add(this.LocationPolyXYMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.LocationYSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LocationXSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatSLEFactorGridLookUpEdit2);
            this.dataLayoutControl1.Controls.Add(this.CavatFunctionalValueTextEdit2);
            this.dataLayoutControl1.Controls.Add(this.CavatFunctionalValueFactorGridLookUpEdit2);
            this.dataLayoutControl1.Controls.Add(this.CavatCTIValueTextEdit2);
            this.dataLayoutControl1.Controls.Add(this.CavatCTIFactorGridLookUpEdit2);
            this.dataLayoutControl1.Controls.Add(this.CavatUnitValueFactorSpinEdit2);
            this.dataLayoutControl1.Controls.Add(this.CavatStemDiameterSpinEdit2);
            this.dataLayoutControl1.Controls.Add(this.CavatSLEFactorGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatAdjustedValueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatAdjustedPercentSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatAppropriatenessGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatFunctionalValueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatAmenityFactorsGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatFunctionalValueFactorGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatCTIValueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatAccessibilityGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatCTIFactorGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatBasicValueTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatUnitValueFactorSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatStemDiameterSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer3);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer2);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer1);
            this.dataLayoutControl1.Controls.Add(this.SpeciesVarietyGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ObjectWidthSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ObjectLengthSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.decHeightSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intTreeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strGridRefTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strPostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateAddedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.idTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intDistanceSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.strHouseNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dtLastInspectionDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.intInspectionCycleSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dtNextInspectionDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.intDBHSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownDiameterSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownDepthSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dtPlantDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.intGroupNoSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.strMapLabelTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.intXSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.intYSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.decAreaHaSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.intReplantCountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dtSurveyDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.decRiskFactorSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CavatSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.StemCountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CrownNorthSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CrownSouthSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CrownEastSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CrownWestSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SULESpinEdit);
            this.dataLayoutControl1.Controls.Add(this.intDBHRangeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intHeightRangeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intSpeciesIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intStatusGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.strTreeRefButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.intLegalStatusGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intSafetyPriorityGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intSiteHazardClassGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.strPolygonXYTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intPlantSourceGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intPlantMethodGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList1GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intAccessGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intVisibilityGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intGroundTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intProtectionTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intAgeClassGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.strSituationMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.intNearbyObjectGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intNearbyObject2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intNearbyObject3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intOwnershipIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.RetentionCategoryGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intSizeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.strInspectionUnitComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.intPlantSizeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.strContextPopupContainerEdit);
            this.dataLayoutControl1.Controls.Add(this.strManagementPopupContainerEdit);
            this.dataLayoutControl1.DataSource = this.sp01204ATEditTreeDetailsBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintTreeID,
            this.ItemForDateAdded,
            this.ItemForstrGridRef,
            this.ItemForstrPostcode,
            this.ItemForClientID,
            this.ItemForSiteID,
            this.ItemForSiteCode});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(192, 200, 379, 558);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1145, 700);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SiteCodeTextEdit
            // 
            this.SiteCodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "SiteCode", true));
            this.SiteCodeTextEdit.Location = new System.Drawing.Point(131, 179);
            this.SiteCodeTextEdit.MenuManager = this.barManager1;
            this.SiteCodeTextEdit.Name = "SiteCodeTextEdit";
            this.SiteCodeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteCodeTextEdit, true);
            this.SiteCodeTextEdit.Size = new System.Drawing.Size(985, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteCodeTextEdit, optionsSpelling1);
            this.SiteCodeTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteCodeTextEdit.TabIndex = 105;
            // 
            // sp01204ATEditTreeDetailsBindingSource
            // 
            this.sp01204ATEditTreeDetailsBindingSource.DataMember = "sp01204_AT_Edit_Tree_Details";
            this.sp01204ATEditTreeDetailsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SiteIDTextEdit
            // 
            this.SiteIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "SiteID", true));
            this.SiteIDTextEdit.Location = new System.Drawing.Point(143, 241);
            this.SiteIDTextEdit.MenuManager = this.barManager1;
            this.SiteIDTextEdit.Name = "SiteIDTextEdit";
            this.SiteIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteIDTextEdit, true);
            this.SiteIDTextEdit.Size = new System.Drawing.Size(961, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteIDTextEdit, optionsSpelling2);
            this.SiteIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteIDTextEdit.TabIndex = 104;
            // 
            // ClientEditTextEdit
            // 
            this.ClientEditTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "ClientID", true));
            this.ClientEditTextEdit.Location = new System.Drawing.Point(143, 241);
            this.ClientEditTextEdit.MenuManager = this.barManager1;
            this.ClientEditTextEdit.Name = "ClientEditTextEdit";
            this.ClientEditTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientEditTextEdit, true);
            this.ClientEditTextEdit.Size = new System.Drawing.Size(961, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientEditTextEdit, optionsSpelling3);
            this.ClientEditTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientEditTextEdit.TabIndex = 103;
            // 
            // SiteNameButtonEdit
            // 
            this.SiteNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "SiteName", true));
            this.SiteNameButtonEdit.EditValue = "";
            this.SiteNameButtonEdit.Location = new System.Drawing.Point(143, -74);
            this.SiteNameButtonEdit.MenuManager = this.barManager1;
            this.SiteNameButtonEdit.Name = "SiteNameButtonEdit";
            this.SiteNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Site screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SiteNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SiteNameButtonEdit.Size = new System.Drawing.Size(961, 20);
            this.SiteNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameButtonEdit.TabIndex = 80;
            this.SiteNameButtonEdit.TabStop = false;
            this.SiteNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SiteNameButtonEdit_ButtonClick);
            this.SiteNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SiteNameButtonEdit_Validating);
            // 
            // gridControl39
            // 
            this.gridControl39.DataSource = this.sp02064ATTreePicturesListBindingSource;
            this.gridControl39.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl39.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl39.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl39.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl39_EmbeddedNavigator_ButtonClick);
            this.gridControl39.Location = new System.Drawing.Point(24, 58);
            this.gridControl39.MainView = this.gridView39;
            this.gridControl39.MenuManager = this.barManager1;
            this.gridControl39.Name = "gridControl39";
            this.gridControl39.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemTextEditDateTime});
            this.gridControl39.Size = new System.Drawing.Size(1080, 618);
            this.gridControl39.TabIndex = 80;
            this.gridControl39.UseEmbeddedNavigator = true;
            this.gridControl39.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView39});
            // 
            // sp02064ATTreePicturesListBindingSource
            // 
            this.sp02064ATTreePicturesListBindingSource.DataMember = "sp02064_AT_Tree_Pictures_List";
            this.sp02064ATTreePicturesListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView39
            // 
            this.gridView39.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn169,
            this.gridColumn170,
            this.gridColumn171,
            this.gridColumn172,
            this.gridColumn173,
            this.gridColumn174,
            this.gridColumn175,
            this.gridColumn176,
            this.gridColumn177,
            this.gridColumn178,
            this.gridColumn179,
            this.colShortLinkedRecordDescription});
            this.gridView39.GridControl = this.gridControl39;
            this.gridView39.Name = "gridView39";
            this.gridView39.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView39.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView39.OptionsLayout.StoreAppearance = true;
            this.gridView39.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView39.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView39.OptionsSelection.MultiSelect = true;
            this.gridView39.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView39.OptionsView.ColumnAutoWidth = false;
            this.gridView39.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView39.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView39.OptionsView.ShowGroupPanel = false;
            this.gridView39.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn174, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView39.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView39_CustomDrawCell);
            this.gridView39.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView39.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView39.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView39.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView39.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView39.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView39_MouseUp);
            this.gridView39.DoubleClick += new System.EventHandler(this.gridView39_DoubleClick);
            this.gridView39.GotFocus += new System.EventHandler(this.gridView39_GotFocus);
            // 
            // gridColumn169
            // 
            this.gridColumn169.Caption = "Survey Picture ID";
            this.gridColumn169.FieldName = "SurveyPictureID";
            this.gridColumn169.Name = "gridColumn169";
            this.gridColumn169.OptionsColumn.AllowEdit = false;
            this.gridColumn169.OptionsColumn.AllowFocus = false;
            this.gridColumn169.OptionsColumn.ReadOnly = true;
            this.gridColumn169.Width = 105;
            // 
            // gridColumn170
            // 
            this.gridColumn170.Caption = "Linked To Record ID";
            this.gridColumn170.FieldName = "LinkedToRecordID";
            this.gridColumn170.Name = "gridColumn170";
            this.gridColumn170.OptionsColumn.AllowEdit = false;
            this.gridColumn170.OptionsColumn.AllowFocus = false;
            this.gridColumn170.OptionsColumn.ReadOnly = true;
            this.gridColumn170.Width = 117;
            // 
            // gridColumn171
            // 
            this.gridColumn171.Caption = "Linked To Record Type ID";
            this.gridColumn171.FieldName = "LinkedToRecordTypeID";
            this.gridColumn171.Name = "gridColumn171";
            this.gridColumn171.OptionsColumn.AllowEdit = false;
            this.gridColumn171.OptionsColumn.AllowFocus = false;
            this.gridColumn171.OptionsColumn.ReadOnly = true;
            this.gridColumn171.Width = 144;
            // 
            // gridColumn172
            // 
            this.gridColumn172.Caption = "Picture Type ID";
            this.gridColumn172.FieldName = "PictureTypeID";
            this.gridColumn172.Name = "gridColumn172";
            this.gridColumn172.OptionsColumn.AllowEdit = false;
            this.gridColumn172.OptionsColumn.AllowFocus = false;
            this.gridColumn172.OptionsColumn.ReadOnly = true;
            this.gridColumn172.Width = 95;
            // 
            // gridColumn173
            // 
            this.gridColumn173.Caption = "Picture Path";
            this.gridColumn173.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.gridColumn173.FieldName = "PicturePath";
            this.gridColumn173.Name = "gridColumn173";
            this.gridColumn173.OptionsColumn.ReadOnly = true;
            this.gridColumn173.Visible = true;
            this.gridColumn173.VisibleIndex = 2;
            this.gridColumn173.Width = 472;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // gridColumn174
            // 
            this.gridColumn174.Caption = "Date Taken";
            this.gridColumn174.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.gridColumn174.FieldName = "DateTimeTaken";
            this.gridColumn174.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn174.Name = "gridColumn174";
            this.gridColumn174.OptionsColumn.AllowEdit = false;
            this.gridColumn174.OptionsColumn.AllowFocus = false;
            this.gridColumn174.OptionsColumn.ReadOnly = true;
            this.gridColumn174.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn174.Visible = true;
            this.gridColumn174.VisibleIndex = 0;
            this.gridColumn174.Width = 99;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // gridColumn175
            // 
            this.gridColumn175.Caption = "Added By Staff ID";
            this.gridColumn175.FieldName = "AddedByStaffID";
            this.gridColumn175.Name = "gridColumn175";
            this.gridColumn175.OptionsColumn.AllowEdit = false;
            this.gridColumn175.OptionsColumn.AllowFocus = false;
            this.gridColumn175.OptionsColumn.ReadOnly = true;
            this.gridColumn175.Width = 108;
            // 
            // gridColumn176
            // 
            this.gridColumn176.Caption = "GUID";
            this.gridColumn176.FieldName = "GUID";
            this.gridColumn176.Name = "gridColumn176";
            this.gridColumn176.OptionsColumn.AllowEdit = false;
            this.gridColumn176.OptionsColumn.AllowFocus = false;
            this.gridColumn176.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn177
            // 
            this.gridColumn177.Caption = "Remarks";
            this.gridColumn177.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn177.FieldName = "Remarks";
            this.gridColumn177.Name = "gridColumn177";
            this.gridColumn177.OptionsColumn.ReadOnly = true;
            this.gridColumn177.Visible = true;
            this.gridColumn177.VisibleIndex = 3;
            this.gridColumn177.Width = 252;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // gridColumn178
            // 
            this.gridColumn178.Caption = "Linked To Tree";
            this.gridColumn178.FieldName = "LinkedRecordDescription";
            this.gridColumn178.Name = "gridColumn178";
            this.gridColumn178.OptionsColumn.AllowEdit = false;
            this.gridColumn178.OptionsColumn.AllowFocus = false;
            this.gridColumn178.OptionsColumn.ReadOnly = true;
            this.gridColumn178.Width = 303;
            // 
            // gridColumn179
            // 
            this.gridColumn179.Caption = "Added By";
            this.gridColumn179.FieldName = "AddedByStaffName";
            this.gridColumn179.Name = "gridColumn179";
            this.gridColumn179.OptionsColumn.AllowEdit = false;
            this.gridColumn179.OptionsColumn.AllowFocus = false;
            this.gridColumn179.OptionsColumn.ReadOnly = true;
            this.gridColumn179.Visible = true;
            this.gridColumn179.VisibleIndex = 1;
            this.gridColumn179.Width = 108;
            // 
            // colShortLinkedRecordDescription
            // 
            this.colShortLinkedRecordDescription.Caption = "Linked To Tree (Short Desc)";
            this.colShortLinkedRecordDescription.FieldName = "ShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.Name = "colShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colShortLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colShortLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colShortLinkedRecordDescription.Width = 231;
            // 
            // LocationPolyXYMemoEdit
            // 
            this.LocationPolyXYMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "LocationPolyXY", true));
            this.LocationPolyXYMemoEdit.Location = new System.Drawing.Point(685, 318);
            this.LocationPolyXYMemoEdit.MenuManager = this.barManager1;
            this.LocationPolyXYMemoEdit.Name = "LocationPolyXYMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.LocationPolyXYMemoEdit, true);
            this.LocationPolyXYMemoEdit.Size = new System.Drawing.Size(407, 92);
            this.scSpellChecker.SetSpellCheckerOptions(this.LocationPolyXYMemoEdit, optionsSpelling4);
            this.LocationPolyXYMemoEdit.StyleController = this.dataLayoutControl1;
            this.LocationPolyXYMemoEdit.TabIndex = 99;
            // 
            // LocationYSpinEdit
            // 
            this.LocationYSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "LocationY", true));
            this.LocationYSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LocationYSpinEdit.Location = new System.Drawing.Point(685, 294);
            this.LocationYSpinEdit.MenuManager = this.barManager1;
            this.LocationYSpinEdit.Name = "LocationYSpinEdit";
            this.LocationYSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LocationYSpinEdit.Size = new System.Drawing.Size(407, 20);
            this.LocationYSpinEdit.StyleController = this.dataLayoutControl1;
            this.LocationYSpinEdit.TabIndex = 98;
            // 
            // LocationXSpinEdit
            // 
            this.LocationXSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "LocationX", true));
            this.LocationXSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LocationXSpinEdit.Location = new System.Drawing.Point(685, 270);
            this.LocationXSpinEdit.MenuManager = this.barManager1;
            this.LocationXSpinEdit.Name = "LocationXSpinEdit";
            this.LocationXSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LocationXSpinEdit.Size = new System.Drawing.Size(407, 20);
            this.LocationXSpinEdit.StyleController = this.dataLayoutControl1;
            this.LocationXSpinEdit.TabIndex = 97;
            // 
            // CavatSLEFactorGridLookUpEdit2
            // 
            this.CavatSLEFactorGridLookUpEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatSLEFactor", true));
            this.CavatSLEFactorGridLookUpEdit2.Location = new System.Drawing.Point(680, 362);
            this.CavatSLEFactorGridLookUpEdit2.MenuManager = this.barManager1;
            this.CavatSLEFactorGridLookUpEdit2.Name = "CavatSLEFactorGridLookUpEdit2";
            this.CavatSLEFactorGridLookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CavatSLEFactorGridLookUpEdit2.Properties.DataSource = this.sp01444ATCAVATListSLEFactor2BindingSource;
            this.CavatSLEFactorGridLookUpEdit2.Properties.DisplayMember = "DisplayValue";
            this.CavatSLEFactorGridLookUpEdit2.Properties.NullText = "";
            this.CavatSLEFactorGridLookUpEdit2.Properties.PopupView = this.gridView38;
            this.CavatSLEFactorGridLookUpEdit2.Properties.ValueMember = "Value";
            this.CavatSLEFactorGridLookUpEdit2.Size = new System.Drawing.Size(400, 20);
            this.CavatSLEFactorGridLookUpEdit2.StyleController = this.dataLayoutControl1;
            this.CavatSLEFactorGridLookUpEdit2.TabIndex = 95;
            this.CavatSLEFactorGridLookUpEdit2.Tag = "8";
            // 
            // sp01444ATCAVATListSLEFactor2BindingSource
            // 
            this.sp01444ATCAVATListSLEFactor2BindingSource.DataMember = "sp01444_AT_CAVAT_List_SLEFactor2";
            this.sp01444ATCAVATListSLEFactor2BindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView38
            // 
            this.gridView38.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn165,
            this.gridColumn166,
            this.gridColumn167,
            this.gridColumn168});
            this.gridView38.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView38.Name = "gridView38";
            this.gridView38.OptionsCustomization.AllowFilter = false;
            this.gridView38.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView38.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView38.OptionsLayout.StoreAppearance = true;
            this.gridView38.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView38.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView38.OptionsView.ColumnAutoWidth = false;
            this.gridView38.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView38.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView38.OptionsView.ShowGroupPanel = false;
            this.gridView38.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView38.OptionsView.ShowIndicator = false;
            this.gridView38.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn166, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn165
            // 
            this.gridColumn165.Caption = "Value";
            this.gridColumn165.FieldName = "DisplayValue";
            this.gridColumn165.Name = "gridColumn165";
            this.gridColumn165.OptionsColumn.AllowEdit = false;
            this.gridColumn165.OptionsColumn.AllowFocus = false;
            this.gridColumn165.OptionsColumn.ReadOnly = true;
            this.gridColumn165.Visible = true;
            this.gridColumn165.VisibleIndex = 0;
            this.gridColumn165.Width = 219;
            // 
            // gridColumn166
            // 
            this.gridColumn166.Caption = "Order";
            this.gridColumn166.FieldName = "ItemOrder";
            this.gridColumn166.Name = "gridColumn166";
            this.gridColumn166.OptionsColumn.AllowEdit = false;
            this.gridColumn166.OptionsColumn.AllowFocus = false;
            this.gridColumn166.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn167
            // 
            this.gridColumn167.Caption = "List Type";
            this.gridColumn167.FieldName = "ListType";
            this.gridColumn167.Name = "gridColumn167";
            this.gridColumn167.OptionsColumn.AllowEdit = false;
            this.gridColumn167.OptionsColumn.AllowFocus = false;
            this.gridColumn167.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn168
            // 
            this.gridColumn168.Caption = "Actual Value";
            this.gridColumn168.FieldName = "Value";
            this.gridColumn168.Name = "gridColumn168";
            this.gridColumn168.OptionsColumn.AllowEdit = false;
            this.gridColumn168.OptionsColumn.AllowFocus = false;
            this.gridColumn168.OptionsColumn.ReadOnly = true;
            this.gridColumn168.Width = 81;
            // 
            // CavatFunctionalValueTextEdit2
            // 
            this.CavatFunctionalValueTextEdit2.Location = new System.Drawing.Point(680, 292);
            this.CavatFunctionalValueTextEdit2.MenuManager = this.barManager1;
            this.CavatFunctionalValueTextEdit2.Name = "CavatFunctionalValueTextEdit2";
            this.CavatFunctionalValueTextEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.CavatFunctionalValueTextEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CavatFunctionalValueTextEdit2.Properties.Mask.EditMask = "c";
            this.CavatFunctionalValueTextEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CavatFunctionalValueTextEdit2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CavatFunctionalValueTextEdit2.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CavatFunctionalValueTextEdit2, true);
            this.CavatFunctionalValueTextEdit2.Size = new System.Drawing.Size(400, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CavatFunctionalValueTextEdit2, optionsSpelling5);
            this.CavatFunctionalValueTextEdit2.StyleController = this.dataLayoutControl1;
            this.CavatFunctionalValueTextEdit2.TabIndex = 91;
            // 
            // CavatFunctionalValueFactorGridLookUpEdit2
            // 
            this.CavatFunctionalValueFactorGridLookUpEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatFunctionalValueFactor", true));
            this.CavatFunctionalValueFactorGridLookUpEdit2.Location = new System.Drawing.Point(680, 268);
            this.CavatFunctionalValueFactorGridLookUpEdit2.MenuManager = this.barManager1;
            this.CavatFunctionalValueFactorGridLookUpEdit2.Name = "CavatFunctionalValueFactorGridLookUpEdit2";
            this.CavatFunctionalValueFactorGridLookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CavatFunctionalValueFactorGridLookUpEdit2.Properties.DataSource = this.sp01443ATCAVATListFunctionalAdjustmentBindingSource;
            this.CavatFunctionalValueFactorGridLookUpEdit2.Properties.DisplayMember = "DisplayValue";
            this.CavatFunctionalValueFactorGridLookUpEdit2.Properties.NullText = "";
            this.CavatFunctionalValueFactorGridLookUpEdit2.Properties.PopupView = this.gridView37;
            this.CavatFunctionalValueFactorGridLookUpEdit2.Properties.ValueMember = "Value";
            this.CavatFunctionalValueFactorGridLookUpEdit2.Size = new System.Drawing.Size(400, 20);
            this.CavatFunctionalValueFactorGridLookUpEdit2.StyleController = this.dataLayoutControl1;
            this.CavatFunctionalValueFactorGridLookUpEdit2.TabIndex = 89;
            this.CavatFunctionalValueFactorGridLookUpEdit2.Tag = "7";
            // 
            // sp01443ATCAVATListFunctionalAdjustmentBindingSource
            // 
            this.sp01443ATCAVATListFunctionalAdjustmentBindingSource.DataMember = "sp01443_AT_CAVAT_List_FunctionalAdjustment";
            this.sp01443ATCAVATListFunctionalAdjustmentBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView37
            // 
            this.gridView37.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn161,
            this.gridColumn162,
            this.gridColumn163,
            this.gridColumn164});
            this.gridView37.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView37.Name = "gridView37";
            this.gridView37.OptionsCustomization.AllowFilter = false;
            this.gridView37.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView37.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView37.OptionsLayout.StoreAppearance = true;
            this.gridView37.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView37.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView37.OptionsView.ColumnAutoWidth = false;
            this.gridView37.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView37.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView37.OptionsView.ShowGroupPanel = false;
            this.gridView37.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView37.OptionsView.ShowIndicator = false;
            this.gridView37.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn162, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn161
            // 
            this.gridColumn161.Caption = "Value";
            this.gridColumn161.FieldName = "DisplayValue";
            this.gridColumn161.Name = "gridColumn161";
            this.gridColumn161.OptionsColumn.AllowEdit = false;
            this.gridColumn161.OptionsColumn.AllowFocus = false;
            this.gridColumn161.OptionsColumn.ReadOnly = true;
            this.gridColumn161.Visible = true;
            this.gridColumn161.VisibleIndex = 0;
            this.gridColumn161.Width = 219;
            // 
            // gridColumn162
            // 
            this.gridColumn162.Caption = "Order";
            this.gridColumn162.FieldName = "ItemOrder";
            this.gridColumn162.Name = "gridColumn162";
            this.gridColumn162.OptionsColumn.AllowEdit = false;
            this.gridColumn162.OptionsColumn.AllowFocus = false;
            this.gridColumn162.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn163
            // 
            this.gridColumn163.Caption = "List Type";
            this.gridColumn163.FieldName = "ListType";
            this.gridColumn163.Name = "gridColumn163";
            this.gridColumn163.OptionsColumn.AllowEdit = false;
            this.gridColumn163.OptionsColumn.AllowFocus = false;
            this.gridColumn163.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn164
            // 
            this.gridColumn164.Caption = "Actual Value";
            this.gridColumn164.FieldName = "Value";
            this.gridColumn164.Name = "gridColumn164";
            this.gridColumn164.OptionsColumn.AllowEdit = false;
            this.gridColumn164.OptionsColumn.AllowFocus = false;
            this.gridColumn164.OptionsColumn.ReadOnly = true;
            this.gridColumn164.Width = 81;
            // 
            // CavatCTIValueTextEdit2
            // 
            this.CavatCTIValueTextEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatCTIValue", true));
            this.CavatCTIValueTextEdit2.Location = new System.Drawing.Point(680, 198);
            this.CavatCTIValueTextEdit2.MenuManager = this.barManager1;
            this.CavatCTIValueTextEdit2.Name = "CavatCTIValueTextEdit2";
            this.CavatCTIValueTextEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.CavatCTIValueTextEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CavatCTIValueTextEdit2.Properties.Mask.EditMask = "c";
            this.CavatCTIValueTextEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CavatCTIValueTextEdit2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CavatCTIValueTextEdit2.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CavatCTIValueTextEdit2, true);
            this.CavatCTIValueTextEdit2.Size = new System.Drawing.Size(400, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CavatCTIValueTextEdit2, optionsSpelling6);
            this.CavatCTIValueTextEdit2.StyleController = this.dataLayoutControl1;
            this.CavatCTIValueTextEdit2.TabIndex = 88;
            // 
            // CavatCTIFactorGridLookUpEdit2
            // 
            this.CavatCTIFactorGridLookUpEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatCTIFactor", true));
            this.CavatCTIFactorGridLookUpEdit2.Location = new System.Drawing.Point(680, 174);
            this.CavatCTIFactorGridLookUpEdit2.MenuManager = this.barManager1;
            this.CavatCTIFactorGridLookUpEdit2.Name = "CavatCTIFactorGridLookUpEdit2";
            this.CavatCTIFactorGridLookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CavatCTIFactorGridLookUpEdit2.Properties.DataSource = this.sp01442ATCAVATListCTIRatingBindingSource;
            this.CavatCTIFactorGridLookUpEdit2.Properties.DisplayMember = "DisplayValue";
            this.CavatCTIFactorGridLookUpEdit2.Properties.NullText = "";
            this.CavatCTIFactorGridLookUpEdit2.Properties.PopupView = this.gridView36;
            this.CavatCTIFactorGridLookUpEdit2.Properties.ValueMember = "Value";
            this.CavatCTIFactorGridLookUpEdit2.Size = new System.Drawing.Size(400, 20);
            this.CavatCTIFactorGridLookUpEdit2.StyleController = this.dataLayoutControl1;
            this.CavatCTIFactorGridLookUpEdit2.TabIndex = 96;
            this.CavatCTIFactorGridLookUpEdit2.Tag = "6";
            // 
            // sp01442ATCAVATListCTIRatingBindingSource
            // 
            this.sp01442ATCAVATListCTIRatingBindingSource.DataMember = "sp01442_AT_CAVAT_List_CTIRating";
            this.sp01442ATCAVATListCTIRatingBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView36
            // 
            this.gridView36.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn157,
            this.gridColumn158,
            this.gridColumn159,
            this.gridColumn160});
            this.gridView36.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView36.Name = "gridView36";
            this.gridView36.OptionsCustomization.AllowFilter = false;
            this.gridView36.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView36.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView36.OptionsLayout.StoreAppearance = true;
            this.gridView36.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView36.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView36.OptionsView.ColumnAutoWidth = false;
            this.gridView36.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView36.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView36.OptionsView.ShowGroupPanel = false;
            this.gridView36.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView36.OptionsView.ShowIndicator = false;
            this.gridView36.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn158, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn157
            // 
            this.gridColumn157.Caption = "Value";
            this.gridColumn157.FieldName = "DisplayValue";
            this.gridColumn157.Name = "gridColumn157";
            this.gridColumn157.OptionsColumn.AllowEdit = false;
            this.gridColumn157.OptionsColumn.AllowFocus = false;
            this.gridColumn157.OptionsColumn.ReadOnly = true;
            this.gridColumn157.Visible = true;
            this.gridColumn157.VisibleIndex = 0;
            this.gridColumn157.Width = 219;
            // 
            // gridColumn158
            // 
            this.gridColumn158.Caption = "Order";
            this.gridColumn158.FieldName = "ItemOrder";
            this.gridColumn158.Name = "gridColumn158";
            this.gridColumn158.OptionsColumn.AllowEdit = false;
            this.gridColumn158.OptionsColumn.AllowFocus = false;
            this.gridColumn158.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn159
            // 
            this.gridColumn159.Caption = "List Type";
            this.gridColumn159.FieldName = "ListType";
            this.gridColumn159.Name = "gridColumn159";
            this.gridColumn159.OptionsColumn.AllowEdit = false;
            this.gridColumn159.OptionsColumn.AllowFocus = false;
            this.gridColumn159.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn160
            // 
            this.gridColumn160.Caption = "Actual Value";
            this.gridColumn160.FieldName = "Value";
            this.gridColumn160.Name = "gridColumn160";
            this.gridColumn160.OptionsColumn.AllowEdit = false;
            this.gridColumn160.OptionsColumn.AllowFocus = false;
            this.gridColumn160.OptionsColumn.ReadOnly = true;
            this.gridColumn160.Width = 81;
            // 
            // CavatUnitValueFactorSpinEdit2
            // 
            this.CavatUnitValueFactorSpinEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatUnitValueFactor", true));
            this.CavatUnitValueFactorSpinEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CavatUnitValueFactorSpinEdit2.Location = new System.Drawing.Point(680, 150);
            this.CavatUnitValueFactorSpinEdit2.MenuManager = this.barManager1;
            this.CavatUnitValueFactorSpinEdit2.Name = "CavatUnitValueFactorSpinEdit2";
            this.CavatUnitValueFactorSpinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CavatUnitValueFactorSpinEdit2.Properties.Mask.EditMask = "f2";
            this.CavatUnitValueFactorSpinEdit2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CavatUnitValueFactorSpinEdit2.Size = new System.Drawing.Size(400, 20);
            this.CavatUnitValueFactorSpinEdit2.StyleController = this.dataLayoutControl1;
            this.CavatUnitValueFactorSpinEdit2.TabIndex = 95;
            this.CavatUnitValueFactorSpinEdit2.ToolTip = "Default Value supplied by System Settings";
            this.CavatUnitValueFactorSpinEdit2.EditValueChanged += new System.EventHandler(this.CavatUnitValueFactorSpinEdit2_EditValueChanged);
            this.CavatUnitValueFactorSpinEdit2.Validated += new System.EventHandler(this.CavatUnitValueFactorSpinEdit2_Validated);
            // 
            // CavatStemDiameterSpinEdit2
            // 
            this.CavatStemDiameterSpinEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intDBH", true));
            this.CavatStemDiameterSpinEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CavatStemDiameterSpinEdit2.Location = new System.Drawing.Point(680, 126);
            this.CavatStemDiameterSpinEdit2.MenuManager = this.barManager1;
            this.CavatStemDiameterSpinEdit2.Name = "CavatStemDiameterSpinEdit2";
            this.CavatStemDiameterSpinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CavatStemDiameterSpinEdit2.Properties.IsFloatValue = false;
            this.CavatStemDiameterSpinEdit2.Properties.Mask.EditMask = "N00";
            this.CavatStemDiameterSpinEdit2.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.CavatStemDiameterSpinEdit2.Properties.ReadOnly = true;
            this.CavatStemDiameterSpinEdit2.Size = new System.Drawing.Size(400, 20);
            this.CavatStemDiameterSpinEdit2.StyleController = this.dataLayoutControl1;
            this.CavatStemDiameterSpinEdit2.TabIndex = 83;
            this.CavatStemDiameterSpinEdit2.ToolTip = "Value taken from DBH Box";
            // 
            // CavatSLEFactorGridLookUpEdit
            // 
            this.CavatSLEFactorGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatSLEFactor", true));
            this.CavatSLEFactorGridLookUpEdit.Location = new System.Drawing.Point(167, 598);
            this.CavatSLEFactorGridLookUpEdit.MenuManager = this.barManager1;
            this.CavatSLEFactorGridLookUpEdit.Name = "CavatSLEFactorGridLookUpEdit";
            this.CavatSLEFactorGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CavatSLEFactorGridLookUpEdit.Properties.DataSource = this.sp01441ATCAVATListSLEFactors2BindingSource;
            this.CavatSLEFactorGridLookUpEdit.Properties.DisplayMember = "DisplayValue";
            this.CavatSLEFactorGridLookUpEdit.Properties.NullText = "";
            this.CavatSLEFactorGridLookUpEdit.Properties.PopupView = this.gridView35;
            this.CavatSLEFactorGridLookUpEdit.Properties.ValueMember = "Value";
            this.CavatSLEFactorGridLookUpEdit.Size = new System.Drawing.Size(342, 20);
            this.CavatSLEFactorGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CavatSLEFactorGridLookUpEdit.TabIndex = 94;
            this.CavatSLEFactorGridLookUpEdit.Tag = "5";
            // 
            // sp01441ATCAVATListSLEFactors2BindingSource
            // 
            this.sp01441ATCAVATListSLEFactors2BindingSource.DataMember = "sp01441_AT_CAVAT_List_SLEFactors2";
            this.sp01441ATCAVATListSLEFactors2BindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView35
            // 
            this.gridView35.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn141,
            this.gridColumn142,
            this.gridColumn143,
            this.gridColumn144});
            this.gridView35.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView35.Name = "gridView35";
            this.gridView35.OptionsCustomization.AllowFilter = false;
            this.gridView35.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView35.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView35.OptionsLayout.StoreAppearance = true;
            this.gridView35.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView35.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView35.OptionsView.ColumnAutoWidth = false;
            this.gridView35.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView35.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView35.OptionsView.ShowGroupPanel = false;
            this.gridView35.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView35.OptionsView.ShowIndicator = false;
            this.gridView35.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn142, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn141
            // 
            this.gridColumn141.Caption = "Value";
            this.gridColumn141.FieldName = "DisplayValue";
            this.gridColumn141.Name = "gridColumn141";
            this.gridColumn141.OptionsColumn.AllowEdit = false;
            this.gridColumn141.OptionsColumn.AllowFocus = false;
            this.gridColumn141.OptionsColumn.ReadOnly = true;
            this.gridColumn141.Visible = true;
            this.gridColumn141.VisibleIndex = 0;
            this.gridColumn141.Width = 219;
            // 
            // gridColumn142
            // 
            this.gridColumn142.Caption = "Order";
            this.gridColumn142.FieldName = "ItemOrder";
            this.gridColumn142.Name = "gridColumn142";
            this.gridColumn142.OptionsColumn.AllowEdit = false;
            this.gridColumn142.OptionsColumn.AllowFocus = false;
            this.gridColumn142.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn143
            // 
            this.gridColumn143.Caption = "List Type";
            this.gridColumn143.FieldName = "ListType";
            this.gridColumn143.Name = "gridColumn143";
            this.gridColumn143.OptionsColumn.AllowEdit = false;
            this.gridColumn143.OptionsColumn.AllowFocus = false;
            this.gridColumn143.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn144
            // 
            this.gridColumn144.Caption = "Actual Value";
            this.gridColumn144.FieldName = "Value";
            this.gridColumn144.Name = "gridColumn144";
            this.gridColumn144.OptionsColumn.AllowEdit = false;
            this.gridColumn144.OptionsColumn.AllowFocus = false;
            this.gridColumn144.OptionsColumn.ReadOnly = true;
            this.gridColumn144.Width = 81;
            // 
            // CavatAdjustedValueTextEdit
            // 
            this.CavatAdjustedValueTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatAdjustedValue", true));
            this.CavatAdjustedValueTextEdit.Location = new System.Drawing.Point(167, 528);
            this.CavatAdjustedValueTextEdit.MenuManager = this.barManager1;
            this.CavatAdjustedValueTextEdit.Name = "CavatAdjustedValueTextEdit";
            this.CavatAdjustedValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CavatAdjustedValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CavatAdjustedValueTextEdit.Properties.Mask.EditMask = "c";
            this.CavatAdjustedValueTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CavatAdjustedValueTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CavatAdjustedValueTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CavatAdjustedValueTextEdit, true);
            this.CavatAdjustedValueTextEdit.Size = new System.Drawing.Size(342, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CavatAdjustedValueTextEdit, optionsSpelling7);
            this.CavatAdjustedValueTextEdit.StyleController = this.dataLayoutControl1;
            this.CavatAdjustedValueTextEdit.TabIndex = 93;
            // 
            // CavatAdjustedPercentSpinEdit
            // 
            this.CavatAdjustedPercentSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatAdjustedPercent", true));
            this.CavatAdjustedPercentSpinEdit.EditValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.CavatAdjustedPercentSpinEdit.Location = new System.Drawing.Point(167, 504);
            this.CavatAdjustedPercentSpinEdit.MenuManager = this.barManager1;
            this.CavatAdjustedPercentSpinEdit.Name = "CavatAdjustedPercentSpinEdit";
            this.CavatAdjustedPercentSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CavatAdjustedPercentSpinEdit.Properties.Mask.EditMask = "P2";
            this.CavatAdjustedPercentSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CavatAdjustedPercentSpinEdit.Size = new System.Drawing.Size(342, 20);
            this.CavatAdjustedPercentSpinEdit.StyleController = this.dataLayoutControl1;
            this.CavatAdjustedPercentSpinEdit.TabIndex = 92;
            // 
            // CavatAppropriatenessGridLookUpEdit
            // 
            this.CavatAppropriatenessGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatAppropriateness", true));
            this.CavatAppropriatenessGridLookUpEdit.Location = new System.Drawing.Point(167, 480);
            this.CavatAppropriatenessGridLookUpEdit.MenuManager = this.barManager1;
            this.CavatAppropriatenessGridLookUpEdit.Name = "CavatAppropriatenessGridLookUpEdit";
            this.CavatAppropriatenessGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CavatAppropriatenessGridLookUpEdit.Properties.DataSource = this.sp01440ATCAVATListCTIAppropriatenessBindingSource;
            this.CavatAppropriatenessGridLookUpEdit.Properties.DisplayMember = "DisplayValue";
            this.CavatAppropriatenessGridLookUpEdit.Properties.NullText = "";
            this.CavatAppropriatenessGridLookUpEdit.Properties.PopupView = this.gridView34;
            this.CavatAppropriatenessGridLookUpEdit.Properties.ValueMember = "Value";
            this.CavatAppropriatenessGridLookUpEdit.Size = new System.Drawing.Size(342, 20);
            this.CavatAppropriatenessGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CavatAppropriatenessGridLookUpEdit.TabIndex = 91;
            this.CavatAppropriatenessGridLookUpEdit.Tag = "4";
            // 
            // sp01440ATCAVATListCTIAppropriatenessBindingSource
            // 
            this.sp01440ATCAVATListCTIAppropriatenessBindingSource.DataMember = "sp01440_AT_CAVAT_List_CTI_Appropriateness";
            this.sp01440ATCAVATListCTIAppropriatenessBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView34
            // 
            this.gridView34.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn145,
            this.gridColumn146,
            this.gridColumn147,
            this.gridColumn148});
            this.gridView34.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView34.Name = "gridView34";
            this.gridView34.OptionsCustomization.AllowFilter = false;
            this.gridView34.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView34.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView34.OptionsLayout.StoreAppearance = true;
            this.gridView34.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView34.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView34.OptionsView.ColumnAutoWidth = false;
            this.gridView34.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView34.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView34.OptionsView.ShowGroupPanel = false;
            this.gridView34.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView34.OptionsView.ShowIndicator = false;
            this.gridView34.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn146, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn145
            // 
            this.gridColumn145.Caption = "Value";
            this.gridColumn145.FieldName = "DisplayValue";
            this.gridColumn145.Name = "gridColumn145";
            this.gridColumn145.OptionsColumn.AllowEdit = false;
            this.gridColumn145.OptionsColumn.AllowFocus = false;
            this.gridColumn145.OptionsColumn.ReadOnly = true;
            this.gridColumn145.Visible = true;
            this.gridColumn145.VisibleIndex = 0;
            this.gridColumn145.Width = 219;
            // 
            // gridColumn146
            // 
            this.gridColumn146.Caption = "Order";
            this.gridColumn146.FieldName = "ItemOrder";
            this.gridColumn146.Name = "gridColumn146";
            this.gridColumn146.OptionsColumn.AllowEdit = false;
            this.gridColumn146.OptionsColumn.AllowFocus = false;
            this.gridColumn146.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn147
            // 
            this.gridColumn147.Caption = "List Type";
            this.gridColumn147.FieldName = "ListType";
            this.gridColumn147.Name = "gridColumn147";
            this.gridColumn147.OptionsColumn.AllowEdit = false;
            this.gridColumn147.OptionsColumn.AllowFocus = false;
            this.gridColumn147.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn148
            // 
            this.gridColumn148.Caption = "Actual Value";
            this.gridColumn148.FieldName = "Value";
            this.gridColumn148.Name = "gridColumn148";
            this.gridColumn148.OptionsColumn.AllowEdit = false;
            this.gridColumn148.OptionsColumn.AllowFocus = false;
            this.gridColumn148.OptionsColumn.ReadOnly = true;
            this.gridColumn148.Width = 81;
            // 
            // CavatFunctionalValueTextEdit
            // 
            this.CavatFunctionalValueTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatFunctionalValue", true));
            this.CavatFunctionalValueTextEdit.Location = new System.Drawing.Point(167, 386);
            this.CavatFunctionalValueTextEdit.MenuManager = this.barManager1;
            this.CavatFunctionalValueTextEdit.Name = "CavatFunctionalValueTextEdit";
            this.CavatFunctionalValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CavatFunctionalValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CavatFunctionalValueTextEdit.Properties.Mask.EditMask = "c";
            this.CavatFunctionalValueTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CavatFunctionalValueTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CavatFunctionalValueTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CavatFunctionalValueTextEdit, true);
            this.CavatFunctionalValueTextEdit.Size = new System.Drawing.Size(342, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CavatFunctionalValueTextEdit, optionsSpelling8);
            this.CavatFunctionalValueTextEdit.StyleController = this.dataLayoutControl1;
            this.CavatFunctionalValueTextEdit.TabIndex = 90;
            // 
            // CavatAmenityFactorsGridLookUpEdit
            // 
            this.CavatAmenityFactorsGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatAmenityFactors", true));
            this.CavatAmenityFactorsGridLookUpEdit.Location = new System.Drawing.Point(167, 456);
            this.CavatAmenityFactorsGridLookUpEdit.MenuManager = this.barManager1;
            this.CavatAmenityFactorsGridLookUpEdit.Name = "CavatAmenityFactorsGridLookUpEdit";
            this.CavatAmenityFactorsGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CavatAmenityFactorsGridLookUpEdit.Properties.DataSource = this.sp01439ATCAVATListCTIAmenityFactorsBindingSource;
            this.CavatAmenityFactorsGridLookUpEdit.Properties.DisplayMember = "DisplayValue";
            this.CavatAmenityFactorsGridLookUpEdit.Properties.NullText = "";
            this.CavatAmenityFactorsGridLookUpEdit.Properties.PopupView = this.gridView33;
            this.CavatAmenityFactorsGridLookUpEdit.Properties.ValueMember = "Value";
            this.CavatAmenityFactorsGridLookUpEdit.Size = new System.Drawing.Size(342, 20);
            this.CavatAmenityFactorsGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CavatAmenityFactorsGridLookUpEdit.TabIndex = 89;
            this.CavatAmenityFactorsGridLookUpEdit.Tag = "3";
            // 
            // sp01439ATCAVATListCTIAmenityFactorsBindingSource
            // 
            this.sp01439ATCAVATListCTIAmenityFactorsBindingSource.DataMember = "sp01439_AT_CAVAT_List_CTI_AmenityFactors";
            this.sp01439ATCAVATListCTIAmenityFactorsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView33
            // 
            this.gridView33.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn149,
            this.gridColumn150,
            this.gridColumn151,
            this.gridColumn152});
            this.gridView33.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView33.Name = "gridView33";
            this.gridView33.OptionsCustomization.AllowFilter = false;
            this.gridView33.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView33.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView33.OptionsLayout.StoreAppearance = true;
            this.gridView33.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView33.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView33.OptionsView.ColumnAutoWidth = false;
            this.gridView33.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView33.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView33.OptionsView.ShowGroupPanel = false;
            this.gridView33.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView33.OptionsView.ShowIndicator = false;
            this.gridView33.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn150, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn149
            // 
            this.gridColumn149.Caption = "Value";
            this.gridColumn149.FieldName = "DisplayValue";
            this.gridColumn149.Name = "gridColumn149";
            this.gridColumn149.OptionsColumn.AllowEdit = false;
            this.gridColumn149.OptionsColumn.AllowFocus = false;
            this.gridColumn149.OptionsColumn.ReadOnly = true;
            this.gridColumn149.Visible = true;
            this.gridColumn149.VisibleIndex = 0;
            this.gridColumn149.Width = 219;
            // 
            // gridColumn150
            // 
            this.gridColumn150.Caption = "Order";
            this.gridColumn150.FieldName = "ItemOrder";
            this.gridColumn150.Name = "gridColumn150";
            this.gridColumn150.OptionsColumn.AllowEdit = false;
            this.gridColumn150.OptionsColumn.AllowFocus = false;
            this.gridColumn150.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn151
            // 
            this.gridColumn151.Caption = "List Type";
            this.gridColumn151.FieldName = "ListType";
            this.gridColumn151.Name = "gridColumn151";
            this.gridColumn151.OptionsColumn.AllowEdit = false;
            this.gridColumn151.OptionsColumn.AllowFocus = false;
            this.gridColumn151.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn152
            // 
            this.gridColumn152.Caption = "Actual Value";
            this.gridColumn152.FieldName = "Value";
            this.gridColumn152.Name = "gridColumn152";
            this.gridColumn152.OptionsColumn.AllowEdit = false;
            this.gridColumn152.OptionsColumn.AllowFocus = false;
            this.gridColumn152.OptionsColumn.ReadOnly = true;
            this.gridColumn152.Width = 81;
            // 
            // CavatFunctionalValueFactorGridLookUpEdit
            // 
            this.CavatFunctionalValueFactorGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatFunctionalValueFactor", true));
            this.CavatFunctionalValueFactorGridLookUpEdit.Location = new System.Drawing.Point(167, 362);
            this.CavatFunctionalValueFactorGridLookUpEdit.MenuManager = this.barManager1;
            this.CavatFunctionalValueFactorGridLookUpEdit.Name = "CavatFunctionalValueFactorGridLookUpEdit";
            this.CavatFunctionalValueFactorGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CavatFunctionalValueFactorGridLookUpEdit.Properties.DataSource = this.sp01438ATCAVATListCTIFunctionalValueFactorsBindingSource;
            this.CavatFunctionalValueFactorGridLookUpEdit.Properties.DisplayMember = "DisplayValue";
            this.CavatFunctionalValueFactorGridLookUpEdit.Properties.NullText = "";
            this.CavatFunctionalValueFactorGridLookUpEdit.Properties.PopupView = this.gridView32;
            this.CavatFunctionalValueFactorGridLookUpEdit.Properties.ValueMember = "Value";
            this.CavatFunctionalValueFactorGridLookUpEdit.Size = new System.Drawing.Size(342, 20);
            this.CavatFunctionalValueFactorGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CavatFunctionalValueFactorGridLookUpEdit.TabIndex = 88;
            this.CavatFunctionalValueFactorGridLookUpEdit.Tag = "2";
            // 
            // sp01438ATCAVATListCTIFunctionalValueFactorsBindingSource
            // 
            this.sp01438ATCAVATListCTIFunctionalValueFactorsBindingSource.DataMember = "sp01438_AT_CAVAT_List_CTI_FunctionalValueFactors";
            this.sp01438ATCAVATListCTIFunctionalValueFactorsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView32
            // 
            this.gridView32.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn153,
            this.gridColumn154,
            this.gridColumn155,
            this.gridColumn156});
            this.gridView32.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView32.Name = "gridView32";
            this.gridView32.OptionsCustomization.AllowFilter = false;
            this.gridView32.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView32.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView32.OptionsLayout.StoreAppearance = true;
            this.gridView32.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView32.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView32.OptionsView.ColumnAutoWidth = false;
            this.gridView32.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView32.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView32.OptionsView.ShowGroupPanel = false;
            this.gridView32.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView32.OptionsView.ShowIndicator = false;
            this.gridView32.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn154, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn153
            // 
            this.gridColumn153.Caption = "Value";
            this.gridColumn153.FieldName = "DisplayValue";
            this.gridColumn153.Name = "gridColumn153";
            this.gridColumn153.OptionsColumn.AllowEdit = false;
            this.gridColumn153.OptionsColumn.AllowFocus = false;
            this.gridColumn153.OptionsColumn.ReadOnly = true;
            this.gridColumn153.Visible = true;
            this.gridColumn153.VisibleIndex = 0;
            this.gridColumn153.Width = 219;
            // 
            // gridColumn154
            // 
            this.gridColumn154.Caption = "Order";
            this.gridColumn154.FieldName = "ItemOrder";
            this.gridColumn154.Name = "gridColumn154";
            this.gridColumn154.OptionsColumn.AllowEdit = false;
            this.gridColumn154.OptionsColumn.AllowFocus = false;
            this.gridColumn154.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn155
            // 
            this.gridColumn155.Caption = "List Type";
            this.gridColumn155.FieldName = "ListType";
            this.gridColumn155.Name = "gridColumn155";
            this.gridColumn155.OptionsColumn.AllowEdit = false;
            this.gridColumn155.OptionsColumn.AllowFocus = false;
            this.gridColumn155.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn156
            // 
            this.gridColumn156.Caption = "Actual Value";
            this.gridColumn156.FieldName = "Value";
            this.gridColumn156.Name = "gridColumn156";
            this.gridColumn156.OptionsColumn.AllowEdit = false;
            this.gridColumn156.OptionsColumn.AllowFocus = false;
            this.gridColumn156.OptionsColumn.ReadOnly = true;
            this.gridColumn156.Width = 81;
            // 
            // CavatCTIValueTextEdit
            // 
            this.CavatCTIValueTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatCTIValue", true));
            this.CavatCTIValueTextEdit.Location = new System.Drawing.Point(167, 292);
            this.CavatCTIValueTextEdit.MenuManager = this.barManager1;
            this.CavatCTIValueTextEdit.Name = "CavatCTIValueTextEdit";
            this.CavatCTIValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CavatCTIValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CavatCTIValueTextEdit.Properties.Mask.EditMask = "c";
            this.CavatCTIValueTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CavatCTIValueTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CavatCTIValueTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CavatCTIValueTextEdit, true);
            this.CavatCTIValueTextEdit.Size = new System.Drawing.Size(342, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CavatCTIValueTextEdit, optionsSpelling9);
            this.CavatCTIValueTextEdit.StyleController = this.dataLayoutControl1;
            this.CavatCTIValueTextEdit.TabIndex = 87;
            // 
            // CavatAccessibilityGridLookUpEdit
            // 
            this.CavatAccessibilityGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatAccessibility", true));
            this.CavatAccessibilityGridLookUpEdit.Location = new System.Drawing.Point(167, 268);
            this.CavatAccessibilityGridLookUpEdit.MenuManager = this.barManager1;
            this.CavatAccessibilityGridLookUpEdit.Name = "CavatAccessibilityGridLookUpEdit";
            this.CavatAccessibilityGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CavatAccessibilityGridLookUpEdit.Properties.DataSource = this.sp01437ATCAVATListCTIAccessibilityBindingSource;
            this.CavatAccessibilityGridLookUpEdit.Properties.DisplayMember = "DisplayValue";
            this.CavatAccessibilityGridLookUpEdit.Properties.NullText = "";
            this.CavatAccessibilityGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit2View;
            this.CavatAccessibilityGridLookUpEdit.Properties.ValueMember = "Value";
            this.CavatAccessibilityGridLookUpEdit.Size = new System.Drawing.Size(342, 20);
            this.CavatAccessibilityGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CavatAccessibilityGridLookUpEdit.TabIndex = 86;
            this.CavatAccessibilityGridLookUpEdit.Tag = "1";
            // 
            // sp01437ATCAVATListCTIAccessibilityBindingSource
            // 
            this.sp01437ATCAVATListCTIAccessibilityBindingSource.DataMember = "sp01437_AT_CAVAT_List_CTI_Accessibility";
            this.sp01437ATCAVATListCTIAccessibilityBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridLookUpEdit2View
            // 
            this.gridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn137,
            this.gridColumn138,
            this.gridColumn139,
            this.gridColumn140});
            this.gridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit2View.Name = "gridLookUpEdit2View";
            this.gridLookUpEdit2View.OptionsCustomization.AllowFilter = false;
            this.gridLookUpEdit2View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit2View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn138, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn137
            // 
            this.gridColumn137.Caption = "Value";
            this.gridColumn137.FieldName = "DisplayValue";
            this.gridColumn137.Name = "gridColumn137";
            this.gridColumn137.OptionsColumn.AllowEdit = false;
            this.gridColumn137.OptionsColumn.AllowFocus = false;
            this.gridColumn137.OptionsColumn.ReadOnly = true;
            this.gridColumn137.Visible = true;
            this.gridColumn137.VisibleIndex = 0;
            this.gridColumn137.Width = 219;
            // 
            // gridColumn138
            // 
            this.gridColumn138.Caption = "Order";
            this.gridColumn138.FieldName = "ItemOrder";
            this.gridColumn138.Name = "gridColumn138";
            this.gridColumn138.OptionsColumn.AllowEdit = false;
            this.gridColumn138.OptionsColumn.AllowFocus = false;
            this.gridColumn138.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn139
            // 
            this.gridColumn139.Caption = "List Type";
            this.gridColumn139.FieldName = "ListType";
            this.gridColumn139.Name = "gridColumn139";
            this.gridColumn139.OptionsColumn.AllowEdit = false;
            this.gridColumn139.OptionsColumn.AllowFocus = false;
            this.gridColumn139.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn140
            // 
            this.gridColumn140.Caption = "Actual Value";
            this.gridColumn140.FieldName = "Value";
            this.gridColumn140.Name = "gridColumn140";
            this.gridColumn140.OptionsColumn.AllowEdit = false;
            this.gridColumn140.OptionsColumn.AllowFocus = false;
            this.gridColumn140.OptionsColumn.ReadOnly = true;
            this.gridColumn140.Width = 81;
            // 
            // CavatCTIFactorGridLookUpEdit
            // 
            this.CavatCTIFactorGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatCTIFactor", true));
            this.CavatCTIFactorGridLookUpEdit.Location = new System.Drawing.Point(167, 244);
            this.CavatCTIFactorGridLookUpEdit.MenuManager = this.barManager1;
            this.CavatCTIFactorGridLookUpEdit.Name = "CavatCTIFactorGridLookUpEdit";
            this.CavatCTIFactorGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CavatCTIFactorGridLookUpEdit.Properties.DataSource = this.sp01436ATCAVATListCTIFactorsBindingSource;
            this.CavatCTIFactorGridLookUpEdit.Properties.DisplayMember = "DisplayValue";
            this.CavatCTIFactorGridLookUpEdit.Properties.NullText = "";
            this.CavatCTIFactorGridLookUpEdit.Properties.PopupView = this.gridView31;
            this.CavatCTIFactorGridLookUpEdit.Properties.ValueMember = "Value";
            this.CavatCTIFactorGridLookUpEdit.Size = new System.Drawing.Size(342, 20);
            this.CavatCTIFactorGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CavatCTIFactorGridLookUpEdit.TabIndex = 85;
            this.CavatCTIFactorGridLookUpEdit.Tag = "0";
            // 
            // sp01436ATCAVATListCTIFactorsBindingSource
            // 
            this.sp01436ATCAVATListCTIFactorsBindingSource.DataMember = "sp01436_AT_CAVAT_List_CTI_Factors";
            this.sp01436ATCAVATListCTIFactorsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView31
            // 
            this.gridView31.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDisplayValue,
            this.colItemOrder,
            this.colListType,
            this.colValue});
            this.gridView31.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView31.Name = "gridView31";
            this.gridView31.OptionsCustomization.AllowFilter = false;
            this.gridView31.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView31.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView31.OptionsLayout.StoreAppearance = true;
            this.gridView31.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView31.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView31.OptionsView.ColumnAutoWidth = false;
            this.gridView31.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView31.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView31.OptionsView.ShowGroupPanel = false;
            this.gridView31.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView31.OptionsView.ShowIndicator = false;
            this.gridView31.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDisplayValue
            // 
            this.colDisplayValue.Caption = "Value";
            this.colDisplayValue.FieldName = "DisplayValue";
            this.colDisplayValue.Name = "colDisplayValue";
            this.colDisplayValue.OptionsColumn.AllowEdit = false;
            this.colDisplayValue.OptionsColumn.AllowFocus = false;
            this.colDisplayValue.OptionsColumn.ReadOnly = true;
            this.colDisplayValue.Visible = true;
            this.colDisplayValue.VisibleIndex = 0;
            this.colDisplayValue.Width = 219;
            // 
            // colItemOrder
            // 
            this.colItemOrder.Caption = "Order";
            this.colItemOrder.FieldName = "ItemOrder";
            this.colItemOrder.Name = "colItemOrder";
            this.colItemOrder.OptionsColumn.AllowEdit = false;
            this.colItemOrder.OptionsColumn.AllowFocus = false;
            this.colItemOrder.OptionsColumn.ReadOnly = true;
            // 
            // colListType
            // 
            this.colListType.Caption = "List Type";
            this.colListType.FieldName = "ListType";
            this.colListType.Name = "colListType";
            this.colListType.OptionsColumn.AllowEdit = false;
            this.colListType.OptionsColumn.AllowFocus = false;
            this.colListType.OptionsColumn.ReadOnly = true;
            // 
            // colValue
            // 
            this.colValue.Caption = "Actual Value";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            this.colValue.Width = 81;
            // 
            // CavatBasicValueTextEdit
            // 
            this.CavatBasicValueTextEdit.Location = new System.Drawing.Point(167, 174);
            this.CavatBasicValueTextEdit.MenuManager = this.barManager1;
            this.CavatBasicValueTextEdit.Name = "CavatBasicValueTextEdit";
            this.CavatBasicValueTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CavatBasicValueTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CavatBasicValueTextEdit.Properties.Mask.EditMask = "c";
            this.CavatBasicValueTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CavatBasicValueTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CavatBasicValueTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CavatBasicValueTextEdit, true);
            this.CavatBasicValueTextEdit.Size = new System.Drawing.Size(342, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CavatBasicValueTextEdit, optionsSpelling10);
            this.CavatBasicValueTextEdit.StyleController = this.dataLayoutControl1;
            this.CavatBasicValueTextEdit.TabIndex = 84;
            // 
            // CavatUnitValueFactorSpinEdit
            // 
            this.CavatUnitValueFactorSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CavatUnitValueFactor", true));
            this.CavatUnitValueFactorSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CavatUnitValueFactorSpinEdit.Location = new System.Drawing.Point(167, 150);
            this.CavatUnitValueFactorSpinEdit.MenuManager = this.barManager1;
            this.CavatUnitValueFactorSpinEdit.Name = "CavatUnitValueFactorSpinEdit";
            this.CavatUnitValueFactorSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CavatUnitValueFactorSpinEdit.Properties.Mask.EditMask = "f2";
            this.CavatUnitValueFactorSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CavatUnitValueFactorSpinEdit.Size = new System.Drawing.Size(342, 20);
            this.CavatUnitValueFactorSpinEdit.StyleController = this.dataLayoutControl1;
            this.CavatUnitValueFactorSpinEdit.TabIndex = 83;
            this.CavatUnitValueFactorSpinEdit.ToolTip = "Default Value supplied by System Settings";
            this.CavatUnitValueFactorSpinEdit.EditValueChanged += new System.EventHandler(this.CavatUnitValueFactorSpinEdit_EditValueChanged);
            this.CavatUnitValueFactorSpinEdit.Validated += new System.EventHandler(this.CavatUnitValueFactorSpinEdit_Validated);
            // 
            // CavatStemDiameterSpinEdit
            // 
            this.CavatStemDiameterSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intDBH", true));
            this.CavatStemDiameterSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CavatStemDiameterSpinEdit.Location = new System.Drawing.Point(167, 126);
            this.CavatStemDiameterSpinEdit.MenuManager = this.barManager1;
            this.CavatStemDiameterSpinEdit.Name = "CavatStemDiameterSpinEdit";
            this.CavatStemDiameterSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CavatStemDiameterSpinEdit.Properties.IsFloatValue = false;
            this.CavatStemDiameterSpinEdit.Properties.Mask.EditMask = "N00";
            this.CavatStemDiameterSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.CavatStemDiameterSpinEdit.Properties.ReadOnly = true;
            this.CavatStemDiameterSpinEdit.Size = new System.Drawing.Size(342, 20);
            this.CavatStemDiameterSpinEdit.StyleController = this.dataLayoutControl1;
            this.CavatStemDiameterSpinEdit.TabIndex = 82;
            this.CavatStemDiameterSpinEdit.ToolTip = "Value taken from DBH Box";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Grid = this.gridControl4;
            this.gridSplitContainer3.Location = new System.Drawing.Point(24, 58);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer3.Size = new System.Drawing.Size(1080, 618);
            this.gridSplitContainer3.TabIndex = 102;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView30;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemHyperLinkEdit2});
            this.gridControl4.Size = new System.Drawing.Size(1080, 618);
            this.gridControl4.TabIndex = 81;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView30});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView30
            // 
            this.gridView30.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.gridColumn136,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView30.GridControl = this.gridControl4;
            this.gridView30.GroupCount = 1;
            this.gridView30.Name = "gridView30";
            this.gridView30.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView30.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView30.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView30.OptionsLayout.StoreAppearance = true;
            this.gridView30.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView30.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView30.OptionsSelection.MultiSelect = true;
            this.gridView30.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView30.OptionsView.ColumnAutoWidth = false;
            this.gridView30.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView30.OptionsView.ShowGroupPanel = false;
            this.gridView30.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn136, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView30.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView30.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView30.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView30.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView30.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView30.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView30_MouseUp);
            this.gridView30.DoubleClick += new System.EventHandler(this.gridView30_DoubleClick);
            this.gridView30.GotFocus += new System.EventHandler(this.gridView30_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // gridColumn136
            // 
            this.gridColumn136.Caption = "Description";
            this.gridColumn136.FieldName = "Description";
            this.gridColumn136.Name = "gridColumn136";
            this.gridColumn136.OptionsColumn.AllowEdit = false;
            this.gridColumn136.OptionsColumn.AllowFocus = false;
            this.gridColumn136.OptionsColumn.ReadOnly = true;
            this.gridColumn136.Visible = true;
            this.gridColumn136.VisibleIndex = 1;
            this.gridColumn136.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Grid = this.gridControl3;
            this.gridSplitContainer2.Location = new System.Drawing.Point(36, 396);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1056, 268);
            this.gridSplitContainer2.TabIndex = 101;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp01380ATActionsLinkedToInspectionsBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView29;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit4,
            this.repositoryItemMemoExEdit4});
            this.gridControl3.Size = new System.Drawing.Size(1056, 268);
            this.gridControl3.TabIndex = 80;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView29});
            // 
            // sp01380ATActionsLinkedToInspectionsBindingSource
            // 
            this.sp01380ATActionsLinkedToInspectionsBindingSource.DataMember = "sp01380_AT_Actions_Linked_To_Inspections";
            this.sp01380ATActionsLinkedToInspectionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView29
            // 
            this.gridView29.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID1,
            this.colTreeID1,
            this.colSiteID2,
            this.colClientID2,
            this.colTreeReference1,
            this.colTreeMappingID1,
            this.colInspectionReference1,
            this.colInspectionDate1,
            this.colActionID,
            this.colActionJobNumber,
            this.colAction,
            this.colActionPriority,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colActionBy,
            this.colActionSupervisor,
            this.colActionOwnership,
            this.colActionCostCentre,
            this.colActionBudget,
            this.colActionWorkUnits,
            this.colActionScheduleOfRates,
            this.colActionBudgetedRateDescription,
            this.colActionBudgetedRate,
            this.colActionBudgetedCost,
            this.colActionActualRateDescription,
            this.colActionActualRate,
            this.colActionActualCost,
            this.colActionWorkOrder,
            this.colActionRemarks,
            this.colActionUserDefined1,
            this.colActionUserDefined2,
            this.colActionUserDefined3,
            this.colActionCostDifference,
            this.colActionCompletionStatus,
            this.colActionTimeliness,
            this.colDiscountRate});
            this.gridView29.GridControl = this.gridControl3;
            this.gridView29.GroupCount = 2;
            this.gridView29.Name = "gridView29";
            this.gridView29.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView29.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView29.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView29.OptionsLayout.StoreAppearance = true;
            this.gridView29.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView29.OptionsSelection.MultiSelect = true;
            this.gridView29.OptionsView.ColumnAutoWidth = false;
            this.gridView29.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView29.OptionsView.ShowGroupPanel = false;
            this.gridView29.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionDate1, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDueDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView29.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView29.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView29.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView29.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView29.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView29.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView29_MouseUp);
            this.gridView29.DoubleClick += new System.EventHandler(this.gridView29_DoubleClick);
            this.gridView29.GotFocus += new System.EventHandler(this.gridView29_GotFocus);
            // 
            // colInspectionID1
            // 
            this.colInspectionID1.Caption = "Inspection ID";
            this.colInspectionID1.FieldName = "InspectionID";
            this.colInspectionID1.Name = "colInspectionID1";
            this.colInspectionID1.OptionsColumn.AllowEdit = false;
            this.colInspectionID1.OptionsColumn.AllowFocus = false;
            this.colInspectionID1.OptionsColumn.ReadOnly = true;
            this.colInspectionID1.Width = 76;
            // 
            // colTreeID1
            // 
            this.colTreeID1.Caption = "Tree ID";
            this.colTreeID1.FieldName = "TreeID";
            this.colTreeID1.Name = "colTreeID1";
            this.colTreeID1.OptionsColumn.AllowEdit = false;
            this.colTreeID1.OptionsColumn.AllowFocus = false;
            this.colTreeID1.OptionsColumn.ReadOnly = true;
            this.colTreeID1.Width = 48;
            // 
            // colSiteID2
            // 
            this.colSiteID2.Caption = "Site ID";
            this.colSiteID2.FieldName = "SiteID";
            this.colSiteID2.Name = "colSiteID2";
            this.colSiteID2.OptionsColumn.AllowEdit = false;
            this.colSiteID2.OptionsColumn.AllowFocus = false;
            this.colSiteID2.OptionsColumn.ReadOnly = true;
            this.colSiteID2.Width = 62;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            this.colClientID2.Width = 59;
            // 
            // colTreeReference1
            // 
            this.colTreeReference1.Caption = "Tree Reference";
            this.colTreeReference1.FieldName = "TreeReference";
            this.colTreeReference1.Name = "colTreeReference1";
            this.colTreeReference1.OptionsColumn.AllowEdit = false;
            this.colTreeReference1.OptionsColumn.AllowFocus = false;
            this.colTreeReference1.OptionsColumn.ReadOnly = true;
            this.colTreeReference1.Width = 97;
            // 
            // colTreeMappingID1
            // 
            this.colTreeMappingID1.Caption = "Map ID";
            this.colTreeMappingID1.FieldName = "TreeMappingID";
            this.colTreeMappingID1.Name = "colTreeMappingID1";
            this.colTreeMappingID1.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID1.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID1.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID1.Width = 46;
            // 
            // colInspectionReference1
            // 
            this.colInspectionReference1.Caption = "Inspection Reference";
            this.colInspectionReference1.FieldName = "InspectionReference";
            this.colInspectionReference1.Name = "colInspectionReference1";
            this.colInspectionReference1.OptionsColumn.AllowEdit = false;
            this.colInspectionReference1.OptionsColumn.AllowFocus = false;
            this.colInspectionReference1.OptionsColumn.ReadOnly = true;
            this.colInspectionReference1.Visible = true;
            this.colInspectionReference1.VisibleIndex = 0;
            this.colInspectionReference1.Width = 125;
            // 
            // colInspectionDate1
            // 
            this.colInspectionDate1.Caption = "Inspection Date";
            this.colInspectionDate1.FieldName = "InspectionDate";
            this.colInspectionDate1.Name = "colInspectionDate1";
            this.colInspectionDate1.OptionsColumn.AllowEdit = false;
            this.colInspectionDate1.OptionsColumn.AllowFocus = false;
            this.colInspectionDate1.OptionsColumn.ReadOnly = true;
            this.colInspectionDate1.Width = 98;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            this.colActionID.Width = 56;
            // 
            // colActionJobNumber
            // 
            this.colActionJobNumber.Caption = "Job Number";
            this.colActionJobNumber.FieldName = "ActionJobNumber";
            this.colActionJobNumber.Name = "colActionJobNumber";
            this.colActionJobNumber.OptionsColumn.AllowEdit = false;
            this.colActionJobNumber.OptionsColumn.AllowFocus = false;
            this.colActionJobNumber.OptionsColumn.ReadOnly = true;
            this.colActionJobNumber.Visible = true;
            this.colActionJobNumber.VisibleIndex = 1;
            this.colActionJobNumber.Width = 106;
            // 
            // colAction
            // 
            this.colAction.Caption = "Action";
            this.colAction.FieldName = "Action";
            this.colAction.Name = "colAction";
            this.colAction.OptionsColumn.AllowEdit = false;
            this.colAction.OptionsColumn.AllowFocus = false;
            this.colAction.OptionsColumn.ReadOnly = true;
            this.colAction.Visible = true;
            this.colAction.VisibleIndex = 2;
            this.colAction.Width = 114;
            // 
            // colActionPriority
            // 
            this.colActionPriority.Caption = "Priority";
            this.colActionPriority.FieldName = "ActionPriority";
            this.colActionPriority.Name = "colActionPriority";
            this.colActionPriority.OptionsColumn.AllowEdit = false;
            this.colActionPriority.OptionsColumn.AllowFocus = false;
            this.colActionPriority.OptionsColumn.ReadOnly = true;
            this.colActionPriority.Visible = true;
            this.colActionPriority.VisibleIndex = 3;
            this.colActionPriority.Width = 119;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Due Date";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 4;
            this.colActionDueDate.Width = 88;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Done Date";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 5;
            this.colActionDoneDate.Width = 79;
            // 
            // colActionBy
            // 
            this.colActionBy.Caption = "Action By";
            this.colActionBy.FieldName = "ActionBy";
            this.colActionBy.Name = "colActionBy";
            this.colActionBy.OptionsColumn.AllowEdit = false;
            this.colActionBy.OptionsColumn.AllowFocus = false;
            this.colActionBy.OptionsColumn.ReadOnly = true;
            this.colActionBy.Visible = true;
            this.colActionBy.VisibleIndex = 8;
            this.colActionBy.Width = 126;
            // 
            // colActionSupervisor
            // 
            this.colActionSupervisor.Caption = "Supervisor";
            this.colActionSupervisor.FieldName = "ActionSupervisor";
            this.colActionSupervisor.Name = "colActionSupervisor";
            this.colActionSupervisor.OptionsColumn.AllowEdit = false;
            this.colActionSupervisor.OptionsColumn.AllowFocus = false;
            this.colActionSupervisor.OptionsColumn.ReadOnly = true;
            this.colActionSupervisor.Visible = true;
            this.colActionSupervisor.VisibleIndex = 9;
            this.colActionSupervisor.Width = 125;
            // 
            // colActionOwnership
            // 
            this.colActionOwnership.Caption = "Ownership";
            this.colActionOwnership.FieldName = "ActionOwnership";
            this.colActionOwnership.Name = "colActionOwnership";
            this.colActionOwnership.OptionsColumn.AllowEdit = false;
            this.colActionOwnership.OptionsColumn.AllowFocus = false;
            this.colActionOwnership.OptionsColumn.ReadOnly = true;
            this.colActionOwnership.Visible = true;
            this.colActionOwnership.VisibleIndex = 10;
            this.colActionOwnership.Width = 118;
            // 
            // colActionCostCentre
            // 
            this.colActionCostCentre.Caption = "Cost Centre";
            this.colActionCostCentre.FieldName = "ActionCostCentre";
            this.colActionCostCentre.Name = "colActionCostCentre";
            this.colActionCostCentre.OptionsColumn.AllowEdit = false;
            this.colActionCostCentre.OptionsColumn.AllowFocus = false;
            this.colActionCostCentre.OptionsColumn.ReadOnly = true;
            this.colActionCostCentre.Visible = true;
            this.colActionCostCentre.VisibleIndex = 11;
            this.colActionCostCentre.Width = 124;
            // 
            // colActionBudget
            // 
            this.colActionBudget.Caption = "Budget";
            this.colActionBudget.FieldName = "ActionBudget";
            this.colActionBudget.Name = "colActionBudget";
            this.colActionBudget.OptionsColumn.AllowEdit = false;
            this.colActionBudget.OptionsColumn.AllowFocus = false;
            this.colActionBudget.OptionsColumn.ReadOnly = true;
            this.colActionBudget.Visible = true;
            this.colActionBudget.VisibleIndex = 12;
            this.colActionBudget.Width = 111;
            // 
            // colActionWorkUnits
            // 
            this.colActionWorkUnits.Caption = "Work Units";
            this.colActionWorkUnits.ColumnEdit = this.repositoryItemTextEdit1;
            this.colActionWorkUnits.FieldName = "ActionWorkUnits";
            this.colActionWorkUnits.Name = "colActionWorkUnits";
            this.colActionWorkUnits.OptionsColumn.AllowEdit = false;
            this.colActionWorkUnits.OptionsColumn.AllowFocus = false;
            this.colActionWorkUnits.OptionsColumn.ReadOnly = true;
            this.colActionWorkUnits.Visible = true;
            this.colActionWorkUnits.VisibleIndex = 13;
            this.colActionWorkUnits.Width = 74;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "f2";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colActionScheduleOfRates
            // 
            this.colActionScheduleOfRates.Caption = "Schedule Of Rates";
            this.colActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.colActionScheduleOfRates.Name = "colActionScheduleOfRates";
            this.colActionScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colActionScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colActionScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colActionScheduleOfRates.Visible = true;
            this.colActionScheduleOfRates.VisibleIndex = 14;
            this.colActionScheduleOfRates.Width = 111;
            // 
            // colActionBudgetedRateDescription
            // 
            this.colActionBudgetedRateDescription.Caption = "Budgeted Rate Desc";
            this.colActionBudgetedRateDescription.FieldName = "ActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.Name = "colActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRateDescription.Visible = true;
            this.colActionBudgetedRateDescription.VisibleIndex = 15;
            this.colActionBudgetedRateDescription.Width = 165;
            // 
            // colActionBudgetedRate
            // 
            this.colActionBudgetedRate.Caption = "Budgeted Rate";
            this.colActionBudgetedRate.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionBudgetedRate.FieldName = "ActionBudgetedRate";
            this.colActionBudgetedRate.Name = "colActionBudgetedRate";
            this.colActionBudgetedRate.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRate.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRate.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRate.Visible = true;
            this.colActionBudgetedRate.VisibleIndex = 16;
            this.colActionBudgetedRate.Width = 94;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colActionBudgetedCost
            // 
            this.colActionBudgetedCost.Caption = "Budgeted Cost";
            this.colActionBudgetedCost.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionBudgetedCost.FieldName = "ActionBudgetedCost";
            this.colActionBudgetedCost.Name = "colActionBudgetedCost";
            this.colActionBudgetedCost.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedCost.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedCost.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedCost.Visible = true;
            this.colActionBudgetedCost.VisibleIndex = 17;
            this.colActionBudgetedCost.Width = 93;
            // 
            // colActionActualRateDescription
            // 
            this.colActionActualRateDescription.Caption = "Actual Rate Desc";
            this.colActionActualRateDescription.FieldName = "ActionActualRateDescription";
            this.colActionActualRateDescription.Name = "colActionActualRateDescription";
            this.colActionActualRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionActualRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionActualRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionActualRateDescription.Visible = true;
            this.colActionActualRateDescription.VisibleIndex = 18;
            this.colActionActualRateDescription.Width = 148;
            // 
            // colActionActualRate
            // 
            this.colActionActualRate.Caption = "Actual Rate";
            this.colActionActualRate.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionActualRate.FieldName = "ActionActualRate";
            this.colActionActualRate.Name = "colActionActualRate";
            this.colActionActualRate.OptionsColumn.AllowEdit = false;
            this.colActionActualRate.OptionsColumn.AllowFocus = false;
            this.colActionActualRate.OptionsColumn.ReadOnly = true;
            this.colActionActualRate.Visible = true;
            this.colActionActualRate.VisibleIndex = 19;
            this.colActionActualRate.Width = 78;
            // 
            // colActionActualCost
            // 
            this.colActionActualCost.Caption = "Actual Cost";
            this.colActionActualCost.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionActualCost.FieldName = "ActionActualCost";
            this.colActionActualCost.Name = "colActionActualCost";
            this.colActionActualCost.OptionsColumn.AllowEdit = false;
            this.colActionActualCost.OptionsColumn.AllowFocus = false;
            this.colActionActualCost.OptionsColumn.ReadOnly = true;
            this.colActionActualCost.Visible = true;
            this.colActionActualCost.VisibleIndex = 21;
            this.colActionActualCost.Width = 77;
            // 
            // colActionWorkOrder
            // 
            this.colActionWorkOrder.FieldName = "ActionWorkOrder";
            this.colActionWorkOrder.Name = "colActionWorkOrder";
            this.colActionWorkOrder.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrder.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrder.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrder.Visible = true;
            this.colActionWorkOrder.VisibleIndex = 23;
            this.colActionWorkOrder.Width = 111;
            // 
            // colActionRemarks
            // 
            this.colActionRemarks.Caption = "Remarks";
            this.colActionRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colActionRemarks.FieldName = "ActionRemarks";
            this.colActionRemarks.Name = "colActionRemarks";
            this.colActionRemarks.OptionsColumn.ReadOnly = true;
            this.colActionRemarks.Visible = true;
            this.colActionRemarks.VisibleIndex = 24;
            this.colActionRemarks.Width = 63;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colActionUserDefined1
            // 
            this.colActionUserDefined1.Caption = "User 1";
            this.colActionUserDefined1.FieldName = "ActionUserDefined1";
            this.colActionUserDefined1.Name = "colActionUserDefined1";
            this.colActionUserDefined1.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined1.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined1.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined1.Visible = true;
            this.colActionUserDefined1.VisibleIndex = 25;
            this.colActionUserDefined1.Width = 53;
            // 
            // colActionUserDefined2
            // 
            this.colActionUserDefined2.Caption = "User 2";
            this.colActionUserDefined2.FieldName = "ActionUserDefined2";
            this.colActionUserDefined2.Name = "colActionUserDefined2";
            this.colActionUserDefined2.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined2.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined2.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined2.Visible = true;
            this.colActionUserDefined2.VisibleIndex = 26;
            this.colActionUserDefined2.Width = 53;
            // 
            // colActionUserDefined3
            // 
            this.colActionUserDefined3.Caption = "User 3";
            this.colActionUserDefined3.FieldName = "ActionUserDefined3";
            this.colActionUserDefined3.Name = "colActionUserDefined3";
            this.colActionUserDefined3.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined3.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined3.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined3.Visible = true;
            this.colActionUserDefined3.VisibleIndex = 27;
            this.colActionUserDefined3.Width = 53;
            // 
            // colActionCostDifference
            // 
            this.colActionCostDifference.Caption = "Cost Difference";
            this.colActionCostDifference.FieldName = "ActionCostDifference";
            this.colActionCostDifference.Name = "colActionCostDifference";
            this.colActionCostDifference.OptionsColumn.AllowEdit = false;
            this.colActionCostDifference.OptionsColumn.AllowFocus = false;
            this.colActionCostDifference.OptionsColumn.ReadOnly = true;
            this.colActionCostDifference.Visible = true;
            this.colActionCostDifference.VisibleIndex = 22;
            this.colActionCostDifference.Width = 97;
            // 
            // colActionCompletionStatus
            // 
            this.colActionCompletionStatus.Caption = "Completion Status";
            this.colActionCompletionStatus.FieldName = "ActionCompletionStatus";
            this.colActionCompletionStatus.Name = "colActionCompletionStatus";
            this.colActionCompletionStatus.OptionsColumn.AllowEdit = false;
            this.colActionCompletionStatus.OptionsColumn.AllowFocus = false;
            this.colActionCompletionStatus.OptionsColumn.ReadOnly = true;
            this.colActionCompletionStatus.Visible = true;
            this.colActionCompletionStatus.VisibleIndex = 7;
            this.colActionCompletionStatus.Width = 109;
            // 
            // colActionTimeliness
            // 
            this.colActionTimeliness.Caption = "Timeliness";
            this.colActionTimeliness.FieldName = "ActionTimeliness";
            this.colActionTimeliness.Name = "colActionTimeliness";
            this.colActionTimeliness.OptionsColumn.AllowEdit = false;
            this.colActionTimeliness.OptionsColumn.AllowFocus = false;
            this.colActionTimeliness.OptionsColumn.ReadOnly = true;
            this.colActionTimeliness.Visible = true;
            this.colActionTimeliness.VisibleIndex = 6;
            this.colActionTimeliness.Width = 70;
            // 
            // colDiscountRate
            // 
            this.colDiscountRate.Caption = "Discount Rate";
            this.colDiscountRate.ColumnEdit = this.repositoryItemTextEdit4;
            this.colDiscountRate.FieldName = "DiscountRate";
            this.colDiscountRate.Name = "colDiscountRate";
            this.colDiscountRate.OptionsColumn.AllowEdit = false;
            this.colDiscountRate.OptionsColumn.AllowFocus = false;
            this.colDiscountRate.OptionsColumn.ReadOnly = true;
            this.colDiscountRate.Visible = true;
            this.colDiscountRate.VisibleIndex = 20;
            this.colDiscountRate.Width = 89;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "P2";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(36, 174);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1056, 212);
            this.gridSplitContainer1.TabIndex = 100;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01367ATInspectionsForTreesBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView25;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1056, 212);
            this.gridControl1.TabIndex = 74;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView25});
            // 
            // sp01367ATInspectionsForTreesBindingSource
            // 
            this.sp01367ATInspectionsForTreesBindingSource.DataMember = "sp01367_AT_Inspections_For_Trees";
            this.sp01367ATInspectionsForTreesBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView25
            // 
            this.gridView25.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID,
            this.gridColumn132,
            this.colLocalityID1,
            this.colDistrictID1,
            this.colLocalityName1,
            this.colDistrictName1,
            this.colTreeReference2,
            this.colTreeMappingID,
            this.colTreeSpeciesName,
            this.colTreeX,
            this.colTreeY,
            this.colTreePolygonXY,
            this.colInspectionReference,
            this.colInspectionInspector,
            this.colInspectionDate,
            this.colInspectionIncidentReference,
            this.colInspectionIncidentDate,
            this.colIncidentID,
            this.colInspectionAngleToVertical,
            this.colInspectionLastModified,
            this.colInspectionUserDefined1,
            this.colInspectionUserDefined2,
            this.colInspectionUserDefined3,
            this.colInspectionRemarks,
            this.colInspectionStemPhysical1,
            this.colInspectionStemPhysical2,
            this.colInspectionStemPhysical3,
            this.colInspectionStemDisease1,
            this.colInspectionStemDisease2,
            this.colInspectionStemDisease3,
            this.colInspectionCrownPhysical1,
            this.colInspectionCrownPhysical2,
            this.colInspectionCrownPhysical3,
            this.colInspectionCrownDisease1,
            this.colInspectionCrownDisease2,
            this.colInspectionCrownDisease3,
            this.colInspectionCrownFoliation1,
            this.colInspectionCrownFoliation2,
            this.colInspectionCrownFoliation3,
            this.colInspectionRiskCategory,
            this.colInspectionGeneralCondition,
            this.colInspectionBasePhysical1,
            this.colInspectionBasePhysical2,
            this.colInspectionBasePhysical3,
            this.colInspectionVitality,
            this.colLinkedActionCount,
            this.colLinkedOutstandingActionCount,
            this.colNoFurtherActionRequired,
            this.gridColumn133,
            this.gridColumn134,
            this.gridColumn135});
            this.gridView25.GridControl = this.gridControl1;
            this.gridView25.GroupCount = 1;
            this.gridView25.Name = "gridView25";
            this.gridView25.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView25.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView25.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView25.OptionsLayout.StoreAppearance = true;
            this.gridView25.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView25.OptionsSelection.MultiSelect = true;
            this.gridView25.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView25.OptionsView.ColumnAutoWidth = false;
            this.gridView25.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView25.OptionsView.ShowGroupPanel = false;
            this.gridView25.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView25.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView25_CustomRowCellEdit);
            this.gridView25.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView25.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView25.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView25.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView25_ShowingEditor);
            this.gridView25.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView25.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView25.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView25_MouseUp);
            this.gridView25.DoubleClick += new System.EventHandler(this.gridView25_DoubleClick);
            this.gridView25.GotFocus += new System.EventHandler(this.gridView25_GotFocus);
            // 
            // colInspectionID
            // 
            this.colInspectionID.Caption = "Inspection ID";
            this.colInspectionID.FieldName = "InspectionID";
            this.colInspectionID.Name = "colInspectionID";
            this.colInspectionID.OptionsColumn.AllowEdit = false;
            this.colInspectionID.OptionsColumn.AllowFocus = false;
            this.colInspectionID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn132
            // 
            this.gridColumn132.Caption = "Tree ID";
            this.gridColumn132.FieldName = "TreeID";
            this.gridColumn132.Name = "gridColumn132";
            this.gridColumn132.OptionsColumn.AllowEdit = false;
            this.gridColumn132.OptionsColumn.AllowFocus = false;
            this.gridColumn132.OptionsColumn.ReadOnly = true;
            this.gridColumn132.Width = 47;
            // 
            // colLocalityID1
            // 
            this.colLocalityID1.Caption = "Locality ID";
            this.colLocalityID1.FieldName = "LocalityID";
            this.colLocalityID1.Name = "colLocalityID1";
            this.colLocalityID1.OptionsColumn.AllowEdit = false;
            this.colLocalityID1.OptionsColumn.AllowFocus = false;
            this.colLocalityID1.OptionsColumn.ReadOnly = true;
            this.colLocalityID1.Width = 61;
            // 
            // colDistrictID1
            // 
            this.colDistrictID1.Caption = "District ID";
            this.colDistrictID1.FieldName = "DistrictID";
            this.colDistrictID1.Name = "colDistrictID1";
            this.colDistrictID1.OptionsColumn.AllowEdit = false;
            this.colDistrictID1.OptionsColumn.AllowFocus = false;
            this.colDistrictID1.OptionsColumn.ReadOnly = true;
            this.colDistrictID1.Width = 58;
            // 
            // colLocalityName1
            // 
            this.colLocalityName1.Caption = "Locality Name";
            this.colLocalityName1.FieldName = "LocalityName";
            this.colLocalityName1.Name = "colLocalityName1";
            this.colLocalityName1.OptionsColumn.AllowEdit = false;
            this.colLocalityName1.OptionsColumn.AllowFocus = false;
            this.colLocalityName1.OptionsColumn.ReadOnly = true;
            this.colLocalityName1.Width = 100;
            // 
            // colDistrictName1
            // 
            this.colDistrictName1.Caption = "District Name";
            this.colDistrictName1.FieldName = "DistrictName";
            this.colDistrictName1.Name = "colDistrictName1";
            this.colDistrictName1.OptionsColumn.AllowEdit = false;
            this.colDistrictName1.OptionsColumn.AllowFocus = false;
            this.colDistrictName1.OptionsColumn.ReadOnly = true;
            this.colDistrictName1.Width = 97;
            // 
            // colTreeReference2
            // 
            this.colTreeReference2.Caption = "Tree Reference";
            this.colTreeReference2.FieldName = "TreeReference";
            this.colTreeReference2.Name = "colTreeReference2";
            this.colTreeReference2.OptionsColumn.AllowEdit = false;
            this.colTreeReference2.OptionsColumn.AllowFocus = false;
            this.colTreeReference2.OptionsColumn.ReadOnly = true;
            this.colTreeReference2.Width = 109;
            // 
            // colTreeMappingID
            // 
            this.colTreeMappingID.Caption = "Map ID";
            this.colTreeMappingID.FieldName = "TreeMappingID";
            this.colTreeMappingID.Name = "colTreeMappingID";
            this.colTreeMappingID.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID.Width = 45;
            // 
            // colTreeSpeciesName
            // 
            this.colTreeSpeciesName.Caption = "Species";
            this.colTreeSpeciesName.FieldName = "TreeSpeciesName";
            this.colTreeSpeciesName.Name = "colTreeSpeciesName";
            this.colTreeSpeciesName.OptionsColumn.AllowEdit = false;
            this.colTreeSpeciesName.OptionsColumn.AllowFocus = false;
            this.colTreeSpeciesName.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesName.Width = 57;
            // 
            // colTreeX
            // 
            this.colTreeX.Caption = "Tree X Coordinate";
            this.colTreeX.FieldName = "TreeX";
            this.colTreeX.Name = "colTreeX";
            this.colTreeX.OptionsColumn.AllowEdit = false;
            this.colTreeX.OptionsColumn.AllowFocus = false;
            this.colTreeX.OptionsColumn.ReadOnly = true;
            this.colTreeX.Width = 98;
            // 
            // colTreeY
            // 
            this.colTreeY.Caption = "Tree Y Coordinate";
            this.colTreeY.FieldName = "TreeY";
            this.colTreeY.Name = "colTreeY";
            this.colTreeY.OptionsColumn.AllowEdit = false;
            this.colTreeY.OptionsColumn.AllowFocus = false;
            this.colTreeY.OptionsColumn.ReadOnly = true;
            this.colTreeY.Width = 98;
            // 
            // colTreePolygonXY
            // 
            this.colTreePolygonXY.Caption = "Tree Polygon XY";
            this.colTreePolygonXY.FieldName = "TreePolygonXY";
            this.colTreePolygonXY.Name = "colTreePolygonXY";
            this.colTreePolygonXY.OptionsColumn.ReadOnly = true;
            this.colTreePolygonXY.Width = 89;
            // 
            // colInspectionReference
            // 
            this.colInspectionReference.Caption = "Inspection Reference No";
            this.colInspectionReference.FieldName = "InspectionReference";
            this.colInspectionReference.Name = "colInspectionReference";
            this.colInspectionReference.OptionsColumn.AllowEdit = false;
            this.colInspectionReference.OptionsColumn.AllowFocus = false;
            this.colInspectionReference.OptionsColumn.ReadOnly = true;
            this.colInspectionReference.Visible = true;
            this.colInspectionReference.VisibleIndex = 1;
            this.colInspectionReference.Width = 140;
            // 
            // colInspectionInspector
            // 
            this.colInspectionInspector.Caption = "Inspector";
            this.colInspectionInspector.FieldName = "InspectionInspector";
            this.colInspectionInspector.Name = "colInspectionInspector";
            this.colInspectionInspector.OptionsColumn.AllowEdit = false;
            this.colInspectionInspector.OptionsColumn.AllowFocus = false;
            this.colInspectionInspector.OptionsColumn.ReadOnly = true;
            this.colInspectionInspector.Visible = true;
            this.colInspectionInspector.VisibleIndex = 2;
            this.colInspectionInspector.Width = 67;
            // 
            // colInspectionDate
            // 
            this.colInspectionDate.Caption = "Inspection Date";
            this.colInspectionDate.FieldName = "InspectionDate";
            this.colInspectionDate.Name = "colInspectionDate";
            this.colInspectionDate.OptionsColumn.AllowEdit = false;
            this.colInspectionDate.OptionsColumn.AllowFocus = false;
            this.colInspectionDate.OptionsColumn.ReadOnly = true;
            this.colInspectionDate.Visible = true;
            this.colInspectionDate.VisibleIndex = 0;
            this.colInspectionDate.Width = 110;
            // 
            // colInspectionIncidentReference
            // 
            this.colInspectionIncidentReference.Caption = "Incident Reference No";
            this.colInspectionIncidentReference.FieldName = "InspectionIncidentReference";
            this.colInspectionIncidentReference.Name = "colInspectionIncidentReference";
            this.colInspectionIncidentReference.OptionsColumn.AllowEdit = false;
            this.colInspectionIncidentReference.OptionsColumn.AllowFocus = false;
            this.colInspectionIncidentReference.OptionsColumn.ReadOnly = true;
            this.colInspectionIncidentReference.Visible = true;
            this.colInspectionIncidentReference.VisibleIndex = 7;
            this.colInspectionIncidentReference.Width = 129;
            // 
            // colInspectionIncidentDate
            // 
            this.colInspectionIncidentDate.Caption = "Incident Date";
            this.colInspectionIncidentDate.FieldName = "InspectionIncidentDate";
            this.colInspectionIncidentDate.Name = "colInspectionIncidentDate";
            this.colInspectionIncidentDate.OptionsColumn.AllowEdit = false;
            this.colInspectionIncidentDate.OptionsColumn.AllowFocus = false;
            this.colInspectionIncidentDate.OptionsColumn.ReadOnly = true;
            this.colInspectionIncidentDate.Visible = true;
            this.colInspectionIncidentDate.VisibleIndex = 8;
            this.colInspectionIncidentDate.Width = 86;
            // 
            // colIncidentID
            // 
            this.colIncidentID.Caption = "Incident ID";
            this.colIncidentID.FieldName = "IncidentID";
            this.colIncidentID.Name = "colIncidentID";
            this.colIncidentID.OptionsColumn.AllowEdit = false;
            this.colIncidentID.OptionsColumn.AllowFocus = false;
            this.colIncidentID.OptionsColumn.ReadOnly = true;
            this.colIncidentID.Width = 64;
            // 
            // colInspectionAngleToVertical
            // 
            this.colInspectionAngleToVertical.Caption = "Angle to Vertical";
            this.colInspectionAngleToVertical.FieldName = "InspectionAngleToVertical";
            this.colInspectionAngleToVertical.Name = "colInspectionAngleToVertical";
            this.colInspectionAngleToVertical.OptionsColumn.AllowEdit = false;
            this.colInspectionAngleToVertical.OptionsColumn.AllowFocus = false;
            this.colInspectionAngleToVertical.OptionsColumn.ReadOnly = true;
            this.colInspectionAngleToVertical.Visible = true;
            this.colInspectionAngleToVertical.VisibleIndex = 9;
            this.colInspectionAngleToVertical.Width = 99;
            // 
            // colInspectionLastModified
            // 
            this.colInspectionLastModified.Caption = "Inspection Last Modified";
            this.colInspectionLastModified.FieldName = "InspectionLastModified";
            this.colInspectionLastModified.Name = "colInspectionLastModified";
            this.colInspectionLastModified.OptionsColumn.AllowEdit = false;
            this.colInspectionLastModified.OptionsColumn.AllowFocus = false;
            this.colInspectionLastModified.OptionsColumn.ReadOnly = true;
            this.colInspectionLastModified.OptionsColumn.ShowInCustomizationForm = false;
            this.colInspectionLastModified.Width = 127;
            // 
            // colInspectionUserDefined1
            // 
            this.colInspectionUserDefined1.Caption = "Inspection User Defined 1";
            this.colInspectionUserDefined1.FieldName = "InspectionUserDefined1";
            this.colInspectionUserDefined1.Name = "colInspectionUserDefined1";
            this.colInspectionUserDefined1.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined1.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined1.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined1.Visible = true;
            this.colInspectionUserDefined1.VisibleIndex = 31;
            this.colInspectionUserDefined1.Width = 145;
            // 
            // colInspectionUserDefined2
            // 
            this.colInspectionUserDefined2.Caption = "Inspection User Defined 2";
            this.colInspectionUserDefined2.FieldName = "InspectionUserDefined2";
            this.colInspectionUserDefined2.Name = "colInspectionUserDefined2";
            this.colInspectionUserDefined2.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined2.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined2.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined2.Visible = true;
            this.colInspectionUserDefined2.VisibleIndex = 32;
            this.colInspectionUserDefined2.Width = 145;
            // 
            // colInspectionUserDefined3
            // 
            this.colInspectionUserDefined3.Caption = "Inspection User Defined 3";
            this.colInspectionUserDefined3.FieldName = "InspectionUserDefined3";
            this.colInspectionUserDefined3.Name = "colInspectionUserDefined3";
            this.colInspectionUserDefined3.OptionsColumn.AllowEdit = false;
            this.colInspectionUserDefined3.OptionsColumn.AllowFocus = false;
            this.colInspectionUserDefined3.OptionsColumn.ReadOnly = true;
            this.colInspectionUserDefined3.Visible = true;
            this.colInspectionUserDefined3.VisibleIndex = 33;
            this.colInspectionUserDefined3.Width = 145;
            // 
            // colInspectionRemarks
            // 
            this.colInspectionRemarks.Caption = "Inspection Remarks";
            this.colInspectionRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colInspectionRemarks.FieldName = "InspectionRemarks";
            this.colInspectionRemarks.Name = "colInspectionRemarks";
            this.colInspectionRemarks.OptionsColumn.ReadOnly = true;
            this.colInspectionRemarks.Visible = true;
            this.colInspectionRemarks.VisibleIndex = 10;
            this.colInspectionRemarks.Width = 115;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colInspectionStemPhysical1
            // 
            this.colInspectionStemPhysical1.Caption = "Stem Physical 1";
            this.colInspectionStemPhysical1.FieldName = "InspectionStemPhysical1";
            this.colInspectionStemPhysical1.Name = "colInspectionStemPhysical1";
            this.colInspectionStemPhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical1.Visible = true;
            this.colInspectionStemPhysical1.VisibleIndex = 11;
            this.colInspectionStemPhysical1.Width = 95;
            // 
            // colInspectionStemPhysical2
            // 
            this.colInspectionStemPhysical2.Caption = "Stem Physical 2";
            this.colInspectionStemPhysical2.FieldName = "InspectionStemPhysical2";
            this.colInspectionStemPhysical2.Name = "colInspectionStemPhysical2";
            this.colInspectionStemPhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical2.Visible = true;
            this.colInspectionStemPhysical2.VisibleIndex = 12;
            this.colInspectionStemPhysical2.Width = 95;
            // 
            // colInspectionStemPhysical3
            // 
            this.colInspectionStemPhysical3.Caption = "Stem Physical 3";
            this.colInspectionStemPhysical3.FieldName = "InspectionStemPhysical3";
            this.colInspectionStemPhysical3.Name = "colInspectionStemPhysical3";
            this.colInspectionStemPhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionStemPhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionStemPhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionStemPhysical3.Visible = true;
            this.colInspectionStemPhysical3.VisibleIndex = 13;
            this.colInspectionStemPhysical3.Width = 95;
            // 
            // colInspectionStemDisease1
            // 
            this.colInspectionStemDisease1.Caption = "Stem Disease 1";
            this.colInspectionStemDisease1.FieldName = "InspectionStemDisease1";
            this.colInspectionStemDisease1.Name = "colInspectionStemDisease1";
            this.colInspectionStemDisease1.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease1.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease1.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease1.Visible = true;
            this.colInspectionStemDisease1.VisibleIndex = 14;
            this.colInspectionStemDisease1.Width = 94;
            // 
            // colInspectionStemDisease2
            // 
            this.colInspectionStemDisease2.Caption = "Stem Disease 2";
            this.colInspectionStemDisease2.FieldName = "InspectionStemDisease2";
            this.colInspectionStemDisease2.Name = "colInspectionStemDisease2";
            this.colInspectionStemDisease2.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease2.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease2.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease2.Visible = true;
            this.colInspectionStemDisease2.VisibleIndex = 15;
            this.colInspectionStemDisease2.Width = 94;
            // 
            // colInspectionStemDisease3
            // 
            this.colInspectionStemDisease3.Caption = "Stem Disease 3";
            this.colInspectionStemDisease3.FieldName = "InspectionStemDisease3";
            this.colInspectionStemDisease3.Name = "colInspectionStemDisease3";
            this.colInspectionStemDisease3.OptionsColumn.AllowEdit = false;
            this.colInspectionStemDisease3.OptionsColumn.AllowFocus = false;
            this.colInspectionStemDisease3.OptionsColumn.ReadOnly = true;
            this.colInspectionStemDisease3.Visible = true;
            this.colInspectionStemDisease3.VisibleIndex = 16;
            this.colInspectionStemDisease3.Width = 94;
            // 
            // colInspectionCrownPhysical1
            // 
            this.colInspectionCrownPhysical1.Caption = "Crown Physical 1";
            this.colInspectionCrownPhysical1.FieldName = "InspectionCrownPhysical1";
            this.colInspectionCrownPhysical1.Name = "colInspectionCrownPhysical1";
            this.colInspectionCrownPhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical1.Visible = true;
            this.colInspectionCrownPhysical1.VisibleIndex = 17;
            this.colInspectionCrownPhysical1.Width = 102;
            // 
            // colInspectionCrownPhysical2
            // 
            this.colInspectionCrownPhysical2.Caption = "Crown Physical 2";
            this.colInspectionCrownPhysical2.FieldName = "InspectionCrownPhysical2";
            this.colInspectionCrownPhysical2.Name = "colInspectionCrownPhysical2";
            this.colInspectionCrownPhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical2.Visible = true;
            this.colInspectionCrownPhysical2.VisibleIndex = 18;
            this.colInspectionCrownPhysical2.Width = 102;
            // 
            // colInspectionCrownPhysical3
            // 
            this.colInspectionCrownPhysical3.Caption = "Crown Physical 3";
            this.colInspectionCrownPhysical3.FieldName = "InspectionCrownPhysical3";
            this.colInspectionCrownPhysical3.Name = "colInspectionCrownPhysical3";
            this.colInspectionCrownPhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownPhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownPhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownPhysical3.Visible = true;
            this.colInspectionCrownPhysical3.VisibleIndex = 19;
            this.colInspectionCrownPhysical3.Width = 102;
            // 
            // colInspectionCrownDisease1
            // 
            this.colInspectionCrownDisease1.CustomizationCaption = "Crown Disease 1";
            this.colInspectionCrownDisease1.FieldName = "InspectionCrownDisease1";
            this.colInspectionCrownDisease1.Name = "colInspectionCrownDisease1";
            this.colInspectionCrownDisease1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease1.Visible = true;
            this.colInspectionCrownDisease1.VisibleIndex = 20;
            this.colInspectionCrownDisease1.Width = 151;
            // 
            // colInspectionCrownDisease2
            // 
            this.colInspectionCrownDisease2.CustomizationCaption = "Crown Disease 2";
            this.colInspectionCrownDisease2.FieldName = "InspectionCrownDisease2";
            this.colInspectionCrownDisease2.Name = "colInspectionCrownDisease2";
            this.colInspectionCrownDisease2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease2.Visible = true;
            this.colInspectionCrownDisease2.VisibleIndex = 21;
            this.colInspectionCrownDisease2.Width = 151;
            // 
            // colInspectionCrownDisease3
            // 
            this.colInspectionCrownDisease3.CustomizationCaption = "Crown Disease 3";
            this.colInspectionCrownDisease3.FieldName = "InspectionCrownDisease3";
            this.colInspectionCrownDisease3.Name = "colInspectionCrownDisease3";
            this.colInspectionCrownDisease3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownDisease3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownDisease3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownDisease3.Visible = true;
            this.colInspectionCrownDisease3.VisibleIndex = 22;
            this.colInspectionCrownDisease3.Width = 151;
            // 
            // colInspectionCrownFoliation1
            // 
            this.colInspectionCrownFoliation1.CustomizationCaption = "Crown Foliation 1";
            this.colInspectionCrownFoliation1.FieldName = "InspectionCrownFoliation1";
            this.colInspectionCrownFoliation1.Name = "colInspectionCrownFoliation1";
            this.colInspectionCrownFoliation1.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation1.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation1.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation1.Visible = true;
            this.colInspectionCrownFoliation1.VisibleIndex = 23;
            this.colInspectionCrownFoliation1.Width = 154;
            // 
            // colInspectionCrownFoliation2
            // 
            this.colInspectionCrownFoliation2.CustomizationCaption = "Crown Foliation 2";
            this.colInspectionCrownFoliation2.FieldName = "InspectionCrownFoliation2";
            this.colInspectionCrownFoliation2.Name = "colInspectionCrownFoliation2";
            this.colInspectionCrownFoliation2.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation2.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation2.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation2.Visible = true;
            this.colInspectionCrownFoliation2.VisibleIndex = 24;
            this.colInspectionCrownFoliation2.Width = 154;
            // 
            // colInspectionCrownFoliation3
            // 
            this.colInspectionCrownFoliation3.CustomizationCaption = "Crown Foliation 3";
            this.colInspectionCrownFoliation3.FieldName = "InspectionCrownFoliation3";
            this.colInspectionCrownFoliation3.Name = "colInspectionCrownFoliation3";
            this.colInspectionCrownFoliation3.OptionsColumn.AllowEdit = false;
            this.colInspectionCrownFoliation3.OptionsColumn.AllowFocus = false;
            this.colInspectionCrownFoliation3.OptionsColumn.ReadOnly = true;
            this.colInspectionCrownFoliation3.Visible = true;
            this.colInspectionCrownFoliation3.VisibleIndex = 25;
            this.colInspectionCrownFoliation3.Width = 154;
            // 
            // colInspectionRiskCategory
            // 
            this.colInspectionRiskCategory.CustomizationCaption = "Inspection Risk Category";
            this.colInspectionRiskCategory.FieldName = "InspectionRiskCategory";
            this.colInspectionRiskCategory.Name = "colInspectionRiskCategory";
            this.colInspectionRiskCategory.OptionsColumn.AllowEdit = false;
            this.colInspectionRiskCategory.OptionsColumn.AllowFocus = false;
            this.colInspectionRiskCategory.OptionsColumn.ReadOnly = true;
            this.colInspectionRiskCategory.Visible = true;
            this.colInspectionRiskCategory.VisibleIndex = 3;
            this.colInspectionRiskCategory.Width = 141;
            // 
            // colInspectionGeneralCondition
            // 
            this.colInspectionGeneralCondition.CustomizationCaption = "Inspection General condition";
            this.colInspectionGeneralCondition.FieldName = "InspectionGeneralCondition";
            this.colInspectionGeneralCondition.Name = "colInspectionGeneralCondition";
            this.colInspectionGeneralCondition.OptionsColumn.AllowEdit = false;
            this.colInspectionGeneralCondition.OptionsColumn.AllowFocus = false;
            this.colInspectionGeneralCondition.OptionsColumn.ReadOnly = true;
            this.colInspectionGeneralCondition.Visible = true;
            this.colInspectionGeneralCondition.VisibleIndex = 29;
            this.colInspectionGeneralCondition.Width = 159;
            // 
            // colInspectionBasePhysical1
            // 
            this.colInspectionBasePhysical1.CustomizationCaption = "Base Physical 1";
            this.colInspectionBasePhysical1.FieldName = "InspectionBasePhysical1";
            this.colInspectionBasePhysical1.Name = "colInspectionBasePhysical1";
            this.colInspectionBasePhysical1.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical1.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical1.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical1.Visible = true;
            this.colInspectionBasePhysical1.VisibleIndex = 26;
            this.colInspectionBasePhysical1.Width = 144;
            // 
            // colInspectionBasePhysical2
            // 
            this.colInspectionBasePhysical2.CustomizationCaption = "Base Physical  2";
            this.colInspectionBasePhysical2.FieldName = "InspectionBasePhysical2";
            this.colInspectionBasePhysical2.Name = "colInspectionBasePhysical2";
            this.colInspectionBasePhysical2.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical2.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical2.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical2.Visible = true;
            this.colInspectionBasePhysical2.VisibleIndex = 27;
            this.colInspectionBasePhysical2.Width = 144;
            // 
            // colInspectionBasePhysical3
            // 
            this.colInspectionBasePhysical3.CustomizationCaption = "Base Physical 3";
            this.colInspectionBasePhysical3.FieldName = "InspectionBasePhysical3";
            this.colInspectionBasePhysical3.Name = "colInspectionBasePhysical3";
            this.colInspectionBasePhysical3.OptionsColumn.AllowEdit = false;
            this.colInspectionBasePhysical3.OptionsColumn.AllowFocus = false;
            this.colInspectionBasePhysical3.OptionsColumn.ReadOnly = true;
            this.colInspectionBasePhysical3.Visible = true;
            this.colInspectionBasePhysical3.VisibleIndex = 28;
            this.colInspectionBasePhysical3.Width = 144;
            // 
            // colInspectionVitality
            // 
            this.colInspectionVitality.CustomizationCaption = "Inspection Vitality";
            this.colInspectionVitality.FieldName = "InspectionVitality";
            this.colInspectionVitality.Name = "colInspectionVitality";
            this.colInspectionVitality.OptionsColumn.AllowEdit = false;
            this.colInspectionVitality.OptionsColumn.AllowFocus = false;
            this.colInspectionVitality.OptionsColumn.ReadOnly = true;
            this.colInspectionVitality.Visible = true;
            this.colInspectionVitality.VisibleIndex = 30;
            this.colInspectionVitality.Width = 106;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.CustomizationCaption = "Linked Action Count";
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 4;
            this.colLinkedActionCount.Width = 116;
            // 
            // colLinkedOutstandingActionCount
            // 
            this.colLinkedOutstandingActionCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedOutstandingActionCount.CustomizationCaption = "Linked Outstanding Action Count";
            this.colLinkedOutstandingActionCount.FieldName = "LinkedOutstandingActionCount";
            this.colLinkedOutstandingActionCount.Name = "colLinkedOutstandingActionCount";
            this.colLinkedOutstandingActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedOutstandingActionCount.Visible = true;
            this.colLinkedOutstandingActionCount.VisibleIndex = 5;
            this.colLinkedOutstandingActionCount.Width = 178;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colNoFurtherActionRequired
            // 
            this.colNoFurtherActionRequired.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colNoFurtherActionRequired.CustomizationCaption = "No Further Action Required";
            this.colNoFurtherActionRequired.FieldName = "NoFurtherActionRequired";
            this.colNoFurtherActionRequired.Name = "colNoFurtherActionRequired";
            this.colNoFurtherActionRequired.OptionsColumn.ReadOnly = true;
            this.colNoFurtherActionRequired.Visible = true;
            this.colNoFurtherActionRequired.VisibleIndex = 6;
            this.colNoFurtherActionRequired.Width = 152;
            // 
            // gridColumn133
            // 
            this.gridColumn133.Caption = "<b>Calculated 1</b>";
            this.gridColumn133.FieldName = "Calculated1";
            this.gridColumn133.Name = "gridColumn133";
            this.gridColumn133.OptionsColumn.AllowEdit = false;
            this.gridColumn133.OptionsColumn.AllowFocus = false;
            this.gridColumn133.OptionsColumn.ReadOnly = true;
            this.gridColumn133.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn133.Visible = true;
            this.gridColumn133.VisibleIndex = 34;
            this.gridColumn133.Width = 90;
            // 
            // gridColumn134
            // 
            this.gridColumn134.Caption = "<b>Calculated 2</b>";
            this.gridColumn134.FieldName = "Calculated2";
            this.gridColumn134.Name = "gridColumn134";
            this.gridColumn134.OptionsColumn.AllowEdit = false;
            this.gridColumn134.OptionsColumn.AllowFocus = false;
            this.gridColumn134.OptionsColumn.ReadOnly = true;
            this.gridColumn134.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn134.Visible = true;
            this.gridColumn134.VisibleIndex = 35;
            this.gridColumn134.Width = 90;
            // 
            // gridColumn135
            // 
            this.gridColumn135.Caption = "<b>Calculated 3</b>";
            this.gridColumn135.FieldName = "Calculated3";
            this.gridColumn135.Name = "gridColumn135";
            this.gridColumn135.OptionsColumn.AllowEdit = false;
            this.gridColumn135.OptionsColumn.AllowFocus = false;
            this.gridColumn135.OptionsColumn.ReadOnly = true;
            this.gridColumn135.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn135.Visible = true;
            this.gridColumn135.VisibleIndex = 36;
            this.gridColumn135.Width = 90;
            // 
            // SpeciesVarietyGridLookUpEdit
            // 
            this.SpeciesVarietyGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "SpeciesVariety", true));
            this.SpeciesVarietyGridLookUpEdit.Location = new System.Drawing.Point(143, 84);
            this.SpeciesVarietyGridLookUpEdit.MenuManager = this.barManager1;
            this.SpeciesVarietyGridLookUpEdit.Name = "SpeciesVarietyGridLookUpEdit";
            editorButtonImageOptions2.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions3.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.SpeciesVarietyGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SpeciesVarietyGridLookUpEdit.Properties.DataSource = this.sp01374ATSpeciesVarietiesWithBlanksBindingSource;
            this.SpeciesVarietyGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.SpeciesVarietyGridLookUpEdit.Properties.NullText = "";
            this.SpeciesVarietyGridLookUpEdit.Properties.PopupView = this.gridView28;
            this.SpeciesVarietyGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.SpeciesVarietyGridLookUpEdit.Size = new System.Drawing.Size(419, 22);
            this.SpeciesVarietyGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SpeciesVarietyGridLookUpEdit.TabIndex = 79;
            this.SpeciesVarietyGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SpeciesVarietyGridLookUpEdit_ButtonClick);
            this.SpeciesVarietyGridLookUpEdit.EditValueChanged += new System.EventHandler(this.SpeciesVarietyGridLookUpEdit_EditValueChanged);
            this.SpeciesVarietyGridLookUpEdit.Enter += new System.EventHandler(this.SpeciesVarietyGridLookUpEdit_Enter);
            this.SpeciesVarietyGridLookUpEdit.Leave += new System.EventHandler(this.SpeciesVarietyGridLookUpEdit_Leave);
            this.SpeciesVarietyGridLookUpEdit.Validated += new System.EventHandler(this.SpeciesVarietyGridLookUpEdit_Validated);
            // 
            // sp01374ATSpeciesVarietiesWithBlanksBindingSource
            // 
            this.sp01374ATSpeciesVarietiesWithBlanksBindingSource.DataMember = "sp01374_AT_Species_Varieties_With_Blanks";
            this.sp01374ATSpeciesVarietiesWithBlanksBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView28
            // 
            this.gridView28.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colItemDescription1,
            this.colItemID1,
            this.colSpeciesID1,
            this.colSpeciesName1});
            this.gridView28.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colItemID1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView28.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView28.GroupCount = 1;
            this.gridView28.Name = "gridView28";
            this.gridView28.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView28.OptionsCustomization.AllowFilter = false;
            this.gridView28.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView28.OptionsLayout.StoreAppearance = true;
            this.gridView28.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView28.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView28.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView28.OptionsView.ColumnAutoWidth = false;
            this.gridView28.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView28.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView28.OptionsView.ShowGroupPanel = false;
            this.gridView28.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView28.OptionsView.ShowIndicator = false;
            this.gridView28.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSpeciesName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemDescription1
            // 
            this.colItemDescription1.Caption = "Variety";
            this.colItemDescription1.FieldName = "ItemDescription";
            this.colItemDescription1.Name = "colItemDescription1";
            this.colItemDescription1.OptionsColumn.AllowEdit = false;
            this.colItemDescription1.OptionsColumn.AllowFocus = false;
            this.colItemDescription1.OptionsColumn.ReadOnly = true;
            this.colItemDescription1.Visible = true;
            this.colItemDescription1.VisibleIndex = 0;
            this.colItemDescription1.Width = 254;
            // 
            // colSpeciesID1
            // 
            this.colSpeciesID1.Caption = "Species ID";
            this.colSpeciesID1.FieldName = "SpeciesID";
            this.colSpeciesID1.Name = "colSpeciesID1";
            this.colSpeciesID1.OptionsColumn.AllowEdit = false;
            this.colSpeciesID1.OptionsColumn.AllowFocus = false;
            this.colSpeciesID1.OptionsColumn.ReadOnly = true;
            // 
            // colSpeciesName1
            // 
            this.colSpeciesName1.Caption = "Species Name";
            this.colSpeciesName1.FieldName = "SpeciesName";
            this.colSpeciesName1.Name = "colSpeciesName1";
            this.colSpeciesName1.OptionsColumn.AllowEdit = false;
            this.colSpeciesName1.OptionsColumn.AllowFocus = false;
            this.colSpeciesName1.OptionsColumn.ReadOnly = true;
            this.colSpeciesName1.Visible = true;
            this.colSpeciesName1.VisibleIndex = 1;
            this.colSpeciesName1.Width = 154;
            // 
            // ObjectWidthSpinEdit
            // 
            this.ObjectWidthSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "ObjectWidth", true));
            this.ObjectWidthSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ObjectWidthSpinEdit.Location = new System.Drawing.Point(685, 414);
            this.ObjectWidthSpinEdit.MenuManager = this.barManager1;
            this.ObjectWidthSpinEdit.Name = "ObjectWidthSpinEdit";
            this.ObjectWidthSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ObjectWidthSpinEdit.Properties.Mask.EditMask = "######0.00 metres";
            this.ObjectWidthSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ObjectWidthSpinEdit.Size = new System.Drawing.Size(407, 20);
            this.ObjectWidthSpinEdit.StyleController = this.dataLayoutControl1;
            this.ObjectWidthSpinEdit.TabIndex = 78;
            this.ObjectWidthSpinEdit.EditValueChanged += new System.EventHandler(this.ObjectWidthSpinEdit_EditValueChanged);
            this.ObjectWidthSpinEdit.Validated += new System.EventHandler(this.ObjectWidthSpinEdit_Validated);
            // 
            // ObjectLengthSpinEdit
            // 
            this.ObjectLengthSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "ObjectLength", true));
            this.ObjectLengthSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ObjectLengthSpinEdit.Location = new System.Drawing.Point(155, 414);
            this.ObjectLengthSpinEdit.MenuManager = this.barManager1;
            this.ObjectLengthSpinEdit.Name = "ObjectLengthSpinEdit";
            this.ObjectLengthSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ObjectLengthSpinEdit.Properties.Mask.EditMask = "######0.00 metres";
            this.ObjectLengthSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ObjectLengthSpinEdit.Size = new System.Drawing.Size(407, 20);
            this.ObjectLengthSpinEdit.StyleController = this.dataLayoutControl1;
            this.ObjectLengthSpinEdit.TabIndex = 77;
            this.ObjectLengthSpinEdit.EditValueChanged += new System.EventHandler(this.ObjectLengthSpinEdit_EditValueChanged);
            this.ObjectLengthSpinEdit.Validated += new System.EventHandler(this.ObjectLengthSpinEdit_Validated);
            // 
            // decHeightSpinEdit
            // 
            this.decHeightSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "decHeight", true));
            this.decHeightSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.decHeightSpinEdit.Location = new System.Drawing.Point(143, 136);
            this.decHeightSpinEdit.MenuManager = this.barManager1;
            this.decHeightSpinEdit.Name = "decHeightSpinEdit";
            this.decHeightSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.decHeightSpinEdit.Properties.Mask.EditMask = "##0.0 metres";
            this.decHeightSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.decHeightSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            65536});
            this.decHeightSpinEdit.Size = new System.Drawing.Size(419, 20);
            this.decHeightSpinEdit.StyleController = this.dataLayoutControl1;
            this.decHeightSpinEdit.TabIndex = 73;
            this.decHeightSpinEdit.EditValueChanged += new System.EventHandler(this.decHeightSpinEdit_EditValueChanged);
            this.decHeightSpinEdit.Validated += new System.EventHandler(this.decHeightSpinEdit_Validated);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp01204ATEditTreeDetailsBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, -155);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 72;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // intTreeIDTextEdit
            // 
            this.intTreeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intTreeID", true));
            this.intTreeIDTextEdit.Location = new System.Drawing.Point(131, 35);
            this.intTreeIDTextEdit.MenuManager = this.barManager1;
            this.intTreeIDTextEdit.Name = "intTreeIDTextEdit";
            this.intTreeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intTreeIDTextEdit, true);
            this.intTreeIDTextEdit.Size = new System.Drawing.Size(989, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intTreeIDTextEdit, optionsSpelling11);
            this.intTreeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intTreeIDTextEdit.TabIndex = 4;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(143, -98);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(961, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling12);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 6;
            // 
            // strGridRefTextEdit
            // 
            this.strGridRefTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strGridRef", true));
            this.strGridRefTextEdit.Location = new System.Drawing.Point(134, 294);
            this.strGridRefTextEdit.MenuManager = this.barManager1;
            this.strGridRefTextEdit.Name = "strGridRefTextEdit";
            this.strGridRefTextEdit.Properties.MaxLength = 14;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strGridRefTextEdit, true);
            this.strGridRefTextEdit.Size = new System.Drawing.Size(438, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strGridRefTextEdit, optionsSpelling13);
            this.strGridRefTextEdit.StyleController = this.dataLayoutControl1;
            this.strGridRefTextEdit.TabIndex = 9;
            // 
            // strPostcodeTextEdit
            // 
            this.strPostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strPostcode", true));
            this.strPostcodeTextEdit.Location = new System.Drawing.Point(134, 294);
            this.strPostcodeTextEdit.MenuManager = this.barManager1;
            this.strPostcodeTextEdit.Name = "strPostcodeTextEdit";
            this.strPostcodeTextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strPostcodeTextEdit, true);
            this.strPostcodeTextEdit.Size = new System.Drawing.Size(991, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strPostcodeTextEdit, optionsSpelling14);
            this.strPostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.strPostcodeTextEdit.TabIndex = 34;
            // 
            // DateAddedDateEdit
            // 
            this.DateAddedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "DateAdded", true));
            this.DateAddedDateEdit.EditValue = null;
            this.DateAddedDateEdit.Location = new System.Drawing.Point(122, 459);
            this.DateAddedDateEdit.MenuManager = this.barManager1;
            this.DateAddedDateEdit.Name = "DateAddedDateEdit";
            this.DateAddedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateAddedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateAddedDateEdit.Size = new System.Drawing.Size(998, 20);
            this.DateAddedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateAddedDateEdit.TabIndex = 58;
            // 
            // idTextEdit
            // 
            this.idTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "id", true));
            this.idTextEdit.Location = new System.Drawing.Point(155, 246);
            this.idTextEdit.MenuManager = this.barManager1;
            this.idTextEdit.Name = "idTextEdit";
            this.idTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.idTextEdit, true);
            this.idTextEdit.Size = new System.Drawing.Size(407, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.idTextEdit, optionsSpelling15);
            this.idTextEdit.StyleController = this.dataLayoutControl1;
            this.idTextEdit.TabIndex = 8;
            // 
            // intDistanceSpinEdit
            // 
            this.intDistanceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intDistance", true));
            this.intDistanceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intDistanceSpinEdit.Location = new System.Drawing.Point(688, 170);
            this.intDistanceSpinEdit.MenuManager = this.barManager1;
            this.intDistanceSpinEdit.Name = "intDistanceSpinEdit";
            this.intDistanceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intDistanceSpinEdit.Properties.IsFloatValue = false;
            this.intDistanceSpinEdit.Properties.Mask.EditMask = "N00";
            this.intDistanceSpinEdit.Size = new System.Drawing.Size(404, 20);
            this.intDistanceSpinEdit.StyleController = this.dataLayoutControl1;
            this.intDistanceSpinEdit.TabIndex = 10;
            // 
            // strHouseNameTextEdit
            // 
            this.strHouseNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strHouseName", true));
            this.strHouseNameTextEdit.Location = new System.Drawing.Point(143, 58);
            this.strHouseNameTextEdit.MenuManager = this.barManager1;
            this.strHouseNameTextEdit.Name = "strHouseNameTextEdit";
            this.strHouseNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strHouseNameTextEdit, true);
            this.strHouseNameTextEdit.Size = new System.Drawing.Size(961, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strHouseNameTextEdit, optionsSpelling16);
            this.strHouseNameTextEdit.StyleController = this.dataLayoutControl1;
            this.strHouseNameTextEdit.TabIndex = 11;
            // 
            // dtLastInspectionDateDateEdit
            // 
            this.dtLastInspectionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "dtLastInspectionDate", true));
            this.dtLastInspectionDateDateEdit.EditValue = null;
            this.dtLastInspectionDateDateEdit.Location = new System.Drawing.Point(143, 58);
            this.dtLastInspectionDateDateEdit.MenuManager = this.barManager1;
            this.dtLastInspectionDateDateEdit.Name = "dtLastInspectionDateDateEdit";
            this.dtLastInspectionDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dtLastInspectionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Clear Date", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dtLastInspectionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtLastInspectionDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dtLastInspectionDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtLastInspectionDateDateEdit.Size = new System.Drawing.Size(142, 20);
            this.dtLastInspectionDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtLastInspectionDateDateEdit.TabIndex = 18;
            this.dtLastInspectionDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dtLastInspectionDateDateEdit_ButtonClick);
            this.dtLastInspectionDateDateEdit.EditValueChanged += new System.EventHandler(this.dtLastInspectionDateDateEdit_EditValueChanged);
            this.dtLastInspectionDateDateEdit.Validated += new System.EventHandler(this.dtLastInspectionDateDateEdit_Validated);
            // 
            // intInspectionCycleSpinEdit
            // 
            this.intInspectionCycleSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intInspectionCycle", true));
            this.intInspectionCycleSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intInspectionCycleSpinEdit.Location = new System.Drawing.Point(143, 82);
            this.intInspectionCycleSpinEdit.MenuManager = this.barManager1;
            this.intInspectionCycleSpinEdit.Name = "intInspectionCycleSpinEdit";
            this.intInspectionCycleSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intInspectionCycleSpinEdit.Properties.IsFloatValue = false;
            this.intInspectionCycleSpinEdit.Properties.Mask.EditMask = "N00";
            this.intInspectionCycleSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.intInspectionCycleSpinEdit.Size = new System.Drawing.Size(142, 20);
            this.intInspectionCycleSpinEdit.StyleController = this.dataLayoutControl1;
            this.intInspectionCycleSpinEdit.TabIndex = 19;
            this.intInspectionCycleSpinEdit.EditValueChanged += new System.EventHandler(this.intInspectionCycleSpinEdit_EditValueChanged);
            this.intInspectionCycleSpinEdit.Validated += new System.EventHandler(this.intInspectionCycleSpinEdit_Validated);
            // 
            // dtNextInspectionDateDateEdit
            // 
            this.dtNextInspectionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "dtNextInspectionDate", true));
            this.dtNextInspectionDateDateEdit.EditValue = null;
            this.dtNextInspectionDateDateEdit.Location = new System.Drawing.Point(143, 106);
            this.dtNextInspectionDateDateEdit.MenuManager = this.barManager1;
            this.dtNextInspectionDateDateEdit.Name = "dtNextInspectionDateDateEdit";
            this.dtNextInspectionDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dtNextInspectionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Clear Date", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dtNextInspectionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtNextInspectionDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dtNextInspectionDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtNextInspectionDateDateEdit.Size = new System.Drawing.Size(142, 20);
            this.dtNextInspectionDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtNextInspectionDateDateEdit.TabIndex = 21;
            this.dtNextInspectionDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dtNextInspectionDateDateEdit_ButtonClick);
            // 
            // intDBHSpinEdit
            // 
            this.intDBHSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intDBH", true));
            this.intDBHSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intDBHSpinEdit.Location = new System.Drawing.Point(143, 110);
            this.intDBHSpinEdit.MenuManager = this.barManager1;
            this.intDBHSpinEdit.Name = "intDBHSpinEdit";
            this.intDBHSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intDBHSpinEdit.Properties.IsFloatValue = false;
            this.intDBHSpinEdit.Properties.Mask.EditMask = "####0";
            this.intDBHSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.intDBHSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.intDBHSpinEdit.Size = new System.Drawing.Size(419, 20);
            this.intDBHSpinEdit.StyleController = this.dataLayoutControl1;
            this.intDBHSpinEdit.TabIndex = 23;
            this.intDBHSpinEdit.EditValueChanged += new System.EventHandler(this.intDBHSpinEdit_EditValueChanged);
            this.intDBHSpinEdit.Validated += new System.EventHandler(this.intDBHSpinEdit_Validated);
            // 
            // intCrownDiameterSpinEdit
            // 
            this.intCrownDiameterSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intCrownDiameter", true));
            this.intCrownDiameterSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownDiameterSpinEdit.Location = new System.Drawing.Point(143, 162);
            this.intCrownDiameterSpinEdit.MenuManager = this.barManager1;
            this.intCrownDiameterSpinEdit.Name = "intCrownDiameterSpinEdit";
            this.intCrownDiameterSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intCrownDiameterSpinEdit.Properties.IsFloatValue = false;
            this.intCrownDiameterSpinEdit.Properties.Mask.EditMask = "#0 metres";
            this.intCrownDiameterSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.intCrownDiameterSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.intCrownDiameterSpinEdit.Size = new System.Drawing.Size(419, 20);
            this.intCrownDiameterSpinEdit.StyleController = this.dataLayoutControl1;
            this.intCrownDiameterSpinEdit.TabIndex = 25;
            // 
            // intCrownDepthSpinEdit
            // 
            this.intCrownDepthSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intCrownDepth", true));
            this.intCrownDepthSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownDepthSpinEdit.Location = new System.Drawing.Point(685, 162);
            this.intCrownDepthSpinEdit.MenuManager = this.barManager1;
            this.intCrownDepthSpinEdit.Name = "intCrownDepthSpinEdit";
            this.intCrownDepthSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intCrownDepthSpinEdit.Size = new System.Drawing.Size(419, 20);
            this.intCrownDepthSpinEdit.StyleController = this.dataLayoutControl1;
            this.intCrownDepthSpinEdit.TabIndex = 26;
            // 
            // dtPlantDateDateEdit
            // 
            this.dtPlantDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "dtPlantDate", true));
            this.dtPlantDateDateEdit.EditValue = null;
            this.dtPlantDateDateEdit.Location = new System.Drawing.Point(143, 58);
            this.dtPlantDateDateEdit.MenuManager = this.barManager1;
            this.dtPlantDateDateEdit.Name = "dtPlantDateDateEdit";
            this.dtPlantDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dtPlantDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "Clear Date", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dtPlantDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtPlantDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dtPlantDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtPlantDateDateEdit.Size = new System.Drawing.Size(453, 20);
            this.dtPlantDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtPlantDateDateEdit.TabIndex = 27;
            this.dtPlantDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dtPlantDateDateEdit_ButtonClick);
            // 
            // intGroupNoSpinEdit
            // 
            this.intGroupNoSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intGroupNo", true));
            this.intGroupNoSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intGroupNoSpinEdit.Location = new System.Drawing.Point(143, 263);
            this.intGroupNoSpinEdit.MenuManager = this.barManager1;
            this.intGroupNoSpinEdit.Name = "intGroupNoSpinEdit";
            this.intGroupNoSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intGroupNoSpinEdit.Properties.IsFloatValue = false;
            this.intGroupNoSpinEdit.Properties.Mask.EditMask = "N00";
            this.intGroupNoSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.intGroupNoSpinEdit.Size = new System.Drawing.Size(420, 20);
            this.intGroupNoSpinEdit.StyleController = this.dataLayoutControl1;
            this.intGroupNoSpinEdit.TabIndex = 28;
            // 
            // strMapLabelTextEdit
            // 
            this.strMapLabelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strMapLabel", true));
            this.strMapLabelTextEdit.Location = new System.Drawing.Point(685, 246);
            this.strMapLabelTextEdit.MenuManager = this.barManager1;
            this.strMapLabelTextEdit.Name = "strMapLabelTextEdit";
            this.strMapLabelTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strMapLabelTextEdit, true);
            this.strMapLabelTextEdit.Size = new System.Drawing.Size(407, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strMapLabelTextEdit, optionsSpelling17);
            this.strMapLabelTextEdit.StyleController = this.dataLayoutControl1;
            this.strMapLabelTextEdit.TabIndex = 35;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(24, 58);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(1080, 618);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling18);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 39;
            // 
            // strUser1TextEdit
            // 
            this.strUser1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strUser1", true));
            this.strUser1TextEdit.Location = new System.Drawing.Point(143, 58);
            this.strUser1TextEdit.MenuManager = this.barManager1;
            this.strUser1TextEdit.Name = "strUser1TextEdit";
            this.strUser1TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser1TextEdit, true);
            this.strUser1TextEdit.Size = new System.Drawing.Size(438, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser1TextEdit, optionsSpelling19);
            this.strUser1TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser1TextEdit.TabIndex = 40;
            // 
            // strUser2TextEdit
            // 
            this.strUser2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strUser2", true));
            this.strUser2TextEdit.Location = new System.Drawing.Point(143, 82);
            this.strUser2TextEdit.MenuManager = this.barManager1;
            this.strUser2TextEdit.Name = "strUser2TextEdit";
            this.strUser2TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser2TextEdit, true);
            this.strUser2TextEdit.Size = new System.Drawing.Size(438, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser2TextEdit, optionsSpelling20);
            this.strUser2TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser2TextEdit.TabIndex = 41;
            // 
            // strUser3TextEdit
            // 
            this.strUser3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strUser3", true));
            this.strUser3TextEdit.Location = new System.Drawing.Point(143, 106);
            this.strUser3TextEdit.MenuManager = this.barManager1;
            this.strUser3TextEdit.Name = "strUser3TextEdit";
            this.strUser3TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser3TextEdit, true);
            this.strUser3TextEdit.Size = new System.Drawing.Size(438, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser3TextEdit, optionsSpelling21);
            this.strUser3TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser3TextEdit.TabIndex = 42;
            // 
            // intXSpinEdit
            // 
            this.intXSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intX", true));
            this.intXSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intXSpinEdit.Location = new System.Drawing.Point(155, 270);
            this.intXSpinEdit.MenuManager = this.barManager1;
            this.intXSpinEdit.Name = "intXSpinEdit";
            this.intXSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intXSpinEdit.Properties.IsFloatValue = false;
            this.intXSpinEdit.Properties.Mask.EditMask = "N00";
            this.intXSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.intXSpinEdit.Properties.MinValue = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.intXSpinEdit.Size = new System.Drawing.Size(407, 20);
            this.intXSpinEdit.StyleController = this.dataLayoutControl1;
            this.intXSpinEdit.TabIndex = 44;
            // 
            // intYSpinEdit
            // 
            this.intYSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intY", true));
            this.intYSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intYSpinEdit.Location = new System.Drawing.Point(155, 294);
            this.intYSpinEdit.MenuManager = this.barManager1;
            this.intYSpinEdit.Name = "intYSpinEdit";
            this.intYSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intYSpinEdit.Properties.IsFloatValue = false;
            this.intYSpinEdit.Properties.Mask.EditMask = "N00";
            this.intYSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.intYSpinEdit.Properties.MinValue = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.intYSpinEdit.Size = new System.Drawing.Size(407, 20);
            this.intYSpinEdit.StyleController = this.dataLayoutControl1;
            this.intYSpinEdit.TabIndex = 45;
            // 
            // decAreaHaSpinEdit
            // 
            this.decAreaHaSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "decAreaHa", true));
            this.decAreaHaSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.decAreaHaSpinEdit.Location = new System.Drawing.Point(155, 438);
            this.decAreaHaSpinEdit.MenuManager = this.barManager1;
            this.decAreaHaSpinEdit.Name = "decAreaHaSpinEdit";
            this.decAreaHaSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.decAreaHaSpinEdit.Properties.Mask.EditMask = "######0.00 M�";
            this.decAreaHaSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.decAreaHaSpinEdit.Size = new System.Drawing.Size(407, 20);
            this.decAreaHaSpinEdit.StyleController = this.dataLayoutControl1;
            this.decAreaHaSpinEdit.TabIndex = 46;
            // 
            // intReplantCountSpinEdit
            // 
            this.intReplantCountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intReplantCount", true));
            this.intReplantCountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intReplantCountSpinEdit.Location = new System.Drawing.Point(143, 160);
            this.intReplantCountSpinEdit.MenuManager = this.barManager1;
            this.intReplantCountSpinEdit.Name = "intReplantCountSpinEdit";
            this.intReplantCountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intReplantCountSpinEdit.Properties.IsFloatValue = false;
            this.intReplantCountSpinEdit.Properties.Mask.EditMask = "N00";
            this.intReplantCountSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.intReplantCountSpinEdit.Size = new System.Drawing.Size(453, 20);
            this.intReplantCountSpinEdit.StyleController = this.dataLayoutControl1;
            this.intReplantCountSpinEdit.TabIndex = 47;
            // 
            // dtSurveyDateDateEdit
            // 
            this.dtSurveyDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "dtSurveyDate", true));
            this.dtSurveyDateDateEdit.EditValue = null;
            this.dtSurveyDateDateEdit.Location = new System.Drawing.Point(408, 106);
            this.dtSurveyDateDateEdit.MenuManager = this.barManager1;
            this.dtSurveyDateDateEdit.Name = "dtSurveyDateDateEdit";
            this.dtSurveyDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dtSurveyDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear Date", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Clear Date", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dtSurveyDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtSurveyDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dtSurveyDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtSurveyDateDateEdit.Size = new System.Drawing.Size(146, 20);
            this.dtSurveyDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtSurveyDateDateEdit.TabIndex = 48;
            this.dtSurveyDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dtSurveyDateDateEdit_ButtonClick);
            // 
            // decRiskFactorSpinEdit
            // 
            this.decRiskFactorSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "decRiskFactor", true));
            this.decRiskFactorSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.decRiskFactorSpinEdit.Location = new System.Drawing.Point(143, 84);
            this.decRiskFactorSpinEdit.MenuManager = this.barManager1;
            this.decRiskFactorSpinEdit.Name = "decRiskFactorSpinEdit";
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Calculate risk Button - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to <b>Calculate</b> the <b>Risk</b> using the formula set in the Utiliti" +
    "es screen.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.decRiskFactorSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Calculate", -1, true, true, true, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", "calculate", superToolTip9, DevExpress.Utils.ToolTipAnchor.Default)});
            this.decRiskFactorSpinEdit.Properties.Mask.EditMask = "f2";
            this.decRiskFactorSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.decRiskFactorSpinEdit.Size = new System.Drawing.Size(410, 20);
            this.decRiskFactorSpinEdit.StyleController = this.dataLayoutControl1;
            this.decRiskFactorSpinEdit.TabIndex = 51;
            this.decRiskFactorSpinEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.decRiskFactorSpinEdit_ButtonClick);
            // 
            // CavatSpinEdit
            // 
            this.CavatSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "Cavat", true));
            this.CavatSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CavatSpinEdit.Location = new System.Drawing.Point(143, 646);
            this.CavatSpinEdit.MenuManager = this.barManager1;
            this.CavatSpinEdit.Name = "CavatSpinEdit";
            this.CavatSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CavatSpinEdit.Properties.Mask.EditMask = "c";
            this.CavatSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CavatSpinEdit.Size = new System.Drawing.Size(961, 20);
            this.CavatSpinEdit.StyleController = this.dataLayoutControl1;
            this.CavatSpinEdit.TabIndex = 59;
            // 
            // StemCountSpinEdit
            // 
            this.StemCountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "StemCount", true));
            this.StemCountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StemCountSpinEdit.Location = new System.Drawing.Point(686, 263);
            this.StemCountSpinEdit.MenuManager = this.barManager1;
            this.StemCountSpinEdit.Name = "StemCountSpinEdit";
            this.StemCountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StemCountSpinEdit.Properties.IsFloatValue = false;
            this.StemCountSpinEdit.Properties.Mask.EditMask = "N00";
            this.StemCountSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.StemCountSpinEdit.Size = new System.Drawing.Size(418, 20);
            this.StemCountSpinEdit.StyleController = this.dataLayoutControl1;
            this.StemCountSpinEdit.TabIndex = 60;
            // 
            // CrownNorthSpinEdit
            // 
            this.CrownNorthSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CrownNorth", true));
            this.CrownNorthSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CrownNorthSpinEdit.Location = new System.Drawing.Point(143, 196);
            this.CrownNorthSpinEdit.MenuManager = this.barManager1;
            this.CrownNorthSpinEdit.Name = "CrownNorthSpinEdit";
            this.CrownNorthSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CrownNorthSpinEdit.Properties.Mask.EditMask = "#0.0 metres";
            this.CrownNorthSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CrownNorthSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            65536});
            this.CrownNorthSpinEdit.Size = new System.Drawing.Size(417, 20);
            this.CrownNorthSpinEdit.StyleController = this.dataLayoutControl1;
            this.CrownNorthSpinEdit.TabIndex = 61;
            // 
            // CrownSouthSpinEdit
            // 
            this.CrownSouthSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CrownSouth", true));
            this.CrownSouthSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CrownSouthSpinEdit.Location = new System.Drawing.Point(683, 196);
            this.CrownSouthSpinEdit.MenuManager = this.barManager1;
            this.CrownSouthSpinEdit.Name = "CrownSouthSpinEdit";
            this.CrownSouthSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CrownSouthSpinEdit.Properties.Mask.EditMask = "#0.0 metres";
            this.CrownSouthSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CrownSouthSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            65536});
            this.CrownSouthSpinEdit.Size = new System.Drawing.Size(414, 20);
            this.CrownSouthSpinEdit.StyleController = this.dataLayoutControl1;
            this.CrownSouthSpinEdit.TabIndex = 62;
            // 
            // CrownEastSpinEdit
            // 
            this.CrownEastSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CrownEast", true));
            this.CrownEastSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CrownEastSpinEdit.Location = new System.Drawing.Point(683, 220);
            this.CrownEastSpinEdit.MenuManager = this.barManager1;
            this.CrownEastSpinEdit.Name = "CrownEastSpinEdit";
            this.CrownEastSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CrownEastSpinEdit.Properties.Mask.EditMask = "#0.0 metres";
            this.CrownEastSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CrownEastSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            65536});
            this.CrownEastSpinEdit.Size = new System.Drawing.Size(414, 20);
            this.CrownEastSpinEdit.StyleController = this.dataLayoutControl1;
            this.CrownEastSpinEdit.TabIndex = 63;
            // 
            // CrownWestSpinEdit
            // 
            this.CrownWestSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "CrownWest", true));
            this.CrownWestSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CrownWestSpinEdit.Location = new System.Drawing.Point(143, 220);
            this.CrownWestSpinEdit.MenuManager = this.barManager1;
            this.CrownWestSpinEdit.Name = "CrownWestSpinEdit";
            this.CrownWestSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CrownWestSpinEdit.Properties.Mask.EditMask = "#0.0 metres";
            this.CrownWestSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CrownWestSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            65536});
            this.CrownWestSpinEdit.Size = new System.Drawing.Size(417, 20);
            this.CrownWestSpinEdit.StyleController = this.dataLayoutControl1;
            this.CrownWestSpinEdit.TabIndex = 64;
            // 
            // SULESpinEdit
            // 
            this.SULESpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "SULE", true));
            this.SULESpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SULESpinEdit.Location = new System.Drawing.Point(143, 287);
            this.SULESpinEdit.MenuManager = this.barManager1;
            this.SULESpinEdit.Name = "SULESpinEdit";
            this.SULESpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SULESpinEdit.Properties.IsFloatValue = false;
            this.SULESpinEdit.Properties.Mask.EditMask = "N00";
            this.SULESpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.SULESpinEdit.Size = new System.Drawing.Size(420, 20);
            this.SULESpinEdit.StyleController = this.dataLayoutControl1;
            this.SULESpinEdit.TabIndex = 66;
            // 
            // intDBHRangeGridLookUpEdit
            // 
            this.intDBHRangeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intDBHRange", true));
            this.intDBHRangeGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intDBHRangeGridLookUpEdit.Location = new System.Drawing.Point(685, 110);
            this.intDBHRangeGridLookUpEdit.MenuManager = this.barManager1;
            this.intDBHRangeGridLookUpEdit.Name = "intDBHRangeGridLookUpEdit";
            this.intDBHRangeGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions9.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions10.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intDBHRangeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intDBHRangeGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intDBHRangeGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intDBHRangeGridLookUpEdit.Properties.NullText = "";
            this.intDBHRangeGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.intDBHRangeGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intDBHRangeGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intDBHRangeGridLookUpEdit.Size = new System.Drawing.Size(419, 22);
            this.intDBHRangeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intDBHRangeGridLookUpEdit.TabIndex = 55;
            this.intDBHRangeGridLookUpEdit.TabStop = false;
            this.intDBHRangeGridLookUpEdit.Tag = "136";
            // 
            // sp01372ATMultiplePicklistsWithBlanksBindingSource
            // 
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataMember = "sp01372_AT_Multiple_Picklists_With_Blanks";
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.colBandStart,
            this.colBandEnd});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn10;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsCustomization.AllowFilter = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowFilterEditor = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowMRUFilterList = false;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn11, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Picklist Type";
            this.gridColumn6.FieldName = "HeaderDescription";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 189;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Picklist ID";
            this.gridColumn7.FieldName = "HeaderID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 67;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Item Code";
            this.gridColumn8.FieldName = "ItemCode";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 58;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Description";
            this.gridColumn9.FieldName = "ItemDescription";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 0;
            this.gridColumn9.Width = 264;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Order";
            this.gridColumn11.FieldName = "Order";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 65;
            // 
            // colBandStart
            // 
            this.colBandStart.Caption = "Band Start";
            this.colBandStart.FieldName = "BandStart";
            this.colBandStart.Name = "colBandStart";
            this.colBandStart.Visible = true;
            this.colBandStart.VisibleIndex = 1;
            // 
            // colBandEnd
            // 
            this.colBandEnd.Caption = "Band End";
            this.colBandEnd.FieldName = "BandEnd";
            this.colBandEnd.Name = "colBandEnd";
            this.colBandEnd.Visible = true;
            this.colBandEnd.VisibleIndex = 2;
            // 
            // intHeightRangeGridLookUpEdit
            // 
            this.intHeightRangeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intHeightRange", true));
            this.intHeightRangeGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intHeightRangeGridLookUpEdit.Location = new System.Drawing.Point(685, 136);
            this.intHeightRangeGridLookUpEdit.MenuManager = this.barManager1;
            this.intHeightRangeGridLookUpEdit.Name = "intHeightRangeGridLookUpEdit";
            this.intHeightRangeGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions11.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions12.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intHeightRangeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intHeightRangeGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intHeightRangeGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intHeightRangeGridLookUpEdit.Properties.NullText = "";
            this.intHeightRangeGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.intHeightRangeGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intHeightRangeGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intHeightRangeGridLookUpEdit.Size = new System.Drawing.Size(419, 22);
            this.intHeightRangeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intHeightRangeGridLookUpEdit.TabIndex = 56;
            this.intHeightRangeGridLookUpEdit.TabStop = false;
            this.intHeightRangeGridLookUpEdit.Tag = "135";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.colBandStart1,
            this.colBandEnd1});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn16;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsFilter.AllowMRUFilterList = false;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn17, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Picklist Type";
            this.gridColumn12.FieldName = "HeaderDescription";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 189;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Picklist ID";
            this.gridColumn13.FieldName = "HeaderID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 67;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Item ID";
            this.gridColumn14.FieldName = "ItemCode";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 58;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Description";
            this.gridColumn15.FieldName = "ItemDescription";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 264;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Order";
            this.gridColumn17.FieldName = "Order";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 65;
            // 
            // colBandStart1
            // 
            this.colBandStart1.Caption = "Band Start";
            this.colBandStart1.FieldName = "BandStart";
            this.colBandStart1.Name = "colBandStart1";
            this.colBandStart1.Visible = true;
            this.colBandStart1.VisibleIndex = 1;
            // 
            // colBandEnd1
            // 
            this.colBandEnd1.Caption = "Band End";
            this.colBandEnd1.FieldName = "BandEnd";
            this.colBandEnd1.Name = "colBandEnd1";
            this.colBandEnd1.Visible = true;
            this.colBandEnd1.VisibleIndex = 2;
            // 
            // intSpeciesIDGridLookUpEdit
            // 
            this.intSpeciesIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intSpeciesID", true));
            this.intSpeciesIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intSpeciesIDGridLookUpEdit.Location = new System.Drawing.Point(143, 58);
            this.intSpeciesIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intSpeciesIDGridLookUpEdit.Name = "intSpeciesIDGridLookUpEdit";
            editorButtonImageOptions13.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions14.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intSpeciesIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions13, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions14, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject53, serializableAppearanceObject54, serializableAppearanceObject55, serializableAppearanceObject56, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intSpeciesIDGridLookUpEdit.Properties.DataSource = this.sp00179SpeciesListWithBlankBindingSource;
            this.intSpeciesIDGridLookUpEdit.Properties.DisplayMember = "SpeciesName";
            this.intSpeciesIDGridLookUpEdit.Properties.NullText = "";
            this.intSpeciesIDGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.intSpeciesIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit1});
            this.intSpeciesIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intSpeciesIDGridLookUpEdit.Properties.ValueMember = "SpeciesID";
            this.intSpeciesIDGridLookUpEdit.Size = new System.Drawing.Size(419, 22);
            this.intSpeciesIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intSpeciesIDGridLookUpEdit.TabIndex = 12;
            this.intSpeciesIDGridLookUpEdit.TabStop = false;
            this.intSpeciesIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intSpeciesIDGridLookUpEdit_ButtonClick);
            this.intSpeciesIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.intSpeciesIDGridLookUpEdit_EditValueChanged);
            this.intSpeciesIDGridLookUpEdit.Validated += new System.EventHandler(this.intSpeciesIDGridLookUpEdit_Validated);
            // 
            // sp00179SpeciesListWithBlankBindingSource
            // 
            this.sp00179SpeciesListWithBlankBindingSource.DataMember = "sp00179_Species_List_With_Blank";
            this.sp00179SpeciesListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSpeciesCode,
            this.colSpeciesID,
            this.colSpeciesName,
            this.colSpeciesOrder,
            this.colSpeciesRemark,
            this.colSpeciesRiskFactor,
            this.colSpeciesScientificName,
            this.colSpeciesType,
            this.colAmenityArbVisible,
            this.colUtilityArbVisible});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colSpeciesID;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSpeciesOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSpeciesName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colSpeciesCode
            // 
            this.colSpeciesCode.Caption = "Code";
            this.colSpeciesCode.FieldName = "SpeciesCode";
            this.colSpeciesCode.Name = "colSpeciesCode";
            this.colSpeciesCode.OptionsColumn.AllowEdit = false;
            this.colSpeciesCode.OptionsColumn.AllowFocus = false;
            this.colSpeciesCode.OptionsColumn.ReadOnly = true;
            this.colSpeciesCode.Width = 147;
            // 
            // colSpeciesName
            // 
            this.colSpeciesName.Caption = "Name";
            this.colSpeciesName.FieldName = "SpeciesName";
            this.colSpeciesName.Name = "colSpeciesName";
            this.colSpeciesName.OptionsColumn.AllowEdit = false;
            this.colSpeciesName.OptionsColumn.AllowFocus = false;
            this.colSpeciesName.OptionsColumn.ReadOnly = true;
            this.colSpeciesName.Visible = true;
            this.colSpeciesName.VisibleIndex = 0;
            this.colSpeciesName.Width = 212;
            // 
            // colSpeciesOrder
            // 
            this.colSpeciesOrder.Caption = "Order";
            this.colSpeciesOrder.FieldName = "SpeciesOrder";
            this.colSpeciesOrder.Name = "colSpeciesOrder";
            this.colSpeciesOrder.OptionsColumn.AllowEdit = false;
            this.colSpeciesOrder.OptionsColumn.AllowFocus = false;
            this.colSpeciesOrder.OptionsColumn.ReadOnly = true;
            this.colSpeciesOrder.Width = 62;
            // 
            // colSpeciesRemark
            // 
            this.colSpeciesRemark.Caption = "Remarks";
            this.colSpeciesRemark.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colSpeciesRemark.FieldName = "SpeciesRemark";
            this.colSpeciesRemark.Name = "colSpeciesRemark";
            this.colSpeciesRemark.OptionsColumn.ReadOnly = true;
            this.colSpeciesRemark.Visible = true;
            this.colSpeciesRemark.VisibleIndex = 4;
            this.colSpeciesRemark.Width = 143;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colSpeciesRiskFactor
            // 
            this.colSpeciesRiskFactor.Caption = "Risk Factor";
            this.colSpeciesRiskFactor.FieldName = "SpeciesRiskFactor";
            this.colSpeciesRiskFactor.Name = "colSpeciesRiskFactor";
            this.colSpeciesRiskFactor.OptionsColumn.AllowEdit = false;
            this.colSpeciesRiskFactor.OptionsColumn.AllowFocus = false;
            this.colSpeciesRiskFactor.OptionsColumn.ReadOnly = true;
            this.colSpeciesRiskFactor.Width = 74;
            // 
            // colSpeciesScientificName
            // 
            this.colSpeciesScientificName.Caption = "Scientific Name";
            this.colSpeciesScientificName.FieldName = "SpeciesScientificName";
            this.colSpeciesScientificName.Name = "colSpeciesScientificName";
            this.colSpeciesScientificName.OptionsColumn.AllowEdit = false;
            this.colSpeciesScientificName.OptionsColumn.AllowFocus = false;
            this.colSpeciesScientificName.OptionsColumn.ReadOnly = true;
            this.colSpeciesScientificName.Visible = true;
            this.colSpeciesScientificName.VisibleIndex = 1;
            this.colSpeciesScientificName.Width = 205;
            // 
            // colSpeciesType
            // 
            this.colSpeciesType.Caption = "Type";
            this.colSpeciesType.FieldName = "SpeciesType";
            this.colSpeciesType.Name = "colSpeciesType";
            this.colSpeciesType.OptionsColumn.AllowEdit = false;
            this.colSpeciesType.OptionsColumn.AllowFocus = false;
            this.colSpeciesType.OptionsColumn.ReadOnly = true;
            this.colSpeciesType.Width = 101;
            // 
            // colAmenityArbVisible
            // 
            this.colAmenityArbVisible.Caption = "Amenity Arb";
            this.colAmenityArbVisible.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAmenityArbVisible.FieldName = "AmenityArbVisible";
            this.colAmenityArbVisible.Name = "colAmenityArbVisible";
            this.colAmenityArbVisible.OptionsColumn.AllowEdit = false;
            this.colAmenityArbVisible.OptionsColumn.AllowFocus = false;
            this.colAmenityArbVisible.OptionsColumn.ReadOnly = true;
            this.colAmenityArbVisible.Visible = true;
            this.colAmenityArbVisible.VisibleIndex = 2;
            this.colAmenityArbVisible.Width = 78;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colUtilityArbVisible
            // 
            this.colUtilityArbVisible.Caption = "Utility Arb";
            this.colUtilityArbVisible.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colUtilityArbVisible.FieldName = "UtilityArbVisible";
            this.colUtilityArbVisible.Name = "colUtilityArbVisible";
            this.colUtilityArbVisible.OptionsColumn.AllowEdit = false;
            this.colUtilityArbVisible.OptionsColumn.AllowFocus = false;
            this.colUtilityArbVisible.OptionsColumn.ReadOnly = true;
            this.colUtilityArbVisible.Visible = true;
            this.colUtilityArbVisible.VisibleIndex = 3;
            this.colUtilityArbVisible.Width = 66;
            // 
            // intStatusGridLookUpEdit
            // 
            this.intStatusGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intStatus", true));
            this.intStatusGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intStatusGridLookUpEdit.Location = new System.Drawing.Point(143, -26);
            this.intStatusGridLookUpEdit.MenuManager = this.barManager1;
            this.intStatusGridLookUpEdit.Name = "intStatusGridLookUpEdit";
            this.intStatusGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.intStatusGridLookUpEdit.Properties.DataSource = this.sp01371ATObjectStatusesNoBlankBindingSource;
            this.intStatusGridLookUpEdit.Properties.DisplayMember = "Description";
            this.intStatusGridLookUpEdit.Properties.NullText = "";
            this.intStatusGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.intStatusGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intStatusGridLookUpEdit.Properties.ValueMember = "ID";
            this.intStatusGridLookUpEdit.Size = new System.Drawing.Size(419, 20);
            this.intStatusGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intStatusGridLookUpEdit.TabIndex = 36;
            this.intStatusGridLookUpEdit.TabStop = false;
            this.intStatusGridLookUpEdit.EditValueChanged += new System.EventHandler(this.intStatusGridLookUpEdit_EditValueChanged);
            this.intStatusGridLookUpEdit.Validated += new System.EventHandler(this.intStatusGridLookUpEdit_Validated);
            // 
            // sp01371ATObjectStatusesNoBlankBindingSource
            // 
            this.sp01371ATObjectStatusesNoBlankBindingSource.DataMember = "sp01371_AT_Object_Statuses_No_Blank";
            this.sp01371ATObjectStatusesNoBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ID";
            this.gridColumn3.FieldName = "ID";
            this.gridColumn3.Name = "gridColumn3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Status";
            this.gridColumn4.FieldName = "Description";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 265;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Order";
            this.gridColumn5.FieldName = "Order";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            // 
            // strTreeRefButtonEdit
            // 
            this.strTreeRefButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strTreeRef", true));
            this.strTreeRefButtonEdit.Location = new System.Drawing.Point(143, -50);
            this.strTreeRefButtonEdit.MenuManager = this.barManager1;
            this.strTreeRefButtonEdit.Name = "strTreeRefButtonEdit";
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Sequence Button - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            superToolTip11.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem11.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "Number Button - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.strTreeRefButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions15, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject57, serializableAppearanceObject58, serializableAppearanceObject59, serializableAppearanceObject60, "", "Sequence", superToolTip10, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject61, serializableAppearanceObject62, serializableAppearanceObject63, serializableAppearanceObject64, "", "Number", superToolTip11, DevExpress.Utils.ToolTipAnchor.Default)});
            this.strTreeRefButtonEdit.Properties.MaxLength = 20;
            this.strTreeRefButtonEdit.Size = new System.Drawing.Size(419, 20);
            this.strTreeRefButtonEdit.StyleController = this.dataLayoutControl1;
            this.strTreeRefButtonEdit.TabIndex = 7;
            this.strTreeRefButtonEdit.TabStop = false;
            this.strTreeRefButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.strTreeRefButtonEdit_ButtonClick);
            this.strTreeRefButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strTreeRefButtonEdit_Validating);
            // 
            // intLegalStatusGridLookUpEdit
            // 
            this.intLegalStatusGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intLegalStatus", true));
            this.intLegalStatusGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intLegalStatusGridLookUpEdit.Location = new System.Drawing.Point(143, 237);
            this.intLegalStatusGridLookUpEdit.MenuManager = this.barManager1;
            this.intLegalStatusGridLookUpEdit.Name = "intLegalStatusGridLookUpEdit";
            this.intLegalStatusGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions17.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions18.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intLegalStatusGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions17, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject65, serializableAppearanceObject66, serializableAppearanceObject67, serializableAppearanceObject68, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions18, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject69, serializableAppearanceObject70, serializableAppearanceObject71, serializableAppearanceObject72, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intLegalStatusGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intLegalStatusGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intLegalStatusGridLookUpEdit.Properties.NullText = "";
            this.intLegalStatusGridLookUpEdit.Properties.PopupView = this.gridView5;
            this.intLegalStatusGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intLegalStatusGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intLegalStatusGridLookUpEdit.Size = new System.Drawing.Size(420, 22);
            this.intLegalStatusGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intLegalStatusGridLookUpEdit.TabIndex = 17;
            this.intLegalStatusGridLookUpEdit.TabStop = false;
            this.intLegalStatusGridLookUpEdit.Tag = "114";
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.gridColumn22;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridView5.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsCustomization.AllowFilter = false;
            this.gridView5.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowFilterEditor = false;
            this.gridView5.OptionsFilter.AllowMRUFilterList = false;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn23, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Picklist Type";
            this.gridColumn18.FieldName = "HeaderDescription";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 189;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Picklist ID";
            this.gridColumn19.FieldName = "HeaderID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 67;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Item ID";
            this.gridColumn20.FieldName = "ItemCode";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 58;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Description";
            this.gridColumn21.FieldName = "ItemDescription";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 0;
            this.gridColumn21.Width = 264;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Order";
            this.gridColumn23.FieldName = "Order";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 65;
            // 
            // intSafetyPriorityGridLookUpEdit
            // 
            this.intSafetyPriorityGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intSafetyPriority", true));
            this.intSafetyPriorityGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intSafetyPriorityGridLookUpEdit.Location = new System.Drawing.Point(143, 58);
            this.intSafetyPriorityGridLookUpEdit.MenuManager = this.barManager1;
            this.intSafetyPriorityGridLookUpEdit.Name = "intSafetyPriorityGridLookUpEdit";
            this.intSafetyPriorityGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions19.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions20.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intSafetyPriorityGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions19, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject73, serializableAppearanceObject74, serializableAppearanceObject75, serializableAppearanceObject76, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions20, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject77, serializableAppearanceObject78, serializableAppearanceObject79, serializableAppearanceObject80, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intSafetyPriorityGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intSafetyPriorityGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intSafetyPriorityGridLookUpEdit.Properties.NullText = "";
            this.intSafetyPriorityGridLookUpEdit.Properties.PopupView = this.gridView6;
            this.intSafetyPriorityGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intSafetyPriorityGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intSafetyPriorityGridLookUpEdit.Size = new System.Drawing.Size(410, 22);
            this.intSafetyPriorityGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intSafetyPriorityGridLookUpEdit.TabIndex = 29;
            this.intSafetyPriorityGridLookUpEdit.TabStop = false;
            this.intSafetyPriorityGridLookUpEdit.Tag = "103";
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.gridColumn40;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView6.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView6.OptionsCustomization.AllowFilter = false;
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn41, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Picklist Type";
            this.gridColumn36.FieldName = "HeaderDescription";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 189;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Picklist ID";
            this.gridColumn37.FieldName = "HeaderID";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Width = 67;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Item ID";
            this.gridColumn38.FieldName = "ItemCode";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 58;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Description";
            this.gridColumn39.FieldName = "ItemDescription";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 0;
            this.gridColumn39.Width = 264;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Order";
            this.gridColumn41.FieldName = "Order";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Width = 65;
            // 
            // intSiteHazardClassGridLookUpEdit
            // 
            this.intSiteHazardClassGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intSiteHazardClass", true));
            this.intSiteHazardClassGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intSiteHazardClassGridLookUpEdit.Location = new System.Drawing.Point(143, 108);
            this.intSiteHazardClassGridLookUpEdit.MenuManager = this.barManager1;
            this.intSiteHazardClassGridLookUpEdit.Name = "intSiteHazardClassGridLookUpEdit";
            this.intSiteHazardClassGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions21.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions22.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intSiteHazardClassGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions21, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject81, serializableAppearanceObject82, serializableAppearanceObject83, serializableAppearanceObject84, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions22, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject85, serializableAppearanceObject86, serializableAppearanceObject87, serializableAppearanceObject88, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intSiteHazardClassGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intSiteHazardClassGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intSiteHazardClassGridLookUpEdit.Properties.NullText = "";
            this.intSiteHazardClassGridLookUpEdit.Properties.PopupView = this.gridView7;
            this.intSiteHazardClassGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intSiteHazardClassGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intSiteHazardClassGridLookUpEdit.Size = new System.Drawing.Size(410, 22);
            this.intSiteHazardClassGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intSiteHazardClassGridLookUpEdit.TabIndex = 30;
            this.intSiteHazardClassGridLookUpEdit.TabStop = false;
            this.intSiteHazardClassGridLookUpEdit.Tag = "118";
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition7.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition7.Appearance.Options.UseForeColor = true;
            styleFormatCondition7.ApplyToRow = true;
            styleFormatCondition7.Column = this.gridColumn46;
            styleFormatCondition7.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition7.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition7});
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView7.OptionsCustomization.AllowFilter = false;
            this.gridView7.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView7.OptionsFilter.AllowFilterEditor = false;
            this.gridView7.OptionsFilter.AllowMRUFilterList = false;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn47, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Picklist Type";
            this.gridColumn42.FieldName = "HeaderDescription";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Width = 189;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Picklist ID";
            this.gridColumn43.FieldName = "HeaderID";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Width = 67;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Item ID";
            this.gridColumn44.FieldName = "ItemCode";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Width = 58;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Description";
            this.gridColumn45.FieldName = "ItemDescription";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 0;
            this.gridColumn45.Width = 264;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Order";
            this.gridColumn47.FieldName = "Order";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Width = 65;
            // 
            // strPolygonXYTextEdit
            // 
            this.strPolygonXYTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strPolygonXY", true));
            this.strPolygonXYTextEdit.Location = new System.Drawing.Point(155, 318);
            this.strPolygonXYTextEdit.MenuManager = this.barManager1;
            this.strPolygonXYTextEdit.Name = "strPolygonXYTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strPolygonXYTextEdit, true);
            this.strPolygonXYTextEdit.Size = new System.Drawing.Size(407, 92);
            this.scSpellChecker.SetSpellCheckerOptions(this.strPolygonXYTextEdit, optionsSpelling22);
            this.strPolygonXYTextEdit.StyleController = this.dataLayoutControl1;
            this.strPolygonXYTextEdit.TabIndex = 52;
            this.strPolygonXYTextEdit.TabStop = false;
            this.strPolygonXYTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strPolygonXYTextEdit_Validating);
            // 
            // intPlantSourceGridLookUpEdit
            // 
            this.intPlantSourceGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intPlantSource", true));
            this.intPlantSourceGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intPlantSourceGridLookUpEdit.Location = new System.Drawing.Point(143, 108);
            this.intPlantSourceGridLookUpEdit.MenuManager = this.barManager1;
            this.intPlantSourceGridLookUpEdit.Name = "intPlantSourceGridLookUpEdit";
            this.intPlantSourceGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions23.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions24.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intPlantSourceGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions23, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject89, serializableAppearanceObject90, serializableAppearanceObject91, serializableAppearanceObject92, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject93, serializableAppearanceObject94, serializableAppearanceObject95, serializableAppearanceObject96, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intPlantSourceGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intPlantSourceGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intPlantSourceGridLookUpEdit.Properties.NullText = "";
            this.intPlantSourceGridLookUpEdit.Properties.PopupView = this.gridView9;
            this.intPlantSourceGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intPlantSourceGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intPlantSourceGridLookUpEdit.Size = new System.Drawing.Size(453, 22);
            this.intPlantSourceGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intPlantSourceGridLookUpEdit.TabIndex = 32;
            this.intPlantSourceGridLookUpEdit.TabStop = false;
            this.intPlantSourceGridLookUpEdit.Tag = "121";
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn84,
            this.gridColumn85,
            this.gridColumn86,
            this.gridColumn87,
            this.gridColumn88,
            this.gridColumn89});
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition8.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition8.Appearance.Options.UseForeColor = true;
            styleFormatCondition8.ApplyToRow = true;
            styleFormatCondition8.Column = this.gridColumn88;
            styleFormatCondition8.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition8.Value1 = 0;
            this.gridView9.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition8});
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView9.OptionsCustomization.AllowFilter = false;
            this.gridView9.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowFilterEditor = false;
            this.gridView9.OptionsFilter.AllowMRUFilterList = false;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView9.OptionsView.ShowIndicator = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn89, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Picklist Type";
            this.gridColumn84.FieldName = "HeaderDescription";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.Width = 189;
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Picklist ID";
            this.gridColumn85.FieldName = "HeaderID";
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.Width = 67;
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Item ID";
            this.gridColumn86.FieldName = "ItemCode";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            this.gridColumn86.Width = 58;
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Description";
            this.gridColumn87.FieldName = "ItemDescription";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.AllowEdit = false;
            this.gridColumn87.OptionsColumn.AllowFocus = false;
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.Visible = true;
            this.gridColumn87.VisibleIndex = 0;
            this.gridColumn87.Width = 264;
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Order";
            this.gridColumn89.FieldName = "Order";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Width = 65;
            // 
            // intPlantMethodGridLookUpEdit
            // 
            this.intPlantMethodGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intPlantMethod", true));
            this.intPlantMethodGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intPlantMethodGridLookUpEdit.Location = new System.Drawing.Point(143, 134);
            this.intPlantMethodGridLookUpEdit.MenuManager = this.barManager1;
            this.intPlantMethodGridLookUpEdit.Name = "intPlantMethodGridLookUpEdit";
            this.intPlantMethodGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions25.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions26.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intPlantMethodGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions25, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject97, serializableAppearanceObject98, serializableAppearanceObject99, serializableAppearanceObject100, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions26, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject101, serializableAppearanceObject102, serializableAppearanceObject103, serializableAppearanceObject104, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intPlantMethodGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intPlantMethodGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intPlantMethodGridLookUpEdit.Properties.NullText = "";
            this.intPlantMethodGridLookUpEdit.Properties.PopupView = this.gridView10;
            this.intPlantMethodGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intPlantMethodGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intPlantMethodGridLookUpEdit.Size = new System.Drawing.Size(453, 22);
            this.intPlantMethodGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intPlantMethodGridLookUpEdit.TabIndex = 33;
            this.intPlantMethodGridLookUpEdit.TabStop = false;
            this.intPlantMethodGridLookUpEdit.Tag = "120";
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn90,
            this.gridColumn91,
            this.gridColumn92,
            this.gridColumn93,
            this.gridColumn94,
            this.gridColumn95});
            this.gridView10.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition9.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition9.Appearance.Options.UseForeColor = true;
            styleFormatCondition9.ApplyToRow = true;
            styleFormatCondition9.Column = this.gridColumn94;
            styleFormatCondition9.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition9.Value1 = 0;
            this.gridView10.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition9});
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView10.OptionsCustomization.AllowFilter = false;
            this.gridView10.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView10.OptionsFilter.AllowFilterEditor = false;
            this.gridView10.OptionsFilter.AllowMRUFilterList = false;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView10.OptionsView.ShowIndicator = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn95, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Picklist Type";
            this.gridColumn90.FieldName = "HeaderDescription";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Width = 189;
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "Picklist ID";
            this.gridColumn91.FieldName = "HeaderID";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            this.gridColumn91.Width = 67;
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "Item ID";
            this.gridColumn92.FieldName = "ItemCode";
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            this.gridColumn92.Width = 58;
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "Description";
            this.gridColumn93.FieldName = "ItemDescription";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.Visible = true;
            this.gridColumn93.VisibleIndex = 0;
            this.gridColumn93.Width = 264;
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Order";
            this.gridColumn95.FieldName = "Order";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.AllowEdit = false;
            this.gridColumn95.OptionsColumn.AllowFocus = false;
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            this.gridColumn95.Width = 65;
            // 
            // UserPickList1GridLookUpEdit
            // 
            this.UserPickList1GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "UserPickList1", true));
            this.UserPickList1GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList1GridLookUpEdit.Location = new System.Drawing.Point(143, 130);
            this.UserPickList1GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList1GridLookUpEdit.Name = "UserPickList1GridLookUpEdit";
            this.UserPickList1GridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions27.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions28.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPickList1GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions27, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject105, serializableAppearanceObject106, serializableAppearanceObject107, serializableAppearanceObject108, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions28, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject109, serializableAppearanceObject110, serializableAppearanceObject111, serializableAppearanceObject112, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPickList1GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList1GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList1GridLookUpEdit.Properties.NullText = "";
            this.UserPickList1GridLookUpEdit.Properties.PopupView = this.gridView11;
            this.UserPickList1GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList1GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList1GridLookUpEdit.Size = new System.Drawing.Size(438, 22);
            this.UserPickList1GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList1GridLookUpEdit.TabIndex = 67;
            this.UserPickList1GridLookUpEdit.TabStop = false;
            this.UserPickList1GridLookUpEdit.Tag = "141";
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn108,
            this.gridColumn109,
            this.gridColumn110,
            this.gridColumn111,
            this.gridColumn112,
            this.gridColumn113});
            this.gridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition10.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition10.Appearance.Options.UseForeColor = true;
            styleFormatCondition10.ApplyToRow = true;
            styleFormatCondition10.Column = this.gridColumn112;
            styleFormatCondition10.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition10.Value1 = 0;
            this.gridView11.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition10});
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView11.OptionsCustomization.AllowFilter = false;
            this.gridView11.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView11.OptionsFilter.AllowFilterEditor = false;
            this.gridView11.OptionsFilter.AllowMRUFilterList = false;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView11.OptionsView.ShowIndicator = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn113, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn108
            // 
            this.gridColumn108.Caption = "Picklist Type";
            this.gridColumn108.FieldName = "HeaderDescription";
            this.gridColumn108.Name = "gridColumn108";
            this.gridColumn108.OptionsColumn.AllowEdit = false;
            this.gridColumn108.OptionsColumn.AllowFocus = false;
            this.gridColumn108.OptionsColumn.ReadOnly = true;
            this.gridColumn108.Width = 189;
            // 
            // gridColumn109
            // 
            this.gridColumn109.Caption = "Picklist ID";
            this.gridColumn109.FieldName = "HeaderID";
            this.gridColumn109.Name = "gridColumn109";
            this.gridColumn109.OptionsColumn.AllowEdit = false;
            this.gridColumn109.OptionsColumn.AllowFocus = false;
            this.gridColumn109.OptionsColumn.ReadOnly = true;
            this.gridColumn109.Width = 67;
            // 
            // gridColumn110
            // 
            this.gridColumn110.Caption = "Item ID";
            this.gridColumn110.FieldName = "ItemCode";
            this.gridColumn110.Name = "gridColumn110";
            this.gridColumn110.OptionsColumn.AllowEdit = false;
            this.gridColumn110.OptionsColumn.AllowFocus = false;
            this.gridColumn110.OptionsColumn.ReadOnly = true;
            this.gridColumn110.Width = 58;
            // 
            // gridColumn111
            // 
            this.gridColumn111.Caption = "Description";
            this.gridColumn111.FieldName = "ItemDescription";
            this.gridColumn111.Name = "gridColumn111";
            this.gridColumn111.OptionsColumn.AllowEdit = false;
            this.gridColumn111.OptionsColumn.AllowFocus = false;
            this.gridColumn111.OptionsColumn.ReadOnly = true;
            this.gridColumn111.Visible = true;
            this.gridColumn111.VisibleIndex = 0;
            this.gridColumn111.Width = 264;
            // 
            // gridColumn113
            // 
            this.gridColumn113.Caption = "Order";
            this.gridColumn113.FieldName = "Order";
            this.gridColumn113.Name = "gridColumn113";
            this.gridColumn113.OptionsColumn.AllowEdit = false;
            this.gridColumn113.OptionsColumn.AllowFocus = false;
            this.gridColumn113.OptionsColumn.ReadOnly = true;
            this.gridColumn113.Width = 65;
            // 
            // UserPickList2GridLookUpEdit
            // 
            this.UserPickList2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "UserPickList2", true));
            this.UserPickList2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList2GridLookUpEdit.Location = new System.Drawing.Point(143, 156);
            this.UserPickList2GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList2GridLookUpEdit.Name = "UserPickList2GridLookUpEdit";
            this.UserPickList2GridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions29.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions30.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPickList2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions29, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject113, serializableAppearanceObject114, serializableAppearanceObject115, serializableAppearanceObject116, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions30, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject117, serializableAppearanceObject118, serializableAppearanceObject119, serializableAppearanceObject120, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPickList2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList2GridLookUpEdit.Properties.NullText = "";
            this.UserPickList2GridLookUpEdit.Properties.PopupView = this.gridView12;
            this.UserPickList2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList2GridLookUpEdit.Size = new System.Drawing.Size(438, 22);
            this.UserPickList2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList2GridLookUpEdit.TabIndex = 68;
            this.UserPickList2GridLookUpEdit.TabStop = false;
            this.UserPickList2GridLookUpEdit.Tag = "142";
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn114,
            this.gridColumn115,
            this.gridColumn116,
            this.gridColumn117,
            this.gridColumn118,
            this.gridColumn119});
            this.gridView12.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition11.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition11.Appearance.Options.UseForeColor = true;
            styleFormatCondition11.ApplyToRow = true;
            styleFormatCondition11.Column = this.gridColumn118;
            styleFormatCondition11.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition11.Value1 = 0;
            this.gridView12.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition11});
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView12.OptionsCustomization.AllowFilter = false;
            this.gridView12.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView12.OptionsFilter.AllowFilterEditor = false;
            this.gridView12.OptionsFilter.AllowMRUFilterList = false;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView12.OptionsView.ShowIndicator = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn119, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn114
            // 
            this.gridColumn114.Caption = "Picklist Type";
            this.gridColumn114.FieldName = "HeaderDescription";
            this.gridColumn114.Name = "gridColumn114";
            this.gridColumn114.OptionsColumn.AllowEdit = false;
            this.gridColumn114.OptionsColumn.AllowFocus = false;
            this.gridColumn114.OptionsColumn.ReadOnly = true;
            this.gridColumn114.Width = 189;
            // 
            // gridColumn115
            // 
            this.gridColumn115.Caption = "Picklist ID";
            this.gridColumn115.FieldName = "HeaderID";
            this.gridColumn115.Name = "gridColumn115";
            this.gridColumn115.OptionsColumn.AllowEdit = false;
            this.gridColumn115.OptionsColumn.AllowFocus = false;
            this.gridColumn115.OptionsColumn.ReadOnly = true;
            this.gridColumn115.Width = 67;
            // 
            // gridColumn116
            // 
            this.gridColumn116.Caption = "Item ID";
            this.gridColumn116.FieldName = "ItemCode";
            this.gridColumn116.Name = "gridColumn116";
            this.gridColumn116.OptionsColumn.AllowEdit = false;
            this.gridColumn116.OptionsColumn.AllowFocus = false;
            this.gridColumn116.OptionsColumn.ReadOnly = true;
            this.gridColumn116.Width = 58;
            // 
            // gridColumn117
            // 
            this.gridColumn117.Caption = "Description";
            this.gridColumn117.FieldName = "ItemDescription";
            this.gridColumn117.Name = "gridColumn117";
            this.gridColumn117.OptionsColumn.AllowEdit = false;
            this.gridColumn117.OptionsColumn.AllowFocus = false;
            this.gridColumn117.OptionsColumn.ReadOnly = true;
            this.gridColumn117.Visible = true;
            this.gridColumn117.VisibleIndex = 0;
            this.gridColumn117.Width = 264;
            // 
            // gridColumn119
            // 
            this.gridColumn119.Caption = "Order";
            this.gridColumn119.FieldName = "Order";
            this.gridColumn119.Name = "gridColumn119";
            this.gridColumn119.OptionsColumn.AllowEdit = false;
            this.gridColumn119.OptionsColumn.AllowFocus = false;
            this.gridColumn119.OptionsColumn.ReadOnly = true;
            this.gridColumn119.Width = 65;
            // 
            // UserPickList3GridLookUpEdit
            // 
            this.UserPickList3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "UserPickList3", true));
            this.UserPickList3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList3GridLookUpEdit.Location = new System.Drawing.Point(143, 182);
            this.UserPickList3GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList3GridLookUpEdit.Name = "UserPickList3GridLookUpEdit";
            this.UserPickList3GridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions31.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions32.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPickList3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions31, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject121, serializableAppearanceObject122, serializableAppearanceObject123, serializableAppearanceObject124, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions32, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject125, serializableAppearanceObject126, serializableAppearanceObject127, serializableAppearanceObject128, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPickList3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList3GridLookUpEdit.Properties.NullText = "";
            this.UserPickList3GridLookUpEdit.Properties.PopupView = this.gridView13;
            this.UserPickList3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList3GridLookUpEdit.Size = new System.Drawing.Size(438, 22);
            this.UserPickList3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList3GridLookUpEdit.TabIndex = 69;
            this.UserPickList3GridLookUpEdit.TabStop = false;
            this.UserPickList3GridLookUpEdit.Tag = "143";
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn120,
            this.gridColumn121,
            this.gridColumn122,
            this.gridColumn123,
            this.gridColumn124,
            this.gridColumn125});
            this.gridView13.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition12.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition12.Appearance.Options.UseForeColor = true;
            styleFormatCondition12.ApplyToRow = true;
            styleFormatCondition12.Column = this.gridColumn124;
            styleFormatCondition12.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition12.Value1 = 0;
            this.gridView13.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition12});
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView13.OptionsCustomization.AllowFilter = false;
            this.gridView13.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView13.OptionsFilter.AllowFilterEditor = false;
            this.gridView13.OptionsFilter.AllowMRUFilterList = false;
            this.gridView13.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView13.OptionsLayout.StoreAppearance = true;
            this.gridView13.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView13.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView13.OptionsView.ColumnAutoWidth = false;
            this.gridView13.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView13.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            this.gridView13.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView13.OptionsView.ShowIndicator = false;
            this.gridView13.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn125, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn120
            // 
            this.gridColumn120.Caption = "Picklist Type";
            this.gridColumn120.FieldName = "HeaderDescription";
            this.gridColumn120.Name = "gridColumn120";
            this.gridColumn120.OptionsColumn.AllowEdit = false;
            this.gridColumn120.OptionsColumn.AllowFocus = false;
            this.gridColumn120.OptionsColumn.ReadOnly = true;
            this.gridColumn120.Width = 189;
            // 
            // gridColumn121
            // 
            this.gridColumn121.Caption = "Picklist ID";
            this.gridColumn121.FieldName = "HeaderID";
            this.gridColumn121.Name = "gridColumn121";
            this.gridColumn121.OptionsColumn.AllowEdit = false;
            this.gridColumn121.OptionsColumn.AllowFocus = false;
            this.gridColumn121.OptionsColumn.ReadOnly = true;
            this.gridColumn121.Width = 67;
            // 
            // gridColumn122
            // 
            this.gridColumn122.Caption = "Item ID";
            this.gridColumn122.FieldName = "ItemCode";
            this.gridColumn122.Name = "gridColumn122";
            this.gridColumn122.OptionsColumn.AllowEdit = false;
            this.gridColumn122.OptionsColumn.AllowFocus = false;
            this.gridColumn122.OptionsColumn.ReadOnly = true;
            this.gridColumn122.Width = 58;
            // 
            // gridColumn123
            // 
            this.gridColumn123.Caption = "Description";
            this.gridColumn123.FieldName = "ItemDescription";
            this.gridColumn123.Name = "gridColumn123";
            this.gridColumn123.OptionsColumn.AllowEdit = false;
            this.gridColumn123.OptionsColumn.AllowFocus = false;
            this.gridColumn123.OptionsColumn.ReadOnly = true;
            this.gridColumn123.Visible = true;
            this.gridColumn123.VisibleIndex = 0;
            this.gridColumn123.Width = 264;
            // 
            // gridColumn125
            // 
            this.gridColumn125.Caption = "Order";
            this.gridColumn125.FieldName = "Order";
            this.gridColumn125.Name = "gridColumn125";
            this.gridColumn125.OptionsColumn.AllowEdit = false;
            this.gridColumn125.OptionsColumn.AllowFocus = false;
            this.gridColumn125.OptionsColumn.ReadOnly = true;
            this.gridColumn125.Width = 65;
            // 
            // intAccessGridLookUpEdit
            // 
            this.intAccessGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intAccess", true));
            this.intAccessGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intAccessGridLookUpEdit.Location = new System.Drawing.Point(143, 176);
            this.intAccessGridLookUpEdit.MenuManager = this.barManager1;
            this.intAccessGridLookUpEdit.Name = "intAccessGridLookUpEdit";
            this.intAccessGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions33.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions34.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intAccessGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions33, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject129, serializableAppearanceObject130, serializableAppearanceObject131, serializableAppearanceObject132, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions34, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject133, serializableAppearanceObject134, serializableAppearanceObject135, serializableAppearanceObject136, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intAccessGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intAccessGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intAccessGridLookUpEdit.Properties.NullText = "";
            this.intAccessGridLookUpEdit.Properties.PopupView = this.gridView14;
            this.intAccessGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intAccessGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intAccessGridLookUpEdit.Size = new System.Drawing.Size(420, 22);
            this.intAccessGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intAccessGridLookUpEdit.TabIndex = 13;
            this.intAccessGridLookUpEdit.TabStop = false;
            this.intAccessGridLookUpEdit.Tag = "112";
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn66,
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn71});
            this.gridView14.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition13.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition13.Appearance.Options.UseForeColor = true;
            styleFormatCondition13.ApplyToRow = true;
            styleFormatCondition13.Column = this.gridColumn70;
            styleFormatCondition13.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition13.Value1 = 0;
            this.gridView14.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition13});
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView14.OptionsCustomization.AllowFilter = false;
            this.gridView14.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView14.OptionsFilter.AllowFilterEditor = false;
            this.gridView14.OptionsFilter.AllowMRUFilterList = false;
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView14.OptionsView.ShowIndicator = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn71, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Picklist Type";
            this.gridColumn66.FieldName = "HeaderDescription";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            this.gridColumn66.Width = 189;
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Picklist ID";
            this.gridColumn67.FieldName = "HeaderID";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Width = 67;
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "Item ID";
            this.gridColumn68.FieldName = "ItemCode";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            this.gridColumn68.Width = 58;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Description";
            this.gridColumn69.FieldName = "ItemDescription";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Visible = true;
            this.gridColumn69.VisibleIndex = 0;
            this.gridColumn69.Width = 264;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Order";
            this.gridColumn71.FieldName = "Order";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Width = 65;
            // 
            // intVisibilityGridLookUpEdit
            // 
            this.intVisibilityGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intVisibility", true));
            this.intVisibilityGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intVisibilityGridLookUpEdit.Location = new System.Drawing.Point(686, 176);
            this.intVisibilityGridLookUpEdit.MenuManager = this.barManager1;
            this.intVisibilityGridLookUpEdit.Name = "intVisibilityGridLookUpEdit";
            this.intVisibilityGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions35.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions36.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intVisibilityGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions35, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject137, serializableAppearanceObject138, serializableAppearanceObject139, serializableAppearanceObject140, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions36, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject141, serializableAppearanceObject142, serializableAppearanceObject143, serializableAppearanceObject144, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intVisibilityGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intVisibilityGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intVisibilityGridLookUpEdit.Properties.NullText = "";
            this.intVisibilityGridLookUpEdit.Properties.PopupView = this.gridView15;
            this.intVisibilityGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intVisibilityGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intVisibilityGridLookUpEdit.Size = new System.Drawing.Size(418, 22);
            this.intVisibilityGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intVisibilityGridLookUpEdit.TabIndex = 14;
            this.intVisibilityGridLookUpEdit.TabStop = false;
            this.intVisibilityGridLookUpEdit.Tag = "113";
            // 
            // gridView15
            // 
            this.gridView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn72,
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.gridColumn77});
            this.gridView15.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition14.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition14.Appearance.Options.UseForeColor = true;
            styleFormatCondition14.ApplyToRow = true;
            styleFormatCondition14.Column = this.gridColumn76;
            styleFormatCondition14.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition14.Value1 = 0;
            this.gridView15.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition14});
            this.gridView15.Name = "gridView15";
            this.gridView15.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView15.OptionsCustomization.AllowFilter = false;
            this.gridView15.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView15.OptionsFilter.AllowFilterEditor = false;
            this.gridView15.OptionsFilter.AllowMRUFilterList = false;
            this.gridView15.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView15.OptionsLayout.StoreAppearance = true;
            this.gridView15.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView15.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView15.OptionsView.ColumnAutoWidth = false;
            this.gridView15.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView15.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView15.OptionsView.ShowGroupPanel = false;
            this.gridView15.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView15.OptionsView.ShowIndicator = false;
            this.gridView15.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn77, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Picklist Type";
            this.gridColumn72.FieldName = "HeaderDescription";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Width = 189;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Picklist ID";
            this.gridColumn73.FieldName = "HeaderID";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Width = 67;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Item ID";
            this.gridColumn74.FieldName = "ItemCode";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Width = 58;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Description";
            this.gridColumn75.FieldName = "ItemDescription";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Visible = true;
            this.gridColumn75.VisibleIndex = 0;
            this.gridColumn75.Width = 264;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Order";
            this.gridColumn77.FieldName = "Order";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            this.gridColumn77.Width = 65;
            // 
            // intGroundTypeGridLookUpEdit
            // 
            this.intGroundTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intGroundType", true));
            this.intGroundTypeGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intGroundTypeGridLookUpEdit.Location = new System.Drawing.Point(143, 184);
            this.intGroundTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.intGroundTypeGridLookUpEdit.Name = "intGroundTypeGridLookUpEdit";
            this.intGroundTypeGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions37.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions38.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intGroundTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions37, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject145, serializableAppearanceObject146, serializableAppearanceObject147, serializableAppearanceObject148, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions38, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject149, serializableAppearanceObject150, serializableAppearanceObject151, serializableAppearanceObject152, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intGroundTypeGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intGroundTypeGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intGroundTypeGridLookUpEdit.Properties.NullText = "";
            this.intGroundTypeGridLookUpEdit.Properties.PopupView = this.gridView16;
            this.intGroundTypeGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intGroundTypeGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intGroundTypeGridLookUpEdit.Size = new System.Drawing.Size(453, 22);
            this.intGroundTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intGroundTypeGridLookUpEdit.TabIndex = 22;
            this.intGroundTypeGridLookUpEdit.TabStop = false;
            this.intGroundTypeGridLookUpEdit.Tag = "115";
            // 
            // gridView16
            // 
            this.gridView16.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn96,
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99,
            this.gridColumn100,
            this.gridColumn101});
            this.gridView16.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition15.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition15.Appearance.Options.UseForeColor = true;
            styleFormatCondition15.ApplyToRow = true;
            styleFormatCondition15.Column = this.gridColumn100;
            styleFormatCondition15.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition15.Value1 = 0;
            this.gridView16.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition15});
            this.gridView16.Name = "gridView16";
            this.gridView16.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView16.OptionsCustomization.AllowFilter = false;
            this.gridView16.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView16.OptionsFilter.AllowFilterEditor = false;
            this.gridView16.OptionsFilter.AllowMRUFilterList = false;
            this.gridView16.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView16.OptionsLayout.StoreAppearance = true;
            this.gridView16.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView16.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView16.OptionsView.ColumnAutoWidth = false;
            this.gridView16.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView16.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView16.OptionsView.ShowGroupPanel = false;
            this.gridView16.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView16.OptionsView.ShowIndicator = false;
            this.gridView16.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn101, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Picklist Type";
            this.gridColumn96.FieldName = "HeaderDescription";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            this.gridColumn96.Width = 189;
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "Picklist ID";
            this.gridColumn97.FieldName = "HeaderID";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.AllowEdit = false;
            this.gridColumn97.OptionsColumn.AllowFocus = false;
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            this.gridColumn97.Width = 67;
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Item ID";
            this.gridColumn98.FieldName = "ItemCode";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Width = 58;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Description";
            this.gridColumn99.FieldName = "ItemDescription";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            this.gridColumn99.Visible = true;
            this.gridColumn99.VisibleIndex = 0;
            this.gridColumn99.Width = 264;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Order";
            this.gridColumn101.FieldName = "Order";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.AllowEdit = false;
            this.gridColumn101.OptionsColumn.AllowFocus = false;
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            this.gridColumn101.Width = 65;
            // 
            // intProtectionTypeGridLookUpEdit
            // 
            this.intProtectionTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intProtectionType", true));
            this.intProtectionTypeGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intProtectionTypeGridLookUpEdit.Location = new System.Drawing.Point(143, 210);
            this.intProtectionTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.intProtectionTypeGridLookUpEdit.Name = "intProtectionTypeGridLookUpEdit";
            this.intProtectionTypeGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions39.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions40.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intProtectionTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions39, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject153, serializableAppearanceObject154, serializableAppearanceObject155, serializableAppearanceObject156, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions40, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject157, serializableAppearanceObject158, serializableAppearanceObject159, serializableAppearanceObject160, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intProtectionTypeGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intProtectionTypeGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intProtectionTypeGridLookUpEdit.Properties.NullText = "";
            this.intProtectionTypeGridLookUpEdit.Properties.PopupView = this.gridView17;
            this.intProtectionTypeGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intProtectionTypeGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intProtectionTypeGridLookUpEdit.Size = new System.Drawing.Size(453, 22);
            this.intProtectionTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intProtectionTypeGridLookUpEdit.TabIndex = 24;
            this.intProtectionTypeGridLookUpEdit.TabStop = false;
            this.intProtectionTypeGridLookUpEdit.Tag = "116";
            // 
            // gridView17
            // 
            this.gridView17.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn102,
            this.gridColumn103,
            this.gridColumn104,
            this.gridColumn105,
            this.gridColumn106,
            this.gridColumn107});
            this.gridView17.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition16.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition16.Appearance.Options.UseForeColor = true;
            styleFormatCondition16.ApplyToRow = true;
            styleFormatCondition16.Column = this.gridColumn106;
            styleFormatCondition16.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition16.Value1 = 0;
            this.gridView17.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition16});
            this.gridView17.Name = "gridView17";
            this.gridView17.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView17.OptionsCustomization.AllowFilter = false;
            this.gridView17.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView17.OptionsFilter.AllowFilterEditor = false;
            this.gridView17.OptionsFilter.AllowMRUFilterList = false;
            this.gridView17.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView17.OptionsLayout.StoreAppearance = true;
            this.gridView17.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView17.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView17.OptionsView.ColumnAutoWidth = false;
            this.gridView17.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView17.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView17.OptionsView.ShowGroupPanel = false;
            this.gridView17.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView17.OptionsView.ShowIndicator = false;
            this.gridView17.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn107, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "Picklist Type";
            this.gridColumn102.FieldName = "HeaderDescription";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.AllowEdit = false;
            this.gridColumn102.OptionsColumn.AllowFocus = false;
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.Width = 189;
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "Picklist ID";
            this.gridColumn103.FieldName = "HeaderID";
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowFocus = false;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            this.gridColumn103.Width = 67;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "Item ID";
            this.gridColumn104.FieldName = "ItemCode";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowFocus = false;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            this.gridColumn104.Width = 58;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "Description";
            this.gridColumn105.FieldName = "ItemDescription";
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowFocus = false;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.Visible = true;
            this.gridColumn105.VisibleIndex = 0;
            this.gridColumn105.Width = 264;
            // 
            // gridColumn107
            // 
            this.gridColumn107.Caption = "Order";
            this.gridColumn107.FieldName = "Order";
            this.gridColumn107.Name = "gridColumn107";
            this.gridColumn107.OptionsColumn.AllowEdit = false;
            this.gridColumn107.OptionsColumn.AllowFocus = false;
            this.gridColumn107.OptionsColumn.ReadOnly = true;
            this.gridColumn107.Width = 65;
            // 
            // intAgeClassGridLookUpEdit
            // 
            this.intAgeClassGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intAgeClass", true));
            this.intAgeClassGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intAgeClassGridLookUpEdit.Location = new System.Drawing.Point(686, 287);
            this.intAgeClassGridLookUpEdit.MenuManager = this.barManager1;
            this.intAgeClassGridLookUpEdit.Name = "intAgeClassGridLookUpEdit";
            this.intAgeClassGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions41.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions42.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intAgeClassGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions41, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject161, serializableAppearanceObject162, serializableAppearanceObject163, serializableAppearanceObject164, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions42, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject165, serializableAppearanceObject166, serializableAppearanceObject167, serializableAppearanceObject168, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intAgeClassGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intAgeClassGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intAgeClassGridLookUpEdit.Properties.NullText = "";
            this.intAgeClassGridLookUpEdit.Properties.PopupView = this.gridView18;
            this.intAgeClassGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intAgeClassGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intAgeClassGridLookUpEdit.Size = new System.Drawing.Size(418, 22);
            this.intAgeClassGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intAgeClassGridLookUpEdit.TabIndex = 43;
            this.intAgeClassGridLookUpEdit.TabStop = false;
            this.intAgeClassGridLookUpEdit.Tag = "123";
            // 
            // gridView18
            // 
            this.gridView18.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29});
            this.gridView18.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition17.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition17.Appearance.Options.UseForeColor = true;
            styleFormatCondition17.ApplyToRow = true;
            styleFormatCondition17.Column = this.gridColumn28;
            styleFormatCondition17.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition17.Value1 = 0;
            this.gridView18.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition17});
            this.gridView18.Name = "gridView18";
            this.gridView18.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView18.OptionsCustomization.AllowFilter = false;
            this.gridView18.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView18.OptionsFilter.AllowFilterEditor = false;
            this.gridView18.OptionsFilter.AllowMRUFilterList = false;
            this.gridView18.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView18.OptionsLayout.StoreAppearance = true;
            this.gridView18.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView18.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView18.OptionsView.ColumnAutoWidth = false;
            this.gridView18.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView18.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView18.OptionsView.ShowGroupPanel = false;
            this.gridView18.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView18.OptionsView.ShowIndicator = false;
            this.gridView18.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn29, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Picklist Type";
            this.gridColumn24.FieldName = "HeaderDescription";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 189;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Picklist ID";
            this.gridColumn25.FieldName = "HeaderID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Width = 67;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Item ID";
            this.gridColumn26.FieldName = "ItemCode";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Width = 58;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Description";
            this.gridColumn27.FieldName = "ItemDescription";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 0;
            this.gridColumn27.Width = 264;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Order";
            this.gridColumn29.FieldName = "Order";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 65;
            // 
            // strSituationMemoEdit
            // 
            this.strSituationMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strSituation", true));
            this.strSituationMemoEdit.Location = new System.Drawing.Point(143, 82);
            this.strSituationMemoEdit.MenuManager = this.barManager1;
            this.strSituationMemoEdit.Name = "strSituationMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strSituationMemoEdit, true);
            this.strSituationMemoEdit.Size = new System.Drawing.Size(961, 90);
            this.scSpellChecker.SetSpellCheckerOptions(this.strSituationMemoEdit, optionsSpelling23);
            this.strSituationMemoEdit.StyleController = this.dataLayoutControl1;
            this.strSituationMemoEdit.TabIndex = 50;
            this.strSituationMemoEdit.TabStop = false;
            // 
            // intNearbyObjectGridLookUpEdit
            // 
            this.intNearbyObjectGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intNearbyObject", true));
            this.intNearbyObjectGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intNearbyObjectGridLookUpEdit.Location = new System.Drawing.Point(688, 92);
            this.intNearbyObjectGridLookUpEdit.MenuManager = this.barManager1;
            this.intNearbyObjectGridLookUpEdit.Name = "intNearbyObjectGridLookUpEdit";
            this.intNearbyObjectGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions43.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions44.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intNearbyObjectGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions43, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject169, serializableAppearanceObject170, serializableAppearanceObject171, serializableAppearanceObject172, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions44, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject173, serializableAppearanceObject174, serializableAppearanceObject175, serializableAppearanceObject176, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intNearbyObjectGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intNearbyObjectGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intNearbyObjectGridLookUpEdit.Properties.NullText = "";
            this.intNearbyObjectGridLookUpEdit.Properties.PopupView = this.gridView20;
            this.intNearbyObjectGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intNearbyObjectGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intNearbyObjectGridLookUpEdit.Size = new System.Drawing.Size(404, 22);
            this.intNearbyObjectGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intNearbyObjectGridLookUpEdit.TabIndex = 38;
            this.intNearbyObjectGridLookUpEdit.TabStop = false;
            this.intNearbyObjectGridLookUpEdit.Tag = "132";
            // 
            // gridView20
            // 
            this.gridView20.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53});
            this.gridView20.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition18.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition18.Appearance.Options.UseForeColor = true;
            styleFormatCondition18.ApplyToRow = true;
            styleFormatCondition18.Column = this.gridColumn52;
            styleFormatCondition18.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition18.Value1 = 0;
            this.gridView20.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition18});
            this.gridView20.Name = "gridView20";
            this.gridView20.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView20.OptionsCustomization.AllowFilter = false;
            this.gridView20.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView20.OptionsFilter.AllowFilterEditor = false;
            this.gridView20.OptionsFilter.AllowMRUFilterList = false;
            this.gridView20.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView20.OptionsLayout.StoreAppearance = true;
            this.gridView20.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView20.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView20.OptionsView.ColumnAutoWidth = false;
            this.gridView20.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView20.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView20.OptionsView.ShowGroupPanel = false;
            this.gridView20.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView20.OptionsView.ShowIndicator = false;
            this.gridView20.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn53, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Picklist Type";
            this.gridColumn48.FieldName = "HeaderDescription";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Width = 189;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Picklist ID";
            this.gridColumn49.FieldName = "HeaderID";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Width = 67;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Item ID";
            this.gridColumn50.FieldName = "ItemCode";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Width = 58;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Description";
            this.gridColumn51.FieldName = "ItemDescription";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 0;
            this.gridColumn51.Width = 264;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Order";
            this.gridColumn53.FieldName = "Order";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Width = 65;
            // 
            // intNearbyObject2GridLookUpEdit
            // 
            this.intNearbyObject2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intNearbyObject2", true));
            this.intNearbyObject2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intNearbyObject2GridLookUpEdit.Location = new System.Drawing.Point(688, 118);
            this.intNearbyObject2GridLookUpEdit.MenuManager = this.barManager1;
            this.intNearbyObject2GridLookUpEdit.Name = "intNearbyObject2GridLookUpEdit";
            this.intNearbyObject2GridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions45.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions46.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intNearbyObject2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions45, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject177, serializableAppearanceObject178, serializableAppearanceObject179, serializableAppearanceObject180, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions46, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject181, serializableAppearanceObject182, serializableAppearanceObject183, serializableAppearanceObject184, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intNearbyObject2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intNearbyObject2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intNearbyObject2GridLookUpEdit.Properties.NullText = "";
            this.intNearbyObject2GridLookUpEdit.Properties.PopupView = this.gridView21;
            this.intNearbyObject2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intNearbyObject2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intNearbyObject2GridLookUpEdit.Size = new System.Drawing.Size(404, 22);
            this.intNearbyObject2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intNearbyObject2GridLookUpEdit.TabIndex = 53;
            this.intNearbyObject2GridLookUpEdit.TabStop = false;
            this.intNearbyObject2GridLookUpEdit.Tag = "132";
            // 
            // gridView21
            // 
            this.gridView21.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59});
            this.gridView21.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition19.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition19.Appearance.Options.UseForeColor = true;
            styleFormatCondition19.ApplyToRow = true;
            styleFormatCondition19.Column = this.gridColumn58;
            styleFormatCondition19.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition19.Value1 = 0;
            this.gridView21.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition19});
            this.gridView21.Name = "gridView21";
            this.gridView21.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView21.OptionsCustomization.AllowFilter = false;
            this.gridView21.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView21.OptionsFilter.AllowFilterEditor = false;
            this.gridView21.OptionsFilter.AllowMRUFilterList = false;
            this.gridView21.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView21.OptionsLayout.StoreAppearance = true;
            this.gridView21.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView21.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView21.OptionsView.ColumnAutoWidth = false;
            this.gridView21.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView21.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView21.OptionsView.ShowGroupPanel = false;
            this.gridView21.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView21.OptionsView.ShowIndicator = false;
            this.gridView21.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn59, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Picklist Type";
            this.gridColumn54.FieldName = "HeaderDescription";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 189;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Picklist ID";
            this.gridColumn55.FieldName = "HeaderID";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Width = 67;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Item ID";
            this.gridColumn56.FieldName = "ItemCode";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Width = 58;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Description";
            this.gridColumn57.FieldName = "ItemDescription";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 0;
            this.gridColumn57.Width = 264;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Order";
            this.gridColumn59.FieldName = "Order";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Width = 65;
            // 
            // intNearbyObject3GridLookUpEdit
            // 
            this.intNearbyObject3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intNearbyObject3", true));
            this.intNearbyObject3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intNearbyObject3GridLookUpEdit.Location = new System.Drawing.Point(688, 144);
            this.intNearbyObject3GridLookUpEdit.MenuManager = this.barManager1;
            this.intNearbyObject3GridLookUpEdit.Name = "intNearbyObject3GridLookUpEdit";
            this.intNearbyObject3GridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions47.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions48.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intNearbyObject3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions47, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject185, serializableAppearanceObject186, serializableAppearanceObject187, serializableAppearanceObject188, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions48, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject189, serializableAppearanceObject190, serializableAppearanceObject191, serializableAppearanceObject192, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intNearbyObject3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intNearbyObject3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intNearbyObject3GridLookUpEdit.Properties.NullText = "";
            this.intNearbyObject3GridLookUpEdit.Properties.PopupView = this.gridView22;
            this.intNearbyObject3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intNearbyObject3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intNearbyObject3GridLookUpEdit.Size = new System.Drawing.Size(404, 22);
            this.intNearbyObject3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intNearbyObject3GridLookUpEdit.TabIndex = 54;
            this.intNearbyObject3GridLookUpEdit.TabStop = false;
            this.intNearbyObject3GridLookUpEdit.Tag = "132";
            // 
            // gridView22
            // 
            this.gridView22.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65});
            this.gridView22.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition20.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition20.Appearance.Options.UseForeColor = true;
            styleFormatCondition20.ApplyToRow = true;
            styleFormatCondition20.Column = this.gridColumn64;
            styleFormatCondition20.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition20.Value1 = 0;
            this.gridView22.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition20});
            this.gridView22.Name = "gridView22";
            this.gridView22.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView22.OptionsCustomization.AllowFilter = false;
            this.gridView22.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView22.OptionsFilter.AllowFilterEditor = false;
            this.gridView22.OptionsFilter.AllowMRUFilterList = false;
            this.gridView22.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView22.OptionsLayout.StoreAppearance = true;
            this.gridView22.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView22.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView22.OptionsView.ColumnAutoWidth = false;
            this.gridView22.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView22.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView22.OptionsView.ShowGroupPanel = false;
            this.gridView22.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView22.OptionsView.ShowIndicator = false;
            this.gridView22.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn65, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Picklist Type";
            this.gridColumn60.FieldName = "HeaderDescription";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Width = 189;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Picklist ID";
            this.gridColumn61.FieldName = "HeaderID";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            this.gridColumn61.Width = 67;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Item ID";
            this.gridColumn62.FieldName = "ItemCode";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.Width = 58;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Description";
            this.gridColumn63.FieldName = "ItemDescription";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.Visible = true;
            this.gridColumn63.VisibleIndex = 0;
            this.gridColumn63.Width = 264;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Order";
            this.gridColumn65.FieldName = "Order";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Width = 65;
            // 
            // intOwnershipIDGridLookUpEdit
            // 
            this.intOwnershipIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intOwnershipID", true));
            this.intOwnershipIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intOwnershipIDGridLookUpEdit.Location = new System.Drawing.Point(685, -26);
            this.intOwnershipIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intOwnershipIDGridLookUpEdit.Name = "intOwnershipIDGridLookUpEdit";
            editorButtonImageOptions49.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions50.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intOwnershipIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions49, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject193, serializableAppearanceObject194, serializableAppearanceObject195, serializableAppearanceObject196, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions50, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject197, serializableAppearanceObject198, serializableAppearanceObject199, serializableAppearanceObject200, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intOwnershipIDGridLookUpEdit.Properties.DataSource = this.sp01362ATOwnershipListWithBlankBindingSource;
            this.intOwnershipIDGridLookUpEdit.Properties.DisplayMember = "OwnershipName";
            this.intOwnershipIDGridLookUpEdit.Properties.NullText = "";
            this.intOwnershipIDGridLookUpEdit.Properties.PopupView = this.gridView23;
            this.intOwnershipIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.intOwnershipIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intOwnershipIDGridLookUpEdit.Properties.ValueMember = "OwnershipID";
            this.intOwnershipIDGridLookUpEdit.Size = new System.Drawing.Size(419, 22);
            this.intOwnershipIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intOwnershipIDGridLookUpEdit.TabIndex = 57;
            this.intOwnershipIDGridLookUpEdit.TabStop = false;
            this.intOwnershipIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intOwnershipIDGridLookUpEdit_ButtonClick);
            this.intOwnershipIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.intOwnershipIDGridLookUpEdit_EditValueChanged);
            this.intOwnershipIDGridLookUpEdit.Validated += new System.EventHandler(this.intOwnershipIDGridLookUpEdit_Validated);
            // 
            // sp01362ATOwnershipListWithBlankBindingSource
            // 
            this.sp01362ATOwnershipListWithBlankBindingSource.DataMember = "sp01362_AT_Ownership_List_With_Blank";
            this.sp01362ATOwnershipListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView23
            // 
            this.gridView23.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOwnershipCode,
            this.colOwnershipID,
            this.colOwnershipName,
            this.colRemarks});
            this.gridView23.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition21.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition21.Appearance.Options.UseForeColor = true;
            styleFormatCondition21.ApplyToRow = true;
            styleFormatCondition21.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition21.Value1 = 0;
            this.gridView23.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition21});
            this.gridView23.Name = "gridView23";
            this.gridView23.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView23.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView23.OptionsLayout.StoreAppearance = true;
            this.gridView23.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView23.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView23.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView23.OptionsView.ColumnAutoWidth = false;
            this.gridView23.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView23.OptionsView.ShowGroupPanel = false;
            this.gridView23.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView23.OptionsView.ShowIndicator = false;
            this.gridView23.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOwnershipName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colOwnershipCode
            // 
            this.colOwnershipCode.Caption = "Ownership Code";
            this.colOwnershipCode.FieldName = "OwnershipCode";
            this.colOwnershipCode.Name = "colOwnershipCode";
            this.colOwnershipCode.OptionsColumn.AllowEdit = false;
            this.colOwnershipCode.OptionsColumn.AllowFocus = false;
            this.colOwnershipCode.OptionsColumn.ReadOnly = true;
            this.colOwnershipCode.Visible = true;
            this.colOwnershipCode.VisibleIndex = 0;
            this.colOwnershipCode.Width = 124;
            // 
            // colOwnershipID
            // 
            this.colOwnershipID.Caption = "Ownership ID";
            this.colOwnershipID.FieldName = "OwnershipID";
            this.colOwnershipID.Name = "colOwnershipID";
            this.colOwnershipID.OptionsColumn.AllowEdit = false;
            this.colOwnershipID.OptionsColumn.AllowFocus = false;
            this.colOwnershipID.OptionsColumn.ReadOnly = true;
            this.colOwnershipID.Width = 120;
            // 
            // colOwnershipName
            // 
            this.colOwnershipName.Caption = "Ownership Name";
            this.colOwnershipName.FieldName = "OwnershipName";
            this.colOwnershipName.Name = "colOwnershipName";
            this.colOwnershipName.OptionsColumn.AllowEdit = false;
            this.colOwnershipName.OptionsColumn.AllowFocus = false;
            this.colOwnershipName.OptionsColumn.ReadOnly = true;
            this.colOwnershipName.Visible = true;
            this.colOwnershipName.VisibleIndex = 1;
            this.colOwnershipName.Width = 239;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 2;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // RetentionCategoryGridLookUpEdit
            // 
            this.RetentionCategoryGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "RetentionCategory", true));
            this.RetentionCategoryGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.RetentionCategoryGridLookUpEdit.Location = new System.Drawing.Point(686, 237);
            this.RetentionCategoryGridLookUpEdit.MenuManager = this.barManager1;
            this.RetentionCategoryGridLookUpEdit.Name = "RetentionCategoryGridLookUpEdit";
            this.RetentionCategoryGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions51.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions52.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.RetentionCategoryGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions51, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject201, serializableAppearanceObject202, serializableAppearanceObject203, serializableAppearanceObject204, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions52, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject205, serializableAppearanceObject206, serializableAppearanceObject207, serializableAppearanceObject208, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.RetentionCategoryGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.RetentionCategoryGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.RetentionCategoryGridLookUpEdit.Properties.NullText = "";
            this.RetentionCategoryGridLookUpEdit.Properties.PopupView = this.gridView24;
            this.RetentionCategoryGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.RetentionCategoryGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.RetentionCategoryGridLookUpEdit.Size = new System.Drawing.Size(418, 22);
            this.RetentionCategoryGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RetentionCategoryGridLookUpEdit.TabIndex = 65;
            this.RetentionCategoryGridLookUpEdit.TabStop = false;
            this.RetentionCategoryGridLookUpEdit.Tag = "139";
            // 
            // gridView24
            // 
            this.gridView24.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35});
            this.gridView24.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition22.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition22.Appearance.Options.UseForeColor = true;
            styleFormatCondition22.ApplyToRow = true;
            styleFormatCondition22.Column = this.gridColumn34;
            styleFormatCondition22.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition22.Value1 = 0;
            this.gridView24.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition22});
            this.gridView24.Name = "gridView24";
            this.gridView24.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView24.OptionsCustomization.AllowFilter = false;
            this.gridView24.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView24.OptionsFilter.AllowFilterEditor = false;
            this.gridView24.OptionsFilter.AllowMRUFilterList = false;
            this.gridView24.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView24.OptionsLayout.StoreAppearance = true;
            this.gridView24.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView24.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView24.OptionsView.ColumnAutoWidth = false;
            this.gridView24.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView24.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView24.OptionsView.ShowGroupPanel = false;
            this.gridView24.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView24.OptionsView.ShowIndicator = false;
            this.gridView24.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn35, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Picklist Type";
            this.gridColumn30.FieldName = "HeaderDescription";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Width = 189;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Picklist ID";
            this.gridColumn31.FieldName = "HeaderID";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Width = 67;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Item ID";
            this.gridColumn32.FieldName = "ItemCode";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 58;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Description";
            this.gridColumn33.FieldName = "ItemDescription";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 0;
            this.gridColumn33.Width = 264;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Order";
            this.gridColumn35.FieldName = "Order";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Width = 65;
            // 
            // intTypeGridLookUpEdit
            // 
            this.intTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intType", true));
            this.intTypeGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intTypeGridLookUpEdit.Location = new System.Drawing.Point(685, -50);
            this.intTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.intTypeGridLookUpEdit.Name = "intTypeGridLookUpEdit";
            this.intTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.intTypeGridLookUpEdit.Properties.DataSource = this.sp01370ATObjectTypesNoBlankBindingSource;
            this.intTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.intTypeGridLookUpEdit.Properties.NullText = "";
            this.intTypeGridLookUpEdit.Properties.PopupView = this.gridView19;
            this.intTypeGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intTypeGridLookUpEdit.Properties.ValueMember = "ID";
            this.intTypeGridLookUpEdit.Size = new System.Drawing.Size(419, 20);
            this.intTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intTypeGridLookUpEdit.TabIndex = 49;
            this.intTypeGridLookUpEdit.TabStop = false;
            this.intTypeGridLookUpEdit.EditValueChanged += new System.EventHandler(this.intTypeGridLookUpEdit_EditValueChanged);
            this.intTypeGridLookUpEdit.Validated += new System.EventHandler(this.intTypeGridLookUpEdit_Validated);
            // 
            // sp01370ATObjectTypesNoBlankBindingSource
            // 
            this.sp01370ATObjectTypesNoBlankBindingSource.DataMember = "sp01370_AT_Object_Types_No_Blank";
            this.sp01370ATObjectTypesNoBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView19
            // 
            this.gridView19.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colDescription,
            this.colOrder});
            this.gridView19.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView19.Name = "gridView19";
            this.gridView19.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView19.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView19.OptionsLayout.StoreAppearance = true;
            this.gridView19.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView19.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView19.OptionsView.ColumnAutoWidth = false;
            this.gridView19.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView19.OptionsView.ShowGroupPanel = false;
            this.gridView19.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView19.OptionsView.ShowIndicator = false;
            this.gridView19.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Object Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 264;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // intSizeGridLookUpEdit
            // 
            this.intSizeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intSize", true));
            this.intSizeGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intSizeGridLookUpEdit.Location = new System.Drawing.Point(685, 84);
            this.intSizeGridLookUpEdit.MenuManager = this.barManager1;
            this.intSizeGridLookUpEdit.Name = "intSizeGridLookUpEdit";
            this.intSizeGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions53.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions54.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intSizeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions53, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject209, serializableAppearanceObject210, serializableAppearanceObject211, serializableAppearanceObject212, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions54, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject213, serializableAppearanceObject214, serializableAppearanceObject215, serializableAppearanceObject216, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intSizeGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intSizeGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intSizeGridLookUpEdit.Properties.NullText = "";
            this.intSizeGridLookUpEdit.Properties.PopupView = this.gridView26;
            this.intSizeGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intSizeGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intSizeGridLookUpEdit.Size = new System.Drawing.Size(419, 22);
            this.intSizeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intSizeGridLookUpEdit.TabIndex = 37;
            this.intSizeGridLookUpEdit.TabStop = false;
            this.intSizeGridLookUpEdit.Tag = "122";
            // 
            // gridView26
            // 
            this.gridView26.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHeaderDescription,
            this.colHeaderID,
            this.colItemCode,
            this.colItemDescription,
            this.colItemID,
            this.colOrder1});
            this.gridView26.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition23.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition23.Appearance.Options.UseForeColor = true;
            styleFormatCondition23.ApplyToRow = true;
            styleFormatCondition23.Column = this.colItemID;
            styleFormatCondition23.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition23.Value1 = 0;
            this.gridView26.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition23});
            this.gridView26.Name = "gridView26";
            this.gridView26.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView26.OptionsCustomization.AllowFilter = false;
            this.gridView26.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView26.OptionsFilter.AllowFilterEditor = false;
            this.gridView26.OptionsFilter.AllowMRUFilterList = false;
            this.gridView26.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView26.OptionsLayout.StoreAppearance = true;
            this.gridView26.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView26.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView26.OptionsView.ColumnAutoWidth = false;
            this.gridView26.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView26.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView26.OptionsView.ShowGroupPanel = false;
            this.gridView26.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView26.OptionsView.ShowIndicator = false;
            this.gridView26.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Picklist Type";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.Width = 189;
            // 
            // colHeaderID
            // 
            this.colHeaderID.Caption = "Picklist ID";
            this.colHeaderID.FieldName = "HeaderID";
            this.colHeaderID.Name = "colHeaderID";
            this.colHeaderID.OptionsColumn.AllowEdit = false;
            this.colHeaderID.OptionsColumn.AllowFocus = false;
            this.colHeaderID.OptionsColumn.ReadOnly = true;
            this.colHeaderID.Width = 67;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Item ID";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.OptionsColumn.AllowFocus = false;
            this.colItemCode.OptionsColumn.ReadOnly = true;
            this.colItemCode.Width = 58;
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Description";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 264;
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.AllowFocus = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            this.colOrder1.Width = 65;
            // 
            // strInspectionUnitComboBoxEdit
            // 
            this.strInspectionUnitComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strInspectionUnit", true));
            this.strInspectionUnitComboBoxEdit.Location = new System.Drawing.Point(408, 82);
            this.strInspectionUnitComboBoxEdit.MenuManager = this.barManager1;
            this.strInspectionUnitComboBoxEdit.Name = "strInspectionUnitComboBoxEdit";
            this.strInspectionUnitComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.strInspectionUnitComboBoxEdit.Properties.Items.AddRange(new object[] {
            "",
            "Days",
            "Weeks",
            "Months",
            "Years"});
            this.strInspectionUnitComboBoxEdit.Properties.PopupSizeable = true;
            this.strInspectionUnitComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.strInspectionUnitComboBoxEdit.Size = new System.Drawing.Size(146, 20);
            this.strInspectionUnitComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.strInspectionUnitComboBoxEdit.TabIndex = 20;
            this.strInspectionUnitComboBoxEdit.TabStop = false;
            this.strInspectionUnitComboBoxEdit.EditValueChanged += new System.EventHandler(this.strInspectionUnitComboBoxEdit_EditValueChanged);
            this.strInspectionUnitComboBoxEdit.Validated += new System.EventHandler(this.strInspectionUnitComboBoxEdit_Validated);
            // 
            // intPlantSizeGridLookUpEdit
            // 
            this.intPlantSizeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "intPlantSize", true));
            this.intPlantSizeGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intPlantSizeGridLookUpEdit.Location = new System.Drawing.Point(143, 82);
            this.intPlantSizeGridLookUpEdit.MenuManager = this.barManager1;
            this.intPlantSizeGridLookUpEdit.Name = "intPlantSizeGridLookUpEdit";
            this.intPlantSizeGridLookUpEdit.Properties.AutoComplete = false;
            editorButtonImageOptions55.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions56.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intPlantSizeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions55, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject217, serializableAppearanceObject218, serializableAppearanceObject219, serializableAppearanceObject220, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions56, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject221, serializableAppearanceObject222, serializableAppearanceObject223, serializableAppearanceObject224, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intPlantSizeGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intPlantSizeGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intPlantSizeGridLookUpEdit.Properties.NullText = "";
            this.intPlantSizeGridLookUpEdit.Properties.PopupView = this.gridView8;
            this.intPlantSizeGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intPlantSizeGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intPlantSizeGridLookUpEdit.Size = new System.Drawing.Size(453, 22);
            this.intPlantSizeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intPlantSizeGridLookUpEdit.TabIndex = 31;
            this.intPlantSizeGridLookUpEdit.TabStop = false;
            this.intPlantSizeGridLookUpEdit.Tag = "119";
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn78,
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81,
            this.gridColumn82,
            this.gridColumn83});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition24.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition24.Appearance.Options.UseForeColor = true;
            styleFormatCondition24.ApplyToRow = true;
            styleFormatCondition24.Column = this.gridColumn82;
            styleFormatCondition24.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition24.Value1 = 0;
            this.gridView8.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition24});
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView8.OptionsCustomization.AllowFilter = false;
            this.gridView8.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowFilterEditor = false;
            this.gridView8.OptionsFilter.AllowMRUFilterList = false;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn83, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "Picklist Type";
            this.gridColumn78.FieldName = "HeaderDescription";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            this.gridColumn78.Width = 189;
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Picklist ID";
            this.gridColumn79.FieldName = "HeaderID";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            this.gridColumn79.Width = 67;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Item ID";
            this.gridColumn80.FieldName = "ItemCode";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            this.gridColumn80.Width = 58;
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Description";
            this.gridColumn81.FieldName = "ItemDescription";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            this.gridColumn81.Visible = true;
            this.gridColumn81.VisibleIndex = 0;
            this.gridColumn81.Width = 264;
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Order";
            this.gridColumn83.FieldName = "Order";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Width = 65;
            // 
            // strContextPopupContainerEdit
            // 
            this.strContextPopupContainerEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strContext", true));
            this.strContextPopupContainerEdit.Location = new System.Drawing.Point(143, 313);
            this.strContextPopupContainerEdit.MenuManager = this.barManager1;
            this.strContextPopupContainerEdit.Name = "strContextPopupContainerEdit";
            editorButtonImageOptions57.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions58.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.strContextPopupContainerEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions57, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject225, serializableAppearanceObject226, serializableAppearanceObject227, serializableAppearanceObject228, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions58, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject229, serializableAppearanceObject230, serializableAppearanceObject231, serializableAppearanceObject232, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.strContextPopupContainerEdit.Properties.MaxLength = 50;
            this.strContextPopupContainerEdit.Properties.PopupControl = this.popupContainerControl1;
            this.strContextPopupContainerEdit.Properties.ShowPopupCloseButton = false;
            this.strContextPopupContainerEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.strContextPopupContainerEdit.Size = new System.Drawing.Size(420, 22);
            this.strContextPopupContainerEdit.StyleController = this.dataLayoutControl1;
            this.strContextPopupContainerEdit.TabIndex = 15;
            this.strContextPopupContainerEdit.TabStop = false;
            this.strContextPopupContainerEdit.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.strContextPopupContainerEdit_QueryResultValue);
            this.strContextPopupContainerEdit.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.strContextPopupContainerEdit_QueryPopUp);
            this.strContextPopupContainerEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.strContextPopupContainerEdit_ButtonClick);
            this.strContextPopupContainerEdit.Enter += new System.EventHandler(this.strContextPopupContainerEdit_Enter);
            this.strContextPopupContainerEdit.Leave += new System.EventHandler(this.strContextPopupContainerEdit_Leave);
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Controls.Add(this.btnPopoupContainerOK1);
            this.popupContainerControl1.Controls.Add(this.gridControl2);
            this.popupContainerControl1.Controls.Add(this.ClearAnyExistingValuesCheckEdit);
            this.popupContainerControl1.Location = new System.Drawing.Point(585, 2);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(423, 244);
            this.popupContainerControl1.TabIndex = 75;
            // 
            // btnPopoupContainerOK1
            // 
            this.btnPopoupContainerOK1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPopoupContainerOK1.Location = new System.Drawing.Point(3, 218);
            this.btnPopoupContainerOK1.Name = "btnPopoupContainerOK1";
            this.btnPopoupContainerOK1.Size = new System.Drawing.Size(75, 23);
            this.btnPopoupContainerOK1.TabIndex = 6;
            this.btnPopoupContainerOK1.Text = "OK";
            this.btnPopoupContainerOK1.Click += new System.EventHandler(this.btnPopoupContainerOK1_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(4, 23);
            this.gridControl2.MainView = this.gridView27;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(417, 193);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView27});
            // 
            // gridView27
            // 
            this.gridView27.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn126,
            this.gridColumn127,
            this.gridColumn128,
            this.gridColumn129,
            this.gridColumn130,
            this.gridColumn131});
            this.gridView27.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition25.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition25.Appearance.Options.UseForeColor = true;
            styleFormatCondition25.ApplyToRow = true;
            styleFormatCondition25.Column = this.gridColumn130;
            styleFormatCondition25.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition25.Value1 = 0;
            this.gridView27.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition25});
            this.gridView27.GridControl = this.gridControl2;
            this.gridView27.Name = "gridView27";
            this.gridView27.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView27.OptionsCustomization.AllowFilter = false;
            this.gridView27.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView27.OptionsFilter.AllowFilterEditor = false;
            this.gridView27.OptionsFilter.AllowMRUFilterList = false;
            this.gridView27.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView27.OptionsLayout.StoreAppearance = true;
            this.gridView27.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView27.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView27.OptionsView.ColumnAutoWidth = false;
            this.gridView27.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView27.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView27.OptionsView.ShowGroupPanel = false;
            this.gridView27.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView27.OptionsView.ShowIndicator = false;
            this.gridView27.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn131, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView27.GotFocus += new System.EventHandler(this.gridView27_GotFocus);
            // 
            // gridColumn126
            // 
            this.gridColumn126.Caption = "Picklist Type";
            this.gridColumn126.FieldName = "HeaderDescription";
            this.gridColumn126.Name = "gridColumn126";
            this.gridColumn126.OptionsColumn.AllowEdit = false;
            this.gridColumn126.OptionsColumn.AllowFocus = false;
            this.gridColumn126.OptionsColumn.ReadOnly = true;
            this.gridColumn126.Width = 189;
            // 
            // gridColumn127
            // 
            this.gridColumn127.Caption = "Picklist ID";
            this.gridColumn127.FieldName = "HeaderID";
            this.gridColumn127.Name = "gridColumn127";
            this.gridColumn127.OptionsColumn.AllowEdit = false;
            this.gridColumn127.OptionsColumn.AllowFocus = false;
            this.gridColumn127.OptionsColumn.ReadOnly = true;
            this.gridColumn127.Width = 67;
            // 
            // gridColumn128
            // 
            this.gridColumn128.Caption = "Item Code";
            this.gridColumn128.FieldName = "ItemCode";
            this.gridColumn128.Name = "gridColumn128";
            this.gridColumn128.OptionsColumn.AllowEdit = false;
            this.gridColumn128.OptionsColumn.AllowFocus = false;
            this.gridColumn128.OptionsColumn.ReadOnly = true;
            this.gridColumn128.Visible = true;
            this.gridColumn128.VisibleIndex = 0;
            this.gridColumn128.Width = 100;
            // 
            // gridColumn129
            // 
            this.gridColumn129.Caption = "Description";
            this.gridColumn129.FieldName = "ItemDescription";
            this.gridColumn129.Name = "gridColumn129";
            this.gridColumn129.OptionsColumn.AllowEdit = false;
            this.gridColumn129.OptionsColumn.AllowFocus = false;
            this.gridColumn129.OptionsColumn.ReadOnly = true;
            this.gridColumn129.Visible = true;
            this.gridColumn129.VisibleIndex = 1;
            this.gridColumn129.Width = 264;
            // 
            // gridColumn131
            // 
            this.gridColumn131.Caption = "Order";
            this.gridColumn131.FieldName = "Order";
            this.gridColumn131.Name = "gridColumn131";
            this.gridColumn131.OptionsColumn.AllowEdit = false;
            this.gridColumn131.OptionsColumn.AllowFocus = false;
            this.gridColumn131.OptionsColumn.ReadOnly = true;
            this.gridColumn131.Width = 65;
            // 
            // ClearAnyExistingValuesCheckEdit
            // 
            this.ClearAnyExistingValuesCheckEdit.Location = new System.Drawing.Point(3, 3);
            this.ClearAnyExistingValuesCheckEdit.MenuManager = this.barManager1;
            this.ClearAnyExistingValuesCheckEdit.Name = "ClearAnyExistingValuesCheckEdit";
            this.ClearAnyExistingValuesCheckEdit.Properties.Caption = "Clear Any Existing Values";
            this.ClearAnyExistingValuesCheckEdit.Size = new System.Drawing.Size(142, 19);
            this.ClearAnyExistingValuesCheckEdit.TabIndex = 0;
            // 
            // strManagementPopupContainerEdit
            // 
            this.strManagementPopupContainerEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01204ATEditTreeDetailsBindingSource, "strManagement", true));
            this.strManagementPopupContainerEdit.Location = new System.Drawing.Point(686, 313);
            this.strManagementPopupContainerEdit.MenuManager = this.barManager1;
            this.strManagementPopupContainerEdit.Name = "strManagementPopupContainerEdit";
            editorButtonImageOptions59.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions60.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.strManagementPopupContainerEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions59, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject233, serializableAppearanceObject234, serializableAppearanceObject235, serializableAppearanceObject236, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions60, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject237, serializableAppearanceObject238, serializableAppearanceObject239, serializableAppearanceObject240, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.strManagementPopupContainerEdit.Properties.CloseOnOuterMouseClick = false;
            this.strManagementPopupContainerEdit.Properties.MaxLength = 50;
            this.strManagementPopupContainerEdit.Properties.ShowPopupCloseButton = false;
            this.strManagementPopupContainerEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.strManagementPopupContainerEdit.Size = new System.Drawing.Size(418, 22);
            this.strManagementPopupContainerEdit.StyleController = this.dataLayoutControl1;
            this.strManagementPopupContainerEdit.TabIndex = 16;
            this.strManagementPopupContainerEdit.TabStop = false;
            this.strManagementPopupContainerEdit.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.strManagementPopupContainerEdit_QueryResultValue);
            this.strManagementPopupContainerEdit.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.strManagementPopupContainerEdit_QueryPopUp);
            this.strManagementPopupContainerEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.strManagementPopupContainerEdit_ButtonClick);
            this.strManagementPopupContainerEdit.Enter += new System.EventHandler(this.strManagementPopupContainerEdit_Enter);
            this.strManagementPopupContainerEdit.Leave += new System.EventHandler(this.strManagementPopupContainerEdit_Leave);
            // 
            // ItemForintTreeID
            // 
            this.ItemForintTreeID.Control = this.intTreeIDTextEdit;
            this.ItemForintTreeID.CustomizationFormText = "Tree ID:";
            this.ItemForintTreeID.Location = new System.Drawing.Point(0, 23);
            this.ItemForintTreeID.Name = "ItemForintTreeID";
            this.ItemForintTreeID.Size = new System.Drawing.Size(1112, 24);
            this.ItemForintTreeID.Text = "Tree ID:";
            this.ItemForintTreeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForDateAdded
            // 
            this.ItemForDateAdded.Control = this.DateAddedDateEdit;
            this.ItemForDateAdded.CustomizationFormText = "Date Added:";
            this.ItemForDateAdded.Location = new System.Drawing.Point(0, 48);
            this.ItemForDateAdded.Name = "ItemForDateAdded";
            this.ItemForDateAdded.Size = new System.Drawing.Size(1112, 24);
            this.ItemForDateAdded.Text = "Date Added:";
            this.ItemForDateAdded.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForstrGridRef
            // 
            this.ItemForstrGridRef.Control = this.strGridRefTextEdit;
            this.ItemForstrGridRef.CustomizationFormText = "Grid Reference:";
            this.ItemForstrGridRef.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrGridRef.Name = "ItemForstrGridRef";
            this.ItemForstrGridRef.Size = new System.Drawing.Size(552, 24);
            this.ItemForstrGridRef.Text = "Grid Reference:";
            this.ItemForstrGridRef.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForstrPostcode
            // 
            this.ItemForstrPostcode.Control = this.strPostcodeTextEdit;
            this.ItemForstrPostcode.CustomizationFormText = "Postcode:";
            this.ItemForstrPostcode.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrPostcode.Name = "ItemForstrPostcode";
            this.ItemForstrPostcode.Size = new System.Drawing.Size(1105, 24);
            this.ItemForstrPostcode.Text = "Postcode:";
            this.ItemForstrPostcode.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientEditTextEdit;
            this.ItemForClientID.Location = new System.Drawing.Point(0, 172);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(1084, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteID
            // 
            this.ItemForSiteID.Control = this.SiteIDTextEdit;
            this.ItemForSiteID.Location = new System.Drawing.Point(0, 172);
            this.ItemForSiteID.Name = "ItemForSiteID";
            this.ItemForSiteID.Size = new System.Drawing.Size(1084, 24);
            this.ItemForSiteID.Text = "Site ID:";
            this.ItemForSiteID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSiteCode
            // 
            this.ItemForSiteCode.Control = this.SiteCodeTextEdit;
            this.ItemForSiteCode.Location = new System.Drawing.Point(0, 167);
            this.ItemForSiteCode.Name = "ItemForSiteCode";
            this.ItemForSiteCode.Size = new System.Drawing.Size(1108, 24);
            this.ItemForSiteCode.Text = "Site Code:";
            this.ItemForSiteCode.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1128, 867);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.tabbedControlGroup1,
            this.layoutControlGroup14});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1108, 847);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(200, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(908, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(1108, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 177);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1108, 670);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup6,
            this.layoutControlGroup16,
            this.layoutControlGroup15,
            this.layoutControlGroup11,
            this.layoutControlGroup12,
            this.layoutControlGroup9,
            this.layoutControlGroup27,
            this.layoutControlGroup5,
            this.layoutControlGroup13});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Details";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintDBH,
            this.ItemForintDBHRange,
            this.ItemForintHeightRange,
            this.ItemForintLegalStatus,
            this.ItemFordecHeight,
            this.ItemForintGroupNo,
            this.ItemForSULE,
            this.ItemForintAgeClass,
            this.ItemForStemCount,
            this.ItemForintSize,
            this.ItemForstrContext,
            this.ItemForstrManagement,
            this.emptySpaceItem17,
            this.emptySpaceItem18,
            this.ItemForintCrownDiameter,
            this.ItemForintCrownDepth,
            this.layoutControlGroup8,
            this.emptySpaceItem16,
            this.ItemForintSpeciesID,
            this.emptySpaceItem1,
            this.ItemForSpeciesVarietyGridLookUpEdit,
            this.ItemForRetentionCategory});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlGroup7.Text = "Details";
            // 
            // ItemForintDBH
            // 
            this.ItemForintDBH.Control = this.intDBHSpinEdit;
            this.ItemForintDBH.CustomizationFormText = "DBH:";
            this.ItemForintDBH.Location = new System.Drawing.Point(0, 52);
            this.ItemForintDBH.Name = "ItemForintDBH";
            this.ItemForintDBH.Size = new System.Drawing.Size(542, 26);
            this.ItemForintDBH.Text = "DBH:";
            this.ItemForintDBH.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintDBHRange
            // 
            this.ItemForintDBHRange.Control = this.intDBHRangeGridLookUpEdit;
            this.ItemForintDBHRange.CustomizationFormText = "DBH Band:";
            this.ItemForintDBHRange.Location = new System.Drawing.Point(542, 52);
            this.ItemForintDBHRange.Name = "ItemForintDBHRange";
            this.ItemForintDBHRange.Size = new System.Drawing.Size(542, 26);
            this.ItemForintDBHRange.Text = "DBH Band:";
            this.ItemForintDBHRange.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintHeightRange
            // 
            this.ItemForintHeightRange.Control = this.intHeightRangeGridLookUpEdit;
            this.ItemForintHeightRange.CustomizationFormText = "Height Band:";
            this.ItemForintHeightRange.Location = new System.Drawing.Point(542, 78);
            this.ItemForintHeightRange.Name = "ItemForintHeightRange";
            this.ItemForintHeightRange.Size = new System.Drawing.Size(542, 26);
            this.ItemForintHeightRange.Text = "Height Band:";
            this.ItemForintHeightRange.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintLegalStatus
            // 
            this.ItemForintLegalStatus.Control = this.intLegalStatusGridLookUpEdit;
            this.ItemForintLegalStatus.CustomizationFormText = "Legal Status:";
            this.ItemForintLegalStatus.Location = new System.Drawing.Point(0, 179);
            this.ItemForintLegalStatus.Name = "ItemForintLegalStatus";
            this.ItemForintLegalStatus.Size = new System.Drawing.Size(543, 26);
            this.ItemForintLegalStatus.Text = "Legal Status:";
            this.ItemForintLegalStatus.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemFordecHeight
            // 
            this.ItemFordecHeight.Control = this.decHeightSpinEdit;
            this.ItemFordecHeight.CustomizationFormText = "Height:";
            this.ItemFordecHeight.Location = new System.Drawing.Point(0, 78);
            this.ItemFordecHeight.Name = "ItemFordecHeight";
            this.ItemFordecHeight.Size = new System.Drawing.Size(542, 26);
            this.ItemFordecHeight.Text = "Height:";
            this.ItemFordecHeight.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintGroupNo
            // 
            this.ItemForintGroupNo.Control = this.intGroupNoSpinEdit;
            this.ItemForintGroupNo.CustomizationFormText = "Group Number:";
            this.ItemForintGroupNo.Location = new System.Drawing.Point(0, 205);
            this.ItemForintGroupNo.Name = "ItemForintGroupNo";
            this.ItemForintGroupNo.Size = new System.Drawing.Size(543, 24);
            this.ItemForintGroupNo.Text = "Group Number:";
            this.ItemForintGroupNo.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForSULE
            // 
            this.ItemForSULE.Control = this.SULESpinEdit;
            this.ItemForSULE.CustomizationFormText = "SULE:";
            this.ItemForSULE.Location = new System.Drawing.Point(0, 229);
            this.ItemForSULE.Name = "ItemForSULE";
            this.ItemForSULE.Size = new System.Drawing.Size(543, 26);
            this.ItemForSULE.Text = "SULE:";
            this.ItemForSULE.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintAgeClass
            // 
            this.ItemForintAgeClass.Control = this.intAgeClassGridLookUpEdit;
            this.ItemForintAgeClass.CustomizationFormText = "Age Class:";
            this.ItemForintAgeClass.Location = new System.Drawing.Point(543, 229);
            this.ItemForintAgeClass.Name = "ItemForintAgeClass";
            this.ItemForintAgeClass.Size = new System.Drawing.Size(541, 26);
            this.ItemForintAgeClass.Text = "Age Class:";
            this.ItemForintAgeClass.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForStemCount
            // 
            this.ItemForStemCount.Control = this.StemCountSpinEdit;
            this.ItemForStemCount.CustomizationFormText = "Stem Count:";
            this.ItemForStemCount.Location = new System.Drawing.Point(543, 205);
            this.ItemForStemCount.Name = "ItemForStemCount";
            this.ItemForStemCount.Size = new System.Drawing.Size(541, 24);
            this.ItemForStemCount.Text = "Stem Count:";
            this.ItemForStemCount.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintSize
            // 
            this.ItemForintSize.Control = this.intSizeGridLookUpEdit;
            this.ItemForintSize.CustomizationFormText = "Size:";
            this.ItemForintSize.Location = new System.Drawing.Point(542, 26);
            this.ItemForintSize.Name = "ItemForintSize";
            this.ItemForintSize.Size = new System.Drawing.Size(542, 26);
            this.ItemForintSize.Text = "Size:";
            this.ItemForintSize.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForstrContext
            // 
            this.ItemForstrContext.Control = this.strContextPopupContainerEdit;
            this.ItemForstrContext.CustomizationFormText = "Context:";
            this.ItemForstrContext.Location = new System.Drawing.Point(0, 255);
            this.ItemForstrContext.Name = "ItemForstrContext";
            this.ItemForstrContext.Size = new System.Drawing.Size(543, 26);
            this.ItemForstrContext.Text = "Context:";
            this.ItemForstrContext.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForstrManagement
            // 
            this.ItemForstrManagement.Control = this.strManagementPopupContainerEdit;
            this.ItemForstrManagement.CustomizationFormText = "Management:";
            this.ItemForstrManagement.Location = new System.Drawing.Point(543, 255);
            this.ItemForstrManagement.Name = "ItemForstrManagement";
            this.ItemForstrManagement.Size = new System.Drawing.Size(541, 26);
            this.ItemForstrManagement.Text = "Management:";
            this.ItemForstrManagement.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.CustomizationFormText = "emptySpaceItem17";
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 128);
            this.emptySpaceItem17.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem17.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(1084, 10);
            this.emptySpaceItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.CustomizationFormText = "emptySpaceItem18";
            this.emptySpaceItem18.Location = new System.Drawing.Point(0, 169);
            this.emptySpaceItem18.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem18.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(1084, 10);
            this.emptySpaceItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForintCrownDiameter
            // 
            this.ItemForintCrownDiameter.Control = this.intCrownDiameterSpinEdit;
            this.ItemForintCrownDiameter.CustomizationFormText = "Crown Diameter:";
            this.ItemForintCrownDiameter.Location = new System.Drawing.Point(0, 104);
            this.ItemForintCrownDiameter.Name = "ItemForintCrownDiameter";
            this.ItemForintCrownDiameter.Size = new System.Drawing.Size(542, 24);
            this.ItemForintCrownDiameter.Text = "Crown Diameter:";
            this.ItemForintCrownDiameter.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintCrownDepth
            // 
            this.ItemForintCrownDepth.Control = this.intCrownDepthSpinEdit;
            this.ItemForintCrownDepth.CustomizationFormText = "Crown Depth:";
            this.ItemForintCrownDepth.Location = new System.Drawing.Point(542, 104);
            this.ItemForintCrownDepth.Name = "ItemForintCrownDepth";
            this.ItemForintCrownDepth.Size = new System.Drawing.Size(542, 24);
            this.ItemForintCrownDepth.Text = "Crown Depth:";
            this.ItemForintCrownDepth.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Cardinal Points";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.Expanded = false;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCrownNorth,
            this.ItemForCrownWest,
            this.ItemForCrownSouth,
            this.ItemForCrownEast});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 138);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1084, 31);
            this.layoutControlGroup8.Text = "Cardinal Points on Crown Radius";
            // 
            // ItemForCrownNorth
            // 
            this.ItemForCrownNorth.Control = this.CrownNorthSpinEdit;
            this.ItemForCrownNorth.CustomizationFormText = "Crown North:";
            this.ItemForCrownNorth.Location = new System.Drawing.Point(0, 0);
            this.ItemForCrownNorth.Name = "ItemForCrownNorth";
            this.ItemForCrownNorth.Size = new System.Drawing.Size(540, 24);
            this.ItemForCrownNorth.Text = "Crown North:";
            this.ItemForCrownNorth.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForCrownWest
            // 
            this.ItemForCrownWest.Control = this.CrownWestSpinEdit;
            this.ItemForCrownWest.CustomizationFormText = "Crown West:";
            this.ItemForCrownWest.Location = new System.Drawing.Point(0, 24);
            this.ItemForCrownWest.Name = "ItemForCrownWest";
            this.ItemForCrownWest.Size = new System.Drawing.Size(540, 24);
            this.ItemForCrownWest.Text = "Crown West:";
            this.ItemForCrownWest.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForCrownSouth
            // 
            this.ItemForCrownSouth.Control = this.CrownSouthSpinEdit;
            this.ItemForCrownSouth.CustomizationFormText = "Crown South:";
            this.ItemForCrownSouth.Location = new System.Drawing.Point(540, 0);
            this.ItemForCrownSouth.Name = "ItemForCrownSouth";
            this.ItemForCrownSouth.Size = new System.Drawing.Size(537, 24);
            this.ItemForCrownSouth.Text = "Crown South:";
            this.ItemForCrownSouth.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForCrownEast
            // 
            this.ItemForCrownEast.Control = this.CrownEastSpinEdit;
            this.ItemForCrownEast.CustomizationFormText = "Crown East:";
            this.ItemForCrownEast.Location = new System.Drawing.Point(540, 24);
            this.ItemForCrownEast.Name = "ItemForCrownEast";
            this.ItemForCrownEast.Size = new System.Drawing.Size(537, 24);
            this.ItemForCrownEast.Text = "Crown East:";
            this.ItemForCrownEast.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.CustomizationFormText = "emptySpaceItem16";
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 281);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(1084, 341);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForintSpeciesID
            // 
            this.ItemForintSpeciesID.Control = this.intSpeciesIDGridLookUpEdit;
            this.ItemForintSpeciesID.CustomizationFormText = "Species:";
            this.ItemForintSpeciesID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintSpeciesID.Name = "ItemForintSpeciesID";
            this.ItemForintSpeciesID.Size = new System.Drawing.Size(542, 26);
            this.ItemForintSpeciesID.Text = "Species:";
            this.ItemForintSpeciesID.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(542, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(542, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSpeciesVarietyGridLookUpEdit
            // 
            this.ItemForSpeciesVarietyGridLookUpEdit.Control = this.SpeciesVarietyGridLookUpEdit;
            this.ItemForSpeciesVarietyGridLookUpEdit.CustomizationFormText = "Species Variety:";
            this.ItemForSpeciesVarietyGridLookUpEdit.Location = new System.Drawing.Point(0, 26);
            this.ItemForSpeciesVarietyGridLookUpEdit.Name = "ItemForSpeciesVarietyGridLookUpEdit";
            this.ItemForSpeciesVarietyGridLookUpEdit.Size = new System.Drawing.Size(542, 26);
            this.ItemForSpeciesVarietyGridLookUpEdit.Text = "Species Variety:";
            this.ItemForSpeciesVarietyGridLookUpEdit.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForRetentionCategory
            // 
            this.ItemForRetentionCategory.Control = this.RetentionCategoryGridLookUpEdit;
            this.ItemForRetentionCategory.CustomizationFormText = "Retention Category:";
            this.ItemForRetentionCategory.Location = new System.Drawing.Point(543, 179);
            this.ItemForRetentionCategory.Name = "ItemForRetentionCategory";
            this.ItemForRetentionCategory.Size = new System.Drawing.Size(541, 26);
            this.ItemForRetentionCategory.Text = "Retention Category:";
            this.ItemForRetentionCategory.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Risk";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintSafetyPriority,
            this.ItemFordecRiskFactor,
            this.ItemForintSiteHazardClass,
            this.layoutControlGroup3,
            this.emptySpaceItem3});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlGroup6.Text = "Risk";
            // 
            // ItemForintSafetyPriority
            // 
            this.ItemForintSafetyPriority.Control = this.intSafetyPriorityGridLookUpEdit;
            this.ItemForintSafetyPriority.CustomizationFormText = "Safety Priority:";
            this.ItemForintSafetyPriority.Location = new System.Drawing.Point(0, 0);
            this.ItemForintSafetyPriority.Name = "ItemForintSafetyPriority";
            this.ItemForintSafetyPriority.Size = new System.Drawing.Size(533, 26);
            this.ItemForintSafetyPriority.Text = "Risk Category:";
            this.ItemForintSafetyPriority.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemFordecRiskFactor
            // 
            this.ItemFordecRiskFactor.Control = this.decRiskFactorSpinEdit;
            this.ItemFordecRiskFactor.CustomizationFormText = "Risk Factor:";
            this.ItemFordecRiskFactor.Location = new System.Drawing.Point(0, 26);
            this.ItemFordecRiskFactor.Name = "ItemFordecRiskFactor";
            this.ItemFordecRiskFactor.Size = new System.Drawing.Size(533, 24);
            this.ItemFordecRiskFactor.Text = "Risk Factor:";
            this.ItemFordecRiskFactor.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintSiteHazardClass
            // 
            this.ItemForintSiteHazardClass.Control = this.intSiteHazardClassGridLookUpEdit;
            this.ItemForintSiteHazardClass.CustomizationFormText = "Site Hazard Class:";
            this.ItemForintSiteHazardClass.Location = new System.Drawing.Point(0, 50);
            this.ItemForintSiteHazardClass.Name = "ItemForintSiteHazardClass";
            this.ItemForintSiteHazardClass.Size = new System.Drawing.Size(533, 98);
            this.ItemForintSiteHazardClass.Text = "Site Hazard Class:";
            this.ItemForintSiteHazardClass.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Nearby Objects";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintNearbyObject,
            this.ItemForintNearbyObject3,
            this.ItemForintNearbyObject2,
            this.ItemForintDistance});
            this.layoutControlGroup3.Location = new System.Drawing.Point(533, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(551, 148);
            this.layoutControlGroup3.Text = "Nearby Objects";
            // 
            // ItemForintNearbyObject
            // 
            this.ItemForintNearbyObject.Control = this.intNearbyObjectGridLookUpEdit;
            this.ItemForintNearbyObject.CustomizationFormText = "Nearby Object 1:";
            this.ItemForintNearbyObject.Location = new System.Drawing.Point(0, 0);
            this.ItemForintNearbyObject.Name = "ItemForintNearbyObject";
            this.ItemForintNearbyObject.Size = new System.Drawing.Size(527, 26);
            this.ItemForintNearbyObject.Text = "Nearby Object 1:";
            this.ItemForintNearbyObject.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintNearbyObject3
            // 
            this.ItemForintNearbyObject3.Control = this.intNearbyObject3GridLookUpEdit;
            this.ItemForintNearbyObject3.CustomizationFormText = "Nearby Object 3:";
            this.ItemForintNearbyObject3.Location = new System.Drawing.Point(0, 52);
            this.ItemForintNearbyObject3.Name = "ItemForintNearbyObject3";
            this.ItemForintNearbyObject3.Size = new System.Drawing.Size(527, 26);
            this.ItemForintNearbyObject3.Text = "Nearby Object 3:";
            this.ItemForintNearbyObject3.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintNearbyObject2
            // 
            this.ItemForintNearbyObject2.Control = this.intNearbyObject2GridLookUpEdit;
            this.ItemForintNearbyObject2.CustomizationFormText = "Nearby Object 2:";
            this.ItemForintNearbyObject2.Location = new System.Drawing.Point(0, 26);
            this.ItemForintNearbyObject2.Name = "ItemForintNearbyObject2";
            this.ItemForintNearbyObject2.Size = new System.Drawing.Size(527, 26);
            this.ItemForintNearbyObject2.Text = "Nearby Object 2:";
            this.ItemForintNearbyObject2.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintDistance
            // 
            this.ItemForintDistance.Control = this.intDistanceSpinEdit;
            this.ItemForintDistance.CustomizationFormText = "Distance:";
            this.ItemForintDistance.Location = new System.Drawing.Point(0, 78);
            this.ItemForintDistance.Name = "ItemForintDistance";
            this.ItemForintDistance.Size = new System.Drawing.Size(527, 24);
            this.ItemForintDistance.Text = "Distance:";
            this.ItemForintDistance.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 148);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1084, 474);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.CustomizationFormText = "CAVAT";
            this.layoutControlGroup16.ExpandButtonVisible = true;
            this.layoutControlGroup16.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup17,
            this.emptySpaceItem12,
            this.layoutControlGroup23,
            this.emptySpaceItem15,
            this.ItemForCavat});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlGroup16.Text = "CAVAT";
            // 
            // layoutControlGroup17
            // 
            this.layoutControlGroup17.CustomizationFormText = "Calculation Method: Full";
            this.layoutControlGroup17.ExpandButtonVisible = true;
            this.layoutControlGroup17.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup18,
            this.layoutControlGroup19,
            this.layoutControlGroup20,
            this.layoutControlGroup21,
            this.layoutControlGroup22});
            this.layoutControlGroup17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup17.Name = "layoutControlGroup17";
            this.layoutControlGroup17.Size = new System.Drawing.Size(513, 588);
            this.layoutControlGroup17.Text = "Calculation Method: Full";
            // 
            // layoutControlGroup18
            // 
            this.layoutControlGroup18.CustomizationFormText = "Step 1: Basic Value";
            this.layoutControlGroup18.ExpandButtonVisible = true;
            this.layoutControlGroup18.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup18.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.layoutControlGroup18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup18.Name = "layoutControlGroup18";
            this.layoutControlGroup18.Size = new System.Drawing.Size(489, 118);
            this.layoutControlGroup18.Text = "Step 1: Basic Value";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.CavatStemDiameterSpinEdit;
            this.layoutControlItem5.CustomizationFormText = "Stem Diameter:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem5.Text = "Stem Diameter:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.CavatUnitValueFactorSpinEdit;
            this.layoutControlItem6.CustomizationFormText = "Unit Value Factor:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem6.Text = "Unit Value Factor:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.CavatBasicValueTextEdit;
            this.layoutControlItem7.CustomizationFormText = "Basic Value:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem7.Text = "Basic Value:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup19
            // 
            this.layoutControlGroup19.CustomizationFormText = "Step 2: CTI Value";
            this.layoutControlGroup19.ExpandButtonVisible = true;
            this.layoutControlGroup19.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup19.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutControlGroup19.Location = new System.Drawing.Point(0, 118);
            this.layoutControlGroup19.Name = "layoutControlGroup19";
            this.layoutControlGroup19.Size = new System.Drawing.Size(489, 118);
            this.layoutControlGroup19.Text = "Step 2: CTI Value";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.CavatCTIFactorGridLookUpEdit;
            this.layoutControlItem8.CustomizationFormText = "CTI Factor:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem8.Text = "CTI Factor:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.CavatAccessibilityGridLookUpEdit;
            this.layoutControlItem9.CustomizationFormText = "Accessibility:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem9.Text = "Accessibility:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.CavatCTIValueTextEdit;
            this.layoutControlItem10.CustomizationFormText = "CTI Value:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem10.Text = "CTI Value:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup20
            // 
            this.layoutControlGroup20.CustomizationFormText = "Step 3: Functional Value";
            this.layoutControlGroup20.ExpandButtonVisible = true;
            this.layoutControlGroup20.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup20.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem13});
            this.layoutControlGroup20.Location = new System.Drawing.Point(0, 236);
            this.layoutControlGroup20.Name = "layoutControlGroup20";
            this.layoutControlGroup20.Size = new System.Drawing.Size(489, 94);
            this.layoutControlGroup20.Text = "Step 3: Functional Value";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.CavatFunctionalValueFactorGridLookUpEdit;
            this.layoutControlItem11.CustomizationFormText = "Functional Value Factor:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem11.Text = "Functional Value Factor:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.CavatFunctionalValueTextEdit;
            this.layoutControlItem13.CustomizationFormText = "Functional Value:";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem13.Text = "Functional Value:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup21
            // 
            this.layoutControlGroup21.CustomizationFormText = "Step 4: Adjusted Value";
            this.layoutControlGroup21.ExpandButtonVisible = true;
            this.layoutControlGroup21.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup21.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16});
            this.layoutControlGroup21.Location = new System.Drawing.Point(0, 330);
            this.layoutControlGroup21.Name = "layoutControlGroup21";
            this.layoutControlGroup21.Size = new System.Drawing.Size(489, 142);
            this.layoutControlGroup21.Text = "Step 4: Adjusted Value";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.CavatAmenityFactorsGridLookUpEdit;
            this.layoutControlItem12.CustomizationFormText = "Amenity Factors:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem12.Text = "Amenity Factors:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.CavatAppropriatenessGridLookUpEdit;
            this.layoutControlItem14.CustomizationFormText = "Appropriateness:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem14.Text = "Appropriateness:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.CavatAdjustedPercentSpinEdit;
            this.layoutControlItem15.CustomizationFormText = "Adjusted %:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem15.Text = "Adjusted %:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.CavatAdjustedValueTextEdit;
            this.layoutControlItem16.CustomizationFormText = "Adjusted Value:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem16.Text = "Adjusted Value:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup22
            // 
            this.layoutControlGroup22.CustomizationFormText = "Step 5: Final Value";
            this.layoutControlGroup22.ExpandButtonVisible = true;
            this.layoutControlGroup22.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup22.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17});
            this.layoutControlGroup22.Location = new System.Drawing.Point(0, 472);
            this.layoutControlGroup22.Name = "layoutControlGroup22";
            this.layoutControlGroup22.Size = new System.Drawing.Size(489, 70);
            this.layoutControlGroup22.Text = "Step 5: Final Value";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.CavatSLEFactorGridLookUpEdit;
            this.layoutControlItem17.CustomizationFormText = "SLE Factor:";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(465, 24);
            this.layoutControlItem17.Text = "SLE Factor:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(513, 612);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(571, 10);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup23
            // 
            this.layoutControlGroup23.CustomizationFormText = "Calculation Method: Quick";
            this.layoutControlGroup23.ExpandButtonVisible = true;
            this.layoutControlGroup23.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup23.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup24,
            this.layoutControlGroup25,
            this.layoutControlGroup26});
            this.layoutControlGroup23.Location = new System.Drawing.Point(513, 0);
            this.layoutControlGroup23.Name = "layoutControlGroup23";
            this.layoutControlGroup23.Size = new System.Drawing.Size(571, 588);
            this.layoutControlGroup23.Text = "Calculation Method: Quick";
            // 
            // layoutControlGroup24
            // 
            this.layoutControlGroup24.CustomizationFormText = "Step 1: Basic Value";
            this.layoutControlGroup24.ExpandButtonVisible = true;
            this.layoutControlGroup24.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup24.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22});
            this.layoutControlGroup24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup24.Name = "layoutControlGroup24";
            this.layoutControlGroup24.Size = new System.Drawing.Size(547, 142);
            this.layoutControlGroup24.Text = "Step 1: Basic Value";
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.CavatStemDiameterSpinEdit2;
            this.layoutControlItem19.CustomizationFormText = "Stem Diameter:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(523, 24);
            this.layoutControlItem19.Text = "Stem Diameter:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.CavatUnitValueFactorSpinEdit2;
            this.layoutControlItem20.CustomizationFormText = "Unit Value Factor:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(523, 24);
            this.layoutControlItem20.Text = "Unit Value Factor:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.CavatCTIFactorGridLookUpEdit2;
            this.layoutControlItem21.CustomizationFormText = "CTI Rating:";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(523, 24);
            this.layoutControlItem21.Text = "CTI Rating:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.CavatCTIValueTextEdit2;
            this.layoutControlItem22.CustomizationFormText = "CTI Value:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(523, 24);
            this.layoutControlItem22.Text = "CTI Value:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup25
            // 
            this.layoutControlGroup25.CustomizationFormText = "Step 2: Functional Value";
            this.layoutControlGroup25.ExpandButtonVisible = true;
            this.layoutControlGroup25.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup25.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem23,
            this.layoutControlItem24});
            this.layoutControlGroup25.Location = new System.Drawing.Point(0, 142);
            this.layoutControlGroup25.Name = "layoutControlGroup25";
            this.layoutControlGroup25.Size = new System.Drawing.Size(547, 94);
            this.layoutControlGroup25.Text = "Step 2: Functional Value";
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.CavatFunctionalValueFactorGridLookUpEdit2;
            this.layoutControlItem23.CustomizationFormText = "Functional Adjustment:";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(523, 24);
            this.layoutControlItem23.Text = "Functional Adjustment:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.CavatFunctionalValueTextEdit2;
            this.layoutControlItem24.CustomizationFormText = "Adjusted Value:";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(523, 24);
            this.layoutControlItem24.Text = "Adjusted Value:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup26
            // 
            this.layoutControlGroup26.CustomizationFormText = "Step 3: Final Value";
            this.layoutControlGroup26.ExpandButtonVisible = true;
            this.layoutControlGroup26.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup26.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25});
            this.layoutControlGroup26.Location = new System.Drawing.Point(0, 236);
            this.layoutControlGroup26.Name = "layoutControlGroup26";
            this.layoutControlGroup26.Size = new System.Drawing.Size(547, 306);
            this.layoutControlGroup26.Text = "Step 3: Final Value";
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.CavatSLEFactorGridLookUpEdit2;
            this.layoutControlItem25.CustomizationFormText = "SLE Factor:";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(523, 260);
            this.layoutControlItem25.Text = "SLE Factor:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.CustomizationFormText = "emptySpaceItem15";
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 612);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(513, 10);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCavat
            // 
            this.ItemForCavat.Control = this.CavatSpinEdit;
            this.ItemForCavat.CustomizationFormText = "CAVAT:";
            this.ItemForCavat.Location = new System.Drawing.Point(0, 588);
            this.ItemForCavat.Name = "ItemForCavat";
            this.ItemForCavat.Size = new System.Drawing.Size(1084, 24);
            this.ItemForCavat.Text = "CAVAT Value:";
            this.ItemForCavat.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.CustomizationFormText = "Location";
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrHouseName,
            this.ItemForintAccess,
            this.ItemForstrSituation,
            this.emptySpaceItem5,
            this.ItemForintVisibility,
            this.layoutControlGroup10,
            this.emptySpaceItem13});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlGroup15.Text = "Location";
            // 
            // ItemForstrHouseName
            // 
            this.ItemForstrHouseName.Control = this.strHouseNameTextEdit;
            this.ItemForstrHouseName.CustomizationFormText = "Nearest House Name:";
            this.ItemForstrHouseName.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrHouseName.Name = "ItemForstrHouseName";
            this.ItemForstrHouseName.Size = new System.Drawing.Size(1084, 24);
            this.ItemForstrHouseName.Text = "Nearest House Name:";
            this.ItemForstrHouseName.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintAccess
            // 
            this.ItemForintAccess.Control = this.intAccessGridLookUpEdit;
            this.ItemForintAccess.CustomizationFormText = "Access:";
            this.ItemForintAccess.Location = new System.Drawing.Point(0, 118);
            this.ItemForintAccess.Name = "ItemForintAccess";
            this.ItemForintAccess.Size = new System.Drawing.Size(543, 26);
            this.ItemForintAccess.Text = "Accessibility:";
            this.ItemForintAccess.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForstrSituation
            // 
            this.ItemForstrSituation.Control = this.strSituationMemoEdit;
            this.ItemForstrSituation.CustomizationFormText = "Situation:";
            this.ItemForstrSituation.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrSituation.MinSize = new System.Drawing.Size(124, 20);
            this.ItemForstrSituation.Name = "ItemForstrSituation";
            this.ItemForstrSituation.Size = new System.Drawing.Size(1084, 94);
            this.ItemForstrSituation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrSituation.Text = "Situation:";
            this.ItemForstrSituation.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 416);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1084, 206);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForintVisibility
            // 
            this.ItemForintVisibility.Control = this.intVisibilityGridLookUpEdit;
            this.ItemForintVisibility.CustomizationFormText = "Visibility:";
            this.ItemForintVisibility.Location = new System.Drawing.Point(543, 118);
            this.ItemForintVisibility.Name = "ItemForintVisibility";
            this.ItemForintVisibility.Size = new System.Drawing.Size(541, 26);
            this.ItemForintVisibility.Text = "Visibility:";
            this.ItemForintVisibility.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Mapping";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForid,
            this.ItemForintX,
            this.ItemFordecAreaHa,
            this.ItemForstrMapLabel,
            this.emptySpaceItem14,
            this.ItemForObjectLengthSpinEdit,
            this.ItemForObjectWidthSpinEdit,
            this.ItemForstrPolygonXY,
            this.ItemForintY,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 154);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(1084, 262);
            this.layoutControlGroup10.Text = "Mapping";
            // 
            // ItemForid
            // 
            this.ItemForid.Control = this.idTextEdit;
            this.ItemForid.CustomizationFormText = "Map ID:";
            this.ItemForid.Location = new System.Drawing.Point(0, 0);
            this.ItemForid.Name = "ItemForid";
            this.ItemForid.Size = new System.Drawing.Size(530, 24);
            this.ItemForid.Text = "Map ID:";
            this.ItemForid.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintX
            // 
            this.ItemForintX.Control = this.intXSpinEdit;
            this.ItemForintX.CustomizationFormText = "X Coordinate:";
            this.ItemForintX.Location = new System.Drawing.Point(0, 24);
            this.ItemForintX.Name = "ItemForintX";
            this.ItemForintX.Size = new System.Drawing.Size(530, 24);
            this.ItemForintX.Text = "X Coordinate:";
            this.ItemForintX.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemFordecAreaHa
            // 
            this.ItemFordecAreaHa.Control = this.decAreaHaSpinEdit;
            this.ItemFordecAreaHa.CustomizationFormText = "Area:";
            this.ItemFordecAreaHa.Location = new System.Drawing.Point(0, 192);
            this.ItemFordecAreaHa.Name = "ItemFordecAreaHa";
            this.ItemFordecAreaHa.Size = new System.Drawing.Size(530, 24);
            this.ItemFordecAreaHa.Text = "Object Area:";
            this.ItemFordecAreaHa.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForstrMapLabel
            // 
            this.ItemForstrMapLabel.Control = this.strMapLabelTextEdit;
            this.ItemForstrMapLabel.CustomizationFormText = "Map Label:";
            this.ItemForstrMapLabel.Location = new System.Drawing.Point(530, 0);
            this.ItemForstrMapLabel.Name = "ItemForstrMapLabel";
            this.ItemForstrMapLabel.Size = new System.Drawing.Size(530, 24);
            this.ItemForstrMapLabel.Text = "Map Label:";
            this.ItemForstrMapLabel.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(530, 192);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(530, 24);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForObjectLengthSpinEdit
            // 
            this.ItemForObjectLengthSpinEdit.Control = this.ObjectLengthSpinEdit;
            this.ItemForObjectLengthSpinEdit.CustomizationFormText = "Object Length:";
            this.ItemForObjectLengthSpinEdit.Location = new System.Drawing.Point(0, 168);
            this.ItemForObjectLengthSpinEdit.Name = "ItemForObjectLengthSpinEdit";
            this.ItemForObjectLengthSpinEdit.Size = new System.Drawing.Size(530, 24);
            this.ItemForObjectLengthSpinEdit.Text = "Object Length:";
            this.ItemForObjectLengthSpinEdit.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForObjectWidthSpinEdit
            // 
            this.ItemForObjectWidthSpinEdit.Control = this.ObjectWidthSpinEdit;
            this.ItemForObjectWidthSpinEdit.CustomizationFormText = "Object Width:";
            this.ItemForObjectWidthSpinEdit.Location = new System.Drawing.Point(530, 168);
            this.ItemForObjectWidthSpinEdit.Name = "ItemForObjectWidthSpinEdit";
            this.ItemForObjectWidthSpinEdit.Size = new System.Drawing.Size(530, 24);
            this.ItemForObjectWidthSpinEdit.Text = "Object Width:";
            this.ItemForObjectWidthSpinEdit.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForstrPolygonXY
            // 
            this.ItemForstrPolygonXY.Control = this.strPolygonXYTextEdit;
            this.ItemForstrPolygonXY.CustomizationFormText = "Polygon Coordinates:";
            this.ItemForstrPolygonXY.Location = new System.Drawing.Point(0, 72);
            this.ItemForstrPolygonXY.MinSize = new System.Drawing.Size(124, 20);
            this.ItemForstrPolygonXY.Name = "ItemForstrPolygonXY";
            this.ItemForstrPolygonXY.Size = new System.Drawing.Size(530, 96);
            this.ItemForstrPolygonXY.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrPolygonXY.Text = "Poly Coordinates:";
            this.ItemForstrPolygonXY.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintY
            // 
            this.ItemForintY.Control = this.intYSpinEdit;
            this.ItemForintY.CustomizationFormText = "Y Coordinate:";
            this.ItemForintY.Location = new System.Drawing.Point(0, 48);
            this.ItemForintY.Name = "ItemForintY";
            this.ItemForintY.Size = new System.Drawing.Size(530, 24);
            this.ItemForintY.Text = "Y Coordinate:";
            this.ItemForintY.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.LocationXSpinEdit;
            this.layoutControlItem26.CustomizationFormText = "Latitude:";
            this.layoutControlItem26.Location = new System.Drawing.Point(530, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(530, 24);
            this.layoutControlItem26.Text = "Latitude:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.LocationYSpinEdit;
            this.layoutControlItem27.CustomizationFormText = "Longitude:";
            this.layoutControlItem27.Location = new System.Drawing.Point(530, 48);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(530, 24);
            this.layoutControlItem27.Text = "Longitude:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.LocationPolyXYMemoEdit;
            this.layoutControlItem28.CustomizationFormText = "Poly Lat \\ Long:";
            this.layoutControlItem28.Location = new System.Drawing.Point(530, 72);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(530, 96);
            this.layoutControlItem28.Text = "Poly Lat \\ Long:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 144);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(1084, 10);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Inspection";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemFordtNextInspectionDate,
            this.ItemFordtLastInspectionDate,
            this.ItemForintInspectionCycle,
            this.ItemForstrInspectionUnit,
            this.ItemFordtSurveyDate,
            this.layoutControlGroup4,
            this.emptySpaceItem7,
            this.emptySpaceItem19,
            this.emptySpaceItem6,
            this.emptySpaceItem20});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlGroup11.Text = "Inspections";
            // 
            // ItemFordtNextInspectionDate
            // 
            this.ItemFordtNextInspectionDate.Control = this.dtNextInspectionDateDateEdit;
            this.ItemFordtNextInspectionDate.CustomizationFormText = "Next Inspection Date:";
            this.ItemFordtNextInspectionDate.Location = new System.Drawing.Point(0, 48);
            this.ItemFordtNextInspectionDate.MaxSize = new System.Drawing.Size(265, 24);
            this.ItemFordtNextInspectionDate.MinSize = new System.Drawing.Size(265, 24);
            this.ItemFordtNextInspectionDate.Name = "ItemFordtNextInspectionDate";
            this.ItemFordtNextInspectionDate.Size = new System.Drawing.Size(265, 24);
            this.ItemFordtNextInspectionDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtNextInspectionDate.Text = "Next Inspection Date:";
            this.ItemFordtNextInspectionDate.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemFordtLastInspectionDate
            // 
            this.ItemFordtLastInspectionDate.Control = this.dtLastInspectionDateDateEdit;
            this.ItemFordtLastInspectionDate.CustomizationFormText = "Last Inspection Date:";
            this.ItemFordtLastInspectionDate.Location = new System.Drawing.Point(0, 0);
            this.ItemFordtLastInspectionDate.MaxSize = new System.Drawing.Size(265, 24);
            this.ItemFordtLastInspectionDate.MinSize = new System.Drawing.Size(265, 24);
            this.ItemFordtLastInspectionDate.Name = "ItemFordtLastInspectionDate";
            this.ItemFordtLastInspectionDate.Size = new System.Drawing.Size(265, 24);
            this.ItemFordtLastInspectionDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtLastInspectionDate.Text = "Last Inspection Date:";
            this.ItemFordtLastInspectionDate.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintInspectionCycle
            // 
            this.ItemForintInspectionCycle.Control = this.intInspectionCycleSpinEdit;
            this.ItemForintInspectionCycle.CustomizationFormText = "Inspection Cycle:";
            this.ItemForintInspectionCycle.Location = new System.Drawing.Point(0, 24);
            this.ItemForintInspectionCycle.MaxSize = new System.Drawing.Size(265, 24);
            this.ItemForintInspectionCycle.MinSize = new System.Drawing.Size(265, 24);
            this.ItemForintInspectionCycle.Name = "ItemForintInspectionCycle";
            this.ItemForintInspectionCycle.Size = new System.Drawing.Size(265, 24);
            this.ItemForintInspectionCycle.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForintInspectionCycle.Text = "Inspection Cycle:";
            this.ItemForintInspectionCycle.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForstrInspectionUnit
            // 
            this.ItemForstrInspectionUnit.Control = this.strInspectionUnitComboBoxEdit;
            this.ItemForstrInspectionUnit.CustomizationFormText = "Inspection Unit:";
            this.ItemForstrInspectionUnit.Location = new System.Drawing.Point(265, 24);
            this.ItemForstrInspectionUnit.MaxSize = new System.Drawing.Size(269, 24);
            this.ItemForstrInspectionUnit.MinSize = new System.Drawing.Size(269, 24);
            this.ItemForstrInspectionUnit.Name = "ItemForstrInspectionUnit";
            this.ItemForstrInspectionUnit.Size = new System.Drawing.Size(269, 24);
            this.ItemForstrInspectionUnit.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrInspectionUnit.Text = "Inspection Unit:";
            this.ItemForstrInspectionUnit.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemFordtSurveyDate
            // 
            this.ItemFordtSurveyDate.Control = this.dtSurveyDateDateEdit;
            this.ItemFordtSurveyDate.CustomizationFormText = "Survey Date:";
            this.ItemFordtSurveyDate.Location = new System.Drawing.Point(265, 48);
            this.ItemFordtSurveyDate.MaxSize = new System.Drawing.Size(269, 24);
            this.ItemFordtSurveyDate.MinSize = new System.Drawing.Size(269, 24);
            this.ItemFordtSurveyDate.Name = "ItemFordtSurveyDate";
            this.ItemFordtSurveyDate.Size = new System.Drawing.Size(269, 24);
            this.ItemFordtSurveyDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtSurveyDate.Text = "Survey Date:";
            this.ItemFordtSurveyDate.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Linked Inspections";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.splitterItem1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 82);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1084, 540);
            this.layoutControlGroup4.Text = "Linked Inspections and Actions";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridSplitContainer1;
            this.layoutControlItem2.CustomizationFormText = "Inspection Grid:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1060, 216);
            this.layoutControlItem2.Text = "Inspection Grid:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridSplitContainer2;
            this.layoutControlItem3.CustomizationFormText = "Action Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 222);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1060, 272);
            this.layoutControlItem3.Text = "Action Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 216);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1060, 6);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(1084, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(265, 0);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(819, 24);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(534, 24);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(550, 24);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.Location = new System.Drawing.Point(534, 48);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(550, 24);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Planting";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintPlantSource,
            this.ItemForintReplantCount,
            this.ItemFordtPlantDate,
            this.emptySpaceItem9,
            this.ItemForintPlantSize,
            this.ItemForintPlantMethod,
            this.ItemForintGroundType,
            this.ItemForintProtectionType,
            this.emptySpaceItem8});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlGroup12.Text = "Planting";
            // 
            // ItemForintPlantSource
            // 
            this.ItemForintPlantSource.Control = this.intPlantSourceGridLookUpEdit;
            this.ItemForintPlantSource.CustomizationFormText = "Plant Source:";
            this.ItemForintPlantSource.Location = new System.Drawing.Point(0, 50);
            this.ItemForintPlantSource.Name = "ItemForintPlantSource";
            this.ItemForintPlantSource.Size = new System.Drawing.Size(576, 26);
            this.ItemForintPlantSource.Text = "Plant Source:";
            this.ItemForintPlantSource.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintReplantCount
            // 
            this.ItemForintReplantCount.Control = this.intReplantCountSpinEdit;
            this.ItemForintReplantCount.CustomizationFormText = "Replant Count:";
            this.ItemForintReplantCount.Location = new System.Drawing.Point(0, 102);
            this.ItemForintReplantCount.Name = "ItemForintReplantCount";
            this.ItemForintReplantCount.Size = new System.Drawing.Size(576, 24);
            this.ItemForintReplantCount.Text = "Replant Count:";
            this.ItemForintReplantCount.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemFordtPlantDate
            // 
            this.ItemFordtPlantDate.Control = this.dtPlantDateDateEdit;
            this.ItemFordtPlantDate.CustomizationFormText = "Plant Date:";
            this.ItemFordtPlantDate.Location = new System.Drawing.Point(0, 0);
            this.ItemFordtPlantDate.Name = "ItemFordtPlantDate";
            this.ItemFordtPlantDate.Size = new System.Drawing.Size(576, 24);
            this.ItemFordtPlantDate.Text = "Plant Date:";
            this.ItemFordtPlantDate.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 178);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1084, 444);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForintPlantSize
            // 
            this.ItemForintPlantSize.Control = this.intPlantSizeGridLookUpEdit;
            this.ItemForintPlantSize.CustomizationFormText = "Plant Size:";
            this.ItemForintPlantSize.Location = new System.Drawing.Point(0, 24);
            this.ItemForintPlantSize.Name = "ItemForintPlantSize";
            this.ItemForintPlantSize.Size = new System.Drawing.Size(576, 26);
            this.ItemForintPlantSize.Text = "Plant Size:";
            this.ItemForintPlantSize.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintPlantMethod
            // 
            this.ItemForintPlantMethod.Control = this.intPlantMethodGridLookUpEdit;
            this.ItemForintPlantMethod.CustomizationFormText = "Plant Method:";
            this.ItemForintPlantMethod.Location = new System.Drawing.Point(0, 76);
            this.ItemForintPlantMethod.Name = "ItemForintPlantMethod";
            this.ItemForintPlantMethod.Size = new System.Drawing.Size(576, 26);
            this.ItemForintPlantMethod.Text = "Plant Method:";
            this.ItemForintPlantMethod.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintGroundType
            // 
            this.ItemForintGroundType.Control = this.intGroundTypeGridLookUpEdit;
            this.ItemForintGroundType.CustomizationFormText = "Ground Type:";
            this.ItemForintGroundType.Location = new System.Drawing.Point(0, 126);
            this.ItemForintGroundType.Name = "ItemForintGroundType";
            this.ItemForintGroundType.Size = new System.Drawing.Size(576, 26);
            this.ItemForintGroundType.Text = "Ground Type:";
            this.ItemForintGroundType.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintProtectionType
            // 
            this.ItemForintProtectionType.Control = this.intProtectionTypeGridLookUpEdit;
            this.ItemForintProtectionType.CustomizationFormText = "Protection Type:";
            this.ItemForintProtectionType.Location = new System.Drawing.Point(0, 152);
            this.ItemForintProtectionType.Name = "ItemForintProtectionType";
            this.ItemForintProtectionType.Size = new System.Drawing.Size(576, 26);
            this.ItemForintProtectionType.Text = "Protection Type:";
            this.ItemForintProtectionType.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(576, 0);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(508, 178);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "User Defined";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForUserPickList1,
            this.ItemForUserPickList2,
            this.ItemForUserPickList3,
            this.ItemForstrUser3,
            this.ItemForstrUser2,
            this.ItemForstrUser1,
            this.emptySpaceItem10,
            this.emptySpaceItem11});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlGroup9.Text = "User Fields";
            // 
            // ItemForUserPickList1
            // 
            this.ItemForUserPickList1.Control = this.UserPickList1GridLookUpEdit;
            this.ItemForUserPickList1.CustomizationFormText = "Pick List 1:";
            this.ItemForUserPickList1.Location = new System.Drawing.Point(0, 72);
            this.ItemForUserPickList1.Name = "ItemForUserPickList1";
            this.ItemForUserPickList1.Size = new System.Drawing.Size(561, 26);
            this.ItemForUserPickList1.Text = "Pick List 1:";
            this.ItemForUserPickList1.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForUserPickList2
            // 
            this.ItemForUserPickList2.Control = this.UserPickList2GridLookUpEdit;
            this.ItemForUserPickList2.CustomizationFormText = "Pick List 2:";
            this.ItemForUserPickList2.Location = new System.Drawing.Point(0, 98);
            this.ItemForUserPickList2.Name = "ItemForUserPickList2";
            this.ItemForUserPickList2.Size = new System.Drawing.Size(561, 26);
            this.ItemForUserPickList2.Text = "Pick List 2:";
            this.ItemForUserPickList2.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForUserPickList3
            // 
            this.ItemForUserPickList3.Control = this.UserPickList3GridLookUpEdit;
            this.ItemForUserPickList3.CustomizationFormText = "Pick List 3:";
            this.ItemForUserPickList3.Location = new System.Drawing.Point(0, 124);
            this.ItemForUserPickList3.Name = "ItemForUserPickList3";
            this.ItemForUserPickList3.Size = new System.Drawing.Size(561, 26);
            this.ItemForUserPickList3.Text = "Pick List 3:";
            this.ItemForUserPickList3.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForstrUser3
            // 
            this.ItemForstrUser3.Control = this.strUser3TextEdit;
            this.ItemForstrUser3.CustomizationFormText = "Text 3:";
            this.ItemForstrUser3.Location = new System.Drawing.Point(0, 48);
            this.ItemForstrUser3.Name = "ItemForstrUser3";
            this.ItemForstrUser3.Size = new System.Drawing.Size(561, 24);
            this.ItemForstrUser3.Text = "Text 3:";
            this.ItemForstrUser3.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForstrUser2
            // 
            this.ItemForstrUser2.Control = this.strUser2TextEdit;
            this.ItemForstrUser2.CustomizationFormText = "Text 2:";
            this.ItemForstrUser2.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrUser2.Name = "ItemForstrUser2";
            this.ItemForstrUser2.Size = new System.Drawing.Size(561, 24);
            this.ItemForstrUser2.Text = "Text 2:";
            this.ItemForstrUser2.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForstrUser1
            // 
            this.ItemForstrUser1.Control = this.strUser1TextEdit;
            this.ItemForstrUser1.CustomizationFormText = "Text 1:";
            this.ItemForstrUser1.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrUser1.Name = "ItemForstrUser1";
            this.ItemForstrUser1.Size = new System.Drawing.Size(561, 24);
            this.ItemForstrUser1.Text = "Text 1:";
            this.ItemForstrUser1.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(561, 0);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(523, 150);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 150);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(1084, 472);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup27
            // 
            this.layoutControlGroup27.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup27.CaptionImageOptions.Image")));
            this.layoutControlGroup27.CustomizationFormText = "Linked Pictures";
            this.layoutControlGroup27.ExpandButtonVisible = true;
            this.layoutControlGroup27.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup27.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPicturesGrid});
            this.layoutControlGroup27.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup27.Name = "layoutControlGroup27";
            this.layoutControlGroup27.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlGroup27.Text = "Linked Pictures";
            // 
            // ItemForPicturesGrid
            // 
            this.ItemForPicturesGrid.Control = this.gridControl39;
            this.ItemForPicturesGrid.CustomizationFormText = "Linked Pictures Grid:";
            this.ItemForPicturesGrid.Location = new System.Drawing.Point(0, 0);
            this.ItemForPicturesGrid.Name = "ItemForPicturesGrid";
            this.ItemForPicturesGrid.Size = new System.Drawing.Size(1084, 622);
            this.ItemForPicturesGrid.Text = "Linked Pictures Grid:";
            this.ItemForPicturesGrid.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForPicturesGrid.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.linked_documents_16_16;
            this.layoutControlGroup5.CustomizationFormText = "Linked Documents";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlGroup5.Text = "Linked Documents";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridSplitContainer3;
            this.layoutControlItem4.CustomizationFormText = "Linked Documents:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlItem4.Text = "Linked Documents:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup13.CaptionImageOptions.Image")));
            this.layoutControlGroup13.CustomizationFormText = "Remarks";
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(1084, 622);
            this.layoutControlGroup13.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(1084, 622);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "Core Info";
            this.layoutControlGroup14.ExpandButtonVisible = true;
            this.layoutControlGroup14.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientName,
            this.ItemForintStatus,
            this.ItemForstrTreeRef,
            this.ItemForSiteName,
            this.ItemForintType,
            this.ItemForintOwnershipID});
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(1108, 144);
            this.layoutControlGroup14.Text = "Core Data";
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 0);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(1084, 24);
            this.ItemForClientName.Text = "Client:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintStatus
            // 
            this.ItemForintStatus.Control = this.intStatusGridLookUpEdit;
            this.ItemForintStatus.CustomizationFormText = "Status:";
            this.ItemForintStatus.Location = new System.Drawing.Point(0, 72);
            this.ItemForintStatus.Name = "ItemForintStatus";
            this.ItemForintStatus.Size = new System.Drawing.Size(542, 26);
            this.ItemForintStatus.Text = "Status:";
            this.ItemForintStatus.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForstrTreeRef
            // 
            this.ItemForstrTreeRef.AllowHide = false;
            this.ItemForstrTreeRef.Control = this.strTreeRefButtonEdit;
            this.ItemForstrTreeRef.CustomizationFormText = "Tree Reference:";
            this.ItemForstrTreeRef.Location = new System.Drawing.Point(0, 48);
            this.ItemForstrTreeRef.Name = "ItemForstrTreeRef";
            this.ItemForstrTreeRef.Size = new System.Drawing.Size(542, 24);
            this.ItemForstrTreeRef.Text = "Tree Reference:";
            this.ItemForstrTreeRef.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameButtonEdit;
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 24);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(1084, 24);
            this.ItemForSiteName.Text = "Site:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintType
            // 
            this.ItemForintType.Control = this.intTypeGridLookUpEdit;
            this.ItemForintType.CustomizationFormText = "Object Type:";
            this.ItemForintType.Location = new System.Drawing.Point(542, 48);
            this.ItemForintType.Name = "ItemForintType";
            this.ItemForintType.Size = new System.Drawing.Size(542, 24);
            this.ItemForintType.Text = "Object Type:";
            this.ItemForintType.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForintOwnershipID
            // 
            this.ItemForintOwnershipID.Control = this.intOwnershipIDGridLookUpEdit;
            this.ItemForintOwnershipID.CustomizationFormText = "Ownership:";
            this.ItemForintOwnershipID.Location = new System.Drawing.Point(542, 72);
            this.ItemForintOwnershipID.Name = "ItemForintOwnershipID";
            this.ItemForintOwnershipID.Size = new System.Drawing.Size(542, 26);
            this.ItemForintOwnershipID.Text = "Ownership:";
            this.ItemForintOwnershipID.TextSize = new System.Drawing.Size(116, 13);
            // 
            // sp01204_AT_Edit_Tree_DetailsTableAdapter
            // 
            this.sp01204_AT_Edit_Tree_DetailsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01370_AT_Object_Types_No_BlankTableAdapter
            // 
            this.sp01370_AT_Object_Types_No_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01371_AT_Object_Statuses_No_BlankTableAdapter
            // 
            this.sp01371_AT_Object_Statuses_No_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01362_AT_Ownership_List_With_BlankTableAdapter
            // 
            this.sp01362_AT_Ownership_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00179_Species_List_With_BlankTableAdapter
            // 
            this.sp00179_Species_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter
            // 
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.ClearBeforeFill = true;
            // 
            // sp01367_AT_Inspections_For_TreesTableAdapter
            // 
            this.sp01367_AT_Inspections_For_TreesTableAdapter.ClearBeforeFill = true;
            // 
            // sp01374_AT_Species_Varieties_With_BlanksTableAdapter
            // 
            this.sp01374_AT_Species_Varieties_With_BlanksTableAdapter.ClearBeforeFill = true;
            // 
            // sp01375_AT_User_Screen_SettingsBindingSource
            // 
            this.sp01375_AT_User_Screen_SettingsBindingSource.DataMember = "sp01375_AT_User_Screen_Settings";
            this.sp01375_AT_User_Screen_SettingsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp01375_AT_User_Screen_SettingsTableAdapter
            // 
            this.sp01375_AT_User_Screen_SettingsTableAdapter.ClearBeforeFill = true;
            // 
            // bbiBlockAddAction
            // 
            this.bbiBlockAddAction.Caption = "Add Action to Selected Records";
            this.bbiBlockAddAction.Id = 26;
            this.bbiBlockAddAction.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAddAction.ImageOptions.Image")));
            this.bbiBlockAddAction.Name = "bbiBlockAddAction";
            this.bbiBlockAddAction.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockAddAction_ItemClick);
            // 
            // sp01380_AT_Actions_Linked_To_InspectionsTableAdapter
            // 
            this.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.CavatStemDiameterSpinEdit;
            this.layoutControlItem18.CustomizationFormText = "Stem Diameter:";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.Name = "layoutControlItem5";
            this.layoutControlItem18.Size = new System.Drawing.Size(473, 24);
            this.layoutControlItem18.Text = "Stem Diameter:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(116, 13);
            // 
            // sp01436_AT_CAVAT_List_CTI_FactorsTableAdapter
            // 
            this.sp01436_AT_CAVAT_List_CTI_FactorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01437_AT_CAVAT_List_CTI_AccessibilityTableAdapter
            // 
            this.sp01437_AT_CAVAT_List_CTI_AccessibilityTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSetATWorkOrdersBindingSource
            // 
            this.dataSetATWorkOrdersBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            this.dataSetATWorkOrdersBindingSource.Position = 0;
            // 
            // sp01438_AT_CAVAT_List_CTI_FunctionalValueFactorsTableAdapter
            // 
            this.sp01438_AT_CAVAT_List_CTI_FunctionalValueFactorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01439_AT_CAVAT_List_CTI_AmenityFactorsTableAdapter
            // 
            this.sp01439_AT_CAVAT_List_CTI_AmenityFactorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01440_AT_CAVAT_List_CTI_AppropriatenessTableAdapter
            // 
            this.sp01440_AT_CAVAT_List_CTI_AppropriatenessTableAdapter.ClearBeforeFill = true;
            // 
            // sp01441_AT_CAVAT_List_SLEFactors2TableAdapter
            // 
            this.sp01441_AT_CAVAT_List_SLEFactors2TableAdapter.ClearBeforeFill = true;
            // 
            // sp01442_AT_CAVAT_List_CTIRatingTableAdapter
            // 
            this.sp01442_AT_CAVAT_List_CTIRatingTableAdapter.ClearBeforeFill = true;
            // 
            // sp01443_AT_CAVAT_List_FunctionalAdjustmentTableAdapter
            // 
            this.sp01443_AT_CAVAT_List_FunctionalAdjustmentTableAdapter.ClearBeforeFill = true;
            // 
            // sp01444_AT_CAVAT_List_SLEFactor2TableAdapter
            // 
            this.sp01444_AT_CAVAT_List_SLEFactor2TableAdapter.ClearBeforeFill = true;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp02064_AT_Tree_Pictures_ListTableAdapter
            // 
            this.sp02064_AT_Tree_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending39
            // 
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending39.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending39.GridControl = this.gridControl39;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl2;
            // 
            // frm_AT_Tree_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1145, 756);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.popupContainerControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Tree_Edit";
            this.Text = "Edit Tree";
            this.Activated += new System.EventHandler(this.frm_AT_Tree_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Tree_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Tree_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.popupContainerControl1, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SiteCodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01204ATEditTreeDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientEditTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp02064ATTreePicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationPolyXYMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationYSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationXSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatSLEFactorGridLookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01444ATCAVATListSLEFactor2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatFunctionalValueTextEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatFunctionalValueFactorGridLookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01443ATCAVATListFunctionalAdjustmentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatCTIValueTextEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatCTIFactorGridLookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01442ATCAVATListCTIRatingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatUnitValueFactorSpinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatStemDiameterSpinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatSLEFactorGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01441ATCAVATListSLEFactors2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatAdjustedValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatAdjustedPercentSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatAppropriatenessGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01440ATCAVATListCTIAppropriatenessBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatFunctionalValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatAmenityFactorsGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01439ATCAVATListCTIAmenityFactorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatFunctionalValueFactorGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01438ATCAVATListCTIFunctionalValueFactorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatCTIValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatAccessibilityGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01437ATCAVATListCTIAccessibilityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatCTIFactorGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01436ATCAVATListCTIFactorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatBasicValueTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatUnitValueFactorSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatStemDiameterSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01380ATActionsLinkedToInspectionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01367ATInspectionsForTreesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeciesVarietyGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01374ATSpeciesVarietiesWithBlanksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObjectWidthSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObjectLengthSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.decHeightSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTreeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strGridRefTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.idTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intDistanceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strHouseNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLastInspectionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLastInspectionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intInspectionCycleSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNextInspectionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNextInspectionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intDBHSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownDiameterSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownDepthSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPlantDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPlantDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intGroupNoSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strMapLabelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intXSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intYSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.decAreaHaSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intReplantCountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtSurveyDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtSurveyDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.decRiskFactorSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CavatSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StemCountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrownNorthSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrownSouthSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrownEastSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrownWestSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SULESpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intDBHRangeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intHeightRangeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSpeciesIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00179SpeciesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStatusGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01371ATObjectStatusesNoBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strTreeRefButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intLegalStatusGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSafetyPriorityGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSiteHazardClassGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strPolygonXYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPlantSourceGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPlantMethodGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList1GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intAccessGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intVisibilityGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intGroundTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intProtectionTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intAgeClassGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strSituationMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intNearbyObjectGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intNearbyObject2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intNearbyObject3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intOwnershipIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01362ATOwnershipListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RetentionCategoryGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01370ATObjectTypesNoBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSizeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strInspectionUnitComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPlantSizeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strContextPopupContainerEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClearAnyExistingValuesCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strManagementPopupContainerEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintTreeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrGridRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintDBH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintDBHRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintHeightRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintLegalStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordecHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintGroupNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSULE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintAgeClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStemCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrContext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrManagement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownDiameter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownDepth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCrownNorth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCrownWest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCrownSouth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCrownEast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSpeciesID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpeciesVarietyGridLookUpEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRetentionCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSafetyPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordecRiskFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSiteHazardClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintNearbyObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintNearbyObject3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintNearbyObject2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCavat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrHouseName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintAccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrSituation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintVisibility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordecAreaHa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrMapLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForObjectLengthSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForObjectWidthSpinEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrPolygonXY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtNextInspectionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtLastInspectionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintInspectionCycle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrInspectionUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtSurveyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPlantSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintReplantCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtPlantDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPlantSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPlantMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintGroundType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintProtectionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPicturesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrTreeRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintOwnershipID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01375_AT_User_Screen_SettingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetATWorkOrdersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit intTreeIDTextEdit;
        private System.Windows.Forms.BindingSource sp01204ATEditTreeDetailsBindingSource;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraEditors.TextEdit idTextEdit;
        private DevExpress.XtraEditors.TextEdit strGridRefTextEdit;
        private DevExpress.XtraEditors.SpinEdit intDistanceSpinEdit;
        private DevExpress.XtraEditors.TextEdit strHouseNameTextEdit;
        private DevExpress.XtraEditors.DateEdit dtLastInspectionDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit intInspectionCycleSpinEdit;
        private DevExpress.XtraEditors.DateEdit dtNextInspectionDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit intDBHSpinEdit;
        private DevExpress.XtraEditors.SpinEdit intCrownDiameterSpinEdit;
        private DevExpress.XtraEditors.SpinEdit intCrownDepthSpinEdit;
        private DevExpress.XtraEditors.DateEdit dtPlantDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit intGroupNoSpinEdit;
        private DevExpress.XtraEditors.TextEdit strPostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit strMapLabelTextEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraEditors.TextEdit strUser1TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser2TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser3TextEdit;
        private DevExpress.XtraEditors.SpinEdit intXSpinEdit;
        private DevExpress.XtraEditors.SpinEdit intYSpinEdit;
        private DevExpress.XtraEditors.SpinEdit decAreaHaSpinEdit;
        private DevExpress.XtraEditors.SpinEdit intReplantCountSpinEdit;
        private DevExpress.XtraEditors.DateEdit dtSurveyDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit decRiskFactorSpinEdit;
        private DevExpress.XtraEditors.DateEdit DateAddedDateEdit;
        private DevExpress.XtraEditors.SpinEdit CavatSpinEdit;
        private DevExpress.XtraEditors.SpinEdit StemCountSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CrownNorthSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CrownSouthSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CrownEastSpinEdit;
        private DevExpress.XtraEditors.SpinEdit CrownWestSpinEdit;
        private DevExpress.XtraEditors.SpinEdit SULESpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintTreeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrTreeRef;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrGridRef;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrHouseName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSpeciesID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintAccess;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintVisibility;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrContext;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrManagement;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintLegalStatus;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtLastInspectionDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintInspectionCycle;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrInspectionUnit;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtNextInspectionDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintGroundType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintDBH;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintProtectionType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownDiameter;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownDepth;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtPlantDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintGroupNo;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSafetyPriority;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSiteHazardClass;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintPlantSize;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintPlantSource;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintPlantMethod;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrPostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintStatus;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSize;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintNearbyObject;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintAgeClass;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintReplantCount;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtSurveyDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrSituation;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordecRiskFactor;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintNearbyObject2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintNearbyObject3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintDBHRange;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintHeightRange;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintOwnershipID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateAdded;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCavat;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStemCount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRetentionCategory;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSULE;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList3;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01204_AT_Edit_Tree_DetailsTableAdapter sp01204_AT_Edit_Tree_DetailsTableAdapter;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintDistance;
        private DevExpress.XtraEditors.SpinEdit decHeightSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordecHeight;
        private DevExpress.XtraEditors.GridLookUpEdit intDBHRangeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit intHeightRangeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GridLookUpEdit intSpeciesIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.GridLookUpEdit intStatusGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.ButtonEdit strTreeRefButtonEdit;
        private DevExpress.XtraEditors.GridLookUpEdit intLegalStatusGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.GridLookUpEdit intSafetyPriorityGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.GridLookUpEdit intSiteHazardClassGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.MemoEdit strPolygonXYTextEdit;
        private DevExpress.XtraEditors.GridLookUpEdit intPlantSourceGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraEditors.GridLookUpEdit intPlantMethodGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList1GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraEditors.GridLookUpEdit intAccessGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraEditors.GridLookUpEdit intVisibilityGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private DevExpress.XtraEditors.GridLookUpEdit intGroundTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView16;
        private DevExpress.XtraEditors.GridLookUpEdit intProtectionTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView17;
        private DevExpress.XtraEditors.GridLookUpEdit intAgeClassGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView18;
        private DevExpress.XtraEditors.MemoEdit strSituationMemoEdit;
        private DevExpress.XtraEditors.GridLookUpEdit intNearbyObjectGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView20;
        private DevExpress.XtraEditors.GridLookUpEdit intNearbyObject2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView21;
        private DevExpress.XtraEditors.GridLookUpEdit intNearbyObject3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView22;
        private DevExpress.XtraEditors.GridLookUpEdit intOwnershipIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView23;
        private DevExpress.XtraEditors.GridLookUpEdit RetentionCategoryGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView24;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView25;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCrownNorth;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCrownWest;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCrownSouth;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCrownEast;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForid;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintX;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordecAreaHa;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrMapLabel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintY;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrPolygonXY;
        private DevExpress.XtraBars.BarButtonItem bbiCopyAllValues;
        private DevExpress.XtraBars.BarButtonItem bbiCopySelectedValues;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraEditors.GridLookUpEdit intTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView19;
        private System.Windows.Forms.BindingSource sp01370ATObjectTypesNoBlankBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01370_AT_Object_Types_No_BlankTableAdapter sp01370_AT_Object_Types_No_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private System.Windows.Forms.BindingSource sp01371ATObjectStatusesNoBlankBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01371_AT_Object_Statuses_No_BlankTableAdapter sp01371_AT_Object_Statuses_No_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private System.Windows.Forms.BindingSource sp01362ATOwnershipListWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01362_AT_Ownership_List_With_BlankTableAdapter sp01362_AT_Ownership_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipCode;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipID;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnershipName;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private WoodPlanDataSet woodPlanDataSet;
        private System.Windows.Forms.BindingSource sp00179SpeciesListWithBlankBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00179_Species_List_With_BlankTableAdapter sp00179_Species_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesRemark;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesRiskFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesScientificName;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesType;
        private DevExpress.XtraEditors.GridLookUpEdit intSizeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView26;
        private System.Windows.Forms.BindingSource sp01372ATMultiplePicklistsWithBlanksBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colItemCode;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn94;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn108;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn109;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn110;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn111;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn112;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn113;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn107;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn114;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn115;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn116;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn117;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn118;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn119;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn120;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn121;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn122;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn123;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn124;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn125;
        private DevExpress.XtraEditors.ComboBoxEdit strInspectionUnitComboBoxEdit;
        private DevExpress.XtraEditors.GridLookUpEdit intPlantSizeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraEditors.PopupContainerEdit strContextPopupContainerEdit;
        private DevExpress.XtraEditors.PopupContainerEdit strManagementPopupContainerEdit;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView27;
        private DevExpress.XtraEditors.CheckEdit ClearAnyExistingValuesCheckEdit;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn126;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn127;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn128;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn129;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn130;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn131;
        private DevExpress.XtraEditors.SimpleButton btnPopoupContainerOK1;
        private DevExpress.XtraGrid.Columns.GridColumn colBandStart;
        private DevExpress.XtraGrid.Columns.GridColumn colBandEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colBandStart1;
        private DevExpress.XtraGrid.Columns.GridColumn colBandEnd1;
        private System.Windows.Forms.BindingSource sp01367ATInspectionsForTreesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityName1;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeX;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeY;
        private DevExpress.XtraGrid.Columns.GridColumn colTreePolygonXY;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionInspector;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionIncidentReference;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionIncidentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIncidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionAngleToVertical;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionLastModified;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionStemDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownPhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownDisease3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCrownFoliation3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionGeneralCondition;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical2;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionBasePhysical3;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionVitality;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedOutstandingActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colNoFurtherActionRequired;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01367_AT_Inspections_For_TreesTableAdapter sp01367_AT_Inspections_For_TreesTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.BindingSource sp01374ATSpeciesVarietiesWithBlanksBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01374_AT_Species_Varieties_With_BlanksTableAdapter sp01374_AT_Species_Varieties_With_BlanksTableAdapter;
        private DevExpress.XtraEditors.SpinEdit ObjectWidthSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ObjectLengthSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForObjectLengthSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForObjectWidthSpinEdit;
        private DevExpress.XtraEditors.GridLookUpEdit SpeciesVarietyGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView28;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSpeciesVarietyGridLookUpEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colItemID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesName1;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarButtonItem bbiShowMapButton;
        private System.Windows.Forms.BindingSource sp01375_AT_User_Screen_SettingsBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01375_AT_User_Screen_SettingsTableAdapter sp01375_AT_User_Screen_SettingsTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiRememberDetails;
        private DevExpress.XtraBars.BarButtonItem bbiReload;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn132;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn133;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn134;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn135;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiBlockAddAction;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView29;
        private System.Windows.Forms.BindingSource sp01380ATActionsLinkedToInspectionsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAction;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colActionSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colActionOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudget;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActionScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostDifference;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCompletionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionTimeliness;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter sp01380_AT_Actions_Linked_To_InspectionsTableAdapter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView30;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn136;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.SpinEdit CavatStemDiameterSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SpinEdit CavatUnitValueFactorSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit CavatBasicValueTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.GridLookUpEdit CavatCTIFactorGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView31;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup18;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.GridLookUpEdit CavatAccessibilityGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit2View;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit CavatCTIValueTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.GridLookUpEdit CavatFunctionalValueFactorGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView32;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.TextEdit CavatFunctionalValueTextEdit;
        private DevExpress.XtraEditors.GridLookUpEdit CavatAmenityFactorsGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.GridLookUpEdit CavatAppropriatenessGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.SpinEdit CavatAdjustedPercentSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.TextEdit CavatAdjustedValueTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.GridLookUpEdit CavatSLEFactorGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView35;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.SpinEdit CavatStemDiameterSpinEdit2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup23;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.SpinEdit CavatUnitValueFactorSpinEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.TextEdit CavatCTIValueTextEdit2;
        private DevExpress.XtraEditors.GridLookUpEdit CavatCTIFactorGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.TextEdit CavatFunctionalValueTextEdit2;
        private DevExpress.XtraEditors.GridLookUpEdit CavatFunctionalValueFactorGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView37;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraEditors.GridLookUpEdit CavatSLEFactorGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView38;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayValue;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colListType;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn141;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn142;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn143;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn144;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn145;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn146;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn147;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn148;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn149;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn150;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn151;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn152;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn153;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn154;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn155;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn156;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn137;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn138;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn139;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn140;
        private System.Windows.Forms.BindingSource sp01436ATCAVATListCTIFactorsBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01436_AT_CAVAT_List_CTI_FactorsTableAdapter sp01436_AT_CAVAT_List_CTI_FactorsTableAdapter;
        private System.Windows.Forms.BindingSource sp01437ATCAVATListCTIAccessibilityBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01437_AT_CAVAT_List_CTI_AccessibilityTableAdapter sp01437_AT_CAVAT_List_CTI_AccessibilityTableAdapter;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private System.Windows.Forms.BindingSource dataSetATWorkOrdersBindingSource;
        private System.Windows.Forms.BindingSource sp01438ATCAVATListCTIFunctionalValueFactorsBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01438_AT_CAVAT_List_CTI_FunctionalValueFactorsTableAdapter sp01438_AT_CAVAT_List_CTI_FunctionalValueFactorsTableAdapter;
        private System.Windows.Forms.BindingSource sp01439ATCAVATListCTIAmenityFactorsBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01439_AT_CAVAT_List_CTI_AmenityFactorsTableAdapter sp01439_AT_CAVAT_List_CTI_AmenityFactorsTableAdapter;
        private System.Windows.Forms.BindingSource sp01440ATCAVATListCTIAppropriatenessBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01440_AT_CAVAT_List_CTI_AppropriatenessTableAdapter sp01440_AT_CAVAT_List_CTI_AppropriatenessTableAdapter;
        private System.Windows.Forms.BindingSource sp01441ATCAVATListSLEFactors2BindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01441_AT_CAVAT_List_SLEFactors2TableAdapter sp01441_AT_CAVAT_List_SLEFactors2TableAdapter;
        private System.Windows.Forms.BindingSource sp01442ATCAVATListCTIRatingBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01442_AT_CAVAT_List_CTIRatingTableAdapter sp01442_AT_CAVAT_List_CTIRatingTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn157;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn158;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn159;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn160;
        private System.Windows.Forms.BindingSource sp01443ATCAVATListFunctionalAdjustmentBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01443_AT_CAVAT_List_FunctionalAdjustmentTableAdapter sp01443_AT_CAVAT_List_FunctionalAdjustmentTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn161;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn162;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn163;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn164;
        private System.Windows.Forms.BindingSource sp01444ATCAVATListSLEFactor2BindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01444_AT_CAVAT_List_SLEFactor2TableAdapter sp01444_AT_CAVAT_List_SLEFactor2TableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn165;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn166;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn167;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn168;
        private DevExpress.XtraEditors.SpinEdit LocationXSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.MemoEdit LocationPolyXYMemoEdit;
        private DevExpress.XtraEditors.SpinEdit LocationYSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup27;
        private DevExpress.XtraGrid.GridControl gridControl39;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn169;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn170;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn171;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn172;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn173;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn174;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn175;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn176;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn177;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn178;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn179;
        private DevExpress.XtraGrid.Columns.GridColumn colShortLinkedRecordDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPicturesGrid;
        private System.Windows.Forms.BindingSource sp02064ATTreePicturesListBindingSource;
        private DataSet_ATTableAdapters.sp02064_AT_Tree_Pictures_ListTableAdapter sp02064_AT_Tree_Pictures_ListTableAdapter;
        private DevExpress.XtraEditors.TextEdit SiteIDTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientEditTextEdit;
        private DevExpress.XtraEditors.ButtonEdit SiteNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending39;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraEditors.TextEdit SiteCodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteCode;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colAmenityArbVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArbVisible;
    }
}

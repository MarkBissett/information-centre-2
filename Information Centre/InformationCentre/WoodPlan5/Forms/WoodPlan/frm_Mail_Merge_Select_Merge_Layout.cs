using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraLayout;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Mail_Merge_Select_Merge_Layout : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public int intMailMergeLayoutID = 0;
        public int intMailMergeTypeID = 0;
        public int intModuleID = 0;
        public int intCompanyHeaderID = 0;  // Only used when Running a mail merge from Incidents //
        public string strMode = "mailmerge";  // mailmerge = called by Normal MailMerge Process, mailmerge_incidents = called by Incidents Manager screen. //
        
        #endregion

        public frm_Mail_Merge_Select_Merge_Layout()
        {
            InitializeComponent();
        }

        private void frm_Mail_Merge_Select_Merge_Layout_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 20161;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp00233_Mail_Merge_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;

            if (strMode.ToLower() == "mailmerge_incidents")  // Only used when Running a mail merge from Incidents //
            {
                sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp00232_Company_Header_Drop_Down_List_With_BlankTableAdapter.Fill(dataSet_AT_WorkOrders.sp00232_Company_Header_Drop_Down_List_With_Blank);

                int intDefaultCompanyHeader = 0;
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    intDefaultCompanyHeader = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesDefaultCompanyHeader"));
                    if (intDefaultCompanyHeader != 0) CompanyHeaderGridLookUpEdit.EditValue = intDefaultCompanyHeader;
                }
                catch (Exception) { }
            }

            GridView view = (GridView)gridControl1.MainView;

            string strMailMergeTypeID = (intMailMergeTypeID != 0 ? intMailMergeTypeID.ToString() + "," : "");
            view.BeginUpdate();
            this.sp00233_Mail_Merge_ManagerTableAdapter.Fill(this.dataSet_Utilities.sp00233_Mail_Merge_Manager, intModuleID, strMailMergeTypeID);
            view.EndUpdate();
            gridControl1.ForceInitialize();
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            if (strMode.ToLower() == "mailmerge_incidents")  // Only used when Running a mail merge from Incidents //
            {
                this.Text = "Mail Merge - Select Layout and Company Header";
                CompanyHeaderGridLookUpEdit.Enabled = true;
                CompanyHeaderGridLookUpEdit.Visible = true;
                CompanyHeaderLabelControl.Enabled = true;
                CompanyHeaderLabelControl.Visible = true;
            }
            else
            {
                this.Text = "Mail Merge - Select Layout";
                CompanyHeaderGridLookUpEdit.Enabled = false;
                CompanyHeaderGridLookUpEdit.Visible = false;
                CompanyHeaderLabelControl.Enabled = false;
                CompanyHeaderLabelControl.Visible = false;
            }
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Mail Merge Types Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            GridView view = (GridView)sender;
            intMailMergeLayoutID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ReportLayoutID"));
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            if (intMailMergeLayoutID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a Mail Merge Layout before proceeding.", "Select Mail Merge Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (strMode.ToLower() == "mailmerge_incidents")  // Only used when Running a mail merge from Incidents //
            {
                try
                {
                    intCompanyHeaderID = Convert.ToInt32(CompanyHeaderGridLookUpEdit.EditValue);
                }
                catch (Exception) { }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }




    }
}


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_WorkOrder_Map_Preview : BaseObjects.frmBase
    {
        #region Instance Variables
        
        public string strImage;

        #endregion

        public frm_AT_Mapping_WorkOrder_Map_Preview()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_WorkOrder_Map_Preview_Load(object sender, EventArgs e)
        {
            this.FormID = 200414;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            try
            {
                pictureBox1.Image = Image.FromFile(strImage);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the image [" + ex.Message + "]!\n\nTry previewing again - if the problem persists, contact Technical Support.", "Preview Image", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
        }

        private void bbiPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                frmPrintPreview fPrintPreview = new frmPrintPreview();
                frmBase fbBase;
                fbBase = this;
                fPrintPreview.objObject = fbBase;
                fPrintPreview.passedImage = pictureBox1.Image;
                fPrintPreview.GlobalSettings = this.GlobalSettings;
                fPrintPreview.ShowDialog();
                fPrintPreview.Dispose();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred when attempting to print preview the map [" + ex.Message + "]!", "Work Order Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void bbiRotateAntiClockwise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            pictureBox1.Image = RotateImage(pictureBox1.Image, RotateFlipType.Rotate270FlipNone);
        }

        private void bbiRotateClockwise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            pictureBox1.Image = RotateImage(pictureBox1.Image, RotateFlipType.Rotate90FlipNone);
        }

        public static Image RotateImage(Image img, RotateFlipType rotate_type)
        {
            img.RotateFlip(rotate_type);
            return img;
        }

        private void frm_AT_Mapping_WorkOrder_Map_Preview_FormClosing(object sender, FormClosingEventArgs e)
        {
            pictureBox1.Dispose();
            pictureBox1.Image = null;  // Clear image and collect garbage so no references are left to it. //
            GC.GetTotalMemory(true);
        }


    }
}


﻿namespace WoodPlan5
{
    partial class frm_AT_Data_Sync
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Data_Sync));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFriendlyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInsertedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUpdatedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeletedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStoredProcGet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStoredProcSet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.progressBarControlExport = new DevExpress.XtraEditors.ProgressBarControl();
            this.btnExport = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.TimeoutLabel = new DevExpress.XtraEditors.LabelControl();
            this.labelLastImportDate = new DevExpress.XtraEditors.LabelControl();
            this.progressBarControlImport = new DevExpress.XtraEditors.ProgressBarControl();
            this.btnImport = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.dataSet_AT_DataTransfer = new WoodPlan5.DataSet_AT_DataTransfer();
            this.sp02102ATWebServiceTabletGetExportOverviewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp02102_AT_WebService_Tablet_Get_Export_OverviewTableAdapter = new WoodPlan5.DataSet_AT_DataTransferTableAdapters.sp02102_AT_WebService_Tablet_Get_Export_OverviewTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControlExport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControlImport.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataTransfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp02102ATWebServiceTabletGetExportOverviewBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(629, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 568);
            this.barDockControlBottom.Size = new System.Drawing.Size(629, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 568);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(629, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 568);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gridControl1.BackgroundImage")));
            this.gridControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gridControl1.DataSource = this.sp02102ATWebServiceTabletGetExportOverviewBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(3, 22);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(601, 329);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRecordType,
            this.colFriendlyName,
            this.colInsertedCount,
            this.colUpdatedCount,
            this.colDeletedCount,
            this.colStoredProcGet,
            this.colStoredProcSet,
            this.colRecordOrder});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type (Internal)";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colRecordType.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.OptionsFilter.AllowAutoFilter = false;
            this.colRecordType.OptionsFilter.AllowFilter = false;
            this.colRecordType.Width = 122;
            // 
            // colFriendlyName
            // 
            this.colFriendlyName.Caption = "Record Type";
            this.colFriendlyName.FieldName = "FriendlyName";
            this.colFriendlyName.Name = "colFriendlyName";
            this.colFriendlyName.OptionsColumn.AllowEdit = false;
            this.colFriendlyName.OptionsColumn.AllowFocus = false;
            this.colFriendlyName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colFriendlyName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colFriendlyName.OptionsColumn.ReadOnly = true;
            this.colFriendlyName.OptionsFilter.AllowAutoFilter = false;
            this.colFriendlyName.OptionsFilter.AllowFilter = false;
            this.colFriendlyName.Visible = true;
            this.colFriendlyName.VisibleIndex = 0;
            this.colFriendlyName.Width = 275;
            // 
            // colInsertedCount
            // 
            this.colInsertedCount.Caption = "Inserted Count";
            this.colInsertedCount.FieldName = "InsertedCount";
            this.colInsertedCount.Name = "colInsertedCount";
            this.colInsertedCount.OptionsColumn.AllowEdit = false;
            this.colInsertedCount.OptionsColumn.AllowFocus = false;
            this.colInsertedCount.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colInsertedCount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colInsertedCount.OptionsColumn.ReadOnly = true;
            this.colInsertedCount.OptionsFilter.AllowAutoFilter = false;
            this.colInsertedCount.OptionsFilter.AllowFilter = false;
            this.colInsertedCount.Visible = true;
            this.colInsertedCount.VisibleIndex = 1;
            this.colInsertedCount.Width = 87;
            // 
            // colUpdatedCount
            // 
            this.colUpdatedCount.Caption = "Updated Count";
            this.colUpdatedCount.FieldName = "UpdatedCount";
            this.colUpdatedCount.Name = "colUpdatedCount";
            this.colUpdatedCount.OptionsColumn.AllowEdit = false;
            this.colUpdatedCount.OptionsColumn.AllowFocus = false;
            this.colUpdatedCount.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colUpdatedCount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colUpdatedCount.OptionsColumn.ReadOnly = true;
            this.colUpdatedCount.OptionsFilter.AllowAutoFilter = false;
            this.colUpdatedCount.OptionsFilter.AllowFilter = false;
            this.colUpdatedCount.Visible = true;
            this.colUpdatedCount.VisibleIndex = 2;
            this.colUpdatedCount.Width = 85;
            // 
            // colDeletedCount
            // 
            this.colDeletedCount.Caption = "Deleted Count";
            this.colDeletedCount.FieldName = "DeletedCount";
            this.colDeletedCount.Name = "colDeletedCount";
            this.colDeletedCount.OptionsColumn.AllowEdit = false;
            this.colDeletedCount.OptionsColumn.AllowFocus = false;
            this.colDeletedCount.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colDeletedCount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colDeletedCount.OptionsColumn.ReadOnly = true;
            this.colDeletedCount.OptionsFilter.AllowAutoFilter = false;
            this.colDeletedCount.OptionsFilter.AllowFilter = false;
            this.colDeletedCount.Visible = true;
            this.colDeletedCount.VisibleIndex = 3;
            this.colDeletedCount.Width = 81;
            // 
            // colStoredProcGet
            // 
            this.colStoredProcGet.Caption = "Retrieve Stored Procedure";
            this.colStoredProcGet.FieldName = "StoredProcGet";
            this.colStoredProcGet.Name = "colStoredProcGet";
            this.colStoredProcGet.OptionsColumn.AllowEdit = false;
            this.colStoredProcGet.OptionsColumn.AllowFocus = false;
            this.colStoredProcGet.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colStoredProcGet.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colStoredProcGet.OptionsColumn.ReadOnly = true;
            this.colStoredProcGet.OptionsFilter.AllowAutoFilter = false;
            this.colStoredProcGet.OptionsFilter.AllowFilter = false;
            this.colStoredProcGet.Width = 270;
            // 
            // colStoredProcSet
            // 
            this.colStoredProcSet.Caption = "Update Stored Procedure";
            this.colStoredProcSet.FieldName = "StoredProcSet";
            this.colStoredProcSet.Name = "colStoredProcSet";
            this.colStoredProcSet.OptionsColumn.AllowEdit = false;
            this.colStoredProcSet.OptionsColumn.AllowFocus = false;
            this.colStoredProcSet.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colStoredProcSet.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colStoredProcSet.OptionsColumn.ReadOnly = true;
            this.colStoredProcSet.OptionsFilter.AllowAutoFilter = false;
            this.colStoredProcSet.OptionsFilter.AllowFilter = false;
            this.colStoredProcSet.Width = 278;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colRecordOrder.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.OptionsFilter.AllowAutoFilter = false;
            this.colRecordOrder.OptionsFilter.AllowFilter = false;
            this.colRecordOrder.Width = 57;
            // 
            // groupControl1
            // 
            this.groupControl1.AllowHtmlText = true;
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.progressBarControlExport);
            this.groupControl1.Controls.Add(this.btnExport);
            this.groupControl1.Controls.Add(this.gridControl1);
            this.groupControl1.Location = new System.Drawing.Point(12, 81);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(607, 380);
            this.groupControl1.TabIndex = 6;
            this.groupControl1.Text = "Outstanding Changes to <b>Send</b> to Central System";
            // 
            // progressBarControlExport
            // 
            this.progressBarControlExport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarControlExport.Location = new System.Drawing.Point(190, 356);
            this.progressBarControlExport.MenuManager = this.barManager1;
            this.progressBarControlExport.Name = "progressBarControlExport";
            this.progressBarControlExport.Properties.ShowTitle = true;
            this.progressBarControlExport.Size = new System.Drawing.Size(412, 18);
            this.progressBarControlExport.TabIndex = 7;
            this.progressBarControlExport.Visible = false;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExport.Location = new System.Drawing.Point(3, 354);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(181, 23);
            this.btnExport.TabIndex = 6;
            this.btnExport.Text = "Send Changes to Central System";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.EditValue = ((object)(resources.GetObject("pictureEdit2.EditValue")));
            this.pictureEdit2.Location = new System.Drawing.Point(8, 10);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ReadOnly = true;
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Size = new System.Drawing.Size(30, 30);
            this.pictureEdit2.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Location = new System.Drawing.Point(44, 14);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(435, 19);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Welcome to the <b>Amenity ARB</b> Data Synchronisation process";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl2.Location = new System.Drawing.Point(44, 40);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(575, 13);
            this.labelControl2.TabIndex = 9;
            this.labelControl2.Text = "You must send any outstanding changes to the central system before you can import" +
    " any data from the central system.";
            // 
            // groupControl2
            // 
            this.groupControl2.AllowHtmlText = true;
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.TimeoutLabel);
            this.groupControl2.Controls.Add(this.labelLastImportDate);
            this.groupControl2.Controls.Add(this.progressBarControlImport);
            this.groupControl2.Controls.Add(this.btnImport);
            this.groupControl2.Location = new System.Drawing.Point(12, 485);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(607, 71);
            this.groupControl2.TabIndex = 10;
            this.groupControl2.Text = "Import Latest Changes <b>From</b> Central System";
            // 
            // TimeoutLabel
            // 
            this.TimeoutLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TimeoutLabel.Appearance.ForeColor = System.Drawing.Color.Red;
            this.TimeoutLabel.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("TimeoutLabel.Appearance.Image")));
            this.TimeoutLabel.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TimeoutLabel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TimeoutLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.TimeoutLabel.Location = new System.Drawing.Point(325, 24);
            this.TimeoutLabel.Name = "TimeoutLabel";
            this.TimeoutLabel.Size = new System.Drawing.Size(277, 17);
            this.TimeoutLabel.TabIndex = 12;
            this.TimeoutLabel.Text = "        You cannot Import again until: ";
            this.TimeoutLabel.Visible = false;
            // 
            // labelLastImportDate
            // 
            this.labelLastImportDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLastImportDate.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelLastImportDate.Appearance.Image")));
            this.labelLastImportDate.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelLastImportDate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelLastImportDate.Location = new System.Drawing.Point(5, 24);
            this.labelLastImportDate.Name = "labelLastImportDate";
            this.labelLastImportDate.Size = new System.Drawing.Size(314, 17);
            this.labelLastImportDate.TabIndex = 9;
            this.labelLastImportDate.Text = "        Last Successfull Import:";
            // 
            // progressBarControlImport
            // 
            this.progressBarControlImport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarControlImport.Location = new System.Drawing.Point(190, 47);
            this.progressBarControlImport.MenuManager = this.barManager1;
            this.progressBarControlImport.Name = "progressBarControlImport";
            this.progressBarControlImport.Properties.ShowTitle = true;
            this.progressBarControlImport.Size = new System.Drawing.Size(412, 18);
            this.progressBarControlImport.TabIndex = 8;
            this.progressBarControlImport.Visible = false;
            // 
            // btnImport
            // 
            this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnImport.Location = new System.Drawing.Point(3, 44);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(181, 23);
            this.btnImport.TabIndex = 7;
            this.btnImport.Text = "Get Changes from Central System";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(544, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // dataSet_AT_DataTransfer
            // 
            this.dataSet_AT_DataTransfer.DataSetName = "DataSet_AT_DataTransfer";
            this.dataSet_AT_DataTransfer.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp02102ATWebServiceTabletGetExportOverviewBindingSource
            // 
            this.sp02102ATWebServiceTabletGetExportOverviewBindingSource.DataMember = "sp02102_AT_WebService_Tablet_Get_Export_Overview";
            this.sp02102ATWebServiceTabletGetExportOverviewBindingSource.DataSource = this.dataSet_AT_DataTransfer;
            // 
            // sp02102_AT_WebService_Tablet_Get_Export_OverviewTableAdapter
            // 
            this.sp02102_AT_WebService_Tablet_Get_Export_OverviewTableAdapter.ClearBeforeFill = true;
            // 
            // frm_AT_Data_Sync
            // 
            this.ClientSize = new System.Drawing.Size(629, 568);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.pictureEdit2);
            this.Controls.Add(this.groupControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_AT_Data_Sync";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Amenity ARB - Data Synchronisation";
            this.Activated += new System.EventHandler(this.frm_AT_Data_Sync_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Data_Sync_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Data_Sync_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.pictureEdit2, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControlExport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControlImport.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataTransfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp02102ATWebServiceTabletGetExportOverviewBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnExport;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControlExport;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnImport;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControlImport;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyName;
        private DevExpress.XtraGrid.Columns.GridColumn colInsertedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colUpdatedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colDeletedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colStoredProcGet;
        private DevExpress.XtraGrid.Columns.GridColumn colStoredProcSet;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraEditors.LabelControl labelLastImportDate;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.LabelControl TimeoutLabel;
        private System.Windows.Forms.BindingSource sp02102ATWebServiceTabletGetExportOverviewBindingSource;
        private DataSet_AT_DataTransfer dataSet_AT_DataTransfer;
        private DataSet_AT_DataTransferTableAdapters.sp02102_AT_WebService_Tablet_Get_Export_OverviewTableAdapter sp02102_AT_WebService_Tablet_Get_Export_OverviewTableAdapter;
    }
}

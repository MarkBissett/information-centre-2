using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // for StreamReader //
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors.DXErrorProvider;  // For Validation Rules //

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Styles_Save : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int intStyleSetID = 0;
        public int intCreatedByID = 0;
        public string strStyleSetDescription = "";
        public string strStyleSetRemarks = "";
        
        #endregion
        
        public frm_AT_Mapping_Styles_Save()
        {
            InitializeComponent();
            InitValidationRules();
        }

        private void InitValidationRules()
        {
            // Create Rule // 
            ConditionValidationRule notEmptyValidationRule = new ConditionValidationRule();
            notEmptyValidationRule.ConditionOperator = ConditionOperator.IsNotBlank;
            notEmptyValidationRule.ErrorText = "Please enter a value [100 characters maximum]";

            // Link Rule to control //
            dxValidationProvider1.SetValidationRule(textEdit1, notEmptyValidationRule);
        }

        private void frm_AT_Mapping_Styles_Save_Load(object sender, EventArgs e)
        {
            this.FormID = 20048;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            textEdit1.Text = "";

            dxValidationProvider1.ValidationMode = ValidationMode.Auto;
            dxValidationProvider1.Validate();

            //InitValidationRules();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            strStyleSetDescription = textEdit1.Text;
            if (strStyleSetDescription.Trim() == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Thematic Style Set Description has been entered!\n\nPlease enter a description before proceeding.", "Save Thematic Style Set", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        
            // Check the Descriptor is Unique //
            int intMatchingDescriptors = 0;
            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter GetCount = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
            GetCount.ChangeConnectionString(strConnectionString);
            intMatchingDescriptors = Convert.ToInt32(GetCount.sp01258_AT_Tree_Picker_Style_Set_Save_Check_Unique(strStyleSetDescription));
            if (intMatchingDescriptors > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Thematic Style Set Description entered [" + strStyleSetDescription + "] has already been linked to another thematic style set!\n\nPlease enter a different description before proceeding.", "Save Thematic Style Set", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to save the current thematic style set?", "Save Thematic Style Set", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
           
            intStyleSetID = 0;
            intCreatedByID = this.GlobalSettings.UserID;
            strStyleSetRemarks = memoEdit1.Text;

            if (this.ValidateChildren() == true)
            {
                this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails validation test earlier in this process //
                this.Close();
            }
        }


    }
}


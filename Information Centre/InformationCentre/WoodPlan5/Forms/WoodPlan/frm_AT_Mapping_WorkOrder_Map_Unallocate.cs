using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_WorkOrder_Map_Unallocate : BaseObjects.frmBase
    {
        #region Instance Variables

        public string strMapDescription;
        public string strMapRemarks;
        public int intWorkOrderID = 0;

        #endregion
        
        public frm_AT_Mapping_WorkOrder_Map_Unallocate()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_WorkOrder_Map_Unallocate_Load(object sender, EventArgs e)
        {
            this.Text += "  [Work Order: " + intWorkOrderID.ToString().PadLeft(7, '0') + "]  [Map: " + (string.IsNullOrEmpty(strMapDescription) ? "No Description" : strMapDescription) + "]";
            teMapDescription.EditValue = strMapDescription;
            meMapRemarks.EditValue = strMapRemarks;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            strMapDescription = teMapDescription.EditValue.ToString();
            strMapRemarks = meMapRemarks.EditValue.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}


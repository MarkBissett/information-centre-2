using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors.DXErrorProvider;  // For Validation Rules //

namespace WoodPlan5
{
    public partial class frm_AT_Data_Transfer_GBM_Mobile_Template_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        /// <summary>
        /// 
        /// </summary>
        public bool boolAllowEdit = false;
        /// <summary>
        /// 
        /// </summary>
        public string strCaller = "";
        /// <summary>
        /// 
        /// </summary>
        public string strRecordIDs = "";
        /// <summary>
        /// 
        /// </summary>
        public int intRecordCount = 0;

        #endregion

         public frm_AT_Data_Transfer_GBM_Mobile_Template_Edit()
        {
            InitializeComponent();
        }

        private void InitValidationRules()
        {
            // Create Rule // 
            ConditionValidationRule notEmptyValidationRule = new ConditionValidationRule();
            notEmptyValidationRule.ConditionOperator = ConditionOperator.IsNotBlank;
            notEmptyValidationRule.ErrorText = "Please enter a value [100 characters maximum]";

            // Link Rule to control //
            dxValidationProvider1.SetValidationRule(TemplateDescriptionTextEdit, notEmptyValidationRule);
        }

        private void frm_AT_Data_Transfer_GBM_Mobile_Template_Edit_Load(object sender, EventArgs e)
        {

            this.fProgress = new frmProgress(20);  // Show Progress Window //
            //this.AddOwnedForm(fProgress);
            fProgress.UpdateCaption("Loading Data, Please Wait...");
            fProgress.Show();
            Application.DoEvents();
            
            this.LockThisWindow();

            strConnectionString = this.GlobalSettings.ConnectionString;
            
            dxValidationProvider1.ValidationMode = ValidationMode.Auto;
            dxValidationProvider1.Validate();
            this.fProgress = new frmProgress(30);  // Update Progress Bar //

            dataLayoutControl1.BeginUpdate();
            sp00113_export_to_GBM_saved_template_editTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00113_export_to_GBM_saved_template_editTableAdapter.Fill(this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit, strRecordIDs);
            dataLayoutControl1.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(40); // Update Progress Bar //
            if (strFormMode != "blockedit") InitValidationRules();

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100); // Set Progress Bar Complete (100%)//
                fProgress.Close();
                fProgress = null;
                Application.DoEvents();
            }

            this.UnlockThisWindow();
        }

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.CancelEdit)
            {
                e.Handled = true;
                this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows.Clear();  // Abort any changes //
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
            else if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.EndEdit)
            {
                e.Handled = true;
                if (SaveChanges(false) == 1)
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Commit any outstanding changes to underlying dataset //

            for (int i = 0; i < this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows.Count; i++)
            {
                switch (this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (strFormMode == "blockedit")
                {
                    strMessage = "You are block editing " + Convert.ToString(intRecordCount) + " records on the current screen but you have not saved the block edit changes!\n";
                }
                else
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
            }
            return strMessage;
        }

         public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

         private int SaveChanges(Boolean ib_SuccessMessage)
        {
            // Checks passed successfully so OK to proceed with save //
            this.fProgress = new frmProgress(10);  // Show Progress Window //
            //this.AddOwnedForm(fProgress);
            fProgress.UpdateCaption("Checking Data, Please Wait...");
            fProgress.Show();
            Application.DoEvents();
            
            // Paramters - ib_SuccessMessage - determins if the success message box should be shown at the end of the event //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            // Check to ensure all required fields have values //
            int intErrorFound = 0;
            string strErrorMessage = "";

            DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter GetCount = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            GetCount.ChangeConnectionString(strConnectionString);
            int intMatchingDescriptors = 0;
            for (int i = this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows.Count - 1; i >= 0; i--)
            {
                if (this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i].RowState == DataRowState.Added || this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i].RowState == DataRowState.Modified)
                {
                    if (strFormMode != "blockedit")
                    {
                        if (this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateDescription"] == DBNull.Value)
                        {
                            intErrorFound = 1;
                            strErrorMessage += "Missing Template Description\n";
                        }


                        intMatchingDescriptors = Convert.ToInt32(GetCount.sp00114_export_to_GBM_save_template_edit_check_descriptor_unique(Convert.ToString(this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateDescription"]), Convert.ToInt32(this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateID"])));
                        if (intMatchingDescriptors > 0)
                        {
                            intErrorFound = 2;
                            strErrorMessage += "Duplicate Template Description - " + this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateDescription"].ToString() + "\n";
                        }
                    }
                    if (intErrorFound > 0)
                    {
                        break;
                    }
                }
            }
            if (intErrorFound > 0)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("The following errors are present...\n\n" + strErrorMessage + "\nPlease fix then try re-saving.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }

            // Checks passed successfully so OK to proceed with save //
            if (fProgress != null)
            {
                fProgress.UpdateCaption("Saving Changes, Please Wait...");
                fProgress.UpdateProgress(40); // Update Progress Bar //
            }

            Boolean boolSaveSuccessfull = true;
            int intSaveFailCount = 0;

            int? intTemplateID = null;
            string strTemplateDescription = null;
            string strTemplateRemarks = null;

            DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter SaveChanges = new DataSet_AT_DataTransferTableAdapters.QueriesTableAdapter();
            SaveChanges.ChangeConnectionString(strConnectionString);

            for (int i = this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows.Count - 1; i >= 0; i--)
            {
                switch (this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i].RowState)
                {
                    case DataRowState.Added:  // Handles Add and Block Edit via the strFormMode passed in the parameter string to the SP //
                        boolSaveSuccessfull = true;

                        if (this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateDescription"] == DBNull.Value) strTemplateDescription = null;
                        else strTemplateDescription = Convert.ToString(this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateDescription"]);

                        if (this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateRemarks"] == DBNull.Value) strTemplateRemarks = null;
                        else strTemplateRemarks = Convert.ToString(this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateRemarks"]);

                        try
                        {
                            intTemplateID = Convert.ToInt32(SaveChanges.sp00115_export_to_GBM_save_template_save_changes(strFormMode, 0, strRecordIDs, strTemplateDescription, strTemplateRemarks));
                        }
                        catch (Exception Ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            boolSaveSuccessfull = false;
                            intSaveFailCount++;
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while saving. Please try again.\n\nException:" + Ex.Message, "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return 0;
                        }
                        if (boolSaveSuccessfull && strFormMode == "add")  // Update Data on front end with new ID [adding only , not block editing] //
                        {
                            this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["LayoutID"] = intTemplateID;
                            strFormMode = "edit";  // Switch form mode so that record is not added on any subsequent saves //
                        }
                        break;

                    case DataRowState.Modified:
                        boolSaveSuccessfull = true;
                        if (this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateID"] == DBNull.Value) intTemplateID = null;
                        else intTemplateID = Convert.ToInt32(this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateID"]);

                        if (this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateDescription"] == DBNull.Value) strTemplateDescription = null;
                        else strTemplateDescription = Convert.ToString(this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateDescription"]);

                        if (this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateRemarks"] == DBNull.Value) strTemplateRemarks = null;
                        else strTemplateRemarks = Convert.ToString(this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i]["TemplateRemarks"]);

                        try
                        {
                            SaveChanges.sp00115_export_to_GBM_save_template_save_changes(strFormMode, intTemplateID, "", strTemplateDescription, strTemplateRemarks);
                        }
                        catch (Exception Ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            boolSaveSuccessfull = false;
                            intSaveFailCount++;
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while saving. Please try again.\n\nException:" + Ex.Message, "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return 0;
                        }
                        break;
                }
                if (boolSaveSuccessfull)
                {
                    this.dataSet_AT_DataTransfer.sp00113_export_to_GBM_saved_template_edit.Rows[i].AcceptChanges();  // Set row status to Unchanged so that update is not fired again unless further changes are made //
                }
            }
            if (!boolSaveSuccessfull) // Error Occurred //
            {
                return 0;
            }
            if (fProgress != null)
            {
                fProgress.SetProgressValue(100); // Set Progress Bar Complete (100%)//
                fProgress.Close();
                fProgress = null;
            }

            return 1;
        }

 
    }
}


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;  // Mouse down Args for GridView Mouse Down //
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Repository;  // Blank Editor //
using DevExpress.XtraLayout;
using WoodPlan5.Properties;
using BaseObjects;
using System.Data.SqlClient;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_Workspace_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool isRunning = false;
        private GridHitInfo downHitInfo = null;

        public int intWorkSpaceID = 0;
        public string strShareWithGroupIDs = "";

        RepositoryItem emptyEditor;  // Used to conditionally hide the checkbox editor //

        BaseObjects.GridCheckMarksSelection selection1;
        AutoScrollHelper autoScrollHelper;
        #endregion

        public frm_AT_Mapping_Workspace_Edit()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_Workspace_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 20042;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked at end of event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            this.sp01254_AT_Tree_Picker_scale_listTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01254_AT_Tree_Picker_scale_listTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01254_AT_Tree_Picker_scale_list);
            gridControl4.ForceInitialize();

            this.sp01266_AT_Tree_Picker_Workspace_Share_groupsTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01266_AT_Tree_Picker_Workspace_Share_groupsTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01266_AT_Tree_Picker_Workspace_Share_groups, this.GlobalSettings.UserID, this.GlobalSettings.PersonType);
            gridControl2.ForceInitialize();

            this.sp01267_AT_Tree_Picker_Workspace_layers_listTableAdapter.Connection.ConnectionString = strConnectionString;

            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit, intWorkSpaceID);

            // Add record selection checkboxes to grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            ConfigureFormAccordingToMode();

            autoScrollHelper = new AutoScrollHelper(gridView1);  // Add Autoscroll functionality when dragging to adjust record order //
            gridControl1.AllowDrop = true;

            emptyEditor = new RepositoryItem();
            gridControl1.RepositoryItems.Add(emptyEditor);

            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }

        private void ConfigureFormAccordingToMode()
        {
            dataLayoutControl1.BeginUpdate();
            this.sp01267_AT_Tree_Picker_Workspace_layers_listTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01267_AT_Tree_Picker_Workspace_layers_list, intWorkSpaceID);
            switch (strFormMode)
            {
                case "add":
                    this.Text = "Edit Workspace - Add New Workspace";
                    WorkspaceNameTextEdit.Focus();
                    break;
                case "edit":
                    this.Text = "Edit Workspace";
                    WorkspaceRemarksMemoEdit.Focus();

                    if (Convert.ToInt32(radioGroup1.EditValue) == 2)  // Check if the list of groups to be shared with needs to be updated with the current selection //
                    {
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SqlCommand cmd = null;
                        cmd = new SqlCommand("sp01287_AT_Tree_Picker_Workspace_Get_Share_Groups", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@WorkspaceID", intWorkSpaceID));
                        SqlDataAdapter sdaDataAdpter = new SqlDataAdapter();
                        DataSet dsDataSet = new DataSet("NewDataSet");
                        sdaDataAdpter = new SqlDataAdapter(cmd);
                        sdaDataAdpter.Fill(dsDataSet, "Table");

                        GridView view = (GridView)gridControl2.MainView;
                        int intShareGroupID = 0;
                        int intFoundRow = 0;
                        foreach (DataRow dr in dsDataSet.Tables[0].Rows)
                        {
                            intShareGroupID = Convert.ToInt32(dr["GroupID"]);
                            intFoundRow = view.LocateByValue(0, view.Columns["GroupID"], intShareGroupID);
                            if (intFoundRow != GridControl.InvalidRowHandle) view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        }
                    }
                    break;
                default:
                    break;
            }
            InitValidationRules(dxValidationProvider1);
            dxValidationProvider1.Validate();
            dataLayoutControl1.EndUpdate();
        }

        private void InitValidationRules(DXValidationProvider dxValidationProvider)
        {
            // ***** IMPORTANT NOTE: Forms AutoValidate property set to EnableAllowFocusChange ***** //
            // Create Rule // 
            ConditionValidationRule notEmptyValidationRule = new ConditionValidationRule();
            notEmptyValidationRule.ConditionOperator = ConditionOperator.IsNotBlank;
            notEmptyValidationRule.ErrorText = "Please enter a value [100 characters maximum]";

            // Link Rule to controls //
            if (strFormMode != "blockadd" && strFormMode != "blockedit")
            {
                dxValidationProvider.SetValidationRule(WorkspaceNameTextEdit, notEmptyValidationRule);
            }
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioGroup rg = (RadioGroup)sender;
            gridControl2.Enabled = (Convert.ToInt32(rg.EditValue) == 2 ? true : false);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // Error Checking Prior to Saving - See if Group Sharing is on [make sure at least one group selected] //
            strShareWithGroupIDs = "";
            int intShareType = Convert.ToInt32(radioGroup1.EditValue);
            if (intShareType == 2)
            {
                GridView view2 = (GridView)gridControl2.MainView;
                for (int i = 0; i < view2.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view2.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        strShareWithGroupIDs += Convert.ToString(view2.GetRowCellValue(i, "GroupID")) + ",";
                    }
                }
                if (strShareWithGroupIDs == "")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("You have set the Sharing option to 'Only Staff in the Following Groups...', but you have not chosen any groups to share with!\n\nTip: To specify groups, tick the tick box next to their name.\n\nIf you cannot see any groups in the list then you do not have access to any groups. Contact your system administrator for advice.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            // Ensure the Workspace Name is present //
            string strDescription = WorkspaceNameTextEdit.EditValue.ToString();
            if (string.IsNullOrEmpty(strDescription.Trim()))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Workspace Name has been entered!\n\nPlease enter a name before proceeding.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Ensure the Workspace Name is Unique //
            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter CheckUnique = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
            CheckUnique.ChangeConnectionString(strConnectionString);
            int intMatches = 0;
            try
            {
                intMatches = Convert.ToInt32(CheckUnique.sp01275_AT_Tree_Picker_Workspace_Check_Name_Unique(intWorkSpaceID, strDescription));
                if (intMatches > 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("The workspace name entered [" + strDescription + "]  has already been asigned to a different Workspace!\n\nPlease enter a different name before proceeding.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            catch
            {
                XtraMessageBox.Show("Unable to save workspace changes - an error occurred while checking the Name was unique!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Ensure at least one map layer (+ Map Objects and Map Object Labels + Temp Map Objects) : 4 or more layers are present //
            GridView view1 = (GridView)gridControl1.MainView;
            if (view1.DataRowCount < 4)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Mapping Vector or Raster Layers have been selected!\n\nAdd at least one Mapping Raster\\Vector Layer before proceeding.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // All Checks Passed - Save any pending changes to layers(record order or layer name changes) first //
            string strReturn = SavePendingChanges();
            if (strReturn != "")
            {
                XtraMessageBox.Show("An error occurred while saving the Layer changes [" + strReturn + "]!\n\nTry clicking OK again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Save Changes to Workspace //
            this.sp01274ATTreePickerWorkspaceEditBindingSource.EndEdit();
            try
            {
                this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Update(dataSet_AT_TreePicker);
            }
            catch
            {
                XtraMessageBox.Show("Unable to save workspace changes - an error occurred while saving!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Update Any Sharing if Required //
            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter UpdateSharing = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
            UpdateSharing.ChangeConnectionString(strConnectionString);
            try
            {
                UpdateSharing.sp01283_AT_Tree_Picker_Workspace_Update_Sharing(intWorkSpaceID, intShareType, strShareWithGroupIDs);
            }
            catch
            {
                XtraMessageBox.Show("Unable to save workspace changes - an error occurred while updating the sharing settings!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails any validation tests //
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


        #region PopupContainer2 Scale

        private void DefaultScalePopupContainerEdit_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerScale_Get_Selected();
        }

        private string PopupContainerScale_Get_Selected()
        {
            string strCurrentScale = "";
            int intDefaultScale = 0;
            if (Convert.ToInt32(seUserDefinedScale.Value) > 0)
            {
                intDefaultScale = Convert.ToInt32(seUserDefinedScale.Value);
                strCurrentScale = intDefaultScale.ToString();
            }
            else
            {
                GridView view = (GridView)gridControl4.MainView;
                if (view.DataRowCount <= 0)
                {
                    XtraMessageBox.Show("Select or enter a scale before proceeding.", "Select Scale", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return strCurrentScale;
                }
                else
                {
                    intDefaultScale = Convert.ToInt32(view.GetFocusedRowCellValue("Scale"));
                    strCurrentScale = intDefaultScale.ToString();
                }
            }
            return strCurrentScale;
        }

        private void btnSetScale_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                {
                    seUserDefinedScale.Value = 0;  // Clear User Defined Value since grid selection used //
                }
            }
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Layers Available";
                    break;
                case "gridView2":
                    message = "No Groups Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView4":
                    seUserDefinedScale.Value = 0;  // Clear User Defined Value since grid selection used //
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridControl 1

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LayerHitable":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LayerType")) == 0) e.RepositoryItem = emptyEditor;  // Don't Show Hitable checkbox for Raster layers //
                    break;
                default:
                    break;
            }
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            // Disable Show Group By Box Menu Item //
            if (e.Menu == null) return;
            for (int i = e.Menu.Items.Count - 1; i >= 0; i--)
            {
                if (e.Menu.Items[i].Caption == "Show Group By Box")
                {
                    e.Menu.Items[i].Enabled = false;
                    break;
                }
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent editing of LayerName if value = "Map Object Labels" or "Map Objects" //
            GridView view = (GridView)sender;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            try
            {
                if ((view.FocusedColumn.FieldName == "LayerName") && (view.GetFocusedRowCellValue("LayerName").ToString() == "Map Object Labels" || view.GetFocusedRowCellValue("LayerName").ToString() == "Map Objects")) e.Cancel = true;
            }
            catch (Exception)
            {
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Append:
                    AddRecord();
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    DeleteRecord();
                    e.Handled = true;
                    break;
            }
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiLayerManagerProperties.Enabled = true;
                bbiAddLayer.Enabled = true;
                if (view.RowCount <= 0)
                {
                    bbiDeleteLayer.Enabled = false;
                    bbiLayerManagerProperties.Enabled = false;
                }
                else if (view.GetFocusedRowCellValue("LayerName").ToString() == "Map Objects" || view.GetFocusedRowCellValue("LayerName").ToString() == "Map Object Labels")
                {
                    bbiDeleteLayer.Enabled = false;
                }
                else
                {
                    bbiDeleteLayer.Enabled = true;
                }
                popupMenu_LayerManager.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #region Drag Drop Functionality

        // AutoScrollHelper autoScrollHelper declared under instance vars //
        // autoScrollHelper linked to grid in Form Load Event //
        // gridControl1.AllowDrop = true in form Load Event //

        int dropTargetRowHandle = -1;
        int DropTargetRowHandle
        {
            get
            {
                return dropTargetRowHandle;
            }
            set
            {
                dropTargetRowHandle = value;
                gridControl1.Invalidate();
            }
        }
        const string OrderFieldName = "LayerOrder";

        private void gridControl1_DragOver(object sender, DragEventArgs e)
        {
            GridControl grid = (GridControl)sender;
            Point pt = new Point(e.X, e.Y);
            pt = grid.PointToClient(pt);
            GridView view = grid.GetViewAt(pt) as GridView;
            if (view == null) return;

            if (view.FocusedRowHandle == 0 || view.FocusedRowHandle == 1 || view.FocusedRowHandle == 2)  // Don't allow Map Objects or Map Objects Labels or Temporary Map Objects [may be -1 if at end of grid] //
            {
                DropTargetRowHandle = -1;
                return;
            }
            
            GridHitInfo hitInfo = view.CalcHitInfo(pt);
            if (hitInfo.HitTest == GridHitTest.EmptyRow)
            {
                DropTargetRowHandle = view.DataRowCount;
            }
            else
            {
                DropTargetRowHandle = hitInfo.RowHandle;
            }

            e.Effect = DragDropEffects.None;

            GridHitInfo downHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            if (downHitInfo != null)
            {
                //if (hitInfo.RowHandle != downHitInfo.RowHandle)
                if (hitInfo.RowHandle != downHitInfo.RowHandle && (hitInfo.RowHandle < 0 || hitInfo.RowHandle > 2))  // Everything after && Stops row 1, 2 and 3 being moved //
                    e.Effect = DragDropEffects.Move;
            }

            autoScrollHelper.ScrollIfNeeded(hitInfo);
        }

        private void gridControl1_DragDrop(object sender, DragEventArgs e)
        {
            GridControl grid = sender as GridControl;
            GridView view = grid.MainView as GridView;
            GridHitInfo srcHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            GridHitInfo hitInfo = view.CalcHitInfo(grid.PointToClient(new Point(e.X, e.Y)));
            int sourceRow = srcHitInfo.RowHandle;
            int targetRow = (hitInfo.RowHandle == GridControl.InvalidRowHandle ? -1 : hitInfo.RowHandle);
          
            if (DropTargetRowHandle < 0) return;  // No Valid Place for Dropping //

            DropTargetRowHandle = -1;
            MoveRow(sourceRow, targetRow, view);
        }

        private void gridControl1_DragLeave(object sender, EventArgs e)
        {
            DropTargetRowHandle = -1;
        }

        private void gridControl1_Paint(object sender, PaintEventArgs e)
        {
            if (DropTargetRowHandle < 0) return;
            GridControl grid = (GridControl)sender;
            GridView view = (GridView)grid.MainView;

            bool isBottomLine = DropTargetRowHandle == view.DataRowCount;

            GridViewInfo viewInfo = view.GetViewInfo() as GridViewInfo;
            GridRowInfo rowInfo = viewInfo.GetGridRowInfo(isBottomLine ? DropTargetRowHandle - 1 : DropTargetRowHandle);

            if (rowInfo == null) return;

            Point p1, p2;
            if (isBottomLine)
            {
                p1 = new Point(rowInfo.Bounds.Left, rowInfo.Bounds.Bottom - 1);
                p2 = new Point(rowInfo.Bounds.Right, rowInfo.Bounds.Bottom - 1);
            }
            else
            {
                p1 = new Point(rowInfo.Bounds.Left, rowInfo.Bounds.Top - 1);
                p2 = new Point(rowInfo.Bounds.Right, rowInfo.Bounds.Top - 1);
            }
            e.Graphics.DrawLine(Pens.Blue, p1, p2);
        }

        private void gridView1_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Button == MouseButtons.Left && downHitInfo != null)
            {
                Size dragSize = SystemInformation.DragSize;
                Rectangle dragRect = new Rectangle(new Point(downHitInfo.HitPoint.X - dragSize.Width / 2, downHitInfo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new Point(e.X, e.Y)))
                {
                    view.GridControl.DoDragDrop(downHitInfo, DragDropEffects.All);
                    downHitInfo = null;
                }
            }
        }

        private void MoveRow(int sourceRow, int targetRow, GridView view)
        {
            /*DataRow row1;
            DataRow row2;
            if (targetRow <= 0) // 0: start of Grid, -1: end of grid //
            {
                row1 = (targetRow == 0 ? view.GetDataRow(targetRow) : view.GetDataRow(view.DataRowCount-1));
                row2 = (targetRow == 0 ? view.GetDataRow(targetRow + 1) : view.GetDataRow(view.DataRowCount-1));
            }
            else
            {
                row1 = view.GetDataRow(targetRow - 1);
                row2 = view.GetDataRow(targetRow);
            }

            DataRow dragRow = view.GetDataRow(sourceRow);
            decimal val1 = (decimal)row1[OrderFieldName];

            if (row2 == null || targetRow == -1)
                dragRow[OrderFieldName] = val1 + 1;
            else
            {
                if (targetRow == 0)
                {
                    dragRow[OrderFieldName] = val1 / 2;
                }
                else
                {
                    decimal val2 = (decimal)row2[OrderFieldName];
                    dragRow[OrderFieldName] = (val1 + val2) / 2;
                }
            }*/

            view.BeginDataUpdate();
            view.BeginSort();

            if (sourceRow > targetRow && targetRow != -1)
            {
                for (int i = targetRow; i < sourceRow; i++)
                {
                    view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) + 1);
                }
                DataRow dragRow = view.GetDataRow(sourceRow);
                dragRow[OrderFieldName] = targetRow;
            }
            else
            {
                if (targetRow > sourceRow && targetRow != -1) // Not trying to position on end of grid //
                {
                    for (int i = targetRow - 1; i > sourceRow; i--)
                    {
                        view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) - 1);
                    }
                    DataRow dragRow = view.GetDataRow(sourceRow);
                    dragRow[OrderFieldName] = targetRow - 1;
                    //dragRow[OrderFieldName] = targetRow;
                }
                else // Positioning on end of grid //
                {
                    targetRow = view.DataRowCount - 1;
                    for (int i = targetRow; i > sourceRow; i--)
                    {
                        view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) - 1);
                    }
                    DataRow dragRow = view.GetDataRow(sourceRow);
                    dragRow[OrderFieldName] = targetRow;
                }
            }
            view.EndSort();
            view.EndDataUpdate();

        }

        #endregion

        #endregion


        private string SavePendingChanges()
        {
            // Save any pending changes (record order or layer name changes) first //
            GridView view = (GridView)gridControl1.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow()) return "Column Error on Current Row in Layer Grid";
            
            // Sort out Layer Ordering //
            for (int i = 0; i < view.DataRowCount; i++)
            {
                view.SetRowCellValue(i, "LayerOrder", i);
            }

            this.sp01267ATTreePickerWorkspacelayerslistBindingSource.EndEdit();
            try
            {
                this.sp01267_AT_Tree_Picker_Workspace_layers_listTableAdapter.Update(dataSet_AT_TreePicker);  // Update, Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                XtraMessageBox.Show("An error occurred while saving the Layer changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return ex.Message;
            }
            return "";  // No problems //
        }

        private void bbiLayerManagerProperties_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the layer to view the properties for before proceeding.", "View Layer Properties", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strLayerName = view.GetFocusedRowCellValue("LayerName").ToString(); 
            switch (strLayerName)
            {
                case "Map Objects":
                    frm_AT_Mapping_Workspace_Layer_MapObject_Properties fChildForm = new frm_AT_Mapping_Workspace_Layer_MapObject_Properties();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;

                    fChildForm.intDefaultThematicStyleSet = Convert.ToInt32(view.GetFocusedRowCellValue("ThematicStyleSet"));
                    fChildForm.intUseThematicStyling = Convert.ToInt32(view.GetFocusedRowCellValue("UseThematicStyling"));
                    fChildForm.strPointSize = view.GetFocusedRowCellValue("PointSize").ToString();

                    if (fChildForm.ShowDialog() == DialogResult.OK)
                    {
                        // OK clicked so update current row //
                        view.SetFocusedRowCellValue("ThematicStyleSet", fChildForm.intDefaultThematicStyleSet);
                        view.SetFocusedRowCellValue("UseThematicStyling", fChildForm.intUseThematicStyling);
                        view.SetFocusedRowCellValue("PointSize", fChildForm.strPointSize);
                    }
                    break;
                case "Map Object Labels":
                    frm_AT_Mapping_Layer_Label_Properties fChildForm2 = new frm_AT_Mapping_Layer_Label_Properties();
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intParentLayerID = Convert.ToInt32(view.GetFocusedRowCellValue("LayerID"));
                    fChildForm2.intTransparency = Convert.ToInt32(view.GetFocusedRowCellValue("Transparency"));
                    fChildForm2.intLineColour = Convert.ToInt32(view.GetFocusedRowCellValue("LineColour"));
                    fChildForm2.intLineWidth = Convert.ToInt32(view.GetFocusedRowCellValue("LineWidth"));
                    if (fChildForm2.ShowDialog() == DialogResult.OK)
                    {
                        // OK clicked so update current row //
                        view.SetFocusedRowCellValue("Transparency", fChildForm2.intTransparency);
                        view.SetFocusedRowCellValue("LineColour", fChildForm2.intLineColour);
                        view.SetFocusedRowCellValue("LineWidth", fChildForm2.intLineWidth);
                    }
                    break;
                default:  // Map backgrounds (type = 0: Raster or 1: Vector //
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LayerType")) == 0)  // Raster //
                    {
                        frm_AT_Mapping_Properties_Layer_Raster fChildForm3 = new frm_AT_Mapping_Properties_Layer_Raster();
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.intTransparency = Convert.ToInt32(view.GetFocusedRowCellValue("Transparency"));
                        fChildForm3.intGreyScale = Convert.ToInt32(view.GetFocusedRowCellValue("GreyScale"));

                        if (fChildForm3.ShowDialog() == DialogResult.OK)
                        {
                            // OK clicked so update current row //
                            view.SetFocusedRowCellValue("Transparency", fChildForm3.intTransparency);
                            view.SetFocusedRowCellValue("GreyScale", fChildForm3.intGreyScale);
                        }
                    }
                    else  // Vector //
                    {
                        frm_AT_Mapping_Properties_Layer_Vector fChildForm3 = new frm_AT_Mapping_Properties_Layer_Vector();
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.intPreserveDefaultStyling = Convert.ToInt32(view.GetFocusedRowCellValue("PreserveDefaultStyling"));
                        fChildForm3.intTransparency = Convert.ToInt32(view.GetFocusedRowCellValue("Transparency"));
                        fChildForm3.intLineColour = Convert.ToInt32(view.GetFocusedRowCellValue("LineColour"));
                        fChildForm3.intLineWidth = Convert.ToInt32(view.GetFocusedRowCellValue("LineWidth"));

                        if (fChildForm3.ShowDialog() == DialogResult.OK)
                        {
                            // OK clicked so update current row //
                            view.SetFocusedRowCellValue("PreserveDefaultStyling", fChildForm3.intPreserveDefaultStyling);
                            view.SetFocusedRowCellValue("Transparency", fChildForm3.intTransparency);
                            view.SetFocusedRowCellValue("LineColour", fChildForm3.intLineColour);
                            view.SetFocusedRowCellValue("LineWidth", fChildForm3.intLineWidth);
                        }
                    }
                    break;
            }
        }

        private void bbiAddLayer_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AddRecord();
        }

        private void bbiDeleteLayer_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeleteRecord();
        }

        private void AddRecord()
        {
            // Save any pending changes (record order or layer name changes) first //
            string strReturn = SavePendingChanges();
            if (strReturn != "")
            {
                XtraMessageBox.Show("An error occurred while saving the Layer changes first [" + strReturn + "]!\n\nTry selecting files again - if the problem persists, contact Technical Support.", "Select File(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get Default Mapping Path //
            string strDefaultPath = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                //strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesMapBackgroundFolder").ToString().ToLower();
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, (this.GlobalSettings.SystemDataTransferMode != "Tablet" ? "AmenityTreesMapBackgroundFolder" : "AmenityTreesMapBackgroundFolderTablet")).ToString().ToLower();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Select Mapping File(s) - No Mapping Background Folder Location has been specified in the System Settings table.", "Select Files(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter AddLayer = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
            AddLayer.ChangeConnectionString(strConnectionString);
            int intNewID = 0;

            // Process all selected files //
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.DefaultExt = "tab";
                //dlg.Filter = "TAB Files (*.tab)|*.tab|Shape Files (*.shp)|*.shp";
                dlg.Filter = "TAB Files (*.tab)|*.tab";
                if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                dlg.Multiselect = true;
                dlg.ShowDialog();
                if (dlg.FileNames.Length > 0)
                {
                    string strTempFileName = "";
                    int intFileType = 0;  // 0 = Raster, 1 = Vector //
                    GridView view = (GridView)gridControl1.MainView;
                    int intMaxOrder = view.DataRowCount;  // Don't need to add 1 as DataRowCount is 1 higher than actual value because it doesn't start at 0 //
                    int intAddedCount = 0;
                    foreach (string filename in dlg.FileNames)
                    {
                        if (strDefaultPath != "")
                        {
                            if (!filename.ToLower().StartsWith(strDefaultPath))
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Mapping - Background Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                        }
                        strTempFileName = filename.Substring(strDefaultPath.Length);

                        // Determin if the file is Raster or Vector //
                        try
                        {
                            string strFileText = System.IO.File.ReadAllText(filename);
                            intFileType = (!strFileText.Contains("Type \"RASTER\"") ? 1 : 0);
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to select mapping file: " + filename +".\n\nThe following error occurred while attempting to determin it's type - [" + Ex.Message + "].", "Select Files(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            continue;
                        }

                        // Add Layer to Database //
                        try
                        {
                            intNewID = Convert.ToInt32(AddLayer.sp01284_AT_Tree_Picker_Workspace_Layer_Add(intWorkSpaceID, intFileType, System.IO.Path.GetFileName(strTempFileName), strTempFileName, intMaxOrder + intAddedCount, 1, 0, 0, 0, -16777216, 1, 0));
                            intAddedCount++;
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to select mapping file: " + filename + ".\n\nThe following error occurred while attempting to add it to the workspace - [" + Ex.Message + "].", "Select Files(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
            }
            // Refresh layer grid //
            this.sp01267_AT_Tree_Picker_Workspace_layers_listTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01267_AT_Tree_Picker_Workspace_layers_list, intWorkSpaceID);
        }

        private void DeleteRecord()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                XtraMessageBox.Show("Select the row to be deleted by clicking on it first then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (view.GetFocusedRowCellValue("LayerName").ToString() == "Map Object Labels" || view.GetFocusedRowCellValue("LayerName").ToString() == "Map Objects" || view.GetFocusedRowCellValue("LayerName").ToString() == "Temporary Map Objects")
            {
                XtraMessageBox.Show("You cannot delete the Map Object, Map Object Label or Temporary Map Object layers. Select a different row to be deleted by clicking on it first then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have 1 Map Layer selected for delete!\n\nProceed?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                gridControl1.BeginUpdate();
                view.DeleteRow(view.FocusedRowHandle);  // Delete record from grid //
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    view.SetRowCellValue(i, "LayerOrder", i);
                }

                gridControl1.EndUpdate();
            }
        }


    }
}


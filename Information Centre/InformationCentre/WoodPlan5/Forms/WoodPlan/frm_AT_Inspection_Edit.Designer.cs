namespace WoodPlan5
{
    partial class frm_AT_Inspection_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Inspection_Edit));
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions13 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions14 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject53 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject54 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject55 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject56 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions15 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject57 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject58 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject59 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject60 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions16 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject61 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject62 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject63 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject64 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions17 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject65 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject66 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject67 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject68 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions18 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject69 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject70 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject71 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject72 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions19 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject73 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject74 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject75 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject76 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions20 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject77 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject78 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject79 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject80 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions21 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject81 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject82 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject83 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject84 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions22 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject85 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject86 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject87 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject88 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition7 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions23 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject89 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject90 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject91 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject92 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions24 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject93 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject94 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject95 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject96 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition8 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions25 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject97 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject98 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject99 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject100 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions26 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject101 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject102 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject103 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject104 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition9 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions27 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject105 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject106 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject107 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject108 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions28 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject109 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject110 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject111 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject112 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition10 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions29 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject113 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject114 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject115 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject116 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions30 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject117 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject118 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject119 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject120 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition11 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions31 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject121 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject122 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject123 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject124 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions32 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject125 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject126 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject127 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject128 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition12 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions33 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject129 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject130 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject131 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject132 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions34 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject133 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject134 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject135 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject136 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition13 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions35 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject137 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject138 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject139 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject140 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions36 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject141 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject142 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject143 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject144 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition14 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions37 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject145 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject146 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject147 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject148 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions38 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject149 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject150 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject151 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject152 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition15 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions39 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject153 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject154 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject155 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject156 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions40 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject157 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject158 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject159 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject160 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition16 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions41 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject161 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject162 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject163 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject164 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions42 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject165 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject166 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject167 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject168 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition17 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions43 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject169 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject170 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject171 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject172 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions44 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject173 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject174 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject175 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject176 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition18 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions45 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject177 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject178 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject179 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject180 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions46 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject181 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject182 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject183 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject184 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition19 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions47 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject185 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject186 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject187 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject188 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions48 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject189 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject190 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject191 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject192 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition20 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions49 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject193 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject194 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject195 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject196 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions50 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject197 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject198 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject199 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject200 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition21 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions51 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject201 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject202 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject203 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject204 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions52 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject205 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject206 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject207 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject208 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition22 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions53 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject209 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject210 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject211 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject212 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions54 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject213 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject214 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject215 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject216 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition23 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions55 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject217 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject218 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject219 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject220 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions56 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject221 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject222 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject223 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject224 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition24 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions57 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject225 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject226 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject227 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject228 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions58 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject229 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject230 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject231 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject232 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition25 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn101 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn107 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn113 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn119 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn137 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn125 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn131 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiRememberDetails = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReload = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopyAllValues = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopySelectedValues = new DevExpress.XtraBars.BarButtonItem();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bciShowPrevious = new DevExpress.XtraBars.BarCheckItem();
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.bbiCopyActions = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPasteActions = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.gridControl39 = new DevExpress.XtraGrid.GridControl();
            this.sp02065ATInspectionPicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView39 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn169 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn170 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn171 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn172 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn173 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn174 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn175 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn176 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn177 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn178 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn179 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShortLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView26 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn139 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01380ATActionsLinkedToInspectionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInspectionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalityID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistrictID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeMappingID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionOwnership = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudget = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActionScheduleOfRates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionBudgetedRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActionBudgetedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionWorkOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colActionUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCostDifference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCompletionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionTimeliness = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.IncidentDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.sp01381ATEditInspectionDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.intInspectionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TreeReferenceButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.intTreeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dtInspectDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.strPictureTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateAddedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.GUIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intIncidentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intAngleToVerticalSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.strUser1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.strInspectRefButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.NoFurtherActionRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.intInspectorGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00190ContractorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalCOntractorDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intSafetyGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intGeneralConditionGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intVitalityGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intRootHeaveGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intRootHeave2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intRootHeave3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intStemPhysicalGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intStemPhysical2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intStemPhysical3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intStemDiseaseGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intStemDisease2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intStemDisease3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCrownPhysicalGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCrownPhysical2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCrownPhysical3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView16 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCrownDiseaseGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView17 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCrownDisease2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView18 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn94 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCrownDisease3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView19 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn102 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCrownFoliationGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView20 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn103 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn104 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn105 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn106 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn108 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCrownFoliation2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView21 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn109 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn110 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn111 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn112 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn114 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intCrownFoliation3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView22 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn115 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn116 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn117 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn118 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn120 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList1GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView23 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn133 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn134 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn135 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn136 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn138 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView24 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn121 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn122 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn123 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn124 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn126 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView25 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn127 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn128 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn129 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn130 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn132 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForintTreeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateAdded = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrPicture = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGUID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintIncidentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintInspectionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTreeReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrInspectRef = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintInspector = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtInspectDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIncidentDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintStemPhysical = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintStemPhysical2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintStemPhysical3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForintStemDisease = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintStemDisease2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintStemDisease3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintAngleToVertical = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintCrownPhysical = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCrownPhysical2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCrownPhysical3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCrownDisease = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCrownDisease2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCrownDisease3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCrownFoliation3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCrownFoliation2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintCrownFoliation = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintRootHeave = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintRootHeave2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintRootHeave3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForintGeneralCondition = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForintVitality = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrUser1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedPicturesGrid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNoFurtherActionRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintSafety = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp01381_AT_Edit_Inspection_DetailsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01381_AT_Edit_Inspection_DetailsTableAdapter();
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter();
            this.sp00190_Contractor_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter();
            this.sp01375_AT_User_Screen_SettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp01375_AT_User_Screen_SettingsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01375_AT_User_Screen_SettingsTableAdapter();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelLastInspection = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.checkEditSychronise = new DevExpress.XtraEditors.CheckEdit();
            this.vGridControl1 = new DevExpress.XtraVerticalGrid.VGridControl();
            this.sp01384ATInspectionEditPreviousInspectionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.categoryRow1 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowTreeID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowInspectionID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSiteID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowClientID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowClientName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowSiteName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeReferenceButtonEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeMappingID = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeGridReference = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeDistance = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow4 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow5 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow6 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow7 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow8 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow9 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow10 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow11 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow12 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow13 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow14 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow15 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow16 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow17 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow18 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow19 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow20 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow21 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow22 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow23 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow24 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow25 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow26 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow27 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow28 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow29 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeTarget1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeTarget2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeTarget3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow30 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow31 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow32 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow33 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow34 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow35 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow36 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow37 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow38 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.editorRow39 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow2 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowstrInspectRefButtonEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintInspectorGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowdtInspectDateDateEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowIncidentDescriptionButtonEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowInspectionIncidentDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintIncidentIDTextEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintGeneralConditionGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintVitalityGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowNoFurtherActionRequiredCheckEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintSafetyGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow3 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowintRootHeaveGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintRootHeave2GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintRootHeave3GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow4 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowintStemPhysicalGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintStemPhysical2GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintStemPhysical3GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintStemDiseaseGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintStemDisease2GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintStemDisease3GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintAngleToVerticalSpinEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow5 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowintCrownPhysicalGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintCrownPhysical2GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintCrownPhysical3GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintCrownDiseaseGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintCrownDisease2GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintCrownDisease3GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintCrownFoliationGridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintCrownFoliation2GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowintCrownFoliation3GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.categoryRow6 = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.rowstrUser1TextEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowstrUser2TextEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowstrUser3TextEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowUserPickList1GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowUserPickList2GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowUserPickList3GridLookUpEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowstrRemarksMemoEdit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLinkedActionCount = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowLinkedOutstandingActionCount = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeHouseName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeSpeciesName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeAccess = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeVisibility = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeContext = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeManagement = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeLegalStatus = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeLastInspectionDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeInspectionCycle = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeInspectionUnit = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeNextInspectionDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeGroundType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeDBH = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeDBHRange = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeHeight = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeHeightRange = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeProtectionType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeCrownDiameter = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreePlantDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeGroupNumber = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeRiskCategory = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeSiteHazardClass = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreePlantSize = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreePlantSource = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreePlantMethod = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreePostcode = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeMapLabel = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeStatus = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeSize = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeLastModifiedDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeRemarks = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeUser1 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeUser2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeUser3 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeAgeClass = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeAreaHa = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeReplantCount = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeSurveyDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeSiteLevel = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeSituation = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeRiskFactor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rowTreeOwnershipName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.sp01384_AT_Inspection_Edit_Previous_InspectionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01384_AT_Inspection_Edit_Previous_InspectionsTableAdapter();
            this.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp02065_AT_Inspection_Pictures_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp02065_AT_Inspection_Pictures_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending39 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp02065ATInspectionPicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01380ATActionsLinkedToInspectionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01381ATEditInspectionDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intInspectionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReferenceButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTreeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInspectDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInspectDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strPictureTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GUIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIncidentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intAngleToVerticalSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strInspectRefButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoFurtherActionRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intInspectorGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSafetyGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intGeneralConditionGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intVitalityGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intRootHeaveGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intRootHeave2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intRootHeave3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemPhysicalGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemPhysical2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemPhysical3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemDiseaseGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemDisease2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemDisease3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownPhysicalGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownPhysical2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownPhysical3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownDiseaseGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownDisease2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownDisease3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownFoliationGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownFoliation2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownFoliation3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList1GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintTreeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGUID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintIncidentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintInspectionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrInspectRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintInspector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtInspectDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemPhysical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemPhysical2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemPhysical3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemDisease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemDisease2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemDisease3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintAngleToVertical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownPhysical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownPhysical2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownPhysical3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownDisease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownDisease2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownDisease3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownFoliation3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownFoliation2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownFoliation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintRootHeave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintRootHeave2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintRootHeave3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintGeneralCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintVitality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedPicturesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoFurtherActionRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSafety)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01375_AT_User_Screen_SettingsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanelLastInspection.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSychronise.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01384ATInspectionEditPreviousInspectionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1110, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 694);
            this.barDockControlBottom.Size = new System.Drawing.Size(1110, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 668);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1110, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 668);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.DockManager = this.dockManager1;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // colItemID
            // 
            this.colItemID.Caption = "Item ID";
            this.colItemID.FieldName = "ItemID";
            this.colItemID.Name = "colItemID";
            this.colItemID.OptionsColumn.AllowEdit = false;
            this.colItemID.OptionsColumn.AllowFocus = false;
            this.colItemID.OptionsColumn.ReadOnly = true;
            this.colItemID.Width = 58;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Item ID";
            this.gridColumn5.FieldName = "ItemID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 58;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Item ID";
            this.gridColumn11.FieldName = "ItemID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 58;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Item ID";
            this.gridColumn17.FieldName = "ItemID";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 58;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Item ID";
            this.gridColumn23.FieldName = "ItemID";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 58;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Item ID";
            this.gridColumn29.FieldName = "ItemID";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 58;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Item ID";
            this.gridColumn35.FieldName = "ItemID";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Width = 58;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Item ID";
            this.gridColumn41.FieldName = "ItemID";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Width = 58;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Item ID";
            this.gridColumn47.FieldName = "ItemID";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.Width = 58;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Item ID";
            this.gridColumn53.FieldName = "ItemID";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.AllowEdit = false;
            this.gridColumn53.OptionsColumn.AllowFocus = false;
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Width = 58;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Item ID";
            this.gridColumn59.FieldName = "ItemID";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Width = 58;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Item ID";
            this.gridColumn65.FieldName = "ItemID";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            this.gridColumn65.Width = 58;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Item ID";
            this.gridColumn71.FieldName = "ItemID";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Width = 58;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Item ID";
            this.gridColumn77.FieldName = "ItemID";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            this.gridColumn77.Width = 58;
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Item ID";
            this.gridColumn83.FieldName = "ItemID";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Width = 58;
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Item ID";
            this.gridColumn89.FieldName = "ItemID";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            this.gridColumn89.Width = 58;
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Item ID";
            this.gridColumn95.FieldName = "ItemID";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.AllowEdit = false;
            this.gridColumn95.OptionsColumn.AllowFocus = false;
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            this.gridColumn95.Width = 58;
            // 
            // gridColumn101
            // 
            this.gridColumn101.Caption = "Item ID";
            this.gridColumn101.FieldName = "ItemID";
            this.gridColumn101.Name = "gridColumn101";
            this.gridColumn101.OptionsColumn.AllowEdit = false;
            this.gridColumn101.OptionsColumn.AllowFocus = false;
            this.gridColumn101.OptionsColumn.ReadOnly = true;
            this.gridColumn101.Width = 58;
            // 
            // gridColumn107
            // 
            this.gridColumn107.Caption = "Item ID";
            this.gridColumn107.FieldName = "ItemID";
            this.gridColumn107.Name = "gridColumn107";
            this.gridColumn107.OptionsColumn.AllowEdit = false;
            this.gridColumn107.OptionsColumn.AllowFocus = false;
            this.gridColumn107.OptionsColumn.ReadOnly = true;
            this.gridColumn107.Width = 58;
            // 
            // gridColumn113
            // 
            this.gridColumn113.Caption = "Item ID";
            this.gridColumn113.FieldName = "ItemID";
            this.gridColumn113.Name = "gridColumn113";
            this.gridColumn113.OptionsColumn.AllowEdit = false;
            this.gridColumn113.OptionsColumn.AllowFocus = false;
            this.gridColumn113.OptionsColumn.ReadOnly = true;
            this.gridColumn113.Width = 58;
            // 
            // gridColumn119
            // 
            this.gridColumn119.Caption = "Item ID";
            this.gridColumn119.FieldName = "ItemID";
            this.gridColumn119.Name = "gridColumn119";
            this.gridColumn119.OptionsColumn.AllowEdit = false;
            this.gridColumn119.OptionsColumn.AllowFocus = false;
            this.gridColumn119.OptionsColumn.ReadOnly = true;
            this.gridColumn119.Width = 58;
            // 
            // gridColumn137
            // 
            this.gridColumn137.Caption = "Item ID";
            this.gridColumn137.FieldName = "ItemID";
            this.gridColumn137.Name = "gridColumn137";
            this.gridColumn137.OptionsColumn.AllowEdit = false;
            this.gridColumn137.OptionsColumn.AllowFocus = false;
            this.gridColumn137.OptionsColumn.ReadOnly = true;
            this.gridColumn137.Width = 58;
            // 
            // gridColumn125
            // 
            this.gridColumn125.Caption = "Item ID";
            this.gridColumn125.FieldName = "ItemID";
            this.gridColumn125.Name = "gridColumn125";
            this.gridColumn125.OptionsColumn.AllowEdit = false;
            this.gridColumn125.OptionsColumn.AllowFocus = false;
            this.gridColumn125.OptionsColumn.ReadOnly = true;
            this.gridColumn125.Width = 58;
            // 
            // gridColumn131
            // 
            this.gridColumn131.Caption = "Item ID";
            this.gridColumn131.FieldName = "ItemID";
            this.gridColumn131.Name = "gridColumn131";
            this.gridColumn131.OptionsColumn.AllowEdit = false;
            this.gridColumn131.OptionsColumn.AllowFocus = false;
            this.gridColumn131.OptionsColumn.ReadOnly = true;
            this.gridColumn131.Width = 58;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2,
            this.bar4,
            this.bar5});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiCopyAllValues,
            this.bbiCopySelectedValues,
            this.bbiRememberDetails,
            this.bbiReload,
            this.bciShowPrevious,
            this.bbiCopyActions,
            this.bbiPasteActions});
            this.barManager2.MaxItemId = 26;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Save Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiFormSave.SuperTip = superToolTip4;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Cancel Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiFormCancel.SuperTip = superToolTip5;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Form Mode - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.barStaticItemFormMode.SuperTip = superToolTip6;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(591, 185);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRememberDetails),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReload, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopyAllValues, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopySelectedValues)});
            this.bar2.Offset = 29;
            this.bar2.Text = "Copy Values";
            // 
            // bbiRememberDetails
            // 
            this.bbiRememberDetails.Caption = "Copy Inspection";
            this.bbiRememberDetails.Id = 21;
            this.bbiRememberDetails.Name = "bbiRememberDetails";
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Copy Inspection - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = resources.GetString("toolTipItem7.Text");
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiRememberDetails.SuperTip = superToolTip7;
            this.bbiRememberDetails.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRememberDetails_ItemClick);
            // 
            // bbiReload
            // 
            this.bbiReload.Caption = "Reload";
            this.bbiReload.Id = 22;
            this.bbiReload.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiReload.ImageOptions.Image")));
            this.bbiReload.Name = "bbiReload";
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Reload - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to refresh the currently loaded last copied record.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiReload.SuperTip = superToolTip8;
            this.bbiReload.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReload_ItemClick);
            // 
            // bbiCopyAllValues
            // 
            this.bbiCopyAllValues.Caption = "Copy ALL Values";
            this.bbiCopyAllValues.Id = 15;
            this.bbiCopyAllValues.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyAllValues.ImageOptions.Image")));
            this.bbiCopyAllValues.Name = "bbiCopyAllValues";
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Paste All Values - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to paste <b>all</b> values from the last copied record to this record.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bbiCopyAllValues.SuperTip = superToolTip9;
            this.bbiCopyAllValues.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopyAllValues_ItemClick);
            // 
            // bbiCopySelectedValues
            // 
            this.bbiCopySelectedValues.Caption = "Copy Selected Values";
            this.bbiCopySelectedValues.Id = 16;
            this.bbiCopySelectedValues.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopySelectedValues.ImageOptions.Image")));
            this.bbiCopySelectedValues.Name = "bbiCopySelectedValues";
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Paste Selected Values - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to open a screen to specify which values to paste from the last copied r" +
    "ecord to this record.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.bbiCopySelectedValues.SuperTip = superToolTip10;
            this.bbiCopySelectedValues.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopySelectedValues_ItemClick);
            // 
            // bar4
            // 
            this.bar4.BarName = "Custom 5";
            this.bar4.DockCol = 3;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciShowPrevious)});
            this.bar4.Text = "Custom 5";
            // 
            // bciShowPrevious
            // 
            this.bciShowPrevious.Caption = "Show Previous Details";
            this.bciShowPrevious.Id = 23;
            this.bciShowPrevious.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciShowPrevious.ImageOptions.Image")));
            this.bciShowPrevious.Name = "bciShowPrevious";
            toolTipTitleItem11.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem11.Text = "Show Last Inspection - Information\r\n";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to show \\ hide the Last Inspection Details Panel.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.bciShowPrevious.SuperTip = superToolTip11;
            this.bciShowPrevious.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciShowPrevious_CheckedChanged);
            // 
            // bar5
            // 
            this.bar5.BarName = "Custom 6";
            this.bar5.DockCol = 2;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopyActions),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPasteActions, true)});
            this.bar5.Text = "Custom 6";
            // 
            // bbiCopyActions
            // 
            this.bbiCopyActions.Caption = "Copy Actions";
            this.bbiCopyActions.Id = 24;
            this.bbiCopyActions.Name = "bbiCopyActions";
            toolTipTitleItem12.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem12.Text = "Copy Actions - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to take a snapshot of the current record�s actions for later recall.\r\n\r\n" +
    "The recalled actions can be copied into other inspections using the Paste Action" +
    "s button.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.bbiCopyActions.SuperTip = superToolTip12;
            this.bbiCopyActions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopyActions_ItemClick);
            // 
            // bbiPasteActions
            // 
            this.bbiPasteActions.Caption = "Paste Actions";
            this.bbiPasteActions.Id = 25;
            this.bbiPasteActions.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPasteActions.ImageOptions.Image")));
            this.bbiPasteActions.Name = "bbiPasteActions";
            toolTipTitleItem13.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem13.Text = "Paste Actions";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Click me to paste copied actions into this inspection.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.bbiPasteActions.SuperTip = superToolTip13;
            this.bbiPasteActions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPasteActions_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1110, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 694);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1110, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 668);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1110, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 668);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Refresh2_16x16, "Refresh2_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "Refresh2_16x16");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gridControl39);
            this.dataLayoutControl1.Controls.Add(this.gridControl2);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.IncidentDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intInspectionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TreeReferenceButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.intTreeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dtInspectDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.strPictureTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateAddedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.GUIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intIncidentIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intAngleToVerticalSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.strInspectRefButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.NoFurtherActionRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.intInspectorGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intSafetyGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intGeneralConditionGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intVitalityGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intRootHeaveGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intRootHeave2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intRootHeave3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intStemPhysicalGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intStemPhysical2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intStemPhysical3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intStemDiseaseGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intStemDisease2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intStemDisease3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownPhysicalGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownPhysical2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownPhysical3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownDiseaseGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownDisease2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownDisease3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownFoliationGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownFoliation2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intCrownFoliation3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList1GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList3GridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp01381ATEditInspectionDetailsBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintTreeID,
            this.ItemForDateAdded,
            this.ItemForstrPicture,
            this.ItemForGUID,
            this.ItemForintIncidentID,
            this.ItemForintInspectionID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1045, 543, 450, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1110, 668);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            this.dataLayoutControl1.HideCustomization += new System.EventHandler(this.dataLayoutControl1_HideCustomization);
            this.dataLayoutControl1.LayoutUpdate += new System.EventHandler(this.dataLayoutControl1_LayoutUpdate);
            // 
            // gridControl39
            // 
            this.gridControl39.DataSource = this.sp02065ATInspectionPicturesListBindingSource;
            this.gridControl39.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl39.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl39.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl39.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl39.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl39.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl39_EmbeddedNavigator_ButtonClick);
            this.gridControl39.Location = new System.Drawing.Point(24, 252);
            this.gridControl39.MainView = this.gridView39;
            this.gridControl39.MenuManager = this.barManager1;
            this.gridControl39.Name = "gridControl39";
            this.gridControl39.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemTextEditDateTime});
            this.gridControl39.Size = new System.Drawing.Size(1062, 392);
            this.gridControl39.TabIndex = 81;
            this.gridControl39.UseEmbeddedNavigator = true;
            this.gridControl39.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView39});
            // 
            // sp02065ATInspectionPicturesListBindingSource
            // 
            this.sp02065ATInspectionPicturesListBindingSource.DataMember = "sp02065_AT_Inspection_Pictures_List";
            this.sp02065ATInspectionPicturesListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView39
            // 
            this.gridView39.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn169,
            this.gridColumn170,
            this.gridColumn171,
            this.gridColumn172,
            this.gridColumn173,
            this.gridColumn174,
            this.gridColumn175,
            this.gridColumn176,
            this.gridColumn177,
            this.gridColumn178,
            this.gridColumn179,
            this.colShortLinkedRecordDescription});
            this.gridView39.GridControl = this.gridControl39;
            this.gridView39.Name = "gridView39";
            this.gridView39.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView39.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView39.OptionsLayout.StoreAppearance = true;
            this.gridView39.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView39.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView39.OptionsSelection.MultiSelect = true;
            this.gridView39.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView39.OptionsView.ColumnAutoWidth = false;
            this.gridView39.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView39.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView39.OptionsView.ShowGroupPanel = false;
            this.gridView39.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn174, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView39.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView39_CustomDrawCell);
            this.gridView39.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView39.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView39.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView39.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView39.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView39.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView39_MouseUp);
            this.gridView39.DoubleClick += new System.EventHandler(this.gridView39_DoubleClick);
            this.gridView39.GotFocus += new System.EventHandler(this.gridView39_GotFocus);
            // 
            // gridColumn169
            // 
            this.gridColumn169.Caption = "Survey Picture ID";
            this.gridColumn169.FieldName = "SurveyPictureID";
            this.gridColumn169.Name = "gridColumn169";
            this.gridColumn169.OptionsColumn.AllowEdit = false;
            this.gridColumn169.OptionsColumn.AllowFocus = false;
            this.gridColumn169.OptionsColumn.ReadOnly = true;
            this.gridColumn169.Width = 105;
            // 
            // gridColumn170
            // 
            this.gridColumn170.Caption = "Linked To Record ID";
            this.gridColumn170.FieldName = "LinkedToRecordID";
            this.gridColumn170.Name = "gridColumn170";
            this.gridColumn170.OptionsColumn.AllowEdit = false;
            this.gridColumn170.OptionsColumn.AllowFocus = false;
            this.gridColumn170.OptionsColumn.ReadOnly = true;
            this.gridColumn170.Width = 117;
            // 
            // gridColumn171
            // 
            this.gridColumn171.Caption = "Linked To Record Type ID";
            this.gridColumn171.FieldName = "LinkedToRecordTypeID";
            this.gridColumn171.Name = "gridColumn171";
            this.gridColumn171.OptionsColumn.AllowEdit = false;
            this.gridColumn171.OptionsColumn.AllowFocus = false;
            this.gridColumn171.OptionsColumn.ReadOnly = true;
            this.gridColumn171.Width = 144;
            // 
            // gridColumn172
            // 
            this.gridColumn172.Caption = "Picture Type ID";
            this.gridColumn172.FieldName = "PictureTypeID";
            this.gridColumn172.Name = "gridColumn172";
            this.gridColumn172.OptionsColumn.AllowEdit = false;
            this.gridColumn172.OptionsColumn.AllowFocus = false;
            this.gridColumn172.OptionsColumn.ReadOnly = true;
            this.gridColumn172.Width = 95;
            // 
            // gridColumn173
            // 
            this.gridColumn173.Caption = "Picture Path";
            this.gridColumn173.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.gridColumn173.FieldName = "PicturePath";
            this.gridColumn173.Name = "gridColumn173";
            this.gridColumn173.OptionsColumn.ReadOnly = true;
            this.gridColumn173.Visible = true;
            this.gridColumn173.VisibleIndex = 2;
            this.gridColumn173.Width = 472;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // gridColumn174
            // 
            this.gridColumn174.Caption = "Date Taken";
            this.gridColumn174.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.gridColumn174.FieldName = "DateTimeTaken";
            this.gridColumn174.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn174.Name = "gridColumn174";
            this.gridColumn174.OptionsColumn.AllowEdit = false;
            this.gridColumn174.OptionsColumn.AllowFocus = false;
            this.gridColumn174.OptionsColumn.ReadOnly = true;
            this.gridColumn174.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn174.Visible = true;
            this.gridColumn174.VisibleIndex = 0;
            this.gridColumn174.Width = 99;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // gridColumn175
            // 
            this.gridColumn175.Caption = "Added By Staff ID";
            this.gridColumn175.FieldName = "AddedByStaffID";
            this.gridColumn175.Name = "gridColumn175";
            this.gridColumn175.OptionsColumn.AllowEdit = false;
            this.gridColumn175.OptionsColumn.AllowFocus = false;
            this.gridColumn175.OptionsColumn.ReadOnly = true;
            this.gridColumn175.Width = 108;
            // 
            // gridColumn176
            // 
            this.gridColumn176.Caption = "GUID";
            this.gridColumn176.FieldName = "GUID";
            this.gridColumn176.Name = "gridColumn176";
            this.gridColumn176.OptionsColumn.AllowEdit = false;
            this.gridColumn176.OptionsColumn.AllowFocus = false;
            this.gridColumn176.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn177
            // 
            this.gridColumn177.Caption = "Remarks";
            this.gridColumn177.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn177.FieldName = "Remarks";
            this.gridColumn177.Name = "gridColumn177";
            this.gridColumn177.OptionsColumn.ReadOnly = true;
            this.gridColumn177.Visible = true;
            this.gridColumn177.VisibleIndex = 3;
            this.gridColumn177.Width = 252;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // gridColumn178
            // 
            this.gridColumn178.Caption = "Linked To Inspection";
            this.gridColumn178.FieldName = "LinkedRecordDescription";
            this.gridColumn178.Name = "gridColumn178";
            this.gridColumn178.OptionsColumn.AllowEdit = false;
            this.gridColumn178.OptionsColumn.AllowFocus = false;
            this.gridColumn178.OptionsColumn.ReadOnly = true;
            this.gridColumn178.Width = 303;
            // 
            // gridColumn179
            // 
            this.gridColumn179.Caption = "Added By";
            this.gridColumn179.FieldName = "AddedByStaffName";
            this.gridColumn179.Name = "gridColumn179";
            this.gridColumn179.OptionsColumn.AllowEdit = false;
            this.gridColumn179.OptionsColumn.AllowFocus = false;
            this.gridColumn179.OptionsColumn.ReadOnly = true;
            this.gridColumn179.Visible = true;
            this.gridColumn179.VisibleIndex = 1;
            this.gridColumn179.Width = 108;
            // 
            // colShortLinkedRecordDescription
            // 
            this.colShortLinkedRecordDescription.Caption = "Linked To Inspection (Short Desc)";
            this.colShortLinkedRecordDescription.FieldName = "ShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.Name = "colShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colShortLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colShortLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colShortLinkedRecordDescription.Width = 231;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(24, 252);
            this.gridControl2.MainView = this.gridView26;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl2.Size = new System.Drawing.Size(1062, 392);
            this.gridControl2.TabIndex = 49;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView26});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView26
            // 
            this.gridView26.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.gridColumn139,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView26.GridControl = this.gridControl2;
            this.gridView26.GroupCount = 1;
            this.gridView26.Name = "gridView26";
            this.gridView26.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView26.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView26.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView26.OptionsLayout.StoreAppearance = true;
            this.gridView26.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView26.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView26.OptionsSelection.MultiSelect = true;
            this.gridView26.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView26.OptionsView.ColumnAutoWidth = false;
            this.gridView26.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView26.OptionsView.ShowGroupPanel = false;
            this.gridView26.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn139, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView26.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView26.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView26.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView26.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView26.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView26.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView26_MouseUp);
            this.gridView26.DoubleClick += new System.EventHandler(this.gridView26_DoubleClick);
            this.gridView26.GotFocus += new System.EventHandler(this.gridView26_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // gridColumn139
            // 
            this.gridColumn139.Caption = "Description";
            this.gridColumn139.FieldName = "Description";
            this.gridColumn139.Name = "gridColumn139";
            this.gridColumn139.OptionsColumn.AllowEdit = false;
            this.gridColumn139.OptionsColumn.AllowFocus = false;
            this.gridColumn139.OptionsColumn.ReadOnly = true;
            this.gridColumn139.Visible = true;
            this.gridColumn139.VisibleIndex = 1;
            this.gridColumn139.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01380ATActionsLinkedToInspectionsBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(24, 252);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit4,
            this.repositoryItemMemoExEdit2});
            this.gridControl1.Size = new System.Drawing.Size(1062, 392);
            this.gridControl1.TabIndex = 48;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01380ATActionsLinkedToInspectionsBindingSource
            // 
            this.sp01380ATActionsLinkedToInspectionsBindingSource.DataMember = "sp01380_AT_Actions_Linked_To_Inspections";
            this.sp01380ATActionsLinkedToInspectionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInspectionID1,
            this.colTreeID1,
            this.colLocalityID2,
            this.colDistrictID2,
            this.colTreeReference1,
            this.colTreeMappingID1,
            this.colInspectionReference1,
            this.colInspectionDate1,
            this.colActionID,
            this.colActionJobNumber,
            this.colAction,
            this.colActionPriority,
            this.colActionDueDate,
            this.colActionDoneDate,
            this.colActionBy,
            this.colActionSupervisor,
            this.colActionOwnership,
            this.colActionCostCentre,
            this.colActionBudget,
            this.colActionWorkUnits,
            this.colActionScheduleOfRates,
            this.colActionBudgetedRateDescription,
            this.colActionBudgetedRate,
            this.colActionBudgetedCost,
            this.colActionActualRateDescription,
            this.colActionActualRate,
            this.colActionActualCost,
            this.colActionWorkOrder,
            this.colActionRemarks,
            this.colActionUserDefined1,
            this.colActionUserDefined2,
            this.colActionUserDefined3,
            this.colActionCostDifference,
            this.colActionCompletionStatus,
            this.colActionTimeliness,
            this.colDiscountRate});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 2;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInspectionDate1, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDueDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colInspectionID1
            // 
            this.colInspectionID1.Caption = "Inspection ID";
            this.colInspectionID1.FieldName = "InspectionID";
            this.colInspectionID1.Name = "colInspectionID1";
            this.colInspectionID1.OptionsColumn.AllowEdit = false;
            this.colInspectionID1.OptionsColumn.AllowFocus = false;
            this.colInspectionID1.OptionsColumn.ReadOnly = true;
            this.colInspectionID1.Width = 76;
            // 
            // colTreeID1
            // 
            this.colTreeID1.Caption = "Tree ID";
            this.colTreeID1.FieldName = "TreeID";
            this.colTreeID1.Name = "colTreeID1";
            this.colTreeID1.OptionsColumn.AllowEdit = false;
            this.colTreeID1.OptionsColumn.AllowFocus = false;
            this.colTreeID1.OptionsColumn.ReadOnly = true;
            this.colTreeID1.Width = 48;
            // 
            // colLocalityID2
            // 
            this.colLocalityID2.Caption = "Locality ID";
            this.colLocalityID2.FieldName = "LocalityID";
            this.colLocalityID2.Name = "colLocalityID2";
            this.colLocalityID2.OptionsColumn.AllowEdit = false;
            this.colLocalityID2.OptionsColumn.AllowFocus = false;
            this.colLocalityID2.OptionsColumn.ReadOnly = true;
            this.colLocalityID2.Width = 62;
            // 
            // colDistrictID2
            // 
            this.colDistrictID2.Caption = "District ID";
            this.colDistrictID2.FieldName = "DistrictID";
            this.colDistrictID2.Name = "colDistrictID2";
            this.colDistrictID2.OptionsColumn.AllowEdit = false;
            this.colDistrictID2.OptionsColumn.AllowFocus = false;
            this.colDistrictID2.OptionsColumn.ReadOnly = true;
            this.colDistrictID2.Width = 59;
            // 
            // colTreeReference1
            // 
            this.colTreeReference1.Caption = "Tree Reference";
            this.colTreeReference1.FieldName = "TreeReference";
            this.colTreeReference1.Name = "colTreeReference1";
            this.colTreeReference1.OptionsColumn.AllowEdit = false;
            this.colTreeReference1.OptionsColumn.AllowFocus = false;
            this.colTreeReference1.OptionsColumn.ReadOnly = true;
            this.colTreeReference1.Width = 97;
            // 
            // colTreeMappingID1
            // 
            this.colTreeMappingID1.Caption = "Map ID";
            this.colTreeMappingID1.FieldName = "TreeMappingID";
            this.colTreeMappingID1.Name = "colTreeMappingID1";
            this.colTreeMappingID1.OptionsColumn.AllowEdit = false;
            this.colTreeMappingID1.OptionsColumn.AllowFocus = false;
            this.colTreeMappingID1.OptionsColumn.ReadOnly = true;
            this.colTreeMappingID1.Width = 46;
            // 
            // colInspectionReference1
            // 
            this.colInspectionReference1.Caption = "Inspection Reference";
            this.colInspectionReference1.FieldName = "InspectionReference";
            this.colInspectionReference1.Name = "colInspectionReference1";
            this.colInspectionReference1.OptionsColumn.AllowEdit = false;
            this.colInspectionReference1.OptionsColumn.AllowFocus = false;
            this.colInspectionReference1.OptionsColumn.ReadOnly = true;
            this.colInspectionReference1.Width = 125;
            // 
            // colInspectionDate1
            // 
            this.colInspectionDate1.Caption = "Inspection Date";
            this.colInspectionDate1.FieldName = "InspectionDate";
            this.colInspectionDate1.Name = "colInspectionDate1";
            this.colInspectionDate1.OptionsColumn.AllowEdit = false;
            this.colInspectionDate1.OptionsColumn.AllowFocus = false;
            this.colInspectionDate1.OptionsColumn.ReadOnly = true;
            this.colInspectionDate1.Width = 98;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            this.colActionID.Width = 56;
            // 
            // colActionJobNumber
            // 
            this.colActionJobNumber.Caption = "Job Number";
            this.colActionJobNumber.FieldName = "ActionJobNumber";
            this.colActionJobNumber.Name = "colActionJobNumber";
            this.colActionJobNumber.OptionsColumn.AllowEdit = false;
            this.colActionJobNumber.OptionsColumn.AllowFocus = false;
            this.colActionJobNumber.OptionsColumn.ReadOnly = true;
            this.colActionJobNumber.Visible = true;
            this.colActionJobNumber.VisibleIndex = 0;
            this.colActionJobNumber.Width = 151;
            // 
            // colAction
            // 
            this.colAction.Caption = "Action";
            this.colAction.FieldName = "Action";
            this.colAction.Name = "colAction";
            this.colAction.OptionsColumn.AllowEdit = false;
            this.colAction.OptionsColumn.AllowFocus = false;
            this.colAction.OptionsColumn.ReadOnly = true;
            this.colAction.Visible = true;
            this.colAction.VisibleIndex = 1;
            this.colAction.Width = 114;
            // 
            // colActionPriority
            // 
            this.colActionPriority.Caption = "Priority";
            this.colActionPriority.FieldName = "ActionPriority";
            this.colActionPriority.Name = "colActionPriority";
            this.colActionPriority.OptionsColumn.AllowEdit = false;
            this.colActionPriority.OptionsColumn.AllowFocus = false;
            this.colActionPriority.OptionsColumn.ReadOnly = true;
            this.colActionPriority.Visible = true;
            this.colActionPriority.VisibleIndex = 2;
            this.colActionPriority.Width = 119;
            // 
            // colActionDueDate
            // 
            this.colActionDueDate.Caption = "Due Date";
            this.colActionDueDate.FieldName = "ActionDueDate";
            this.colActionDueDate.Name = "colActionDueDate";
            this.colActionDueDate.OptionsColumn.AllowEdit = false;
            this.colActionDueDate.OptionsColumn.AllowFocus = false;
            this.colActionDueDate.OptionsColumn.ReadOnly = true;
            this.colActionDueDate.Visible = true;
            this.colActionDueDate.VisibleIndex = 3;
            this.colActionDueDate.Width = 88;
            // 
            // colActionDoneDate
            // 
            this.colActionDoneDate.Caption = "Done Date";
            this.colActionDoneDate.FieldName = "ActionDoneDate";
            this.colActionDoneDate.Name = "colActionDoneDate";
            this.colActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colActionDoneDate.Visible = true;
            this.colActionDoneDate.VisibleIndex = 4;
            this.colActionDoneDate.Width = 79;
            // 
            // colActionBy
            // 
            this.colActionBy.Caption = "Action By";
            this.colActionBy.FieldName = "ActionBy";
            this.colActionBy.Name = "colActionBy";
            this.colActionBy.OptionsColumn.AllowEdit = false;
            this.colActionBy.OptionsColumn.AllowFocus = false;
            this.colActionBy.OptionsColumn.ReadOnly = true;
            this.colActionBy.Visible = true;
            this.colActionBy.VisibleIndex = 7;
            this.colActionBy.Width = 126;
            // 
            // colActionSupervisor
            // 
            this.colActionSupervisor.Caption = "Supervisor";
            this.colActionSupervisor.FieldName = "ActionSupervisor";
            this.colActionSupervisor.Name = "colActionSupervisor";
            this.colActionSupervisor.OptionsColumn.AllowEdit = false;
            this.colActionSupervisor.OptionsColumn.AllowFocus = false;
            this.colActionSupervisor.OptionsColumn.ReadOnly = true;
            this.colActionSupervisor.Visible = true;
            this.colActionSupervisor.VisibleIndex = 8;
            this.colActionSupervisor.Width = 125;
            // 
            // colActionOwnership
            // 
            this.colActionOwnership.Caption = "Ownership";
            this.colActionOwnership.FieldName = "ActionOwnership";
            this.colActionOwnership.Name = "colActionOwnership";
            this.colActionOwnership.OptionsColumn.AllowEdit = false;
            this.colActionOwnership.OptionsColumn.AllowFocus = false;
            this.colActionOwnership.OptionsColumn.ReadOnly = true;
            this.colActionOwnership.Visible = true;
            this.colActionOwnership.VisibleIndex = 9;
            this.colActionOwnership.Width = 118;
            // 
            // colActionCostCentre
            // 
            this.colActionCostCentre.Caption = "Cost Centre";
            this.colActionCostCentre.FieldName = "ActionCostCentre";
            this.colActionCostCentre.Name = "colActionCostCentre";
            this.colActionCostCentre.OptionsColumn.AllowEdit = false;
            this.colActionCostCentre.OptionsColumn.AllowFocus = false;
            this.colActionCostCentre.OptionsColumn.ReadOnly = true;
            this.colActionCostCentre.Visible = true;
            this.colActionCostCentre.VisibleIndex = 10;
            this.colActionCostCentre.Width = 124;
            // 
            // colActionBudget
            // 
            this.colActionBudget.Caption = "Budget";
            this.colActionBudget.FieldName = "ActionBudget";
            this.colActionBudget.Name = "colActionBudget";
            this.colActionBudget.OptionsColumn.AllowEdit = false;
            this.colActionBudget.OptionsColumn.AllowFocus = false;
            this.colActionBudget.OptionsColumn.ReadOnly = true;
            this.colActionBudget.Visible = true;
            this.colActionBudget.VisibleIndex = 11;
            this.colActionBudget.Width = 111;
            // 
            // colActionWorkUnits
            // 
            this.colActionWorkUnits.Caption = "Work Units";
            this.colActionWorkUnits.ColumnEdit = this.repositoryItemTextEdit1;
            this.colActionWorkUnits.FieldName = "ActionWorkUnits";
            this.colActionWorkUnits.Name = "colActionWorkUnits";
            this.colActionWorkUnits.OptionsColumn.AllowEdit = false;
            this.colActionWorkUnits.OptionsColumn.AllowFocus = false;
            this.colActionWorkUnits.OptionsColumn.ReadOnly = true;
            this.colActionWorkUnits.Visible = true;
            this.colActionWorkUnits.VisibleIndex = 12;
            this.colActionWorkUnits.Width = 74;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "f2";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colActionScheduleOfRates
            // 
            this.colActionScheduleOfRates.Caption = "Schedule Of Rates";
            this.colActionScheduleOfRates.FieldName = "ActionScheduleOfRates";
            this.colActionScheduleOfRates.Name = "colActionScheduleOfRates";
            this.colActionScheduleOfRates.OptionsColumn.AllowEdit = false;
            this.colActionScheduleOfRates.OptionsColumn.AllowFocus = false;
            this.colActionScheduleOfRates.OptionsColumn.ReadOnly = true;
            this.colActionScheduleOfRates.Visible = true;
            this.colActionScheduleOfRates.VisibleIndex = 13;
            this.colActionScheduleOfRates.Width = 111;
            // 
            // colActionBudgetedRateDescription
            // 
            this.colActionBudgetedRateDescription.Caption = "Budgeted Rate Desc";
            this.colActionBudgetedRateDescription.FieldName = "ActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.Name = "colActionBudgetedRateDescription";
            this.colActionBudgetedRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRateDescription.Visible = true;
            this.colActionBudgetedRateDescription.VisibleIndex = 14;
            this.colActionBudgetedRateDescription.Width = 165;
            // 
            // colActionBudgetedRate
            // 
            this.colActionBudgetedRate.Caption = "Budgeted Rate";
            this.colActionBudgetedRate.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionBudgetedRate.FieldName = "ActionBudgetedRate";
            this.colActionBudgetedRate.Name = "colActionBudgetedRate";
            this.colActionBudgetedRate.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedRate.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedRate.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedRate.Visible = true;
            this.colActionBudgetedRate.VisibleIndex = 15;
            this.colActionBudgetedRate.Width = 94;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colActionBudgetedCost
            // 
            this.colActionBudgetedCost.Caption = "Budgeted Cost";
            this.colActionBudgetedCost.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionBudgetedCost.FieldName = "ActionBudgetedCost";
            this.colActionBudgetedCost.Name = "colActionBudgetedCost";
            this.colActionBudgetedCost.OptionsColumn.AllowEdit = false;
            this.colActionBudgetedCost.OptionsColumn.AllowFocus = false;
            this.colActionBudgetedCost.OptionsColumn.ReadOnly = true;
            this.colActionBudgetedCost.Visible = true;
            this.colActionBudgetedCost.VisibleIndex = 16;
            this.colActionBudgetedCost.Width = 93;
            // 
            // colActionActualRateDescription
            // 
            this.colActionActualRateDescription.Caption = "Actual Rate Desc";
            this.colActionActualRateDescription.FieldName = "ActionActualRateDescription";
            this.colActionActualRateDescription.Name = "colActionActualRateDescription";
            this.colActionActualRateDescription.OptionsColumn.AllowEdit = false;
            this.colActionActualRateDescription.OptionsColumn.AllowFocus = false;
            this.colActionActualRateDescription.OptionsColumn.ReadOnly = true;
            this.colActionActualRateDescription.Visible = true;
            this.colActionActualRateDescription.VisibleIndex = 17;
            this.colActionActualRateDescription.Width = 148;
            // 
            // colActionActualRate
            // 
            this.colActionActualRate.Caption = "Actual Rate";
            this.colActionActualRate.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionActualRate.FieldName = "ActionActualRate";
            this.colActionActualRate.Name = "colActionActualRate";
            this.colActionActualRate.OptionsColumn.AllowEdit = false;
            this.colActionActualRate.OptionsColumn.AllowFocus = false;
            this.colActionActualRate.OptionsColumn.ReadOnly = true;
            this.colActionActualRate.Visible = true;
            this.colActionActualRate.VisibleIndex = 18;
            this.colActionActualRate.Width = 78;
            // 
            // colActionActualCost
            // 
            this.colActionActualCost.Caption = "Actual Cost";
            this.colActionActualCost.ColumnEdit = this.repositoryItemTextEdit2;
            this.colActionActualCost.FieldName = "ActionActualCost";
            this.colActionActualCost.Name = "colActionActualCost";
            this.colActionActualCost.OptionsColumn.AllowEdit = false;
            this.colActionActualCost.OptionsColumn.AllowFocus = false;
            this.colActionActualCost.OptionsColumn.ReadOnly = true;
            this.colActionActualCost.Visible = true;
            this.colActionActualCost.VisibleIndex = 20;
            this.colActionActualCost.Width = 77;
            // 
            // colActionWorkOrder
            // 
            this.colActionWorkOrder.FieldName = "ActionWorkOrder";
            this.colActionWorkOrder.Name = "colActionWorkOrder";
            this.colActionWorkOrder.OptionsColumn.AllowEdit = false;
            this.colActionWorkOrder.OptionsColumn.AllowFocus = false;
            this.colActionWorkOrder.OptionsColumn.ReadOnly = true;
            this.colActionWorkOrder.Visible = true;
            this.colActionWorkOrder.VisibleIndex = 22;
            this.colActionWorkOrder.Width = 111;
            // 
            // colActionRemarks
            // 
            this.colActionRemarks.Caption = "Remarks";
            this.colActionRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colActionRemarks.FieldName = "ActionRemarks";
            this.colActionRemarks.Name = "colActionRemarks";
            this.colActionRemarks.OptionsColumn.ReadOnly = true;
            this.colActionRemarks.Visible = true;
            this.colActionRemarks.VisibleIndex = 23;
            this.colActionRemarks.Width = 63;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colActionUserDefined1
            // 
            this.colActionUserDefined1.Caption = "User 1";
            this.colActionUserDefined1.FieldName = "ActionUserDefined1";
            this.colActionUserDefined1.Name = "colActionUserDefined1";
            this.colActionUserDefined1.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined1.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined1.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined1.Visible = true;
            this.colActionUserDefined1.VisibleIndex = 24;
            this.colActionUserDefined1.Width = 53;
            // 
            // colActionUserDefined2
            // 
            this.colActionUserDefined2.Caption = "User 2";
            this.colActionUserDefined2.FieldName = "ActionUserDefined2";
            this.colActionUserDefined2.Name = "colActionUserDefined2";
            this.colActionUserDefined2.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined2.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined2.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined2.Visible = true;
            this.colActionUserDefined2.VisibleIndex = 25;
            this.colActionUserDefined2.Width = 53;
            // 
            // colActionUserDefined3
            // 
            this.colActionUserDefined3.Caption = "User 3";
            this.colActionUserDefined3.FieldName = "ActionUserDefined3";
            this.colActionUserDefined3.Name = "colActionUserDefined3";
            this.colActionUserDefined3.OptionsColumn.AllowEdit = false;
            this.colActionUserDefined3.OptionsColumn.AllowFocus = false;
            this.colActionUserDefined3.OptionsColumn.ReadOnly = true;
            this.colActionUserDefined3.Visible = true;
            this.colActionUserDefined3.VisibleIndex = 26;
            this.colActionUserDefined3.Width = 53;
            // 
            // colActionCostDifference
            // 
            this.colActionCostDifference.Caption = "Cost Difference";
            this.colActionCostDifference.FieldName = "ActionCostDifference";
            this.colActionCostDifference.Name = "colActionCostDifference";
            this.colActionCostDifference.OptionsColumn.AllowEdit = false;
            this.colActionCostDifference.OptionsColumn.AllowFocus = false;
            this.colActionCostDifference.OptionsColumn.ReadOnly = true;
            this.colActionCostDifference.Visible = true;
            this.colActionCostDifference.VisibleIndex = 21;
            this.colActionCostDifference.Width = 97;
            // 
            // colActionCompletionStatus
            // 
            this.colActionCompletionStatus.Caption = "Completion Status";
            this.colActionCompletionStatus.FieldName = "ActionCompletionStatus";
            this.colActionCompletionStatus.Name = "colActionCompletionStatus";
            this.colActionCompletionStatus.OptionsColumn.AllowEdit = false;
            this.colActionCompletionStatus.OptionsColumn.AllowFocus = false;
            this.colActionCompletionStatus.OptionsColumn.ReadOnly = true;
            this.colActionCompletionStatus.Visible = true;
            this.colActionCompletionStatus.VisibleIndex = 6;
            this.colActionCompletionStatus.Width = 109;
            // 
            // colActionTimeliness
            // 
            this.colActionTimeliness.Caption = "Timeliness";
            this.colActionTimeliness.FieldName = "ActionTimeliness";
            this.colActionTimeliness.Name = "colActionTimeliness";
            this.colActionTimeliness.OptionsColumn.AllowEdit = false;
            this.colActionTimeliness.OptionsColumn.AllowFocus = false;
            this.colActionTimeliness.OptionsColumn.ReadOnly = true;
            this.colActionTimeliness.Visible = true;
            this.colActionTimeliness.VisibleIndex = 5;
            this.colActionTimeliness.Width = 70;
            // 
            // colDiscountRate
            // 
            this.colDiscountRate.Caption = "Discount Rate";
            this.colDiscountRate.ColumnEdit = this.repositoryItemTextEdit4;
            this.colDiscountRate.FieldName = "DiscountRate";
            this.colDiscountRate.Name = "colDiscountRate";
            this.colDiscountRate.OptionsColumn.AllowEdit = false;
            this.colDiscountRate.OptionsColumn.AllowFocus = false;
            this.colDiscountRate.OptionsColumn.ReadOnly = true;
            this.colDiscountRate.Visible = true;
            this.colDiscountRate.VisibleIndex = 19;
            this.colDiscountRate.Width = 89;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "P2";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // IncidentDescriptionButtonEdit
            // 
            this.IncidentDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "IncidentDescription", true));
            this.IncidentDescriptionButtonEdit.Location = new System.Drawing.Point(150, 133);
            this.IncidentDescriptionButtonEdit.MenuManager = this.barManager1;
            this.IncidentDescriptionButtonEdit.Name = "IncidentDescriptionButtonEdit";
            this.IncidentDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Open Incident Selection Screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "View Selected Incident", "view", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Clear", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Clear Selected Incident", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.IncidentDescriptionButtonEdit.Properties.ReadOnly = true;
            this.IncidentDescriptionButtonEdit.Size = new System.Drawing.Size(948, 20);
            this.IncidentDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.IncidentDescriptionButtonEdit.TabIndex = 0;
            this.IncidentDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.IncidentDescriptionButtonEdit_ButtonClick);
            // 
            // sp01381ATEditInspectionDetailsBindingSource
            // 
            this.sp01381ATEditInspectionDetailsBindingSource.DataMember = "sp01381_AT_Edit_Inspection_Details";
            this.sp01381ATEditInspectionDetailsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp01381ATEditInspectionDetailsBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(149, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 47;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // intInspectionIDTextEdit
            // 
            this.intInspectionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intInspectionID", true));
            this.intInspectionIDTextEdit.Location = new System.Drawing.Point(163, 422);
            this.intInspectionIDTextEdit.MenuManager = this.barManager1;
            this.intInspectionIDTextEdit.Name = "intInspectionIDTextEdit";
            this.intInspectionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intInspectionIDTextEdit, true);
            this.intInspectionIDTextEdit.Size = new System.Drawing.Size(196, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intInspectionIDTextEdit, optionsSpelling1);
            this.intInspectionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intInspectionIDTextEdit.TabIndex = 4;
            // 
            // TreeReferenceButtonEdit
            // 
            this.TreeReferenceButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "TreeReference", true));
            this.TreeReferenceButtonEdit.Location = new System.Drawing.Point(150, 35);
            this.TreeReferenceButtonEdit.MenuManager = this.barManager1;
            this.TreeReferenceButtonEdit.Name = "TreeReferenceButtonEdit";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "View Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>open</b> the <b>Parent Tree</b> record for the current Inspection." +
    "";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.TreeReferenceButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to Select Parent Tree", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "view", superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.TreeReferenceButtonEdit.Properties.ReadOnly = true;
            this.TreeReferenceButtonEdit.Size = new System.Drawing.Size(948, 20);
            this.TreeReferenceButtonEdit.StyleController = this.dataLayoutControl1;
            this.TreeReferenceButtonEdit.TabIndex = 1;
            this.TreeReferenceButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TreeReferenceButtonEdit_ButtonClick);
            this.TreeReferenceButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TreeReferenceButtonEdit_Validating);
            // 
            // intTreeIDTextEdit
            // 
            this.intTreeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intTreeID", true));
            this.intTreeIDTextEdit.Location = new System.Drawing.Point(147, 59);
            this.intTreeIDTextEdit.MenuManager = this.barManager1;
            this.intTreeIDTextEdit.Name = "intTreeIDTextEdit";
            this.intTreeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intTreeIDTextEdit, true);
            this.intTreeIDTextEdit.Size = new System.Drawing.Size(897, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intTreeIDTextEdit, optionsSpelling2);
            this.intTreeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intTreeIDTextEdit.TabIndex = 6;
            // 
            // dtInspectDateDateEdit
            // 
            this.dtInspectDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "dtInspectDate", true));
            this.dtInspectDateDateEdit.EditValue = null;
            this.dtInspectDateDateEdit.Location = new System.Drawing.Point(150, 109);
            this.dtInspectDateDateEdit.MenuManager = this.barManager1;
            this.dtInspectDateDateEdit.Name = "dtInspectDateDateEdit";
            this.dtInspectDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dtInspectDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dtInspectDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtInspectDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dtInspectDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtInspectDateDateEdit.Size = new System.Drawing.Size(160, 20);
            this.dtInspectDateDateEdit.StyleController = this.dataLayoutControl1;
            this.dtInspectDateDateEdit.TabIndex = 9;
            this.dtInspectDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.dtInspectDateDateEdit_Validating);
            // 
            // strPictureTextEdit
            // 
            this.strPictureTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "strPicture", true));
            this.strPictureTextEdit.Location = new System.Drawing.Point(147, 111);
            this.strPictureTextEdit.MenuManager = this.barManager1;
            this.strPictureTextEdit.Name = "strPictureTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strPictureTextEdit, true);
            this.strPictureTextEdit.Size = new System.Drawing.Size(897, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strPictureTextEdit, optionsSpelling3);
            this.strPictureTextEdit.StyleController = this.dataLayoutControl1;
            this.strPictureTextEdit.TabIndex = 25;
            // 
            // DateAddedDateEdit
            // 
            this.DateAddedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "DateAdded", true));
            this.DateAddedDateEdit.EditValue = null;
            this.DateAddedDateEdit.Location = new System.Drawing.Point(147, 59);
            this.DateAddedDateEdit.MenuManager = this.barManager1;
            this.DateAddedDateEdit.Name = "DateAddedDateEdit";
            this.DateAddedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateAddedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateAddedDateEdit.Size = new System.Drawing.Size(897, 20);
            this.DateAddedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateAddedDateEdit.TabIndex = 40;
            // 
            // GUIDTextEdit
            // 
            this.GUIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "GUID", true));
            this.GUIDTextEdit.Location = new System.Drawing.Point(161, 624);
            this.GUIDTextEdit.MenuManager = this.barManager1;
            this.GUIDTextEdit.Name = "GUIDTextEdit";
            this.GUIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GUIDTextEdit, true);
            this.GUIDTextEdit.Size = new System.Drawing.Size(925, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GUIDTextEdit, optionsSpelling4);
            this.GUIDTextEdit.StyleController = this.dataLayoutControl1;
            this.GUIDTextEdit.TabIndex = 41;
            // 
            // intIncidentIDTextEdit
            // 
            this.intIncidentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intIncidentID", true));
            this.intIncidentIDTextEdit.EditValue = "";
            this.intIncidentIDTextEdit.Location = new System.Drawing.Point(149, 636);
            this.intIncidentIDTextEdit.MenuManager = this.barManager1;
            this.intIncidentIDTextEdit.Name = "intIncidentIDTextEdit";
            this.intIncidentIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.intIncidentIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.intIncidentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intIncidentIDTextEdit, true);
            this.intIncidentIDTextEdit.Size = new System.Drawing.Size(949, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intIncidentIDTextEdit, optionsSpelling5);
            this.intIncidentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intIncidentIDTextEdit.TabIndex = 10;
            this.intIncidentIDTextEdit.TabStop = false;
            // 
            // intAngleToVerticalSpinEdit
            // 
            this.intAngleToVerticalSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intAngleToVertical", true));
            this.intAngleToVerticalSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intAngleToVerticalSpinEdit.Location = new System.Drawing.Point(162, 530);
            this.intAngleToVerticalSpinEdit.MenuManager = this.barManager1;
            this.intAngleToVerticalSpinEdit.Name = "intAngleToVerticalSpinEdit";
            this.intAngleToVerticalSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.intAngleToVerticalSpinEdit.Properties.IsFloatValue = false;
            this.intAngleToVerticalSpinEdit.Properties.Mask.EditMask = "##0 degrees";
            this.intAngleToVerticalSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.intAngleToVerticalSpinEdit.Properties.MaxValue = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.intAngleToVerticalSpinEdit.Size = new System.Drawing.Size(883, 20);
            this.intAngleToVerticalSpinEdit.StyleController = this.dataLayoutControl1;
            this.intAngleToVerticalSpinEdit.TabIndex = 13;
            // 
            // strUser1TextEdit
            // 
            this.strUser1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "strUser1", true));
            this.strUser1TextEdit.Location = new System.Drawing.Point(162, 252);
            this.strUser1TextEdit.MenuManager = this.barManager1;
            this.strUser1TextEdit.Name = "strUser1TextEdit";
            this.strUser1TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser1TextEdit, true);
            this.strUser1TextEdit.Size = new System.Drawing.Size(268, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser1TextEdit, optionsSpelling6);
            this.strUser1TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser1TextEdit.TabIndex = 21;
            // 
            // strUser2TextEdit
            // 
            this.strUser2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "strUser2", true));
            this.strUser2TextEdit.Location = new System.Drawing.Point(162, 276);
            this.strUser2TextEdit.MenuManager = this.barManager1;
            this.strUser2TextEdit.Name = "strUser2TextEdit";
            this.strUser2TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser2TextEdit, true);
            this.strUser2TextEdit.Size = new System.Drawing.Size(268, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser2TextEdit, optionsSpelling7);
            this.strUser2TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser2TextEdit.TabIndex = 22;
            // 
            // strUser3TextEdit
            // 
            this.strUser3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "strUser3", true));
            this.strUser3TextEdit.Location = new System.Drawing.Point(162, 300);
            this.strUser3TextEdit.MenuManager = this.barManager1;
            this.strUser3TextEdit.Name = "strUser3TextEdit";
            this.strUser3TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser3TextEdit, true);
            this.strUser3TextEdit.Size = new System.Drawing.Size(268, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser3TextEdit, optionsSpelling8);
            this.strUser3TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser3TextEdit.TabIndex = 23;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(24, 252);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(1062, 392);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling9);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 24;
            // 
            // strInspectRefButtonEdit
            // 
            this.strInspectRefButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "strInspectRef", true));
            this.strInspectRefButtonEdit.Location = new System.Drawing.Point(150, 59);
            this.strInspectRefButtonEdit.MenuManager = this.barManager1;
            this.strInspectRefButtonEdit.Name = "strInspectRefButtonEdit";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Sequence Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Number Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.strInspectRefButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", "Sequence", superToolTip2, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", "Number", superToolTip3, DevExpress.Utils.ToolTipAnchor.Default)});
            this.strInspectRefButtonEdit.Size = new System.Drawing.Size(948, 20);
            this.strInspectRefButtonEdit.StyleController = this.dataLayoutControl1;
            this.strInspectRefButtonEdit.TabIndex = 7;
            this.strInspectRefButtonEdit.TabStop = false;
            this.strInspectRefButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.strInspectRefButtonEdit_ButtonClick);
            this.strInspectRefButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strInspectRefButtonEdit_Validating);
            // 
            // NoFurtherActionRequiredCheckEdit
            // 
            this.NoFurtherActionRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "NoFurtherActionRequired", true));
            this.NoFurtherActionRequiredCheckEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.NoFurtherActionRequiredCheckEdit.Location = new System.Drawing.Point(150, 183);
            this.NoFurtherActionRequiredCheckEdit.MenuManager = this.barManager1;
            this.NoFurtherActionRequiredCheckEdit.Name = "NoFurtherActionRequiredCheckEdit";
            this.NoFurtherActionRequiredCheckEdit.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.NoFurtherActionRequiredCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.NoFurtherActionRequiredCheckEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.NoFurtherActionRequiredCheckEdit.Properties.ValueChecked = 1;
            this.NoFurtherActionRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.NoFurtherActionRequiredCheckEdit.Size = new System.Drawing.Size(160, 19);
            this.NoFurtherActionRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.NoFurtherActionRequiredCheckEdit.TabIndex = 39;
            this.NoFurtherActionRequiredCheckEdit.TabStop = false;
            // 
            // intInspectorGridLookUpEdit
            // 
            this.intInspectorGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intInspector", true));
            this.intInspectorGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intInspectorGridLookUpEdit.Location = new System.Drawing.Point(150, 83);
            this.intInspectorGridLookUpEdit.MenuManager = this.barManager1;
            this.intInspectorGridLookUpEdit.Name = "intInspectorGridLookUpEdit";
            editorButtonImageOptions9.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions10.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intInspectorGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intInspectorGridLookUpEdit.Properties.DataSource = this.sp00190ContractorListWithBlankBindingSource;
            this.intInspectorGridLookUpEdit.Properties.DisplayMember = "ContractorName";
            this.intInspectorGridLookUpEdit.Properties.NullText = "";
            this.intInspectorGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.intInspectorGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.intInspectorGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intInspectorGridLookUpEdit.Properties.ValueMember = "ContractorID";
            this.intInspectorGridLookUpEdit.Size = new System.Drawing.Size(948, 22);
            this.intInspectorGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intInspectorGridLookUpEdit.TabIndex = 8;
            this.intInspectorGridLookUpEdit.TabStop = false;
            this.intInspectorGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intInspectorGridLookUpEdit_ButtonClick);
            this.intInspectorGridLookUpEdit.Enter += new System.EventHandler(this.intInspectorGridLookUpEdit_Enter);
            this.intInspectorGridLookUpEdit.Leave += new System.EventHandler(this.intInspectorGridLookUpEdit_Leave);
            // 
            // sp00190ContractorListWithBlankBindingSource
            // 
            this.sp00190ContractorListWithBlankBindingSource.DataMember = "sp00190_Contractor_List_With_Blank";
            this.sp00190ContractorListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colContractorCode,
            this.colContractorID,
            this.colContractorName,
            this.colDisabled,
            this.colEmailPassword,
            this.colInternalContractor,
            this.colInternalCOntractorDescription,
            this.colMobile,
            this.colPostcode,
            this.colRemarks,
            this.colTelephone1,
            this.colTelephone2,
            this.colTypeDescription,
            this.colTypeID,
            this.colUserDefined1,
            this.colUserDefined2,
            this.colUserDefined3,
            this.colVatReg,
            this.colWebsite});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colContractorID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInternalCOntractorDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 2;
            this.colAddressLine1.Width = 144;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Contractor Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Width = 101;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 273;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Width = 94;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor ID";
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 118;
            // 
            // colInternalCOntractorDescription
            // 
            this.colInternalCOntractorDescription.Caption = "Status";
            this.colInternalCOntractorDescription.FieldName = "InternalCOntractorDescription";
            this.colInternalCOntractorDescription.Name = "colInternalCOntractorDescription";
            this.colInternalCOntractorDescription.OptionsColumn.AllowEdit = false;
            this.colInternalCOntractorDescription.OptionsColumn.AllowFocus = false;
            this.colInternalCOntractorDescription.OptionsColumn.ReadOnly = true;
            this.colInternalCOntractorDescription.Width = 52;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Width = 51;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Width = 62;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 80;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 1;
            this.colTypeDescription.Width = 111;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            this.colTypeID.Width = 49;
            // 
            // colUserDefined1
            // 
            this.colUserDefined1.Caption = "User Defined 1";
            this.colUserDefined1.FieldName = "UserDefined1";
            this.colUserDefined1.Name = "colUserDefined1";
            this.colUserDefined1.OptionsColumn.AllowEdit = false;
            this.colUserDefined1.OptionsColumn.AllowFocus = false;
            this.colUserDefined1.OptionsColumn.ReadOnly = true;
            this.colUserDefined1.Width = 92;
            // 
            // colUserDefined2
            // 
            this.colUserDefined2.Caption = "User Defined 2";
            this.colUserDefined2.FieldName = "UserDefined2";
            this.colUserDefined2.Name = "colUserDefined2";
            this.colUserDefined2.OptionsColumn.AllowEdit = false;
            this.colUserDefined2.OptionsColumn.AllowFocus = false;
            this.colUserDefined2.OptionsColumn.ReadOnly = true;
            this.colUserDefined2.Width = 92;
            // 
            // colUserDefined3
            // 
            this.colUserDefined3.Caption = "User Defined 3";
            this.colUserDefined3.FieldName = "UserDefined3";
            this.colUserDefined3.Name = "colUserDefined3";
            this.colUserDefined3.OptionsColumn.AllowEdit = false;
            this.colUserDefined3.OptionsColumn.AllowFocus = false;
            this.colUserDefined3.OptionsColumn.ReadOnly = true;
            this.colUserDefined3.Width = 92;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "VAT Reg";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Width = 62;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Width = 60;
            // 
            // intSafetyGridLookUpEdit
            // 
            this.intSafetyGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intSafety", true));
            this.intSafetyGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intSafetyGridLookUpEdit.Location = new System.Drawing.Point(150, 157);
            this.intSafetyGridLookUpEdit.MenuManager = this.barManager1;
            this.intSafetyGridLookUpEdit.Name = "intSafetyGridLookUpEdit";
            editorButtonImageOptions11.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions12.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intSafetyGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intSafetyGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intSafetyGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intSafetyGridLookUpEdit.Properties.NullText = "";
            this.intSafetyGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.intSafetyGridLookUpEdit.Properties.Tag = "";
            this.intSafetyGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intSafetyGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intSafetyGridLookUpEdit.Size = new System.Drawing.Size(948, 22);
            this.intSafetyGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intSafetyGridLookUpEdit.TabIndex = 17;
            this.intSafetyGridLookUpEdit.TabStop = false;
            this.intSafetyGridLookUpEdit.Tag = "103";
            // 
            // sp01372ATMultiplePicklistsWithBlanksBindingSource
            // 
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataMember = "sp01372_AT_Multiple_Picklists_With_Blanks";
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHeaderDescription,
            this.colHeaderID,
            this.colItemCode,
            this.colItemDescription,
            this.colItemID,
            this.colOrder1});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colItemID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Picklist Type";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.Width = 189;
            // 
            // colHeaderID
            // 
            this.colHeaderID.Caption = "Picklist ID";
            this.colHeaderID.FieldName = "HeaderID";
            this.colHeaderID.Name = "colHeaderID";
            this.colHeaderID.OptionsColumn.AllowEdit = false;
            this.colHeaderID.OptionsColumn.AllowFocus = false;
            this.colHeaderID.OptionsColumn.ReadOnly = true;
            this.colHeaderID.Width = 67;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Item ID";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.OptionsColumn.AllowFocus = false;
            this.colItemCode.OptionsColumn.ReadOnly = true;
            this.colItemCode.Width = 58;
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Description";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 264;
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.AllowFocus = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            this.colOrder1.Width = 65;
            // 
            // intGeneralConditionGridLookUpEdit
            // 
            this.intGeneralConditionGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intGeneralCondition", true));
            this.intGeneralConditionGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intGeneralConditionGridLookUpEdit.Location = new System.Drawing.Point(162, 252);
            this.intGeneralConditionGridLookUpEdit.MenuManager = this.barManager1;
            this.intGeneralConditionGridLookUpEdit.Name = "intGeneralConditionGridLookUpEdit";
            editorButtonImageOptions13.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions14.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intGeneralConditionGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions13, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions14, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject53, serializableAppearanceObject54, serializableAppearanceObject55, serializableAppearanceObject56, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intGeneralConditionGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intGeneralConditionGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intGeneralConditionGridLookUpEdit.Properties.NullText = "";
            this.intGeneralConditionGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.intGeneralConditionGridLookUpEdit.Properties.Tag = "";
            this.intGeneralConditionGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intGeneralConditionGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intGeneralConditionGridLookUpEdit.Size = new System.Drawing.Size(924, 22);
            this.intGeneralConditionGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intGeneralConditionGridLookUpEdit.TabIndex = 18;
            this.intGeneralConditionGridLookUpEdit.TabStop = false;
            this.intGeneralConditionGridLookUpEdit.Tag = "104";
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn5;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsCustomization.AllowFilter = false;
            this.gridView3.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowFilterEditor = false;
            this.gridView3.OptionsFilter.AllowMRUFilterList = false;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Picklist Type";
            this.gridColumn1.FieldName = "HeaderDescription";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 189;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Picklist ID";
            this.gridColumn2.FieldName = "HeaderID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 67;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Item ID";
            this.gridColumn3.FieldName = "ItemCode";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 58;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Description";
            this.gridColumn4.FieldName = "ItemDescription";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 264;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 65;
            // 
            // intVitalityGridLookUpEdit
            // 
            this.intVitalityGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intVitality", true));
            this.intVitalityGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intVitalityGridLookUpEdit.Location = new System.Drawing.Point(162, 278);
            this.intVitalityGridLookUpEdit.MenuManager = this.barManager1;
            this.intVitalityGridLookUpEdit.Name = "intVitalityGridLookUpEdit";
            editorButtonImageOptions15.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions16.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intVitalityGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions15, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject57, serializableAppearanceObject58, serializableAppearanceObject59, serializableAppearanceObject60, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject61, serializableAppearanceObject62, serializableAppearanceObject63, serializableAppearanceObject64, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intVitalityGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intVitalityGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intVitalityGridLookUpEdit.Properties.NullText = "";
            this.intVitalityGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.intVitalityGridLookUpEdit.Properties.Tag = "";
            this.intVitalityGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intVitalityGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intVitalityGridLookUpEdit.Size = new System.Drawing.Size(924, 22);
            this.intVitalityGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intVitalityGridLookUpEdit.TabIndex = 26;
            this.intVitalityGridLookUpEdit.TabStop = false;
            this.intVitalityGridLookUpEdit.Tag = "138";
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.gridColumn11;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView4.OptionsCustomization.AllowFilter = false;
            this.gridView4.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView4.OptionsFilter.AllowFilterEditor = false;
            this.gridView4.OptionsFilter.AllowMRUFilterList = false;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Picklist Type";
            this.gridColumn7.FieldName = "HeaderDescription";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 189;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Picklist ID";
            this.gridColumn8.FieldName = "HeaderID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 67;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Item ID";
            this.gridColumn9.FieldName = "ItemCode";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 58;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Description";
            this.gridColumn10.FieldName = "ItemDescription";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 264;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "Order";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 65;
            // 
            // intRootHeaveGridLookUpEdit
            // 
            this.intRootHeaveGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intRootHeave", true));
            this.intRootHeaveGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intRootHeaveGridLookUpEdit.Location = new System.Drawing.Point(162, 314);
            this.intRootHeaveGridLookUpEdit.MenuManager = this.barManager1;
            this.intRootHeaveGridLookUpEdit.Name = "intRootHeaveGridLookUpEdit";
            editorButtonImageOptions17.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions18.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intRootHeaveGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions17, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject65, serializableAppearanceObject66, serializableAppearanceObject67, serializableAppearanceObject68, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions18, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject69, serializableAppearanceObject70, serializableAppearanceObject71, serializableAppearanceObject72, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intRootHeaveGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intRootHeaveGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intRootHeaveGridLookUpEdit.Properties.NullText = "";
            this.intRootHeaveGridLookUpEdit.Properties.PopupView = this.gridView5;
            this.intRootHeaveGridLookUpEdit.Properties.Tag = "";
            this.intRootHeaveGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intRootHeaveGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intRootHeaveGridLookUpEdit.Size = new System.Drawing.Size(900, 22);
            this.intRootHeaveGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intRootHeaveGridLookUpEdit.TabIndex = 19;
            this.intRootHeaveGridLookUpEdit.TabStop = false;
            this.intRootHeaveGridLookUpEdit.Tag = "133";
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.gridColumn17;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridView5.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsCustomization.AllowFilter = false;
            this.gridView5.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowFilterEditor = false;
            this.gridView5.OptionsFilter.AllowMRUFilterList = false;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn18, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Picklist Type";
            this.gridColumn13.FieldName = "HeaderDescription";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 189;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Picklist ID";
            this.gridColumn14.FieldName = "HeaderID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 67;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Item ID";
            this.gridColumn15.FieldName = "ItemCode";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 58;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Description";
            this.gridColumn16.FieldName = "ItemDescription";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            this.gridColumn16.Width = 264;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Order";
            this.gridColumn18.FieldName = "Order";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 65;
            // 
            // intRootHeave2GridLookUpEdit
            // 
            this.intRootHeave2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intRootHeave2", true));
            this.intRootHeave2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intRootHeave2GridLookUpEdit.Location = new System.Drawing.Point(162, 340);
            this.intRootHeave2GridLookUpEdit.MenuManager = this.barManager1;
            this.intRootHeave2GridLookUpEdit.Name = "intRootHeave2GridLookUpEdit";
            editorButtonImageOptions19.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions20.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intRootHeave2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions19, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject73, serializableAppearanceObject74, serializableAppearanceObject75, serializableAppearanceObject76, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions20, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject77, serializableAppearanceObject78, serializableAppearanceObject79, serializableAppearanceObject80, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intRootHeave2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intRootHeave2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intRootHeave2GridLookUpEdit.Properties.NullText = "";
            this.intRootHeave2GridLookUpEdit.Properties.PopupView = this.gridView6;
            this.intRootHeave2GridLookUpEdit.Properties.Tag = "";
            this.intRootHeave2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intRootHeave2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intRootHeave2GridLookUpEdit.Size = new System.Drawing.Size(900, 22);
            this.intRootHeave2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intRootHeave2GridLookUpEdit.TabIndex = 37;
            this.intRootHeave2GridLookUpEdit.TabStop = false;
            this.intRootHeave2GridLookUpEdit.Tag = "133";
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.gridColumn23;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView6.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView6.OptionsCustomization.AllowFilter = false;
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn24, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Picklist Type";
            this.gridColumn19.FieldName = "HeaderDescription";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 189;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Picklist ID";
            this.gridColumn20.FieldName = "HeaderID";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 67;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Item ID";
            this.gridColumn21.FieldName = "ItemCode";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 58;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Description";
            this.gridColumn22.FieldName = "ItemDescription";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 0;
            this.gridColumn22.Width = 264;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Order";
            this.gridColumn24.FieldName = "Order";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 65;
            // 
            // intRootHeave3GridLookUpEdit
            // 
            this.intRootHeave3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intRootHeave3", true));
            this.intRootHeave3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intRootHeave3GridLookUpEdit.Location = new System.Drawing.Point(162, 366);
            this.intRootHeave3GridLookUpEdit.MenuManager = this.barManager1;
            this.intRootHeave3GridLookUpEdit.Name = "intRootHeave3GridLookUpEdit";
            editorButtonImageOptions21.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions22.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intRootHeave3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions21, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject81, serializableAppearanceObject82, serializableAppearanceObject83, serializableAppearanceObject84, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions22, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject85, serializableAppearanceObject86, serializableAppearanceObject87, serializableAppearanceObject88, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intRootHeave3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intRootHeave3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intRootHeave3GridLookUpEdit.Properties.NullText = "";
            this.intRootHeave3GridLookUpEdit.Properties.PopupView = this.gridView7;
            this.intRootHeave3GridLookUpEdit.Properties.Tag = "";
            this.intRootHeave3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intRootHeave3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intRootHeave3GridLookUpEdit.Size = new System.Drawing.Size(900, 22);
            this.intRootHeave3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intRootHeave3GridLookUpEdit.TabIndex = 38;
            this.intRootHeave3GridLookUpEdit.TabStop = false;
            this.intRootHeave3GridLookUpEdit.Tag = "133";
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition7.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition7.Appearance.Options.UseForeColor = true;
            styleFormatCondition7.ApplyToRow = true;
            styleFormatCondition7.Column = this.gridColumn29;
            styleFormatCondition7.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition7.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition7});
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView7.OptionsCustomization.AllowFilter = false;
            this.gridView7.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView7.OptionsFilter.AllowFilterEditor = false;
            this.gridView7.OptionsFilter.AllowMRUFilterList = false;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn30, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Picklist Type";
            this.gridColumn25.FieldName = "HeaderDescription";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Width = 189;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Picklist ID";
            this.gridColumn26.FieldName = "HeaderID";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Width = 67;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Item ID";
            this.gridColumn27.FieldName = "ItemCode";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Width = 58;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Description";
            this.gridColumn28.FieldName = "ItemDescription";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 0;
            this.gridColumn28.Width = 264;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Order";
            this.gridColumn30.FieldName = "Order";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Width = 65;
            // 
            // intStemPhysicalGridLookUpEdit
            // 
            this.intStemPhysicalGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intStemPhysical", true));
            this.intStemPhysicalGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intStemPhysicalGridLookUpEdit.Location = new System.Drawing.Point(162, 355);
            this.intStemPhysicalGridLookUpEdit.MenuManager = this.barManager1;
            this.intStemPhysicalGridLookUpEdit.Name = "intStemPhysicalGridLookUpEdit";
            editorButtonImageOptions23.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions24.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intStemPhysicalGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions23, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject89, serializableAppearanceObject90, serializableAppearanceObject91, serializableAppearanceObject92, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions24, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject93, serializableAppearanceObject94, serializableAppearanceObject95, serializableAppearanceObject96, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intStemPhysicalGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intStemPhysicalGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intStemPhysicalGridLookUpEdit.Properties.NullText = "";
            this.intStemPhysicalGridLookUpEdit.Properties.PopupView = this.gridView8;
            this.intStemPhysicalGridLookUpEdit.Properties.Tag = "";
            this.intStemPhysicalGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intStemPhysicalGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intStemPhysicalGridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intStemPhysicalGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intStemPhysicalGridLookUpEdit.TabIndex = 11;
            this.intStemPhysicalGridLookUpEdit.TabStop = false;
            this.intStemPhysicalGridLookUpEdit.Tag = "105";
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition8.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition8.Appearance.Options.UseForeColor = true;
            styleFormatCondition8.ApplyToRow = true;
            styleFormatCondition8.Column = this.gridColumn35;
            styleFormatCondition8.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition8.Value1 = 0;
            this.gridView8.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition8});
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView8.OptionsCustomization.AllowFilter = false;
            this.gridView8.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView8.OptionsFilter.AllowFilterEditor = false;
            this.gridView8.OptionsFilter.AllowMRUFilterList = false;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn36, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Picklist Type";
            this.gridColumn31.FieldName = "HeaderDescription";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Width = 189;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Picklist ID";
            this.gridColumn32.FieldName = "HeaderID";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 67;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Item ID";
            this.gridColumn33.FieldName = "ItemCode";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 58;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Description";
            this.gridColumn34.FieldName = "ItemDescription";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 0;
            this.gridColumn34.Width = 264;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Order";
            this.gridColumn36.FieldName = "Order";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 65;
            // 
            // intStemPhysical2GridLookUpEdit
            // 
            this.intStemPhysical2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intStemPhysical2", true));
            this.intStemPhysical2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intStemPhysical2GridLookUpEdit.Location = new System.Drawing.Point(162, 381);
            this.intStemPhysical2GridLookUpEdit.MenuManager = this.barManager1;
            this.intStemPhysical2GridLookUpEdit.Name = "intStemPhysical2GridLookUpEdit";
            editorButtonImageOptions25.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions26.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intStemPhysical2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions25, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject97, serializableAppearanceObject98, serializableAppearanceObject99, serializableAppearanceObject100, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions26, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject101, serializableAppearanceObject102, serializableAppearanceObject103, serializableAppearanceObject104, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intStemPhysical2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intStemPhysical2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intStemPhysical2GridLookUpEdit.Properties.NullText = "";
            this.intStemPhysical2GridLookUpEdit.Properties.PopupView = this.gridView9;
            this.intStemPhysical2GridLookUpEdit.Properties.Tag = "";
            this.intStemPhysical2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intStemPhysical2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intStemPhysical2GridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intStemPhysical2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intStemPhysical2GridLookUpEdit.TabIndex = 27;
            this.intStemPhysical2GridLookUpEdit.TabStop = false;
            this.intStemPhysical2GridLookUpEdit.Tag = "105";
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42});
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition9.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition9.Appearance.Options.UseForeColor = true;
            styleFormatCondition9.ApplyToRow = true;
            styleFormatCondition9.Column = this.gridColumn41;
            styleFormatCondition9.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition9.Value1 = 0;
            this.gridView9.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition9});
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView9.OptionsCustomization.AllowFilter = false;
            this.gridView9.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView9.OptionsFilter.AllowFilterEditor = false;
            this.gridView9.OptionsFilter.AllowMRUFilterList = false;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView9.OptionsView.ShowIndicator = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn42, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Picklist Type";
            this.gridColumn37.FieldName = "HeaderDescription";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Width = 189;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Picklist ID";
            this.gridColumn38.FieldName = "HeaderID";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 67;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Item ID";
            this.gridColumn39.FieldName = "ItemCode";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 58;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Description";
            this.gridColumn40.FieldName = "ItemDescription";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 0;
            this.gridColumn40.Width = 264;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Order";
            this.gridColumn42.FieldName = "Order";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Width = 65;
            // 
            // intStemPhysical3GridLookUpEdit
            // 
            this.intStemPhysical3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intStemPhysical3", true));
            this.intStemPhysical3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intStemPhysical3GridLookUpEdit.Location = new System.Drawing.Point(162, 407);
            this.intStemPhysical3GridLookUpEdit.MenuManager = this.barManager1;
            this.intStemPhysical3GridLookUpEdit.Name = "intStemPhysical3GridLookUpEdit";
            editorButtonImageOptions27.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions28.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intStemPhysical3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions27, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject105, serializableAppearanceObject106, serializableAppearanceObject107, serializableAppearanceObject108, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions28, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject109, serializableAppearanceObject110, serializableAppearanceObject111, serializableAppearanceObject112, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intStemPhysical3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intStemPhysical3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intStemPhysical3GridLookUpEdit.Properties.NullText = "";
            this.intStemPhysical3GridLookUpEdit.Properties.PopupView = this.gridView10;
            this.intStemPhysical3GridLookUpEdit.Properties.Tag = "";
            this.intStemPhysical3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intStemPhysical3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intStemPhysical3GridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intStemPhysical3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intStemPhysical3GridLookUpEdit.TabIndex = 32;
            this.intStemPhysical3GridLookUpEdit.TabStop = false;
            this.intStemPhysical3GridLookUpEdit.Tag = "105";
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48});
            this.gridView10.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition10.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition10.Appearance.Options.UseForeColor = true;
            styleFormatCondition10.ApplyToRow = true;
            styleFormatCondition10.Column = this.gridColumn47;
            styleFormatCondition10.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition10.Value1 = 0;
            this.gridView10.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition10});
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView10.OptionsCustomization.AllowFilter = false;
            this.gridView10.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView10.OptionsFilter.AllowFilterEditor = false;
            this.gridView10.OptionsFilter.AllowMRUFilterList = false;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView10.OptionsView.ShowIndicator = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn48, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Picklist Type";
            this.gridColumn43.FieldName = "HeaderDescription";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Width = 189;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Picklist ID";
            this.gridColumn44.FieldName = "HeaderID";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Width = 67;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Item ID";
            this.gridColumn45.FieldName = "ItemCode";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Width = 58;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Description";
            this.gridColumn46.FieldName = "ItemDescription";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 0;
            this.gridColumn46.Width = 264;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Order";
            this.gridColumn48.FieldName = "Order";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Width = 65;
            // 
            // intStemDiseaseGridLookUpEdit
            // 
            this.intStemDiseaseGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intStemDisease", true));
            this.intStemDiseaseGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intStemDiseaseGridLookUpEdit.Location = new System.Drawing.Point(162, 443);
            this.intStemDiseaseGridLookUpEdit.MenuManager = this.barManager1;
            this.intStemDiseaseGridLookUpEdit.Name = "intStemDiseaseGridLookUpEdit";
            editorButtonImageOptions29.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions30.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intStemDiseaseGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions29, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject113, serializableAppearanceObject114, serializableAppearanceObject115, serializableAppearanceObject116, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions30, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject117, serializableAppearanceObject118, serializableAppearanceObject119, serializableAppearanceObject120, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intStemDiseaseGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intStemDiseaseGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intStemDiseaseGridLookUpEdit.Properties.NullText = "";
            this.intStemDiseaseGridLookUpEdit.Properties.PopupView = this.gridView11;
            this.intStemDiseaseGridLookUpEdit.Properties.Tag = "";
            this.intStemDiseaseGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intStemDiseaseGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intStemDiseaseGridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intStemDiseaseGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intStemDiseaseGridLookUpEdit.TabIndex = 12;
            this.intStemDiseaseGridLookUpEdit.TabStop = false;
            this.intStemDiseaseGridLookUpEdit.Tag = "106";
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54});
            this.gridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition11.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition11.Appearance.Options.UseForeColor = true;
            styleFormatCondition11.ApplyToRow = true;
            styleFormatCondition11.Column = this.gridColumn53;
            styleFormatCondition11.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition11.Value1 = 0;
            this.gridView11.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition11});
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView11.OptionsCustomization.AllowFilter = false;
            this.gridView11.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView11.OptionsFilter.AllowFilterEditor = false;
            this.gridView11.OptionsFilter.AllowMRUFilterList = false;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView11.OptionsView.ShowIndicator = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn54, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Picklist Type";
            this.gridColumn49.FieldName = "HeaderDescription";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Width = 189;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Picklist ID";
            this.gridColumn50.FieldName = "HeaderID";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Width = 67;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Item ID";
            this.gridColumn51.FieldName = "ItemCode";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Width = 58;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Description";
            this.gridColumn52.FieldName = "ItemDescription";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 0;
            this.gridColumn52.Width = 264;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Order";
            this.gridColumn54.FieldName = "Order";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.AllowEdit = false;
            this.gridColumn54.OptionsColumn.AllowFocus = false;
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 65;
            // 
            // intStemDisease2GridLookUpEdit
            // 
            this.intStemDisease2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intStemDisease2", true));
            this.intStemDisease2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intStemDisease2GridLookUpEdit.Location = new System.Drawing.Point(162, 468);
            this.intStemDisease2GridLookUpEdit.MenuManager = this.barManager1;
            this.intStemDisease2GridLookUpEdit.Name = "intStemDisease2GridLookUpEdit";
            editorButtonImageOptions31.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions32.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intStemDisease2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions31, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject121, serializableAppearanceObject122, serializableAppearanceObject123, serializableAppearanceObject124, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions32, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject125, serializableAppearanceObject126, serializableAppearanceObject127, serializableAppearanceObject128, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intStemDisease2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intStemDisease2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intStemDisease2GridLookUpEdit.Properties.NullText = "";
            this.intStemDisease2GridLookUpEdit.Properties.PopupView = this.gridView12;
            this.intStemDisease2GridLookUpEdit.Properties.Tag = "";
            this.intStemDisease2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intStemDisease2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intStemDisease2GridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intStemDisease2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intStemDisease2GridLookUpEdit.TabIndex = 28;
            this.intStemDisease2GridLookUpEdit.TabStop = false;
            this.intStemDisease2GridLookUpEdit.Tag = "106";
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60});
            this.gridView12.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition12.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition12.Appearance.Options.UseForeColor = true;
            styleFormatCondition12.ApplyToRow = true;
            styleFormatCondition12.Column = this.gridColumn59;
            styleFormatCondition12.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition12.Value1 = 0;
            this.gridView12.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition12});
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView12.OptionsCustomization.AllowFilter = false;
            this.gridView12.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView12.OptionsFilter.AllowFilterEditor = false;
            this.gridView12.OptionsFilter.AllowMRUFilterList = false;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView12.OptionsView.ShowIndicator = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn60, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Picklist Type";
            this.gridColumn55.FieldName = "HeaderDescription";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.AllowEdit = false;
            this.gridColumn55.OptionsColumn.AllowFocus = false;
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Width = 189;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Picklist ID";
            this.gridColumn56.FieldName = "HeaderID";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Width = 67;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Item ID";
            this.gridColumn57.FieldName = "ItemCode";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Width = 58;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Description";
            this.gridColumn58.FieldName = "ItemDescription";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 0;
            this.gridColumn58.Width = 264;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Order";
            this.gridColumn60.FieldName = "Order";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Width = 65;
            // 
            // intStemDisease3GridLookUpEdit
            // 
            this.intStemDisease3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intStemDisease3", true));
            this.intStemDisease3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intStemDisease3GridLookUpEdit.Location = new System.Drawing.Point(162, 494);
            this.intStemDisease3GridLookUpEdit.MenuManager = this.barManager1;
            this.intStemDisease3GridLookUpEdit.Name = "intStemDisease3GridLookUpEdit";
            editorButtonImageOptions33.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions34.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intStemDisease3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions33, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject129, serializableAppearanceObject130, serializableAppearanceObject131, serializableAppearanceObject132, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions34, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject133, serializableAppearanceObject134, serializableAppearanceObject135, serializableAppearanceObject136, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intStemDisease3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intStemDisease3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intStemDisease3GridLookUpEdit.Properties.NullText = "";
            this.intStemDisease3GridLookUpEdit.Properties.PopupView = this.gridView13;
            this.intStemDisease3GridLookUpEdit.Properties.Tag = "";
            this.intStemDisease3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intStemDisease3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intStemDisease3GridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intStemDisease3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intStemDisease3GridLookUpEdit.TabIndex = 33;
            this.intStemDisease3GridLookUpEdit.TabStop = false;
            this.intStemDisease3GridLookUpEdit.Tag = "106";
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65,
            this.gridColumn66});
            this.gridView13.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition13.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition13.Appearance.Options.UseForeColor = true;
            styleFormatCondition13.ApplyToRow = true;
            styleFormatCondition13.Column = this.gridColumn65;
            styleFormatCondition13.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition13.Value1 = 0;
            this.gridView13.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition13});
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView13.OptionsCustomization.AllowFilter = false;
            this.gridView13.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView13.OptionsFilter.AllowFilterEditor = false;
            this.gridView13.OptionsFilter.AllowMRUFilterList = false;
            this.gridView13.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView13.OptionsLayout.StoreAppearance = true;
            this.gridView13.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView13.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView13.OptionsView.ColumnAutoWidth = false;
            this.gridView13.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView13.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            this.gridView13.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView13.OptionsView.ShowIndicator = false;
            this.gridView13.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn66, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Picklist Type";
            this.gridColumn61.FieldName = "HeaderDescription";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            this.gridColumn61.Width = 189;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Picklist ID";
            this.gridColumn62.FieldName = "HeaderID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.Width = 67;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Item ID";
            this.gridColumn63.FieldName = "ItemCode";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.Width = 58;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Description";
            this.gridColumn64.FieldName = "ItemDescription";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.Visible = true;
            this.gridColumn64.VisibleIndex = 0;
            this.gridColumn64.Width = 264;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Order";
            this.gridColumn66.FieldName = "Order";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            this.gridColumn66.Width = 65;
            // 
            // intCrownPhysicalGridLookUpEdit
            // 
            this.intCrownPhysicalGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intCrownPhysical", true));
            this.intCrownPhysicalGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownPhysicalGridLookUpEdit.Location = new System.Drawing.Point(162, 396);
            this.intCrownPhysicalGridLookUpEdit.MenuManager = this.barManager1;
            this.intCrownPhysicalGridLookUpEdit.Name = "intCrownPhysicalGridLookUpEdit";
            editorButtonImageOptions35.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions36.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCrownPhysicalGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions35, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject137, serializableAppearanceObject138, serializableAppearanceObject139, serializableAppearanceObject140, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions36, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject141, serializableAppearanceObject142, serializableAppearanceObject143, serializableAppearanceObject144, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCrownPhysicalGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intCrownPhysicalGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intCrownPhysicalGridLookUpEdit.Properties.NullText = "";
            this.intCrownPhysicalGridLookUpEdit.Properties.PopupView = this.gridView14;
            this.intCrownPhysicalGridLookUpEdit.Properties.Tag = "";
            this.intCrownPhysicalGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCrownPhysicalGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intCrownPhysicalGridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intCrownPhysicalGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCrownPhysicalGridLookUpEdit.TabIndex = 14;
            this.intCrownPhysicalGridLookUpEdit.TabStop = false;
            this.intCrownPhysicalGridLookUpEdit.Tag = "107";
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn71,
            this.gridColumn72});
            this.gridView14.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition14.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition14.Appearance.Options.UseForeColor = true;
            styleFormatCondition14.ApplyToRow = true;
            styleFormatCondition14.Column = this.gridColumn71;
            styleFormatCondition14.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition14.Value1 = 0;
            this.gridView14.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition14});
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView14.OptionsCustomization.AllowFilter = false;
            this.gridView14.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView14.OptionsFilter.AllowFilterEditor = false;
            this.gridView14.OptionsFilter.AllowMRUFilterList = false;
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView14.OptionsView.ShowIndicator = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn72, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "Picklist Type";
            this.gridColumn67.FieldName = "HeaderDescription";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            this.gridColumn67.Width = 189;
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "Picklist ID";
            this.gridColumn68.FieldName = "HeaderID";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            this.gridColumn68.Width = 67;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Item ID";
            this.gridColumn69.FieldName = "ItemCode";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Width = 58;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Description";
            this.gridColumn70.FieldName = "ItemDescription";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            this.gridColumn70.Visible = true;
            this.gridColumn70.VisibleIndex = 0;
            this.gridColumn70.Width = 264;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Order";
            this.gridColumn72.FieldName = "Order";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Width = 65;
            // 
            // intCrownPhysical2GridLookUpEdit
            // 
            this.intCrownPhysical2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intCrownPhysical2", true));
            this.intCrownPhysical2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownPhysical2GridLookUpEdit.Location = new System.Drawing.Point(162, 422);
            this.intCrownPhysical2GridLookUpEdit.MenuManager = this.barManager1;
            this.intCrownPhysical2GridLookUpEdit.Name = "intCrownPhysical2GridLookUpEdit";
            editorButtonImageOptions37.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions38.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCrownPhysical2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions37, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject145, serializableAppearanceObject146, serializableAppearanceObject147, serializableAppearanceObject148, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions38, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject149, serializableAppearanceObject150, serializableAppearanceObject151, serializableAppearanceObject152, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCrownPhysical2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intCrownPhysical2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intCrownPhysical2GridLookUpEdit.Properties.NullText = "";
            this.intCrownPhysical2GridLookUpEdit.Properties.PopupView = this.gridView15;
            this.intCrownPhysical2GridLookUpEdit.Properties.Tag = "";
            this.intCrownPhysical2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCrownPhysical2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intCrownPhysical2GridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intCrownPhysical2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCrownPhysical2GridLookUpEdit.TabIndex = 29;
            this.intCrownPhysical2GridLookUpEdit.TabStop = false;
            this.intCrownPhysical2GridLookUpEdit.Tag = "107";
            // 
            // gridView15
            // 
            this.gridView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75,
            this.gridColumn76,
            this.gridColumn77,
            this.gridColumn78});
            this.gridView15.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition15.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition15.Appearance.Options.UseForeColor = true;
            styleFormatCondition15.ApplyToRow = true;
            styleFormatCondition15.Column = this.gridColumn77;
            styleFormatCondition15.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition15.Value1 = 0;
            this.gridView15.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition15});
            this.gridView15.Name = "gridView15";
            this.gridView15.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView15.OptionsCustomization.AllowFilter = false;
            this.gridView15.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView15.OptionsFilter.AllowFilterEditor = false;
            this.gridView15.OptionsFilter.AllowMRUFilterList = false;
            this.gridView15.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView15.OptionsLayout.StoreAppearance = true;
            this.gridView15.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView15.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView15.OptionsView.ColumnAutoWidth = false;
            this.gridView15.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView15.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView15.OptionsView.ShowGroupPanel = false;
            this.gridView15.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView15.OptionsView.ShowIndicator = false;
            this.gridView15.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn78, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Picklist Type";
            this.gridColumn73.FieldName = "HeaderDescription";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Width = 189;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Picklist ID";
            this.gridColumn74.FieldName = "HeaderID";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Width = 67;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Item ID";
            this.gridColumn75.FieldName = "ItemCode";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowEdit = false;
            this.gridColumn75.OptionsColumn.AllowFocus = false;
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Width = 58;
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Description";
            this.gridColumn76.FieldName = "ItemDescription";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.Visible = true;
            this.gridColumn76.VisibleIndex = 0;
            this.gridColumn76.Width = 264;
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "Order";
            this.gridColumn78.FieldName = "Order";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            this.gridColumn78.Width = 65;
            // 
            // intCrownPhysical3GridLookUpEdit
            // 
            this.intCrownPhysical3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intCrownPhysical3", true));
            this.intCrownPhysical3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownPhysical3GridLookUpEdit.Location = new System.Drawing.Point(162, 448);
            this.intCrownPhysical3GridLookUpEdit.MenuManager = this.barManager1;
            this.intCrownPhysical3GridLookUpEdit.Name = "intCrownPhysical3GridLookUpEdit";
            editorButtonImageOptions39.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions40.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCrownPhysical3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions39, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject153, serializableAppearanceObject154, serializableAppearanceObject155, serializableAppearanceObject156, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions40, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject157, serializableAppearanceObject158, serializableAppearanceObject159, serializableAppearanceObject160, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCrownPhysical3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intCrownPhysical3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intCrownPhysical3GridLookUpEdit.Properties.NullText = "";
            this.intCrownPhysical3GridLookUpEdit.Properties.PopupView = this.gridView16;
            this.intCrownPhysical3GridLookUpEdit.Properties.Tag = "";
            this.intCrownPhysical3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCrownPhysical3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intCrownPhysical3GridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intCrownPhysical3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCrownPhysical3GridLookUpEdit.TabIndex = 34;
            this.intCrownPhysical3GridLookUpEdit.TabStop = false;
            this.intCrownPhysical3GridLookUpEdit.Tag = "107";
            // 
            // gridView16
            // 
            this.gridView16.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81,
            this.gridColumn82,
            this.gridColumn83,
            this.gridColumn84});
            this.gridView16.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition16.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition16.Appearance.Options.UseForeColor = true;
            styleFormatCondition16.ApplyToRow = true;
            styleFormatCondition16.Column = this.gridColumn83;
            styleFormatCondition16.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition16.Value1 = 0;
            this.gridView16.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition16});
            this.gridView16.Name = "gridView16";
            this.gridView16.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView16.OptionsCustomization.AllowFilter = false;
            this.gridView16.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView16.OptionsFilter.AllowFilterEditor = false;
            this.gridView16.OptionsFilter.AllowMRUFilterList = false;
            this.gridView16.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView16.OptionsLayout.StoreAppearance = true;
            this.gridView16.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView16.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView16.OptionsView.ColumnAutoWidth = false;
            this.gridView16.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView16.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView16.OptionsView.ShowGroupPanel = false;
            this.gridView16.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView16.OptionsView.ShowIndicator = false;
            this.gridView16.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn84, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "Picklist Type";
            this.gridColumn79.FieldName = "HeaderDescription";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            this.gridColumn79.Width = 189;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Picklist ID";
            this.gridColumn80.FieldName = "HeaderID";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            this.gridColumn80.Width = 67;
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Item ID";
            this.gridColumn81.FieldName = "ItemCode";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            this.gridColumn81.Width = 58;
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Description";
            this.gridColumn82.FieldName = "ItemDescription";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            this.gridColumn82.Visible = true;
            this.gridColumn82.VisibleIndex = 0;
            this.gridColumn82.Width = 264;
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Order";
            this.gridColumn84.FieldName = "Order";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.Width = 65;
            // 
            // intCrownDiseaseGridLookUpEdit
            // 
            this.intCrownDiseaseGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intCrownDisease", true));
            this.intCrownDiseaseGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownDiseaseGridLookUpEdit.Location = new System.Drawing.Point(162, 484);
            this.intCrownDiseaseGridLookUpEdit.MenuManager = this.barManager1;
            this.intCrownDiseaseGridLookUpEdit.Name = "intCrownDiseaseGridLookUpEdit";
            editorButtonImageOptions41.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions42.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCrownDiseaseGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions41, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject161, serializableAppearanceObject162, serializableAppearanceObject163, serializableAppearanceObject164, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions42, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject165, serializableAppearanceObject166, serializableAppearanceObject167, serializableAppearanceObject168, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCrownDiseaseGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intCrownDiseaseGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intCrownDiseaseGridLookUpEdit.Properties.NullText = "";
            this.intCrownDiseaseGridLookUpEdit.Properties.PopupView = this.gridView17;
            this.intCrownDiseaseGridLookUpEdit.Properties.Tag = "";
            this.intCrownDiseaseGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCrownDiseaseGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intCrownDiseaseGridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intCrownDiseaseGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCrownDiseaseGridLookUpEdit.TabIndex = 15;
            this.intCrownDiseaseGridLookUpEdit.TabStop = false;
            this.intCrownDiseaseGridLookUpEdit.Tag = "108";
            // 
            // gridView17
            // 
            this.gridView17.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn85,
            this.gridColumn86,
            this.gridColumn87,
            this.gridColumn88,
            this.gridColumn89,
            this.gridColumn90});
            this.gridView17.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition17.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition17.Appearance.Options.UseForeColor = true;
            styleFormatCondition17.ApplyToRow = true;
            styleFormatCondition17.Column = this.gridColumn89;
            styleFormatCondition17.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition17.Value1 = 0;
            this.gridView17.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition17});
            this.gridView17.Name = "gridView17";
            this.gridView17.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView17.OptionsCustomization.AllowFilter = false;
            this.gridView17.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView17.OptionsFilter.AllowFilterEditor = false;
            this.gridView17.OptionsFilter.AllowMRUFilterList = false;
            this.gridView17.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView17.OptionsLayout.StoreAppearance = true;
            this.gridView17.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView17.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView17.OptionsView.ColumnAutoWidth = false;
            this.gridView17.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView17.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView17.OptionsView.ShowGroupPanel = false;
            this.gridView17.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView17.OptionsView.ShowIndicator = false;
            this.gridView17.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn90, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Picklist Type";
            this.gridColumn85.FieldName = "HeaderDescription";
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.Width = 189;
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Picklist ID";
            this.gridColumn86.FieldName = "HeaderID";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            this.gridColumn86.Width = 67;
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Item ID";
            this.gridColumn87.FieldName = "ItemCode";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.AllowEdit = false;
            this.gridColumn87.OptionsColumn.AllowFocus = false;
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.Width = 58;
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Description";
            this.gridColumn88.FieldName = "ItemDescription";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.AllowEdit = false;
            this.gridColumn88.OptionsColumn.AllowFocus = false;
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Visible = true;
            this.gridColumn88.VisibleIndex = 0;
            this.gridColumn88.Width = 264;
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Order";
            this.gridColumn90.FieldName = "Order";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Width = 65;
            // 
            // intCrownDisease2GridLookUpEdit
            // 
            this.intCrownDisease2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intCrownDisease2", true));
            this.intCrownDisease2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownDisease2GridLookUpEdit.Location = new System.Drawing.Point(162, 510);
            this.intCrownDisease2GridLookUpEdit.MenuManager = this.barManager1;
            this.intCrownDisease2GridLookUpEdit.Name = "intCrownDisease2GridLookUpEdit";
            editorButtonImageOptions43.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions44.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCrownDisease2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions43, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject169, serializableAppearanceObject170, serializableAppearanceObject171, serializableAppearanceObject172, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions44, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject173, serializableAppearanceObject174, serializableAppearanceObject175, serializableAppearanceObject176, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCrownDisease2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intCrownDisease2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intCrownDisease2GridLookUpEdit.Properties.NullText = "";
            this.intCrownDisease2GridLookUpEdit.Properties.PopupView = this.gridView18;
            this.intCrownDisease2GridLookUpEdit.Properties.Tag = "";
            this.intCrownDisease2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCrownDisease2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intCrownDisease2GridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intCrownDisease2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCrownDisease2GridLookUpEdit.TabIndex = 30;
            this.intCrownDisease2GridLookUpEdit.TabStop = false;
            this.intCrownDisease2GridLookUpEdit.Tag = "108";
            // 
            // gridView18
            // 
            this.gridView18.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn91,
            this.gridColumn92,
            this.gridColumn93,
            this.gridColumn94,
            this.gridColumn95,
            this.gridColumn96});
            this.gridView18.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition18.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition18.Appearance.Options.UseForeColor = true;
            styleFormatCondition18.ApplyToRow = true;
            styleFormatCondition18.Column = this.gridColumn95;
            styleFormatCondition18.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition18.Value1 = 0;
            this.gridView18.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition18});
            this.gridView18.Name = "gridView18";
            this.gridView18.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView18.OptionsCustomization.AllowFilter = false;
            this.gridView18.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView18.OptionsFilter.AllowFilterEditor = false;
            this.gridView18.OptionsFilter.AllowMRUFilterList = false;
            this.gridView18.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView18.OptionsLayout.StoreAppearance = true;
            this.gridView18.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView18.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView18.OptionsView.ColumnAutoWidth = false;
            this.gridView18.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView18.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView18.OptionsView.ShowGroupPanel = false;
            this.gridView18.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView18.OptionsView.ShowIndicator = false;
            this.gridView18.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn96, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "Picklist Type";
            this.gridColumn91.FieldName = "HeaderDescription";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            this.gridColumn91.Width = 189;
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "Picklist ID";
            this.gridColumn92.FieldName = "HeaderID";
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            this.gridColumn92.Width = 67;
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "Item ID";
            this.gridColumn93.FieldName = "ItemCode";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.Width = 58;
            // 
            // gridColumn94
            // 
            this.gridColumn94.Caption = "Description";
            this.gridColumn94.FieldName = "ItemDescription";
            this.gridColumn94.Name = "gridColumn94";
            this.gridColumn94.OptionsColumn.AllowEdit = false;
            this.gridColumn94.OptionsColumn.AllowFocus = false;
            this.gridColumn94.OptionsColumn.ReadOnly = true;
            this.gridColumn94.Visible = true;
            this.gridColumn94.VisibleIndex = 0;
            this.gridColumn94.Width = 264;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Order";
            this.gridColumn96.FieldName = "Order";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            this.gridColumn96.Width = 65;
            // 
            // intCrownDisease3GridLookUpEdit
            // 
            this.intCrownDisease3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intCrownDisease3", true));
            this.intCrownDisease3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownDisease3GridLookUpEdit.Location = new System.Drawing.Point(162, 536);
            this.intCrownDisease3GridLookUpEdit.MenuManager = this.barManager1;
            this.intCrownDisease3GridLookUpEdit.Name = "intCrownDisease3GridLookUpEdit";
            editorButtonImageOptions45.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions46.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCrownDisease3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions45, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject177, serializableAppearanceObject178, serializableAppearanceObject179, serializableAppearanceObject180, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions46, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject181, serializableAppearanceObject182, serializableAppearanceObject183, serializableAppearanceObject184, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCrownDisease3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intCrownDisease3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intCrownDisease3GridLookUpEdit.Properties.NullText = "";
            this.intCrownDisease3GridLookUpEdit.Properties.PopupView = this.gridView19;
            this.intCrownDisease3GridLookUpEdit.Properties.Tag = "";
            this.intCrownDisease3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCrownDisease3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intCrownDisease3GridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intCrownDisease3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCrownDisease3GridLookUpEdit.TabIndex = 35;
            this.intCrownDisease3GridLookUpEdit.TabStop = false;
            this.intCrownDisease3GridLookUpEdit.Tag = "108";
            // 
            // gridView19
            // 
            this.gridView19.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99,
            this.gridColumn100,
            this.gridColumn101,
            this.gridColumn102});
            this.gridView19.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition19.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition19.Appearance.Options.UseForeColor = true;
            styleFormatCondition19.ApplyToRow = true;
            styleFormatCondition19.Column = this.gridColumn101;
            styleFormatCondition19.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition19.Value1 = 0;
            this.gridView19.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition19});
            this.gridView19.Name = "gridView19";
            this.gridView19.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView19.OptionsCustomization.AllowFilter = false;
            this.gridView19.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView19.OptionsFilter.AllowFilterEditor = false;
            this.gridView19.OptionsFilter.AllowMRUFilterList = false;
            this.gridView19.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView19.OptionsLayout.StoreAppearance = true;
            this.gridView19.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView19.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView19.OptionsView.ColumnAutoWidth = false;
            this.gridView19.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView19.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView19.OptionsView.ShowGroupPanel = false;
            this.gridView19.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView19.OptionsView.ShowIndicator = false;
            this.gridView19.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn102, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "Picklist Type";
            this.gridColumn97.FieldName = "HeaderDescription";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.AllowEdit = false;
            this.gridColumn97.OptionsColumn.AllowFocus = false;
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            this.gridColumn97.Width = 189;
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Picklist ID";
            this.gridColumn98.FieldName = "HeaderID";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Width = 67;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Item ID";
            this.gridColumn99.FieldName = "ItemCode";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.AllowEdit = false;
            this.gridColumn99.OptionsColumn.AllowFocus = false;
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            this.gridColumn99.Width = 58;
            // 
            // gridColumn100
            // 
            this.gridColumn100.Caption = "Description";
            this.gridColumn100.FieldName = "ItemDescription";
            this.gridColumn100.Name = "gridColumn100";
            this.gridColumn100.OptionsColumn.AllowEdit = false;
            this.gridColumn100.OptionsColumn.AllowFocus = false;
            this.gridColumn100.OptionsColumn.ReadOnly = true;
            this.gridColumn100.Visible = true;
            this.gridColumn100.VisibleIndex = 0;
            this.gridColumn100.Width = 264;
            // 
            // gridColumn102
            // 
            this.gridColumn102.Caption = "Order";
            this.gridColumn102.FieldName = "Order";
            this.gridColumn102.Name = "gridColumn102";
            this.gridColumn102.OptionsColumn.AllowEdit = false;
            this.gridColumn102.OptionsColumn.AllowFocus = false;
            this.gridColumn102.OptionsColumn.ReadOnly = true;
            this.gridColumn102.Width = 65;
            // 
            // intCrownFoliationGridLookUpEdit
            // 
            this.intCrownFoliationGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intCrownFoliation", true));
            this.intCrownFoliationGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownFoliationGridLookUpEdit.Location = new System.Drawing.Point(162, 572);
            this.intCrownFoliationGridLookUpEdit.MenuManager = this.barManager1;
            this.intCrownFoliationGridLookUpEdit.Name = "intCrownFoliationGridLookUpEdit";
            editorButtonImageOptions47.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions48.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCrownFoliationGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions47, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject185, serializableAppearanceObject186, serializableAppearanceObject187, serializableAppearanceObject188, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions48, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject189, serializableAppearanceObject190, serializableAppearanceObject191, serializableAppearanceObject192, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCrownFoliationGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intCrownFoliationGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intCrownFoliationGridLookUpEdit.Properties.NullText = "";
            this.intCrownFoliationGridLookUpEdit.Properties.PopupView = this.gridView20;
            this.intCrownFoliationGridLookUpEdit.Properties.Tag = "";
            this.intCrownFoliationGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCrownFoliationGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intCrownFoliationGridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intCrownFoliationGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCrownFoliationGridLookUpEdit.TabIndex = 16;
            this.intCrownFoliationGridLookUpEdit.TabStop = false;
            this.intCrownFoliationGridLookUpEdit.Tag = "109";
            // 
            // gridView20
            // 
            this.gridView20.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn103,
            this.gridColumn104,
            this.gridColumn105,
            this.gridColumn106,
            this.gridColumn107,
            this.gridColumn108});
            this.gridView20.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition20.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition20.Appearance.Options.UseForeColor = true;
            styleFormatCondition20.ApplyToRow = true;
            styleFormatCondition20.Column = this.gridColumn107;
            styleFormatCondition20.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition20.Value1 = 0;
            this.gridView20.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition20});
            this.gridView20.Name = "gridView20";
            this.gridView20.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView20.OptionsCustomization.AllowFilter = false;
            this.gridView20.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView20.OptionsFilter.AllowFilterEditor = false;
            this.gridView20.OptionsFilter.AllowMRUFilterList = false;
            this.gridView20.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView20.OptionsLayout.StoreAppearance = true;
            this.gridView20.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView20.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView20.OptionsView.ColumnAutoWidth = false;
            this.gridView20.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView20.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView20.OptionsView.ShowGroupPanel = false;
            this.gridView20.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView20.OptionsView.ShowIndicator = false;
            this.gridView20.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn108, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn103
            // 
            this.gridColumn103.Caption = "Picklist Type";
            this.gridColumn103.FieldName = "HeaderDescription";
            this.gridColumn103.Name = "gridColumn103";
            this.gridColumn103.OptionsColumn.AllowEdit = false;
            this.gridColumn103.OptionsColumn.AllowFocus = false;
            this.gridColumn103.OptionsColumn.ReadOnly = true;
            this.gridColumn103.Width = 189;
            // 
            // gridColumn104
            // 
            this.gridColumn104.Caption = "Picklist ID";
            this.gridColumn104.FieldName = "HeaderID";
            this.gridColumn104.Name = "gridColumn104";
            this.gridColumn104.OptionsColumn.AllowEdit = false;
            this.gridColumn104.OptionsColumn.AllowFocus = false;
            this.gridColumn104.OptionsColumn.ReadOnly = true;
            this.gridColumn104.Width = 67;
            // 
            // gridColumn105
            // 
            this.gridColumn105.Caption = "Item ID";
            this.gridColumn105.FieldName = "ItemCode";
            this.gridColumn105.Name = "gridColumn105";
            this.gridColumn105.OptionsColumn.AllowEdit = false;
            this.gridColumn105.OptionsColumn.AllowFocus = false;
            this.gridColumn105.OptionsColumn.ReadOnly = true;
            this.gridColumn105.Width = 58;
            // 
            // gridColumn106
            // 
            this.gridColumn106.Caption = "Description";
            this.gridColumn106.FieldName = "ItemDescription";
            this.gridColumn106.Name = "gridColumn106";
            this.gridColumn106.OptionsColumn.AllowEdit = false;
            this.gridColumn106.OptionsColumn.AllowFocus = false;
            this.gridColumn106.OptionsColumn.ReadOnly = true;
            this.gridColumn106.Visible = true;
            this.gridColumn106.VisibleIndex = 0;
            this.gridColumn106.Width = 264;
            // 
            // gridColumn108
            // 
            this.gridColumn108.Caption = "Order";
            this.gridColumn108.FieldName = "Order";
            this.gridColumn108.Name = "gridColumn108";
            this.gridColumn108.OptionsColumn.AllowEdit = false;
            this.gridColumn108.OptionsColumn.AllowFocus = false;
            this.gridColumn108.OptionsColumn.ReadOnly = true;
            this.gridColumn108.Width = 65;
            // 
            // intCrownFoliation2GridLookUpEdit
            // 
            this.intCrownFoliation2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intCrownFoliation2", true));
            this.intCrownFoliation2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownFoliation2GridLookUpEdit.Location = new System.Drawing.Point(162, 598);
            this.intCrownFoliation2GridLookUpEdit.MenuManager = this.barManager1;
            this.intCrownFoliation2GridLookUpEdit.Name = "intCrownFoliation2GridLookUpEdit";
            editorButtonImageOptions49.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions50.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCrownFoliation2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions49, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject193, serializableAppearanceObject194, serializableAppearanceObject195, serializableAppearanceObject196, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions50, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject197, serializableAppearanceObject198, serializableAppearanceObject199, serializableAppearanceObject200, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCrownFoliation2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intCrownFoliation2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intCrownFoliation2GridLookUpEdit.Properties.NullText = "";
            this.intCrownFoliation2GridLookUpEdit.Properties.PopupView = this.gridView21;
            this.intCrownFoliation2GridLookUpEdit.Properties.Tag = "";
            this.intCrownFoliation2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCrownFoliation2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intCrownFoliation2GridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intCrownFoliation2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCrownFoliation2GridLookUpEdit.TabIndex = 31;
            this.intCrownFoliation2GridLookUpEdit.TabStop = false;
            this.intCrownFoliation2GridLookUpEdit.Tag = "109";
            // 
            // gridView21
            // 
            this.gridView21.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn109,
            this.gridColumn110,
            this.gridColumn111,
            this.gridColumn112,
            this.gridColumn113,
            this.gridColumn114});
            this.gridView21.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition21.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition21.Appearance.Options.UseForeColor = true;
            styleFormatCondition21.ApplyToRow = true;
            styleFormatCondition21.Column = this.gridColumn113;
            styleFormatCondition21.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition21.Value1 = 0;
            this.gridView21.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition21});
            this.gridView21.Name = "gridView21";
            this.gridView21.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView21.OptionsCustomization.AllowFilter = false;
            this.gridView21.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView21.OptionsFilter.AllowFilterEditor = false;
            this.gridView21.OptionsFilter.AllowMRUFilterList = false;
            this.gridView21.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView21.OptionsLayout.StoreAppearance = true;
            this.gridView21.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView21.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView21.OptionsView.ColumnAutoWidth = false;
            this.gridView21.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView21.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView21.OptionsView.ShowGroupPanel = false;
            this.gridView21.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView21.OptionsView.ShowIndicator = false;
            this.gridView21.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn114, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn109
            // 
            this.gridColumn109.Caption = "Picklist Type";
            this.gridColumn109.FieldName = "HeaderDescription";
            this.gridColumn109.Name = "gridColumn109";
            this.gridColumn109.OptionsColumn.AllowEdit = false;
            this.gridColumn109.OptionsColumn.AllowFocus = false;
            this.gridColumn109.OptionsColumn.ReadOnly = true;
            this.gridColumn109.Width = 189;
            // 
            // gridColumn110
            // 
            this.gridColumn110.Caption = "Picklist ID";
            this.gridColumn110.FieldName = "HeaderID";
            this.gridColumn110.Name = "gridColumn110";
            this.gridColumn110.OptionsColumn.AllowEdit = false;
            this.gridColumn110.OptionsColumn.AllowFocus = false;
            this.gridColumn110.OptionsColumn.ReadOnly = true;
            this.gridColumn110.Width = 67;
            // 
            // gridColumn111
            // 
            this.gridColumn111.Caption = "Item ID";
            this.gridColumn111.FieldName = "ItemCode";
            this.gridColumn111.Name = "gridColumn111";
            this.gridColumn111.OptionsColumn.AllowEdit = false;
            this.gridColumn111.OptionsColumn.AllowFocus = false;
            this.gridColumn111.OptionsColumn.ReadOnly = true;
            this.gridColumn111.Width = 58;
            // 
            // gridColumn112
            // 
            this.gridColumn112.Caption = "Description";
            this.gridColumn112.FieldName = "ItemDescription";
            this.gridColumn112.Name = "gridColumn112";
            this.gridColumn112.OptionsColumn.AllowEdit = false;
            this.gridColumn112.OptionsColumn.AllowFocus = false;
            this.gridColumn112.OptionsColumn.ReadOnly = true;
            this.gridColumn112.Visible = true;
            this.gridColumn112.VisibleIndex = 0;
            this.gridColumn112.Width = 264;
            // 
            // gridColumn114
            // 
            this.gridColumn114.Caption = "Order";
            this.gridColumn114.FieldName = "Order";
            this.gridColumn114.Name = "gridColumn114";
            this.gridColumn114.OptionsColumn.AllowEdit = false;
            this.gridColumn114.OptionsColumn.AllowFocus = false;
            this.gridColumn114.OptionsColumn.ReadOnly = true;
            this.gridColumn114.Width = 65;
            // 
            // intCrownFoliation3GridLookUpEdit
            // 
            this.intCrownFoliation3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "intCrownFoliation3", true));
            this.intCrownFoliation3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intCrownFoliation3GridLookUpEdit.Location = new System.Drawing.Point(162, 624);
            this.intCrownFoliation3GridLookUpEdit.MenuManager = this.barManager1;
            this.intCrownFoliation3GridLookUpEdit.Name = "intCrownFoliation3GridLookUpEdit";
            editorButtonImageOptions51.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions52.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.intCrownFoliation3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions51, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject201, serializableAppearanceObject202, serializableAppearanceObject203, serializableAppearanceObject204, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions52, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject205, serializableAppearanceObject206, serializableAppearanceObject207, serializableAppearanceObject208, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.intCrownFoliation3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intCrownFoliation3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intCrownFoliation3GridLookUpEdit.Properties.NullText = "";
            this.intCrownFoliation3GridLookUpEdit.Properties.PopupView = this.gridView22;
            this.intCrownFoliation3GridLookUpEdit.Properties.Tag = "";
            this.intCrownFoliation3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intCrownFoliation3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intCrownFoliation3GridLookUpEdit.Size = new System.Drawing.Size(883, 22);
            this.intCrownFoliation3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intCrownFoliation3GridLookUpEdit.TabIndex = 36;
            this.intCrownFoliation3GridLookUpEdit.TabStop = false;
            this.intCrownFoliation3GridLookUpEdit.Tag = "109";
            // 
            // gridView22
            // 
            this.gridView22.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn115,
            this.gridColumn116,
            this.gridColumn117,
            this.gridColumn118,
            this.gridColumn119,
            this.gridColumn120});
            this.gridView22.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition22.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition22.Appearance.Options.UseForeColor = true;
            styleFormatCondition22.ApplyToRow = true;
            styleFormatCondition22.Column = this.gridColumn119;
            styleFormatCondition22.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition22.Value1 = 0;
            this.gridView22.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition22});
            this.gridView22.Name = "gridView22";
            this.gridView22.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView22.OptionsCustomization.AllowFilter = false;
            this.gridView22.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView22.OptionsFilter.AllowFilterEditor = false;
            this.gridView22.OptionsFilter.AllowMRUFilterList = false;
            this.gridView22.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView22.OptionsLayout.StoreAppearance = true;
            this.gridView22.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView22.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView22.OptionsView.ColumnAutoWidth = false;
            this.gridView22.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView22.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView22.OptionsView.ShowGroupPanel = false;
            this.gridView22.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView22.OptionsView.ShowIndicator = false;
            this.gridView22.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn120, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn115
            // 
            this.gridColumn115.Caption = "Picklist Type";
            this.gridColumn115.FieldName = "HeaderDescription";
            this.gridColumn115.Name = "gridColumn115";
            this.gridColumn115.OptionsColumn.AllowEdit = false;
            this.gridColumn115.OptionsColumn.AllowFocus = false;
            this.gridColumn115.OptionsColumn.ReadOnly = true;
            this.gridColumn115.Width = 189;
            // 
            // gridColumn116
            // 
            this.gridColumn116.Caption = "Picklist ID";
            this.gridColumn116.FieldName = "HeaderID";
            this.gridColumn116.Name = "gridColumn116";
            this.gridColumn116.OptionsColumn.AllowEdit = false;
            this.gridColumn116.OptionsColumn.AllowFocus = false;
            this.gridColumn116.OptionsColumn.ReadOnly = true;
            this.gridColumn116.Width = 67;
            // 
            // gridColumn117
            // 
            this.gridColumn117.Caption = "Item ID";
            this.gridColumn117.FieldName = "ItemCode";
            this.gridColumn117.Name = "gridColumn117";
            this.gridColumn117.OptionsColumn.AllowEdit = false;
            this.gridColumn117.OptionsColumn.AllowFocus = false;
            this.gridColumn117.OptionsColumn.ReadOnly = true;
            this.gridColumn117.Width = 58;
            // 
            // gridColumn118
            // 
            this.gridColumn118.Caption = "Description";
            this.gridColumn118.FieldName = "ItemDescription";
            this.gridColumn118.Name = "gridColumn118";
            this.gridColumn118.OptionsColumn.AllowEdit = false;
            this.gridColumn118.OptionsColumn.AllowFocus = false;
            this.gridColumn118.OptionsColumn.ReadOnly = true;
            this.gridColumn118.Visible = true;
            this.gridColumn118.VisibleIndex = 0;
            this.gridColumn118.Width = 264;
            // 
            // gridColumn120
            // 
            this.gridColumn120.Caption = "Order";
            this.gridColumn120.FieldName = "Order";
            this.gridColumn120.Name = "gridColumn120";
            this.gridColumn120.OptionsColumn.AllowEdit = false;
            this.gridColumn120.OptionsColumn.AllowFocus = false;
            this.gridColumn120.OptionsColumn.ReadOnly = true;
            this.gridColumn120.Width = 65;
            // 
            // UserPickList1GridLookUpEdit
            // 
            this.UserPickList1GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "UserPickList1", true));
            this.UserPickList1GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList1GridLookUpEdit.Location = new System.Drawing.Point(162, 324);
            this.UserPickList1GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList1GridLookUpEdit.Name = "UserPickList1GridLookUpEdit";
            editorButtonImageOptions53.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions54.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPickList1GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions53, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject209, serializableAppearanceObject210, serializableAppearanceObject211, serializableAppearanceObject212, "Edit Picklist", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions54, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject213, serializableAppearanceObject214, serializableAppearanceObject215, serializableAppearanceObject216, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPickList1GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList1GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList1GridLookUpEdit.Properties.NullText = "";
            this.UserPickList1GridLookUpEdit.Properties.PopupView = this.gridView23;
            this.UserPickList1GridLookUpEdit.Properties.Tag = "";
            this.UserPickList1GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList1GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList1GridLookUpEdit.Size = new System.Drawing.Size(924, 22);
            this.UserPickList1GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList1GridLookUpEdit.TabIndex = 42;
            this.UserPickList1GridLookUpEdit.TabStop = false;
            this.UserPickList1GridLookUpEdit.Tag = "144";
            // 
            // gridView23
            // 
            this.gridView23.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn133,
            this.gridColumn134,
            this.gridColumn135,
            this.gridColumn136,
            this.gridColumn137,
            this.gridColumn138});
            this.gridView23.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition23.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition23.Appearance.Options.UseForeColor = true;
            styleFormatCondition23.ApplyToRow = true;
            styleFormatCondition23.Column = this.gridColumn137;
            styleFormatCondition23.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition23.Value1 = 0;
            this.gridView23.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition23});
            this.gridView23.Name = "gridView23";
            this.gridView23.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView23.OptionsCustomization.AllowFilter = false;
            this.gridView23.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView23.OptionsFilter.AllowFilterEditor = false;
            this.gridView23.OptionsFilter.AllowMRUFilterList = false;
            this.gridView23.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView23.OptionsLayout.StoreAppearance = true;
            this.gridView23.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView23.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView23.OptionsView.ColumnAutoWidth = false;
            this.gridView23.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView23.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView23.OptionsView.ShowGroupPanel = false;
            this.gridView23.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView23.OptionsView.ShowIndicator = false;
            this.gridView23.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn138, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn133
            // 
            this.gridColumn133.Caption = "Picklist Type";
            this.gridColumn133.FieldName = "HeaderDescription";
            this.gridColumn133.Name = "gridColumn133";
            this.gridColumn133.OptionsColumn.AllowEdit = false;
            this.gridColumn133.OptionsColumn.AllowFocus = false;
            this.gridColumn133.OptionsColumn.ReadOnly = true;
            this.gridColumn133.Width = 189;
            // 
            // gridColumn134
            // 
            this.gridColumn134.Caption = "Picklist ID";
            this.gridColumn134.FieldName = "HeaderID";
            this.gridColumn134.Name = "gridColumn134";
            this.gridColumn134.OptionsColumn.AllowEdit = false;
            this.gridColumn134.OptionsColumn.AllowFocus = false;
            this.gridColumn134.OptionsColumn.ReadOnly = true;
            this.gridColumn134.Width = 67;
            // 
            // gridColumn135
            // 
            this.gridColumn135.Caption = "Item ID";
            this.gridColumn135.FieldName = "ItemCode";
            this.gridColumn135.Name = "gridColumn135";
            this.gridColumn135.OptionsColumn.AllowEdit = false;
            this.gridColumn135.OptionsColumn.AllowFocus = false;
            this.gridColumn135.OptionsColumn.ReadOnly = true;
            this.gridColumn135.Width = 58;
            // 
            // gridColumn136
            // 
            this.gridColumn136.Caption = "Description";
            this.gridColumn136.FieldName = "ItemDescription";
            this.gridColumn136.Name = "gridColumn136";
            this.gridColumn136.OptionsColumn.AllowEdit = false;
            this.gridColumn136.OptionsColumn.AllowFocus = false;
            this.gridColumn136.OptionsColumn.ReadOnly = true;
            this.gridColumn136.Visible = true;
            this.gridColumn136.VisibleIndex = 0;
            this.gridColumn136.Width = 264;
            // 
            // gridColumn138
            // 
            this.gridColumn138.Caption = "Order";
            this.gridColumn138.FieldName = "Order";
            this.gridColumn138.Name = "gridColumn138";
            this.gridColumn138.OptionsColumn.AllowEdit = false;
            this.gridColumn138.OptionsColumn.AllowFocus = false;
            this.gridColumn138.OptionsColumn.ReadOnly = true;
            this.gridColumn138.Width = 65;
            // 
            // UserPickList2GridLookUpEdit
            // 
            this.UserPickList2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "UserPickList2", true));
            this.UserPickList2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList2GridLookUpEdit.Location = new System.Drawing.Point(162, 350);
            this.UserPickList2GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList2GridLookUpEdit.Name = "UserPickList2GridLookUpEdit";
            editorButtonImageOptions55.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions56.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPickList2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions55, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject217, serializableAppearanceObject218, serializableAppearanceObject219, serializableAppearanceObject220, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions56, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject221, serializableAppearanceObject222, serializableAppearanceObject223, serializableAppearanceObject224, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPickList2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList2GridLookUpEdit.Properties.NullText = "";
            this.UserPickList2GridLookUpEdit.Properties.PopupView = this.gridView24;
            this.UserPickList2GridLookUpEdit.Properties.Tag = "";
            this.UserPickList2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList2GridLookUpEdit.Size = new System.Drawing.Size(924, 22);
            this.UserPickList2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList2GridLookUpEdit.TabIndex = 43;
            this.UserPickList2GridLookUpEdit.TabStop = false;
            this.UserPickList2GridLookUpEdit.Tag = "145";
            // 
            // gridView24
            // 
            this.gridView24.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn121,
            this.gridColumn122,
            this.gridColumn123,
            this.gridColumn124,
            this.gridColumn125,
            this.gridColumn126});
            this.gridView24.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition24.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition24.Appearance.Options.UseForeColor = true;
            styleFormatCondition24.ApplyToRow = true;
            styleFormatCondition24.Column = this.gridColumn125;
            styleFormatCondition24.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition24.Value1 = 0;
            this.gridView24.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition24});
            this.gridView24.Name = "gridView24";
            this.gridView24.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView24.OptionsCustomization.AllowFilter = false;
            this.gridView24.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView24.OptionsFilter.AllowFilterEditor = false;
            this.gridView24.OptionsFilter.AllowMRUFilterList = false;
            this.gridView24.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView24.OptionsLayout.StoreAppearance = true;
            this.gridView24.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView24.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView24.OptionsView.ColumnAutoWidth = false;
            this.gridView24.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView24.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView24.OptionsView.ShowGroupPanel = false;
            this.gridView24.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView24.OptionsView.ShowIndicator = false;
            this.gridView24.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn126, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn121
            // 
            this.gridColumn121.Caption = "Picklist Type";
            this.gridColumn121.FieldName = "HeaderDescription";
            this.gridColumn121.Name = "gridColumn121";
            this.gridColumn121.OptionsColumn.AllowEdit = false;
            this.gridColumn121.OptionsColumn.AllowFocus = false;
            this.gridColumn121.OptionsColumn.ReadOnly = true;
            this.gridColumn121.Width = 189;
            // 
            // gridColumn122
            // 
            this.gridColumn122.Caption = "Picklist ID";
            this.gridColumn122.FieldName = "HeaderID";
            this.gridColumn122.Name = "gridColumn122";
            this.gridColumn122.OptionsColumn.AllowEdit = false;
            this.gridColumn122.OptionsColumn.AllowFocus = false;
            this.gridColumn122.OptionsColumn.ReadOnly = true;
            this.gridColumn122.Width = 67;
            // 
            // gridColumn123
            // 
            this.gridColumn123.Caption = "Item ID";
            this.gridColumn123.FieldName = "ItemCode";
            this.gridColumn123.Name = "gridColumn123";
            this.gridColumn123.OptionsColumn.AllowEdit = false;
            this.gridColumn123.OptionsColumn.AllowFocus = false;
            this.gridColumn123.OptionsColumn.ReadOnly = true;
            this.gridColumn123.Width = 58;
            // 
            // gridColumn124
            // 
            this.gridColumn124.Caption = "Description";
            this.gridColumn124.FieldName = "ItemDescription";
            this.gridColumn124.Name = "gridColumn124";
            this.gridColumn124.OptionsColumn.AllowEdit = false;
            this.gridColumn124.OptionsColumn.AllowFocus = false;
            this.gridColumn124.OptionsColumn.ReadOnly = true;
            this.gridColumn124.Visible = true;
            this.gridColumn124.VisibleIndex = 0;
            this.gridColumn124.Width = 264;
            // 
            // gridColumn126
            // 
            this.gridColumn126.Caption = "Order";
            this.gridColumn126.FieldName = "Order";
            this.gridColumn126.Name = "gridColumn126";
            this.gridColumn126.OptionsColumn.AllowEdit = false;
            this.gridColumn126.OptionsColumn.AllowFocus = false;
            this.gridColumn126.OptionsColumn.ReadOnly = true;
            this.gridColumn126.Width = 65;
            // 
            // UserPickList3GridLookUpEdit
            // 
            this.UserPickList3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01381ATEditInspectionDetailsBindingSource, "UserPickList3", true));
            this.UserPickList3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList3GridLookUpEdit.Location = new System.Drawing.Point(162, 376);
            this.UserPickList3GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList3GridLookUpEdit.Name = "UserPickList3GridLookUpEdit";
            editorButtonImageOptions57.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions58.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.UserPickList3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions57, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject225, serializableAppearanceObject226, serializableAppearanceObject227, serializableAppearanceObject228, "Edit Picklist", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions58, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject229, serializableAppearanceObject230, serializableAppearanceObject231, serializableAppearanceObject232, "Reload Picklist", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.UserPickList3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList3GridLookUpEdit.Properties.NullText = "";
            this.UserPickList3GridLookUpEdit.Properties.PopupView = this.gridView25;
            this.UserPickList3GridLookUpEdit.Properties.Tag = "";
            this.UserPickList3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList3GridLookUpEdit.Size = new System.Drawing.Size(924, 22);
            this.UserPickList3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList3GridLookUpEdit.TabIndex = 44;
            this.UserPickList3GridLookUpEdit.TabStop = false;
            this.UserPickList3GridLookUpEdit.Tag = "146";
            // 
            // gridView25
            // 
            this.gridView25.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn127,
            this.gridColumn128,
            this.gridColumn129,
            this.gridColumn130,
            this.gridColumn131,
            this.gridColumn132});
            this.gridView25.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition25.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition25.Appearance.Options.UseForeColor = true;
            styleFormatCondition25.ApplyToRow = true;
            styleFormatCondition25.Column = this.gridColumn131;
            styleFormatCondition25.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition25.Value1 = 0;
            this.gridView25.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition25});
            this.gridView25.Name = "gridView25";
            this.gridView25.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView25.OptionsCustomization.AllowFilter = false;
            this.gridView25.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView25.OptionsFilter.AllowFilterEditor = false;
            this.gridView25.OptionsFilter.AllowMRUFilterList = false;
            this.gridView25.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView25.OptionsLayout.StoreAppearance = true;
            this.gridView25.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView25.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView25.OptionsView.ColumnAutoWidth = false;
            this.gridView25.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView25.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView25.OptionsView.ShowGroupPanel = false;
            this.gridView25.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView25.OptionsView.ShowIndicator = false;
            this.gridView25.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn132, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn127
            // 
            this.gridColumn127.Caption = "Picklist Type";
            this.gridColumn127.FieldName = "HeaderDescription";
            this.gridColumn127.Name = "gridColumn127";
            this.gridColumn127.OptionsColumn.AllowEdit = false;
            this.gridColumn127.OptionsColumn.AllowFocus = false;
            this.gridColumn127.OptionsColumn.ReadOnly = true;
            this.gridColumn127.Width = 189;
            // 
            // gridColumn128
            // 
            this.gridColumn128.Caption = "Picklist ID";
            this.gridColumn128.FieldName = "HeaderID";
            this.gridColumn128.Name = "gridColumn128";
            this.gridColumn128.OptionsColumn.AllowEdit = false;
            this.gridColumn128.OptionsColumn.AllowFocus = false;
            this.gridColumn128.OptionsColumn.ReadOnly = true;
            this.gridColumn128.Width = 67;
            // 
            // gridColumn129
            // 
            this.gridColumn129.Caption = "Item ID";
            this.gridColumn129.FieldName = "ItemCode";
            this.gridColumn129.Name = "gridColumn129";
            this.gridColumn129.OptionsColumn.AllowEdit = false;
            this.gridColumn129.OptionsColumn.AllowFocus = false;
            this.gridColumn129.OptionsColumn.ReadOnly = true;
            this.gridColumn129.Width = 58;
            // 
            // gridColumn130
            // 
            this.gridColumn130.Caption = "Description";
            this.gridColumn130.FieldName = "ItemDescription";
            this.gridColumn130.Name = "gridColumn130";
            this.gridColumn130.OptionsColumn.AllowEdit = false;
            this.gridColumn130.OptionsColumn.AllowFocus = false;
            this.gridColumn130.OptionsColumn.ReadOnly = true;
            this.gridColumn130.Visible = true;
            this.gridColumn130.VisibleIndex = 0;
            this.gridColumn130.Width = 264;
            // 
            // gridColumn132
            // 
            this.gridColumn132.Caption = "Order";
            this.gridColumn132.FieldName = "Order";
            this.gridColumn132.Name = "gridColumn132";
            this.gridColumn132.OptionsColumn.AllowEdit = false;
            this.gridColumn132.OptionsColumn.AllowFocus = false;
            this.gridColumn132.OptionsColumn.ReadOnly = true;
            this.gridColumn132.Width = 65;
            // 
            // ItemForintTreeID
            // 
            this.ItemForintTreeID.Control = this.intTreeIDTextEdit;
            this.ItemForintTreeID.CustomizationFormText = "Tree ID:";
            this.ItemForintTreeID.Location = new System.Drawing.Point(0, 47);
            this.ItemForintTreeID.Name = "ItemForintTreeID";
            this.ItemForintTreeID.Size = new System.Drawing.Size(1036, 24);
            this.ItemForintTreeID.Text = "Tree ID:";
            this.ItemForintTreeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForDateAdded
            // 
            this.ItemForDateAdded.Control = this.DateAddedDateEdit;
            this.ItemForDateAdded.CustomizationFormText = "Date Added:";
            this.ItemForDateAdded.Location = new System.Drawing.Point(0, 47);
            this.ItemForDateAdded.Name = "ItemForDateAdded";
            this.ItemForDateAdded.Size = new System.Drawing.Size(1036, 24);
            this.ItemForDateAdded.Text = "Date Added:";
            this.ItemForDateAdded.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForstrPicture
            // 
            this.ItemForstrPicture.Control = this.strPictureTextEdit;
            this.ItemForstrPicture.CustomizationFormText = "Picture Path:";
            this.ItemForstrPicture.Location = new System.Drawing.Point(0, 119);
            this.ItemForstrPicture.Name = "ItemForstrPicture";
            this.ItemForstrPicture.Size = new System.Drawing.Size(1036, 24);
            this.ItemForstrPicture.Text = "Picture Path:";
            this.ItemForstrPicture.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForGUID
            // 
            this.ItemForGUID.Control = this.GUIDTextEdit;
            this.ItemForGUID.CustomizationFormText = "GUID";
            this.ItemForGUID.Location = new System.Drawing.Point(0, 374);
            this.ItemForGUID.Name = "ItemForGUID";
            this.ItemForGUID.Size = new System.Drawing.Size(1066, 24);
            this.ItemForGUID.Text = "GUID:";
            this.ItemForGUID.TextSize = new System.Drawing.Size(131, 13);
            // 
            // ItemForintIncidentID
            // 
            this.ItemForintIncidentID.Control = this.intIncidentIDTextEdit;
            this.ItemForintIncidentID.CustomizationFormText = "Incident ID:";
            this.ItemForintIncidentID.Location = new System.Drawing.Point(0, 624);
            this.ItemForintIncidentID.Name = "ItemForintIncidentID";
            this.ItemForintIncidentID.Size = new System.Drawing.Size(1090, 24);
            this.ItemForintIncidentID.Text = "Incident ID:";
            this.ItemForintIncidentID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintInspectionID
            // 
            this.ItemForintInspectionID.Control = this.intInspectionIDTextEdit;
            this.ItemForintInspectionID.CustomizationFormText = "Inspection ID:";
            this.ItemForintInspectionID.Location = new System.Drawing.Point(0, 172);
            this.ItemForintInspectionID.Name = "ItemForintInspectionID";
            this.ItemForintInspectionID.Size = new System.Drawing.Size(339, 226);
            this.ItemForintInspectionID.Text = "Inspection ID:";
            this.ItemForintInspectionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1110, 668);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTreeReference,
            this.ItemForstrInspectRef,
            this.ItemForintInspector,
            this.ItemFordtInspectDate,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.ItemForIncidentDescription,
            this.emptySpaceItem4,
            this.tabbedControlGroup1,
            this.ItemForNoFurtherActionRequired,
            this.ItemForintSafety,
            this.emptySpaceItem13,
            this.emptySpaceItem14});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1090, 648);
            // 
            // ItemForTreeReference
            // 
            this.ItemForTreeReference.Control = this.TreeReferenceButtonEdit;
            this.ItemForTreeReference.CustomizationFormText = "Linked To Tree:";
            this.ItemForTreeReference.Location = new System.Drawing.Point(0, 23);
            this.ItemForTreeReference.Name = "ItemForTreeReference";
            this.ItemForTreeReference.Size = new System.Drawing.Size(1090, 24);
            this.ItemForTreeReference.Text = "Linked To Tree:";
            this.ItemForTreeReference.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForstrInspectRef
            // 
            this.ItemForstrInspectRef.Control = this.strInspectRefButtonEdit;
            this.ItemForstrInspectRef.CustomizationFormText = "Inspect Reference:";
            this.ItemForstrInspectRef.Location = new System.Drawing.Point(0, 47);
            this.ItemForstrInspectRef.Name = "ItemForstrInspectRef";
            this.ItemForstrInspectRef.Size = new System.Drawing.Size(1090, 24);
            this.ItemForstrInspectRef.Text = "Inspection Reference:";
            this.ItemForstrInspectRef.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintInspector
            // 
            this.ItemForintInspector.Control = this.intInspectorGridLookUpEdit;
            this.ItemForintInspector.CustomizationFormText = "Inspector:";
            this.ItemForintInspector.Location = new System.Drawing.Point(0, 71);
            this.ItemForintInspector.Name = "ItemForintInspector";
            this.ItemForintInspector.Size = new System.Drawing.Size(1090, 26);
            this.ItemForintInspector.Text = "Inspector:";
            this.ItemForintInspector.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemFordtInspectDate
            // 
            this.ItemFordtInspectDate.Control = this.dtInspectDateDateEdit;
            this.ItemFordtInspectDate.CustomizationFormText = "Inspection Date:";
            this.ItemFordtInspectDate.Location = new System.Drawing.Point(0, 97);
            this.ItemFordtInspectDate.MaxSize = new System.Drawing.Size(302, 24);
            this.ItemFordtInspectDate.MinSize = new System.Drawing.Size(302, 24);
            this.ItemFordtInspectDate.Name = "ItemFordtInspectDate";
            this.ItemFordtInspectDate.Size = new System.Drawing.Size(302, 24);
            this.ItemFordtInspectDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFordtInspectDate.Text = "Inspection Date:";
            this.ItemFordtInspectDate.TextSize = new System.Drawing.Size(135, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(137, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(137, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(137, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(337, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(753, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(137, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForIncidentDescription
            // 
            this.ItemForIncidentDescription.Control = this.IncidentDescriptionButtonEdit;
            this.ItemForIncidentDescription.CustomizationFormText = "Incident:";
            this.ItemForIncidentDescription.Location = new System.Drawing.Point(0, 121);
            this.ItemForIncidentDescription.Name = "ItemForIncidentDescription";
            this.ItemForIncidentDescription.Size = new System.Drawing.Size(1090, 24);
            this.ItemForIncidentDescription.Text = "Incident:";
            this.ItemForIncidentDescription.TextSize = new System.Drawing.Size(135, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 194);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(1090, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 204);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1090, 444);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup3,
            this.layoutControlGroup8,
            this.layoutControlGroup11,
            this.layoutControlGroup10,
            this.layoutControlGroup9});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Condition";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.emptySpaceItem10,
            this.ItemForintGeneralCondition,
            this.emptySpaceItem8,
            this.ItemForintVitality,
            this.emptySpaceItem11,
            this.emptySpaceItem12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1066, 396);
            this.layoutControlGroup5.Text = "Condition";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Stem";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.Expanded = false;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintStemPhysical,
            this.ItemForintStemPhysical2,
            this.ItemForintStemPhysical3,
            this.emptySpaceItem3,
            this.ItemForintStemDisease,
            this.ItemForintStemDisease2,
            this.ItemForintStemDisease3,
            this.ItemForintAngleToVertical,
            this.emptySpaceItem5});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 103);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1066, 31);
            this.layoutControlGroup4.Text = "Stem";
            // 
            // ItemForintStemPhysical
            // 
            this.ItemForintStemPhysical.Control = this.intStemPhysicalGridLookUpEdit;
            this.ItemForintStemPhysical.CustomizationFormText = "int Stem Physical";
            this.ItemForintStemPhysical.Location = new System.Drawing.Point(0, 0);
            this.ItemForintStemPhysical.Name = "ItemForintStemPhysical";
            this.ItemForintStemPhysical.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintStemPhysical.Text = "Stem Physical Condition 1:";
            this.ItemForintStemPhysical.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintStemPhysical2
            // 
            this.ItemForintStemPhysical2.Control = this.intStemPhysical2GridLookUpEdit;
            this.ItemForintStemPhysical2.CustomizationFormText = "int Stem Physical2";
            this.ItemForintStemPhysical2.Location = new System.Drawing.Point(0, 26);
            this.ItemForintStemPhysical2.Name = "ItemForintStemPhysical2";
            this.ItemForintStemPhysical2.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintStemPhysical2.Text = "Stem Physical Condition 2:";
            this.ItemForintStemPhysical2.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintStemPhysical3
            // 
            this.ItemForintStemPhysical3.Control = this.intStemPhysical3GridLookUpEdit;
            this.ItemForintStemPhysical3.CustomizationFormText = "int Stem Physical3";
            this.ItemForintStemPhysical3.Location = new System.Drawing.Point(0, 52);
            this.ItemForintStemPhysical3.Name = "ItemForintStemPhysical3";
            this.ItemForintStemPhysical3.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintStemPhysical3.Text = "Stem Physical Condition 3:";
            this.ItemForintStemPhysical3.TextSize = new System.Drawing.Size(135, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 78);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(1025, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForintStemDisease
            // 
            this.ItemForintStemDisease.Control = this.intStemDiseaseGridLookUpEdit;
            this.ItemForintStemDisease.CustomizationFormText = "int Stem Disease";
            this.ItemForintStemDisease.Location = new System.Drawing.Point(0, 88);
            this.ItemForintStemDisease.MinSize = new System.Drawing.Size(50, 25);
            this.ItemForintStemDisease.Name = "ItemForintStemDisease";
            this.ItemForintStemDisease.Size = new System.Drawing.Size(1025, 25);
            this.ItemForintStemDisease.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForintStemDisease.Text = "Stem Disease 1:";
            this.ItemForintStemDisease.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintStemDisease2
            // 
            this.ItemForintStemDisease2.Control = this.intStemDisease2GridLookUpEdit;
            this.ItemForintStemDisease2.CustomizationFormText = "int Stem Disease2";
            this.ItemForintStemDisease2.Location = new System.Drawing.Point(0, 113);
            this.ItemForintStemDisease2.Name = "ItemForintStemDisease2";
            this.ItemForintStemDisease2.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintStemDisease2.Text = "Stem Disease 2:";
            this.ItemForintStemDisease2.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintStemDisease3
            // 
            this.ItemForintStemDisease3.Control = this.intStemDisease3GridLookUpEdit;
            this.ItemForintStemDisease3.CustomizationFormText = "int Stem Disease3";
            this.ItemForintStemDisease3.Location = new System.Drawing.Point(0, 139);
            this.ItemForintStemDisease3.Name = "ItemForintStemDisease3";
            this.ItemForintStemDisease3.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintStemDisease3.Text = "Stem Disease 3:";
            this.ItemForintStemDisease3.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintAngleToVertical
            // 
            this.ItemForintAngleToVertical.Control = this.intAngleToVerticalSpinEdit;
            this.ItemForintAngleToVertical.CustomizationFormText = "int Angle To Vertical";
            this.ItemForintAngleToVertical.Location = new System.Drawing.Point(0, 175);
            this.ItemForintAngleToVertical.Name = "ItemForintAngleToVertical";
            this.ItemForintAngleToVertical.Size = new System.Drawing.Size(1025, 24);
            this.ItemForintAngleToVertical.Text = "Angle To Vertical:";
            this.ItemForintAngleToVertical.TextSize = new System.Drawing.Size(135, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 165);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(1025, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Crown";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.Expanded = false;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintCrownPhysical,
            this.ItemForintCrownPhysical2,
            this.ItemForintCrownPhysical3,
            this.ItemForintCrownDisease,
            this.ItemForintCrownDisease2,
            this.ItemForintCrownDisease3,
            this.ItemForintCrownFoliation3,
            this.ItemForintCrownFoliation2,
            this.ItemForintCrownFoliation,
            this.emptySpaceItem6,
            this.emptySpaceItem7});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 144);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1066, 31);
            this.layoutControlGroup6.Text = "Crown";
            // 
            // ItemForintCrownPhysical
            // 
            this.ItemForintCrownPhysical.Control = this.intCrownPhysicalGridLookUpEdit;
            this.ItemForintCrownPhysical.CustomizationFormText = "int Crown Physical";
            this.ItemForintCrownPhysical.Location = new System.Drawing.Point(0, 0);
            this.ItemForintCrownPhysical.Name = "ItemForintCrownPhysical";
            this.ItemForintCrownPhysical.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintCrownPhysical.Text = "Crown Physical Condition 1:";
            this.ItemForintCrownPhysical.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintCrownPhysical2
            // 
            this.ItemForintCrownPhysical2.Control = this.intCrownPhysical2GridLookUpEdit;
            this.ItemForintCrownPhysical2.CustomizationFormText = "int Crown Physical2";
            this.ItemForintCrownPhysical2.Location = new System.Drawing.Point(0, 26);
            this.ItemForintCrownPhysical2.Name = "ItemForintCrownPhysical2";
            this.ItemForintCrownPhysical2.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintCrownPhysical2.Text = "Crown Physical Condition 2:";
            this.ItemForintCrownPhysical2.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintCrownPhysical3
            // 
            this.ItemForintCrownPhysical3.Control = this.intCrownPhysical3GridLookUpEdit;
            this.ItemForintCrownPhysical3.CustomizationFormText = "int Crown Physical3";
            this.ItemForintCrownPhysical3.Location = new System.Drawing.Point(0, 52);
            this.ItemForintCrownPhysical3.Name = "ItemForintCrownPhysical3";
            this.ItemForintCrownPhysical3.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintCrownPhysical3.Text = "Crown Physical Condition 3:";
            this.ItemForintCrownPhysical3.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintCrownDisease
            // 
            this.ItemForintCrownDisease.Control = this.intCrownDiseaseGridLookUpEdit;
            this.ItemForintCrownDisease.CustomizationFormText = "int Crown Disease";
            this.ItemForintCrownDisease.Location = new System.Drawing.Point(0, 88);
            this.ItemForintCrownDisease.Name = "ItemForintCrownDisease";
            this.ItemForintCrownDisease.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintCrownDisease.Text = "Crown Disease 1:";
            this.ItemForintCrownDisease.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintCrownDisease2
            // 
            this.ItemForintCrownDisease2.Control = this.intCrownDisease2GridLookUpEdit;
            this.ItemForintCrownDisease2.CustomizationFormText = "int Crown Disease2";
            this.ItemForintCrownDisease2.Location = new System.Drawing.Point(0, 114);
            this.ItemForintCrownDisease2.Name = "ItemForintCrownDisease2";
            this.ItemForintCrownDisease2.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintCrownDisease2.Text = "Crown Disease 2:";
            this.ItemForintCrownDisease2.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintCrownDisease3
            // 
            this.ItemForintCrownDisease3.Control = this.intCrownDisease3GridLookUpEdit;
            this.ItemForintCrownDisease3.CustomizationFormText = "int Crown Disease3";
            this.ItemForintCrownDisease3.Location = new System.Drawing.Point(0, 140);
            this.ItemForintCrownDisease3.Name = "ItemForintCrownDisease3";
            this.ItemForintCrownDisease3.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintCrownDisease3.Text = "Crown Disease 3:";
            this.ItemForintCrownDisease3.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintCrownFoliation3
            // 
            this.ItemForintCrownFoliation3.Control = this.intCrownFoliation3GridLookUpEdit;
            this.ItemForintCrownFoliation3.CustomizationFormText = "int Crown Foliation3";
            this.ItemForintCrownFoliation3.Location = new System.Drawing.Point(0, 228);
            this.ItemForintCrownFoliation3.Name = "ItemForintCrownFoliation3";
            this.ItemForintCrownFoliation3.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintCrownFoliation3.Text = "Crown Foliation 3:";
            this.ItemForintCrownFoliation3.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintCrownFoliation2
            // 
            this.ItemForintCrownFoliation2.Control = this.intCrownFoliation2GridLookUpEdit;
            this.ItemForintCrownFoliation2.CustomizationFormText = "int Crown Foliation2";
            this.ItemForintCrownFoliation2.Location = new System.Drawing.Point(0, 202);
            this.ItemForintCrownFoliation2.Name = "ItemForintCrownFoliation2";
            this.ItemForintCrownFoliation2.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintCrownFoliation2.Text = "Crown Foliation 2:";
            this.ItemForintCrownFoliation2.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintCrownFoliation
            // 
            this.ItemForintCrownFoliation.Control = this.intCrownFoliationGridLookUpEdit;
            this.ItemForintCrownFoliation.CustomizationFormText = "int Crown Foliation";
            this.ItemForintCrownFoliation.Location = new System.Drawing.Point(0, 176);
            this.ItemForintCrownFoliation.Name = "ItemForintCrownFoliation";
            this.ItemForintCrownFoliation.Size = new System.Drawing.Size(1025, 26);
            this.ItemForintCrownFoliation.Text = "Crown Foliation 1:";
            this.ItemForintCrownFoliation.TextSize = new System.Drawing.Size(135, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 78);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1025, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 166);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(1025, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Base";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.Expanded = false;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintRootHeave,
            this.ItemForintRootHeave2,
            this.ItemForintRootHeave3});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 62);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1066, 31);
            this.layoutControlGroup7.Text = "Base";
            // 
            // ItemForintRootHeave
            // 
            this.ItemForintRootHeave.Control = this.intRootHeaveGridLookUpEdit;
            this.ItemForintRootHeave.CustomizationFormText = "int Root Heave";
            this.ItemForintRootHeave.Location = new System.Drawing.Point(0, 0);
            this.ItemForintRootHeave.Name = "ItemForintRootHeave";
            this.ItemForintRootHeave.Size = new System.Drawing.Size(1042, 26);
            this.ItemForintRootHeave.Text = "Root Heave 1:";
            this.ItemForintRootHeave.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintRootHeave2
            // 
            this.ItemForintRootHeave2.Control = this.intRootHeave2GridLookUpEdit;
            this.ItemForintRootHeave2.CustomizationFormText = "int Root Heave2";
            this.ItemForintRootHeave2.Location = new System.Drawing.Point(0, 26);
            this.ItemForintRootHeave2.Name = "ItemForintRootHeave2";
            this.ItemForintRootHeave2.Size = new System.Drawing.Size(1042, 26);
            this.ItemForintRootHeave2.Text = "Root Heave 2:";
            this.ItemForintRootHeave2.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintRootHeave3
            // 
            this.ItemForintRootHeave3.Control = this.intRootHeave3GridLookUpEdit;
            this.ItemForintRootHeave3.CustomizationFormText = "int Root Heave3";
            this.ItemForintRootHeave3.Location = new System.Drawing.Point(0, 52);
            this.ItemForintRootHeave3.Name = "ItemForintRootHeave3";
            this.ItemForintRootHeave3.Size = new System.Drawing.Size(1042, 26);
            this.ItemForintRootHeave3.Text = "Root Heave 3:";
            this.ItemForintRootHeave3.TextSize = new System.Drawing.Size(135, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 175);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(1066, 221);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForintGeneralCondition
            // 
            this.ItemForintGeneralCondition.Control = this.intGeneralConditionGridLookUpEdit;
            this.ItemForintGeneralCondition.CustomizationFormText = "int General Condition";
            this.ItemForintGeneralCondition.Location = new System.Drawing.Point(0, 0);
            this.ItemForintGeneralCondition.Name = "ItemForintGeneralCondition";
            this.ItemForintGeneralCondition.Size = new System.Drawing.Size(1066, 26);
            this.ItemForintGeneralCondition.Text = "General Condition:";
            this.ItemForintGeneralCondition.TextSize = new System.Drawing.Size(135, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 52);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(1066, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForintVitality
            // 
            this.ItemForintVitality.Control = this.intVitalityGridLookUpEdit;
            this.ItemForintVitality.CustomizationFormText = "int Vitality";
            this.ItemForintVitality.Location = new System.Drawing.Point(0, 26);
            this.ItemForintVitality.Name = "ItemForintVitality";
            this.ItemForintVitality.Size = new System.Drawing.Size(1066, 26);
            this.ItemForintVitality.Text = "Vitality:";
            this.ItemForintVitality.TextSize = new System.Drawing.Size(135, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 93);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(1066, 10);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 134);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(1066, 10);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Actions";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1066, 396);
            this.layoutControlGroup3.Text = "Actions";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Action Grid:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1066, 396);
            this.layoutControlItem2.Text = "Action Grid:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "User Fields";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrUser1,
            this.ItemForstrUser2,
            this.ItemForstrUser3,
            this.ItemForUserPickList1,
            this.ItemForUserPickList2,
            this.ItemForUserPickList3,
            this.emptySpaceItem9,
            this.emptySpaceItem15,
            this.emptySpaceItem16,
            this.emptySpaceItem17});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1066, 396);
            this.layoutControlGroup8.Text = "User Fields";
            // 
            // ItemForstrUser1
            // 
            this.ItemForstrUser1.Control = this.strUser1TextEdit;
            this.ItemForstrUser1.CustomizationFormText = "str User1";
            this.ItemForstrUser1.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrUser1.MaxSize = new System.Drawing.Size(410, 24);
            this.ItemForstrUser1.MinSize = new System.Drawing.Size(410, 24);
            this.ItemForstrUser1.Name = "ItemForstrUser1";
            this.ItemForstrUser1.Size = new System.Drawing.Size(410, 24);
            this.ItemForstrUser1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrUser1.Text = "Text 1:";
            this.ItemForstrUser1.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForstrUser2
            // 
            this.ItemForstrUser2.Control = this.strUser2TextEdit;
            this.ItemForstrUser2.CustomizationFormText = "str User2";
            this.ItemForstrUser2.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrUser2.MaxSize = new System.Drawing.Size(410, 24);
            this.ItemForstrUser2.MinSize = new System.Drawing.Size(410, 24);
            this.ItemForstrUser2.Name = "ItemForstrUser2";
            this.ItemForstrUser2.Size = new System.Drawing.Size(410, 24);
            this.ItemForstrUser2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrUser2.Text = "Text 2:";
            this.ItemForstrUser2.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForstrUser3
            // 
            this.ItemForstrUser3.Control = this.strUser3TextEdit;
            this.ItemForstrUser3.CustomizationFormText = "str User3";
            this.ItemForstrUser3.Location = new System.Drawing.Point(0, 48);
            this.ItemForstrUser3.MaxSize = new System.Drawing.Size(410, 24);
            this.ItemForstrUser3.MinSize = new System.Drawing.Size(410, 24);
            this.ItemForstrUser3.Name = "ItemForstrUser3";
            this.ItemForstrUser3.Size = new System.Drawing.Size(410, 24);
            this.ItemForstrUser3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForstrUser3.Text = "Text 3:";
            this.ItemForstrUser3.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForUserPickList1
            // 
            this.ItemForUserPickList1.Control = this.UserPickList1GridLookUpEdit;
            this.ItemForUserPickList1.CustomizationFormText = "User Pick List1";
            this.ItemForUserPickList1.Location = new System.Drawing.Point(0, 72);
            this.ItemForUserPickList1.Name = "ItemForUserPickList1";
            this.ItemForUserPickList1.Size = new System.Drawing.Size(1066, 26);
            this.ItemForUserPickList1.Text = "Pick List 1:";
            this.ItemForUserPickList1.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForUserPickList2
            // 
            this.ItemForUserPickList2.Control = this.UserPickList2GridLookUpEdit;
            this.ItemForUserPickList2.CustomizationFormText = "User Pick List2";
            this.ItemForUserPickList2.Location = new System.Drawing.Point(0, 98);
            this.ItemForUserPickList2.Name = "ItemForUserPickList2";
            this.ItemForUserPickList2.Size = new System.Drawing.Size(1066, 26);
            this.ItemForUserPickList2.Text = "Pick List 2:";
            this.ItemForUserPickList2.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForUserPickList3
            // 
            this.ItemForUserPickList3.Control = this.UserPickList3GridLookUpEdit;
            this.ItemForUserPickList3.CustomizationFormText = "User Pick List3";
            this.ItemForUserPickList3.Location = new System.Drawing.Point(0, 124);
            this.ItemForUserPickList3.Name = "ItemForUserPickList3";
            this.ItemForUserPickList3.Size = new System.Drawing.Size(1066, 26);
            this.ItemForUserPickList3.Text = "Pick List 3:";
            this.ItemForUserPickList3.TextSize = new System.Drawing.Size(135, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 150);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1066, 246);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(410, 0);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(656, 24);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(410, 24);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(656, 24);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(410, 48);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(656, 24);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup11.CaptionImageOptions.Image")));
            this.layoutControlGroup11.CustomizationFormText = "Linked Pictures";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedPicturesGrid});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(1066, 396);
            this.layoutControlGroup11.Text = "Linked Pictures";
            // 
            // ItemForLinkedPicturesGrid
            // 
            this.ItemForLinkedPicturesGrid.Control = this.gridControl39;
            this.ItemForLinkedPicturesGrid.CustomizationFormText = "Linked Pictures Grid:";
            this.ItemForLinkedPicturesGrid.Location = new System.Drawing.Point(0, 0);
            this.ItemForLinkedPicturesGrid.Name = "ItemForLinkedPicturesGrid";
            this.ItemForLinkedPicturesGrid.Size = new System.Drawing.Size(1066, 396);
            this.ItemForLinkedPicturesGrid.Text = "Linked Pictures Grid:";
            this.ItemForLinkedPicturesGrid.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForLinkedPicturesGrid.TextVisible = false;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.linked_documents_16_16;
            this.layoutControlGroup10.CustomizationFormText = "Linked documents";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(1066, 396);
            this.layoutControlGroup10.Text = "Linked Documents";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl2;
            this.layoutControlItem3.CustomizationFormText = "Linked Documents:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1066, 396);
            this.layoutControlItem3.Text = "Linked Documents:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup9.CaptionImageOptions.Image")));
            this.layoutControlGroup9.CustomizationFormText = "Remarks";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1066, 396);
            this.layoutControlGroup9.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "str Remarks";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(1066, 396);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // ItemForNoFurtherActionRequired
            // 
            this.ItemForNoFurtherActionRequired.Control = this.NoFurtherActionRequiredCheckEdit;
            this.ItemForNoFurtherActionRequired.CustomizationFormText = "No Further Action Required";
            this.ItemForNoFurtherActionRequired.Location = new System.Drawing.Point(0, 171);
            this.ItemForNoFurtherActionRequired.MaxSize = new System.Drawing.Size(302, 23);
            this.ItemForNoFurtherActionRequired.MinSize = new System.Drawing.Size(302, 23);
            this.ItemForNoFurtherActionRequired.Name = "ItemForNoFurtherActionRequired";
            this.ItemForNoFurtherActionRequired.Size = new System.Drawing.Size(302, 23);
            this.ItemForNoFurtherActionRequired.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForNoFurtherActionRequired.Text = "No Further Action Required:";
            this.ItemForNoFurtherActionRequired.TextSize = new System.Drawing.Size(135, 13);
            // 
            // ItemForintSafety
            // 
            this.ItemForintSafety.Control = this.intSafetyGridLookUpEdit;
            this.ItemForintSafety.CustomizationFormText = "int Safety";
            this.ItemForintSafety.Location = new System.Drawing.Point(0, 145);
            this.ItemForintSafety.Name = "ItemForintSafety";
            this.ItemForintSafety.Size = new System.Drawing.Size(1090, 26);
            this.ItemForintSafety.Text = "Risk Category:";
            this.ItemForintSafety.TextSize = new System.Drawing.Size(135, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(302, 97);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(788, 24);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(302, 171);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(788, 23);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp01381_AT_Edit_Inspection_DetailsTableAdapter
            // 
            this.sp01381_AT_Edit_Inspection_DetailsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter
            // 
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.ClearBeforeFill = true;
            // 
            // sp00190_Contractor_List_With_BlankTableAdapter
            // 
            this.sp00190_Contractor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp01375_AT_User_Screen_SettingsBindingSource
            // 
            this.sp01375_AT_User_Screen_SettingsBindingSource.DataMember = "sp01375_AT_User_Screen_Settings";
            this.sp01375_AT_User_Screen_SettingsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp01375_AT_User_Screen_SettingsTableAdapter
            // 
            this.sp01375_AT_User_Screen_SettingsTableAdapter.ClearBeforeFill = true;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelLastInspection});
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanelLastInspection
            // 
            this.dockPanelLastInspection.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelLastInspection.Appearance.Options.UseBackColor = true;
            this.dockPanelLastInspection.Controls.Add(this.dockPanel1_Container);
            this.dockPanelLastInspection.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelLastInspection.ID = new System.Guid("3b9a6313-a8fa-4c7e-b244-fe670817c722");
            this.dockPanelLastInspection.Location = new System.Drawing.Point(722, 26);
            this.dockPanelLastInspection.Name = "dockPanelLastInspection";
            this.dockPanelLastInspection.Options.ShowCloseButton = false;
            this.dockPanelLastInspection.OriginalSize = new System.Drawing.Size(388, 200);
            this.dockPanelLastInspection.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelLastInspection.SavedIndex = 0;
            this.dockPanelLastInspection.Size = new System.Drawing.Size(388, 668);
            this.dockPanelLastInspection.Text = "Last Inspection Details";
            this.dockPanelLastInspection.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelLastInspection.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelLastInspection_ClosingPanel);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.checkEditSychronise);
            this.dockPanel1_Container.Controls.Add(this.vGridControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(382, 636);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // checkEditSychronise
            // 
            this.checkEditSychronise.Location = new System.Drawing.Point(3, 2);
            this.checkEditSychronise.MenuManager = this.barManager1;
            this.checkEditSychronise.Name = "checkEditSychronise";
            this.checkEditSychronise.Properties.Caption = "Synchronise";
            this.checkEditSychronise.Size = new System.Drawing.Size(87, 19);
            this.checkEditSychronise.TabIndex = 1;
            // 
            // vGridControl1
            // 
            this.vGridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vGridControl1.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.vGridControl1.DataSource = this.sp01384ATInspectionEditPreviousInspectionsBindingSource;
            this.vGridControl1.Location = new System.Drawing.Point(3, 23);
            this.vGridControl1.Name = "vGridControl1";
            this.vGridControl1.RecordWidth = 133;
            this.vGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.vGridControl1.RowHeaderWidth = 176;
            this.vGridControl1.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.categoryRow1,
            this.categoryRow2});
            this.vGridControl1.Size = new System.Drawing.Size(376, 610);
            this.vGridControl1.TabIndex = 0;
            // 
            // sp01384ATInspectionEditPreviousInspectionsBindingSource
            // 
            this.sp01384ATInspectionEditPreviousInspectionsBindingSource.DataMember = "sp01384_AT_Inspection_Edit_Previous_Inspections";
            this.sp01384ATInspectionEditPreviousInspectionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // categoryRow1
            // 
            this.categoryRow1.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowTreeID,
            this.rowInspectionID,
            this.rowSiteID,
            this.rowClientID,
            this.rowClientName,
            this.rowSiteName,
            this.rowTreeReferenceButtonEdit,
            this.rowTreeMappingID,
            this.rowTreeGridReference,
            this.rowTreeDistance,
            this.editorRow1,
            this.editorRow2,
            this.editorRow3,
            this.editorRow4,
            this.editorRow5,
            this.editorRow6,
            this.editorRow7,
            this.editorRow8,
            this.editorRow9,
            this.editorRow10,
            this.editorRow11,
            this.editorRow12,
            this.editorRow13,
            this.editorRow14,
            this.editorRow15,
            this.editorRow16,
            this.editorRow17,
            this.editorRow18,
            this.editorRow19,
            this.editorRow20,
            this.editorRow21,
            this.editorRow22,
            this.editorRow23,
            this.editorRow24,
            this.editorRow25,
            this.editorRow26,
            this.editorRow27,
            this.editorRow28,
            this.editorRow29,
            this.rowTreeTarget1,
            this.rowTreeTarget2,
            this.rowTreeTarget3,
            this.editorRow30,
            this.editorRow31,
            this.editorRow32,
            this.editorRow33,
            this.editorRow34,
            this.editorRow35,
            this.editorRow36,
            this.editorRow37,
            this.editorRow38,
            this.editorRow39});
            this.categoryRow1.Expanded = false;
            this.categoryRow1.Name = "categoryRow1";
            this.categoryRow1.Properties.Caption = "Tree Data";
            // 
            // rowTreeID
            // 
            this.rowTreeID.Name = "rowTreeID";
            this.rowTreeID.Properties.Caption = "Tree ID:";
            this.rowTreeID.Properties.FieldName = "TreeID";
            this.rowTreeID.Properties.ReadOnly = true;
            this.rowTreeID.Visible = false;
            // 
            // rowInspectionID
            // 
            this.rowInspectionID.Name = "rowInspectionID";
            this.rowInspectionID.Properties.Caption = "Inspection ID:";
            this.rowInspectionID.Properties.FieldName = "InspectionID";
            this.rowInspectionID.Properties.ReadOnly = true;
            this.rowInspectionID.Visible = false;
            // 
            // rowSiteID
            // 
            this.rowSiteID.Name = "rowSiteID";
            this.rowSiteID.Properties.Caption = "Site ID:";
            this.rowSiteID.Properties.FieldName = "SiteID";
            this.rowSiteID.Properties.ReadOnly = true;
            this.rowSiteID.Visible = false;
            // 
            // rowClientID
            // 
            this.rowClientID.Name = "rowClientID";
            this.rowClientID.Properties.Caption = "Client ID:";
            this.rowClientID.Properties.FieldName = "ClientID";
            this.rowClientID.Properties.ReadOnly = true;
            this.rowClientID.Visible = false;
            // 
            // rowClientName
            // 
            this.rowClientName.Name = "rowClientName";
            this.rowClientName.Properties.Caption = "Client Name:";
            this.rowClientName.Properties.FieldName = "ClientName";
            this.rowClientName.Properties.ReadOnly = true;
            // 
            // rowSiteName
            // 
            this.rowSiteName.Name = "rowSiteName";
            this.rowSiteName.Properties.Caption = "Site Name:";
            this.rowSiteName.Properties.FieldName = "SiteName";
            this.rowSiteName.Properties.ReadOnly = true;
            // 
            // rowTreeReferenceButtonEdit
            // 
            this.rowTreeReferenceButtonEdit.Name = "rowTreeReferenceButtonEdit";
            this.rowTreeReferenceButtonEdit.Properties.Caption = "Tree Reference:";
            this.rowTreeReferenceButtonEdit.Properties.FieldName = "TreeReference";
            this.rowTreeReferenceButtonEdit.Properties.ReadOnly = true;
            // 
            // rowTreeMappingID
            // 
            this.rowTreeMappingID.Name = "rowTreeMappingID";
            this.rowTreeMappingID.Properties.Caption = "Mapping ID:";
            this.rowTreeMappingID.Properties.FieldName = "TreeMappingID";
            this.rowTreeMappingID.Properties.ReadOnly = true;
            // 
            // rowTreeGridReference
            // 
            this.rowTreeGridReference.Name = "rowTreeGridReference";
            this.rowTreeGridReference.Properties.Caption = "Grid Reference:";
            this.rowTreeGridReference.Properties.FieldName = "TreeGridReference";
            this.rowTreeGridReference.Properties.ReadOnly = true;
            // 
            // rowTreeDistance
            // 
            this.rowTreeDistance.Name = "rowTreeDistance";
            this.rowTreeDistance.Properties.Caption = "Distance:";
            this.rowTreeDistance.Properties.FieldName = "TreeDistance";
            this.rowTreeDistance.Properties.ReadOnly = true;
            // 
            // editorRow1
            // 
            this.editorRow1.Name = "editorRow1";
            this.editorRow1.Properties.Caption = "Nearest House:";
            this.editorRow1.Properties.FieldName = "TreeHouseName";
            this.editorRow1.Properties.ReadOnly = true;
            // 
            // editorRow2
            // 
            this.editorRow2.Name = "editorRow2";
            this.editorRow2.Properties.Caption = "Species:";
            this.editorRow2.Properties.FieldName = "TreeSpeciesName";
            this.editorRow2.Properties.ReadOnly = true;
            // 
            // editorRow3
            // 
            this.editorRow3.Name = "editorRow3";
            this.editorRow3.Properties.Caption = "Accessibility:";
            this.editorRow3.Properties.FieldName = "TreeAccess";
            this.editorRow3.Properties.ReadOnly = true;
            // 
            // editorRow4
            // 
            this.editorRow4.Name = "editorRow4";
            this.editorRow4.Properties.Caption = "Visibility:";
            this.editorRow4.Properties.FieldName = "TreeVisibility";
            this.editorRow4.Properties.ReadOnly = true;
            // 
            // editorRow5
            // 
            this.editorRow5.Name = "editorRow5";
            this.editorRow5.Properties.Caption = "Landscape Context:";
            this.editorRow5.Properties.FieldName = "TreeContext";
            this.editorRow5.Properties.ReadOnly = true;
            // 
            // editorRow6
            // 
            this.editorRow6.Name = "editorRow6";
            this.editorRow6.Properties.Caption = "Management:";
            this.editorRow6.Properties.FieldName = "TreeManagement";
            this.editorRow6.Properties.ReadOnly = true;
            // 
            // editorRow7
            // 
            this.editorRow7.Name = "editorRow7";
            this.editorRow7.Properties.Caption = "Legal Status:";
            this.editorRow7.Properties.FieldName = "TreeLegalStatus";
            this.editorRow7.Properties.ReadOnly = true;
            // 
            // editorRow8
            // 
            this.editorRow8.Name = "editorRow8";
            this.editorRow8.Properties.Caption = "Last Inspection Date:";
            this.editorRow8.Properties.FieldName = "TreeLastInspectionDate";
            this.editorRow8.Properties.ReadOnly = true;
            // 
            // editorRow9
            // 
            this.editorRow9.Name = "editorRow9";
            this.editorRow9.Properties.Caption = "Inspection Cycle:";
            this.editorRow9.Properties.FieldName = "TreeInspectionCycle";
            this.editorRow9.Properties.ReadOnly = true;
            // 
            // editorRow10
            // 
            this.editorRow10.Name = "editorRow10";
            this.editorRow10.Properties.Caption = "Inspection Unit:";
            this.editorRow10.Properties.FieldName = "TreeInspectionUnit";
            this.editorRow10.Properties.ReadOnly = true;
            // 
            // editorRow11
            // 
            this.editorRow11.Name = "editorRow11";
            this.editorRow11.Properties.Caption = "Next Inspection Date:";
            this.editorRow11.Properties.FieldName = "TreeNextInspectionDate";
            this.editorRow11.Properties.ReadOnly = true;
            // 
            // editorRow12
            // 
            this.editorRow12.Name = "editorRow12";
            this.editorRow12.Properties.Caption = "Ground Type:";
            this.editorRow12.Properties.FieldName = "TreeGroundType";
            this.editorRow12.Properties.ReadOnly = true;
            // 
            // editorRow13
            // 
            this.editorRow13.Name = "editorRow13";
            this.editorRow13.Properties.Caption = "DBH:";
            this.editorRow13.Properties.FieldName = "TreeDBH";
            this.editorRow13.Properties.ReadOnly = true;
            // 
            // editorRow14
            // 
            this.editorRow14.Name = "editorRow14";
            this.editorRow14.Properties.Caption = "DBH Range:";
            this.editorRow14.Properties.FieldName = "TreeDBHRange";
            this.editorRow14.Properties.ReadOnly = true;
            // 
            // editorRow15
            // 
            this.editorRow15.Name = "editorRow15";
            this.editorRow15.Properties.Caption = "Height:";
            this.editorRow15.Properties.FieldName = "TreeHeight";
            this.editorRow15.Properties.ReadOnly = true;
            // 
            // editorRow16
            // 
            this.editorRow16.Name = "editorRow16";
            this.editorRow16.Properties.Caption = "Height Range:";
            this.editorRow16.Properties.FieldName = "TreeHeightRange";
            this.editorRow16.Properties.ReadOnly = true;
            // 
            // editorRow17
            // 
            this.editorRow17.Name = "editorRow17";
            this.editorRow17.Properties.Caption = "Protection Type:";
            this.editorRow17.Properties.FieldName = "TreeProtectionType";
            this.editorRow17.Properties.ReadOnly = true;
            // 
            // editorRow18
            // 
            this.editorRow18.Name = "editorRow18";
            this.editorRow18.Properties.Caption = "Crown Diameter:";
            this.editorRow18.Properties.FieldName = "TreeCrownDiameter";
            this.editorRow18.Properties.ReadOnly = true;
            // 
            // editorRow19
            // 
            this.editorRow19.Name = "editorRow19";
            this.editorRow19.Properties.Caption = "Plant Date:";
            this.editorRow19.Properties.FieldName = "TreePlantDate";
            this.editorRow19.Properties.ReadOnly = true;
            // 
            // editorRow20
            // 
            this.editorRow20.Name = "editorRow20";
            this.editorRow20.Properties.Caption = "Group Number:";
            this.editorRow20.Properties.FieldName = "TreeGroupNumber";
            this.editorRow20.Properties.ReadOnly = true;
            // 
            // editorRow21
            // 
            this.editorRow21.Name = "editorRow21";
            this.editorRow21.Properties.Caption = "Risk Category:";
            this.editorRow21.Properties.FieldName = "TreeRiskCategory";
            this.editorRow21.Properties.ReadOnly = true;
            // 
            // editorRow22
            // 
            this.editorRow22.Name = "editorRow22";
            this.editorRow22.Properties.Caption = "Site Hazard Class:";
            this.editorRow22.Properties.FieldName = "TreeSiteHazardClass";
            this.editorRow22.Properties.ReadOnly = true;
            // 
            // editorRow23
            // 
            this.editorRow23.Name = "editorRow23";
            this.editorRow23.Properties.Caption = "Plant Size:";
            this.editorRow23.Properties.FieldName = "TreePlantSize";
            this.editorRow23.Properties.ReadOnly = true;
            // 
            // editorRow24
            // 
            this.editorRow24.Name = "editorRow24";
            this.editorRow24.Properties.Caption = "Plant Source:";
            this.editorRow24.Properties.FieldName = "TreePlantSource";
            this.editorRow24.Properties.ReadOnly = true;
            // 
            // editorRow25
            // 
            this.editorRow25.Name = "editorRow25";
            this.editorRow25.Properties.Caption = "Plant Method:";
            this.editorRow25.Properties.FieldName = "TreePlantMethod";
            this.editorRow25.Properties.ReadOnly = true;
            // 
            // editorRow26
            // 
            this.editorRow26.Name = "editorRow26";
            this.editorRow26.Properties.Caption = "Postcode:";
            this.editorRow26.Properties.FieldName = "TreePostcode";
            this.editorRow26.Properties.ReadOnly = true;
            // 
            // editorRow27
            // 
            this.editorRow27.Name = "editorRow27";
            this.editorRow27.Properties.Caption = "Map Label:";
            this.editorRow27.Properties.FieldName = "TreeMapLabel";
            this.editorRow27.Properties.ReadOnly = true;
            // 
            // editorRow28
            // 
            this.editorRow28.Name = "editorRow28";
            this.editorRow28.Properties.Caption = "Status:";
            this.editorRow28.Properties.FieldName = "TreeStatus";
            this.editorRow28.Properties.ReadOnly = true;
            // 
            // editorRow29
            // 
            this.editorRow29.Name = "editorRow29";
            this.editorRow29.Properties.Caption = "Size:";
            this.editorRow29.Properties.FieldName = "TreeSize";
            this.editorRow29.Properties.ReadOnly = true;
            // 
            // rowTreeTarget1
            // 
            this.rowTreeTarget1.Name = "rowTreeTarget1";
            this.rowTreeTarget1.Properties.Caption = "Nearby Object 1:";
            this.rowTreeTarget1.Properties.FieldName = "TreeTarget1";
            this.rowTreeTarget1.Properties.ReadOnly = true;
            // 
            // rowTreeTarget2
            // 
            this.rowTreeTarget2.Name = "rowTreeTarget2";
            this.rowTreeTarget2.Properties.Caption = "Nearby Object 2:";
            this.rowTreeTarget2.Properties.FieldName = "TreeTarget2";
            this.rowTreeTarget2.Properties.ReadOnly = true;
            // 
            // rowTreeTarget3
            // 
            this.rowTreeTarget3.Name = "rowTreeTarget3";
            this.rowTreeTarget3.Properties.Caption = "Nearby Object 3:";
            this.rowTreeTarget3.Properties.FieldName = "TreeTarget3";
            this.rowTreeTarget3.Properties.ReadOnly = true;
            // 
            // editorRow30
            // 
            this.editorRow30.Name = "editorRow30";
            this.editorRow30.Properties.Caption = "Tree Remarks:";
            this.editorRow30.Properties.FieldName = "TreeRemarks";
            this.editorRow30.Properties.ReadOnly = true;
            this.editorRow30.Properties.RowEdit = this.repositoryItemMemoExEdit1;
            // 
            // editorRow31
            // 
            this.editorRow31.Name = "editorRow31";
            this.editorRow31.Properties.Caption = "Tree User Defined 1:";
            this.editorRow31.Properties.FieldName = "TreeUser1";
            this.editorRow31.Properties.ReadOnly = true;
            // 
            // editorRow32
            // 
            this.editorRow32.Name = "editorRow32";
            this.editorRow32.Properties.Caption = "Tree User Defined 2:";
            this.editorRow32.Properties.FieldName = "TreeUser2";
            this.editorRow32.Properties.ReadOnly = true;
            // 
            // editorRow33
            // 
            this.editorRow33.Name = "editorRow33";
            this.editorRow33.Properties.Caption = "Tree User Defined 3:";
            this.editorRow33.Properties.FieldName = "TreeUser3";
            this.editorRow33.Properties.ReadOnly = true;
            // 
            // editorRow34
            // 
            this.editorRow34.Name = "editorRow34";
            this.editorRow34.Properties.Caption = "Age Class:";
            this.editorRow34.Properties.FieldName = "TreeAgeClass";
            this.editorRow34.Properties.ReadOnly = true;
            // 
            // editorRow35
            // 
            this.editorRow35.Name = "editorRow35";
            this.editorRow35.Properties.Caption = "Replant Count:";
            this.editorRow35.Properties.FieldName = "TreeReplantCount";
            this.editorRow35.Properties.ReadOnly = true;
            // 
            // editorRow36
            // 
            this.editorRow36.Name = "editorRow36";
            this.editorRow36.Properties.Caption = "Survey Date:";
            this.editorRow36.Properties.FieldName = "TreeSurveyDate";
            this.editorRow36.Properties.ReadOnly = true;
            // 
            // editorRow37
            // 
            this.editorRow37.Name = "editorRow37";
            this.editorRow37.Properties.Caption = "Object Type:";
            this.editorRow37.Properties.FieldName = "TreeType";
            this.editorRow37.Properties.ReadOnly = true;
            // 
            // editorRow38
            // 
            this.editorRow38.Name = "editorRow38";
            this.editorRow38.Properties.Caption = "Risk Factor:";
            this.editorRow38.Properties.FieldName = "TreeRiskFactor";
            this.editorRow38.Properties.ReadOnly = true;
            // 
            // editorRow39
            // 
            this.editorRow39.Name = "editorRow39";
            this.editorRow39.Properties.Caption = "Ownership:";
            this.editorRow39.Properties.FieldName = "TreeOwnershipName";
            this.editorRow39.Properties.ReadOnly = true;
            // 
            // categoryRow2
            // 
            this.categoryRow2.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowstrInspectRefButtonEdit,
            this.rowintInspectorGridLookUpEdit,
            this.rowdtInspectDateDateEdit,
            this.rowIncidentDescriptionButtonEdit,
            this.rowInspectionIncidentDate,
            this.rowintIncidentIDTextEdit,
            this.rowintGeneralConditionGridLookUpEdit,
            this.rowintVitalityGridLookUpEdit,
            this.rowNoFurtherActionRequiredCheckEdit,
            this.rowintSafetyGridLookUpEdit,
            this.categoryRow3,
            this.categoryRow4,
            this.categoryRow5,
            this.categoryRow6,
            this.rowstrRemarksMemoEdit,
            this.rowLinkedActionCount,
            this.rowLinkedOutstandingActionCount});
            this.categoryRow2.Name = "categoryRow2";
            this.categoryRow2.Properties.Caption = "Inspection Data";
            // 
            // rowstrInspectRefButtonEdit
            // 
            this.rowstrInspectRefButtonEdit.Name = "rowstrInspectRefButtonEdit";
            this.rowstrInspectRefButtonEdit.Properties.Caption = "Reference Number:";
            this.rowstrInspectRefButtonEdit.Properties.FieldName = "InspectionReference";
            this.rowstrInspectRefButtonEdit.Properties.ReadOnly = true;
            // 
            // rowintInspectorGridLookUpEdit
            // 
            this.rowintInspectorGridLookUpEdit.Name = "rowintInspectorGridLookUpEdit";
            this.rowintInspectorGridLookUpEdit.Properties.Caption = "Inspector:";
            this.rowintInspectorGridLookUpEdit.Properties.FieldName = "InspectionInspector";
            this.rowintInspectorGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowdtInspectDateDateEdit
            // 
            this.rowdtInspectDateDateEdit.Name = "rowdtInspectDateDateEdit";
            this.rowdtInspectDateDateEdit.Properties.Caption = "Inspection Date:";
            this.rowdtInspectDateDateEdit.Properties.FieldName = "InspectionDate";
            this.rowdtInspectDateDateEdit.Properties.ReadOnly = true;
            // 
            // rowIncidentDescriptionButtonEdit
            // 
            this.rowIncidentDescriptionButtonEdit.Name = "rowIncidentDescriptionButtonEdit";
            this.rowIncidentDescriptionButtonEdit.Properties.Caption = "Incident Reference:";
            this.rowIncidentDescriptionButtonEdit.Properties.FieldName = "InspectionIncidentReference";
            this.rowIncidentDescriptionButtonEdit.Properties.ReadOnly = true;
            // 
            // rowInspectionIncidentDate
            // 
            this.rowInspectionIncidentDate.Name = "rowInspectionIncidentDate";
            this.rowInspectionIncidentDate.Properties.Caption = "Incident Date:";
            this.rowInspectionIncidentDate.Properties.FieldName = "InspectionIncidentDate";
            this.rowInspectionIncidentDate.Properties.ReadOnly = true;
            // 
            // rowintIncidentIDTextEdit
            // 
            this.rowintIncidentIDTextEdit.Name = "rowintIncidentIDTextEdit";
            this.rowintIncidentIDTextEdit.Properties.Caption = "Incident ID:";
            this.rowintIncidentIDTextEdit.Properties.FieldName = "IncidentID";
            this.rowintIncidentIDTextEdit.Properties.ReadOnly = true;
            this.rowintIncidentIDTextEdit.Visible = false;
            // 
            // rowintGeneralConditionGridLookUpEdit
            // 
            this.rowintGeneralConditionGridLookUpEdit.Name = "rowintGeneralConditionGridLookUpEdit";
            this.rowintGeneralConditionGridLookUpEdit.Properties.Caption = "General Condition:";
            this.rowintGeneralConditionGridLookUpEdit.Properties.FieldName = "InspectionGeneralCondition";
            this.rowintGeneralConditionGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintVitalityGridLookUpEdit
            // 
            this.rowintVitalityGridLookUpEdit.Name = "rowintVitalityGridLookUpEdit";
            this.rowintVitalityGridLookUpEdit.Properties.Caption = "Vitality:";
            this.rowintVitalityGridLookUpEdit.Properties.FieldName = "InspectionVitality";
            this.rowintVitalityGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowNoFurtherActionRequiredCheckEdit
            // 
            this.rowNoFurtherActionRequiredCheckEdit.Name = "rowNoFurtherActionRequiredCheckEdit";
            this.rowNoFurtherActionRequiredCheckEdit.Properties.Caption = "No Further Action Required:";
            this.rowNoFurtherActionRequiredCheckEdit.Properties.FieldName = "NoFurtherActionRequired";
            this.rowNoFurtherActionRequiredCheckEdit.Properties.ReadOnly = true;
            // 
            // rowintSafetyGridLookUpEdit
            // 
            this.rowintSafetyGridLookUpEdit.Name = "rowintSafetyGridLookUpEdit";
            this.rowintSafetyGridLookUpEdit.Properties.Caption = "Risk Category:";
            this.rowintSafetyGridLookUpEdit.Properties.FieldName = "InspectionRiskCategory";
            this.rowintSafetyGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // categoryRow3
            // 
            this.categoryRow3.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowintRootHeaveGridLookUpEdit,
            this.rowintRootHeave2GridLookUpEdit,
            this.rowintRootHeave3GridLookUpEdit});
            this.categoryRow3.Name = "categoryRow3";
            this.categoryRow3.Properties.Caption = "Base";
            // 
            // rowintRootHeaveGridLookUpEdit
            // 
            this.rowintRootHeaveGridLookUpEdit.Name = "rowintRootHeaveGridLookUpEdit";
            this.rowintRootHeaveGridLookUpEdit.Properties.Caption = "Base Physical 1:";
            this.rowintRootHeaveGridLookUpEdit.Properties.FieldName = "InspectionBasePhysical1";
            this.rowintRootHeaveGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintRootHeave2GridLookUpEdit
            // 
            this.rowintRootHeave2GridLookUpEdit.Name = "rowintRootHeave2GridLookUpEdit";
            this.rowintRootHeave2GridLookUpEdit.Properties.Caption = "Base Physical 2:";
            this.rowintRootHeave2GridLookUpEdit.Properties.FieldName = "InspectionBasePhysical2";
            this.rowintRootHeave2GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintRootHeave3GridLookUpEdit
            // 
            this.rowintRootHeave3GridLookUpEdit.Name = "rowintRootHeave3GridLookUpEdit";
            this.rowintRootHeave3GridLookUpEdit.Properties.Caption = "Base Physical 3:";
            this.rowintRootHeave3GridLookUpEdit.Properties.FieldName = "InspectionBasePhysical3";
            this.rowintRootHeave3GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // categoryRow4
            // 
            this.categoryRow4.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowintStemPhysicalGridLookUpEdit,
            this.rowintStemPhysical2GridLookUpEdit,
            this.rowintStemPhysical3GridLookUpEdit,
            this.rowintStemDiseaseGridLookUpEdit,
            this.rowintStemDisease2GridLookUpEdit,
            this.rowintStemDisease3GridLookUpEdit,
            this.rowintAngleToVerticalSpinEdit});
            this.categoryRow4.Name = "categoryRow4";
            this.categoryRow4.Properties.Caption = "Stem";
            // 
            // rowintStemPhysicalGridLookUpEdit
            // 
            this.rowintStemPhysicalGridLookUpEdit.Name = "rowintStemPhysicalGridLookUpEdit";
            this.rowintStemPhysicalGridLookUpEdit.Properties.Caption = "Stem Physical 1:";
            this.rowintStemPhysicalGridLookUpEdit.Properties.FieldName = "InspectionStemPhysical1";
            this.rowintStemPhysicalGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintStemPhysical2GridLookUpEdit
            // 
            this.rowintStemPhysical2GridLookUpEdit.Name = "rowintStemPhysical2GridLookUpEdit";
            this.rowintStemPhysical2GridLookUpEdit.Properties.Caption = "Stem Physical 2:";
            this.rowintStemPhysical2GridLookUpEdit.Properties.FieldName = "InspectionStemPhysical2";
            this.rowintStemPhysical2GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintStemPhysical3GridLookUpEdit
            // 
            this.rowintStemPhysical3GridLookUpEdit.Name = "rowintStemPhysical3GridLookUpEdit";
            this.rowintStemPhysical3GridLookUpEdit.Properties.Caption = "Stem Physical 3:";
            this.rowintStemPhysical3GridLookUpEdit.Properties.FieldName = "InspectionStemPhysical3";
            this.rowintStemPhysical3GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintStemDiseaseGridLookUpEdit
            // 
            this.rowintStemDiseaseGridLookUpEdit.Name = "rowintStemDiseaseGridLookUpEdit";
            this.rowintStemDiseaseGridLookUpEdit.Properties.Caption = "Stem Disease 1:";
            this.rowintStemDiseaseGridLookUpEdit.Properties.FieldName = "InspectionStemDisease1";
            this.rowintStemDiseaseGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintStemDisease2GridLookUpEdit
            // 
            this.rowintStemDisease2GridLookUpEdit.Name = "rowintStemDisease2GridLookUpEdit";
            this.rowintStemDisease2GridLookUpEdit.Properties.Caption = "Stem Disease 2:";
            this.rowintStemDisease2GridLookUpEdit.Properties.FieldName = "InspectionStemDisease2";
            this.rowintStemDisease2GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintStemDisease3GridLookUpEdit
            // 
            this.rowintStemDisease3GridLookUpEdit.Name = "rowintStemDisease3GridLookUpEdit";
            this.rowintStemDisease3GridLookUpEdit.Properties.Caption = "Stem Disease 3:";
            this.rowintStemDisease3GridLookUpEdit.Properties.FieldName = "InspectionStemDisease3";
            this.rowintStemDisease3GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintAngleToVerticalSpinEdit
            // 
            this.rowintAngleToVerticalSpinEdit.Name = "rowintAngleToVerticalSpinEdit";
            this.rowintAngleToVerticalSpinEdit.Properties.Caption = "Angle To Vertical:";
            this.rowintAngleToVerticalSpinEdit.Properties.FieldName = "InspectionAngleToVertical";
            this.rowintAngleToVerticalSpinEdit.Properties.ReadOnly = true;
            // 
            // categoryRow5
            // 
            this.categoryRow5.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowintCrownPhysicalGridLookUpEdit,
            this.rowintCrownPhysical2GridLookUpEdit,
            this.rowintCrownPhysical3GridLookUpEdit,
            this.rowintCrownDiseaseGridLookUpEdit,
            this.rowintCrownDisease2GridLookUpEdit,
            this.rowintCrownDisease3GridLookUpEdit,
            this.rowintCrownFoliationGridLookUpEdit,
            this.rowintCrownFoliation2GridLookUpEdit,
            this.rowintCrownFoliation3GridLookUpEdit});
            this.categoryRow5.Name = "categoryRow5";
            this.categoryRow5.Properties.Caption = "Crown";
            // 
            // rowintCrownPhysicalGridLookUpEdit
            // 
            this.rowintCrownPhysicalGridLookUpEdit.Name = "rowintCrownPhysicalGridLookUpEdit";
            this.rowintCrownPhysicalGridLookUpEdit.Properties.Caption = "Crown Physical 1:";
            this.rowintCrownPhysicalGridLookUpEdit.Properties.FieldName = "InspectionCrownPhysical1";
            this.rowintCrownPhysicalGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintCrownPhysical2GridLookUpEdit
            // 
            this.rowintCrownPhysical2GridLookUpEdit.Name = "rowintCrownPhysical2GridLookUpEdit";
            this.rowintCrownPhysical2GridLookUpEdit.Properties.Caption = "Crown Physical 2:";
            this.rowintCrownPhysical2GridLookUpEdit.Properties.FieldName = "InspectionCrownPhysical2";
            this.rowintCrownPhysical2GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintCrownPhysical3GridLookUpEdit
            // 
            this.rowintCrownPhysical3GridLookUpEdit.Name = "rowintCrownPhysical3GridLookUpEdit";
            this.rowintCrownPhysical3GridLookUpEdit.Properties.Caption = "Crown Physica 3:";
            this.rowintCrownPhysical3GridLookUpEdit.Properties.FieldName = "InspectionCrownPhysical3";
            this.rowintCrownPhysical3GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintCrownDiseaseGridLookUpEdit
            // 
            this.rowintCrownDiseaseGridLookUpEdit.Name = "rowintCrownDiseaseGridLookUpEdit";
            this.rowintCrownDiseaseGridLookUpEdit.Properties.Caption = "Crown Disease 1:";
            this.rowintCrownDiseaseGridLookUpEdit.Properties.FieldName = "InspectionCrownDisease1";
            this.rowintCrownDiseaseGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintCrownDisease2GridLookUpEdit
            // 
            this.rowintCrownDisease2GridLookUpEdit.Name = "rowintCrownDisease2GridLookUpEdit";
            this.rowintCrownDisease2GridLookUpEdit.Properties.Caption = "Crown Disease 2:";
            this.rowintCrownDisease2GridLookUpEdit.Properties.FieldName = "InspectionCrownDisease2";
            this.rowintCrownDisease2GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintCrownDisease3GridLookUpEdit
            // 
            this.rowintCrownDisease3GridLookUpEdit.Name = "rowintCrownDisease3GridLookUpEdit";
            this.rowintCrownDisease3GridLookUpEdit.Properties.Caption = "Crown Disease 3:";
            this.rowintCrownDisease3GridLookUpEdit.Properties.FieldName = "InspectionCrownDisease3";
            this.rowintCrownDisease3GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintCrownFoliationGridLookUpEdit
            // 
            this.rowintCrownFoliationGridLookUpEdit.Name = "rowintCrownFoliationGridLookUpEdit";
            this.rowintCrownFoliationGridLookUpEdit.Properties.Caption = "Crown Foliation 1:";
            this.rowintCrownFoliationGridLookUpEdit.Properties.FieldName = "InspectionCrownFoliation1";
            this.rowintCrownFoliationGridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintCrownFoliation2GridLookUpEdit
            // 
            this.rowintCrownFoliation2GridLookUpEdit.Name = "rowintCrownFoliation2GridLookUpEdit";
            this.rowintCrownFoliation2GridLookUpEdit.Properties.Caption = "Crown Foliation 2:";
            this.rowintCrownFoliation2GridLookUpEdit.Properties.FieldName = "InspectionCrownFoliation2";
            this.rowintCrownFoliation2GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowintCrownFoliation3GridLookUpEdit
            // 
            this.rowintCrownFoliation3GridLookUpEdit.Name = "rowintCrownFoliation3GridLookUpEdit";
            this.rowintCrownFoliation3GridLookUpEdit.Properties.Caption = "Crown Foliation 3:";
            this.rowintCrownFoliation3GridLookUpEdit.Properties.FieldName = "InspectionCrownFoliation3";
            this.rowintCrownFoliation3GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // categoryRow6
            // 
            this.categoryRow6.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.rowstrUser1TextEdit,
            this.rowstrUser2TextEdit,
            this.rowstrUser3TextEdit,
            this.rowUserPickList1GridLookUpEdit,
            this.rowUserPickList2GridLookUpEdit,
            this.rowUserPickList3GridLookUpEdit});
            this.categoryRow6.Expanded = false;
            this.categoryRow6.Name = "categoryRow6";
            this.categoryRow6.Properties.Caption = "User Defined";
            // 
            // rowstrUser1TextEdit
            // 
            this.rowstrUser1TextEdit.Name = "rowstrUser1TextEdit";
            this.rowstrUser1TextEdit.Properties.Caption = "User Defined 1:";
            this.rowstrUser1TextEdit.Properties.FieldName = "InspectionUserDefined1";
            this.rowstrUser1TextEdit.Properties.ReadOnly = true;
            // 
            // rowstrUser2TextEdit
            // 
            this.rowstrUser2TextEdit.Name = "rowstrUser2TextEdit";
            this.rowstrUser2TextEdit.Properties.Caption = "User Defined 2:";
            this.rowstrUser2TextEdit.Properties.FieldName = "InspectionUserDefined2";
            this.rowstrUser2TextEdit.Properties.ReadOnly = true;
            // 
            // rowstrUser3TextEdit
            // 
            this.rowstrUser3TextEdit.Name = "rowstrUser3TextEdit";
            this.rowstrUser3TextEdit.Properties.Caption = "User Defined 3:";
            this.rowstrUser3TextEdit.Properties.FieldName = "InspectionUserDefined3";
            this.rowstrUser3TextEdit.Properties.ReadOnly = true;
            // 
            // rowUserPickList1GridLookUpEdit
            // 
            this.rowUserPickList1GridLookUpEdit.Name = "rowUserPickList1GridLookUpEdit";
            this.rowUserPickList1GridLookUpEdit.Properties.Caption = "User Pick List 1:";
            this.rowUserPickList1GridLookUpEdit.Properties.FieldName = "UserPickList1";
            this.rowUserPickList1GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowUserPickList2GridLookUpEdit
            // 
            this.rowUserPickList2GridLookUpEdit.Name = "rowUserPickList2GridLookUpEdit";
            this.rowUserPickList2GridLookUpEdit.Properties.Caption = "User Pick List 2:";
            this.rowUserPickList2GridLookUpEdit.Properties.FieldName = "UserPickList2";
            this.rowUserPickList2GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowUserPickList3GridLookUpEdit
            // 
            this.rowUserPickList3GridLookUpEdit.Name = "rowUserPickList3GridLookUpEdit";
            this.rowUserPickList3GridLookUpEdit.Properties.Caption = "User Pick List 3:";
            this.rowUserPickList3GridLookUpEdit.Properties.FieldName = "UserPickList3";
            this.rowUserPickList3GridLookUpEdit.Properties.ReadOnly = true;
            // 
            // rowstrRemarksMemoEdit
            // 
            this.rowstrRemarksMemoEdit.Name = "rowstrRemarksMemoEdit";
            this.rowstrRemarksMemoEdit.Properties.Caption = "Inspection Remarks:";
            this.rowstrRemarksMemoEdit.Properties.FieldName = "InspectionRemarks";
            this.rowstrRemarksMemoEdit.Properties.ReadOnly = true;
            this.rowstrRemarksMemoEdit.Properties.RowEdit = this.repositoryItemMemoExEdit1;
            // 
            // rowLinkedActionCount
            // 
            this.rowLinkedActionCount.Name = "rowLinkedActionCount";
            this.rowLinkedActionCount.Properties.Caption = "Action Count:";
            this.rowLinkedActionCount.Properties.FieldName = "LinkedActionCount";
            this.rowLinkedActionCount.Properties.ReadOnly = true;
            // 
            // rowLinkedOutstandingActionCount
            // 
            this.rowLinkedOutstandingActionCount.Name = "rowLinkedOutstandingActionCount";
            this.rowLinkedOutstandingActionCount.Properties.Caption = "Outstanding Action Count:";
            this.rowLinkedOutstandingActionCount.Properties.FieldName = "LinkedOutstandingActionCount";
            this.rowLinkedOutstandingActionCount.Properties.ReadOnly = true;
            // 
            // rowTreeHouseName
            // 
            this.rowTreeHouseName.Name = "rowTreeHouseName";
            this.rowTreeHouseName.Properties.Caption = "Tree House Name:";
            this.rowTreeHouseName.Properties.FieldName = "TreeHouseName";
            // 
            // rowTreeSpeciesName
            // 
            this.rowTreeSpeciesName.Name = "rowTreeSpeciesName";
            this.rowTreeSpeciesName.Properties.Caption = "Tree Species:";
            this.rowTreeSpeciesName.Properties.FieldName = "TreeSpeciesName";
            this.rowTreeSpeciesName.Properties.ReadOnly = true;
            // 
            // rowTreeAccess
            // 
            this.rowTreeAccess.Name = "rowTreeAccess";
            this.rowTreeAccess.Properties.Caption = "Tree Access";
            this.rowTreeAccess.Properties.FieldName = "TreeAccess";
            this.rowTreeAccess.Properties.ReadOnly = true;
            // 
            // rowTreeVisibility
            // 
            this.rowTreeVisibility.Name = "rowTreeVisibility";
            this.rowTreeVisibility.Properties.Caption = "Tree Visibility";
            this.rowTreeVisibility.Properties.FieldName = "TreeVisibility";
            this.rowTreeVisibility.Properties.ReadOnly = true;
            // 
            // rowTreeContext
            // 
            this.rowTreeContext.Name = "rowTreeContext";
            this.rowTreeContext.Properties.Caption = "Tree Context";
            this.rowTreeContext.Properties.FieldName = "TreeContext";
            // 
            // rowTreeManagement
            // 
            this.rowTreeManagement.Name = "rowTreeManagement";
            this.rowTreeManagement.Properties.Caption = "Tree Management";
            this.rowTreeManagement.Properties.FieldName = "TreeManagement";
            // 
            // rowTreeLegalStatus
            // 
            this.rowTreeLegalStatus.Name = "rowTreeLegalStatus";
            this.rowTreeLegalStatus.Properties.Caption = "Tree Legal Status";
            this.rowTreeLegalStatus.Properties.FieldName = "TreeLegalStatus";
            this.rowTreeLegalStatus.Properties.ReadOnly = true;
            // 
            // rowTreeLastInspectionDate
            // 
            this.rowTreeLastInspectionDate.Name = "rowTreeLastInspectionDate";
            this.rowTreeLastInspectionDate.Properties.Caption = "Tree Last Inspection Date";
            this.rowTreeLastInspectionDate.Properties.FieldName = "TreeLastInspectionDate";
            // 
            // rowTreeInspectionCycle
            // 
            this.rowTreeInspectionCycle.Name = "rowTreeInspectionCycle";
            this.rowTreeInspectionCycle.Properties.Caption = "Tree Inspection Cycle";
            this.rowTreeInspectionCycle.Properties.FieldName = "TreeInspectionCycle";
            // 
            // rowTreeInspectionUnit
            // 
            this.rowTreeInspectionUnit.Name = "rowTreeInspectionUnit";
            this.rowTreeInspectionUnit.Properties.Caption = "Tree Inspection Unit";
            this.rowTreeInspectionUnit.Properties.FieldName = "TreeInspectionUnit";
            // 
            // rowTreeNextInspectionDate
            // 
            this.rowTreeNextInspectionDate.Name = "rowTreeNextInspectionDate";
            this.rowTreeNextInspectionDate.Properties.Caption = "Tree Next Inspection Date";
            this.rowTreeNextInspectionDate.Properties.FieldName = "TreeNextInspectionDate";
            // 
            // rowTreeGroundType
            // 
            this.rowTreeGroundType.Name = "rowTreeGroundType";
            this.rowTreeGroundType.Properties.Caption = "Tree Ground Type";
            this.rowTreeGroundType.Properties.FieldName = "TreeGroundType";
            this.rowTreeGroundType.Properties.ReadOnly = true;
            // 
            // rowTreeDBH
            // 
            this.rowTreeDBH.Name = "rowTreeDBH";
            this.rowTreeDBH.Properties.Caption = "Tree DBH";
            this.rowTreeDBH.Properties.FieldName = "TreeDBH";
            // 
            // rowTreeDBHRange
            // 
            this.rowTreeDBHRange.Name = "rowTreeDBHRange";
            this.rowTreeDBHRange.Properties.Caption = "Tree DBH Range";
            this.rowTreeDBHRange.Properties.FieldName = "TreeDBHRange";
            this.rowTreeDBHRange.Properties.ReadOnly = true;
            // 
            // rowTreeHeight
            // 
            this.rowTreeHeight.Name = "rowTreeHeight";
            this.rowTreeHeight.Properties.Caption = "Tree Height";
            this.rowTreeHeight.Properties.FieldName = "TreeHeight";
            // 
            // rowTreeHeightRange
            // 
            this.rowTreeHeightRange.Name = "rowTreeHeightRange";
            this.rowTreeHeightRange.Properties.Caption = "Tree Height Range";
            this.rowTreeHeightRange.Properties.FieldName = "TreeHeightRange";
            this.rowTreeHeightRange.Properties.ReadOnly = true;
            // 
            // rowTreeProtectionType
            // 
            this.rowTreeProtectionType.Name = "rowTreeProtectionType";
            this.rowTreeProtectionType.Properties.Caption = "Tree Protection Type";
            this.rowTreeProtectionType.Properties.FieldName = "TreeProtectionType";
            this.rowTreeProtectionType.Properties.ReadOnly = true;
            // 
            // rowTreeCrownDiameter
            // 
            this.rowTreeCrownDiameter.Name = "rowTreeCrownDiameter";
            this.rowTreeCrownDiameter.Properties.Caption = "Tree Crown Diameter";
            this.rowTreeCrownDiameter.Properties.FieldName = "TreeCrownDiameter";
            // 
            // rowTreePlantDate
            // 
            this.rowTreePlantDate.Name = "rowTreePlantDate";
            this.rowTreePlantDate.Properties.Caption = "Tree Plant Date";
            this.rowTreePlantDate.Properties.FieldName = "TreePlantDate";
            // 
            // rowTreeGroupNumber
            // 
            this.rowTreeGroupNumber.Name = "rowTreeGroupNumber";
            this.rowTreeGroupNumber.Properties.Caption = "Tree Group Number";
            this.rowTreeGroupNumber.Properties.FieldName = "TreeGroupNumber";
            // 
            // rowTreeRiskCategory
            // 
            this.rowTreeRiskCategory.Name = "rowTreeRiskCategory";
            this.rowTreeRiskCategory.Properties.Caption = "Tree Risk Category";
            this.rowTreeRiskCategory.Properties.FieldName = "TreeRiskCategory";
            this.rowTreeRiskCategory.Properties.ReadOnly = true;
            // 
            // rowTreeSiteHazardClass
            // 
            this.rowTreeSiteHazardClass.Name = "rowTreeSiteHazardClass";
            this.rowTreeSiteHazardClass.Properties.Caption = "Tree Site Hazard Class";
            this.rowTreeSiteHazardClass.Properties.FieldName = "TreeSiteHazardClass";
            this.rowTreeSiteHazardClass.Properties.ReadOnly = true;
            // 
            // rowTreePlantSize
            // 
            this.rowTreePlantSize.Name = "rowTreePlantSize";
            this.rowTreePlantSize.Properties.Caption = "Tree Plant Size";
            this.rowTreePlantSize.Properties.FieldName = "TreePlantSize";
            this.rowTreePlantSize.Properties.ReadOnly = true;
            // 
            // rowTreePlantSource
            // 
            this.rowTreePlantSource.Name = "rowTreePlantSource";
            this.rowTreePlantSource.Properties.Caption = "Tree Plant Source";
            this.rowTreePlantSource.Properties.FieldName = "TreePlantSource";
            this.rowTreePlantSource.Properties.ReadOnly = true;
            // 
            // rowTreePlantMethod
            // 
            this.rowTreePlantMethod.Name = "rowTreePlantMethod";
            this.rowTreePlantMethod.Properties.Caption = "Tree Plant Method";
            this.rowTreePlantMethod.Properties.FieldName = "TreePlantMethod";
            this.rowTreePlantMethod.Properties.ReadOnly = true;
            // 
            // rowTreePostcode
            // 
            this.rowTreePostcode.Name = "rowTreePostcode";
            this.rowTreePostcode.Properties.Caption = "Tree Postcode";
            this.rowTreePostcode.Properties.FieldName = "TreePostcode";
            // 
            // rowTreeMapLabel
            // 
            this.rowTreeMapLabel.Name = "rowTreeMapLabel";
            this.rowTreeMapLabel.Properties.Caption = "Tree Map Label";
            this.rowTreeMapLabel.Properties.FieldName = "TreeMapLabel";
            // 
            // rowTreeStatus
            // 
            this.rowTreeStatus.Name = "rowTreeStatus";
            this.rowTreeStatus.Properties.Caption = "Tree Status";
            this.rowTreeStatus.Properties.FieldName = "TreeStatus";
            this.rowTreeStatus.Properties.ReadOnly = true;
            // 
            // rowTreeSize
            // 
            this.rowTreeSize.Name = "rowTreeSize";
            this.rowTreeSize.Properties.Caption = "Tree Size";
            this.rowTreeSize.Properties.FieldName = "TreeSize";
            this.rowTreeSize.Properties.ReadOnly = true;
            // 
            // rowTreeLastModifiedDate
            // 
            this.rowTreeLastModifiedDate.Name = "rowTreeLastModifiedDate";
            this.rowTreeLastModifiedDate.Properties.Caption = "Tree Last Modified Date";
            this.rowTreeLastModifiedDate.Properties.FieldName = "TreeLastModifiedDate";
            // 
            // rowTreeRemarks
            // 
            this.rowTreeRemarks.Name = "rowTreeRemarks";
            this.rowTreeRemarks.Properties.Caption = "Tree Remarks";
            this.rowTreeRemarks.Properties.FieldName = "TreeRemarks";
            // 
            // rowTreeUser1
            // 
            this.rowTreeUser1.Name = "rowTreeUser1";
            this.rowTreeUser1.Properties.Caption = "Tree User1";
            this.rowTreeUser1.Properties.FieldName = "TreeUser1";
            // 
            // rowTreeUser2
            // 
            this.rowTreeUser2.Name = "rowTreeUser2";
            this.rowTreeUser2.Properties.Caption = "Tree User2";
            this.rowTreeUser2.Properties.FieldName = "TreeUser2";
            // 
            // rowTreeUser3
            // 
            this.rowTreeUser3.Name = "rowTreeUser3";
            this.rowTreeUser3.Properties.Caption = "Tree User3";
            this.rowTreeUser3.Properties.FieldName = "TreeUser3";
            // 
            // rowTreeAgeClass
            // 
            this.rowTreeAgeClass.Name = "rowTreeAgeClass";
            this.rowTreeAgeClass.Properties.Caption = "Tree Age Class";
            this.rowTreeAgeClass.Properties.FieldName = "TreeAgeClass";
            this.rowTreeAgeClass.Properties.ReadOnly = true;
            // 
            // rowTreeAreaHa
            // 
            this.rowTreeAreaHa.Name = "rowTreeAreaHa";
            this.rowTreeAreaHa.Properties.Caption = "Tree Area Ha";
            this.rowTreeAreaHa.Properties.FieldName = "TreeAreaHa";
            // 
            // rowTreeReplantCount
            // 
            this.rowTreeReplantCount.Name = "rowTreeReplantCount";
            this.rowTreeReplantCount.Properties.Caption = "Tree Replant Count";
            this.rowTreeReplantCount.Properties.FieldName = "TreeReplantCount";
            // 
            // rowTreeSurveyDate
            // 
            this.rowTreeSurveyDate.Name = "rowTreeSurveyDate";
            this.rowTreeSurveyDate.Properties.Caption = "Tree Survey Date";
            this.rowTreeSurveyDate.Properties.FieldName = "TreeSurveyDate";
            // 
            // rowTreeType
            // 
            this.rowTreeType.Name = "rowTreeType";
            this.rowTreeType.Properties.Caption = "Tree Type";
            this.rowTreeType.Properties.FieldName = "TreeType";
            this.rowTreeType.Properties.ReadOnly = true;
            // 
            // rowTreeSiteLevel
            // 
            this.rowTreeSiteLevel.Name = "rowTreeSiteLevel";
            this.rowTreeSiteLevel.Properties.Caption = "Tree Site Level";
            this.rowTreeSiteLevel.Properties.FieldName = "TreeSiteLevel";
            // 
            // rowTreeSituation
            // 
            this.rowTreeSituation.Name = "rowTreeSituation";
            this.rowTreeSituation.Properties.Caption = "Tree Situation";
            this.rowTreeSituation.Properties.FieldName = "TreeSituation";
            // 
            // rowTreeRiskFactor
            // 
            this.rowTreeRiskFactor.Name = "rowTreeRiskFactor";
            this.rowTreeRiskFactor.Properties.Caption = "Tree Risk Factor";
            this.rowTreeRiskFactor.Properties.FieldName = "TreeRiskFactor";
            // 
            // rowTreeOwnershipName
            // 
            this.rowTreeOwnershipName.Name = "rowTreeOwnershipName";
            this.rowTreeOwnershipName.Properties.Caption = "Tree Ownership Name";
            this.rowTreeOwnershipName.Properties.FieldName = "TreeOwnershipName";
            this.rowTreeOwnershipName.Properties.ReadOnly = true;
            // 
            // sp01384_AT_Inspection_Edit_Previous_InspectionsTableAdapter
            // 
            this.sp01384_AT_Inspection_Edit_Previous_InspectionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01380_AT_Actions_Linked_To_InspectionsTableAdapter
            // 
            this.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp02065_AT_Inspection_Pictures_ListTableAdapter
            // 
            this.sp02065_AT_Inspection_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending39
            // 
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending39.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending39.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending39.GridControl = this.gridControl39;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // frm_AT_Inspection_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1110, 724);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Inspection_Edit";
            this.Text = "Edit Inspection";
            this.Activated += new System.EventHandler(this.frm_AT_Inspection_Edit_Activated);
            this.Deactivate += new System.EventHandler(this.frm_AT_Inspection_Edit_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Inspection_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Inspection_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp02065ATInspectionPicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01380ATActionsLinkedToInspectionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01381ATEditInspectionDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intInspectionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReferenceButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTreeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInspectDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInspectDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strPictureTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateAddedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GUIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIncidentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intAngleToVerticalSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strInspectRefButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoFurtherActionRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intInspectorGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00190ContractorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intSafetyGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intGeneralConditionGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intVitalityGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intRootHeaveGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intRootHeave2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intRootHeave3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemPhysicalGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemPhysical2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemPhysical3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemDiseaseGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemDisease2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intStemDisease3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownPhysicalGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownPhysical2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownPhysical3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownDiseaseGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownDisease2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownDisease3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownFoliationGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownFoliation2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intCrownFoliation3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList1GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintTreeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGUID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintIncidentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintInspectionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrInspectRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintInspector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtInspectDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemPhysical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemPhysical2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemPhysical3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemDisease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemDisease2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintStemDisease3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintAngleToVertical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownPhysical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownPhysical2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownPhysical3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownDisease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownDisease2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownDisease3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownFoliation3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownFoliation2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintCrownFoliation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintRootHeave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintRootHeave2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintRootHeave3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintGeneralCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintVitality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedPicturesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNoFurtherActionRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintSafety)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01375_AT_User_Screen_SettingsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanelLastInspection.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditSychronise.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01384ATInspectionEditPreviousInspectionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiCopyAllValues;
        private DevExpress.XtraBars.BarButtonItem bbiCopySelectedValues;
        private DevExpress.XtraBars.BarButtonItem bbiReload;
        private DevExpress.XtraBars.BarButtonItem bbiRememberDetails;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private System.Windows.Forms.BindingSource sp01381ATEditInspectionDetailsBindingSource;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01381_AT_Edit_Inspection_DetailsTableAdapter sp01381_AT_Edit_Inspection_DetailsTableAdapter;
        private DevExpress.XtraEditors.TextEdit intInspectionIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit TreeReferenceButtonEdit;
        private DevExpress.XtraEditors.TextEdit intTreeIDTextEdit;
        private DevExpress.XtraEditors.DateEdit dtInspectDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit intAngleToVerticalSpinEdit;
        private DevExpress.XtraEditors.TextEdit strUser1TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser2TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser3TextEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraEditors.TextEdit strPictureTextEdit;
        private DevExpress.XtraEditors.DateEdit DateAddedDateEdit;
        private DevExpress.XtraEditors.TextEdit GUIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrPicture;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateAdded;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintInspectionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintTreeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrInspectRef;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintInspector;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtInspectDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintIncidentID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintSafety;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintGeneralCondition;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintVitality;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNoFurtherActionRequired;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGUID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit intIncidentIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit IncidentDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncidentDescription;
        private DevExpress.XtraEditors.ButtonEdit strInspectRefButtonEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintStemPhysical;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintStemPhysical2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintStemPhysical3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintStemDisease;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintStemDisease2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintStemDisease3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintAngleToVertical;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownPhysical;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownPhysical2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownPhysical3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownDisease;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownDisease2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownDisease3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownFoliation3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownFoliation2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintCrownFoliation;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintRootHeave;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintRootHeave2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintRootHeave3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CheckEdit NoFurtherActionRequiredCheckEdit;
        private DevExpress.XtraEditors.GridLookUpEdit intInspectorGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit intSafetyGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.GridLookUpEdit intGeneralConditionGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.GridLookUpEdit intVitalityGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.GridLookUpEdit intRootHeaveGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.GridLookUpEdit intRootHeave2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.GridLookUpEdit intRootHeave3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.GridLookUpEdit intStemPhysicalGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.GridLookUpEdit intStemPhysical2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraEditors.GridLookUpEdit intStemPhysical3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraEditors.GridLookUpEdit intStemDiseaseGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraEditors.GridLookUpEdit intStemDisease2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraEditors.GridLookUpEdit intStemDisease3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraEditors.GridLookUpEdit intCrownPhysicalGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraEditors.GridLookUpEdit intCrownPhysical2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private DevExpress.XtraEditors.GridLookUpEdit intCrownPhysical3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView16;
        private DevExpress.XtraEditors.GridLookUpEdit intCrownDiseaseGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView17;
        private DevExpress.XtraEditors.GridLookUpEdit intCrownDisease2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView18;
        private DevExpress.XtraEditors.GridLookUpEdit intCrownDisease3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView19;
        private DevExpress.XtraEditors.GridLookUpEdit intCrownFoliationGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView20;
        private DevExpress.XtraEditors.GridLookUpEdit intCrownFoliation2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView21;
        private DevExpress.XtraEditors.GridLookUpEdit intCrownFoliation3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView22;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList1GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView23;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView24;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView25;
        private System.Windows.Forms.BindingSource sp01372ATMultiplePicklistsWithBlanksBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colItemCode;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private WoodPlanDataSet woodPlanDataSet;
        private System.Windows.Forms.BindingSource sp00190ContractorListWithBlankBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00190_Contractor_List_With_BlankTableAdapter sp00190_Contractor_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn94;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn100;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn101;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn102;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn103;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn104;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn105;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn106;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn107;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn108;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn109;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn110;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn111;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn112;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn113;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn114;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn115;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn116;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn117;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn118;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn119;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn120;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn121;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn122;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn123;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn124;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn125;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn126;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn127;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn128;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn129;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn130;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn131;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn132;
        private System.Windows.Forms.BindingSource sp01375_AT_User_Screen_SettingsBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01375_AT_User_Screen_SettingsTableAdapter sp01375_AT_User_Screen_SettingsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalCOntractorDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn133;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn134;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn135;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn136;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn137;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn138;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLastInspection;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarCheckItem bciShowPrevious;
        private System.Windows.Forms.BindingSource sp01384ATInspectionEditPreviousInspectionsBindingSource;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeHouseName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeSpeciesName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeAccess;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeVisibility;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeContext;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeManagement;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeLegalStatus;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeLastInspectionDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeInspectionCycle;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeInspectionUnit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeNextInspectionDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeGroundType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeDBH;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeDBHRange;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeHeight;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeHeightRange;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeProtectionType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeCrownDiameter;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreePlantDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeGroupNumber;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeRiskCategory;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeSiteHazardClass;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreePlantSize;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreePlantSource;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreePlantMethod;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreePostcode;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeMapLabel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeStatus;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeSize;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeLastModifiedDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeRemarks;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeUser1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeUser2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeUser3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeAgeClass;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeAreaHa;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeReplantCount;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeSurveyDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeType;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeSiteLevel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeSituation;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeRiskFactor;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeOwnershipName;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01384_AT_Inspection_Edit_Previous_InspectionsTableAdapter sp01384_AT_Inspection_Edit_Previous_InspectionsTableAdapter;
        private DevExpress.XtraVerticalGrid.VGridControl vGridControl1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowInspectionID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSiteID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowClientID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowSiteName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowClientName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeReferenceButtonEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeMappingID;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeGridReference;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeDistance;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow4;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow5;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow6;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow7;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow8;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow9;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow10;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow11;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow12;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow13;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow14;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow15;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow16;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow17;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow18;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow19;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow20;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow21;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow22;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow23;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow24;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow25;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow26;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow27;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow28;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow29;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeTarget1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeTarget2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowTreeTarget3;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow30;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow31;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow32;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow33;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow34;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow35;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow36;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow37;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow38;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow editorRow39;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintAngleToVerticalSpinEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowstrUser1TextEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowstrUser2TextEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowstrUser3TextEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowstrRemarksMemoEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintStemDiseaseGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintStemDisease2GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintStemDisease3GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintCrownPhysicalGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintCrownPhysical2GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintCrownPhysical3GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintCrownDiseaseGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintCrownDisease2GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintCrownDisease3GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintCrownFoliationGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintCrownFoliation2GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintCrownFoliation3GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLinkedActionCount;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowLinkedOutstandingActionCount;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowUserPickList1GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowUserPickList2GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowUserPickList3GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowstrInspectRefButtonEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintInspectorGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowdtInspectDateDateEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowIncidentDescriptionButtonEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowInspectionIncidentDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintIncidentIDTextEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintGeneralConditionGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintVitalityGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowNoFurtherActionRequiredCheckEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintSafetyGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintRootHeaveGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintRootHeave2GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintRootHeave3GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintStemPhysicalGridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintStemPhysical2GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow rowintStemPhysical3GridLookUpEdit;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow3;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow4;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow5;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow categoryRow6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEditSychronise;
        private System.Windows.Forms.BindingSource sp01380ATActionsLinkedToInspectionsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalityID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDistrictID2;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeMappingID1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAction;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBy;
        private DevExpress.XtraGrid.Columns.GridColumn colActionSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colActionOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudget;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colActionScheduleOfRates;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionBudgetedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualRate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActionWorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colActionUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCostDifference;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCompletionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionTimeliness;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01380_AT_Actions_Linked_To_InspectionsTableAdapter sp01380_AT_Actions_Linked_To_InspectionsTableAdapter;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.BarButtonItem bbiCopyActions;
        private DevExpress.XtraBars.BarButtonItem bbiPasteActions;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView26;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn139;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraGrid.GridControl gridControl39;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn169;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn170;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn171;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn172;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn173;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn174;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn175;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn176;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn177;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn178;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn179;
        private DevExpress.XtraGrid.Columns.GridColumn colShortLinkedRecordDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedPicturesGrid;
        private System.Windows.Forms.BindingSource sp02065ATInspectionPicturesListBindingSource;
        private DataSet_ATTableAdapters.sp02065_AT_Inspection_Pictures_ListTableAdapter sp02065_AT_Inspection_Pictures_ListTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending39;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
    }
}

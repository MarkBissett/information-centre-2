namespace WoodPlan5
{
    partial class frm_AT_WorkOrder_Analysis_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup1 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup2 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup3 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup4 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup5 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_WorkOrder_Analysis_1));
            this.fieldCalculatedWorkOrderIssueYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedWorkOrderIssueQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedWorkOrderIssueMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedWorkOrderCompleteYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedWorkOrderCompleteQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedWorkOrderCompleteMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedJobDueYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedJobDueQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedJobDueMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedJobDoneYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedJobDoneQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedJobDoneMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedWorkOrderTargetYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedWorkOrderTargetQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCalculatedWorkOrderTargetMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableTableAdapter();
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobInspectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobInspectionReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSpeciesAcronym = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCarriedOutBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSupervisor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobPreviousWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobNearestHouse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobPriority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobRateBudgeted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobCostBudgeted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobRateActual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCostActual = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobPreviousActionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobBudgetDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobRateDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintBudgetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDiscountRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colJobRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderPreviousID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderPreviousNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderIssueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderCompleteDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Calculated3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserPicklist3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderTargetDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnRefreshList = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditTo = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.sp01218ATWorkOrdersAnalysisJobsAnalyseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fieldJobActionID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobInspectionID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobClientName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobSiteName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobTreeReference = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobInspectionReference = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobMapID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobSpecies = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobClientID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobSiteID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobTreeID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobSpeciesAcronym = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobDueDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobDoneDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobDescription = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobCarriedOutBy = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobSupervisor = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobNumber = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobUserDefined1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobUserDefined2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobUserDefined3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobWorkOrderID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobPreviousWorkOrderID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobNearestHouse = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobPriority = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobWorkUnits = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobRateBudgeted = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobCostBudgeted = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobRateActual = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobCostActual = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobSortOrder = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobPreviousActionNumber = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldColour = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobBudgetDescription = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobRateDescription = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldintBudgetID = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobDiscountRate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobRemarks = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderNumber = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderPreviousNumber = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderIssueDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderInvoiceNumber = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderUserDefined1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderUserDefined2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderUserDefined3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderCompleteDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobCostCentreCode = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobOwnership = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStatus = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldUserPicklist1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldUserPicklist2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldUserPicklist3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderReference = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderTargetDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.sp01218_AT_WorkOrders_Analysis_Jobs_AnalyseTableAdapter = new WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01218_AT_WorkOrders_Analysis_Jobs_AnalyseTableAdapter();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.pmChart = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiRotateAxis = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChartWizard = new DevExpress.XtraBars.BarButtonItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01218ATWorkOrdersAnalysisJobsAnalyseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1329, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 804);
            this.barDockControlBottom.Size = new System.Drawing.Size(1329, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 804);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1329, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 804);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.bbiChartWizard,
            this.bbiRotateAxis});
            this.barManager1.MaxItemId = 28;
            // 
            // fieldCalculatedWorkOrderIssueYear
            // 
            this.fieldCalculatedWorkOrderIssueYear.AreaIndex = 33;
            this.fieldCalculatedWorkOrderIssueYear.Caption = "Work Order Issue Year";
            this.fieldCalculatedWorkOrderIssueYear.ExpandedInFieldsGroup = false;
            this.fieldCalculatedWorkOrderIssueYear.FieldName = "WorkOrderIssueDate";
            this.fieldCalculatedWorkOrderIssueYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldCalculatedWorkOrderIssueYear.Name = "fieldCalculatedWorkOrderIssueYear";
            this.fieldCalculatedWorkOrderIssueYear.UnboundFieldName = "fieldCalculatedWorkOrderIssueYear";
            // 
            // fieldCalculatedWorkOrderIssueQuarter
            // 
            this.fieldCalculatedWorkOrderIssueQuarter.AreaIndex = 1;
            this.fieldCalculatedWorkOrderIssueQuarter.Caption = "Work Order Issue Quarter";
            this.fieldCalculatedWorkOrderIssueQuarter.FieldName = "WorkOrderIssueDate";
            this.fieldCalculatedWorkOrderIssueQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldCalculatedWorkOrderIssueQuarter.Name = "fieldCalculatedWorkOrderIssueQuarter";
            this.fieldCalculatedWorkOrderIssueQuarter.UnboundFieldName = "fieldCalculatedWorkOrderIssueQuarter";
            this.fieldCalculatedWorkOrderIssueQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldCalculatedWorkOrderIssueQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCalculatedWorkOrderIssueQuarter.Visible = false;
            // 
            // fieldCalculatedWorkOrderIssueMonth
            // 
            this.fieldCalculatedWorkOrderIssueMonth.AreaIndex = 0;
            this.fieldCalculatedWorkOrderIssueMonth.Caption = "Work Order Month";
            this.fieldCalculatedWorkOrderIssueMonth.FieldName = "WorkOrderIssueDate";
            this.fieldCalculatedWorkOrderIssueMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldCalculatedWorkOrderIssueMonth.Name = "fieldCalculatedWorkOrderIssueMonth";
            this.fieldCalculatedWorkOrderIssueMonth.UnboundFieldName = "fieldCalculatedWorkOrderIssueMonth";
            this.fieldCalculatedWorkOrderIssueMonth.Visible = false;
            // 
            // fieldCalculatedWorkOrderCompleteYear
            // 
            this.fieldCalculatedWorkOrderCompleteYear.AreaIndex = 35;
            this.fieldCalculatedWorkOrderCompleteYear.Caption = "Work Order Complete Year";
            this.fieldCalculatedWorkOrderCompleteYear.ExpandedInFieldsGroup = false;
            this.fieldCalculatedWorkOrderCompleteYear.FieldName = "WorkOrderCompleteDate";
            this.fieldCalculatedWorkOrderCompleteYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldCalculatedWorkOrderCompleteYear.Name = "fieldCalculatedWorkOrderCompleteYear";
            this.fieldCalculatedWorkOrderCompleteYear.UnboundFieldName = "fieldCalculatedWorkOrderCompleteYear";
            // 
            // fieldCalculatedWorkOrderCompleteQuarter
            // 
            this.fieldCalculatedWorkOrderCompleteQuarter.AreaIndex = 0;
            this.fieldCalculatedWorkOrderCompleteQuarter.Caption = "Work Order Complete Quarter";
            this.fieldCalculatedWorkOrderCompleteQuarter.FieldName = "WorkOrderCompleteDate";
            this.fieldCalculatedWorkOrderCompleteQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldCalculatedWorkOrderCompleteQuarter.Name = "fieldCalculatedWorkOrderCompleteQuarter";
            this.fieldCalculatedWorkOrderCompleteQuarter.UnboundFieldName = "fieldCalculatedWorkOrderCompleteQuarter";
            this.fieldCalculatedWorkOrderCompleteQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldCalculatedWorkOrderCompleteQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCalculatedWorkOrderCompleteQuarter.Visible = false;
            // 
            // fieldCalculatedWorkOrderCompleteMonth
            // 
            this.fieldCalculatedWorkOrderCompleteMonth.AreaIndex = 0;
            this.fieldCalculatedWorkOrderCompleteMonth.Caption = "Work Order Complete Month";
            this.fieldCalculatedWorkOrderCompleteMonth.FieldName = "WorkOrderCompleteDate";
            this.fieldCalculatedWorkOrderCompleteMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldCalculatedWorkOrderCompleteMonth.Name = "fieldCalculatedWorkOrderCompleteMonth";
            this.fieldCalculatedWorkOrderCompleteMonth.UnboundFieldName = "fieldCalculatedWorkOrderCompleteMonth";
            this.fieldCalculatedWorkOrderCompleteMonth.Visible = false;
            // 
            // fieldCalculatedJobDueYear
            // 
            this.fieldCalculatedJobDueYear.AreaIndex = 36;
            this.fieldCalculatedJobDueYear.Caption = "Job Due Year";
            this.fieldCalculatedJobDueYear.ExpandedInFieldsGroup = false;
            this.fieldCalculatedJobDueYear.FieldName = "JobDueDate";
            this.fieldCalculatedJobDueYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldCalculatedJobDueYear.Name = "fieldCalculatedJobDueYear";
            this.fieldCalculatedJobDueYear.UnboundFieldName = "fieldCalculatedJobDueYear";
            // 
            // fieldCalculatedJobDueQuarter
            // 
            this.fieldCalculatedJobDueQuarter.AreaIndex = 1;
            this.fieldCalculatedJobDueQuarter.Caption = "Job Due Quarter";
            this.fieldCalculatedJobDueQuarter.FieldName = "JobDueDate";
            this.fieldCalculatedJobDueQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldCalculatedJobDueQuarter.Name = "fieldCalculatedJobDueQuarter";
            this.fieldCalculatedJobDueQuarter.UnboundFieldName = "fieldCalculatedJobDueQuarter";
            this.fieldCalculatedJobDueQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldCalculatedJobDueQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCalculatedJobDueQuarter.Visible = false;
            // 
            // fieldCalculatedJobDueMonth
            // 
            this.fieldCalculatedJobDueMonth.AreaIndex = 0;
            this.fieldCalculatedJobDueMonth.Caption = "Job Due Month";
            this.fieldCalculatedJobDueMonth.FieldName = "JobDueDate";
            this.fieldCalculatedJobDueMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldCalculatedJobDueMonth.Name = "fieldCalculatedJobDueMonth";
            this.fieldCalculatedJobDueMonth.UnboundFieldName = "fieldCalculatedJobDueMonth";
            this.fieldCalculatedJobDueMonth.Visible = false;
            // 
            // fieldCalculatedJobDoneYear
            // 
            this.fieldCalculatedJobDoneYear.AreaIndex = 37;
            this.fieldCalculatedJobDoneYear.Caption = "Job Done Year";
            this.fieldCalculatedJobDoneYear.ExpandedInFieldsGroup = false;
            this.fieldCalculatedJobDoneYear.FieldName = "JobDoneDate";
            this.fieldCalculatedJobDoneYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldCalculatedJobDoneYear.Name = "fieldCalculatedJobDoneYear";
            this.fieldCalculatedJobDoneYear.UnboundFieldName = "fieldCalculatedJobDoneYear";
            // 
            // fieldCalculatedJobDoneQuarter
            // 
            this.fieldCalculatedJobDoneQuarter.AreaIndex = 0;
            this.fieldCalculatedJobDoneQuarter.Caption = "Job Done Quarter";
            this.fieldCalculatedJobDoneQuarter.FieldName = "JobDoneDate";
            this.fieldCalculatedJobDoneQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldCalculatedJobDoneQuarter.Name = "fieldCalculatedJobDoneQuarter";
            this.fieldCalculatedJobDoneQuarter.UnboundFieldName = "fieldCalculatedJobDoneQuarter";
            this.fieldCalculatedJobDoneQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldCalculatedJobDoneQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldCalculatedJobDoneQuarter.Visible = false;
            // 
            // fieldCalculatedJobDoneMonth
            // 
            this.fieldCalculatedJobDoneMonth.AreaIndex = 0;
            this.fieldCalculatedJobDoneMonth.Caption = "Job Done Month";
            this.fieldCalculatedJobDoneMonth.FieldName = "JobDoneDate";
            this.fieldCalculatedJobDoneMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldCalculatedJobDoneMonth.Name = "fieldCalculatedJobDoneMonth";
            this.fieldCalculatedJobDoneMonth.UnboundFieldName = "fieldCalculatedJobDoneMonth";
            this.fieldCalculatedJobDoneMonth.Visible = false;
            // 
            // fieldCalculatedWorkOrderTargetYear
            // 
            this.fieldCalculatedWorkOrderTargetYear.AreaIndex = 34;
            this.fieldCalculatedWorkOrderTargetYear.Caption = "Work Order Target Year";
            this.fieldCalculatedWorkOrderTargetYear.ExpandedInFieldsGroup = false;
            this.fieldCalculatedWorkOrderTargetYear.FieldName = "WorkOrderTargetDate";
            this.fieldCalculatedWorkOrderTargetYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldCalculatedWorkOrderTargetYear.Name = "fieldCalculatedWorkOrderTargetYear";
            this.fieldCalculatedWorkOrderTargetYear.UnboundFieldName = "fieldCalculatedWorkOrderTargetYear";
            // 
            // fieldCalculatedWorkOrderTargetQuarter
            // 
            this.fieldCalculatedWorkOrderTargetQuarter.AreaIndex = 37;
            this.fieldCalculatedWorkOrderTargetQuarter.Caption = "Work Order Target Quarter";
            this.fieldCalculatedWorkOrderTargetQuarter.ExpandedInFieldsGroup = false;
            this.fieldCalculatedWorkOrderTargetQuarter.FieldName = "WorkOrderTargetDate";
            this.fieldCalculatedWorkOrderTargetQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldCalculatedWorkOrderTargetQuarter.Name = "fieldCalculatedWorkOrderTargetQuarter";
            this.fieldCalculatedWorkOrderTargetQuarter.UnboundFieldName = "pivotGridField1";
            this.fieldCalculatedWorkOrderTargetQuarter.Visible = false;
            // 
            // fieldCalculatedWorkOrderTargetMonth
            // 
            this.fieldCalculatedWorkOrderTargetMonth.AreaIndex = 37;
            this.fieldCalculatedWorkOrderTargetMonth.Caption = "Work Order Target Month";
            this.fieldCalculatedWorkOrderTargetMonth.ExpandedInFieldsGroup = false;
            this.fieldCalculatedWorkOrderTargetMonth.FieldName = "WorkOrderTargetDate";
            this.fieldCalculatedWorkOrderTargetMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldCalculatedWorkOrderTargetMonth.Name = "fieldCalculatedWorkOrderTargetMonth";
            this.fieldCalculatedWorkOrderTargetMonth.UnboundFieldName = "pivotGridField2";
            this.fieldCalculatedWorkOrderTargetMonth.Visible = false;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp01209_AT_WorkOrders_Analysis_Jobs_AvailableBindingSource
            // 
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableBindingSource.DataMember = "sp01209_AT_WorkOrders_Analysis_Jobs_Available";
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // sp01209_AT_WorkOrders_Analysis_Jobs_AvailableTableAdapter
            // 
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableTableAdapter.ClearBeforeFill = true;
            // 
            // sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl
            // 
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.DataSource = this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableBindingSource;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.Location = new System.Drawing.Point(3, 54);
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.MainView = this.gridView1;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.MenuManager = this.barManager1;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.Name = "sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl";
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.Size = new System.Drawing.Size(293, 701);
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.TabIndex = 6;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.UseEmbeddedNavigator = true;
            this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobActionID,
            this.colJobInspectionID,
            this.colJobClientName,
            this.colJobSiteName,
            this.colJobTreeReference,
            this.colJobInspectionReference,
            this.colJobMapID,
            this.colJobSpecies,
            this.colJobClientID,
            this.colJobSiteID,
            this.colJobTreeID,
            this.colJobSpeciesAcronym,
            this.colJobDueDate,
            this.colJobDoneDate,
            this.colJobDescription,
            this.colJobCarriedOutBy,
            this.colJobSupervisor,
            this.colJobNumber,
            this.colJobUserDefined1,
            this.colJobUserDefined2,
            this.colJobUserDefined3,
            this.colJobWorkOrderID,
            this.colJobPreviousWorkOrderID,
            this.colJobNearestHouse,
            this.colJobPriority,
            this.colJobWorkUnits,
            this.colJobRateBudgeted,
            this.colJobCostBudgeted,
            this.colJobRateActual,
            this.colJobCostActual,
            this.colJobSortOrder,
            this.colJobPreviousActionNumber,
            this.colColour,
            this.colJobBudgetDescription,
            this.colJobRateDescription,
            this.colJobActualCost,
            this.colintBudgetID,
            this.colJobDiscountRate,
            this.colJobRemarks,
            this.colWorkOrderNumber,
            this.colWorkOrderPreviousID,
            this.colWorkOrderPreviousNumber,
            this.colWorkOrderIssueDate,
            this.colWorkOrderInvoiceNumber,
            this.colWorkOrderUserDefined1,
            this.colWorkOrderUserDefined2,
            this.colWorkOrderUserDefined3,
            this.colWorkOrderCompleteDate,
            this.Calculated1,
            this.Calculated2,
            this.Calculated3,
            this.colStatus,
            this.colUserPicklist1,
            this.colUserPicklist2,
            this.colUserPicklist3,
            this.colWorkOrderReference,
            this.colWorkOrderTargetDate});
            this.gridView1.GridControl = this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colWorkOrderNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colJobActionID
            // 
            this.colJobActionID.Caption = "Action ID";
            this.colJobActionID.FieldName = "JobActionID";
            this.colJobActionID.Name = "colJobActionID";
            this.colJobActionID.OptionsColumn.AllowEdit = false;
            this.colJobActionID.OptionsColumn.AllowFocus = false;
            this.colJobActionID.OptionsColumn.ReadOnly = true;
            this.colJobActionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobActionID.Width = 128;
            // 
            // colJobInspectionID
            // 
            this.colJobInspectionID.Caption = "Inspection ID";
            this.colJobInspectionID.FieldName = "JobInspectionID";
            this.colJobInspectionID.Name = "colJobInspectionID";
            this.colJobInspectionID.OptionsColumn.AllowEdit = false;
            this.colJobInspectionID.OptionsColumn.AllowFocus = false;
            this.colJobInspectionID.OptionsColumn.ReadOnly = true;
            this.colJobInspectionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobInspectionID.Width = 162;
            // 
            // colJobClientName
            // 
            this.colJobClientName.Caption = "Client Name";
            this.colJobClientName.FieldName = "JobClientName";
            this.colJobClientName.Name = "colJobClientName";
            this.colJobClientName.OptionsColumn.AllowEdit = false;
            this.colJobClientName.OptionsColumn.AllowFocus = false;
            this.colJobClientName.OptionsColumn.ReadOnly = true;
            this.colJobClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobClientName.Visible = true;
            this.colJobClientName.VisibleIndex = 7;
            this.colJobClientName.Width = 108;
            // 
            // colJobSiteName
            // 
            this.colJobSiteName.Caption = "Site Name";
            this.colJobSiteName.FieldName = "JobSiteName";
            this.colJobSiteName.Name = "colJobSiteName";
            this.colJobSiteName.OptionsColumn.AllowEdit = false;
            this.colJobSiteName.OptionsColumn.AllowFocus = false;
            this.colJobSiteName.OptionsColumn.ReadOnly = true;
            this.colJobSiteName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobSiteName.Visible = true;
            this.colJobSiteName.VisibleIndex = 8;
            this.colJobSiteName.Width = 126;
            // 
            // colJobTreeReference
            // 
            this.colJobTreeReference.Caption = "Tree Reference";
            this.colJobTreeReference.FieldName = "JobTreeReference";
            this.colJobTreeReference.Name = "colJobTreeReference";
            this.colJobTreeReference.OptionsColumn.AllowEdit = false;
            this.colJobTreeReference.OptionsColumn.AllowFocus = false;
            this.colJobTreeReference.OptionsColumn.ReadOnly = true;
            this.colJobTreeReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobTreeReference.Visible = true;
            this.colJobTreeReference.VisibleIndex = 9;
            this.colJobTreeReference.Width = 99;
            // 
            // colJobInspectionReference
            // 
            this.colJobInspectionReference.Caption = "Inspection Reference";
            this.colJobInspectionReference.FieldName = "JobInspectionReference";
            this.colJobInspectionReference.Name = "colJobInspectionReference";
            this.colJobInspectionReference.OptionsColumn.AllowEdit = false;
            this.colJobInspectionReference.OptionsColumn.AllowFocus = false;
            this.colJobInspectionReference.OptionsColumn.ReadOnly = true;
            this.colJobInspectionReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobInspectionReference.Visible = true;
            this.colJobInspectionReference.VisibleIndex = 10;
            this.colJobInspectionReference.Width = 125;
            // 
            // colJobMapID
            // 
            this.colJobMapID.Caption = "Map ID";
            this.colJobMapID.FieldName = "JobMapID";
            this.colJobMapID.Name = "colJobMapID";
            this.colJobMapID.OptionsColumn.AllowEdit = false;
            this.colJobMapID.OptionsColumn.AllowFocus = false;
            this.colJobMapID.OptionsColumn.ReadOnly = true;
            this.colJobMapID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobSpecies
            // 
            this.colJobSpecies.Caption = "Species";
            this.colJobSpecies.FieldName = "JobSpecies";
            this.colJobSpecies.Name = "colJobSpecies";
            this.colJobSpecies.OptionsColumn.AllowEdit = false;
            this.colJobSpecies.OptionsColumn.AllowFocus = false;
            this.colJobSpecies.OptionsColumn.ReadOnly = true;
            this.colJobSpecies.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobSpecies.Visible = true;
            this.colJobSpecies.VisibleIndex = 11;
            this.colJobSpecies.Width = 109;
            // 
            // colJobClientID
            // 
            this.colJobClientID.Caption = "Client ID";
            this.colJobClientID.FieldName = "JobClienttID";
            this.colJobClientID.Name = "colJobClientID";
            this.colJobClientID.OptionsColumn.AllowEdit = false;
            this.colJobClientID.OptionsColumn.AllowFocus = false;
            this.colJobClientID.OptionsColumn.ReadOnly = true;
            this.colJobClientID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobSiteID
            // 
            this.colJobSiteID.Caption = "Site ID";
            this.colJobSiteID.FieldName = "JobSiteID";
            this.colJobSiteID.Name = "colJobSiteID";
            this.colJobSiteID.OptionsColumn.AllowEdit = false;
            this.colJobSiteID.OptionsColumn.AllowFocus = false;
            this.colJobSiteID.OptionsColumn.ReadOnly = true;
            this.colJobSiteID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobTreeID
            // 
            this.colJobTreeID.Caption = "Tree ID";
            this.colJobTreeID.FieldName = "JobTreeID";
            this.colJobTreeID.Name = "colJobTreeID";
            this.colJobTreeID.OptionsColumn.AllowEdit = false;
            this.colJobTreeID.OptionsColumn.AllowFocus = false;
            this.colJobTreeID.OptionsColumn.ReadOnly = true;
            this.colJobTreeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobSpeciesAcronym
            // 
            this.colJobSpeciesAcronym.Caption = "Species Acronym";
            this.colJobSpeciesAcronym.FieldName = "JobSpeciesAcronym";
            this.colJobSpeciesAcronym.Name = "colJobSpeciesAcronym";
            this.colJobSpeciesAcronym.OptionsColumn.AllowEdit = false;
            this.colJobSpeciesAcronym.OptionsColumn.AllowFocus = false;
            this.colJobSpeciesAcronym.OptionsColumn.ReadOnly = true;
            this.colJobSpeciesAcronym.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobDueDate
            // 
            this.colJobDueDate.Caption = "Due Date";
            this.colJobDueDate.FieldName = "JobDueDate";
            this.colJobDueDate.Name = "colJobDueDate";
            this.colJobDueDate.OptionsColumn.AllowEdit = false;
            this.colJobDueDate.OptionsColumn.AllowFocus = false;
            this.colJobDueDate.OptionsColumn.ReadOnly = true;
            this.colJobDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobDueDate.Visible = true;
            this.colJobDueDate.VisibleIndex = 2;
            this.colJobDueDate.Width = 87;
            // 
            // colJobDoneDate
            // 
            this.colJobDoneDate.Caption = "Done Date";
            this.colJobDoneDate.FieldName = "JobDoneDate";
            this.colJobDoneDate.Name = "colJobDoneDate";
            this.colJobDoneDate.OptionsColumn.AllowEdit = false;
            this.colJobDoneDate.OptionsColumn.AllowFocus = false;
            this.colJobDoneDate.OptionsColumn.ReadOnly = true;
            this.colJobDoneDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobDoneDate.Visible = true;
            this.colJobDoneDate.VisibleIndex = 4;
            this.colJobDoneDate.Width = 89;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 0;
            this.colJobDescription.Width = 130;
            // 
            // colJobCarriedOutBy
            // 
            this.colJobCarriedOutBy.Caption = "Job Carried Out By";
            this.colJobCarriedOutBy.FieldName = "JobCarriedOutBy";
            this.colJobCarriedOutBy.Name = "colJobCarriedOutBy";
            this.colJobCarriedOutBy.OptionsColumn.AllowEdit = false;
            this.colJobCarriedOutBy.OptionsColumn.AllowFocus = false;
            this.colJobCarriedOutBy.OptionsColumn.ReadOnly = true;
            this.colJobCarriedOutBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobCarriedOutBy.Visible = true;
            this.colJobCarriedOutBy.VisibleIndex = 6;
            this.colJobCarriedOutBy.Width = 119;
            // 
            // colJobSupervisor
            // 
            this.colJobSupervisor.Caption = "Job Supervisor";
            this.colJobSupervisor.FieldName = "JobSupervisor";
            this.colJobSupervisor.Name = "colJobSupervisor";
            this.colJobSupervisor.OptionsColumn.AllowEdit = false;
            this.colJobSupervisor.OptionsColumn.AllowFocus = false;
            this.colJobSupervisor.OptionsColumn.ReadOnly = true;
            this.colJobSupervisor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobSupervisor.Visible = true;
            this.colJobSupervisor.VisibleIndex = 5;
            this.colJobSupervisor.Width = 91;
            // 
            // colJobNumber
            // 
            this.colJobNumber.Caption = "Job Number";
            this.colJobNumber.FieldName = "JobNumber";
            this.colJobNumber.Name = "colJobNumber";
            this.colJobNumber.OptionsColumn.AllowEdit = false;
            this.colJobNumber.OptionsColumn.AllowFocus = false;
            this.colJobNumber.OptionsColumn.ReadOnly = true;
            this.colJobNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobNumber.Visible = true;
            this.colJobNumber.VisibleIndex = 12;
            this.colJobNumber.Width = 88;
            // 
            // colJobUserDefined1
            // 
            this.colJobUserDefined1.Caption = "Job User Defined 1";
            this.colJobUserDefined1.FieldName = "JobUserDefined1";
            this.colJobUserDefined1.Name = "colJobUserDefined1";
            this.colJobUserDefined1.OptionsColumn.AllowEdit = false;
            this.colJobUserDefined1.OptionsColumn.AllowFocus = false;
            this.colJobUserDefined1.OptionsColumn.ReadOnly = true;
            this.colJobUserDefined1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobUserDefined2
            // 
            this.colJobUserDefined2.Caption = "Job User Defined 2";
            this.colJobUserDefined2.FieldName = "JobUserDefined2";
            this.colJobUserDefined2.Name = "colJobUserDefined2";
            this.colJobUserDefined2.OptionsColumn.AllowEdit = false;
            this.colJobUserDefined2.OptionsColumn.AllowFocus = false;
            this.colJobUserDefined2.OptionsColumn.ReadOnly = true;
            this.colJobUserDefined2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobUserDefined3
            // 
            this.colJobUserDefined3.Caption = "Job User Defined 3";
            this.colJobUserDefined3.FieldName = "JobUserDefined3";
            this.colJobUserDefined3.Name = "colJobUserDefined3";
            this.colJobUserDefined3.OptionsColumn.AllowEdit = false;
            this.colJobUserDefined3.OptionsColumn.AllowFocus = false;
            this.colJobUserDefined3.OptionsColumn.ReadOnly = true;
            this.colJobUserDefined3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobUserDefined3.Width = 162;
            // 
            // colJobWorkOrderID
            // 
            this.colJobWorkOrderID.Caption = "Work Order ID";
            this.colJobWorkOrderID.FieldName = "JobWorkOrderID";
            this.colJobWorkOrderID.Name = "colJobWorkOrderID";
            this.colJobWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colJobWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colJobWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colJobWorkOrderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobPreviousWorkOrderID
            // 
            this.colJobPreviousWorkOrderID.Caption = "Previous WorkOrder ID";
            this.colJobPreviousWorkOrderID.FieldName = "JobPreviousWorkOrderID";
            this.colJobPreviousWorkOrderID.Name = "colJobPreviousWorkOrderID";
            this.colJobPreviousWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colJobPreviousWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colJobPreviousWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colJobPreviousWorkOrderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobNearestHouse
            // 
            this.colJobNearestHouse.Caption = "Nearest House";
            this.colJobNearestHouse.FieldName = "JobNearestHouse";
            this.colJobNearestHouse.Name = "colJobNearestHouse";
            this.colJobNearestHouse.OptionsColumn.AllowEdit = false;
            this.colJobNearestHouse.OptionsColumn.AllowFocus = false;
            this.colJobNearestHouse.OptionsColumn.ReadOnly = true;
            this.colJobNearestHouse.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobPriority
            // 
            this.colJobPriority.Caption = "Priority";
            this.colJobPriority.FieldName = "JobPriority";
            this.colJobPriority.Name = "colJobPriority";
            this.colJobPriority.OptionsColumn.AllowEdit = false;
            this.colJobPriority.OptionsColumn.AllowFocus = false;
            this.colJobPriority.OptionsColumn.ReadOnly = true;
            this.colJobPriority.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobPriority.Visible = true;
            this.colJobPriority.VisibleIndex = 1;
            // 
            // colJobWorkUnits
            // 
            this.colJobWorkUnits.Caption = "Work Units";
            this.colJobWorkUnits.FieldName = "JobWorkUnits";
            this.colJobWorkUnits.Name = "colJobWorkUnits";
            this.colJobWorkUnits.OptionsColumn.AllowEdit = false;
            this.colJobWorkUnits.OptionsColumn.AllowFocus = false;
            this.colJobWorkUnits.OptionsColumn.ReadOnly = true;
            this.colJobWorkUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobRateBudgeted
            // 
            this.colJobRateBudgeted.Caption = "Job Rate Budgeted";
            this.colJobRateBudgeted.ColumnEdit = this.repositoryItemTextEdit1;
            this.colJobRateBudgeted.FieldName = "JobRateBudgeted";
            this.colJobRateBudgeted.Name = "colJobRateBudgeted";
            this.colJobRateBudgeted.OptionsColumn.AllowEdit = false;
            this.colJobRateBudgeted.OptionsColumn.AllowFocus = false;
            this.colJobRateBudgeted.OptionsColumn.ReadOnly = true;
            this.colJobRateBudgeted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colJobCostBudgeted
            // 
            this.colJobCostBudgeted.Caption = "Job Cost Budgeted";
            this.colJobCostBudgeted.ColumnEdit = this.repositoryItemTextEdit1;
            this.colJobCostBudgeted.FieldName = "JobCostBudgeted";
            this.colJobCostBudgeted.Name = "colJobCostBudgeted";
            this.colJobCostBudgeted.OptionsColumn.AllowEdit = false;
            this.colJobCostBudgeted.OptionsColumn.AllowFocus = false;
            this.colJobCostBudgeted.OptionsColumn.ReadOnly = true;
            this.colJobCostBudgeted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobRateActual
            // 
            this.colJobRateActual.Caption = "Job Rate Actual";
            this.colJobRateActual.ColumnEdit = this.repositoryItemTextEdit1;
            this.colJobRateActual.FieldName = "JobRateActual";
            this.colJobRateActual.Name = "colJobRateActual";
            this.colJobRateActual.OptionsColumn.AllowEdit = false;
            this.colJobRateActual.OptionsColumn.AllowFocus = false;
            this.colJobRateActual.OptionsColumn.ReadOnly = true;
            this.colJobRateActual.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobCostActual
            // 
            this.colJobCostActual.Caption = "Job Cost Actual";
            this.colJobCostActual.ColumnEdit = this.repositoryItemTextEdit1;
            this.colJobCostActual.FieldName = "JobCostActual";
            this.colJobCostActual.Name = "colJobCostActual";
            this.colJobCostActual.OptionsColumn.AllowEdit = false;
            this.colJobCostActual.OptionsColumn.AllowFocus = false;
            this.colJobCostActual.OptionsColumn.ReadOnly = true;
            this.colJobCostActual.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobSortOrder
            // 
            this.colJobSortOrder.Caption = "Job Sort Order";
            this.colJobSortOrder.FieldName = "JobSortOrder";
            this.colJobSortOrder.Name = "colJobSortOrder";
            this.colJobSortOrder.OptionsColumn.AllowEdit = false;
            this.colJobSortOrder.OptionsColumn.AllowFocus = false;
            this.colJobSortOrder.OptionsColumn.ReadOnly = true;
            this.colJobSortOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobPreviousActionNumber
            // 
            this.colJobPreviousActionNumber.Caption = "Previous Action Number";
            this.colJobPreviousActionNumber.FieldName = "JobPreviousActionNumber";
            this.colJobPreviousActionNumber.Name = "colJobPreviousActionNumber";
            this.colJobPreviousActionNumber.OptionsColumn.AllowEdit = false;
            this.colJobPreviousActionNumber.OptionsColumn.AllowFocus = false;
            this.colJobPreviousActionNumber.OptionsColumn.ReadOnly = true;
            this.colJobPreviousActionNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colColour
            // 
            this.colColour.Caption = "Colour";
            this.colColour.FieldName = "Colour";
            this.colColour.Name = "colColour";
            this.colColour.OptionsColumn.AllowEdit = false;
            this.colColour.OptionsColumn.AllowFocus = false;
            this.colColour.OptionsColumn.ReadOnly = true;
            this.colColour.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobBudgetDescription
            // 
            this.colJobBudgetDescription.Caption = "Budget Description";
            this.colJobBudgetDescription.FieldName = "JobBudgetDescription";
            this.colJobBudgetDescription.Name = "colJobBudgetDescription";
            this.colJobBudgetDescription.OptionsColumn.AllowEdit = false;
            this.colJobBudgetDescription.OptionsColumn.AllowFocus = false;
            this.colJobBudgetDescription.OptionsColumn.ReadOnly = true;
            this.colJobBudgetDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobRateDescription
            // 
            this.colJobRateDescription.Caption = "Job Rate Description";
            this.colJobRateDescription.FieldName = "JobRateDescription";
            this.colJobRateDescription.Name = "colJobRateDescription";
            this.colJobRateDescription.OptionsColumn.AllowEdit = false;
            this.colJobRateDescription.OptionsColumn.AllowFocus = false;
            this.colJobRateDescription.OptionsColumn.ReadOnly = true;
            this.colJobRateDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobActualCost
            // 
            this.colJobActualCost.Caption = "Job Actual Cost";
            this.colJobActualCost.FieldName = "JobActualCost";
            this.colJobActualCost.Name = "colJobActualCost";
            this.colJobActualCost.OptionsColumn.AllowEdit = false;
            this.colJobActualCost.OptionsColumn.AllowFocus = false;
            this.colJobActualCost.OptionsColumn.ReadOnly = true;
            this.colJobActualCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colintBudgetID
            // 
            this.colintBudgetID.Caption = "Budget ID";
            this.colintBudgetID.FieldName = "intBudgetID";
            this.colintBudgetID.Name = "colintBudgetID";
            this.colintBudgetID.OptionsColumn.AllowEdit = false;
            this.colintBudgetID.OptionsColumn.AllowFocus = false;
            this.colintBudgetID.OptionsColumn.ReadOnly = true;
            this.colintBudgetID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobDiscountRate
            // 
            this.colJobDiscountRate.Caption = "Job Discount Rate";
            this.colJobDiscountRate.ColumnEdit = this.repositoryItemTextEdit2;
            this.colJobDiscountRate.FieldName = "JobDiscountRate";
            this.colJobDiscountRate.Name = "colJobDiscountRate";
            this.colJobDiscountRate.OptionsColumn.AllowEdit = false;
            this.colJobDiscountRate.OptionsColumn.AllowFocus = false;
            this.colJobDiscountRate.OptionsColumn.ReadOnly = true;
            this.colJobDiscountRate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "P";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colJobRemarks
            // 
            this.colJobRemarks.Caption = "Job Remarks";
            this.colJobRemarks.FieldName = "JobRemarks";
            this.colJobRemarks.Name = "colJobRemarks";
            this.colJobRemarks.OptionsColumn.AllowEdit = false;
            this.colJobRemarks.OptionsColumn.AllowFocus = false;
            this.colJobRemarks.OptionsColumn.ReadOnly = true;
            this.colJobRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colWorkOrderNumber
            // 
            this.colWorkOrderNumber.Caption = "Work Order Number";
            this.colWorkOrderNumber.FieldName = "WorkOrderNumber";
            this.colWorkOrderNumber.Name = "colWorkOrderNumber";
            this.colWorkOrderNumber.OptionsColumn.AllowEdit = false;
            this.colWorkOrderNumber.OptionsColumn.AllowFocus = false;
            this.colWorkOrderNumber.OptionsColumn.ReadOnly = true;
            this.colWorkOrderNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colWorkOrderPreviousID
            // 
            this.colWorkOrderPreviousID.Caption = "WorkOrderPreviousID";
            this.colWorkOrderPreviousID.FieldName = "WorkOrderPreviousID";
            this.colWorkOrderPreviousID.Name = "colWorkOrderPreviousID";
            this.colWorkOrderPreviousID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderPreviousID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderPreviousID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderPreviousID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colWorkOrderPreviousNumber
            // 
            this.colWorkOrderPreviousNumber.Caption = "Previous Work Order Number";
            this.colWorkOrderPreviousNumber.FieldName = "WorkOrderPreviousNumber";
            this.colWorkOrderPreviousNumber.Name = "colWorkOrderPreviousNumber";
            this.colWorkOrderPreviousNumber.OptionsColumn.AllowEdit = false;
            this.colWorkOrderPreviousNumber.OptionsColumn.AllowFocus = false;
            this.colWorkOrderPreviousNumber.OptionsColumn.ReadOnly = true;
            this.colWorkOrderPreviousNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colWorkOrderIssueDate
            // 
            this.colWorkOrderIssueDate.Caption = "Issue Date";
            this.colWorkOrderIssueDate.FieldName = "WorkOrderIssueDate";
            this.colWorkOrderIssueDate.Name = "colWorkOrderIssueDate";
            this.colWorkOrderIssueDate.OptionsColumn.AllowEdit = false;
            this.colWorkOrderIssueDate.OptionsColumn.AllowFocus = false;
            this.colWorkOrderIssueDate.OptionsColumn.ReadOnly = true;
            this.colWorkOrderIssueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colWorkOrderInvoiceNumber
            // 
            this.colWorkOrderInvoiceNumber.Caption = "Invoice Number";
            this.colWorkOrderInvoiceNumber.FieldName = "WorkOrderInvoiceNumber";
            this.colWorkOrderInvoiceNumber.Name = "colWorkOrderInvoiceNumber";
            this.colWorkOrderInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colWorkOrderInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colWorkOrderInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colWorkOrderInvoiceNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colWorkOrderUserDefined1
            // 
            this.colWorkOrderUserDefined1.Caption = "Work Order User Defined 1";
            this.colWorkOrderUserDefined1.FieldName = "WorkOrderUserDefined1";
            this.colWorkOrderUserDefined1.Name = "colWorkOrderUserDefined1";
            this.colWorkOrderUserDefined1.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined1.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined1.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colWorkOrderUserDefined2
            // 
            this.colWorkOrderUserDefined2.Caption = "Work Order User Defined 2";
            this.colWorkOrderUserDefined2.FieldName = "WorkOrderUserDefined2";
            this.colWorkOrderUserDefined2.Name = "colWorkOrderUserDefined2";
            this.colWorkOrderUserDefined2.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined2.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined2.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colWorkOrderUserDefined3
            // 
            this.colWorkOrderUserDefined3.Caption = "Work Order User Defined 3";
            this.colWorkOrderUserDefined3.FieldName = "WorkOrderUserDefined3";
            this.colWorkOrderUserDefined3.Name = "colWorkOrderUserDefined3";
            this.colWorkOrderUserDefined3.OptionsColumn.AllowEdit = false;
            this.colWorkOrderUserDefined3.OptionsColumn.AllowFocus = false;
            this.colWorkOrderUserDefined3.OptionsColumn.ReadOnly = true;
            this.colWorkOrderUserDefined3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colWorkOrderCompleteDate
            // 
            this.colWorkOrderCompleteDate.Caption = "Work Order Complete Date";
            this.colWorkOrderCompleteDate.FieldName = "WorkOrderCompleteDate";
            this.colWorkOrderCompleteDate.Name = "colWorkOrderCompleteDate";
            this.colWorkOrderCompleteDate.OptionsColumn.AllowEdit = false;
            this.colWorkOrderCompleteDate.OptionsColumn.AllowFocus = false;
            this.colWorkOrderCompleteDate.OptionsColumn.ReadOnly = true;
            this.colWorkOrderCompleteDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // Calculated1
            // 
            this.Calculated1.Caption = "<b>Calculated 1</b>";
            this.Calculated1.FieldName = "Calculated1";
            this.Calculated1.Name = "Calculated1";
            this.Calculated1.OptionsColumn.AllowEdit = false;
            this.Calculated1.OptionsColumn.AllowFocus = false;
            this.Calculated1.OptionsColumn.ReadOnly = true;
            this.Calculated1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.Calculated1.ShowUnboundExpressionMenu = true;
            this.Calculated1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated1.Visible = true;
            this.Calculated1.VisibleIndex = 14;
            this.Calculated1.Width = 90;
            // 
            // Calculated2
            // 
            this.Calculated2.Caption = "<b>Calculated 2</b>";
            this.Calculated2.FieldName = "Calculated2";
            this.Calculated2.Name = "Calculated2";
            this.Calculated2.OptionsColumn.AllowEdit = false;
            this.Calculated2.OptionsColumn.AllowFocus = false;
            this.Calculated2.OptionsColumn.ReadOnly = true;
            this.Calculated2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.Calculated2.ShowUnboundExpressionMenu = true;
            this.Calculated2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated2.Visible = true;
            this.Calculated2.VisibleIndex = 15;
            this.Calculated2.Width = 90;
            // 
            // Calculated3
            // 
            this.Calculated3.Caption = "<b>Calculated 3</b>";
            this.Calculated3.FieldName = "Calculated3";
            this.Calculated3.Name = "Calculated3";
            this.Calculated3.OptionsColumn.AllowEdit = false;
            this.Calculated3.OptionsColumn.AllowFocus = false;
            this.Calculated3.OptionsColumn.ReadOnly = true;
            this.Calculated3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.Calculated3.ShowUnboundExpressionMenu = true;
            this.Calculated3.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Calculated3.Visible = true;
            this.Calculated3.VisibleIndex = 16;
            this.Calculated3.Width = 90;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "W\\O Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colUserPicklist1
            // 
            this.colUserPicklist1.Caption = "W\\O User Picklist 1";
            this.colUserPicklist1.FieldName = "UserPicklist1";
            this.colUserPicklist1.Name = "colUserPicklist1";
            this.colUserPicklist1.OptionsColumn.AllowEdit = false;
            this.colUserPicklist1.OptionsColumn.AllowFocus = false;
            this.colUserPicklist1.OptionsColumn.ReadOnly = true;
            this.colUserPicklist1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colUserPicklist2
            // 
            this.colUserPicklist2.Caption = "W\\O User Picklist 2";
            this.colUserPicklist2.FieldName = "UserPicklist2";
            this.colUserPicklist2.Name = "colUserPicklist2";
            this.colUserPicklist2.OptionsColumn.AllowEdit = false;
            this.colUserPicklist2.OptionsColumn.AllowFocus = false;
            this.colUserPicklist2.OptionsColumn.ReadOnly = true;
            this.colUserPicklist2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colUserPicklist3
            // 
            this.colUserPicklist3.Caption = "W\\O User Picklist 3";
            this.colUserPicklist3.FieldName = "UserPicklist3";
            this.colUserPicklist3.Name = "colUserPicklist3";
            this.colUserPicklist3.OptionsColumn.AllowEdit = false;
            this.colUserPicklist3.OptionsColumn.AllowFocus = false;
            this.colUserPicklist3.OptionsColumn.ReadOnly = true;
            this.colUserPicklist3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colWorkOrderReference
            // 
            this.colWorkOrderReference.Caption = "Work Order Reference";
            this.colWorkOrderReference.FieldName = "WorkOrderReference";
            this.colWorkOrderReference.Name = "colWorkOrderReference";
            this.colWorkOrderReference.OptionsColumn.AllowEdit = false;
            this.colWorkOrderReference.OptionsColumn.AllowFocus = false;
            this.colWorkOrderReference.OptionsColumn.ReadOnly = true;
            this.colWorkOrderReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkOrderReference.Visible = true;
            this.colWorkOrderReference.VisibleIndex = 13;
            this.colWorkOrderReference.Width = 130;
            // 
            // colWorkOrderTargetDate
            // 
            this.colWorkOrderTargetDate.Caption = "Target Date";
            this.colWorkOrderTargetDate.FieldName = "WorkOrderTargetDate";
            this.colWorkOrderTargetDate.Name = "colWorkOrderTargetDate";
            this.colWorkOrderTargetDate.OptionsColumn.AllowEdit = false;
            this.colWorkOrderTargetDate.OptionsColumn.AllowFocus = false;
            this.colWorkOrderTargetDate.OptionsColumn.ReadOnly = true;
            this.colWorkOrderTargetDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkOrderTargetDate.Visible = true;
            this.colWorkOrderTargetDate.VisibleIndex = 3;
            this.colWorkOrderTargetDate.Width = 79;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.simpleButton1);
            this.splitContainerControl1.Panel1.Controls.Add(this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Work to Analyse - Enter date range and click Refresh";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1329, 804);
            this.splitContainerControl1.SplitterPosition = 300;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl1.Controls.Add(this.btnRefreshList);
            this.layoutControl1.Controls.Add(this.dateEditTo);
            this.layoutControl1.Controls.Add(this.dateEditFrom);
            this.layoutControl1.Location = new System.Drawing.Point(3, 3);
            this.layoutControl1.MenuManager = this.barManager1;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1368, 283, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(293, 50);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnRefreshList
            // 
            this.btnRefreshList.Image = global::WoodPlan5.Properties.Resources.refresh_16x16;
            this.btnRefreshList.Location = new System.Drawing.Point(218, 25);
            this.btnRefreshList.Name = "btnRefreshList";
            this.btnRefreshList.Size = new System.Drawing.Size(72, 22);
            this.btnRefreshList.StyleController = this.layoutControl1;
            this.btnRefreshList.TabIndex = 6;
            this.btnRefreshList.Text = "Refresh";
            this.btnRefreshList.Click += new System.EventHandler(this.btnRefreshList_Click);
            // 
            // dateEditTo
            // 
            this.dateEditTo.EditValue = null;
            this.dateEditTo.Location = new System.Drawing.Point(34, 27);
            this.dateEditTo.Name = "dateEditTo";
            this.dateEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditTo.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditTo.Size = new System.Drawing.Size(180, 20);
            this.dateEditTo.StyleController = this.layoutControl1;
            this.dateEditTo.TabIndex = 5;
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.EditValue = null;
            this.dateEditFrom.Location = new System.Drawing.Point(34, 3);
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFrom.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dateEditFrom.Size = new System.Drawing.Size(180, 20);
            this.dateEditFrom.StyleController = this.layoutControl1;
            this.dateEditFrom.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(293, 50);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AllowHtmlStringInCaption = true;
            this.layoutControlItem1.Control = this.dateEditFrom;
            this.layoutControlItem1.CustomizationFormText = "From:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem1.Text = "From:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(28, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnRefreshList;
            this.layoutControlItem3.Location = new System.Drawing.Point(215, 22);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(76, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(76, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(76, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            this.layoutControlItem3.Click += new System.EventHandler(this.btnRefreshList_Click);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHtmlStringInCaption = true;
            this.layoutControlItem2.Control = this.dateEditTo;
            this.layoutControlItem2.CustomizationFormText = "To:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem2.Text = "To:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(28, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(215, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(76, 22);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(201, 756);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(95, 23);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "Analyse Data";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.pivotGridControl1);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.chartControl1);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1023, 804);
            this.splitContainerControl3.SplitterPosition = 475;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl1.DataSource = this.sp01218ATWorkOrdersAnalysisJobsAnalyseBindingSource;
            this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldJobActionID,
            this.fieldJobInspectionID,
            this.fieldJobClientName,
            this.fieldJobSiteName,
            this.fieldJobTreeReference,
            this.fieldJobInspectionReference,
            this.fieldJobMapID,
            this.fieldJobSpecies,
            this.fieldJobClientID,
            this.fieldJobSiteID,
            this.fieldJobTreeID,
            this.fieldJobSpeciesAcronym,
            this.fieldJobDueDate,
            this.fieldJobDoneDate,
            this.fieldJobDescription,
            this.fieldJobCarriedOutBy,
            this.fieldJobSupervisor,
            this.fieldJobNumber,
            this.fieldJobUserDefined1,
            this.fieldJobUserDefined2,
            this.fieldJobUserDefined3,
            this.fieldJobWorkOrderID,
            this.fieldJobPreviousWorkOrderID,
            this.fieldJobNearestHouse,
            this.fieldJobPriority,
            this.fieldJobWorkUnits,
            this.fieldJobRateBudgeted,
            this.fieldJobCostBudgeted,
            this.fieldJobRateActual,
            this.fieldJobCostActual,
            this.fieldJobSortOrder,
            this.fieldJobPreviousActionNumber,
            this.fieldColour,
            this.fieldJobBudgetDescription,
            this.fieldJobRateDescription,
            this.fieldintBudgetID,
            this.fieldJobDiscountRate,
            this.fieldJobRemarks,
            this.fieldWorkOrderNumber,
            this.fieldWorkOrderPreviousNumber,
            this.fieldWorkOrderIssueDate,
            this.fieldWorkOrderInvoiceNumber,
            this.fieldWorkOrderUserDefined1,
            this.fieldWorkOrderUserDefined2,
            this.fieldWorkOrderUserDefined3,
            this.fieldWorkOrderCompleteDate,
            this.fieldJobCostCentreCode,
            this.fieldJobOwnership,
            this.fieldCalculatedWorkOrderIssueYear,
            this.fieldCalculatedWorkOrderIssueQuarter,
            this.fieldCalculatedWorkOrderIssueMonth,
            this.fieldCalculatedWorkOrderCompleteYear,
            this.fieldCalculatedWorkOrderCompleteQuarter,
            this.fieldCalculatedWorkOrderCompleteMonth,
            this.fieldCalculatedJobDueYear,
            this.fieldCalculatedJobDueQuarter,
            this.fieldCalculatedJobDueMonth,
            this.fieldCalculatedJobDoneYear,
            this.fieldCalculatedJobDoneQuarter,
            this.fieldCalculatedJobDoneMonth,
            this.fieldStatus,
            this.fieldUserPicklist1,
            this.fieldUserPicklist2,
            this.fieldUserPicklist3,
            this.fieldWorkOrderReference,
            this.fieldWorkOrderTargetDate,
            this.fieldCalculatedWorkOrderTargetYear,
            this.fieldCalculatedWorkOrderTargetQuarter,
            this.fieldCalculatedWorkOrderTargetMonth,
            this.fieldActionCount});
            pivotGridGroup1.Caption = "Work Order Issue Year - Quarter - Month";
            pivotGridGroup1.Fields.Add(this.fieldCalculatedWorkOrderIssueYear);
            pivotGridGroup1.Fields.Add(this.fieldCalculatedWorkOrderIssueQuarter);
            pivotGridGroup1.Fields.Add(this.fieldCalculatedWorkOrderIssueMonth);
            pivotGridGroup1.Hierarchy = null;
            pivotGridGroup1.ShowNewValues = true;
            pivotGridGroup2.Caption = "Work Order Complete Year - Quarter - Month";
            pivotGridGroup2.Fields.Add(this.fieldCalculatedWorkOrderCompleteYear);
            pivotGridGroup2.Fields.Add(this.fieldCalculatedWorkOrderCompleteQuarter);
            pivotGridGroup2.Fields.Add(this.fieldCalculatedWorkOrderCompleteMonth);
            pivotGridGroup2.Hierarchy = null;
            pivotGridGroup2.ShowNewValues = true;
            pivotGridGroup3.Caption = "Job Due Year - Quarter - Month";
            pivotGridGroup3.Fields.Add(this.fieldCalculatedJobDueYear);
            pivotGridGroup3.Fields.Add(this.fieldCalculatedJobDueQuarter);
            pivotGridGroup3.Fields.Add(this.fieldCalculatedJobDueMonth);
            pivotGridGroup3.Hierarchy = null;
            pivotGridGroup3.ShowNewValues = true;
            pivotGridGroup4.Caption = "Job Done Year - Quarter - Month";
            pivotGridGroup4.Fields.Add(this.fieldCalculatedJobDoneYear);
            pivotGridGroup4.Fields.Add(this.fieldCalculatedJobDoneQuarter);
            pivotGridGroup4.Fields.Add(this.fieldCalculatedJobDoneMonth);
            pivotGridGroup4.Hierarchy = null;
            pivotGridGroup4.ShowNewValues = true;
            pivotGridGroup5.Caption = "Work Order Target Year - Quarter - Month";
            pivotGridGroup5.Fields.Add(this.fieldCalculatedWorkOrderTargetYear);
            pivotGridGroup5.Fields.Add(this.fieldCalculatedWorkOrderTargetQuarter);
            pivotGridGroup5.Fields.Add(this.fieldCalculatedWorkOrderTargetMonth);
            pivotGridGroup5.Hierarchy = null;
            pivotGridGroup5.ShowNewValues = true;
            this.pivotGridControl1.Groups.AddRange(new DevExpress.XtraPivotGrid.PivotGridGroup[] {
            pivotGridGroup1,
            pivotGridGroup2,
            pivotGridGroup3,
            pivotGridGroup4,
            pivotGridGroup5});
            this.pivotGridControl1.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl1.MenuManager = this.barManager1;
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Excel2007;
            this.pivotGridControl1.Size = new System.Drawing.Size(1023, 475);
            this.pivotGridControl1.TabIndex = 0;
            this.pivotGridControl1.FieldAreaChanged += new DevExpress.XtraPivotGrid.PivotFieldEventHandler(this.pivotGridControl1_FieldAreaChanged);
            this.pivotGridControl1.PopupMenuShowing += new DevExpress.XtraPivotGrid.PopupMenuShowingEventHandler(this.pivotGridControl1_PopupMenuShowing);
            this.pivotGridControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pivotGridControl1_MouseUp);
            // 
            // sp01218ATWorkOrdersAnalysisJobsAnalyseBindingSource
            // 
            this.sp01218ATWorkOrdersAnalysisJobsAnalyseBindingSource.DataMember = "sp01218_AT_WorkOrders_Analysis_Jobs_Analyse";
            this.sp01218ATWorkOrdersAnalysisJobsAnalyseBindingSource.DataSource = this.dataSet_AT_WorkOrders;
            // 
            // fieldJobActionID
            // 
            this.fieldJobActionID.AreaIndex = 0;
            this.fieldJobActionID.Caption = "Action ID";
            this.fieldJobActionID.FieldName = "JobActionID";
            this.fieldJobActionID.Name = "fieldJobActionID";
            this.fieldJobActionID.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobActionID.Visible = false;
            // 
            // fieldJobInspectionID
            // 
            this.fieldJobInspectionID.AreaIndex = 0;
            this.fieldJobInspectionID.Caption = "Inspection ID";
            this.fieldJobInspectionID.FieldName = "JobInspectionID";
            this.fieldJobInspectionID.Name = "fieldJobInspectionID";
            this.fieldJobInspectionID.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobInspectionID.Visible = false;
            // 
            // fieldJobClientName
            // 
            this.fieldJobClientName.AreaIndex = 0;
            this.fieldJobClientName.Caption = "Client Name";
            this.fieldJobClientName.FieldName = "JobClientName";
            this.fieldJobClientName.Name = "fieldJobClientName";
            this.fieldJobClientName.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobSiteName
            // 
            this.fieldJobSiteName.AreaIndex = 1;
            this.fieldJobSiteName.Caption = "Site Name";
            this.fieldJobSiteName.FieldName = "JobSiteName";
            this.fieldJobSiteName.Name = "fieldJobSiteName";
            this.fieldJobSiteName.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobTreeReference
            // 
            this.fieldJobTreeReference.AreaIndex = 2;
            this.fieldJobTreeReference.Caption = "Tree Reference";
            this.fieldJobTreeReference.FieldName = "JobTreeReference";
            this.fieldJobTreeReference.Name = "fieldJobTreeReference";
            this.fieldJobTreeReference.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobInspectionReference
            // 
            this.fieldJobInspectionReference.AreaIndex = 3;
            this.fieldJobInspectionReference.Caption = "Inspection Reference";
            this.fieldJobInspectionReference.FieldName = "JobInspectionReference";
            this.fieldJobInspectionReference.Name = "fieldJobInspectionReference";
            this.fieldJobInspectionReference.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobInspectionReference.Visible = false;
            // 
            // fieldJobMapID
            // 
            this.fieldJobMapID.AreaIndex = 3;
            this.fieldJobMapID.Caption = "Map ID";
            this.fieldJobMapID.FieldName = "JobMapID";
            this.fieldJobMapID.Name = "fieldJobMapID";
            this.fieldJobMapID.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobMapID.Visible = false;
            // 
            // fieldJobSpecies
            // 
            this.fieldJobSpecies.AreaIndex = 3;
            this.fieldJobSpecies.Caption = "Species";
            this.fieldJobSpecies.FieldName = "JobSpecies";
            this.fieldJobSpecies.Name = "fieldJobSpecies";
            this.fieldJobSpecies.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobClientID
            // 
            this.fieldJobClientID.AreaIndex = 4;
            this.fieldJobClientID.Caption = "Client ID";
            this.fieldJobClientID.FieldName = "JobClientID";
            this.fieldJobClientID.Name = "fieldJobClientID";
            this.fieldJobClientID.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobClientID.Visible = false;
            // 
            // fieldJobSiteID
            // 
            this.fieldJobSiteID.AreaIndex = 4;
            this.fieldJobSiteID.Caption = "Site ID";
            this.fieldJobSiteID.FieldName = "JobSiteID";
            this.fieldJobSiteID.Name = "fieldJobSiteID";
            this.fieldJobSiteID.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobSiteID.Visible = false;
            // 
            // fieldJobTreeID
            // 
            this.fieldJobTreeID.AreaIndex = 4;
            this.fieldJobTreeID.Caption = "Tree ID";
            this.fieldJobTreeID.FieldName = "JobTreeID";
            this.fieldJobTreeID.Name = "fieldJobTreeID";
            this.fieldJobTreeID.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobTreeID.Visible = false;
            // 
            // fieldJobSpeciesAcronym
            // 
            this.fieldJobSpeciesAcronym.AreaIndex = 4;
            this.fieldJobSpeciesAcronym.Caption = "Species Acronym";
            this.fieldJobSpeciesAcronym.FieldName = "JobSpeciesAcronym";
            this.fieldJobSpeciesAcronym.Name = "fieldJobSpeciesAcronym";
            this.fieldJobSpeciesAcronym.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobDueDate
            // 
            this.fieldJobDueDate.AreaIndex = 5;
            this.fieldJobDueDate.Caption = "Job Due Date";
            this.fieldJobDueDate.FieldName = "JobDueDate";
            this.fieldJobDueDate.Name = "fieldJobDueDate";
            this.fieldJobDueDate.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobDoneDate
            // 
            this.fieldJobDoneDate.AreaIndex = 6;
            this.fieldJobDoneDate.Caption = "Job Done Date";
            this.fieldJobDoneDate.FieldName = "JobDoneDate";
            this.fieldJobDoneDate.Name = "fieldJobDoneDate";
            this.fieldJobDoneDate.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobDescription
            // 
            this.fieldJobDescription.AreaIndex = 8;
            this.fieldJobDescription.Caption = "Job Description";
            this.fieldJobDescription.FieldName = "JobDescription";
            this.fieldJobDescription.Name = "fieldJobDescription";
            this.fieldJobDescription.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobCarriedOutBy
            // 
            this.fieldJobCarriedOutBy.AreaIndex = 9;
            this.fieldJobCarriedOutBy.Caption = "Carried Out By";
            this.fieldJobCarriedOutBy.FieldName = "JobCarriedOutBy";
            this.fieldJobCarriedOutBy.Name = "fieldJobCarriedOutBy";
            this.fieldJobCarriedOutBy.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobSupervisor
            // 
            this.fieldJobSupervisor.AreaIndex = 10;
            this.fieldJobSupervisor.Caption = "Supervisor";
            this.fieldJobSupervisor.FieldName = "JobSupervisor";
            this.fieldJobSupervisor.Name = "fieldJobSupervisor";
            this.fieldJobSupervisor.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobNumber
            // 
            this.fieldJobNumber.AreaIndex = 10;
            this.fieldJobNumber.Caption = "Job Number";
            this.fieldJobNumber.FieldName = "JobNumber";
            this.fieldJobNumber.Name = "fieldJobNumber";
            this.fieldJobNumber.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobNumber.Visible = false;
            // 
            // fieldJobUserDefined1
            // 
            this.fieldJobUserDefined1.AreaIndex = 10;
            this.fieldJobUserDefined1.Caption = "Job User Defined 1";
            this.fieldJobUserDefined1.FieldName = "JobUserDefined1";
            this.fieldJobUserDefined1.Name = "fieldJobUserDefined1";
            this.fieldJobUserDefined1.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobUserDefined1.Visible = false;
            // 
            // fieldJobUserDefined2
            // 
            this.fieldJobUserDefined2.AreaIndex = 10;
            this.fieldJobUserDefined2.Caption = "Job User Defined 2";
            this.fieldJobUserDefined2.FieldName = "JobUserDefined2";
            this.fieldJobUserDefined2.Name = "fieldJobUserDefined2";
            this.fieldJobUserDefined2.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobUserDefined2.Visible = false;
            // 
            // fieldJobUserDefined3
            // 
            this.fieldJobUserDefined3.AreaIndex = 10;
            this.fieldJobUserDefined3.Caption = "Job User Defined 3";
            this.fieldJobUserDefined3.FieldName = "JobUserDefined3";
            this.fieldJobUserDefined3.Name = "fieldJobUserDefined3";
            this.fieldJobUserDefined3.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobUserDefined3.Visible = false;
            // 
            // fieldJobWorkOrderID
            // 
            this.fieldJobWorkOrderID.AreaIndex = 10;
            this.fieldJobWorkOrderID.Caption = "Work Order ID";
            this.fieldJobWorkOrderID.FieldName = "JobWorkOrderID";
            this.fieldJobWorkOrderID.Name = "fieldJobWorkOrderID";
            this.fieldJobWorkOrderID.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobWorkOrderID.Visible = false;
            // 
            // fieldJobPreviousWorkOrderID
            // 
            this.fieldJobPreviousWorkOrderID.AreaIndex = 11;
            this.fieldJobPreviousWorkOrderID.Caption = "Previous Work Order ID";
            this.fieldJobPreviousWorkOrderID.FieldName = "JobPreviousWorkOrderID";
            this.fieldJobPreviousWorkOrderID.Name = "fieldJobPreviousWorkOrderID";
            this.fieldJobPreviousWorkOrderID.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobPreviousWorkOrderID.Visible = false;
            // 
            // fieldJobNearestHouse
            // 
            this.fieldJobNearestHouse.AreaIndex = 11;
            this.fieldJobNearestHouse.Caption = "Nearest House";
            this.fieldJobNearestHouse.FieldName = "JobNearestHouse";
            this.fieldJobNearestHouse.Name = "fieldJobNearestHouse";
            this.fieldJobNearestHouse.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobNearestHouse.Visible = false;
            // 
            // fieldJobPriority
            // 
            this.fieldJobPriority.AreaIndex = 11;
            this.fieldJobPriority.Caption = "Job Priority";
            this.fieldJobPriority.FieldName = "JobPriority";
            this.fieldJobPriority.Name = "fieldJobPriority";
            this.fieldJobPriority.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobWorkUnits
            // 
            this.fieldJobWorkUnits.AreaIndex = 12;
            this.fieldJobWorkUnits.Caption = "Job Work Units";
            this.fieldJobWorkUnits.FieldName = "JobWorkUnits";
            this.fieldJobWorkUnits.Name = "fieldJobWorkUnits";
            this.fieldJobWorkUnits.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobWorkUnits.Visible = false;
            // 
            // fieldJobRateBudgeted
            // 
            this.fieldJobRateBudgeted.AreaIndex = 13;
            this.fieldJobRateBudgeted.Caption = "Job Rate Budgeted";
            this.fieldJobRateBudgeted.CellFormat.FormatString = "c";
            this.fieldJobRateBudgeted.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobRateBudgeted.FieldName = "JobRateBudgeted";
            this.fieldJobRateBudgeted.GrandTotalCellFormat.FormatString = "c";
            this.fieldJobRateBudgeted.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobRateBudgeted.Name = "fieldJobRateBudgeted";
            this.fieldJobRateBudgeted.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobRateBudgeted.TotalCellFormat.FormatString = "c";
            this.fieldJobRateBudgeted.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobRateBudgeted.TotalValueFormat.FormatString = "c";
            this.fieldJobRateBudgeted.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobRateBudgeted.ValueFormat.FormatString = "c";
            this.fieldJobRateBudgeted.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldJobCostBudgeted
            // 
            this.fieldJobCostBudgeted.AreaIndex = 15;
            this.fieldJobCostBudgeted.Caption = "Job Cost Budgeted";
            this.fieldJobCostBudgeted.CellFormat.FormatString = "c";
            this.fieldJobCostBudgeted.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobCostBudgeted.FieldName = "JobCostBudgeted";
            this.fieldJobCostBudgeted.GrandTotalCellFormat.FormatString = "c";
            this.fieldJobCostBudgeted.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobCostBudgeted.Name = "fieldJobCostBudgeted";
            this.fieldJobCostBudgeted.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobCostBudgeted.TotalCellFormat.FormatString = "c";
            this.fieldJobCostBudgeted.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobCostBudgeted.TotalValueFormat.FormatString = "c";
            this.fieldJobCostBudgeted.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobCostBudgeted.ValueFormat.FormatString = "c";
            this.fieldJobCostBudgeted.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldJobRateActual
            // 
            this.fieldJobRateActual.AreaIndex = 14;
            this.fieldJobRateActual.Caption = "Job Rate Actual";
            this.fieldJobRateActual.CellFormat.FormatString = "c";
            this.fieldJobRateActual.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobRateActual.FieldName = "JobRateActual";
            this.fieldJobRateActual.GrandTotalCellFormat.FormatString = "c";
            this.fieldJobRateActual.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobRateActual.Name = "fieldJobRateActual";
            this.fieldJobRateActual.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobRateActual.TotalCellFormat.FormatString = "c";
            this.fieldJobRateActual.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobRateActual.TotalValueFormat.FormatString = "c";
            this.fieldJobRateActual.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobRateActual.ValueFormat.FormatString = "c";
            this.fieldJobRateActual.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldJobCostActual
            // 
            this.fieldJobCostActual.AreaIndex = 16;
            this.fieldJobCostActual.Caption = "Job Cost Actual";
            this.fieldJobCostActual.CellFormat.FormatString = "c";
            this.fieldJobCostActual.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobCostActual.FieldName = "JobCostActual";
            this.fieldJobCostActual.GrandTotalCellFormat.FormatString = "c";
            this.fieldJobCostActual.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobCostActual.Name = "fieldJobCostActual";
            this.fieldJobCostActual.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobCostActual.TotalCellFormat.FormatString = "c";
            this.fieldJobCostActual.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobCostActual.TotalValueFormat.FormatString = "c";
            this.fieldJobCostActual.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobCostActual.ValueFormat.FormatString = "c";
            this.fieldJobCostActual.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldJobSortOrder
            // 
            this.fieldJobSortOrder.AreaIndex = 16;
            this.fieldJobSortOrder.Caption = "Job Sort Order";
            this.fieldJobSortOrder.FieldName = "JobSortOrder";
            this.fieldJobSortOrder.Name = "fieldJobSortOrder";
            this.fieldJobSortOrder.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobSortOrder.Visible = false;
            // 
            // fieldJobPreviousActionNumber
            // 
            this.fieldJobPreviousActionNumber.AreaIndex = 16;
            this.fieldJobPreviousActionNumber.Caption = "Job Previous Action Number";
            this.fieldJobPreviousActionNumber.FieldName = "JobPreviousActionNumber";
            this.fieldJobPreviousActionNumber.Name = "fieldJobPreviousActionNumber";
            this.fieldJobPreviousActionNumber.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobPreviousActionNumber.Visible = false;
            // 
            // fieldColour
            // 
            this.fieldColour.AreaIndex = 16;
            this.fieldColour.Caption = "Colour";
            this.fieldColour.FieldName = "Colour";
            this.fieldColour.Name = "fieldColour";
            this.fieldColour.Options.AllowRunTimeSummaryChange = true;
            this.fieldColour.Visible = false;
            // 
            // fieldJobBudgetDescription
            // 
            this.fieldJobBudgetDescription.AreaIndex = 20;
            this.fieldJobBudgetDescription.Caption = "Budget Description";
            this.fieldJobBudgetDescription.FieldName = "JobBudgetDescription";
            this.fieldJobBudgetDescription.Name = "fieldJobBudgetDescription";
            this.fieldJobBudgetDescription.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobRateDescription
            // 
            this.fieldJobRateDescription.AreaIndex = 12;
            this.fieldJobRateDescription.Caption = "Job Rate Description";
            this.fieldJobRateDescription.FieldName = "JobRateDescription";
            this.fieldJobRateDescription.Name = "fieldJobRateDescription";
            this.fieldJobRateDescription.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldintBudgetID
            // 
            this.fieldintBudgetID.AreaIndex = 19;
            this.fieldintBudgetID.Caption = "Budget ID";
            this.fieldintBudgetID.FieldName = "intBudgetID";
            this.fieldintBudgetID.Name = "fieldintBudgetID";
            this.fieldintBudgetID.Options.AllowRunTimeSummaryChange = true;
            this.fieldintBudgetID.Visible = false;
            // 
            // fieldJobDiscountRate
            // 
            this.fieldJobDiscountRate.AreaIndex = 17;
            this.fieldJobDiscountRate.Caption = "Job Discount Rate";
            this.fieldJobDiscountRate.CellFormat.FormatString = "P";
            this.fieldJobDiscountRate.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobDiscountRate.FieldName = "JobDiscountRate";
            this.fieldJobDiscountRate.GrandTotalCellFormat.FormatString = "P";
            this.fieldJobDiscountRate.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobDiscountRate.Name = "fieldJobDiscountRate";
            this.fieldJobDiscountRate.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobDiscountRate.TotalCellFormat.FormatString = "P";
            this.fieldJobDiscountRate.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobDiscountRate.TotalValueFormat.FormatString = "P";
            this.fieldJobDiscountRate.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldJobDiscountRate.ValueFormat.FormatString = "P";
            this.fieldJobDiscountRate.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldJobRemarks
            // 
            this.fieldJobRemarks.AreaIndex = 20;
            this.fieldJobRemarks.Caption = "Job Remarks";
            this.fieldJobRemarks.FieldName = "JobRemarks";
            this.fieldJobRemarks.Name = "fieldJobRemarks";
            this.fieldJobRemarks.Options.AllowRunTimeSummaryChange = true;
            this.fieldJobRemarks.Visible = false;
            // 
            // fieldWorkOrderNumber
            // 
            this.fieldWorkOrderNumber.AreaIndex = 22;
            this.fieldWorkOrderNumber.Caption = "Work Order Number";
            this.fieldWorkOrderNumber.FieldName = "WorkOrderNumber";
            this.fieldWorkOrderNumber.Name = "fieldWorkOrderNumber";
            this.fieldWorkOrderNumber.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldWorkOrderPreviousNumber
            // 
            this.fieldWorkOrderPreviousNumber.AreaIndex = 20;
            this.fieldWorkOrderPreviousNumber.Caption = "Work Order Previous Number";
            this.fieldWorkOrderPreviousNumber.FieldName = "WorkOrderPreviousNumber";
            this.fieldWorkOrderPreviousNumber.Name = "fieldWorkOrderPreviousNumber";
            this.fieldWorkOrderPreviousNumber.Options.AllowRunTimeSummaryChange = true;
            this.fieldWorkOrderPreviousNumber.Visible = false;
            // 
            // fieldWorkOrderIssueDate
            // 
            this.fieldWorkOrderIssueDate.AreaIndex = 24;
            this.fieldWorkOrderIssueDate.Caption = "Work Order Issue Date";
            this.fieldWorkOrderIssueDate.FieldName = "WorkOrderIssueDate";
            this.fieldWorkOrderIssueDate.Name = "fieldWorkOrderIssueDate";
            this.fieldWorkOrderIssueDate.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldWorkOrderInvoiceNumber
            // 
            this.fieldWorkOrderInvoiceNumber.AreaIndex = 26;
            this.fieldWorkOrderInvoiceNumber.Caption = "Work Order Invoice Number";
            this.fieldWorkOrderInvoiceNumber.FieldName = "WorkOrderInvoiceNumber";
            this.fieldWorkOrderInvoiceNumber.Name = "fieldWorkOrderInvoiceNumber";
            this.fieldWorkOrderInvoiceNumber.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldWorkOrderUserDefined1
            // 
            this.fieldWorkOrderUserDefined1.AreaIndex = 27;
            this.fieldWorkOrderUserDefined1.Caption = "Work Order User Defined 1";
            this.fieldWorkOrderUserDefined1.FieldName = "WorkOrderUserDefined1";
            this.fieldWorkOrderUserDefined1.Name = "fieldWorkOrderUserDefined1";
            this.fieldWorkOrderUserDefined1.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldWorkOrderUserDefined2
            // 
            this.fieldWorkOrderUserDefined2.AreaIndex = 28;
            this.fieldWorkOrderUserDefined2.Caption = "Work Order User Defined 2";
            this.fieldWorkOrderUserDefined2.FieldName = "WorkOrderUserDefined2";
            this.fieldWorkOrderUserDefined2.Name = "fieldWorkOrderUserDefined2";
            this.fieldWorkOrderUserDefined2.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldWorkOrderUserDefined3
            // 
            this.fieldWorkOrderUserDefined3.AreaIndex = 29;
            this.fieldWorkOrderUserDefined3.Caption = "Work Order User Defined 3";
            this.fieldWorkOrderUserDefined3.FieldName = "WorkOrderUserDefined3";
            this.fieldWorkOrderUserDefined3.Name = "fieldWorkOrderUserDefined3";
            this.fieldWorkOrderUserDefined3.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldWorkOrderCompleteDate
            // 
            this.fieldWorkOrderCompleteDate.AreaIndex = 25;
            this.fieldWorkOrderCompleteDate.Caption = "Work Order Complete Date";
            this.fieldWorkOrderCompleteDate.FieldName = "WorkOrderCompleteDate";
            this.fieldWorkOrderCompleteDate.Name = "fieldWorkOrderCompleteDate";
            this.fieldWorkOrderCompleteDate.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobCostCentreCode
            // 
            this.fieldJobCostCentreCode.AreaIndex = 19;
            this.fieldJobCostCentreCode.Caption = "Job Cost Centre Code";
            this.fieldJobCostCentreCode.FieldName = "JobCostCentreCode";
            this.fieldJobCostCentreCode.Name = "fieldJobCostCentreCode";
            this.fieldJobCostCentreCode.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldJobOwnership
            // 
            this.fieldJobOwnership.AreaIndex = 18;
            this.fieldJobOwnership.Caption = "Job Ownership";
            this.fieldJobOwnership.FieldName = "JobOwnership";
            this.fieldJobOwnership.Name = "fieldJobOwnership";
            this.fieldJobOwnership.Options.AllowRunTimeSummaryChange = true;
            // 
            // fieldStatus
            // 
            this.fieldStatus.AreaIndex = 23;
            this.fieldStatus.Caption = "Work Order Status";
            this.fieldStatus.FieldName = "Status";
            this.fieldStatus.Name = "fieldStatus";
            // 
            // fieldUserPicklist1
            // 
            this.fieldUserPicklist1.AreaIndex = 30;
            this.fieldUserPicklist1.Caption = "Work Order User Picklist 1";
            this.fieldUserPicklist1.FieldName = "UserPicklist1";
            this.fieldUserPicklist1.Name = "fieldUserPicklist1";
            // 
            // fieldUserPicklist2
            // 
            this.fieldUserPicklist2.AreaIndex = 31;
            this.fieldUserPicklist2.Caption = "Work Order User Picklist 2";
            this.fieldUserPicklist2.FieldName = "UserPicklist2";
            this.fieldUserPicklist2.Name = "fieldUserPicklist2";
            // 
            // fieldUserPicklist3
            // 
            this.fieldUserPicklist3.AreaIndex = 32;
            this.fieldUserPicklist3.Caption = "Work Order User Picklist 3";
            this.fieldUserPicklist3.FieldName = "UserPicklist3";
            this.fieldUserPicklist3.Name = "fieldUserPicklist3";
            // 
            // fieldWorkOrderReference
            // 
            this.fieldWorkOrderReference.AreaIndex = 21;
            this.fieldWorkOrderReference.Caption = "Work Order Reference";
            this.fieldWorkOrderReference.FieldName = "WorkOrderReference";
            this.fieldWorkOrderReference.Name = "fieldWorkOrderReference";
            // 
            // fieldWorkOrderTargetDate
            // 
            this.fieldWorkOrderTargetDate.AreaIndex = 7;
            this.fieldWorkOrderTargetDate.Caption = "Work Order Target Date";
            this.fieldWorkOrderTargetDate.FieldName = "WorkOrderTargetDate";
            this.fieldWorkOrderTargetDate.Name = "fieldWorkOrderTargetDate";
            // 
            // fieldActionCount
            // 
            this.fieldActionCount.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldActionCount.AreaIndex = 0;
            this.fieldActionCount.Caption = "Action Count";
            this.fieldActionCount.FieldName = "ActionCount";
            this.fieldActionCount.Name = "fieldActionCount";
            // 
            // chartControl1
            // 
            this.chartControl1.DataAdapter = this.sp01218_AT_WorkOrders_Analysis_Jobs_AnalyseTableAdapter;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl1.EmptyChartText.Text = "No Data To Graph - Try Selecting Data from the Analysis Grid.";
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            sideBySideBarSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl1.SeriesTemplate.Label = sideBySideBarSeriesLabel1;
            this.chartControl1.Size = new System.Drawing.Size(1023, 323);
            this.chartControl1.TabIndex = 0;
            this.chartControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chartControl1_MouseUp);
            // 
            // sp01218_AT_WorkOrders_Analysis_Jobs_AnalyseTableAdapter
            // 
            this.sp01218_AT_WorkOrders_Analysis_Jobs_AnalyseTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.MenuCaption = "Cell Menu";
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.ShowCaption = true;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Copy to Clipboard";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Hint = "Copy the selected cells to the Clipboard for pasting to external application.";
            this.barButtonItem1.Id = 19;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // pmChart
            // 
            this.pmChart.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateAxis),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiChartWizard, true)});
            this.pmChart.Manager = this.barManager1;
            this.pmChart.MenuCaption = "Chart Menu";
            this.pmChart.Name = "pmChart";
            // 
            // bbiRotateAxis
            // 
            this.bbiRotateAxis.Caption = "Rotate Axis";
            this.bbiRotateAxis.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRotateAxis.Glyph")));
            this.bbiRotateAxis.Hint = "Totate Chart Axis";
            this.bbiRotateAxis.Id = 27;
            this.bbiRotateAxis.Name = "bbiRotateAxis";
            this.bbiRotateAxis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateAxis_ItemClick);
            // 
            // bbiChartWizard
            // 
            this.bbiChartWizard.Caption = "Chart Wizard";
            this.bbiChartWizard.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiChartWizard.Glyph")));
            this.bbiChartWizard.Hint = "Open Chart Wizard";
            this.bbiChartWizard.Id = 26;
            this.bbiChartWizard.Name = "bbiChartWizard";
            this.bbiChartWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChartWizard_ItemClick);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl;
            // 
            // frm_AT_WorkOrder_Analysis_1
            // 
            this.ClientSize = new System.Drawing.Size(1329, 804);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_WorkOrder_Analysis_1";
            this.Text = "Work Order Analysis";
            this.Activated += new System.EventHandler(this.frm_AT_WorkOrder_Analysis_1_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_AT_WorkOrder_Analysis_1_FormClosed);
            this.Load += new System.EventHandler(this.frm_AT_WorkOrder_Analysis_1_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01218ATWorkOrdersAnalysisJobsAnalyseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private System.Windows.Forms.BindingSource sp01209_AT_WorkOrders_Analysis_Jobs_AvailableBindingSource;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01209_AT_WorkOrders_Analysis_Jobs_AvailableTableAdapter sp01209_AT_WorkOrders_Analysis_Jobs_AvailableTableAdapter;
        private DevExpress.XtraGrid.GridControl sp01209_AT_WorkOrders_Analysis_Jobs_AvailableGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobInspectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colJobInspectionReference;
        private DevExpress.XtraGrid.Columns.GridColumn colJobMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colJobClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSpeciesAcronym;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCarriedOutBy;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSupervisor;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colJobUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colJobUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobPreviousWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobNearestHouse;
        private DevExpress.XtraGrid.Columns.GridColumn colJobPriority;
        private DevExpress.XtraGrid.Columns.GridColumn colJobWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colJobRateBudgeted;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCostBudgeted;
        private DevExpress.XtraGrid.Columns.GridColumn colJobRateActual;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCostActual;
        private DevExpress.XtraGrid.Columns.GridColumn colJobSortOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colJobPreviousActionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colColour;
        private DevExpress.XtraGrid.Columns.GridColumn colJobBudgetDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobRateDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colintBudgetID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDiscountRate;
        private DevExpress.XtraGrid.Columns.GridColumn colJobRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderPreviousID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderPreviousNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderIssueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderCompleteDate;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DateEdit dateEditTo;
        private DevExpress.XtraEditors.DateEdit dateEditFrom;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private System.Windows.Forms.BindingSource sp01218ATWorkOrdersAnalysisJobsAnalyseBindingSource;
        private WoodPlan5.DataSet_AT_WorkOrdersTableAdapters.sp01218_AT_WorkOrders_Analysis_Jobs_AnalyseTableAdapter sp01218_AT_WorkOrders_Analysis_Jobs_AnalyseTableAdapter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobActionID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobInspectionID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobClientName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobSiteName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobTreeReference;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobInspectionReference;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobMapID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobSpecies;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobClientID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobSiteID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobTreeID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobSpeciesAcronym;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobDueDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobDoneDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobDescription;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobCarriedOutBy;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobSupervisor;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobNumber;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobUserDefined1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobUserDefined2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobUserDefined3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobWorkOrderID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobPreviousWorkOrderID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobNearestHouse;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobPriority;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobWorkUnits;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobRateBudgeted;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobCostBudgeted;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobRateActual;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobCostActual;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobSortOrder;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobPreviousActionNumber;
        private DevExpress.XtraPivotGrid.PivotGridField fieldColour;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobBudgetDescription;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobRateDescription;
        private DevExpress.XtraPivotGrid.PivotGridField fieldintBudgetID;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobDiscountRate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobRemarks;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderNumber;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderPreviousNumber;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderIssueDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderInvoiceNumber;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderUserDefined1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderUserDefined2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderUserDefined3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderCompleteDate;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobCostCentreCode;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobOwnership;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated1;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated2;
        private DevExpress.XtraGrid.Columns.GridColumn Calculated3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedWorkOrderIssueYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedWorkOrderIssueQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedWorkOrderIssueMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedWorkOrderCompleteYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedWorkOrderCompleteQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedWorkOrderCompleteMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedJobDueYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedJobDueQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedJobDueMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedJobDoneYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedJobDoneQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedJobDoneMonth;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserPicklist3;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderReference;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStatus;
        private DevExpress.XtraPivotGrid.PivotGridField fieldUserPicklist1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldUserPicklist2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldUserPicklist3;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderReference;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderTargetDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedWorkOrderTargetYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedWorkOrderTargetQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCalculatedWorkOrderTargetMonth;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderTargetDate;
        private DevExpress.XtraBars.BarButtonItem bbiChartWizard;
        private DevExpress.XtraBars.PopupMenu pmChart;
        private DevExpress.XtraBars.BarButtonItem bbiRotateAxis;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.SimpleButton btnRefreshList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCount;
    }
}

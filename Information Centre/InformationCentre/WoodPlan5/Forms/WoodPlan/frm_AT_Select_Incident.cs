using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_AT_Select_Incident : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSelectedValue = "";
        public int intSelectedID = 0;
        GridHitInfo downHitInfo = null;
        
        #endregion
    
        public frm_AT_Select_Incident()
        {
            InitializeComponent();
        }

        private void frm_AT_Select_Incident_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();
            Application.DoEvents();
            
            this.FormID = 20039;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            dateEditFromDate.DateTime = this.GlobalSettings.ViewedStartDate;
            dateEditToDate.DateTime = this.GlobalSettings.ViewedEndDate;

            this.sp01343_AT_Incidents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            Load_Data();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        private void Load_Data()
        {
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;
            int intShowCompletedOnly = (checkEditShowOpen.Checked ? 1 : 0);
            try
            {
                this.sp01343_AT_Incidents_ListTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01343_AT_Incidents_List, dtFromDate, dtToDate, intShowCompletedOnly);
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the incidents list.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void btnLoadIncidents_Click(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();
            Application.DoEvents();
            
            Load_Data();
            
            if (fProgress != null)
            {
                fProgress.SetProgressValue(100);  // Show Full Progress //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Records Available For Selection");
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            strSelectedValue = Convert.ToString(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "ReferenceID"));
            intSelectedID = Convert.ToInt32(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "IncidentID"));
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            if (intSelectedID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void dateEditFromDate_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditFromDate.EditValue = null;
                }
            }
        }

        private void dateEditToDate_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditToDate.EditValue = null;
                }
            }
        }


 

    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.Skins;
using DevExpress.XtraEditors.Repository;  // Required by emptyEditor //

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_AT_Incident_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        ArrayList ArrayListFilteredPicklists;  // Holds all the picklists bound to sp01372 - used for iterating round to bind and unbind generic Enter, Leave and ButtonClick events //
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        public int intXCoordinate = 0;
        public int intYCoordinate = 0;
        public string strIncidentAddressLine1 = "";
        public string strIncidentAddressLine2 = "";
        public string strIncidentAddressLine3 = "";
        public string strIncidentAddressLine4 = "";
        public string strIncidentAddressLine5 = "";
        public string strIncidentPostcode = "";

        private ArrayList arraylistFloatingPanels = new ArrayList();  // Holds which panels were floating when form was deactivated so the panels can be hidden then re-shown on form activate //

        #endregion

        public frm_AT_Incident_Edit()
        {
            InitializeComponent();
        }

        private void frm_AT_Incident_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 20141;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            ArrayListFilteredPicklists = new ArrayList();  // Note: Each GridLookUpEdit within the array should have it's tag value set to it's value in the SQL PicklistHeader Table //
            ArrayListFilteredPicklists.Add(UserPickList1GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList2GridLookUpEdit);
            ArrayListFilteredPicklists.Add(UserPickList3GridLookUpEdit);
            ArrayListFilteredPicklists.Add(intIncidentTypeIDGridLookUpEdit);
            ArrayListFilteredPicklists.Add(IncidentStatusGridLookUpEdit);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //


            // *** Gazetteer *** //
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01311_AT_Tree_Picker_Gazetteer_Search_Types);
            if (this.dataSet_AT_TreePicker.sp01311_AT_Tree_Picker_Gazetteer_Search_Types.Rows.Count > 1)
            {
                lookUpEditGazetteerSearchType.EditValue = this.dataSet_AT_TreePicker.sp01311_AT_Tree_Picker_Gazetteer_Search_Types.Rows[1]["Description"].ToString();
            }
            buttonEditGazetteerFindValue.EditValue = "";
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl3.MainView = gridView8;

            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00226_Staff_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00226_Staff_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);

            // Get default value for Gazetteer Search Radius on Menu //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strGazetteerSearchRadius = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Gazetteer_Search_Radius").ToString();
            beiGazetteerSearchRadius.EditValue = (string.IsNullOrEmpty(strGazetteerSearchRadius) ? (decimal)50.00 : Convert.ToDecimal(strGazetteerSearchRadius));

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            emptyEditor = new RepositoryItem();

            // Populate Main Dataset //
            sp01413_AT_Incident_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_Incidents.sp01413_AT_Incident_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["dtDateRecorded"] = DateTime.Now;
                        drNewRow["intX"] = intXCoordinate;
                        drNewRow["intY"] = intYCoordinate;
                        drNewRow["strIncidentAddressLine1"] = strIncidentAddressLine1;
                        drNewRow["strIncidentAddressLine2"] = strIncidentAddressLine2;
                        drNewRow["strIncidentAddressLine3"] = strIncidentAddressLine3;
                        drNewRow["strIncidentAddressLine4"] = strIncidentAddressLine4;
                        drNewRow["strIncidentAddressLine5"] = strIncidentAddressLine5;
                        drNewRow["strIncidentPostcode"] = strIncidentPostcode;
                        this.dataSet_AT_Incidents.sp01413_AT_Incident_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT_Incidents.sp01413_AT_Incident_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_AT_Incidents.sp01413_AT_Incident_Edit.Rows.Add(drNewRow);
                        this.dataSet_AT_Incidents.sp01413_AT_Incident_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp01413_AT_Incident_EditTableAdapter.Fill(this.dataSet_AT_Incidents.sp01413_AT_Incident_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
            Attach_Enter_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach Enter Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //
            Attach_ButtonClick_To_GridLookUpEdits(ArrayListFilteredPicklists);  // Attach ButtonClick Event to All Filtered GridLookUpEdits... Detached on Form Closing Event //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        DataTable data;  // Used for holding filtered dataset for picklists //
        private void Attach_Enter_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter += new EventHandler(Filter_GridView);
            }
        }

        private void Detach_Enter_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Enter -= new EventHandler(Filter_GridView);
            }
        }

        private void Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            int intHeaderID = Convert.ToInt32(glue.Tag);
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            view.MakeRowVisible(-1, true);
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            int intHeaderID = Convert.ToInt32(glue.Tag);
            data = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks;
            DataView newView = new DataView(data);
            newView.RowFilter = "[HeaderID] = " + Convert.ToString(intHeaderID) + " or [HeaderID] = 0";
            glue.Properties.DataSource = newView;
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }


        private void Attach_Leave_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave += new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Detach_Leave_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).Leave -= new EventHandler(Clear_Filter_GridView);
            }
        }

        private void Clear_Filter_GridView(object sender, EventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.EndUpdate();*/

            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = glue.Properties.View;
            view.BeginUpdate();
            glue.Properties.DataSource = this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks.DefaultView;
            view.EndUpdate();
        }


        private void Attach_ButtonClick_To_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void Detach_ButtonClick_From_GridLookUpEdits(ArrayList arraylist)
        {
            foreach (Control item in arraylist)
            {
                ((GridLookUpEdit)item).ButtonClick -= new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ButtonClick_GridView);
            }
        }

        private void ButtonClick_GridView(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    GridLookUpEdit glue = (GridLookUpEdit)sender;
                    int intHeaderID = Convert.ToInt32(glue.Tag);
                    String strPickListName = "";
                    switch (glue.Name)
                    {
                        case "intIncidentTypeIDGridLookUpEdit":
                            strPickListName = "Incident Types";
                            break;
                        case "IncidentStatusGridLookUpEdit":
                            strPickListName = "Incident Statuses";
                            break;
                        case "intVitalityGridLookUpEdit":
                            strPickListName = "Vitality";
                            break;
                        case "UserPickList1GridLookUpEdit":
                            strPickListName = "Incident User Defined 1";
                            break;
                        case "UserPickList2GridLookUpEdit":
                            strPickListName = "Incident User Defined 2";
                            break;
                        case "UserPickList3GridLookUpEdit":
                            strPickListName = "Incident User Defined 3";
                            break;
                        default:
                            break;
                    }
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, intHeaderID, strPickListName);
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.Fill(this.dataSet_AT_DataEntry.sp01372_AT_Multiple_Picklists_With_Blanks);
                    this.ValidateChildren();
                }
            }
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_AT_Incidents.sp01413_AT_Incident_Edit.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Incident", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        intIncidentTypeIDGridLookUpEdit.Focus();

                        strReferenceIDButtonEdit.Properties.ReadOnly = false;
                        strReferenceIDButtonEdit.Properties.Buttons[0].Enabled = true;
                        strReferenceIDButtonEdit.Properties.Buttons[1].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        intIncidentTypeIDGridLookUpEdit.Focus();

                        strReferenceIDButtonEdit.Properties.ReadOnly = false;
                        strReferenceIDButtonEdit.Properties.Buttons[0].Enabled = true;
                        strReferenceIDButtonEdit.Properties.Buttons[1].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        intIncidentTypeIDGridLookUpEdit.Focus();

                        strReferenceIDButtonEdit.Properties.ReadOnly = true;
                        strReferenceIDButtonEdit.Properties.Buttons[0].Enabled = true;
                        strReferenceIDButtonEdit.Properties.Buttons[1].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
                btnCopyToIncidentAddress.Enabled = false;
                btnCopyToReportedBy.Enabled = false;
            }
            else
            {
                btnCopyToIncidentAddress.Enabled = true;
                btnCopyToReportedBy.Enabled = true;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            intIncidentTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intIncidentTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intIncidentTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intIncidentTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            IncidentStatusGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            IncidentStatusGridLookUpEdit.Properties.Buttons[1].Visible = false;
            IncidentStatusGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            IncidentStatusGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intPersonRecordedByIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intPersonRecordedByIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intPersonRecordedByIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intPersonRecordedByIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            intPersonResponsibleIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            intPersonResponsibleIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            intPersonResponsibleIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            intPersonResponsibleIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList1GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList2GridLookUpEdit.Properties.Buttons[2].Visible = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[1].Enabled = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[1].Visible = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[2].Enabled = false;
            UserPickList3GridLookUpEdit.Properties.Buttons[2].Visible = false;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "9031,9057,30,9054,9055,9056", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 9031:  // Incident Type Picklist //    
                        {
                            intIncidentTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intIncidentTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intIncidentTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intIncidentTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 2012:  // Ownerships/Budgets //    
                        {
                            IncidentStatusGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            IncidentStatusGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            IncidentStatusGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            IncidentStatusGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 30:  // Staff Manager (Recorded By \ Person Responsible) //    
                        {
                            intPersonRecordedByIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intPersonRecordedByIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intPersonRecordedByIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intPersonRecordedByIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;

                            intPersonResponsibleIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            intPersonResponsibleIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            intPersonResponsibleIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            intPersonResponsibleIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate; 
                        }
                        break;
                    case 9054:  // Incident User Defined 1 //    
                        {
                            UserPickList1GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList1GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9055:  // Incident User Defined 2 //    
                        {
                            UserPickList2GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList2GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9056:  // Incident User Defined 3 //    
                        {
                            UserPickList3GridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            UserPickList3GridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                }
            }
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_AT_Incidents.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                this.sp01413ATIncidentEditBindingSource.EndEdit();
                dsChanges = this.dataSet_AT_Incidents.GetChanges();
                if (dsChanges != null)
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                    bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                    bbiFormSave.Enabled = true;
                    return true;
                }
                else
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                    bbiSave.Enabled = false;
                    bbiFormSave.Enabled = false;
                    return false;
                }
            }
        }


        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_AT_Incident_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in arraylistFloatingPanels)
            {
                dp.Show();
            }
        }

        private void frm_AT_Incident_Edit_Deactivate(object sender, EventArgs e)
        {
            arraylistFloatingPanels = new ArrayList();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in dockManager1.Panels)
            {
                if (dp.Dock == DevExpress.XtraBars.Docking.DockingStyle.Float && dp.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible)
                {
                    arraylistFloatingPanels.Add(dp);
                    dp.Hide();
                }
            }
        }


        private void frm_AT_Incident_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp01413ATIncidentEditBindingSource.EndEdit();
            try
            {
                this.sp01413_AT_Incident_EditTableAdapter.Update(dataSet_AT_Incidents);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp01413ATIncidentEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["intIncidentID"]) + ";";
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_AT_Incident_Manager")
                    {
                        var fParentForm = (frm_AT_Incident_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "");
                    }
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_AT_Incidents.sp01413_AT_Incident_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_AT_Incidents.sp01413_AT_Incident_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        private void dropDownButtonShowMap_Click(object sender, EventArgs e)
        {
            // Activates the Mapping //
            DataRowView currentRow = (DataRowView)sp01413ATIncidentEditBindingSource.Current;
            if (currentRow != null)
            {
                int intX = string.IsNullOrEmpty(currentRow["intX"].ToString()) ? 0 : Convert.ToInt32(currentRow["intX"]);
                int intY = string.IsNullOrEmpty(currentRow["intY"].ToString()) ? 0 : Convert.ToInt32(currentRow["intY"]);
                string strDescription = (string.IsNullOrEmpty(currentRow["strIncidentAddressLine1"].ToString()) ? "" : currentRow["strIncidentAddressLine1"].ToString() + " ") + (string.IsNullOrEmpty(currentRow["strIncidentAddressLine2"].ToString()) ? "" : currentRow["strIncidentAddressLine2"].ToString() + ", ") + (string.IsNullOrEmpty(currentRow["strIncidentAddressLine3"].ToString()) ? "" : currentRow["strIncidentAddressLine3"].ToString() + ", ") + (string.IsNullOrEmpty(currentRow["strIncidentAddressLine4"].ToString()) ? "" : currentRow["strIncidentAddressLine4"].ToString() + ", ") + (string.IsNullOrEmpty(currentRow["strIncidentAddressLine5"].ToString()) ? "" : currentRow["strIncidentAddressLine5"].ToString() + ", ") + (string.IsNullOrEmpty(currentRow["strIncidentPostcode"].ToString()) ? "" : currentRow["strIncidentPostcode"].ToString());
                if (string.IsNullOrEmpty(strDescription)) strDescription = "No Address Specified";
                if (intX == 0 && intY == 0)
                {
                    if (DevExpress.XtraEditors.XtraMessageBox.Show("The current Incident Address has no X and Y Coordinates - if you proceed with opening the map, it will be opened and centred on the default coordinates for the default mapping workspace.\n\nProceed Anyway?", "Incidents - Open Map", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        // Open map but don't attempt to centre it (Incident Address has no X and Y Coordinates) //
                        string strSelectedIDs = "XYincident;0;0;0;None;";
                        try
                        {
                            Mapping_Functions MapFunctions = new Mapping_Functions();
                            MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "client");
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                }
                else
                {
                    // Open map with no centred on X and Y coordinates of Incident Address //
                    decimal decSearchRange = (string.IsNullOrEmpty(beiGazetteerSearchRadius.EditValue.ToString()) ? (decimal)50.00 : Convert.ToDecimal(beiGazetteerSearchRadius.EditValue));
                    string strSelectedIDs = "XYincident;" + intX.ToString() + ";" + intY.ToString() + ";" + decSearchRange.ToString() + ";" + strDescription + ";";
                    try
                    {
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "client");
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }
            }
        }

        private void btnCopyToReportedBy_Click(object sender, EventArgs e)
        {
            // Copies Incident Address to Reported By Address //
            DataRowView currentRow = (DataRowView)sp01413ATIncidentEditBindingSource.Current;
            if (currentRow != null)
            {
                dataLayoutControl1.BeginUpdate();
                currentRow["strReportedByAddressLine1"] = currentRow["strIncidentAddressLine1"];
                currentRow["strReportedByAddressLine2"] = currentRow["strIncidentAddressLine2"];
                currentRow["strReportedByAddressLine3"] = currentRow["strIncidentAddressLine3"];
                currentRow["strReportedByAddressLine4"] = currentRow["strIncidentAddressLine4"];
                currentRow["strReportedByAddressLine5"] = currentRow["strIncidentAddressLine5"];
                currentRow["strReportedByPostcode"] = currentRow["strIncidentPostcode"];
                dataLayoutControl1.EndUpdate();
                this.ValidateChildren();  // Force Validation Message on any controls containing them //     
                this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            }
        }

        private void btnCopyToIncidentAddress_Click(object sender, EventArgs e)
        {
            // Copies Incident Address to Reported By Address //
            DataRowView currentRow = (DataRowView)sp01413ATIncidentEditBindingSource.Current;
            if (currentRow != null)
            {
                dataLayoutControl1.BeginUpdate();
                currentRow["strIncidentAddressLine1"] = currentRow["strReportedByAddressLine1"];
                currentRow["strIncidentAddressLine2"] = currentRow["strReportedByAddressLine2"];
                currentRow["strIncidentAddressLine3"] = currentRow["strReportedByAddressLine3"];
                currentRow["strIncidentAddressLine4"] = currentRow["strReportedByAddressLine4"];
                currentRow["strIncidentAddressLine5"] = currentRow["strReportedByAddressLine5"];
                currentRow["strIncidentPostcode"] = currentRow["strReportedByPostcode"];
                dataLayoutControl1.EndUpdate();
                this.ValidateChildren();  // Force Validation Message on any controls containing them //     
                this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            }
        }



        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void strReferenceIDButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            string strTableName = "tblIncident";
            string strFieldName = "strReferenceID";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    strReferenceIDButtonEdit.EditValue = fChildForm.SelectedSequence;
                    //i_strLastUsedSequencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                // Get next value in sequence //
                string strSequence = strReferenceIDButtonEdit.EditValue.ToString() ?? "";

                SequenceNumberGetNext sequence = new SequenceNumberGetNext();
                string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //

                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";


                // Check if value is already present within dataset //
                string strCurrentReferenceID = strReferenceIDButtonEdit.EditValue.ToString();
                if (string.IsNullOrEmpty(strCurrentReferenceID)) strCurrentReferenceID = "";
                bool boolUniqueValueFound = false;
                bool boolDuplicateFound = false;
                do
                {
                    boolDuplicateFound = false;
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    foreach (DataRow dr in this.dataSet_AT_Incidents.sp01413_AT_Incident_Edit.Rows)
                    {
                        if (dr["strReferenceID"].ToString() == (strSequence + strTempNumber))
                        {
                            intIncrement++;
                            boolDuplicateFound = true;
                            break;
                        }
                    }
                    if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
                } while (!boolUniqueValueFound);
                strReferenceIDButtonEdit.EditValue = strSequence + strTempNumber;
            }
        }

        private void dtDateRecordedDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }

        private void dtDateRecordedDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(dtDateRecordedDateEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(dtDateRecordedDateEdit, "");
            }
        }

        private void intPersonRecordedByIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 102, "Staff");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00226_Staff_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);
                }
            }
        }

        private void intPersonResponsibleIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if ("edit".Equals(e.Button.Tag))
            {
                Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 102, "Staff");
            }
            else if ("reload".Equals(e.Button.Tag))
            {
                sp00226_Staff_List_With_BlankTableAdapter.Fill(this.dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);
            }
        }

        private void TargetDateDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    TargetDateDateEdit.EditValue = null;
                }
            }
        }

        private void DateClosedDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Clear)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    DateClosedDateEdit.EditValue = null;
                }
            }
        }

        #endregion


        #region Gazetteer

        private void checkButtonGazetteer_CheckedChanged(object sender, EventArgs e)
        {
            if (checkButtonGazetteer.Checked)
            {
                dockPanelGazetteer.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelGazetteer.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void checkButtonGazetteer2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkButtonGazetteer2.Checked)
            {
                dockPanelGazetteer.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelGazetteer.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void dockPanelGazetteer_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (checkButtonGazetteer.Checked) checkButtonGazetteer.Checked = false;
        }

        private void lookUpEditGazetteerSearchType_EditValueChanged(object sender, EventArgs e)
        {
            this.dataSet_AT_TreePicker.sp01312_AT_Tree_Picker_Gazetteer_Search.Rows.Clear();
            LookUpEdit lue = (LookUpEdit)sender;
            if (lue.EditValue.ToString().ToLower() == "localities")
            {
                gridControl3.MainView = gridView7;
            }
            else
            {
                gridControl3.MainView = gridView8;
            }
        }

        private void buttonEditGazetteerFindValue_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GazetteerFind();
        }

        private void GazetteerFind()
        {
            WaitDialogForm loading = new WaitDialogForm("Searching for Matches...", "WoodPlan Incidents");
            loading.Show();

            string strType = lookUpEditGazetteerSearchType.EditValue.ToString();
            int intPattern = Convert.ToInt32(radioGroupGazetteerMatchPattern.EditValue);
            string strValue = (intPattern == 0 ? "" : "%") + buttonEditGazetteerFindValue.EditValue.ToString() + "%";
            GridView view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01312_AT_Tree_Picker_Gazetteer_Search, strType, strValue);
            view.EndUpdate();

            loading.Close();
        }

        private void gridView7_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle) Transfer_Address(view.FocusedRowHandle);
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
        }

        private void gridView7_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bbiGazetteerTransferAddressToIncident.Enabled = (intCount == 1 ? true : false);

                pmGazetteer.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView8_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle) Transfer_Address(view.FocusedRowHandle);
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bbiGazetteerTransferAddressToIncident.Enabled = (intCount == 1 ? true : false);

                pmGazetteer.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void buttonEditGazetteerFindValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) GazetteerFind();
        }

        private void bbiGazetteerTransferAddress_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one Gazetteer address match to transfer to the Incident Address before proceeding.", "Gazetteer - Transfer Matching Address to Incident", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Transfer_Address(intRowHandles[0]);
        }

        private void bbiGazetteerTransferToReportedBy_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one Gazetteer address match to transfer to the Reported By Address before proceeding.", "Gazetteer - Transfer Matching Address to Incident", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Transfer_Address2(intRowHandles[0]);
        }

        private void bbiGazetteerClearResults_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridControl3.BeginUpdate();
            dataSet_AT_TreePicker.sp01312_AT_Tree_Picker_Gazetteer_Search.Rows.Clear();
            gridControl3.EndUpdate();
        }

        private void Transfer_Address(int intRow)
        {
            GridView view = (GridView)gridControl3.MainView;
            int intX = Convert.ToInt32(view.GetRowCellValue(intRow, "XCoordinate"));
            int intY = Convert.ToInt32(view.GetRowCellValue(intRow, "YCoordinate"));
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(intRow, "RecordID"));
            string strAddressLine1 = Convert.ToString(view.GetRowCellValue(intRow, "AddressLine1"));
            string strAddressLine2 = Convert.ToString(view.GetRowCellValue(intRow, "AddressLine2"));
            string strAddressLine3 = Convert.ToString(view.GetRowCellValue(intRow, "AddressLine3"));
            string strAddressLine4 = Convert.ToString(view.GetRowCellValue(intRow, "AddressLine4"));
            string strAddressLine5 = Convert.ToString(view.GetRowCellValue(intRow, "AddressLine5"));
            string strPostcode = Convert.ToString(view.GetRowCellValue(intRow, "Postcode"));

            DataRowView currentRow = (DataRowView)sp01413ATIncidentEditBindingSource.Current;
            if (currentRow != null)
            {
                dataLayoutControl1.BeginUpdate();
                currentRow["intX"] = intX;
                currentRow["intY"] = intY;
                currentRow["strIncidentAddressLine1"] = strAddressLine1;
                currentRow["strIncidentAddressLine2"] = strAddressLine2;
                currentRow["strIncidentAddressLine3"] = strAddressLine3;
                currentRow["strIncidentAddressLine4"] = strAddressLine4;
                currentRow["strIncidentAddressLine5"] = strAddressLine5;
                currentRow["strIncidentPostcode"] = strPostcode;
                dataLayoutControl1.EndUpdate();
                this.ValidateChildren();  // Force Validation Message on any controls containing them //     
                this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            }
        }

        private void Transfer_Address2(int intRow)
        {
            GridView view = (GridView)gridControl3.MainView;
            int intX = Convert.ToInt32(view.GetRowCellValue(intRow, "XCoordinate"));
            int intY = Convert.ToInt32(view.GetRowCellValue(intRow, "YCoordinate"));
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(intRow, "RecordID"));
            string strAddressLine1 = Convert.ToString(view.GetRowCellValue(intRow, "AddressLine1"));
            string strAddressLine2 = Convert.ToString(view.GetRowCellValue(intRow, "AddressLine2"));
            string strAddressLine3 = Convert.ToString(view.GetRowCellValue(intRow, "AddressLine3"));
            string strAddressLine4 = Convert.ToString(view.GetRowCellValue(intRow, "AddressLine4"));
            string strAddressLine5 = Convert.ToString(view.GetRowCellValue(intRow, "AddressLine5"));
            string strPostcode = Convert.ToString(view.GetRowCellValue(intRow, "Postcode"));

            DataRowView currentRow = (DataRowView)sp01413ATIncidentEditBindingSource.Current;
            if (currentRow != null)
            {
                dataLayoutControl1.BeginUpdate();
                currentRow["intX"] = intX;
                currentRow["intY"] = intY;
                currentRow["strReportedByAddressLine1"] = strAddressLine1;
                currentRow["strReportedByAddressLine2"] = strAddressLine2;
                currentRow["strReportedByAddressLine3"] = strAddressLine3;
                currentRow["strReportedByAddressLine4"] = strAddressLine4;
                currentRow["strReportedByAddressLine5"] = strAddressLine5;
                currentRow["strReportedByPostcode"] = strPostcode;
                dataLayoutControl1.EndUpdate();
                this.ValidateChildren();  // Force Validation Message on any controls containing them //     
                this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            }
        }

        #endregion


 

    
    
    }
}

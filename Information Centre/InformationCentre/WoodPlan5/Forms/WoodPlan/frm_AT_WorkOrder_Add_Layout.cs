using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;


namespace WoodPlan5
{
    public partial class frm_AT_WorkOrder_Add_Layout : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int intReturnedValue = 0;
        public string strReturnedLayoutName = "";

        #endregion

        public frm_AT_WorkOrder_Add_Layout()
        {
            InitializeComponent();
            InitValidationRules();
        }

        private void frm_AT_WorkOrder_Add_Layout_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;

            dxValidationProvider1.ValidationMode = ValidationMode.Auto;
            dxValidationProvider1.Validate();

            sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01210_AT_Report_Add_Layouts_Choose_Start_Layout, 1, 1, "AmenityTreesWorkOrderLayoutLocation");
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            checkEdit2.Checked = ! checkEdit1.Checked;
            if (checkEdit1.Checked)
            {
                sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutGridControl.Visible = true;
                sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutGridControl.Enabled = true;
                layoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutGridControl.Visible = false;
                sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutGridControl.Enabled = false;
                layoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            checkEdit1.Checked = !checkEdit2.Checked;
            if (checkEdit2.Checked)
            {
                sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutGridControl.Visible = false;
                sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutGridControl.Enabled = false;
                layoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
            else
            {
                sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutGridControl.Visible = true;
                sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutGridControl.Enabled = true;
                layoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
        }


        private void InitValidationRules()
        {
            // Create Rule // 
            ConditionValidationRule notEmptyValidationRule = new ConditionValidationRule();
            notEmptyValidationRule.ConditionOperator = ConditionOperator.IsNotBlank;
            notEmptyValidationRule.ErrorText = "Please enter a value [50 characters maximum]";

            // Link Rule to control //
            dxValidationProvider1.SetValidationRule(textEdit1, notEmptyValidationRule);
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {

            strReturnedLayoutName = textEdit1.Text.Trim();
            if (strReturnedLayoutName == null || strReturnedLayoutName == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter a unique descriptor for the new layout in the box provided before proceeding.", "Create Work Order Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Check the Layout Descriptor is Unique //
            int intMatchingDescriptors = 0;
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            intMatchingDescriptors = Convert.ToInt32(GetSetting.sp01211_AT_Report_Add_Layouts_Check_Descriptor_Unique(1, 1, strReturnedLayoutName));
            if (intMatchingDescriptors > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Layout Desriptor [" + strReturnedLayoutName + "] is already linked to another layout!\n\nPlease enter a different descriptor before proceeding.", "Create Work Order Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            if (checkEdit1.Checked)
            {
                GridView view = (GridView)sp01210_AT_Report_Add_Layouts_Choose_Start_LayoutGridControl.MainView;
                if (view.RowCount == 0 || view.FocusedRowHandle < 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select the layout to use as a starting point from the list available before proceeding.", "Create Work Order Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                intReturnedValue = Convert.ToInt32(view.GetFocusedRowCellValue("ReportLayoutID"));
            }
            else
            {
                intReturnedValue = 0; // Use a blank layout //
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            intReturnedValue = -1;
            this.DialogResult = DialogResult.No;
            this.Close();

        }



    }
}


namespace WoodPlan5
{
    partial class frm_AT_Mapping_Workspace_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Mapping_Workspace_Edit));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.popupContainerControl2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnSetScale = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.seUserDefinedScale = new DevExpress.XtraEditors.SpinEdit();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp01254ATTreePickerscalelistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_TreePicker = new WoodPlan5.DataSet_AT_TreePicker();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colScaleDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.CreatedByNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp01274ATTreePickerWorkspaceEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.WorkspaceNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WorkspaceRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DefaultWorkspaceCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DefaultScalePopupContainerEdit = new DevExpress.XtraEditors.PopupContainerEdit();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01267ATTreePickerWorkspacelayerslistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLayerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkspaceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLayerPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLayerHitable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPreserveDefaultStyling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransparency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGreyScale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUseThematicStyling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThematicStyleSet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPointSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DefaultCentreXSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp01266ATTreePickerWorkspaceSharegroupsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGroupDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.DefaultCentreYSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCreatedByName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkspaceName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkspaceRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDefaultWorkspace = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDefaultScale = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDefaultCentreX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDefaultCentreY = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp01254_AT_Tree_Picker_scale_listTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01254_AT_Tree_Picker_scale_listTableAdapter();
            this.sp01266_AT_Tree_Picker_Workspace_Share_groupsTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01266_AT_Tree_Picker_Workspace_Share_groupsTableAdapter();
            this.dxValidationProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.sp01267_AT_Tree_Picker_Workspace_layers_listTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01267_AT_Tree_Picker_Workspace_layers_listTableAdapter();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiProperties = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu_LayerManager = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiAddLayer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteLayer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLayerManagerProperties = new DevExpress.XtraBars.BarButtonItem();
            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).BeginInit();
            this.popupContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seUserDefinedScale.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01254ATTreePickerscalelistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01274ATTreePickerWorkspaceEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspaceNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspaceRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultWorkspaceCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultScalePopupContainerEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01267ATTreePickerWorkspacelayerslistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultCentreXSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01266ATTreePickerWorkspaceSharegroupsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultCentreYSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkspaceName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkspaceRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultWorkspace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultCentreX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultCentreY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_LayerManager)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(543, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 629);
            this.barDockControlBottom.Size = new System.Drawing.Size(543, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 629);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(543, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 629);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.bbiProperties,
            this.bbiLayerManagerProperties,
            this.bbiAddLayer,
            this.bbiDeleteLayer});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(147, 600);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(122, 22);
            this.btnOK.StyleController = this.dataLayoutControl1;
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.popupContainerControl2);
            this.dataLayoutControl1.Controls.Add(this.CreatedByNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkspaceNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkspaceRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DefaultWorkspaceCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DefaultScalePopupContainerEdit);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer2);
            this.dataLayoutControl1.Controls.Add(this.DefaultCentreXSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.btnCancel);
            this.dataLayoutControl1.Controls.Add(this.btnOK);
            this.dataLayoutControl1.Controls.Add(this.groupControl1);
            this.dataLayoutControl1.Controls.Add(this.DefaultCentreYSpinEdit);
            this.dataLayoutControl1.DataSource = this.sp01274ATTreePickerWorkspaceEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup4;
            this.dataLayoutControl1.Size = new System.Drawing.Size(543, 629);
            this.dataLayoutControl1.TabIndex = 7;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // popupContainerControl2
            // 
            this.popupContainerControl2.Controls.Add(this.btnSetScale);
            this.popupContainerControl2.Controls.Add(this.layoutControl4);
            this.popupContainerControl2.Location = new System.Drawing.Point(248, 80);
            this.popupContainerControl2.Name = "popupContainerControl2";
            this.popupContainerControl2.Size = new System.Drawing.Size(194, 207);
            this.popupContainerControl2.TabIndex = 24;
            // 
            // btnSetScale
            // 
            this.btnSetScale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSetScale.Location = new System.Drawing.Point(3, 181);
            this.btnSetScale.Name = "btnSetScale";
            this.btnSetScale.Size = new System.Drawing.Size(75, 23);
            this.btnSetScale.TabIndex = 4;
            this.btnSetScale.Text = "OK";
            this.btnSetScale.Click += new System.EventHandler(this.btnSetScale_Click);
            // 
            // layoutControl4
            // 
            this.layoutControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl4.Controls.Add(this.seUserDefinedScale);
            this.layoutControl4.Controls.Add(this.gridControl4);
            this.layoutControl4.Location = new System.Drawing.Point(3, 3);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup7;
            this.layoutControl4.Size = new System.Drawing.Size(188, 176);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // seUserDefinedScale
            // 
            this.seUserDefinedScale.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seUserDefinedScale.Location = new System.Drawing.Point(100, 153);
            this.seUserDefinedScale.MenuManager = this.barManager1;
            this.seUserDefinedScale.Name = "seUserDefinedScale";
            this.seUserDefinedScale.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seUserDefinedScale.Properties.IsFloatValue = false;
            this.seUserDefinedScale.Properties.Mask.EditMask = "#####0";
            this.seUserDefinedScale.Properties.MaxValue = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.seUserDefinedScale.Size = new System.Drawing.Size(85, 20);
            this.seUserDefinedScale.StyleController = this.layoutControl4;
            this.seUserDefinedScale.TabIndex = 5;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp01254ATTreePickerscalelistBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 3);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(182, 146);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp01254ATTreePickerscalelistBindingSource
            // 
            this.sp01254ATTreePickerscalelistBindingSource.DataMember = "sp01254_AT_Tree_Picker_scale_list";
            this.sp01254ATTreePickerscalelistBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // dataSet_AT_TreePicker
            // 
            this.dataSet_AT_TreePicker.DataSetName = "DataSet_AT_TreePicker";
            this.dataSet_AT_TreePicker.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colScaleDescription,
            this.colScale});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colScale, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseDown);
            // 
            // colScaleDescription
            // 
            this.colScaleDescription.Caption = "Scale";
            this.colScaleDescription.FieldName = "ScaleDescription";
            this.colScaleDescription.Name = "colScaleDescription";
            this.colScaleDescription.OptionsColumn.AllowEdit = false;
            this.colScaleDescription.OptionsColumn.AllowFocus = false;
            this.colScaleDescription.OptionsColumn.ReadOnly = true;
            this.colScaleDescription.Visible = true;
            this.colScaleDescription.VisibleIndex = 0;
            this.colScaleDescription.Width = 162;
            // 
            // colScale
            // 
            this.colScale.Caption = "Scale Number";
            this.colScale.FieldName = "Scale";
            this.colScale.Name = "colScale";
            this.colScale.OptionsColumn.AllowEdit = false;
            this.colScale.OptionsColumn.AllowFocus = false;
            this.colScale.OptionsColumn.ReadOnly = true;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19,
            this.layoutControlItem20});
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(188, 176);
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.gridControl4;
            this.layoutControlItem19.CustomizationFormText = "Scale Grid:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(186, 150);
            this.layoutControlItem19.Text = "Scale Grid:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AllowHtmlStringInCaption = true;
            this.layoutControlItem20.Control = this.seUserDefinedScale;
            this.layoutControlItem20.CustomizationFormText = "User Defined Scale:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(186, 24);
            this.layoutControlItem20.Text = "User Defined Scale:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(94, 13);
            // 
            // CreatedByNameTextEdit
            // 
            this.CreatedByNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01274ATTreePickerWorkspaceEditBindingSource, "CreatedByName", true));
            this.CreatedByNameTextEdit.Enabled = false;
            this.CreatedByNameTextEdit.Location = new System.Drawing.Point(113, 37);
            this.CreatedByNameTextEdit.MenuManager = this.barManager1;
            this.CreatedByNameTextEdit.Name = "CreatedByNameTextEdit";
            this.CreatedByNameTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByNameTextEdit, true);
            this.CreatedByNameTextEdit.Size = new System.Drawing.Size(415, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByNameTextEdit, optionsSpelling1);
            this.CreatedByNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByNameTextEdit.TabIndex = 6;
            // 
            // sp01274ATTreePickerWorkspaceEditBindingSource
            // 
            this.sp01274ATTreePickerWorkspaceEditBindingSource.DataMember = "sp01274_AT_Tree_Picker_Workspace_Edit";
            this.sp01274ATTreePickerWorkspaceEditBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // WorkspaceNameTextEdit
            // 
            this.WorkspaceNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01274ATTreePickerWorkspaceEditBindingSource, "WorkspaceName", true));
            this.WorkspaceNameTextEdit.Location = new System.Drawing.Point(113, 61);
            this.WorkspaceNameTextEdit.MenuManager = this.barManager1;
            this.WorkspaceNameTextEdit.Name = "WorkspaceNameTextEdit";
            this.WorkspaceNameTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkspaceNameTextEdit, true);
            this.WorkspaceNameTextEdit.Size = new System.Drawing.Size(415, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkspaceNameTextEdit, optionsSpelling2);
            this.WorkspaceNameTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkspaceNameTextEdit.TabIndex = 7;
            // 
            // WorkspaceRemarksMemoEdit
            // 
            this.WorkspaceRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01274ATTreePickerWorkspaceEditBindingSource, "WorkspaceRemarks", true));
            this.WorkspaceRemarksMemoEdit.Location = new System.Drawing.Point(113, 85);
            this.WorkspaceRemarksMemoEdit.MenuManager = this.barManager1;
            this.WorkspaceRemarksMemoEdit.Name = "WorkspaceRemarksMemoEdit";
            this.WorkspaceRemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkspaceRemarksMemoEdit, true);
            this.WorkspaceRemarksMemoEdit.Size = new System.Drawing.Size(415, 48);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkspaceRemarksMemoEdit, optionsSpelling3);
            this.WorkspaceRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.WorkspaceRemarksMemoEdit.TabIndex = 8;
            // 
            // DefaultWorkspaceCheckEdit
            // 
            this.DefaultWorkspaceCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01274ATTreePickerWorkspaceEditBindingSource, "DefaultWorkspace", true));
            this.DefaultWorkspaceCheckEdit.EditValue = 0;
            this.DefaultWorkspaceCheckEdit.Location = new System.Drawing.Point(113, 137);
            this.DefaultWorkspaceCheckEdit.MenuManager = this.barManager1;
            this.DefaultWorkspaceCheckEdit.Name = "DefaultWorkspaceCheckEdit";
            this.DefaultWorkspaceCheckEdit.Properties.Caption = "";
            this.DefaultWorkspaceCheckEdit.Properties.ValueChecked = 1;
            this.DefaultWorkspaceCheckEdit.Properties.ValueUnchecked = 0;
            this.DefaultWorkspaceCheckEdit.Size = new System.Drawing.Size(415, 19);
            this.DefaultWorkspaceCheckEdit.StyleController = this.dataLayoutControl1;
            this.DefaultWorkspaceCheckEdit.TabIndex = 9;
            // 
            // DefaultScalePopupContainerEdit
            // 
            this.DefaultScalePopupContainerEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01274ATTreePickerWorkspaceEditBindingSource, "DefaultScale", true));
            this.DefaultScalePopupContainerEdit.Location = new System.Drawing.Point(113, 160);
            this.DefaultScalePopupContainerEdit.MenuManager = this.barManager1;
            this.DefaultScalePopupContainerEdit.Name = "DefaultScalePopupContainerEdit";
            this.DefaultScalePopupContainerEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DefaultScalePopupContainerEdit.Properties.PopupControl = this.popupContainerControl2;
            this.DefaultScalePopupContainerEdit.Size = new System.Drawing.Size(415, 20);
            this.DefaultScalePopupContainerEdit.StyleController = this.dataLayoutControl1;
            this.DefaultScalePopupContainerEdit.TabIndex = 11;
            this.DefaultScalePopupContainerEdit.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.DefaultScalePopupContainerEdit_QueryResultValue);
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Grid = this.gridControl1;
            this.gridSplitContainer2.Location = new System.Drawing.Point(15, 462);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer2.Size = new System.Drawing.Size(513, 126);
            this.gridSplitContainer2.TabIndex = 25;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01267ATTreePickerWorkspacelayerslistBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(513, 126);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.gridControl1_Paint);
            this.gridControl1.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridControl1_DragDrop);
            this.gridControl1.DragOver += new System.Windows.Forms.DragEventHandler(this.gridControl1_DragOver);
            this.gridControl1.DragLeave += new System.EventHandler(this.gridControl1_DragLeave);
            // 
            // sp01267ATTreePickerWorkspacelayerslistBindingSource
            // 
            this.sp01267ATTreePickerWorkspacelayerslistBindingSource.DataMember = "sp01267_AT_Tree_Picker_Workspace_layers_list";
            this.sp01267ATTreePickerWorkspacelayerslistBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLayerID,
            this.colWorkspaceID,
            this.colLayerType,
            this.colLayerTypeDescription,
            this.colLayerName,
            this.colLayerPath,
            this.colLayerOrder,
            this.colLayerVisible,
            this.colLayerHitable,
            this.colPreserveDefaultStyling,
            this.colTransparency,
            this.colLineColour,
            this.colLineWidth,
            this.colGreyScale,
            this.colUseThematicStyling,
            this.colThematicStyleSet,
            this.colPointSize});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 518, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLayerOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.ViewCaption = "Loaded Layers";
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseMove);
            // 
            // colLayerID
            // 
            this.colLayerID.Caption = "Layer ID";
            this.colLayerID.FieldName = "LayerID";
            this.colLayerID.Name = "colLayerID";
            this.colLayerID.OptionsColumn.AllowEdit = false;
            this.colLayerID.OptionsColumn.AllowFocus = false;
            this.colLayerID.OptionsColumn.ReadOnly = true;
            this.colLayerID.Width = 58;
            // 
            // colWorkspaceID
            // 
            this.colWorkspaceID.Caption = "Workspace ID";
            this.colWorkspaceID.FieldName = "WorkspaceID";
            this.colWorkspaceID.Name = "colWorkspaceID";
            this.colWorkspaceID.OptionsColumn.AllowEdit = false;
            this.colWorkspaceID.OptionsColumn.AllowFocus = false;
            this.colWorkspaceID.OptionsColumn.ReadOnly = true;
            this.colWorkspaceID.Width = 78;
            // 
            // colLayerType
            // 
            this.colLayerType.Caption = "Layer Type ID";
            this.colLayerType.FieldName = "LayerType";
            this.colLayerType.Name = "colLayerType";
            this.colLayerType.OptionsColumn.AllowEdit = false;
            this.colLayerType.OptionsColumn.AllowFocus = false;
            this.colLayerType.OptionsColumn.ReadOnly = true;
            this.colLayerType.Width = 79;
            // 
            // colLayerTypeDescription
            // 
            this.colLayerTypeDescription.Caption = "Layer Type";
            this.colLayerTypeDescription.FieldName = "LayerTypeDescription";
            this.colLayerTypeDescription.Name = "colLayerTypeDescription";
            this.colLayerTypeDescription.OptionsColumn.AllowEdit = false;
            this.colLayerTypeDescription.OptionsColumn.AllowFocus = false;
            this.colLayerTypeDescription.OptionsColumn.ReadOnly = true;
            this.colLayerTypeDescription.Visible = true;
            this.colLayerTypeDescription.VisibleIndex = 1;
            // 
            // colLayerName
            // 
            this.colLayerName.Caption = "Layer Name";
            this.colLayerName.ColumnEdit = this.repositoryItemTextEdit1;
            this.colLayerName.FieldName = "LayerName";
            this.colLayerName.Name = "colLayerName";
            this.colLayerName.Visible = true;
            this.colLayerName.VisibleIndex = 0;
            this.colLayerName.Width = 252;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 100;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colLayerPath
            // 
            this.colLayerPath.Caption = "Layer Path";
            this.colLayerPath.FieldName = "LayerPath";
            this.colLayerPath.Name = "colLayerPath";
            this.colLayerPath.OptionsColumn.AllowEdit = false;
            this.colLayerPath.OptionsColumn.AllowFocus = false;
            this.colLayerPath.OptionsColumn.ReadOnly = true;
            this.colLayerPath.Width = 73;
            // 
            // colLayerOrder
            // 
            this.colLayerOrder.Caption = "Order";
            this.colLayerOrder.FieldName = "LayerOrder";
            this.colLayerOrder.Name = "colLayerOrder";
            this.colLayerOrder.OptionsColumn.AllowEdit = false;
            this.colLayerOrder.OptionsColumn.AllowFocus = false;
            this.colLayerOrder.OptionsColumn.ReadOnly = true;
            this.colLayerOrder.Width = 62;
            // 
            // colLayerVisible
            // 
            this.colLayerVisible.Caption = "Visible";
            this.colLayerVisible.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colLayerVisible.FieldName = "LayerVisible";
            this.colLayerVisible.Name = "colLayerVisible";
            this.colLayerVisible.Visible = true;
            this.colLayerVisible.VisibleIndex = 2;
            this.colLayerVisible.Width = 50;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colLayerHitable
            // 
            this.colLayerHitable.Caption = "Hitable";
            this.colLayerHitable.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colLayerHitable.FieldName = "LayerHitable";
            this.colLayerHitable.Name = "colLayerHitable";
            this.colLayerHitable.Visible = true;
            this.colLayerHitable.VisibleIndex = 3;
            this.colLayerHitable.Width = 54;
            // 
            // colPreserveDefaultStyling
            // 
            this.colPreserveDefaultStyling.Caption = "Preserve Default Styling";
            this.colPreserveDefaultStyling.FieldName = "PreserveDefaultStyling";
            this.colPreserveDefaultStyling.Name = "colPreserveDefaultStyling";
            this.colPreserveDefaultStyling.OptionsColumn.AllowEdit = false;
            this.colPreserveDefaultStyling.OptionsColumn.AllowFocus = false;
            this.colPreserveDefaultStyling.OptionsColumn.ReadOnly = true;
            this.colPreserveDefaultStyling.Width = 137;
            // 
            // colTransparency
            // 
            this.colTransparency.Caption = "Transparency";
            this.colTransparency.FieldName = "Transparency";
            this.colTransparency.Name = "colTransparency";
            this.colTransparency.OptionsColumn.AllowEdit = false;
            this.colTransparency.OptionsColumn.AllowFocus = false;
            this.colTransparency.OptionsColumn.ReadOnly = true;
            this.colTransparency.Width = 87;
            // 
            // colLineColour
            // 
            this.colLineColour.Caption = "Line Colour";
            this.colLineColour.FieldName = "LineColour";
            this.colLineColour.Name = "colLineColour";
            this.colLineColour.OptionsColumn.AllowEdit = false;
            this.colLineColour.OptionsColumn.AllowFocus = false;
            this.colLineColour.OptionsColumn.ReadOnly = true;
            this.colLineColour.Width = 74;
            // 
            // colLineWidth
            // 
            this.colLineWidth.Caption = "Line Width";
            this.colLineWidth.FieldName = "LineWidth";
            this.colLineWidth.Name = "colLineWidth";
            this.colLineWidth.OptionsColumn.AllowEdit = false;
            this.colLineWidth.OptionsColumn.AllowFocus = false;
            this.colLineWidth.OptionsColumn.ReadOnly = true;
            this.colLineWidth.Width = 71;
            // 
            // colGreyScale
            // 
            this.colGreyScale.Caption = "Grey Scale";
            this.colGreyScale.FieldName = "GreyScale";
            this.colGreyScale.Name = "colGreyScale";
            this.colGreyScale.OptionsColumn.AllowEdit = false;
            this.colGreyScale.OptionsColumn.AllowFocus = false;
            this.colGreyScale.OptionsColumn.ReadOnly = true;
            this.colGreyScale.Width = 72;
            // 
            // colUseThematicStyling
            // 
            this.colUseThematicStyling.Caption = "Use Thematic Styling";
            this.colUseThematicStyling.FieldName = "UseThematicStyling";
            this.colUseThematicStyling.Name = "colUseThematicStyling";
            this.colUseThematicStyling.OptionsColumn.AllowEdit = false;
            this.colUseThematicStyling.OptionsColumn.AllowFocus = false;
            this.colUseThematicStyling.OptionsColumn.ReadOnly = true;
            this.colUseThematicStyling.Width = 120;
            // 
            // colThematicStyleSet
            // 
            this.colThematicStyleSet.Caption = "Thematic Style Set";
            this.colThematicStyleSet.FieldName = "ThematicStyleSet";
            this.colThematicStyleSet.Name = "colThematicStyleSet";
            this.colThematicStyleSet.OptionsColumn.AllowEdit = false;
            this.colThematicStyleSet.OptionsColumn.AllowFocus = false;
            this.colThematicStyleSet.OptionsColumn.ReadOnly = true;
            this.colThematicStyleSet.Width = 110;
            // 
            // colPointSize
            // 
            this.colPointSize.Caption = "Point Size";
            this.colPointSize.FieldName = "PointSize";
            this.colPointSize.Name = "colPointSize";
            this.colPointSize.OptionsColumn.AllowEdit = false;
            this.colPointSize.OptionsColumn.AllowFocus = false;
            this.colPointSize.OptionsColumn.ReadOnly = true;
            this.colPointSize.Width = 67;
            // 
            // DefaultCentreXSpinEdit
            // 
            this.DefaultCentreXSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01274ATTreePickerWorkspaceEditBindingSource, "DefaultCentreX", true));
            this.DefaultCentreXSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DefaultCentreXSpinEdit.Enabled = false;
            this.DefaultCentreXSpinEdit.Location = new System.Drawing.Point(113, 184);
            this.DefaultCentreXSpinEdit.MenuManager = this.barManager1;
            this.DefaultCentreXSpinEdit.Name = "DefaultCentreXSpinEdit";
            this.DefaultCentreXSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DefaultCentreXSpinEdit.Size = new System.Drawing.Size(156, 20);
            this.DefaultCentreXSpinEdit.StyleController = this.dataLayoutControl1;
            this.DefaultCentreXSpinEdit.TabIndex = 12;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(273, 600);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(121, 22);
            this.btnCancel.StyleController = this.dataLayoutControl1;
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridSplitContainer1);
            this.groupControl1.Controls.Add(this.radioGroup1);
            this.groupControl1.Location = new System.Drawing.Point(7, 216);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(529, 206);
            this.groupControl1.TabIndex = 12;
            this.groupControl1.Text = "Sharing";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl2;
            this.gridSplitContainer1.Location = new System.Drawing.Point(22, 86);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer1.Size = new System.Drawing.Size(499, 113);
            this.gridSplitContainer1.TabIndex = 13;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp01266ATTreePickerWorkspaceSharegroupsBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Enabled = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl2.Size = new System.Drawing.Size(499, 113);
            this.gridControl2.TabIndex = 3;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp01266ATTreePickerWorkspaceSharegroupsBindingSource
            // 
            this.sp01266ATTreePickerWorkspaceSharegroupsBindingSource.DataMember = "sp01266_AT_Tree_Picker_Workspace_Share_groups";
            this.sp01266ATTreePickerWorkspaceSharegroupsBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGroupDescription,
            this.colCreatedBy,
            this.colGroupRemarks,
            this.colGroupID,
            this.colCreatedByID,
            this.colColour});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            // 
            // colGroupDescription
            // 
            this.colGroupDescription.Caption = "Group Description";
            this.colGroupDescription.FieldName = "GroupDescription";
            this.colGroupDescription.Name = "colGroupDescription";
            this.colGroupDescription.OptionsColumn.AllowEdit = false;
            this.colGroupDescription.OptionsColumn.AllowFocus = false;
            this.colGroupDescription.OptionsColumn.ReadOnly = true;
            this.colGroupDescription.Visible = true;
            this.colGroupDescription.VisibleIndex = 0;
            this.colGroupDescription.Width = 240;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Created By";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.AllowEdit = false;
            this.colCreatedBy.OptionsColumn.AllowFocus = false;
            this.colCreatedBy.OptionsColumn.ReadOnly = true;
            this.colCreatedBy.Visible = true;
            this.colCreatedBy.VisibleIndex = 1;
            this.colCreatedBy.Width = 128;
            // 
            // colGroupRemarks
            // 
            this.colGroupRemarks.Caption = "Remarks";
            this.colGroupRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colGroupRemarks.FieldName = "GroupRemarks";
            this.colGroupRemarks.Name = "colGroupRemarks";
            this.colGroupRemarks.OptionsColumn.ReadOnly = true;
            this.colGroupRemarks.Visible = true;
            this.colGroupRemarks.VisibleIndex = 2;
            this.colGroupRemarks.Width = 63;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGroupID
            // 
            this.colGroupID.Caption = "Group ID";
            this.colGroupID.FieldName = "GroupID";
            this.colGroupID.Name = "colGroupID";
            this.colGroupID.OptionsColumn.AllowEdit = false;
            this.colGroupID.OptionsColumn.AllowFocus = false;
            this.colGroupID.OptionsColumn.ReadOnly = true;
            this.colGroupID.Width = 65;
            // 
            // colCreatedByID
            // 
            this.colCreatedByID.Caption = "Created By ID";
            this.colCreatedByID.FieldName = "CreatedByID";
            this.colCreatedByID.Name = "colCreatedByID";
            this.colCreatedByID.OptionsColumn.AllowEdit = false;
            this.colCreatedByID.OptionsColumn.AllowFocus = false;
            this.colCreatedByID.OptionsColumn.ReadOnly = true;
            this.colCreatedByID.Width = 90;
            // 
            // colColour
            // 
            this.colColour.Caption = "Colour";
            this.colColour.FieldName = "Colour";
            this.colColour.Name = "colColour";
            this.colColour.OptionsColumn.AllowEdit = false;
            this.colColour.OptionsColumn.AllowFocus = false;
            this.colColour.OptionsColumn.ReadOnly = true;
            this.colColour.OptionsColumn.ShowInCustomizationForm = false;
            this.colColour.Width = 43;
            // 
            // radioGroup1
            // 
            this.radioGroup1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radioGroup1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01274ATTreePickerWorkspaceEditBindingSource, "ShareType", true));
            this.radioGroup1.EditValue = 0;
            this.radioGroup1.Location = new System.Drawing.Point(3, 22);
            this.radioGroup1.MenuManager = this.barManager1;
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroup1.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroup1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "No Sharing"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "All Staff"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Only Staff in the Following Groups...")});
            this.radioGroup1.Size = new System.Drawing.Size(518, 66);
            this.radioGroup1.TabIndex = 12;
            this.radioGroup1.SelectedIndexChanged += new System.EventHandler(this.radioGroup1_SelectedIndexChanged);
            // 
            // DefaultCentreYSpinEdit
            // 
            this.DefaultCentreYSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01274ATTreePickerWorkspaceEditBindingSource, "DefaultCentreY", true));
            this.DefaultCentreYSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.DefaultCentreYSpinEdit.Enabled = false;
            this.DefaultCentreYSpinEdit.Location = new System.Drawing.Point(371, 184);
            this.DefaultCentreYSpinEdit.MenuManager = this.barManager1;
            this.DefaultCentreYSpinEdit.Name = "DefaultCentreYSpinEdit";
            this.DefaultCentreYSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DefaultCentreYSpinEdit.Size = new System.Drawing.Size(157, 20);
            this.DefaultCentreYSpinEdit.StyleController = this.dataLayoutControl1;
            this.DefaultCentreYSpinEdit.TabIndex = 13;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Root";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5});
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup4.Size = new System.Drawing.Size(543, 629);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.AllowDrawBackground = false;
            this.layoutControlGroup5.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlItem6,
            this.layoutControlGroup8,
            this.splitterItem2,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem6});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "autoGeneratedGroup0";
            this.layoutControlGroup5.Size = new System.Drawing.Size(533, 619);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "General Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCreatedByName,
            this.ItemForWorkspaceName,
            this.ItemForWorkspaceRemarks,
            this.ItemForDefaultWorkspace,
            this.ItemForDefaultScale,
            this.ItemForDefaultCentreX,
            this.ItemForDefaultCentreY});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup6.Size = new System.Drawing.Size(533, 209);
            this.layoutControlGroup6.Text = "General Details";
            // 
            // ItemForCreatedByName
            // 
            this.ItemForCreatedByName.AllowHtmlStringInCaption = true;
            this.ItemForCreatedByName.Control = this.CreatedByNameTextEdit;
            this.ItemForCreatedByName.CustomizationFormText = "Created By:";
            this.ItemForCreatedByName.Location = new System.Drawing.Point(0, 0);
            this.ItemForCreatedByName.Name = "ItemForCreatedByName";
            this.ItemForCreatedByName.Size = new System.Drawing.Size(517, 24);
            this.ItemForCreatedByName.Text = "Created By:";
            this.ItemForCreatedByName.TextSize = new System.Drawing.Size(95, 13);
            // 
            // ItemForWorkspaceName
            // 
            this.ItemForWorkspaceName.AllowHtmlStringInCaption = true;
            this.ItemForWorkspaceName.Control = this.WorkspaceNameTextEdit;
            this.ItemForWorkspaceName.CustomizationFormText = "Name:";
            this.ItemForWorkspaceName.Location = new System.Drawing.Point(0, 24);
            this.ItemForWorkspaceName.Name = "ItemForWorkspaceName";
            this.ItemForWorkspaceName.Size = new System.Drawing.Size(517, 24);
            this.ItemForWorkspaceName.Text = "Name:";
            this.ItemForWorkspaceName.TextSize = new System.Drawing.Size(95, 13);
            // 
            // ItemForWorkspaceRemarks
            // 
            this.ItemForWorkspaceRemarks.AllowHtmlStringInCaption = true;
            this.ItemForWorkspaceRemarks.Control = this.WorkspaceRemarksMemoEdit;
            this.ItemForWorkspaceRemarks.CustomizationFormText = "Remarks:";
            this.ItemForWorkspaceRemarks.Location = new System.Drawing.Point(0, 48);
            this.ItemForWorkspaceRemarks.Name = "ItemForWorkspaceRemarks";
            this.ItemForWorkspaceRemarks.Size = new System.Drawing.Size(517, 52);
            this.ItemForWorkspaceRemarks.Text = "Remarks:";
            this.ItemForWorkspaceRemarks.TextSize = new System.Drawing.Size(95, 13);
            // 
            // ItemForDefaultWorkspace
            // 
            this.ItemForDefaultWorkspace.AllowHtmlStringInCaption = true;
            this.ItemForDefaultWorkspace.Control = this.DefaultWorkspaceCheckEdit;
            this.ItemForDefaultWorkspace.CustomizationFormText = "Default Workspace:";
            this.ItemForDefaultWorkspace.Location = new System.Drawing.Point(0, 100);
            this.ItemForDefaultWorkspace.Name = "ItemForDefaultWorkspace";
            this.ItemForDefaultWorkspace.Size = new System.Drawing.Size(517, 23);
            this.ItemForDefaultWorkspace.Text = "Default Workspace:";
            this.ItemForDefaultWorkspace.TextSize = new System.Drawing.Size(95, 13);
            // 
            // ItemForDefaultScale
            // 
            this.ItemForDefaultScale.AllowHtmlStringInCaption = true;
            this.ItemForDefaultScale.Control = this.DefaultScalePopupContainerEdit;
            this.ItemForDefaultScale.CustomizationFormText = "Default Scale:";
            this.ItemForDefaultScale.Location = new System.Drawing.Point(0, 123);
            this.ItemForDefaultScale.Name = "ItemForDefaultScale";
            this.ItemForDefaultScale.Size = new System.Drawing.Size(517, 24);
            this.ItemForDefaultScale.Text = "Default Scale:";
            this.ItemForDefaultScale.TextSize = new System.Drawing.Size(95, 13);
            // 
            // ItemForDefaultCentreX
            // 
            this.ItemForDefaultCentreX.AllowHtmlStringInCaption = true;
            this.ItemForDefaultCentreX.Control = this.DefaultCentreXSpinEdit;
            this.ItemForDefaultCentreX.CustomizationFormText = "Default Centre X:";
            this.ItemForDefaultCentreX.Location = new System.Drawing.Point(0, 147);
            this.ItemForDefaultCentreX.Name = "ItemForDefaultCentreX";
            this.ItemForDefaultCentreX.Size = new System.Drawing.Size(258, 24);
            this.ItemForDefaultCentreX.Text = "Default Centre X:";
            this.ItemForDefaultCentreX.TextSize = new System.Drawing.Size(95, 13);
            // 
            // ItemForDefaultCentreY
            // 
            this.ItemForDefaultCentreY.AllowHtmlStringInCaption = true;
            this.ItemForDefaultCentreY.Control = this.DefaultCentreYSpinEdit;
            this.ItemForDefaultCentreY.CustomizationFormText = "Default Centre Y:";
            this.ItemForDefaultCentreY.Location = new System.Drawing.Point(258, 147);
            this.ItemForDefaultCentreY.Name = "ItemForDefaultCentreY";
            this.ItemForDefaultCentreY.Size = new System.Drawing.Size(259, 24);
            this.ItemForDefaultCentreY.Text = "Default Centre Y:";
            this.ItemForDefaultCentreY.TextSize = new System.Drawing.Size(95, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.groupControl1;
            this.layoutControlItem6.CustomizationFormText = "Sharing Panel:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 209);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(533, 210);
            this.layoutControlItem6.Text = "Sharing Panel:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Loaded Map Layers";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 425);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup8.Size = new System.Drawing.Size(533, 168);
            this.layoutControlGroup8.Text = "Loaded Map Layers";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.gridSplitContainer2;
            this.layoutControlItem7.CustomizationFormText = "Layer Grid:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(517, 130);
            this.layoutControlItem7.Text = "Layer Grid:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(0, 419);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(533, 6);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 593);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(140, 26);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnOK;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(140, 593);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(126, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnCancel;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(266, 593);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(125, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(391, 593);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(142, 26);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp01254_AT_Tree_Picker_scale_listTableAdapter
            // 
            this.sp01254_AT_Tree_Picker_scale_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp01266_AT_Tree_Picker_Workspace_Share_groupsTableAdapter
            // 
            this.sp01266_AT_Tree_Picker_Workspace_Share_groupsTableAdapter.ClearBeforeFill = true;
            // 
            // dxValidationProvider1
            // 
            this.dxValidationProvider1.ValidationMode = DevExpress.XtraEditors.DXErrorProvider.ValidationMode.Auto;
            // 
            // sp01267_AT_Tree_Picker_Workspace_layers_listTableAdapter
            // 
            this.sp01267_AT_Tree_Picker_Workspace_layers_listTableAdapter.ClearBeforeFill = true;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 25;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // bbiProperties
            // 
            this.bbiProperties.Caption = "Properties";
            this.bbiProperties.Id = 26;
            this.bbiProperties.Name = "bbiProperties";
            // 
            // popupMenu_LayerManager
            // 
            this.popupMenu_LayerManager.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddLayer),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteLayer),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLayerManagerProperties, true)});
            this.popupMenu_LayerManager.Manager = this.barManager1;
            this.popupMenu_LayerManager.MenuCaption = "Layer Manager Menu";
            this.popupMenu_LayerManager.Name = "popupMenu_LayerManager";
            this.popupMenu_LayerManager.ShowCaption = true;
            // 
            // bbiAddLayer
            // 
            this.bbiAddLayer.Caption = "Add Layer";
            this.bbiAddLayer.Id = 28;
            this.bbiAddLayer.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddLayer.ImageOptions.Image")));
            this.bbiAddLayer.Name = "bbiAddLayer";
            this.bbiAddLayer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddLayer_ItemClick);
            // 
            // bbiDeleteLayer
            // 
            this.bbiDeleteLayer.Caption = "Remove Layer";
            this.bbiDeleteLayer.Id = 29;
            this.bbiDeleteLayer.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDeleteLayer.ImageOptions.Image")));
            this.bbiDeleteLayer.Name = "bbiDeleteLayer";
            this.bbiDeleteLayer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeleteLayer_ItemClick);
            // 
            // bbiLayerManagerProperties
            // 
            this.bbiLayerManagerProperties.Caption = "Properties";
            this.bbiLayerManagerProperties.Id = 27;
            this.bbiLayerManagerProperties.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLayerManagerProperties.ImageOptions.Image")));
            this.bbiLayerManagerProperties.Name = "bbiLayerManagerProperties";
            this.bbiLayerManagerProperties.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayerManagerProperties_ItemClick);
            // 
            // sp01274_AT_Tree_Picker_Workspace_EditTableAdapter
            // 
            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // frm_AT_Mapping_Workspace_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(543, 629);
            this.ControlBox = false;
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_AT_Mapping_Workspace_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Workspace";
            this.Load += new System.EventHandler(this.frm_AT_Mapping_Workspace_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).EndInit();
            this.popupContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.seUserDefinedScale.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01254ATTreePickerscalelistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01274ATTreePickerWorkspaceEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspaceNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspaceRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultWorkspaceCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultScalePopupContainerEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01267ATTreePickerWorkspacelayerslistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultCentreXSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01266ATTreePickerWorkspaceSharegroupsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefaultCentreYSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkspaceName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkspaceRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultWorkspace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultCentreX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDefaultCentreY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_LayerManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DataSet_AT_TreePicker dataSet_AT_TreePicker;
        private System.Windows.Forms.BindingSource sp01254ATTreePickerscalelistBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01254_AT_Tree_Picker_scale_listTableAdapter sp01254_AT_Tree_Picker_scale_listTableAdapter;
        private System.Windows.Forms.BindingSource sp01266ATTreePickerWorkspaceSharegroupsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colColour;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01266_AT_Tree_Picker_Workspace_Share_groupsTableAdapter sp01266_AT_Tree_Picker_Workspace_Share_groupsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private System.Windows.Forms.BindingSource sp01267ATTreePickerWorkspacelayerslistBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkspaceID;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerType;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerName;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerPath;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerHitable;
        private DevExpress.XtraGrid.Columns.GridColumn colPreserveDefaultStyling;
        private DevExpress.XtraGrid.Columns.GridColumn colTransparency;
        private DevExpress.XtraGrid.Columns.GridColumn colLineColour;
        private DevExpress.XtraGrid.Columns.GridColumn colLineWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colGreyScale;
        private DevExpress.XtraGrid.Columns.GridColumn colUseThematicStyling;
        private DevExpress.XtraGrid.Columns.GridColumn colThematicStyleSet;
        private DevExpress.XtraGrid.Columns.GridColumn colPointSize;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01267_AT_Tree_Picker_Workspace_layers_listTableAdapter sp01267_AT_Tree_Picker_Workspace_layers_listTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiProperties;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem bbiLayerManagerProperties;
        private DevExpress.XtraBars.PopupMenu popupMenu_LayerManager;
        private DevExpress.XtraBars.BarButtonItem bbiAddLayer;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteLayer;
        private System.Windows.Forms.BindingSource sp01274ATTreePickerWorkspaceEditBindingSource;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter sp01274_AT_Tree_Picker_Workspace_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit CreatedByNameTextEdit;
        private DevExpress.XtraEditors.TextEdit WorkspaceNameTextEdit;
        private DevExpress.XtraEditors.MemoEdit WorkspaceRemarksMemoEdit;
        private DevExpress.XtraEditors.CheckEdit DefaultWorkspaceCheckEdit;
        private DevExpress.XtraEditors.PopupContainerEdit DefaultScalePopupContainerEdit;
        private DevExpress.XtraEditors.SpinEdit DefaultCentreXSpinEdit;
        private DevExpress.XtraEditors.SpinEdit DefaultCentreYSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkspaceName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkspaceRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultWorkspace;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultScale;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultCentreX;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDefaultCentreY;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl2;
        private DevExpress.XtraEditors.SimpleButton btnSetScale;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraEditors.SpinEdit seUserDefinedScale;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colScaleDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colScale;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
    }
}

namespace WoodPlan5
{
    partial class frm_AT_Incident_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Incident_Edit));
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling28 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling29 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject53 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject54 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject55 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject56 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject57 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject58 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject59 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject60 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject61 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject62 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject63 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject64 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject65 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject66 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject67 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject68 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject69 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject70 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject71 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject72 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition7 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject73 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject74 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject75 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject76 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01312ATTreePickerGazetteerSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_TreePicker = new WoodPlan5.DataSet_AT_TreePicker();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.checkButtonGazetteer2 = new DevExpress.XtraEditors.CheckButton();
            this.btnCopyToIncidentAddress = new DevExpress.XtraEditors.SimpleButton();
            this.dropDownButtonShowMap = new DevExpress.XtraEditors.DropDownButton();
            this.pmGazetteerSearchRadius = new DevExpress.XtraBars.PopupMenu(this.components);
            this.beiGazetteerSearchRadius = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.btnCopyToReportedBy = new DevExpress.XtraEditors.SimpleButton();
            this.checkButtonGazetteer = new DevExpress.XtraEditors.CheckButton();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.sp01413ATIncidentEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_Incidents = new WoodPlan5.DataSet_AT_Incidents();
            this.intIncidentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strBriefDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intXTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.intYTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.strIncidentAddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strIncidentAddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strIncidentAddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strIncidentAddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strIncidentAddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strIncidentPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByTitleTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedBySurnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByForenameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByAddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByAddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByAddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByAddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByAddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByTelephone1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByTelephone2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByFaxTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByMobileTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strReportedByEmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strUser3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateClosedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.intDistrictIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TargetDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.intIncidentTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.strReferenceIDButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.intPersonRecordedByIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00226StaffListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisplayName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetworkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.intPersonResponsibleIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dtDateRecordedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.UserPickList1GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList3GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserPickList2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IncidentStatusGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForintIncidentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintDistrictID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintY = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForintIncidentTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReferenceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrBriefDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintPersonRecordedByID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForintPersonResponsibleID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFordtDateRecorded = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrReportedByTitle = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedBySurname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByTelephone1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByTelephone2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByFax = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByMobile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrReportedByForename = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrIncidentAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrIncidentAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrIncidentAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrIncidentAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrIncidentAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrIncidentPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrUser1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForstrUser3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserPickList3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDateClosed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTargetDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIncidentStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp01413_AT_Incident_EditTableAdapter = new WoodPlan5.DataSet_AT_IncidentsTableAdapters.sp01413_AT_Incident_EditTableAdapter();
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter();
            this.sp00226_Staff_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelGazetteer = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.buttonEditGazetteerFindValue = new DevExpress.XtraEditors.ButtonEdit();
            this.lookUpEditGazetteerSearchType = new DevExpress.XtraEditors.LookUpEdit();
            this.sp01311ATTreePickerGazetteerSearchTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupGazetteerMatchPattern = new DevExpress.XtraEditors.RadioGroup();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter();
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter();
            this.pmGazetteer = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiGazetteerTransferAddressToIncident = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGazetteerTransferToReportedBy = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGazetteerClearResults = new DevExpress.XtraBars.BarButtonItem();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01312ATTreePickerGazetteerSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmGazetteerSearchRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01413ATIncidentEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Incidents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIncidentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strBriefDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intXTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentAddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentAddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentAddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentAddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentAddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByTitleTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedBySurnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByForenameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByAddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByAddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByAddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByAddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByAddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByTelephone1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByTelephone2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByFaxTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByMobileTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByEmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateClosedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateClosedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intDistrictIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIncidentTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReferenceIDButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPersonRecordedByIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPersonResponsibleIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateRecordedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateRecordedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList1GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList3GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentStatusGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintIncidentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintDistrictID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintIncidentTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReferenceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrBriefDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPersonRecordedByID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPersonResponsibleID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtDateRecorded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedBySurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByTelephone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByTelephone2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByForename)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateClosed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanelGazetteer.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditGazetteerFindValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditGazetteerSearchType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01311ATTreePickerGazetteerSearchTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGazetteerMatchPattern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmGazetteer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1168, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 718);
            this.barDockControlBottom.Size = new System.Drawing.Size(1168, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1168, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 692);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiGazetteerTransferAddressToIncident,
            this.bbiGazetteerClearResults,
            this.beiGazetteerSearchRadius,
            this.bbiGazetteerTransferToReportedBy});
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1});
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42});
            this.gridView7.GridControl = this.gridControl3;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn36, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn35, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.DoubleClick += new System.EventHandler(this.gridView7_DoubleClick);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Locality ID";
            this.gridColumn34.FieldName = "RecordID";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Locality Name";
            this.gridColumn35.FieldName = "AddressLine1";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 1;
            this.gridColumn35.Width = 191;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "District Name";
            this.gridColumn36.FieldName = "AddressLine2";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 0;
            this.gridColumn36.Width = 162;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Unused 1";
            this.gridColumn37.FieldName = "AddressLine3";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Width = 145;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Unused 2";
            this.gridColumn38.FieldName = "AddressLine4";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Width = 145;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Unused 3";
            this.gridColumn39.FieldName = "AddressLine5";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 145;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Unused 4";
            this.gridColumn40.FieldName = "Postcode";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Width = 145;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = " X Coordinate";
            this.gridColumn41.FieldName = "XCoordinate";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Y Coordinate";
            this.gridColumn42.FieldName = "YCoordinate";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp01312ATTreePickerGazetteerSearchBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            gridLevelNode1.LevelTemplate = this.gridView7;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl3.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView8;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(397, 532);
            this.gridControl3.TabIndex = 6;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8,
            this.gridView7});
            // 
            // sp01312ATTreePickerGazetteerSearchBindingSource
            // 
            this.sp01312ATTreePickerGazetteerSearchBindingSource.DataMember = "sp01312_AT_Tree_Picker_Gazetteer_Search";
            this.sp01312ATTreePickerGazetteerSearchBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // dataSet_AT_TreePicker
            // 
            this.dataSet_AT_TreePicker.DataSetName = "DataSet_AT_TreePicker";
            this.dataSet_AT_TreePicker.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRecordID,
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colPostcode1,
            this.colXCoordinate1,
            this.colYCoordinate1});
            this.gridView8.GridControl = this.gridControl3;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAddressLine1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAddressLine2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.DoubleClick += new System.EventHandler(this.gridView8_DoubleClick);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colRecordID
            // 
            this.colRecordID.Caption = "Record ID";
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 0;
            this.colAddressLine1.Width = 144;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Visible = true;
            this.colAddressLine2.VisibleIndex = 1;
            this.colAddressLine2.Width = 145;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Visible = true;
            this.colAddressLine3.VisibleIndex = 2;
            this.colAddressLine3.Width = 145;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Visible = true;
            this.colAddressLine4.VisibleIndex = 3;
            this.colAddressLine4.Width = 145;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Visible = true;
            this.colAddressLine5.VisibleIndex = 4;
            this.colAddressLine5.Width = 145;
            // 
            // colPostcode1
            // 
            this.colPostcode1.Caption = "Postcode";
            this.colPostcode1.FieldName = "Postcode";
            this.colPostcode1.Name = "colPostcode1";
            this.colPostcode1.OptionsColumn.AllowEdit = false;
            this.colPostcode1.OptionsColumn.AllowFocus = false;
            this.colPostcode1.OptionsColumn.ReadOnly = true;
            this.colPostcode1.Visible = true;
            this.colPostcode1.VisibleIndex = 5;
            this.colPostcode1.Width = 145;
            // 
            // colXCoordinate1
            // 
            this.colXCoordinate1.Caption = " X Coordinate";
            this.colXCoordinate1.FieldName = "XCoordinate";
            this.colXCoordinate1.Name = "colXCoordinate1";
            this.colXCoordinate1.OptionsColumn.AllowEdit = false;
            this.colXCoordinate1.OptionsColumn.AllowFocus = false;
            this.colXCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate1
            // 
            this.colYCoordinate1.Caption = "Y Coordinate";
            this.colYCoordinate1.FieldName = "YCoordinate";
            this.colYCoordinate1.Name = "colYCoordinate1";
            this.colYCoordinate1.OptionsColumn.AllowEdit = false;
            this.colYCoordinate1.OptionsColumn.AllowFocus = false;
            this.colYCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colItemID
            // 
            this.colItemID.Caption = "Item ID";
            this.colItemID.FieldName = "ItemID";
            this.colItemID.Name = "colItemID";
            this.colItemID.OptionsColumn.AllowEdit = false;
            this.colItemID.OptionsColumn.AllowFocus = false;
            this.colItemID.OptionsColumn.ReadOnly = true;
            this.colItemID.Width = 58;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.Width = 59;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Staff ID";
            this.gridColumn30.FieldName = "StaffID";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Width = 59;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Item ID";
            this.gridColumn11.FieldName = "ItemID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 58;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Item ID";
            this.gridColumn23.FieldName = "ItemID";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 58;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Item ID";
            this.gridColumn17.FieldName = "ItemID";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 58;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Item ID";
            this.gridColumn5.FieldName = "ItemID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 58;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 28;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem8.Text = "Save Button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiFormSave.SuperTip = superToolTip8;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem9.Text = "Cancel Button - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bbiFormCancel.SuperTip = superToolTip9;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem10.Text = "Form Mode - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.barStaticItemFormMode.SuperTip = superToolTip10;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(1168, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 718);
            this.barDockControl2.Size = new System.Drawing.Size(1168, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1168, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 692);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.checkButtonGazetteer2);
            this.dataLayoutControl1.Controls.Add(this.btnCopyToIncidentAddress);
            this.dataLayoutControl1.Controls.Add(this.dropDownButtonShowMap);
            this.dataLayoutControl1.Controls.Add(this.btnCopyToReportedBy);
            this.dataLayoutControl1.Controls.Add(this.checkButtonGazetteer);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.intIncidentIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strBriefDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intXTextEdit);
            this.dataLayoutControl1.Controls.Add(this.intYTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.strIncidentAddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strIncidentAddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strIncidentAddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strIncidentAddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strIncidentAddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strIncidentPostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByTitleTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedBySurnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByForenameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByAddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByAddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByAddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByAddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByAddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByPostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByTelephone1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByTelephone2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByFaxTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByMobileTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strReportedByEmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.strUser3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateClosedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.intDistrictIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TargetDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.intIncidentTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.strReferenceIDButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.intPersonRecordedByIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.intPersonResponsibleIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.dtDateRecordedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList1GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList3GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UserPickList2GridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.IncidentStatusGridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp01413ATIncidentEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintIncidentID,
            this.ItemForintDistrictID,
            this.ItemForintX,
            this.ItemForintY});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1159, 453, 450, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(761, 692);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // checkButtonGazetteer2
            // 
            this.checkButtonGazetteer2.Checked = true;
            this.checkButtonGazetteer2.Location = new System.Drawing.Point(634, 400);
            this.checkButtonGazetteer2.Name = "checkButtonGazetteer2";
            this.checkButtonGazetteer2.Size = new System.Drawing.Size(86, 22);
            this.checkButtonGazetteer2.StyleController = this.dataLayoutControl1;
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Address Search - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>toggle</b> the <b>Gazetteer Pane</b> on and off.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.checkButtonGazetteer2.SuperTip = superToolTip1;
            this.checkButtonGazetteer2.TabIndex = 53;
            this.checkButtonGazetteer2.Text = "Address Search";
            this.checkButtonGazetteer2.CheckedChanged += new System.EventHandler(this.checkButtonGazetteer2_CheckedChanged);
            // 
            // btnCopyToIncidentAddress
            // 
            this.btnCopyToIncidentAddress.Location = new System.Drawing.Point(495, 400);
            this.btnCopyToIncidentAddress.Name = "btnCopyToIncidentAddress";
            this.btnCopyToIncidentAddress.Size = new System.Drawing.Size(135, 22);
            this.btnCopyToIncidentAddress.StyleController = this.dataLayoutControl1;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Copy To Incident Address Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to Copy the details from the Reported By Address to the Incident Address" +
    ".";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.btnCopyToIncidentAddress.SuperTip = superToolTip2;
            this.btnCopyToIncidentAddress.TabIndex = 52;
            this.btnCopyToIncidentAddress.Text = "Copy To Incident Address";
            this.btnCopyToIncidentAddress.Click += new System.EventHandler(this.btnCopyToIncidentAddress_Click);
            // 
            // dropDownButtonShowMap
            // 
            this.dropDownButtonShowMap.DropDownControl = this.pmGazetteerSearchRadius;
            this.dropDownButtonShowMap.Image = ((System.Drawing.Image)(resources.GetObject("dropDownButtonShowMap.Image")));
            this.dropDownButtonShowMap.Location = new System.Drawing.Point(636, 12);
            this.dropDownButtonShowMap.MenuManager = this.barManager1;
            this.dropDownButtonShowMap.Name = "dropDownButtonShowMap";
            this.dropDownButtonShowMap.Size = new System.Drawing.Size(96, 22);
            this.dropDownButtonShowMap.StyleController = this.dataLayoutControl1;
            this.dropDownButtonShowMap.TabIndex = 51;
            this.dropDownButtonShowMap.Text = "Show Map";
            this.dropDownButtonShowMap.Click += new System.EventHandler(this.dropDownButtonShowMap_Click);
            // 
            // pmGazetteerSearchRadius
            // 
            this.pmGazetteerSearchRadius.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiGazetteerSearchRadius, "", false, true, true, 124)});
            this.pmGazetteerSearchRadius.Manager = this.barManager1;
            this.pmGazetteerSearchRadius.Name = "pmGazetteerSearchRadius";
            // 
            // beiGazetteerSearchRadius
            // 
            this.beiGazetteerSearchRadius.Caption = "Show Map Objects in Range:";
            this.beiGazetteerSearchRadius.Edit = this.repositoryItemSpinEdit1;
            this.beiGazetteerSearchRadius.Id = 28;
            this.beiGazetteerSearchRadius.Name = "beiGazetteerSearchRadius";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Show Map Objects in Range - Information";
            toolTipItem3.LeftIndent = 6;
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.beiGazetteerSearchRadius.SuperTip = superToolTip3;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.Mask.EditMask = "#######0.00 Metres";
            this.repositoryItemSpinEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            131072});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // btnCopyToReportedBy
            // 
            this.btnCopyToReportedBy.Location = new System.Drawing.Point(485, 318);
            this.btnCopyToReportedBy.Name = "btnCopyToReportedBy";
            this.btnCopyToReportedBy.Size = new System.Drawing.Size(127, 22);
            this.btnCopyToReportedBy.StyleController = this.dataLayoutControl1;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Copy To Reported By Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to Copy the details from the Incident Address to the Reported By Address" +
    ".";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.btnCopyToReportedBy.SuperTip = superToolTip4;
            this.btnCopyToReportedBy.TabIndex = 48;
            this.btnCopyToReportedBy.Text = "Copy To Reported By";
            this.btnCopyToReportedBy.Click += new System.EventHandler(this.btnCopyToReportedBy_Click);
            // 
            // checkButtonGazetteer
            // 
            this.checkButtonGazetteer.Checked = true;
            this.checkButtonGazetteer.Location = new System.Drawing.Point(616, 318);
            this.checkButtonGazetteer.Name = "checkButtonGazetteer";
            this.checkButtonGazetteer.Size = new System.Drawing.Size(104, 22);
            this.checkButtonGazetteer.StyleController = this.dataLayoutControl1;
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem5.Text = "Address Search Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>toggle</b> the <b>Gazetteer Pane</b> on and off.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.checkButtonGazetteer.SuperTip = superToolTip5;
            this.checkButtonGazetteer.TabIndex = 47;
            this.checkButtonGazetteer.Text = "Address Search";
            this.checkButtonGazetteer.CheckedChanged += new System.EventHandler(this.checkButtonGazetteer_CheckedChanged);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp01413ATIncidentEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(118, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 4;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // sp01413ATIncidentEditBindingSource
            // 
            this.sp01413ATIncidentEditBindingSource.DataMember = "sp01413_AT_Incident_Edit";
            this.sp01413ATIncidentEditBindingSource.DataSource = this.dataSet_AT_Incidents;
            // 
            // dataSet_AT_Incidents
            // 
            this.dataSet_AT_Incidents.DataSetName = "DataSet_AT_Incidents";
            this.dataSet_AT_Incidents.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // intIncidentIDTextEdit
            // 
            this.intIncidentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "intIncidentID", true));
            this.intIncidentIDTextEdit.Location = new System.Drawing.Point(162, 35);
            this.intIncidentIDTextEdit.MenuManager = this.barManager1;
            this.intIncidentIDTextEdit.Name = "intIncidentIDTextEdit";
            this.intIncidentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intIncidentIDTextEdit, true);
            this.intIncidentIDTextEdit.Size = new System.Drawing.Size(437, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intIncidentIDTextEdit, optionsSpelling1);
            this.intIncidentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intIncidentIDTextEdit.TabIndex = 5;
            // 
            // strBriefDescriptionTextEdit
            // 
            this.strBriefDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strBriefDescription", true));
            this.strBriefDescriptionTextEdit.Location = new System.Drawing.Point(117, 88);
            this.strBriefDescriptionTextEdit.MenuManager = this.barManager1;
            this.strBriefDescriptionTextEdit.Name = "strBriefDescriptionTextEdit";
            this.strBriefDescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strBriefDescriptionTextEdit, true);
            this.strBriefDescriptionTextEdit.Size = new System.Drawing.Size(615, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strBriefDescriptionTextEdit, optionsSpelling2);
            this.strBriefDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.strBriefDescriptionTextEdit.TabIndex = 8;
            // 
            // intXTextEdit
            // 
            this.intXTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "intX", true));
            this.intXTextEdit.Location = new System.Drawing.Point(174, 458);
            this.intXTextEdit.MenuManager = this.barManager1;
            this.intXTextEdit.Name = "intXTextEdit";
            this.intXTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intXTextEdit, true);
            this.intXTextEdit.Size = new System.Drawing.Size(314, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intXTextEdit, optionsSpelling3);
            this.intXTextEdit.StyleController = this.dataLayoutControl1;
            this.intXTextEdit.TabIndex = 19;
            // 
            // intYTextEdit
            // 
            this.intYTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "intY", true));
            this.intYTextEdit.Location = new System.Drawing.Point(174, 458);
            this.intYTextEdit.MenuManager = this.barManager1;
            this.intYTextEdit.Name = "intYTextEdit";
            this.intYTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intYTextEdit, true);
            this.intYTextEdit.Size = new System.Drawing.Size(785, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intYTextEdit, optionsSpelling4);
            this.intYTextEdit.StyleController = this.dataLayoutControl1;
            this.intYTextEdit.TabIndex = 20;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strRemarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(24, 318);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(696, 378);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling5);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 12;
            // 
            // strIncidentAddressLine1TextEdit
            // 
            this.strIncidentAddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strIncidentAddressLine1", true));
            this.strIncidentAddressLine1TextEdit.Location = new System.Drawing.Point(129, 344);
            this.strIncidentAddressLine1TextEdit.MenuManager = this.barManager1;
            this.strIncidentAddressLine1TextEdit.Name = "strIncidentAddressLine1TextEdit";
            this.strIncidentAddressLine1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strIncidentAddressLine1TextEdit, true);
            this.strIncidentAddressLine1TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strIncidentAddressLine1TextEdit, optionsSpelling6);
            this.strIncidentAddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.strIncidentAddressLine1TextEdit.TabIndex = 13;
            // 
            // strIncidentAddressLine2TextEdit
            // 
            this.strIncidentAddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strIncidentAddressLine2", true));
            this.strIncidentAddressLine2TextEdit.Location = new System.Drawing.Point(129, 368);
            this.strIncidentAddressLine2TextEdit.MenuManager = this.barManager1;
            this.strIncidentAddressLine2TextEdit.Name = "strIncidentAddressLine2TextEdit";
            this.strIncidentAddressLine2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strIncidentAddressLine2TextEdit, true);
            this.strIncidentAddressLine2TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strIncidentAddressLine2TextEdit, optionsSpelling7);
            this.strIncidentAddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.strIncidentAddressLine2TextEdit.TabIndex = 14;
            // 
            // strIncidentAddressLine3TextEdit
            // 
            this.strIncidentAddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strIncidentAddressLine3", true));
            this.strIncidentAddressLine3TextEdit.Location = new System.Drawing.Point(129, 392);
            this.strIncidentAddressLine3TextEdit.MenuManager = this.barManager1;
            this.strIncidentAddressLine3TextEdit.Name = "strIncidentAddressLine3TextEdit";
            this.strIncidentAddressLine3TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strIncidentAddressLine3TextEdit, true);
            this.strIncidentAddressLine3TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strIncidentAddressLine3TextEdit, optionsSpelling8);
            this.strIncidentAddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.strIncidentAddressLine3TextEdit.TabIndex = 15;
            // 
            // strIncidentAddressLine4TextEdit
            // 
            this.strIncidentAddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strIncidentAddressLine4", true));
            this.strIncidentAddressLine4TextEdit.Location = new System.Drawing.Point(129, 416);
            this.strIncidentAddressLine4TextEdit.MenuManager = this.barManager1;
            this.strIncidentAddressLine4TextEdit.Name = "strIncidentAddressLine4TextEdit";
            this.strIncidentAddressLine4TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strIncidentAddressLine4TextEdit, true);
            this.strIncidentAddressLine4TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strIncidentAddressLine4TextEdit, optionsSpelling9);
            this.strIncidentAddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.strIncidentAddressLine4TextEdit.TabIndex = 16;
            // 
            // strIncidentAddressLine5TextEdit
            // 
            this.strIncidentAddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strIncidentAddressLine5", true));
            this.strIncidentAddressLine5TextEdit.Location = new System.Drawing.Point(129, 440);
            this.strIncidentAddressLine5TextEdit.MenuManager = this.barManager1;
            this.strIncidentAddressLine5TextEdit.Name = "strIncidentAddressLine5TextEdit";
            this.strIncidentAddressLine5TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strIncidentAddressLine5TextEdit, true);
            this.strIncidentAddressLine5TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strIncidentAddressLine5TextEdit, optionsSpelling10);
            this.strIncidentAddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.strIncidentAddressLine5TextEdit.TabIndex = 17;
            // 
            // strIncidentPostcodeTextEdit
            // 
            this.strIncidentPostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strIncidentPostcode", true));
            this.strIncidentPostcodeTextEdit.Location = new System.Drawing.Point(129, 464);
            this.strIncidentPostcodeTextEdit.MenuManager = this.barManager1;
            this.strIncidentPostcodeTextEdit.Name = "strIncidentPostcodeTextEdit";
            this.strIncidentPostcodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strIncidentPostcodeTextEdit, true);
            this.strIncidentPostcodeTextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strIncidentPostcodeTextEdit, optionsSpelling11);
            this.strIncidentPostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.strIncidentPostcodeTextEdit.TabIndex = 18;
            // 
            // strReportedByTitleTextEdit
            // 
            this.strReportedByTitleTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByTitle", true));
            this.strReportedByTitleTextEdit.Location = new System.Drawing.Point(129, 318);
            this.strReportedByTitleTextEdit.MenuManager = this.barManager1;
            this.strReportedByTitleTextEdit.Name = "strReportedByTitleTextEdit";
            this.strReportedByTitleTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByTitleTextEdit, true);
            this.strReportedByTitleTextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByTitleTextEdit, optionsSpelling12);
            this.strReportedByTitleTextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByTitleTextEdit.TabIndex = 21;
            // 
            // strReportedBySurnameTextEdit
            // 
            this.strReportedBySurnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedBySurname", true));
            this.strReportedBySurnameTextEdit.Location = new System.Drawing.Point(129, 366);
            this.strReportedBySurnameTextEdit.MenuManager = this.barManager1;
            this.strReportedBySurnameTextEdit.Name = "strReportedBySurnameTextEdit";
            this.strReportedBySurnameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedBySurnameTextEdit, true);
            this.strReportedBySurnameTextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedBySurnameTextEdit, optionsSpelling13);
            this.strReportedBySurnameTextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedBySurnameTextEdit.TabIndex = 22;
            // 
            // strReportedByForenameTextEdit
            // 
            this.strReportedByForenameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByForename", true));
            this.strReportedByForenameTextEdit.Location = new System.Drawing.Point(129, 342);
            this.strReportedByForenameTextEdit.MenuManager = this.barManager1;
            this.strReportedByForenameTextEdit.Name = "strReportedByForenameTextEdit";
            this.strReportedByForenameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByForenameTextEdit, true);
            this.strReportedByForenameTextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByForenameTextEdit, optionsSpelling14);
            this.strReportedByForenameTextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByForenameTextEdit.TabIndex = 23;
            // 
            // strReportedByAddressLine1TextEdit
            // 
            this.strReportedByAddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByAddressLine1", true));
            this.strReportedByAddressLine1TextEdit.Location = new System.Drawing.Point(129, 426);
            this.strReportedByAddressLine1TextEdit.MenuManager = this.barManager1;
            this.strReportedByAddressLine1TextEdit.Name = "strReportedByAddressLine1TextEdit";
            this.strReportedByAddressLine1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByAddressLine1TextEdit, true);
            this.strReportedByAddressLine1TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByAddressLine1TextEdit, optionsSpelling15);
            this.strReportedByAddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByAddressLine1TextEdit.TabIndex = 24;
            // 
            // strReportedByAddressLine2TextEdit
            // 
            this.strReportedByAddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByAddressLine2", true));
            this.strReportedByAddressLine2TextEdit.Location = new System.Drawing.Point(129, 450);
            this.strReportedByAddressLine2TextEdit.MenuManager = this.barManager1;
            this.strReportedByAddressLine2TextEdit.Name = "strReportedByAddressLine2TextEdit";
            this.strReportedByAddressLine2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByAddressLine2TextEdit, true);
            this.strReportedByAddressLine2TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByAddressLine2TextEdit, optionsSpelling16);
            this.strReportedByAddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByAddressLine2TextEdit.TabIndex = 25;
            // 
            // strReportedByAddressLine3TextEdit
            // 
            this.strReportedByAddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByAddressLine3", true));
            this.strReportedByAddressLine3TextEdit.Location = new System.Drawing.Point(129, 474);
            this.strReportedByAddressLine3TextEdit.MenuManager = this.barManager1;
            this.strReportedByAddressLine3TextEdit.Name = "strReportedByAddressLine3TextEdit";
            this.strReportedByAddressLine3TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByAddressLine3TextEdit, true);
            this.strReportedByAddressLine3TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByAddressLine3TextEdit, optionsSpelling17);
            this.strReportedByAddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByAddressLine3TextEdit.TabIndex = 26;
            // 
            // strReportedByAddressLine4TextEdit
            // 
            this.strReportedByAddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByAddressLine4", true));
            this.strReportedByAddressLine4TextEdit.Location = new System.Drawing.Point(129, 498);
            this.strReportedByAddressLine4TextEdit.MenuManager = this.barManager1;
            this.strReportedByAddressLine4TextEdit.Name = "strReportedByAddressLine4TextEdit";
            this.strReportedByAddressLine4TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByAddressLine4TextEdit, true);
            this.strReportedByAddressLine4TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByAddressLine4TextEdit, optionsSpelling18);
            this.strReportedByAddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByAddressLine4TextEdit.TabIndex = 27;
            // 
            // strReportedByAddressLine5TextEdit
            // 
            this.strReportedByAddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByAddressLine5", true));
            this.strReportedByAddressLine5TextEdit.Location = new System.Drawing.Point(129, 522);
            this.strReportedByAddressLine5TextEdit.MenuManager = this.barManager1;
            this.strReportedByAddressLine5TextEdit.Name = "strReportedByAddressLine5TextEdit";
            this.strReportedByAddressLine5TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByAddressLine5TextEdit, true);
            this.strReportedByAddressLine5TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByAddressLine5TextEdit, optionsSpelling19);
            this.strReportedByAddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByAddressLine5TextEdit.TabIndex = 28;
            // 
            // strReportedByPostcodeTextEdit
            // 
            this.strReportedByPostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByPostcode", true));
            this.strReportedByPostcodeTextEdit.Location = new System.Drawing.Point(129, 546);
            this.strReportedByPostcodeTextEdit.MenuManager = this.barManager1;
            this.strReportedByPostcodeTextEdit.Name = "strReportedByPostcodeTextEdit";
            this.strReportedByPostcodeTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByPostcodeTextEdit, true);
            this.strReportedByPostcodeTextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByPostcodeTextEdit, optionsSpelling20);
            this.strReportedByPostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByPostcodeTextEdit.TabIndex = 29;
            // 
            // strReportedByTelephone1TextEdit
            // 
            this.strReportedByTelephone1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByTelephone1", true));
            this.strReportedByTelephone1TextEdit.Location = new System.Drawing.Point(129, 580);
            this.strReportedByTelephone1TextEdit.MenuManager = this.barManager1;
            this.strReportedByTelephone1TextEdit.Name = "strReportedByTelephone1TextEdit";
            this.strReportedByTelephone1TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByTelephone1TextEdit, true);
            this.strReportedByTelephone1TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByTelephone1TextEdit, optionsSpelling21);
            this.strReportedByTelephone1TextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByTelephone1TextEdit.TabIndex = 30;
            // 
            // strReportedByTelephone2TextEdit
            // 
            this.strReportedByTelephone2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByTelephone2", true));
            this.strReportedByTelephone2TextEdit.Location = new System.Drawing.Point(129, 604);
            this.strReportedByTelephone2TextEdit.MenuManager = this.barManager1;
            this.strReportedByTelephone2TextEdit.Name = "strReportedByTelephone2TextEdit";
            this.strReportedByTelephone2TextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByTelephone2TextEdit, true);
            this.strReportedByTelephone2TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByTelephone2TextEdit, optionsSpelling22);
            this.strReportedByTelephone2TextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByTelephone2TextEdit.TabIndex = 31;
            // 
            // strReportedByFaxTextEdit
            // 
            this.strReportedByFaxTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByFax", true));
            this.strReportedByFaxTextEdit.Location = new System.Drawing.Point(129, 628);
            this.strReportedByFaxTextEdit.MenuManager = this.barManager1;
            this.strReportedByFaxTextEdit.Name = "strReportedByFaxTextEdit";
            this.strReportedByFaxTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByFaxTextEdit, true);
            this.strReportedByFaxTextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByFaxTextEdit, optionsSpelling23);
            this.strReportedByFaxTextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByFaxTextEdit.TabIndex = 32;
            // 
            // strReportedByMobileTextEdit
            // 
            this.strReportedByMobileTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByMobile", true));
            this.strReportedByMobileTextEdit.Location = new System.Drawing.Point(129, 652);
            this.strReportedByMobileTextEdit.MenuManager = this.barManager1;
            this.strReportedByMobileTextEdit.Name = "strReportedByMobileTextEdit";
            this.strReportedByMobileTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByMobileTextEdit, true);
            this.strReportedByMobileTextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByMobileTextEdit, optionsSpelling24);
            this.strReportedByMobileTextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByMobileTextEdit.TabIndex = 33;
            // 
            // strReportedByEmailTextEdit
            // 
            this.strReportedByEmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReportedByEmail", true));
            this.strReportedByEmailTextEdit.Location = new System.Drawing.Point(129, 676);
            this.strReportedByEmailTextEdit.MenuManager = this.barManager1;
            this.strReportedByEmailTextEdit.Name = "strReportedByEmailTextEdit";
            this.strReportedByEmailTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strReportedByEmailTextEdit, true);
            this.strReportedByEmailTextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strReportedByEmailTextEdit, optionsSpelling25);
            this.strReportedByEmailTextEdit.StyleController = this.dataLayoutControl1;
            this.strReportedByEmailTextEdit.TabIndex = 34;
            // 
            // strUser1TextEdit
            // 
            this.strUser1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strUser1", true));
            this.strUser1TextEdit.Location = new System.Drawing.Point(129, 318);
            this.strUser1TextEdit.MenuManager = this.barManager1;
            this.strUser1TextEdit.Name = "strUser1TextEdit";
            this.strUser1TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser1TextEdit, true);
            this.strUser1TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser1TextEdit, optionsSpelling26);
            this.strUser1TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser1TextEdit.TabIndex = 35;
            // 
            // strUser2TextEdit
            // 
            this.strUser2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strUser2", true));
            this.strUser2TextEdit.Location = new System.Drawing.Point(129, 342);
            this.strUser2TextEdit.MenuManager = this.barManager1;
            this.strUser2TextEdit.Name = "strUser2TextEdit";
            this.strUser2TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser2TextEdit, true);
            this.strUser2TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser2TextEdit, optionsSpelling27);
            this.strUser2TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser2TextEdit.TabIndex = 36;
            // 
            // strUser3TextEdit
            // 
            this.strUser3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strUser3", true));
            this.strUser3TextEdit.Location = new System.Drawing.Point(129, 366);
            this.strUser3TextEdit.MenuManager = this.barManager1;
            this.strUser3TextEdit.Name = "strUser3TextEdit";
            this.strUser3TextEdit.Properties.MaxLength = 10;
            this.scSpellChecker.SetShowSpellCheckMenu(this.strUser3TextEdit, true);
            this.strUser3TextEdit.Size = new System.Drawing.Size(591, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.strUser3TextEdit, optionsSpelling28);
            this.strUser3TextEdit.StyleController = this.dataLayoutControl1;
            this.strUser3TextEdit.TabIndex = 37;
            // 
            // DateClosedDateEdit
            // 
            this.DateClosedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "DateClosed", true));
            this.DateClosedDateEdit.EditValue = null;
            this.DateClosedDateEdit.Location = new System.Drawing.Point(117, 248);
            this.DateClosedDateEdit.MenuManager = this.barManager1;
            this.DateClosedDateEdit.Name = "DateClosedDateEdit";
            this.DateClosedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Clear Date", "clear", null, true)});
            this.DateClosedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateClosedDateEdit.Size = new System.Drawing.Size(615, 20);
            this.DateClosedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateClosedDateEdit.TabIndex = 38;
            this.DateClosedDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.DateClosedDateEdit_ButtonClick);
            // 
            // intDistrictIDTextEdit
            // 
            this.intDistrictIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "intDistrictID", true));
            this.intDistrictIDTextEdit.Location = new System.Drawing.Point(162, 660);
            this.intDistrictIDTextEdit.MenuManager = this.barManager1;
            this.intDistrictIDTextEdit.Name = "intDistrictIDTextEdit";
            this.intDistrictIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.intDistrictIDTextEdit, true);
            this.intDistrictIDTextEdit.Size = new System.Drawing.Size(792, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.intDistrictIDTextEdit, optionsSpelling29);
            this.intDistrictIDTextEdit.StyleController = this.dataLayoutControl1;
            this.intDistrictIDTextEdit.TabIndex = 39;
            // 
            // TargetDateDateEdit
            // 
            this.TargetDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "TargetDate", true));
            this.TargetDateDateEdit.EditValue = null;
            this.TargetDateDateEdit.Location = new System.Drawing.Point(117, 224);
            this.TargetDateDateEdit.MenuManager = this.barManager1;
            this.TargetDateDateEdit.Name = "TargetDateDateEdit";
            this.TargetDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Clear, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Clear Date", "clear", null, true)});
            this.TargetDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TargetDateDateEdit.Size = new System.Drawing.Size(615, 20);
            this.TargetDateDateEdit.StyleController = this.dataLayoutControl1;
            this.TargetDateDateEdit.TabIndex = 45;
            this.TargetDateDateEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TargetDateDateEdit_ButtonClick);
            // 
            // intIncidentTypeIDGridLookUpEdit
            // 
            this.intIncidentTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "intIncidentTypeID", true));
            this.intIncidentTypeIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intIncidentTypeIDGridLookUpEdit.Location = new System.Drawing.Point(117, 38);
            this.intIncidentTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intIncidentTypeIDGridLookUpEdit.Name = "intIncidentTypeIDGridLookUpEdit";
            this.intIncidentTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Edit Picklist", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Reload Picklist", "reload", null, true)});
            this.intIncidentTypeIDGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.intIncidentTypeIDGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.intIncidentTypeIDGridLookUpEdit.Properties.NullText = "";
            this.intIncidentTypeIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intIncidentTypeIDGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.intIncidentTypeIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.intIncidentTypeIDGridLookUpEdit.Size = new System.Drawing.Size(615, 22);
            this.intIncidentTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intIncidentTypeIDGridLookUpEdit.TabIndex = 6;
            this.intIncidentTypeIDGridLookUpEdit.TabStop = false;
            this.intIncidentTypeIDGridLookUpEdit.Tag = "131";
            // 
            // sp01372ATMultiplePicklistsWithBlanksBindingSource
            // 
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataMember = "sp01372_AT_Multiple_Picklists_With_Blanks";
            this.sp01372ATMultiplePicklistsWithBlanksBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colHeaderDescription,
            this.colHeaderID,
            this.colItemCode,
            this.colItemDescription,
            this.colItemID,
            this.colOrder1});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colItemID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsCustomization.AllowFilter = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowFilterEditor = false;
            this.gridLookUpEdit1View.OptionsFilter.AllowMRUFilterList = false;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Picklist Type";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.Width = 189;
            // 
            // colHeaderID
            // 
            this.colHeaderID.Caption = "Picklist ID";
            this.colHeaderID.FieldName = "HeaderID";
            this.colHeaderID.Name = "colHeaderID";
            this.colHeaderID.OptionsColumn.AllowEdit = false;
            this.colHeaderID.OptionsColumn.AllowFocus = false;
            this.colHeaderID.OptionsColumn.ReadOnly = true;
            this.colHeaderID.Width = 67;
            // 
            // colItemCode
            // 
            this.colItemCode.Caption = "Item ID";
            this.colItemCode.FieldName = "ItemCode";
            this.colItemCode.Name = "colItemCode";
            this.colItemCode.OptionsColumn.AllowEdit = false;
            this.colItemCode.OptionsColumn.AllowFocus = false;
            this.colItemCode.OptionsColumn.ReadOnly = true;
            this.colItemCode.Width = 58;
            // 
            // colItemDescription
            // 
            this.colItemDescription.Caption = "Description";
            this.colItemDescription.FieldName = "ItemDescription";
            this.colItemDescription.Name = "colItemDescription";
            this.colItemDescription.OptionsColumn.AllowEdit = false;
            this.colItemDescription.OptionsColumn.AllowFocus = false;
            this.colItemDescription.OptionsColumn.ReadOnly = true;
            this.colItemDescription.Visible = true;
            this.colItemDescription.VisibleIndex = 0;
            this.colItemDescription.Width = 264;
            // 
            // colOrder1
            // 
            this.colOrder1.Caption = "Order";
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.AllowFocus = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            this.colOrder1.Width = 65;
            // 
            // strReferenceIDButtonEdit
            // 
            this.strReferenceIDButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "strReferenceID", true));
            this.strReferenceIDButtonEdit.Location = new System.Drawing.Point(117, 64);
            this.strReferenceIDButtonEdit.MenuManager = this.barManager1;
            this.strReferenceIDButtonEdit.Name = "strReferenceIDButtonEdit";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem6.Text = "Sequence Button - Information\r\n";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem7.Text = "Number Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.strReferenceIDButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "Sequence", superToolTip6, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "Number", superToolTip7, true)});
            this.strReferenceIDButtonEdit.Properties.MaxLength = 30;
            this.strReferenceIDButtonEdit.Size = new System.Drawing.Size(615, 20);
            this.strReferenceIDButtonEdit.StyleController = this.dataLayoutControl1;
            this.strReferenceIDButtonEdit.TabIndex = 7;
            this.strReferenceIDButtonEdit.TabStop = false;
            this.strReferenceIDButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.strReferenceIDButtonEdit_ButtonClick);
            // 
            // intPersonRecordedByIDGridLookUpEdit
            // 
            this.intPersonRecordedByIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "intPersonRecordedByID", true));
            this.intPersonRecordedByIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intPersonRecordedByIDGridLookUpEdit.Location = new System.Drawing.Point(117, 148);
            this.intPersonRecordedByIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intPersonRecordedByIDGridLookUpEdit.Name = "intPersonRecordedByIDGridLookUpEdit";
            this.intPersonRecordedByIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Edit Picklist", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Reload Picklist", "reload", null, true)});
            this.intPersonRecordedByIDGridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.intPersonRecordedByIDGridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.intPersonRecordedByIDGridLookUpEdit.Properties.NullText = "";
            this.intPersonRecordedByIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.intPersonRecordedByIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intPersonRecordedByIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.intPersonRecordedByIDGridLookUpEdit.Properties.View = this.gridView1;
            this.intPersonRecordedByIDGridLookUpEdit.Size = new System.Drawing.Size(615, 22);
            this.intPersonRecordedByIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intPersonRecordedByIDGridLookUpEdit.TabIndex = 9;
            this.intPersonRecordedByIDGridLookUpEdit.TabStop = false;
            this.intPersonRecordedByIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intPersonRecordedByIDGridLookUpEdit_ButtonClick);
            // 
            // sp00226StaffListWithBlankBindingSource
            // 
            this.sp00226StaffListWithBlankBindingSource.DataMember = "sp00226_Staff_List_With_Blank";
            this.sp00226StaffListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActive,
            this.colDisplayName,
            this.colEmail,
            this.colForename,
            this.colNetworkID,
            this.colStaffID,
            this.colStaffName,
            this.colSurname,
            this.colUserType});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colStaffID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDisplayName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 51;
            // 
            // colDisplayName
            // 
            this.colDisplayName.Caption = "Surname: Forename";
            this.colDisplayName.FieldName = "DisplayName";
            this.colDisplayName.Name = "colDisplayName";
            this.colDisplayName.OptionsColumn.AllowEdit = false;
            this.colDisplayName.OptionsColumn.AllowFocus = false;
            this.colDisplayName.OptionsColumn.ReadOnly = true;
            this.colDisplayName.Visible = true;
            this.colDisplayName.VisibleIndex = 0;
            this.colDisplayName.Width = 225;
            // 
            // colEmail
            // 
            this.colEmail.Caption = "Email";
            this.colEmail.FieldName = "Email";
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.AllowEdit = false;
            this.colEmail.OptionsColumn.AllowFocus = false;
            this.colEmail.OptionsColumn.ReadOnly = true;
            this.colEmail.Width = 192;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Width = 178;
            // 
            // colNetworkID
            // 
            this.colNetworkID.Caption = "Network ID";
            this.colNetworkID.FieldName = "NetworkID";
            this.colNetworkID.Name = "colNetworkID";
            this.colNetworkID.OptionsColumn.AllowEdit = false;
            this.colNetworkID.OptionsColumn.AllowFocus = false;
            this.colNetworkID.OptionsColumn.ReadOnly = true;
            this.colNetworkID.Width = 180;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Width = 231;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Width = 191;
            // 
            // colUserType
            // 
            this.colUserType.Caption = "User Type";
            this.colUserType.FieldName = "UserType";
            this.colUserType.Name = "colUserType";
            this.colUserType.OptionsColumn.AllowEdit = false;
            this.colUserType.OptionsColumn.AllowFocus = false;
            this.colUserType.OptionsColumn.ReadOnly = true;
            this.colUserType.Width = 223;
            // 
            // intPersonResponsibleIDGridLookUpEdit
            // 
            this.intPersonResponsibleIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "intPersonResponsibleID", true));
            this.intPersonResponsibleIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.intPersonResponsibleIDGridLookUpEdit.Location = new System.Drawing.Point(117, 174);
            this.intPersonResponsibleIDGridLookUpEdit.MenuManager = this.barManager1;
            this.intPersonResponsibleIDGridLookUpEdit.Name = "intPersonResponsibleIDGridLookUpEdit";
            this.intPersonResponsibleIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Edit Picklist", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Reload Picklist", "reload", null, true)});
            this.intPersonResponsibleIDGridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.intPersonResponsibleIDGridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.intPersonResponsibleIDGridLookUpEdit.Properties.NullText = "";
            this.intPersonResponsibleIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.intPersonResponsibleIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.intPersonResponsibleIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.intPersonResponsibleIDGridLookUpEdit.Properties.View = this.gridView2;
            this.intPersonResponsibleIDGridLookUpEdit.Size = new System.Drawing.Size(615, 22);
            this.intPersonResponsibleIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.intPersonResponsibleIDGridLookUpEdit.TabIndex = 10;
            this.intPersonResponsibleIDGridLookUpEdit.TabStop = false;
            this.intPersonResponsibleIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intPersonResponsibleIDGridLookUpEdit_ButtonClick);
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn30;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn26, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Active";
            this.gridColumn25.ColumnEdit = this.repositoryItemCheckEdit2;
            this.gridColumn25.FieldName = "Active";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 1;
            this.gridColumn25.Width = 51;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Surname: Forename";
            this.gridColumn26.FieldName = "DisplayName";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 0;
            this.gridColumn26.Width = 225;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Email";
            this.gridColumn27.FieldName = "Email";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Width = 192;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Forename";
            this.gridColumn28.FieldName = "Forename";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Width = 178;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Network ID";
            this.gridColumn29.FieldName = "NetworkID";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 180;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Staff Name";
            this.gridColumn31.FieldName = "StaffName";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Width = 231;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Surname";
            this.gridColumn32.FieldName = "Surname";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 191;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "User Type";
            this.gridColumn33.FieldName = "UserType";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 223;
            // 
            // dtDateRecordedDateEdit
            // 
            this.dtDateRecordedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "dtDateRecorded", true));
            this.dtDateRecordedDateEdit.EditValue = null;
            this.dtDateRecordedDateEdit.Location = new System.Drawing.Point(117, 200);
            this.dtDateRecordedDateEdit.MenuManager = this.barManager1;
            this.dtDateRecordedDateEdit.Name = "dtDateRecordedDateEdit";
            this.dtDateRecordedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtDateRecordedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtDateRecordedDateEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.dtDateRecordedDateEdit.Properties.Mask.EditMask = "";
            this.dtDateRecordedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dtDateRecordedDateEdit.Size = new System.Drawing.Size(615, 20);
            this.dtDateRecordedDateEdit.StyleController = this.dataLayoutControl1;
            this.dtDateRecordedDateEdit.TabIndex = 11;
            this.dtDateRecordedDateEdit.TabStop = false;
            this.dtDateRecordedDateEdit.EditValueChanged += new System.EventHandler(this.dtDateRecordedDateEdit_EditValueChanged);
            this.dtDateRecordedDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.dtDateRecordedDateEdit_Validating);
            // 
            // UserPickList1GridLookUpEdit
            // 
            this.UserPickList1GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "UserPickList1", true));
            this.UserPickList1GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList1GridLookUpEdit.Location = new System.Drawing.Point(129, 400);
            this.UserPickList1GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList1GridLookUpEdit.Name = "UserPickList1GridLookUpEdit";
            this.UserPickList1GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Edit Picklist", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Reload Picklist", "reload", null, true)});
            this.UserPickList1GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList1GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList1GridLookUpEdit.Properties.NullText = "";
            this.UserPickList1GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList1GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList1GridLookUpEdit.Properties.View = this.gridView3;
            this.UserPickList1GridLookUpEdit.Size = new System.Drawing.Size(591, 22);
            this.UserPickList1GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList1GridLookUpEdit.TabIndex = 41;
            this.UserPickList1GridLookUpEdit.TabStop = false;
            this.UserPickList1GridLookUpEdit.Tag = "154";
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.gridColumn11;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsCustomization.AllowFilter = false;
            this.gridView3.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowFilterEditor = false;
            this.gridView3.OptionsFilter.AllowMRUFilterList = false;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Picklist Type";
            this.gridColumn7.FieldName = "HeaderDescription";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 189;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Picklist ID";
            this.gridColumn8.FieldName = "HeaderID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 67;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Item ID";
            this.gridColumn9.FieldName = "ItemCode";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 58;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Description";
            this.gridColumn10.FieldName = "ItemDescription";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 0;
            this.gridColumn10.Width = 264;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "Order";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 65;
            // 
            // UserPickList3GridLookUpEdit
            // 
            this.UserPickList3GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "UserPickList3", true));
            this.UserPickList3GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList3GridLookUpEdit.Location = new System.Drawing.Point(129, 452);
            this.UserPickList3GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList3GridLookUpEdit.Name = "UserPickList3GridLookUpEdit";
            this.UserPickList3GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "Edit Picklist", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject53, serializableAppearanceObject54, serializableAppearanceObject55, serializableAppearanceObject56, "Reload Picklist", "reload", null, true)});
            this.UserPickList3GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList3GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList3GridLookUpEdit.Properties.NullText = "";
            this.UserPickList3GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList3GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList3GridLookUpEdit.Properties.View = this.gridView4;
            this.UserPickList3GridLookUpEdit.Size = new System.Drawing.Size(591, 22);
            this.UserPickList3GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList3GridLookUpEdit.TabIndex = 43;
            this.UserPickList3GridLookUpEdit.TabStop = false;
            this.UserPickList3GridLookUpEdit.Tag = "156";
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.gridColumn23;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView4.OptionsCustomization.AllowFilter = false;
            this.gridView4.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView4.OptionsFilter.AllowFilterEditor = false;
            this.gridView4.OptionsFilter.AllowMRUFilterList = false;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn24, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Picklist Type";
            this.gridColumn19.FieldName = "HeaderDescription";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 189;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Picklist ID";
            this.gridColumn20.FieldName = "HeaderID";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 67;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Item ID";
            this.gridColumn21.FieldName = "ItemCode";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 58;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Description";
            this.gridColumn22.FieldName = "ItemDescription";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 0;
            this.gridColumn22.Width = 264;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Order";
            this.gridColumn24.FieldName = "Order";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 65;
            // 
            // UserPickList2GridLookUpEdit
            // 
            this.UserPickList2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "UserPickList2", true));
            this.UserPickList2GridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UserPickList2GridLookUpEdit.Location = new System.Drawing.Point(129, 426);
            this.UserPickList2GridLookUpEdit.MenuManager = this.barManager1;
            this.UserPickList2GridLookUpEdit.Name = "UserPickList2GridLookUpEdit";
            this.UserPickList2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject57, serializableAppearanceObject58, serializableAppearanceObject59, serializableAppearanceObject60, "Edit Picklist", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject61, serializableAppearanceObject62, serializableAppearanceObject63, serializableAppearanceObject64, "Reload Picklist", "reload", null, true)});
            this.UserPickList2GridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.UserPickList2GridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.UserPickList2GridLookUpEdit.Properties.NullText = "";
            this.UserPickList2GridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserPickList2GridLookUpEdit.Properties.ValueMember = "ItemID";
            this.UserPickList2GridLookUpEdit.Properties.View = this.gridView5;
            this.UserPickList2GridLookUpEdit.Size = new System.Drawing.Size(591, 22);
            this.UserPickList2GridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserPickList2GridLookUpEdit.TabIndex = 42;
            this.UserPickList2GridLookUpEdit.TabStop = false;
            this.UserPickList2GridLookUpEdit.Tag = "155";
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.gridColumn17;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView5.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsCustomization.AllowFilter = false;
            this.gridView5.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowFilterEditor = false;
            this.gridView5.OptionsFilter.AllowMRUFilterList = false;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn18, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Picklist Type";
            this.gridColumn13.FieldName = "HeaderDescription";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 189;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Picklist ID";
            this.gridColumn14.FieldName = "HeaderID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Width = 67;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Item ID";
            this.gridColumn15.FieldName = "ItemCode";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 58;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Description";
            this.gridColumn16.FieldName = "ItemDescription";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 0;
            this.gridColumn16.Width = 264;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Order";
            this.gridColumn18.FieldName = "Order";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 65;
            // 
            // IncidentStatusGridLookUpEdit
            // 
            this.IncidentStatusGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01413ATIncidentEditBindingSource, "IncidentStatus", true));
            this.IncidentStatusGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.IncidentStatusGridLookUpEdit.Location = new System.Drawing.Point(117, 112);
            this.IncidentStatusGridLookUpEdit.MenuManager = this.barManager1;
            this.IncidentStatusGridLookUpEdit.Name = "IncidentStatusGridLookUpEdit";
            this.IncidentStatusGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Edit_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject65, serializableAppearanceObject66, serializableAppearanceObject67, serializableAppearanceObject68, "Edit Picklist", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, global::WoodPlan5.Properties.Resources.Refresh2_16x16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject69, serializableAppearanceObject70, serializableAppearanceObject71, serializableAppearanceObject72, "Reload Picklilst", "reload", null, true)});
            this.IncidentStatusGridLookUpEdit.Properties.DataSource = this.sp01372ATMultiplePicklistsWithBlanksBindingSource;
            this.IncidentStatusGridLookUpEdit.Properties.DisplayMember = "ItemDescription";
            this.IncidentStatusGridLookUpEdit.Properties.NullText = "";
            this.IncidentStatusGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.IncidentStatusGridLookUpEdit.Properties.ValueMember = "ItemID";
            this.IncidentStatusGridLookUpEdit.Properties.View = this.gridView6;
            this.IncidentStatusGridLookUpEdit.Size = new System.Drawing.Size(615, 22);
            this.IncidentStatusGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.IncidentStatusGridLookUpEdit.TabIndex = 44;
            this.IncidentStatusGridLookUpEdit.TabStop = false;
            this.IncidentStatusGridLookUpEdit.Tag = "157";
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition7.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition7.Appearance.Options.UseForeColor = true;
            styleFormatCondition7.ApplyToRow = true;
            styleFormatCondition7.Column = this.gridColumn5;
            styleFormatCondition7.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition7.Value1 = 0;
            this.gridView6.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition7});
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView6.OptionsCustomization.AllowFilter = false;
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Picklist Type";
            this.gridColumn1.FieldName = "HeaderDescription";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 189;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Picklist ID";
            this.gridColumn2.FieldName = "HeaderID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 67;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Item ID";
            this.gridColumn3.FieldName = "ItemCode";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 58;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Description";
            this.gridColumn4.FieldName = "ItemDescription";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 264;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 65;
            // 
            // ItemForintIncidentID
            // 
            this.ItemForintIncidentID.Control = this.intIncidentIDTextEdit;
            this.ItemForintIncidentID.CustomizationFormText = "Incident ID:";
            this.ItemForintIncidentID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintIncidentID.Name = "ItemForintIncidentID";
            this.ItemForintIncidentID.Size = new System.Drawing.Size(591, 24);
            this.ItemForintIncidentID.Text = "Incident ID:";
            this.ItemForintIncidentID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintDistrictID
            // 
            this.ItemForintDistrictID.Control = this.intDistrictIDTextEdit;
            this.ItemForintDistrictID.CustomizationFormText = "Location ID:";
            this.ItemForintDistrictID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintDistrictID.Name = "ItemForintDistrictID";
            this.ItemForintDistrictID.Size = new System.Drawing.Size(946, 24);
            this.ItemForintDistrictID.Text = "Location ID:";
            this.ItemForintDistrictID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintX
            // 
            this.ItemForintX.Control = this.intXTextEdit;
            this.ItemForintX.CustomizationFormText = "X Coordinate:";
            this.ItemForintX.Location = new System.Drawing.Point(0, 144);
            this.ItemForintX.Name = "ItemForintX";
            this.ItemForintX.Size = new System.Drawing.Size(468, 24);
            this.ItemForintX.Text = "X Coordinate:";
            this.ItemForintX.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForintY
            // 
            this.ItemForintY.Control = this.intYTextEdit;
            this.ItemForintY.CustomizationFormText = "Y Coordinate:";
            this.ItemForintY.Location = new System.Drawing.Point(0, 144);
            this.ItemForintY.Name = "ItemForintY";
            this.ItemForintY.Size = new System.Drawing.Size(939, 24);
            this.ItemForintY.Text = "Y Coordinate:";
            this.ItemForintY.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(744, 732);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(106, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForintIncidentTypeID,
            this.ItemForstrReferenceID,
            this.ItemForstrBriefDescription,
            this.ItemForintPersonRecordedByID,
            this.ItemForintPersonResponsibleID,
            this.ItemFordtDateRecorded,
            this.tabbedControlGroup1,
            this.emptySpaceItem4,
            this.ItemForDateClosed,
            this.ItemForTargetDate,
            this.emptySpaceItem5,
            this.emptySpaceItem8,
            this.ItemForIncidentStatus});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 26);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(724, 684);
            // 
            // ItemForintIncidentTypeID
            // 
            this.ItemForintIncidentTypeID.Control = this.intIncidentTypeIDGridLookUpEdit;
            this.ItemForintIncidentTypeID.CustomizationFormText = "Incident Type:";
            this.ItemForintIncidentTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForintIncidentTypeID.Name = "ItemForintIncidentTypeID";
            this.ItemForintIncidentTypeID.Size = new System.Drawing.Size(724, 26);
            this.ItemForintIncidentTypeID.Text = "Incident Type:";
            this.ItemForintIncidentTypeID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReferenceID
            // 
            this.ItemForstrReferenceID.Control = this.strReferenceIDButtonEdit;
            this.ItemForstrReferenceID.CustomizationFormText = "Reference Number:";
            this.ItemForstrReferenceID.Location = new System.Drawing.Point(0, 26);
            this.ItemForstrReferenceID.Name = "ItemForstrReferenceID";
            this.ItemForstrReferenceID.Size = new System.Drawing.Size(724, 24);
            this.ItemForstrReferenceID.Text = "Reference Number:";
            this.ItemForstrReferenceID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrBriefDescription
            // 
            this.ItemForstrBriefDescription.Control = this.strBriefDescriptionTextEdit;
            this.ItemForstrBriefDescription.CustomizationFormText = "Brief Description:";
            this.ItemForstrBriefDescription.Location = new System.Drawing.Point(0, 50);
            this.ItemForstrBriefDescription.Name = "ItemForstrBriefDescription";
            this.ItemForstrBriefDescription.Size = new System.Drawing.Size(724, 24);
            this.ItemForstrBriefDescription.Text = "Brief Description:";
            this.ItemForstrBriefDescription.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForintPersonRecordedByID
            // 
            this.ItemForintPersonRecordedByID.Control = this.intPersonRecordedByIDGridLookUpEdit;
            this.ItemForintPersonRecordedByID.CustomizationFormText = "Recorded By:";
            this.ItemForintPersonRecordedByID.Location = new System.Drawing.Point(0, 110);
            this.ItemForintPersonRecordedByID.Name = "ItemForintPersonRecordedByID";
            this.ItemForintPersonRecordedByID.Size = new System.Drawing.Size(724, 26);
            this.ItemForintPersonRecordedByID.Text = "Recorded By:";
            this.ItemForintPersonRecordedByID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForintPersonResponsibleID
            // 
            this.ItemForintPersonResponsibleID.Control = this.intPersonResponsibleIDGridLookUpEdit;
            this.ItemForintPersonResponsibleID.CustomizationFormText = "Person Responsible:";
            this.ItemForintPersonResponsibleID.Location = new System.Drawing.Point(0, 136);
            this.ItemForintPersonResponsibleID.Name = "ItemForintPersonResponsibleID";
            this.ItemForintPersonResponsibleID.Size = new System.Drawing.Size(724, 26);
            this.ItemForintPersonResponsibleID.Text = "Person Responsible:";
            this.ItemForintPersonResponsibleID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemFordtDateRecorded
            // 
            this.ItemFordtDateRecorded.Control = this.dtDateRecordedDateEdit;
            this.ItemFordtDateRecorded.CustomizationFormText = "Date\\Time Recorded:";
            this.ItemFordtDateRecorded.Location = new System.Drawing.Point(0, 162);
            this.ItemFordtDateRecorded.Name = "ItemFordtDateRecorded";
            this.ItemFordtDateRecorded.Size = new System.Drawing.Size(724, 24);
            this.ItemFordtDateRecorded.Text = "Date\\Time Recorded:";
            this.ItemFordtDateRecorded.TextSize = new System.Drawing.Size(102, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 244);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup6;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(724, 430);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup5,
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Reported By";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrReportedByTitle,
            this.ItemForstrReportedBySurname,
            this.ItemForstrReportedByAddressLine1,
            this.ItemForstrReportedByAddressLine2,
            this.ItemForstrReportedByAddressLine3,
            this.ItemForstrReportedByAddressLine4,
            this.ItemForstrReportedByAddressLine5,
            this.ItemForstrReportedByPostcode,
            this.ItemForstrReportedByTelephone1,
            this.ItemForstrReportedByTelephone2,
            this.ItemForstrReportedByFax,
            this.ItemForstrReportedByMobile,
            this.ItemForstrReportedByEmail,
            this.ItemForstrReportedByForename,
            this.emptySpaceItem9,
            this.emptySpaceItem10,
            this.emptySpaceItem12,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(700, 382);
            this.layoutControlGroup6.Text = "Reported By";
            // 
            // ItemForstrReportedByTitle
            // 
            this.ItemForstrReportedByTitle.Control = this.strReportedByTitleTextEdit;
            this.ItemForstrReportedByTitle.CustomizationFormText = "Reported By Title:";
            this.ItemForstrReportedByTitle.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrReportedByTitle.Name = "ItemForstrReportedByTitle";
            this.ItemForstrReportedByTitle.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByTitle.Text = "Title:";
            this.ItemForstrReportedByTitle.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedBySurname
            // 
            this.ItemForstrReportedBySurname.Control = this.strReportedBySurnameTextEdit;
            this.ItemForstrReportedBySurname.CustomizationFormText = "Reported By Surname:";
            this.ItemForstrReportedBySurname.Location = new System.Drawing.Point(0, 48);
            this.ItemForstrReportedBySurname.Name = "ItemForstrReportedBySurname";
            this.ItemForstrReportedBySurname.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedBySurname.Text = "Surname:";
            this.ItemForstrReportedBySurname.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByAddressLine1
            // 
            this.ItemForstrReportedByAddressLine1.Control = this.strReportedByAddressLine1TextEdit;
            this.ItemForstrReportedByAddressLine1.CustomizationFormText = "Reported By Address Line 1:";
            this.ItemForstrReportedByAddressLine1.Location = new System.Drawing.Point(0, 108);
            this.ItemForstrReportedByAddressLine1.Name = "ItemForstrReportedByAddressLine1";
            this.ItemForstrReportedByAddressLine1.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByAddressLine1.Text = "Address Line 1:";
            this.ItemForstrReportedByAddressLine1.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByAddressLine2
            // 
            this.ItemForstrReportedByAddressLine2.Control = this.strReportedByAddressLine2TextEdit;
            this.ItemForstrReportedByAddressLine2.CustomizationFormText = "Reported By Address Line 2:";
            this.ItemForstrReportedByAddressLine2.Location = new System.Drawing.Point(0, 132);
            this.ItemForstrReportedByAddressLine2.Name = "ItemForstrReportedByAddressLine2";
            this.ItemForstrReportedByAddressLine2.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByAddressLine2.Text = "Address Line 2:";
            this.ItemForstrReportedByAddressLine2.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByAddressLine3
            // 
            this.ItemForstrReportedByAddressLine3.Control = this.strReportedByAddressLine3TextEdit;
            this.ItemForstrReportedByAddressLine3.CustomizationFormText = "Reported By Address Line 3:";
            this.ItemForstrReportedByAddressLine3.Location = new System.Drawing.Point(0, 156);
            this.ItemForstrReportedByAddressLine3.Name = "ItemForstrReportedByAddressLine3";
            this.ItemForstrReportedByAddressLine3.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByAddressLine3.Text = "Address Line 3:";
            this.ItemForstrReportedByAddressLine3.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByAddressLine4
            // 
            this.ItemForstrReportedByAddressLine4.Control = this.strReportedByAddressLine4TextEdit;
            this.ItemForstrReportedByAddressLine4.CustomizationFormText = "Reported By Address Line 4:";
            this.ItemForstrReportedByAddressLine4.Location = new System.Drawing.Point(0, 180);
            this.ItemForstrReportedByAddressLine4.Name = "ItemForstrReportedByAddressLine4";
            this.ItemForstrReportedByAddressLine4.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByAddressLine4.Text = "Address Line 4:";
            this.ItemForstrReportedByAddressLine4.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByAddressLine5
            // 
            this.ItemForstrReportedByAddressLine5.Control = this.strReportedByAddressLine5TextEdit;
            this.ItemForstrReportedByAddressLine5.CustomizationFormText = "Reported By Address Line 5:";
            this.ItemForstrReportedByAddressLine5.Location = new System.Drawing.Point(0, 204);
            this.ItemForstrReportedByAddressLine5.Name = "ItemForstrReportedByAddressLine5";
            this.ItemForstrReportedByAddressLine5.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByAddressLine5.Text = "Address Line 5:";
            this.ItemForstrReportedByAddressLine5.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByPostcode
            // 
            this.ItemForstrReportedByPostcode.Control = this.strReportedByPostcodeTextEdit;
            this.ItemForstrReportedByPostcode.CustomizationFormText = "Reported By Postcode:";
            this.ItemForstrReportedByPostcode.Location = new System.Drawing.Point(0, 228);
            this.ItemForstrReportedByPostcode.Name = "ItemForstrReportedByPostcode";
            this.ItemForstrReportedByPostcode.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByPostcode.Text = "Postcode:";
            this.ItemForstrReportedByPostcode.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByTelephone1
            // 
            this.ItemForstrReportedByTelephone1.Control = this.strReportedByTelephone1TextEdit;
            this.ItemForstrReportedByTelephone1.CustomizationFormText = "Reported By Telephone 1:";
            this.ItemForstrReportedByTelephone1.Location = new System.Drawing.Point(0, 262);
            this.ItemForstrReportedByTelephone1.Name = "ItemForstrReportedByTelephone1";
            this.ItemForstrReportedByTelephone1.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByTelephone1.Text = "Telephone 1:";
            this.ItemForstrReportedByTelephone1.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByTelephone2
            // 
            this.ItemForstrReportedByTelephone2.Control = this.strReportedByTelephone2TextEdit;
            this.ItemForstrReportedByTelephone2.CustomizationFormText = "Reported By Telephone 2:";
            this.ItemForstrReportedByTelephone2.Location = new System.Drawing.Point(0, 286);
            this.ItemForstrReportedByTelephone2.Name = "ItemForstrReportedByTelephone2";
            this.ItemForstrReportedByTelephone2.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByTelephone2.Text = "Telephone 2:";
            this.ItemForstrReportedByTelephone2.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByFax
            // 
            this.ItemForstrReportedByFax.Control = this.strReportedByFaxTextEdit;
            this.ItemForstrReportedByFax.CustomizationFormText = "Reported By Fax:";
            this.ItemForstrReportedByFax.Location = new System.Drawing.Point(0, 310);
            this.ItemForstrReportedByFax.Name = "ItemForstrReportedByFax";
            this.ItemForstrReportedByFax.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByFax.Text = "Fax:";
            this.ItemForstrReportedByFax.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByMobile
            // 
            this.ItemForstrReportedByMobile.Control = this.strReportedByMobileTextEdit;
            this.ItemForstrReportedByMobile.CustomizationFormText = "Reported By Mobile:";
            this.ItemForstrReportedByMobile.Location = new System.Drawing.Point(0, 334);
            this.ItemForstrReportedByMobile.Name = "ItemForstrReportedByMobile";
            this.ItemForstrReportedByMobile.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByMobile.Text = "Mobile:";
            this.ItemForstrReportedByMobile.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByEmail
            // 
            this.ItemForstrReportedByEmail.Control = this.strReportedByEmailTextEdit;
            this.ItemForstrReportedByEmail.CustomizationFormText = "Reported By Email:";
            this.ItemForstrReportedByEmail.Location = new System.Drawing.Point(0, 358);
            this.ItemForstrReportedByEmail.Name = "ItemForstrReportedByEmail";
            this.ItemForstrReportedByEmail.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByEmail.Text = "Email:";
            this.ItemForstrReportedByEmail.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrReportedByForename
            // 
            this.ItemForstrReportedByForename.Control = this.strReportedByForenameTextEdit;
            this.ItemForstrReportedByForename.CustomizationFormText = "Reported By Forename:";
            this.ItemForstrReportedByForename.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrReportedByForename.Name = "ItemForstrReportedByForename";
            this.ItemForstrReportedByForename.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrReportedByForename.Text = "Forename:";
            this.ItemForstrReportedByForename.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(700, 10);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 252);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(700, 10);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 82);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(471, 26);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnCopyToIncidentAddress;
            this.layoutControlItem4.CustomizationFormText = "Copy to Incident Address:";
            this.layoutControlItem4.Location = new System.Drawing.Point(471, 82);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(139, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(139, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(139, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Copy to Incident Address:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.checkButtonGazetteer2;
            this.layoutControlItem5.CustomizationFormText = "Address Search Button 2:";
            this.layoutControlItem5.Location = new System.Drawing.Point(610, 82);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(90, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(90, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(90, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Address Search Button 2:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Incident Address";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrIncidentAddressLine1,
            this.ItemForstrIncidentAddressLine2,
            this.ItemForstrIncidentAddressLine3,
            this.ItemForstrIncidentAddressLine4,
            this.ItemForstrIncidentAddressLine5,
            this.ItemForstrIncidentPostcode,
            this.emptySpaceItem3,
            this.emptySpaceItem11,
            this.layoutControlItem3,
            this.layoutControlItem2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(700, 382);
            this.layoutControlGroup5.Text = "Incident Address";
            // 
            // ItemForstrIncidentAddressLine1
            // 
            this.ItemForstrIncidentAddressLine1.Control = this.strIncidentAddressLine1TextEdit;
            this.ItemForstrIncidentAddressLine1.CustomizationFormText = "Incident Address Line 1:";
            this.ItemForstrIncidentAddressLine1.Location = new System.Drawing.Point(0, 26);
            this.ItemForstrIncidentAddressLine1.Name = "ItemForstrIncidentAddressLine1";
            this.ItemForstrIncidentAddressLine1.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrIncidentAddressLine1.Text = "Address Line 1:";
            this.ItemForstrIncidentAddressLine1.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrIncidentAddressLine2
            // 
            this.ItemForstrIncidentAddressLine2.Control = this.strIncidentAddressLine2TextEdit;
            this.ItemForstrIncidentAddressLine2.CustomizationFormText = "Incident Address Line 2:";
            this.ItemForstrIncidentAddressLine2.Location = new System.Drawing.Point(0, 50);
            this.ItemForstrIncidentAddressLine2.Name = "ItemForstrIncidentAddressLine2";
            this.ItemForstrIncidentAddressLine2.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrIncidentAddressLine2.Text = "Address Line 2:";
            this.ItemForstrIncidentAddressLine2.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrIncidentAddressLine3
            // 
            this.ItemForstrIncidentAddressLine3.Control = this.strIncidentAddressLine3TextEdit;
            this.ItemForstrIncidentAddressLine3.CustomizationFormText = "Incident Address Line 3:";
            this.ItemForstrIncidentAddressLine3.Location = new System.Drawing.Point(0, 74);
            this.ItemForstrIncidentAddressLine3.Name = "ItemForstrIncidentAddressLine3";
            this.ItemForstrIncidentAddressLine3.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrIncidentAddressLine3.Text = "Address Line 3:";
            this.ItemForstrIncidentAddressLine3.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrIncidentAddressLine4
            // 
            this.ItemForstrIncidentAddressLine4.Control = this.strIncidentAddressLine4TextEdit;
            this.ItemForstrIncidentAddressLine4.CustomizationFormText = "Incident Address Line 4:";
            this.ItemForstrIncidentAddressLine4.Location = new System.Drawing.Point(0, 98);
            this.ItemForstrIncidentAddressLine4.Name = "ItemForstrIncidentAddressLine4";
            this.ItemForstrIncidentAddressLine4.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrIncidentAddressLine4.Text = "Address Line 4:";
            this.ItemForstrIncidentAddressLine4.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrIncidentAddressLine5
            // 
            this.ItemForstrIncidentAddressLine5.Control = this.strIncidentAddressLine5TextEdit;
            this.ItemForstrIncidentAddressLine5.CustomizationFormText = "Incident Address Line 5:";
            this.ItemForstrIncidentAddressLine5.Location = new System.Drawing.Point(0, 122);
            this.ItemForstrIncidentAddressLine5.Name = "ItemForstrIncidentAddressLine5";
            this.ItemForstrIncidentAddressLine5.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrIncidentAddressLine5.Text = "Address Line 5";
            this.ItemForstrIncidentAddressLine5.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrIncidentPostcode
            // 
            this.ItemForstrIncidentPostcode.Control = this.strIncidentPostcodeTextEdit;
            this.ItemForstrIncidentPostcode.CustomizationFormText = "Incident Postcode:";
            this.ItemForstrIncidentPostcode.Location = new System.Drawing.Point(0, 146);
            this.ItemForstrIncidentPostcode.Name = "ItemForstrIncidentPostcode";
            this.ItemForstrIncidentPostcode.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrIncidentPostcode.Text = "Postcode:";
            this.ItemForstrIncidentPostcode.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 170);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(700, 212);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(461, 26);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkButtonGazetteer;
            this.layoutControlItem3.CustomizationFormText = "Address Search Button";
            this.layoutControlItem3.Location = new System.Drawing.Point(592, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(108, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(108, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(108, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Address Search:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnCopyToReportedBy;
            this.layoutControlItem2.CustomizationFormText = "Copy to Reported By Button";
            this.layoutControlItem2.Location = new System.Drawing.Point(461, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(131, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(131, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(131, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Copy to Reported By:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "User Specified";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrUser1,
            this.ItemForstrUser2,
            this.ItemForstrUser3,
            this.ItemForUserPickList1,
            this.ItemForUserPickList2,
            this.ItemForUserPickList3,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(700, 382);
            this.layoutControlGroup7.Text = "User Specified";
            // 
            // ItemForstrUser1
            // 
            this.ItemForstrUser1.Control = this.strUser1TextEdit;
            this.ItemForstrUser1.CustomizationFormText = "User Defined Text 1:";
            this.ItemForstrUser1.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrUser1.Name = "ItemForstrUser1";
            this.ItemForstrUser1.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrUser1.Text = "Text 1:";
            this.ItemForstrUser1.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrUser2
            // 
            this.ItemForstrUser2.Control = this.strUser2TextEdit;
            this.ItemForstrUser2.CustomizationFormText = "User Defined Text 2:";
            this.ItemForstrUser2.Location = new System.Drawing.Point(0, 24);
            this.ItemForstrUser2.Name = "ItemForstrUser2";
            this.ItemForstrUser2.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrUser2.Text = "Text 2:";
            this.ItemForstrUser2.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForstrUser3
            // 
            this.ItemForstrUser3.Control = this.strUser3TextEdit;
            this.ItemForstrUser3.CustomizationFormText = "User Defined Text 3:";
            this.ItemForstrUser3.Location = new System.Drawing.Point(0, 48);
            this.ItemForstrUser3.Name = "ItemForstrUser3";
            this.ItemForstrUser3.Size = new System.Drawing.Size(700, 24);
            this.ItemForstrUser3.Text = "Text 3:";
            this.ItemForstrUser3.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForUserPickList1
            // 
            this.ItemForUserPickList1.Control = this.UserPickList1GridLookUpEdit;
            this.ItemForUserPickList1.CustomizationFormText = "User Defined Pick List 1:";
            this.ItemForUserPickList1.Location = new System.Drawing.Point(0, 82);
            this.ItemForUserPickList1.Name = "ItemForUserPickList1";
            this.ItemForUserPickList1.Size = new System.Drawing.Size(700, 26);
            this.ItemForUserPickList1.Text = "Pick List 1:";
            this.ItemForUserPickList1.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForUserPickList2
            // 
            this.ItemForUserPickList2.Control = this.UserPickList2GridLookUpEdit;
            this.ItemForUserPickList2.CustomizationFormText = "User Defined Pick List 2:";
            this.ItemForUserPickList2.Location = new System.Drawing.Point(0, 108);
            this.ItemForUserPickList2.Name = "ItemForUserPickList2";
            this.ItemForUserPickList2.Size = new System.Drawing.Size(700, 26);
            this.ItemForUserPickList2.Text = "Pick List 2:";
            this.ItemForUserPickList2.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForUserPickList3
            // 
            this.ItemForUserPickList3.Control = this.UserPickList3GridLookUpEdit;
            this.ItemForUserPickList3.CustomizationFormText = "User Defined Pick List 3:";
            this.ItemForUserPickList3.Location = new System.Drawing.Point(0, 134);
            this.ItemForUserPickList3.Name = "ItemForUserPickList3";
            this.ItemForUserPickList3.Size = new System.Drawing.Size(700, 26);
            this.ItemForUserPickList3.Text = "Pick List 3:";
            this.ItemForUserPickList3.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 160);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(700, 222);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(700, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup8.CaptionImage")));
            this.layoutControlGroup8.CustomizationFormText = "Remarks";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(700, 382);
            this.layoutControlGroup8.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(700, 382);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 234);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(724, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDateClosed
            // 
            this.ItemForDateClosed.Control = this.DateClosedDateEdit;
            this.ItemForDateClosed.CustomizationFormText = "Date Closed:";
            this.ItemForDateClosed.Location = new System.Drawing.Point(0, 210);
            this.ItemForDateClosed.Name = "ItemForDateClosed";
            this.ItemForDateClosed.Size = new System.Drawing.Size(724, 24);
            this.ItemForDateClosed.Text = "Date Closed:";
            this.ItemForDateClosed.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForTargetDate
            // 
            this.ItemForTargetDate.Control = this.TargetDateDateEdit;
            this.ItemForTargetDate.CustomizationFormText = "Target Date:";
            this.ItemForTargetDate.Location = new System.Drawing.Point(0, 186);
            this.ItemForTargetDate.Name = "ItemForTargetDate";
            this.ItemForTargetDate.Size = new System.Drawing.Size(724, 24);
            this.ItemForTargetDate.Text = "Target Date:";
            this.ItemForTargetDate.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 674);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(724, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 100);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(724, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIncidentStatus
            // 
            this.ItemForIncidentStatus.Control = this.IncidentStatusGridLookUpEdit;
            this.ItemForIncidentStatus.CustomizationFormText = "Incident Status";
            this.ItemForIncidentStatus.Location = new System.Drawing.Point(0, 74);
            this.ItemForIncidentStatus.Name = "ItemForIncidentStatus";
            this.ItemForIncidentStatus.Size = new System.Drawing.Size(724, 26);
            this.ItemForIncidentStatus.Text = "Incident Status";
            this.ItemForIncidentStatus.TextSize = new System.Drawing.Size(102, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 710);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(724, 1);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AllowDrawBackground = false;
            this.layoutControlGroup4.CustomizationFormText = "autoGeneratedGroup2";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 711);
            this.layoutControlGroup4.Name = "autoGeneratedGroup2";
            this.layoutControlGroup4.Size = new System.Drawing.Size(724, 1);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(106, 0);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(106, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(106, 26);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(283, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(341, 26);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.dropDownButtonShowMap;
            this.layoutControlItem6.CustomizationFormText = "Show Map Button";
            this.layoutControlItem6.Location = new System.Drawing.Point(624, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(100, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(100, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(100, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Show Map Button";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // sp01413_AT_Incident_EditTableAdapter
            // 
            this.sp01413_AT_Incident_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter
            // 
            this.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter.ClearBeforeFill = true;
            // 
            // sp00226_Staff_List_With_BlankTableAdapter
            // 
            this.sp00226_Staff_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelGazetteer});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanelGazetteer
            // 
            this.dockPanelGazetteer.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelGazetteer.Appearance.Options.UseBackColor = true;
            this.dockPanelGazetteer.Controls.Add(this.dockPanel1_Container);
            this.dockPanelGazetteer.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelGazetteer.ID = new System.Guid("3247064a-0850-495a-9aae-3e44ee8cd394");
            this.dockPanelGazetteer.Location = new System.Drawing.Point(761, 26);
            this.dockPanelGazetteer.Name = "dockPanelGazetteer";
            this.dockPanelGazetteer.OriginalSize = new System.Drawing.Size(407, 200);
            this.dockPanelGazetteer.Size = new System.Drawing.Size(407, 692);
            this.dockPanelGazetteer.Text = "Gazetteer";
            this.dockPanelGazetteer.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelGazetteer_ClosingPanel);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.splitContainerControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(401, 660);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.buttonEditGazetteerFindValue);
            this.splitContainerControl1.Panel1.Controls.Add(this.lookUpEditGazetteerSearchType);
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl3);
            this.splitContainerControl1.Panel1.Controls.Add(this.radioGroupGazetteerMatchPattern);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Find Criteria   [Enter criteria then click Find button]";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Find Results   [Double click on result row to transfer address to incident]";
            this.splitContainerControl1.Size = new System.Drawing.Size(401, 660);
            this.splitContainerControl1.SplitterPosition = 98;
            this.splitContainerControl1.TabIndex = 13;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(3, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Search Type:";
            // 
            // buttonEditGazetteerFindValue
            // 
            this.buttonEditGazetteerFindValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditGazetteerFindValue.Location = new System.Drawing.Point(74, 50);
            this.buttonEditGazetteerFindValue.MenuManager = this.barManager1;
            this.buttonEditGazetteerFindValue.Name = "buttonEditGazetteerFindValue";
            this.buttonEditGazetteerFindValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Find", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject73, serializableAppearanceObject74, serializableAppearanceObject75, serializableAppearanceObject76, "", null, null, true)});
            this.buttonEditGazetteerFindValue.Properties.MaxLength = 100;
            this.buttonEditGazetteerFindValue.Size = new System.Drawing.Size(321, 20);
            this.buttonEditGazetteerFindValue.TabIndex = 5;
            this.buttonEditGazetteerFindValue.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditGazetteerFindValue_ButtonClick);
            this.buttonEditGazetteerFindValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonEditGazetteerFindValue_KeyPress);
            // 
            // lookUpEditGazetteerSearchType
            // 
            this.lookUpEditGazetteerSearchType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEditGazetteerSearchType.Location = new System.Drawing.Point(74, 3);
            this.lookUpEditGazetteerSearchType.MenuManager = this.barManager1;
            this.lookUpEditGazetteerSearchType.Name = "lookUpEditGazetteerSearchType";
            this.lookUpEditGazetteerSearchType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditGazetteerSearchType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Search Type", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("RecordOrder", "Record Order", 75, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEditGazetteerSearchType.Properties.DataSource = this.sp01311ATTreePickerGazetteerSearchTypesBindingSource;
            this.lookUpEditGazetteerSearchType.Properties.DisplayMember = "Description";
            this.lookUpEditGazetteerSearchType.Properties.NullText = "";
            this.lookUpEditGazetteerSearchType.Properties.ValueMember = "Description";
            this.lookUpEditGazetteerSearchType.Size = new System.Drawing.Size(321, 20);
            this.lookUpEditGazetteerSearchType.TabIndex = 1;
            this.lookUpEditGazetteerSearchType.EditValueChanged += new System.EventHandler(this.lookUpEditGazetteerSearchType_EditValueChanged);
            // 
            // sp01311ATTreePickerGazetteerSearchTypesBindingSource
            // 
            this.sp01311ATTreePickerGazetteerSearchTypesBindingSource.DataMember = "sp01311_AT_Tree_Picker_Gazetteer_Search_Types";
            this.sp01311ATTreePickerGazetteerSearchTypesBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(3, 53);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(66, 13);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Search Value:";
            // 
            // radioGroupGazetteerMatchPattern
            // 
            this.radioGroupGazetteerMatchPattern.EditValue = 0;
            this.radioGroupGazetteerMatchPattern.Location = new System.Drawing.Point(74, 26);
            this.radioGroupGazetteerMatchPattern.MenuManager = this.barManager1;
            this.radioGroupGazetteerMatchPattern.Name = "radioGroupGazetteerMatchPattern";
            this.radioGroupGazetteerMatchPattern.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroupGazetteerMatchPattern.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupGazetteerMatchPattern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Starts With"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Contains")});
            this.radioGroupGazetteerMatchPattern.Size = new System.Drawing.Size(163, 21);
            this.radioGroupGazetteerMatchPattern.TabIndex = 4;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl3;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer1.Size = new System.Drawing.Size(397, 532);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter
            // 
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter
            // 
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // pmGazetteer
            // 
            this.pmGazetteer.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerTransferAddressToIncident),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerTransferToReportedBy, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerClearResults)});
            this.pmGazetteer.Manager = this.barManager1;
            this.pmGazetteer.MenuCaption = "Gazetteer";
            this.pmGazetteer.Name = "pmGazetteer";
            this.pmGazetteer.ShowCaption = true;
            // 
            // bbiGazetteerTransferAddressToIncident
            // 
            this.bbiGazetteerTransferAddressToIncident.Caption = "Transfer Address to Incident Address";
            this.bbiGazetteerTransferAddressToIncident.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiGazetteerTransferAddressToIncident.Glyph")));
            this.bbiGazetteerTransferAddressToIncident.Id = 26;
            this.bbiGazetteerTransferAddressToIncident.Name = "bbiGazetteerTransferAddressToIncident";
            this.bbiGazetteerTransferAddressToIncident.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGazetteerTransferAddress_ItemClick);
            // 
            // bbiGazetteerTransferToReportedBy
            // 
            this.bbiGazetteerTransferToReportedBy.Caption = "Transfer Address To Reported By Address";
            this.bbiGazetteerTransferToReportedBy.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiGazetteerTransferToReportedBy.Glyph")));
            this.bbiGazetteerTransferToReportedBy.Id = 29;
            this.bbiGazetteerTransferToReportedBy.Name = "bbiGazetteerTransferToReportedBy";
            this.bbiGazetteerTransferToReportedBy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGazetteerTransferToReportedBy_ItemClick);
            // 
            // bbiGazetteerClearResults
            // 
            this.bbiGazetteerClearResults.Caption = "Clear Results";
            this.bbiGazetteerClearResults.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiGazetteerClearResults.Glyph")));
            this.bbiGazetteerClearResults.Id = 27;
            this.bbiGazetteerClearResults.Name = "bbiGazetteerClearResults";
            this.bbiGazetteerClearResults.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGazetteerClearResults_ItemClick);
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // frm_AT_Incident_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1168, 748);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.dockPanelGazetteer);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_AT_Incident_Edit";
            this.Text = "Edit Incident";
            this.Activated += new System.EventHandler(this.frm_AT_Incident_Edit_Activated);
            this.Deactivate += new System.EventHandler(this.frm_AT_Incident_Edit_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AT_Incident_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AT_Incident_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dockPanelGazetteer, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01312ATTreePickerGazetteerSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pmGazetteerSearchRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01413ATIncidentEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Incidents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIncidentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strBriefDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intXTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentAddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentAddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentAddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentAddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentAddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strIncidentPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByTitleTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedBySurnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByForenameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByAddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByAddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByAddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByAddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByAddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByTelephone1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByTelephone2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByFaxTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByMobileTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReportedByEmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strUser3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateClosedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateClosedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intDistrictIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intIncidentTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01372ATMultiplePicklistsWithBlanksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strReferenceIDButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPersonRecordedByIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intPersonResponsibleIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateRecordedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateRecordedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList1GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList3GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserPickList2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentStatusGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintIncidentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintDistrictID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintIncidentTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReferenceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrBriefDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPersonRecordedByID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForintPersonResponsibleID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFordtDateRecorded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedBySurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByTelephone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByTelephone2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrReportedByForename)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrIncidentPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrUser3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserPickList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateClosed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanelGazetteer.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditGazetteerFindValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditGazetteerSearchType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01311ATTreePickerGazetteerSearchTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGazetteerMatchPattern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pmGazetteer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit intIncidentIDTextEdit;
        private System.Windows.Forms.BindingSource sp01413ATIncidentEditBindingSource;
        private DataSet_AT_Incidents dataSet_AT_Incidents;
        private DevExpress.XtraEditors.TextEdit strBriefDescriptionTextEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraEditors.TextEdit strIncidentAddressLine1TextEdit;
        private DevExpress.XtraEditors.TextEdit strIncidentAddressLine2TextEdit;
        private DevExpress.XtraEditors.TextEdit strIncidentAddressLine3TextEdit;
        private DevExpress.XtraEditors.TextEdit strIncidentAddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit strIncidentAddressLine5TextEdit;
        private DevExpress.XtraEditors.TextEdit strIncidentPostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit intXTextEdit;
        private DevExpress.XtraEditors.TextEdit intYTextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByTitleTextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedBySurnameTextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByForenameTextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByAddressLine1TextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByAddressLine2TextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByAddressLine3TextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByAddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByAddressLine5TextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByPostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByTelephone1TextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByTelephone2TextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByFaxTextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByMobileTextEdit;
        private DevExpress.XtraEditors.TextEdit strReportedByEmailTextEdit;
        private DevExpress.XtraEditors.TextEdit strUser1TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser2TextEdit;
        private DevExpress.XtraEditors.TextEdit strUser3TextEdit;
        private DevExpress.XtraEditors.DateEdit DateClosedDateEdit;
        private DevExpress.XtraEditors.TextEdit intDistrictIDTextEdit;
        private DevExpress.XtraEditors.DateEdit TargetDateDateEdit;
        private DevExpress.XtraEditors.GridLookUpEdit intIncidentTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.ButtonEdit strReferenceIDButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintIncidentID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintIncidentTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReferenceID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrBriefDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintPersonRecordedByID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintPersonResponsibleID;
        private DevExpress.XtraLayout.LayoutControlItem ItemFordtDateRecorded;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrUser3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateClosed;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintDistrictID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserPickList3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncidentStatus;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTargetDate;
        private WoodPlan5.DataSet_AT_IncidentsTableAdapters.sp01413_AT_Incident_EditTableAdapter sp01413_AT_Incident_EditTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit intPersonRecordedByIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GridLookUpEdit intPersonResponsibleIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.DateEdit dtDateRecordedDateEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrIncidentAddressLine1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrIncidentAddressLine2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrIncidentAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrIncidentAddressLine4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrIncidentAddressLine5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrIncidentPostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintX;
        private DevExpress.XtraLayout.LayoutControlItem ItemForintY;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByTitle;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedBySurname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByAddressLine1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByAddressLine2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByAddressLine4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByAddressLine5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByPostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByTelephone1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByTelephone2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByFax;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByMobile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrReportedByForename;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList1GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList3GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.GridLookUpEdit UserPickList2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.GridLookUpEdit IncidentStatusGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.BindingSource sp01372ATMultiplePicklistsWithBlanksBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter sp01372_AT_Multiple_Picklists_With_BlanksTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colItemCode;
        private DevExpress.XtraGrid.Columns.GridColumn colItemDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private System.Windows.Forms.BindingSource sp00226StaffListWithBlankBindingSource;
        private WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter sp00226_Staff_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colNetworkID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colUserType;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelGazetteer;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ButtonEdit buttonEditGazetteerFindValue;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditGazetteerSearchType;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.RadioGroup radioGroupGazetteerMatchPattern;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate1;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate1;
        private System.Windows.Forms.BindingSource sp01312ATTreePickerGazetteerSearchBindingSource;
        private DataSet_AT_TreePicker dataSet_AT_TreePicker;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter;
        private System.Windows.Forms.BindingSource sp01311ATTreePickerGazetteerSearchTypesBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter;
        private DevExpress.XtraEditors.CheckButton checkButtonGazetteer;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraBars.BarButtonItem bbiGazetteerTransferAddressToIncident;
        private DevExpress.XtraBars.BarButtonItem bbiGazetteerClearResults;
        private DevExpress.XtraBars.PopupMenu pmGazetteer;
        private DevExpress.XtraEditors.SimpleButton btnCopyToReportedBy;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.DropDownButton dropDownButtonShowMap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraBars.BarEditItem beiGazetteerSearchRadius;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraBars.PopupMenu pmGazetteerSearchRadius;
        private DevExpress.XtraEditors.SimpleButton btnCopyToIncidentAddress;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraBars.BarButtonItem bbiGazetteerTransferToReportedBy;
        private DevExpress.XtraEditors.CheckButton checkButtonGazetteer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
    }
}

namespace WoodPlan5
{
    partial class frm_AT_Mapping_Layer_Label_Properties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Mapping_Layer_Label_Properties));
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01268ATTreePickerWorkspacelayerlabellingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_TreePicker = new WoodPlan5.DataSet_AT_TreePicker();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLabelStructureID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStructureType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStructureTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrefix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPrefix = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colColumnName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditColumnName = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp01269ATTreePickerWorkspacelayerLabelfieldsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFriendlyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInteralName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuffix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditSuffix = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNewLine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditNewLine = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colSeperator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditSeperator = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFieldOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ceDuplicates = new DevExpress.XtraEditors.CheckEdit();
            this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.seLabelRangeTo = new DevExpress.XtraEditors.SpinEdit();
            this.seLabelRangeFrom = new DevExpress.XtraEditors.SpinEdit();
            this.seFontOffset = new DevExpress.XtraEditors.SpinEdit();
            this.fontEdit1 = new DevExpress.XtraEditors.FontEdit();
            this.popupContainerControl_LabelAlignment = new DevExpress.XtraEditors.PopupContainerControl();
            this.alignmentControl1 = new BaseObjects.AlignmentControl();
            this.seFontSize = new DevExpress.XtraEditors.SpinEdit();
            this.tbcTransparency = new DevExpress.XtraEditors.TrackBarControl();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.popupContainerEditLabelPosition = new DevExpress.XtraEditors.PopupContainerEdit();
            this.colorEdit1 = new DevExpress.XtraEditors.ColorEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.ceShowOverlaps = new DevExpress.XtraEditors.CheckEdit();
            this.colorEditLabelFontColour = new DevExpress.XtraEditors.ColorEdit();
            this.seFontAngle = new DevExpress.XtraEditors.SpinEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp01268_AT_Tree_Picker_Workspace_layer_labellingTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01268_AT_Tree_Picker_Workspace_layer_labellingTableAdapter();
            this.sp01269_AT_Tree_Picker_Workspace_layer_Label_fieldsTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01269_AT_Tree_Picker_Workspace_layer_Label_fieldsTableAdapter();
            this.popupMenuLabelStructure = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItemAddLabelLine = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAddTooltipLine = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemDeleteLine = new DevExpress.XtraBars.BarButtonItem();
            this.sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_EditTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_EditTableAdapter();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01268ATTreePickerWorkspacelayerlabellingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPrefix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditColumnName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01269ATTreePickerWorkspacelayerLabelfieldsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditSuffix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditNewLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditSeperator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDuplicates.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seLabelRangeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seLabelRangeFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFontOffset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fontEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl_LabelAlignment)).BeginInit();
            this.popupContainerControl_LabelAlignment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTransparency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTransparency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditLabelPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceShowOverlaps.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditLabelFontColour.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFontAngle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuLabelStructure)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(604, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 570);
            this.barDockControlBottom.Size = new System.Drawing.Size(604, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 570);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(604, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 570);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.barButtonItemAddLabelLine,
            this.barButtonItemAddTooltipLine,
            this.barButtonItemDeleteLine});
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp01268ATTreePickerWorkspacelayerlabellingBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemSpinEdit1,
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4,
            this.repositoryItemGridLookUpEditColumnName,
            this.repositoryItemTextEditPrefix,
            this.repositoryItemTextEditSuffix,
            this.repositoryItemCheckEditNewLine,
            this.repositoryItemTextEditSeperator,
            this.repositoryItemSpinEdit2});
            this.gridControl3.Size = new System.Drawing.Size(576, 189);
            this.gridControl3.TabIndex = 5;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp01268ATTreePickerWorkspacelayerlabellingBindingSource
            // 
            this.sp01268ATTreePickerWorkspacelayerlabellingBindingSource.DataMember = "sp01268_AT_Tree_Picker_Workspace_layer_labelling";
            this.sp01268ATTreePickerWorkspacelayerlabellingBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // dataSet_AT_TreePicker
            // 
            this.dataSet_AT_TreePicker.DataSetName = "DataSet_AT_TreePicker";
            this.dataSet_AT_TreePicker.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLabelStructureID,
            this.colLayerID,
            this.colStructureType,
            this.colStructureTypeDescription,
            this.colPrefix,
            this.colColumnName,
            this.colSuffix,
            this.colNewLine,
            this.colSeperator,
            this.colFieldOrder});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsCustomization.AllowFilter = false;
            this.gridView3.OptionsCustomization.AllowGroup = false;
            this.gridView3.OptionsCustomization.AllowSort = false;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStructureTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFieldOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.ViewCaption = "Label \\ Tooltip Structure";
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView3_PopupMenuShowing);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView3_CustomDrawEmptyForeground);
            this.gridView3.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView3_FocusedRowChanged);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseDown);
            // 
            // colLabelStructureID
            // 
            this.colLabelStructureID.Caption = "Label Structure ID";
            this.colLabelStructureID.FieldName = "LabelStructureID";
            this.colLabelStructureID.Name = "colLabelStructureID";
            this.colLabelStructureID.OptionsColumn.AllowEdit = false;
            this.colLabelStructureID.OptionsColumn.AllowFocus = false;
            this.colLabelStructureID.OptionsColumn.ReadOnly = true;
            this.colLabelStructureID.Width = 98;
            // 
            // colLayerID
            // 
            this.colLayerID.Caption = "Layer ID";
            this.colLayerID.FieldName = "LayerID";
            this.colLayerID.Name = "colLayerID";
            this.colLayerID.OptionsColumn.AllowEdit = false;
            this.colLayerID.OptionsColumn.AllowFocus = false;
            this.colLayerID.OptionsColumn.ReadOnly = true;
            this.colLayerID.Width = 52;
            // 
            // colStructureType
            // 
            this.colStructureType.Caption = "Structure Type ID";
            this.colStructureType.FieldName = "StructureType";
            this.colStructureType.Name = "colStructureType";
            this.colStructureType.OptionsColumn.AllowEdit = false;
            this.colStructureType.OptionsColumn.AllowFocus = false;
            this.colStructureType.OptionsColumn.ReadOnly = true;
            this.colStructureType.Width = 97;
            // 
            // colStructureTypeDescription
            // 
            this.colStructureTypeDescription.Caption = "Type";
            this.colStructureTypeDescription.FieldName = "StructureTypeDescription";
            this.colStructureTypeDescription.Name = "colStructureTypeDescription";
            this.colStructureTypeDescription.OptionsColumn.AllowEdit = false;
            this.colStructureTypeDescription.OptionsColumn.AllowFocus = false;
            this.colStructureTypeDescription.OptionsColumn.ReadOnly = true;
            this.colStructureTypeDescription.Visible = true;
            this.colStructureTypeDescription.VisibleIndex = 0;
            this.colStructureTypeDescription.Width = 58;
            // 
            // colPrefix
            // 
            this.colPrefix.Caption = "Prefix";
            this.colPrefix.ColumnEdit = this.repositoryItemTextEditPrefix;
            this.colPrefix.FieldName = "Prefix";
            this.colPrefix.Name = "colPrefix";
            this.colPrefix.Visible = true;
            this.colPrefix.VisibleIndex = 0;
            this.colPrefix.Width = 59;
            // 
            // repositoryItemTextEditPrefix
            // 
            this.repositoryItemTextEditPrefix.AutoHeight = false;
            this.repositoryItemTextEditPrefix.MaxLength = 50;
            this.repositoryItemTextEditPrefix.Name = "repositoryItemTextEditPrefix";
            // 
            // colColumnName
            // 
            this.colColumnName.Caption = "Column Name";
            this.colColumnName.ColumnEdit = this.repositoryItemGridLookUpEditColumnName;
            this.colColumnName.FieldName = "ColumnName";
            this.colColumnName.Name = "colColumnName";
            this.colColumnName.Visible = true;
            this.colColumnName.VisibleIndex = 1;
            this.colColumnName.Width = 188;
            // 
            // repositoryItemGridLookUpEditColumnName
            // 
            this.repositoryItemGridLookUpEditColumnName.AutoHeight = false;
            this.repositoryItemGridLookUpEditColumnName.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditColumnName.DataSource = this.sp01269ATTreePickerWorkspacelayerLabelfieldsBindingSource;
            this.repositoryItemGridLookUpEditColumnName.DisplayMember = "FriendlyName";
            this.repositoryItemGridLookUpEditColumnName.Name = "repositoryItemGridLookUpEditColumnName";
            this.repositoryItemGridLookUpEditColumnName.NullText = "";
            this.repositoryItemGridLookUpEditColumnName.NullValuePrompt = "Select a Value";
            this.repositoryItemGridLookUpEditColumnName.ValueMember = "InteralName";
            this.repositoryItemGridLookUpEditColumnName.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // sp01269ATTreePickerWorkspacelayerLabelfieldsBindingSource
            // 
            this.sp01269ATTreePickerWorkspacelayerLabelfieldsBindingSource.DataMember = "sp01269_AT_Tree_Picker_Workspace_layer_Label_fields";
            this.sp01269ATTreePickerWorkspacelayerLabelfieldsBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFriendlyName,
            this.colInteralName,
            this.colRecordOrder});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colFriendlyName
            // 
            this.colFriendlyName.Caption = "Field Name";
            this.colFriendlyName.FieldName = "FriendlyName";
            this.colFriendlyName.Name = "colFriendlyName";
            this.colFriendlyName.Visible = true;
            this.colFriendlyName.VisibleIndex = 0;
            this.colFriendlyName.Width = 270;
            // 
            // colInteralName
            // 
            this.colInteralName.Caption = "Internal Name";
            this.colInteralName.FieldName = "InteralName";
            this.colInteralName.Name = "colInteralName";
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Field Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            // 
            // colSuffix
            // 
            this.colSuffix.Caption = "Suffix";
            this.colSuffix.ColumnEdit = this.repositoryItemTextEditSuffix;
            this.colSuffix.FieldName = "Suffix";
            this.colSuffix.Name = "colSuffix";
            this.colSuffix.Visible = true;
            this.colSuffix.VisibleIndex = 2;
            this.colSuffix.Width = 56;
            // 
            // repositoryItemTextEditSuffix
            // 
            this.repositoryItemTextEditSuffix.AutoHeight = false;
            this.repositoryItemTextEditSuffix.MaxLength = 50;
            this.repositoryItemTextEditSuffix.Name = "repositoryItemTextEditSuffix";
            // 
            // colNewLine
            // 
            this.colNewLine.Caption = "New Line";
            this.colNewLine.ColumnEdit = this.repositoryItemCheckEditNewLine;
            this.colNewLine.FieldName = "NewLine";
            this.colNewLine.Name = "colNewLine";
            this.colNewLine.Visible = true;
            this.colNewLine.VisibleIndex = 3;
            this.colNewLine.Width = 64;
            // 
            // repositoryItemCheckEditNewLine
            // 
            this.repositoryItemCheckEditNewLine.AutoHeight = false;
            this.repositoryItemCheckEditNewLine.Name = "repositoryItemCheckEditNewLine";
            this.repositoryItemCheckEditNewLine.ValueChecked = 1;
            this.repositoryItemCheckEditNewLine.ValueUnchecked = 0;
            // 
            // colSeperator
            // 
            this.colSeperator.Caption = "Seperator";
            this.colSeperator.ColumnEdit = this.repositoryItemTextEditSeperator;
            this.colSeperator.FieldName = "Seperator";
            this.colSeperator.Name = "colSeperator";
            this.colSeperator.Visible = true;
            this.colSeperator.VisibleIndex = 4;
            this.colSeperator.Width = 69;
            // 
            // repositoryItemTextEditSeperator
            // 
            this.repositoryItemTextEditSeperator.AutoHeight = false;
            this.repositoryItemTextEditSeperator.MaxLength = 50;
            this.repositoryItemTextEditSeperator.Name = "repositoryItemTextEditSeperator";
            // 
            // colFieldOrder
            // 
            this.colFieldOrder.Caption = "Order";
            this.colFieldOrder.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colFieldOrder.FieldName = "FieldOrder";
            this.colFieldOrder.Name = "colFieldOrder";
            this.colFieldOrder.Visible = true;
            this.colFieldOrder.VisibleIndex = 5;
            this.colFieldOrder.Width = 62;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.IsFloatValue = false;
            this.repositoryItemSpinEdit2.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.MaxLength = 100;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.MaxLength = 100;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.MaxLength = 100;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // ceDuplicates
            // 
            this.ceDuplicates.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource, "ShowDuplicates", true));
            this.ceDuplicates.EditValue = true;
            this.ceDuplicates.Location = new System.Drawing.Point(377, 271);
            this.ceDuplicates.MenuManager = this.barManager1;
            this.ceDuplicates.Name = "ceDuplicates";
            this.ceDuplicates.Properties.Caption = "";
            this.ceDuplicates.Properties.ValueChecked = 1;
            this.ceDuplicates.Properties.ValueUnchecked = 0;
            this.ceDuplicates.Size = new System.Drawing.Size(212, 19);
            this.ceDuplicates.StyleController = this.dataLayoutControl1;
            this.ceDuplicates.TabIndex = 11;
            // 
            // sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource
            // 
            this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource.DataMember = "sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_Edit";
            this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.seLabelRangeTo);
            this.dataLayoutControl1.Controls.Add(this.seLabelRangeFrom);
            this.dataLayoutControl1.Controls.Add(this.seFontOffset);
            this.dataLayoutControl1.Controls.Add(this.fontEdit1);
            this.dataLayoutControl1.Controls.Add(this.popupContainerControl_LabelAlignment);
            this.dataLayoutControl1.Controls.Add(this.seFontSize);
            this.dataLayoutControl1.Controls.Add(this.tbcTransparency);
            this.dataLayoutControl1.Controls.Add(this.spinEdit1);
            this.dataLayoutControl1.Controls.Add(this.ceDuplicates);
            this.dataLayoutControl1.Controls.Add(this.popupContainerEditLabelPosition);
            this.dataLayoutControl1.Controls.Add(this.colorEdit1);
            this.dataLayoutControl1.Controls.Add(this.btnCancel);
            this.dataLayoutControl1.Controls.Add(this.ceShowOverlaps);
            this.dataLayoutControl1.Controls.Add(this.colorEditLabelFontColour);
            this.dataLayoutControl1.Controls.Add(this.seFontAngle);
            this.dataLayoutControl1.Controls.Add(this.btnOK);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer1);
            this.dataLayoutControl1.DataSource = this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup5;
            this.dataLayoutControl1.Size = new System.Drawing.Size(604, 570);
            this.dataLayoutControl1.TabIndex = 5;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // seLabelRangeTo
            // 
            this.seLabelRangeTo.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource, "VisibleToScale", true));
            this.seLabelRangeTo.EditValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.seLabelRangeTo.Location = new System.Drawing.Point(377, 247);
            this.seLabelRangeTo.MenuManager = this.barManager1;
            this.seLabelRangeTo.Name = "seLabelRangeTo";
            this.seLabelRangeTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seLabelRangeTo.Properties.IsFloatValue = false;
            this.seLabelRangeTo.Properties.Mask.EditMask = "#######0 Metres";
            this.seLabelRangeTo.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.seLabelRangeTo.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.seLabelRangeTo.Size = new System.Drawing.Size(212, 20);
            this.seLabelRangeTo.StyleController = this.dataLayoutControl1;
            this.seLabelRangeTo.TabIndex = 9;
            // 
            // seLabelRangeFrom
            // 
            this.seLabelRangeFrom.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource, "VisibleFromScale", true));
            this.seLabelRangeFrom.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seLabelRangeFrom.Location = new System.Drawing.Point(88, 247);
            this.seLabelRangeFrom.MenuManager = this.barManager1;
            this.seLabelRangeFrom.Name = "seLabelRangeFrom";
            this.seLabelRangeFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seLabelRangeFrom.Properties.IsFloatValue = false;
            this.seLabelRangeFrom.Properties.Mask.EditMask = "#######0 Metres";
            this.seLabelRangeFrom.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.seLabelRangeFrom.Size = new System.Drawing.Size(212, 20);
            this.seLabelRangeFrom.StyleController = this.dataLayoutControl1;
            this.seLabelRangeFrom.TabIndex = 8;
            // 
            // seFontOffset
            // 
            this.seFontOffset.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource, "Offset", true));
            this.seFontOffset.EditValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.seFontOffset.Location = new System.Drawing.Point(377, 223);
            this.seFontOffset.MenuManager = this.barManager1;
            this.seFontOffset.Name = "seFontOffset";
            this.seFontOffset.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seFontOffset.Properties.IsFloatValue = false;
            this.seFontOffset.Properties.Mask.EditMask = "N00";
            this.seFontOffset.Properties.MaxValue = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.seFontOffset.Size = new System.Drawing.Size(212, 20);
            this.seFontOffset.StyleController = this.dataLayoutControl1;
            this.seFontOffset.TabIndex = 30;
            // 
            // fontEdit1
            // 
            this.fontEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource, "FontName", true));
            this.fontEdit1.EditValue = "Tahoma";
            this.fontEdit1.Location = new System.Drawing.Point(88, 175);
            this.fontEdit1.MenuManager = this.barManager1;
            this.fontEdit1.Name = "fontEdit1";
            this.fontEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fontEdit1.Size = new System.Drawing.Size(212, 20);
            this.fontEdit1.StyleController = this.dataLayoutControl1;
            this.fontEdit1.TabIndex = 5;
            // 
            // popupContainerControl_LabelAlignment
            // 
            this.popupContainerControl_LabelAlignment.Controls.Add(this.alignmentControl1);
            this.popupContainerControl_LabelAlignment.Location = new System.Drawing.Point(127, 5);
            this.popupContainerControl_LabelAlignment.Name = "popupContainerControl_LabelAlignment";
            this.popupContainerControl_LabelAlignment.Size = new System.Drawing.Size(94, 94);
            this.popupContainerControl_LabelAlignment.TabIndex = 29;
            // 
            // alignmentControl1
            // 
            this.alignmentControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.alignmentControl1.Location = new System.Drawing.Point(0, 0);
            this.alignmentControl1.Name = "alignmentControl1";
            this.alignmentControl1.Size = new System.Drawing.Size(94, 94);
            this.alignmentControl1.TabIndex = 26;
            this.alignmentControl1.AlignmentChanged += new System.EventHandler(this.alignmentControl1_AlignmentChanged);
            // 
            // seFontSize
            // 
            this.seFontSize.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource, "FontSize", true));
            this.seFontSize.EditValue = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.seFontSize.Location = new System.Drawing.Point(377, 175);
            this.seFontSize.MenuManager = this.barManager1;
            this.seFontSize.Name = "seFontSize";
            this.seFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seFontSize.Properties.IsFloatValue = false;
            this.seFontSize.Properties.Mask.EditMask = "N00";
            this.seFontSize.Properties.MaxValue = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.seFontSize.Properties.MinValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.seFontSize.Size = new System.Drawing.Size(212, 20);
            this.seFontSize.StyleController = this.dataLayoutControl1;
            this.seFontSize.TabIndex = 5;
            // 
            // tbcTransparency
            // 
            this.tbcTransparency.EditValue = null;
            this.tbcTransparency.Location = new System.Drawing.Point(88, 37);
            this.tbcTransparency.MinimumSize = new System.Drawing.Size(0, 26);
            this.tbcTransparency.Name = "tbcTransparency";
            this.tbcTransparency.Properties.AutoSize = false;
            this.tbcTransparency.Properties.Maximum = 100;
            this.tbcTransparency.Properties.ShowValueToolTip = true;
            this.tbcTransparency.Properties.TickFrequency = 5;
            this.tbcTransparency.Size = new System.Drawing.Size(501, 26);
            this.tbcTransparency.StyleController = this.dataLayoutControl1;
            this.tbcTransparency.TabIndex = 6;
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(377, 109);
            this.spinEdit1.MenuManager = this.barManager1;
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.IsFloatValue = false;
            this.spinEdit1.Properties.Mask.EditMask = "N00";
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.spinEdit1.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEdit1.Size = new System.Drawing.Size(208, 20);
            this.spinEdit1.StyleController = this.dataLayoutControl1;
            this.spinEdit1.TabIndex = 8;
            // 
            // popupContainerEditLabelPosition
            // 
            this.popupContainerEditLabelPosition.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource, "Position", true));
            this.popupContainerEditLabelPosition.EditValue = "CentreRight";
            this.popupContainerEditLabelPosition.Location = new System.Drawing.Point(88, 223);
            this.popupContainerEditLabelPosition.MenuManager = this.barManager1;
            this.popupContainerEditLabelPosition.Name = "popupContainerEditLabelPosition";
            this.popupContainerEditLabelPosition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEditLabelPosition.Properties.PopupControl = this.popupContainerControl_LabelAlignment;
            this.popupContainerEditLabelPosition.Properties.PopupFormSize = new System.Drawing.Size(94, 120);
            this.popupContainerEditLabelPosition.Size = new System.Drawing.Size(212, 20);
            this.popupContainerEditLabelPosition.StyleController = this.dataLayoutControl1;
            this.popupContainerEditLabelPosition.TabIndex = 10;
            this.popupContainerEditLabelPosition.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.popupContainerEditLabelPosition_QueryPopUp);
            // 
            // colorEdit1
            // 
            this.colorEdit1.EditValue = System.Drawing.Color.Empty;
            this.colorEdit1.Location = new System.Drawing.Point(92, 109);
            this.colorEdit1.MenuManager = this.barManager1;
            this.colorEdit1.Name = "colorEdit1";
            this.colorEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEdit1.Properties.StoreColorAsInteger = true;
            this.colorEdit1.Size = new System.Drawing.Size(208, 20);
            this.colorEdit1.StyleController = this.dataLayoutControl1;
            this.colorEdit1.TabIndex = 7;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(304, 541);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(112, 22);
            this.btnCancel.StyleController = this.dataLayoutControl1;
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ceShowOverlaps
            // 
            this.ceShowOverlaps.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource, "ShowOverlapping", true));
            this.ceShowOverlaps.EditValue = true;
            this.ceShowOverlaps.Location = new System.Drawing.Point(88, 271);
            this.ceShowOverlaps.MenuManager = this.barManager1;
            this.ceShowOverlaps.Name = "ceShowOverlaps";
            this.ceShowOverlaps.Properties.Caption = "";
            this.ceShowOverlaps.Properties.ValueChecked = 1;
            this.ceShowOverlaps.Properties.ValueUnchecked = 0;
            this.ceShowOverlaps.Size = new System.Drawing.Size(212, 19);
            this.ceShowOverlaps.StyleController = this.dataLayoutControl1;
            this.ceShowOverlaps.TabIndex = 31;
            // 
            // colorEditLabelFontColour
            // 
            this.colorEditLabelFontColour.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource, "FontColour", true));
            this.colorEditLabelFontColour.EditValue = System.Drawing.Color.Black;
            this.colorEditLabelFontColour.Location = new System.Drawing.Point(88, 199);
            this.colorEditLabelFontColour.MenuManager = this.barManager1;
            this.colorEditLabelFontColour.Name = "colorEditLabelFontColour";
            this.colorEditLabelFontColour.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditLabelFontColour.Properties.StoreColorAsInteger = true;
            this.colorEditLabelFontColour.Size = new System.Drawing.Size(212, 20);
            this.colorEditLabelFontColour.StyleController = this.dataLayoutControl1;
            this.colorEditLabelFontColour.TabIndex = 5;
            // 
            // seFontAngle
            // 
            this.seFontAngle.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource, "FontAngle", true));
            this.seFontAngle.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seFontAngle.Location = new System.Drawing.Point(377, 199);
            this.seFontAngle.MenuManager = this.barManager1;
            this.seFontAngle.Name = "seFontAngle";
            this.seFontAngle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seFontAngle.Properties.IsFloatValue = false;
            this.seFontAngle.Properties.Mask.EditMask = "N00";
            this.seFontAngle.Properties.MaxValue = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.seFontAngle.Size = new System.Drawing.Size(212, 20);
            this.seFontAngle.StyleController = this.dataLayoutControl1;
            this.seFontAngle.TabIndex = 5;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(188, 541);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(112, 22);
            this.btnOK.StyleController = this.dataLayoutControl1;
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl3;
            this.gridSplitContainer1.Location = new System.Drawing.Point(14, 331);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer1.Size = new System.Drawing.Size(576, 189);
            this.gridSplitContainer1.TabIndex = 32;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Root";
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "Root";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup5.Size = new System.Drawing.Size(604, 570);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AllowDrawBackground = false;
            this.layoutControlGroup6.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlGroup3,
            this.layoutControlGroup2,
            this.layoutControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "autoGeneratedGroup0";
            this.layoutControlGroup6.Size = new System.Drawing.Size(594, 560);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Label Text";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 138);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup7.Size = new System.Drawing.Size(594, 157);
            this.layoutControlGroup7.Text = "Label Text";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.fontEdit1;
            this.layoutControlItem7.CustomizationFormText = "Font:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(289, 24);
            this.layoutControlItem7.Text = "Font:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.seFontSize;
            this.layoutControlItem8.CustomizationFormText = "Font Size:";
            this.layoutControlItem8.Location = new System.Drawing.Point(289, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(289, 24);
            this.layoutControlItem8.Text = "Font Size:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.colorEditLabelFontColour;
            this.layoutControlItem9.CustomizationFormText = "Font Colour:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(289, 24);
            this.layoutControlItem9.Text = "Font Colour:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.seFontAngle;
            this.layoutControlItem10.CustomizationFormText = "Font Angle:";
            this.layoutControlItem10.Location = new System.Drawing.Point(289, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(289, 24);
            this.layoutControlItem10.Text = "Font Angle:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.popupContainerEditLabelPosition;
            this.layoutControlItem11.CustomizationFormText = "Position:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(289, 24);
            this.layoutControlItem11.Text = "Position:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.seFontOffset;
            this.layoutControlItem12.CustomizationFormText = "Offset:";
            this.layoutControlItem12.Location = new System.Drawing.Point(289, 48);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(289, 24);
            this.layoutControlItem12.Text = "Offset:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.seLabelRangeFrom;
            this.layoutControlItem13.CustomizationFormText = "Visible Range:";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(289, 24);
            this.layoutControlItem13.Text = "Visible Range:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.seLabelRangeTo;
            this.layoutControlItem14.CustomizationFormText = "To:";
            this.layoutControlItem14.Location = new System.Drawing.Point(289, 72);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(289, 24);
            this.layoutControlItem14.Text = "To:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.ceShowOverlaps;
            this.layoutControlItem15.CustomizationFormText = "Overlapping:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(289, 23);
            this.layoutControlItem15.Text = "Overlapping:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.ceDuplicates;
            this.layoutControlItem16.CustomizationFormText = "Duplicates:";
            this.layoutControlItem16.Location = new System.Drawing.Point(289, 96);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(289, 23);
            this.layoutControlItem16.Text = "Duplicates:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnCancel;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(297, 534);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(116, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnOK;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(181, 534);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(116, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 524);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(594, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 534);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(181, 26);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(413, 534);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(181, 26);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Label Lines";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 68);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(594, 70);
            this.layoutControlGroup3.Text = "Label Lines";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.colorEdit1;
            this.layoutControlItem4.CustomizationFormText = "Line Colour:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(285, 24);
            this.layoutControlItem4.Text = "Line Colour:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.spinEdit1;
            this.layoutControlItem5.CustomizationFormText = "Line Width:";
            this.layoutControlItem5.Location = new System.Drawing.Point(285, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(285, 24);
            this.layoutControlItem5.Text = "Line Width:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Label Lines and Text";
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(594, 68);
            this.layoutControlGroup2.Text = "Label Lines and Text";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.tbcTransparency;
            this.layoutControlItem3.CustomizationFormText = "Transparency:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(98, 30);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(578, 30);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Transparency:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Label \\ Tooltip Structure";
            this.layoutControlGroup1.ExpandButtonVisible = true;
            this.layoutControlGroup1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 295);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.layoutControlGroup1.Size = new System.Drawing.Size(594, 229);
            this.layoutControlGroup1.Text = "Label \\ Tooltip Structure";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridSplitContainer1;
            this.layoutControlItem6.CustomizationFormText = "Label Structure Grid:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(580, 193);
            this.layoutControlItem6.Text = "Label Structure Grid:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // sp01268_AT_Tree_Picker_Workspace_layer_labellingTableAdapter
            // 
            this.sp01268_AT_Tree_Picker_Workspace_layer_labellingTableAdapter.ClearBeforeFill = true;
            // 
            // sp01269_AT_Tree_Picker_Workspace_layer_Label_fieldsTableAdapter
            // 
            this.sp01269_AT_Tree_Picker_Workspace_layer_Label_fieldsTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenuLabelStructure
            // 
            this.popupMenuLabelStructure.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemDeleteLine)});
            this.popupMenuLabelStructure.Manager = this.barManager1;
            this.popupMenuLabelStructure.MenuCaption = "Label \\ Tooltip Menu";
            this.popupMenuLabelStructure.Name = "popupMenuLabelStructure";
            this.popupMenuLabelStructure.ShowCaption = true;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Add...";
            this.barSubItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItem1.Glyph")));
            this.barSubItem1.Id = 25;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAddLabelLine),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAddTooltipLine)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barButtonItemAddLabelLine
            // 
            this.barButtonItemAddLabelLine.Caption = "Add Label Line";
            this.barButtonItemAddLabelLine.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAddLabelLine.Glyph")));
            this.barButtonItemAddLabelLine.Id = 26;
            this.barButtonItemAddLabelLine.Name = "barButtonItemAddLabelLine";
            this.barButtonItemAddLabelLine.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAddLabelLine_ItemClick);
            // 
            // barButtonItemAddTooltipLine
            // 
            this.barButtonItemAddTooltipLine.Caption = "Add Tooltip Line";
            this.barButtonItemAddTooltipLine.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemAddTooltipLine.Glyph")));
            this.barButtonItemAddTooltipLine.Id = 27;
            this.barButtonItemAddTooltipLine.Name = "barButtonItemAddTooltipLine";
            this.barButtonItemAddTooltipLine.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAddTooltipLine_ItemClick);
            // 
            // barButtonItemDeleteLine
            // 
            this.barButtonItemDeleteLine.Caption = "Delete";
            this.barButtonItemDeleteLine.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemDeleteLine.Glyph")));
            this.barButtonItemDeleteLine.Id = 28;
            this.barButtonItemDeleteLine.Name = "barButtonItemDeleteLine";
            this.barButtonItemDeleteLine.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemDeleteLine_ItemClick);
            // 
            // sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_EditTableAdapter
            // 
            this.sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_EditTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // frm_AT_Mapping_Layer_Label_Properties
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(604, 570);
            this.ControlBox = false;
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_AT_Mapping_Layer_Label_Properties";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Label - Properties";
            this.Load += new System.EventHandler(this.frm_AT_Mapping_Layer_Label_Properties_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01268ATTreePickerWorkspacelayerlabellingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPrefix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditColumnName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01269ATTreePickerWorkspacelayerLabelfieldsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditSuffix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditNewLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditSeperator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceDuplicates.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.seLabelRangeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seLabelRangeFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFontOffset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fontEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl_LabelAlignment)).EndInit();
            this.popupContainerControl_LabelAlignment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.seFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTransparency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbcTransparency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEditLabelPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceShowOverlaps.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditLabelFontColour.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seFontAngle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuLabelStructure)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.TrackBarControl tbcTransparency;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.ColorEdit colorEdit1;
        private DevExpress.XtraEditors.FontEdit fontEdit1;
        private DevExpress.XtraEditors.SpinEdit seFontSize;
        private DevExpress.XtraEditors.ColorEdit colorEditLabelFontColour;
        private DevExpress.XtraEditors.SpinEdit seFontAngle;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEditLabelPosition;
        private DevExpress.XtraEditors.SpinEdit seFontOffset;
        private DevExpress.XtraEditors.SpinEdit seLabelRangeFrom;
        private DevExpress.XtraEditors.SpinEdit seLabelRangeTo;
        private DevExpress.XtraEditors.CheckEdit ceShowOverlaps;
        private DevExpress.XtraEditors.CheckEdit ceDuplicates;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DataSet_AT_TreePicker dataSet_AT_TreePicker;
        private System.Windows.Forms.BindingSource sp01268ATTreePickerWorkspacelayerlabellingBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLabelStructureID;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerID;
        private DevExpress.XtraGrid.Columns.GridColumn colStructureType;
        private DevExpress.XtraGrid.Columns.GridColumn colStructureTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPrefix;
        private DevExpress.XtraGrid.Columns.GridColumn colColumnName;
        private DevExpress.XtraGrid.Columns.GridColumn colSuffix;
        private DevExpress.XtraGrid.Columns.GridColumn colNewLine;
        private DevExpress.XtraGrid.Columns.GridColumn colSeperator;
        private DevExpress.XtraGrid.Columns.GridColumn colFieldOrder;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01268_AT_Tree_Picker_Workspace_layer_labellingTableAdapter sp01268_AT_Tree_Picker_Workspace_layer_labellingTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPrefix;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditColumnName;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditSuffix;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditNewLine;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditSeperator;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private System.Windows.Forms.BindingSource sp01269ATTreePickerWorkspacelayerLabelfieldsBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01269_AT_Tree_Picker_Workspace_layer_Label_fieldsTableAdapter sp01269_AT_Tree_Picker_Workspace_layer_Label_fieldsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colFriendlyName;
        private DevExpress.XtraGrid.Columns.GridColumn colInteralName;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAddLabelLine;
        private DevExpress.XtraBars.BarButtonItem barButtonItemAddTooltipLine;
        private DevExpress.XtraBars.BarButtonItem barButtonItemDeleteLine;
        private DevExpress.XtraBars.PopupMenu popupMenuLabelStructure;
        private System.Windows.Forms.BindingSource sp01276ATTreePickerWorkspaceLabelLayerFontEditBindingSource;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_EditTableAdapter sp01276_AT_Tree_Picker_Workspace_Label_Layer_Font_EditTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl_LabelAlignment;
        private BaseObjects.AlignmentControl alignmentControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
    }
}

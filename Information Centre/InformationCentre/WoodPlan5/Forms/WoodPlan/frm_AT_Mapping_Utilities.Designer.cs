namespace WoodPlan5
{
    partial class frm_AT_Mapping_Utilities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AT_Mapping_Utilities));
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.spinEditTileWidth = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditHeightInPixels = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditTileHeight = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditWidthInPixels = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.spinEdit2 = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.checkEditUseFilename = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditUseCoords = new DevExpress.XtraEditors.CheckEdit();
            this.btnRegister = new DevExpress.XtraEditors.SimpleButton();
            this.btnSelectFiles = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01325ATMappingUtilitiesRegisterFilesStructureBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_TreePicker = new WoodPlan5.DataSet_AT_TreePicker();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFilePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditAllData = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditMissingData = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditAssets = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditTrees = new DevExpress.XtraEditors.CheckEdit();
            this.btnClose2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnSynchronise = new DevExpress.XtraEditors.SimpleButton();
            this.sp01325_AT_Mapping_Utilities_Register_Files_StructureTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01325_AT_Mapping_Utilities_Register_Files_StructureTableAdapter();
            this.popupMenuGrid1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiRemove = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClearGrid = new DevExpress.XtraBars.BarButtonItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTileWidth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHeightInPixels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTileHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditWidthInPixels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditUseFilename.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditUseCoords.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01325ATMappingUtilitiesRegisterFilesStructureBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMissingData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAssets.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTrees.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(713, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 469);
            this.barDockControlBottom.Size = new System.Drawing.Size(713, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 469);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(713, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 469);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRemove,
            this.bbiClearGrid});
            this.barManager1.MaxItemId = 28;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsColumn.TabStop = false;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 0;
            this.colStatus.Width = 119;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(713, 469);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(708, 443);
            this.xtraTabPage1.Text = "Register ECW Files";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Information";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.btnClose);
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControl2);
            this.splitContainerControl1.Panel2.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.btnRegister);
            this.splitContainerControl1.Panel2.Controls.Add(this.btnSelectFiles);
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(708, 443);
            this.splitContainerControl1.SplitterPosition = 58;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            this.labelControl2.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl2.Location = new System.Drawing.Point(0, 0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(704, 34);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = resources.GetString("labelControl2.Text");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(626, 349);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupControl2.Controls.Add(this.spinEditTileWidth);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.spinEditHeightInPixels);
            this.groupControl2.Controls.Add(this.spinEditTileHeight);
            this.groupControl2.Controls.Add(this.spinEditWidthInPixels);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Location = new System.Drawing.Point(331, 264);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(369, 79);
            this.groupControl2.TabIndex = 15;
            this.groupControl2.Text = "Tile Sizes";
            // 
            // spinEditTileWidth
            // 
            this.spinEditTileWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEditTileWidth.EditValue = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.spinEditTileWidth.Location = new System.Drawing.Point(74, 27);
            this.spinEditTileWidth.MenuManager = this.barManager1;
            this.spinEditTileWidth.Name = "spinEditTileWidth";
            this.spinEditTileWidth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditTileWidth.Properties.IsFloatValue = false;
            this.spinEditTileWidth.Properties.Mask.EditMask = "######0 Metres";
            this.spinEditTileWidth.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditTileWidth.Properties.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.spinEditTileWidth.Properties.MinValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditTileWidth.Size = new System.Drawing.Size(102, 20);
            this.spinEditTileWidth.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Location = new System.Drawing.Point(5, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(51, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Tile Width:";
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl5.Location = new System.Drawing.Point(188, 56);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(68, 13);
            this.labelControl5.TabIndex = 11;
            this.labelControl5.Text = "Image Height:";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl3.Location = new System.Drawing.Point(188, 30);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(54, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Tile Height:";
            // 
            // spinEditHeightInPixels
            // 
            this.spinEditHeightInPixels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEditHeightInPixels.EditValue = new decimal(new int[] {
            7874,
            0,
            0,
            0});
            this.spinEditHeightInPixels.Location = new System.Drawing.Point(260, 53);
            this.spinEditHeightInPixels.MenuManager = this.barManager1;
            this.spinEditHeightInPixels.Name = "spinEditHeightInPixels";
            this.spinEditHeightInPixels.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditHeightInPixels.Properties.IsFloatValue = false;
            this.spinEditHeightInPixels.Properties.Mask.EditMask = "######0 Pixels";
            this.spinEditHeightInPixels.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditHeightInPixels.Properties.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.spinEditHeightInPixels.Properties.MinValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditHeightInPixels.Size = new System.Drawing.Size(102, 20);
            this.spinEditHeightInPixels.TabIndex = 4;
            // 
            // spinEditTileHeight
            // 
            this.spinEditTileHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEditTileHeight.EditValue = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.spinEditTileHeight.Location = new System.Drawing.Point(260, 27);
            this.spinEditTileHeight.MenuManager = this.barManager1;
            this.spinEditTileHeight.Name = "spinEditTileHeight";
            this.spinEditTileHeight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditTileHeight.Properties.IsFloatValue = false;
            this.spinEditTileHeight.Properties.Mask.EditMask = "######0 Metres";
            this.spinEditTileHeight.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditTileHeight.Properties.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.spinEditTileHeight.Properties.MinValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditTileHeight.Size = new System.Drawing.Size(102, 20);
            this.spinEditTileHeight.TabIndex = 2;
            // 
            // spinEditWidthInPixels
            // 
            this.spinEditWidthInPixels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEditWidthInPixels.EditValue = new decimal(new int[] {
            7874,
            0,
            0,
            0});
            this.spinEditWidthInPixels.Location = new System.Drawing.Point(74, 53);
            this.spinEditWidthInPixels.MenuManager = this.barManager1;
            this.spinEditWidthInPixels.Name = "spinEditWidthInPixels";
            this.spinEditWidthInPixels.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditWidthInPixels.Properties.IsFloatValue = false;
            this.spinEditWidthInPixels.Properties.Mask.EditMask = "######0 Pixels";
            this.spinEditWidthInPixels.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditWidthInPixels.Properties.MaxValue = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.spinEditWidthInPixels.Properties.MinValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.spinEditWidthInPixels.Size = new System.Drawing.Size(102, 20);
            this.spinEditWidthInPixels.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl4.Location = new System.Drawing.Point(5, 56);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(65, 13);
            this.labelControl4.TabIndex = 8;
            this.labelControl4.Text = "Image Width:";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.spinEdit2);
            this.groupControl1.Controls.Add(this.spinEdit1);
            this.groupControl1.Controls.Add(this.checkEditUseFilename);
            this.groupControl1.Controls.Add(this.checkEditUseCoords);
            this.groupControl1.Location = new System.Drawing.Point(2, 264);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(323, 93);
            this.groupControl1.TabIndex = 14;
            this.groupControl1.Text = "Calculation Method";
            // 
            // labelControl7
            // 
            this.labelControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl7.Location = new System.Drawing.Point(183, 69);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(29, 13);
            this.labelControl7.TabIndex = 17;
            this.labelControl7.Text = "Min Y:";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl6.Location = new System.Drawing.Point(31, 69);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(29, 13);
            this.labelControl6.TabIndex = 15;
            this.labelControl6.Text = "Min X:";
            // 
            // spinEdit2
            // 
            this.spinEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEdit2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit2.Location = new System.Drawing.Point(215, 67);
            this.spinEdit2.MenuManager = this.barManager1;
            this.spinEdit2.Name = "spinEdit2";
            this.spinEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit2.Properties.IsFloatValue = false;
            this.spinEdit2.Properties.Mask.EditMask = "######0 Metres";
            this.spinEdit2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEdit2.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.spinEdit2.Properties.MinValue = new decimal(new int[] {
            9999999,
            0,
            0,
            -2147483648});
            this.spinEdit2.Size = new System.Drawing.Size(102, 20);
            this.spinEdit2.TabIndex = 16;
            // 
            // spinEdit1
            // 
            this.spinEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(66, 66);
            this.spinEdit1.MenuManager = this.barManager1;
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEdit1.Properties.IsFloatValue = false;
            this.spinEdit1.Properties.Mask.EditMask = "######0 Metres";
            this.spinEdit1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEdit1.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.spinEdit1.Properties.MinValue = new decimal(new int[] {
            9999999,
            0,
            0,
            -2147483648});
            this.spinEdit1.Size = new System.Drawing.Size(102, 20);
            this.spinEdit1.TabIndex = 15;
            // 
            // checkEditUseFilename
            // 
            this.checkEditUseFilename.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditUseFilename.EditValue = true;
            this.checkEditUseFilename.Location = new System.Drawing.Point(5, 24);
            this.checkEditUseFilename.MenuManager = this.barManager1;
            this.checkEditUseFilename.Name = "checkEditUseFilename";
            this.checkEditUseFilename.Properties.Caption = "Use Filename";
            this.checkEditUseFilename.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditUseFilename.Properties.RadioGroupIndex = 1;
            this.checkEditUseFilename.Size = new System.Drawing.Size(149, 18);
            this.checkEditUseFilename.TabIndex = 12;
            this.checkEditUseFilename.CheckedChanged += new System.EventHandler(this.checkEditUseFilename_CheckedChanged);
            // 
            // checkEditUseCoords
            // 
            this.checkEditUseCoords.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEditUseCoords.Location = new System.Drawing.Point(5, 46);
            this.checkEditUseCoords.MenuManager = this.barManager1;
            this.checkEditUseCoords.Name = "checkEditUseCoords";
            this.checkEditUseCoords.Properties.Caption = "Use the Following Minimum X and Y:";
            this.checkEditUseCoords.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditUseCoords.Properties.RadioGroupIndex = 1;
            this.checkEditUseCoords.Size = new System.Drawing.Size(193, 18);
            this.checkEditUseCoords.TabIndex = 13;
            this.checkEditUseCoords.TabStop = false;
            this.checkEditUseCoords.CheckedChanged += new System.EventHandler(this.checkEditUseCoords_CheckedChanged);
            // 
            // btnRegister
            // 
            this.btnRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRegister.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnRegister.Appearance.Options.UseFont = true;
            this.btnRegister.Location = new System.Drawing.Point(546, 349);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(75, 23);
            this.btnRegister.TabIndex = 5;
            this.btnRegister.Text = "Register";
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // btnSelectFiles
            // 
            this.btnSelectFiles.Location = new System.Drawing.Point(2, 2);
            this.btnSelectFiles.Name = "btnSelectFiles";
            this.btnSelectFiles.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFiles.TabIndex = 0;
            this.btnSelectFiles.Text = "Select File(s)";
            this.btnSelectFiles.Click += new System.EventHandler(this.btnSelectFiles_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(2, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(700, 232);
            this.gridSplitContainer1.TabIndex = 17;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp01325ATMappingUtilitiesRegisterFilesStructureBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(700, 232);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01325ATMappingUtilitiesRegisterFilesStructureBindingSource
            // 
            this.sp01325ATMappingUtilitiesRegisterFilesStructureBindingSource.DataMember = "sp01325_AT_Mapping_Utilities_Register_Files_Structure";
            this.sp01325ATMappingUtilitiesRegisterFilesStructureBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // dataSet_AT_TreePicker
            // 
            this.dataSet_AT_TreePicker.DataSetName = "DataSet_AT_TreePicker";
            this.dataSet_AT_TreePicker.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStatus,
            this.colFileName,
            this.colFilePath});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colStatus;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = "Filename Error!";
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFileName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.ViewCaption = "Files Selected For Registering";
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            // 
            // colFileName
            // 
            this.colFileName.Caption = "File Name";
            this.colFileName.FieldName = "FileName";
            this.colFileName.Name = "colFileName";
            this.colFileName.OptionsColumn.AllowEdit = false;
            this.colFileName.OptionsColumn.AllowFocus = false;
            this.colFileName.OptionsColumn.ReadOnly = true;
            this.colFileName.OptionsColumn.TabStop = false;
            this.colFileName.Visible = true;
            this.colFileName.VisibleIndex = 1;
            this.colFileName.Width = 142;
            // 
            // colFilePath
            // 
            this.colFilePath.Caption = "File Path";
            this.colFilePath.FieldName = "FilePath";
            this.colFilePath.Name = "colFilePath";
            this.colFilePath.OptionsColumn.AllowEdit = false;
            this.colFilePath.OptionsColumn.AllowFocus = false;
            this.colFilePath.OptionsColumn.ReadOnly = true;
            this.colFilePath.OptionsColumn.TabStop = false;
            this.colFilePath.Visible = true;
            this.colFilePath.VisibleIndex = 2;
            this.colFilePath.Width = 393;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(708, 443);
            this.xtraTabPage2.Text = "Synchronise Lat \\ Long Values";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.Controls.Add(this.labelControl8);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Information";
            this.splitContainerControl2.Panel2.Controls.Add(this.groupControl4);
            this.splitContainerControl2.Panel2.Controls.Add(this.groupControl3);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnClose2);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnSynchronise);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(708, 443);
            this.splitContainerControl2.SplitterPosition = 69;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // labelControl8
            // 
            this.labelControl8.AllowHtmlString = true;
            this.labelControl8.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            this.labelControl8.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl8.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl8.Location = new System.Drawing.Point(0, 0);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(704, 45);
            this.labelControl8.TabIndex = 9;
            this.labelControl8.Text = resources.GetString("labelControl8.Text");
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.checkEditAllData);
            this.groupControl4.Controls.Add(this.checkEditMissingData);
            this.groupControl4.Location = new System.Drawing.Point(11, 96);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(241, 72);
            this.groupControl4.TabIndex = 20;
            this.groupControl4.Text = "Update Condition";
            // 
            // checkEditAllData
            // 
            this.checkEditAllData.Location = new System.Drawing.Point(6, 48);
            this.checkEditAllData.MenuManager = this.barManager1;
            this.checkEditAllData.Name = "checkEditAllData";
            this.checkEditAllData.Properties.Caption = "Update All Records";
            this.checkEditAllData.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditAllData.Properties.RadioGroupIndex = 1;
            this.checkEditAllData.Size = new System.Drawing.Size(230, 19);
            this.checkEditAllData.TabIndex = 1;
            this.checkEditAllData.TabStop = false;
            // 
            // checkEditMissingData
            // 
            this.checkEditMissingData.EditValue = true;
            this.checkEditMissingData.Location = new System.Drawing.Point(6, 26);
            this.checkEditMissingData.MenuManager = this.barManager1;
            this.checkEditMissingData.Name = "checkEditMissingData";
            this.checkEditMissingData.Properties.Caption = "Update Only Records With Missing Data";
            this.checkEditMissingData.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditMissingData.Properties.RadioGroupIndex = 1;
            this.checkEditMissingData.Size = new System.Drawing.Size(230, 19);
            this.checkEditMissingData.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.checkEditAssets);
            this.groupControl3.Controls.Add(this.checkEditTrees);
            this.groupControl3.Location = new System.Drawing.Point(11, 9);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(241, 72);
            this.groupControl3.TabIndex = 19;
            this.groupControl3.Text = "Update Data Type";
            // 
            // checkEditAssets
            // 
            this.checkEditAssets.EditValue = true;
            this.checkEditAssets.Location = new System.Drawing.Point(6, 48);
            this.checkEditAssets.MenuManager = this.barManager1;
            this.checkEditAssets.Name = "checkEditAssets";
            this.checkEditAssets.Properties.Caption = "Asset Management - Assets";
            this.checkEditAssets.Size = new System.Drawing.Size(230, 19);
            this.checkEditAssets.TabIndex = 1;
            // 
            // checkEditTrees
            // 
            this.checkEditTrees.EditValue = true;
            this.checkEditTrees.Location = new System.Drawing.Point(6, 26);
            this.checkEditTrees.MenuManager = this.barManager1;
            this.checkEditTrees.Name = "checkEditTrees";
            this.checkEditTrees.Properties.Caption = "Amenity Trees - Trees";
            this.checkEditTrees.Size = new System.Drawing.Size(230, 19);
            this.checkEditTrees.TabIndex = 0;
            // 
            // btnClose2
            // 
            this.btnClose2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose2.Location = new System.Drawing.Point(628, 340);
            this.btnClose2.Name = "btnClose2";
            this.btnClose2.Size = new System.Drawing.Size(75, 23);
            this.btnClose2.TabIndex = 18;
            this.btnClose2.Text = "Close";
            this.btnClose2.Click += new System.EventHandler(this.btnClose2_Click);
            // 
            // btnSynchronise
            // 
            this.btnSynchronise.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSynchronise.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSynchronise.Appearance.Options.UseFont = true;
            this.btnSynchronise.Location = new System.Drawing.Point(548, 340);
            this.btnSynchronise.Name = "btnSynchronise";
            this.btnSynchronise.Size = new System.Drawing.Size(75, 23);
            this.btnSynchronise.TabIndex = 17;
            this.btnSynchronise.Text = "Synchronise";
            this.btnSynchronise.Click += new System.EventHandler(this.btnSynchronise_Click);
            // 
            // sp01325_AT_Mapping_Utilities_Register_Files_StructureTableAdapter
            // 
            this.sp01325_AT_Mapping_Utilities_Register_Files_StructureTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenuGrid1
            // 
            this.popupMenuGrid1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRemove),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearGrid, true)});
            this.popupMenuGrid1.Manager = this.barManager1;
            this.popupMenuGrid1.Name = "popupMenuGrid1";
            // 
            // bbiRemove
            // 
            this.bbiRemove.Caption = "Remove Selected Files From List";
            this.bbiRemove.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRemove.Glyph")));
            this.bbiRemove.Id = 26;
            this.bbiRemove.Name = "bbiRemove";
            this.bbiRemove.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRemove_ItemClick);
            // 
            // bbiClearGrid
            // 
            this.bbiClearGrid.Caption = "Clear List";
            this.bbiClearGrid.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiClearGrid.Glyph")));
            this.bbiClearGrid.Id = 27;
            this.bbiClearGrid.Name = "bbiClearGrid";
            this.bbiClearGrid.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearGrid_ItemClick);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_AT_Mapping_Utilities
            // 
            this.ClientSize = new System.Drawing.Size(713, 469);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_AT_Mapping_Utilities";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mapping Utilities";
            this.Load += new System.EventHandler(this.frm_AT_Mapping_Utilities_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTileWidth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHeightInPixels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditTileHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditWidthInPixels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditUseFilename.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditUseCoords.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01325ATMappingUtilitiesRegisterFilesStructureBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMissingData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAssets.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTrees.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuGrid1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SimpleButton btnSelectFiles;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SpinEdit spinEditTileWidth;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnRegister;
        private DataSet_AT_TreePicker dataSet_AT_TreePicker;
        private System.Windows.Forms.BindingSource sp01325ATMappingUtilitiesRegisterFilesStructureBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01325_AT_Mapping_Utilities_Register_Files_StructureTableAdapter sp01325_AT_Mapping_Utilities_Register_Files_StructureTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colFileName;
        private DevExpress.XtraGrid.Columns.GridColumn colFilePath;
        private DevExpress.XtraBars.BarButtonItem bbiRemove;
        private DevExpress.XtraBars.PopupMenu popupMenuGrid1;
        private DevExpress.XtraBars.BarButtonItem bbiClearGrid;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SpinEdit spinEditTileHeight;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SpinEdit spinEditWidthInPixels;
        private DevExpress.XtraEditors.SpinEdit spinEditHeightInPixels;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditUseFilename;
        private DevExpress.XtraEditors.CheckEdit checkEditUseCoords;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.SpinEdit spinEdit2;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.SimpleButton btnClose2;
        private DevExpress.XtraEditors.SimpleButton btnSynchronise;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckEdit checkEditAssets;
        private DevExpress.XtraEditors.CheckEdit checkEditTrees;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.CheckEdit checkEditAllData;
        private DevExpress.XtraEditors.CheckEdit checkEditMissingData;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

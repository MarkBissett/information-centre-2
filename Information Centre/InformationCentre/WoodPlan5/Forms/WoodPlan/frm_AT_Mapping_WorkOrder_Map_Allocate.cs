using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_Mapping_WorkOrder_Map_Allocate : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strMapDescription;
        public string strMapRemarks;
        public int intSelectedWorkOrderID = 0;

        #endregion

        public frm_AT_Mapping_WorkOrder_Map_Allocate()
        {
            InitializeComponent();
        }

        private void frm_AT_Mapping_WorkOrder_Map_Allocate_Load(object sender, EventArgs e)
        {
            this.FormID = 200415;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;
            this.Text += "  [Map: " + (string.IsNullOrEmpty(strMapDescription) ? "No Description" : strMapDescription) + "]";

            sp01304_AT_Tree_Picker_WorkOrders_For_Allocate_MapTableAdapter.Connection.ConnectionString = strConnectionString;
            FromDate.DateTime = this.GlobalSettings.ViewedStartDate.Date;
            ToDate.DateTime = this.GlobalSettings.ViewedEndDate.Date;

            teMapDescription.EditValue = strMapDescription;
            meMapRemarks.EditValue = strMapRemarks;
            LoadWorkOrdersList();
        }


        #region GridControl1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Work Orders available");
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }


        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        #endregion


        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadWorkOrdersList();
        }

        private void LoadWorkOrdersList()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01304_AT_Tree_Picker_WorkOrders_For_Allocate_MapTableAdapter.Fill(this.dataSet_AT_WorkOrders.sp01304_AT_Tree_Picker_WorkOrders_For_Allocate_Map, Convert.ToDateTime(FromDate.Text), Convert.ToDateTime(ToDate.Text));
            view.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle < 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Work Order to allocate the map to before proceeding.", "Allocate Work Order Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intSelectedWorkOrderID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderID"));
            strMapDescription = teMapDescription.EditValue.ToString();
            strMapRemarks = meMapRemarks.EditValue.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


    }
}

